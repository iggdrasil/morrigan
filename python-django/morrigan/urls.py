#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from morrigan_common.views import HomePageView
import morrigan_characters.urls

admin.site.site_header = _('Morrigan administration')

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view()),
    url(r'', include(morrigan_characters.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login',
                           {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/'},
                        name='logout'),
    url(r'^admin/', include(admin.site.urls))
)
