# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ability',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('type', models.CharField(verbose_name='Type', max_length=1, choices=[('A', 'Ability'), ('S', 'Skill'), ('L', 'Language')])),
                ('delta_min_max', models.IntegerField(default=0)),
                ('visible', models.BooleanField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AbilityKind',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name')),
                ('slug', models.SlugField(verbose_name='Form', blank=True, unique=True, null=True)),
                ('description', models.TextField(verbose_name='Description', null=True, blank=True)),
                ('attack_formula', models.TextField(verbose_name='Attack formula', null=True, blank=True)),
                ('defense_formula', models.TextField(verbose_name='Defense formula', null=True, blank=True)),
                ('gain_formula', models.TextField(verbose_name='Gain formula', null=True, blank=True)),
                ('help_text', models.TextField(verbose_name='Help', null=True, blank=True)),
                ('fighting_action', models.BooleanField(verbose_name='Is a fighting action?', default=False)),
            ],
        ),
        migrations.CreateModel(
            name='DamageType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Effect',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('length', models.IntegerField(verbose_name='Length', null=True, blank=True)),
                ('period', models.IntegerField(verbose_name='Period', null=True, blank=True)),
                ('physical_description', models.TextField(verbose_name='Physical description', null=True, blank=True)),
                ('formula', models.TextField(verbose_name='Formula', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EffectType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='GenericItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('price', models.IntegerField(verbose_name='Price', null=True, blank=True)),
                ('equipable', models.BooleanField(verbose_name='Equipable', default=False)),
                ('size_index', models.IntegerField(verbose_name='Size index', null=True, blank=True)),
                ('effect', models.ForeignKey(to='morrigan_common.Effect', blank=True, null=True, related_name='generic_items')),
                ('equiped_effect', models.ForeignKey(to='morrigan_common.Effect', blank=True, null=True, related_name='equiped_generic_items')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ItemType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('durability', models.IntegerField(verbose_name='Durability', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('timezone', models.CharField(default='Europe/Paris', verbose_name='Timezone', max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('index', models.IntegerField(verbose_name='Index')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Stylesheet',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name', max_length=32)),
                ('url', models.URLField(verbose_name='Url')),
                ('default', models.BooleanField(verbose_name='Default')),
            ],
        ),
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='player',
            name='stylesheet',
            field=models.ForeignKey(verbose_name='Stylesheet', blank=True, to='morrigan_common.Stylesheet', null=True, related_name='players'),
        ),
        migrations.AddField(
            model_name='player',
            name='user',
            field=models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='genericitem',
            name='material',
            field=models.ForeignKey(to='morrigan_common.Material', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='genericitem',
            name='technology',
            field=models.ForeignKey(to='morrigan_common.Technology', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='genericitem',
            name='type',
            field=models.ForeignKey(to='morrigan_common.ItemType'),
        ),
        migrations.AddField(
            model_name='effect',
            name='effect_type',
            field=models.ForeignKey(verbose_name='Effect type', to='morrigan_common.EffectType'),
        ),
        migrations.AddField(
            model_name='ability',
            name='kind',
            field=models.ForeignKey(to='morrigan_common.AbilityKind'),
        ),
    ]
