#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
Main models description
"""

from django.conf import settings
from django.db.models.signals import post_save
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class GenericType(models.Model):
    """
    Base abstract class for generic type
    """
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    order = models.IntegerField(default=10)
    available = models.BooleanField(default=True)

    class Meta:
        abstract = True
        ordering = ('order',)

    def __str__(self):
        return self.name

class Stylesheet(models.Model):
    '''
    Custom CSS
    '''
    name = models.TextField(_("Name"), max_length=32)
    url = models.URLField(_("Url"))
    default = models.BooleanField(_("Default"))

    def __unicode__(self):
        return self.name

class Player(models.Model):
    """
    Morrigan user
    """
    user = models.OneToOneField(User, related_name='profile')
    timezone = models.CharField(_("Timezone"), max_length=256,
                                default=settings.TIME_ZONE)
    stylesheet = models.ForeignKey(Stylesheet, blank=True, null=True,
                    verbose_name=_("Stylesheet"), related_name='players')

    def __str__(self):
        return str(self.user)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        player, created = Player.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)

class AbilityKind(GenericType):
    '''
    Kind of an ability. Eg.: intellectual, physical, etc.
    '''

    class Meta:
        verbose_name = _("Ability kind")
        verbose_name_plural = _("Ability kinds")
        ordering = ('order',)

ABILITY_TYPES = (('A', _("Ability")),
                 ('S', _("Skill")),
                 ('L', _("Language"))
                )

class Ability(GenericType):
    """
    Ability is a generic model that list ability, skills and languages
    """
    type = models.CharField(_("Type"), max_length=1, choices=ABILITY_TYPES)
    kind = models.ForeignKey("AbilityKind")
    delta_min_max = models.IntegerField(default=0,
              help_text=_("What amount can be lost if no training is done"))
    visible = models.BooleanField(default=True,
              help_text=_("Visible by the player"))

    class Meta:
        verbose_name = _("Ability")
        verbose_name_plural = _("Abilities")
        ordering = ('type', 'kind', 'order',)

class EffectType(GenericType):
    pass

class Effect(GenericType):
    """
    Effects that can affect character
    """
    effect_type = models.ForeignKey("EffectType", verbose_name=_("Effect type"))
    length = models.IntegerField(_("Length"), null=True, blank=True)
    period = models.IntegerField(_("Period"), null=True, blank=True)
    physical_description = models.TextField(_("Physical description"),
                                            blank=True, null=True)
    formula = models.TextField(_("Formula"), blank=True, null=True)

class CostFormula(models.Model):
    ap_cost_formula = models.TextField(_("Action point cost formula"),
                                                 blank=True, null=True)
    fp_cost_formula = models.TextField(_("Feed point cost formula"),
                                                 blank=True, null=True)
    mp_cost_formula = models.TextField(_("Magic point cost formula"),
                                                 blank=True, null=True)
    hp_cost_formula = models.TextField(_("Hit point cost formula"),
                                                 blank=True, null=True)
    hp_failed_cost_formula = models.TextField(
        _("Hit point cost formula on failure"), blank=True, null=True)
    bp_cost_formula = models.TextField(_("Battle point cost formula"),
                                                 blank=True, null=True)
    class Meta:
        abstract = True

class Action(models.Model):
    name = models.TextField(_("Name"))
    slug = models.SlugField(_("Form"), blank=True, null=True, unique=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    attack_formula = models.TextField(_('Attack formula'), blank=True,
                                      null=True)
    defense_formula = models.TextField(_('Defense formula'), blank=True,
                                       null=True)
    gain_formula = models.TextField(_('Gain formula'), blank=True,
                                      null=True)
    help_text = models.TextField(_('Help'), null=True, blank=True)
    fighting_action = models.BooleanField(_("Is a fighting action?"),
                                          default=False)

class DamageType(GenericType):
    pass

class ItemType(GenericType):
    pass

class Material(GenericType):
    durability = models.IntegerField(_("Durability"), null=True, blank=True)

class Size(GenericType):
    index = models.IntegerField(_("Index"))

class Technology(GenericType):
    pass

class GenericItem(GenericType):
    """
    A generic item is a kind of template to define and create new items
    """
    type = models.ForeignKey("ItemType")
    price = models.IntegerField(_("Price"), null=True, blank=True)
    equipable = models.BooleanField(_("Equipable"), default=False)
    effect = models.ForeignKey("Effect", null=True, blank=True,
                               related_name='generic_items')
    equiped_effect = models.ForeignKey("Effect", null=True, blank=True,
                               related_name='equiped_generic_items')
    technology = models.ForeignKey("Technology", null=True, blank=True)
    material = models.ForeignKey("Material", null=True, blank=True)
    size_index = models.IntegerField(_('Size index'), null=True, blank=True)
