#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models

from morrigan_common.models import Ability, DamageType, Effect, \
                                   GenericItem, GenericType
from morrigan_geography.models import Location

class Currency(GenericType):
    index = models.IntegerField(default=1)

class Item(models.Model):
    generic_item = models.ForeignKey(GenericItem)
    name = models.TextField(_("Name"), blank=True, null=True)
    description = models.TextField(_("Description"), null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    equipable = models.BooleanField(_("Equipable"), default=False)
    equiped = models.BooleanField(_("Equiped"), default=False)
    halted = models.NullBooleanField(_("Halted"), null=True, blank=True)
    natural = models.BooleanField(_("Natural"), default=False)
    character = models.ForeignKey('morrigan_characters.Character', null=True,
                                  blank=True)
    location = models.ForeignKey(Location, null=True, blank=True)

class Safe(models.Model):
    """
    A safe can contain items. Shops are kind of specialized safes.
    """
    item = models.OneToOneField(Item, related_name='safes')
    associated_key = models.ForeignKey(GenericItem, null=True, blank=True)
    size = models.IntegerField(_("Size"))
    opened = models.BooleanField(_("Opened"), default=False)

class ItemSafe(models.Model):
    """
    Items inside the safe
    """
    safe = models.ForeignKey(Safe, related_name='items')
    item = models.ForeignKey(Item, related_name='inside_a_safe')
    price = models.IntegerField(_("Price (for shops)"), null=True, blank=True)
    currency = models.ForeignKey(Currency, null=True, blank=True,
                    verbose_name=_("Currency associated to price (for shops)"))

class Robot(models.Model):
    name = models.TextField(_("Name"))

class RobotAbility(models.Model):
    robot = models.ForeignKey("Robot")
    ability = models.ForeignKey(Ability)
    score = models.IntegerField(_("Score"), default=0)

class Shop(models.Model):
    """
    A shop can be owned by a robot (automatic shops) or characters.
    """
    safe = models.OneToOneField('Safe')
    name = models.TextField(_("Name"))
    description = models.TextField(_("Description"), null=True, blank=True)
    owner = models.ForeignKey('morrigan_characters.Character', null=True,
                              blank=True)
    robot = models.ForeignKey(Robot, null=True, blank=True)
    opened = models.BooleanField(_("Opened"), default=False)

class BuyGenericItem(models.Model):
    """
    A shop can automatically buy generic items
    """
    generic_item = models.ForeignKey(GenericItem)
    shop = models.ForeignKey(Shop)
    price = models.IntegerField(_("Price (for shops)"), null=True, blank=True)
    currency = models.ForeignKey(Currency, null=True, blank=True,
                    verbose_name=_("Currency associated to price (for shops)"))
    max_number = models.IntegerField(_("Max number of item the buyer accept"),
                                     null=True, blank=True)

class GenericWeapon(models.Model):
    damage_type = models.ForeignKey(DamageType)
    generic_item = models.ForeignKey(GenericItem)
    effect = models.ForeignKey(Effect, null=True, blank=True)
    weapon_range = models.IntegerField(_("Range"), null=True, blank=True)
    min_damage = models.IntegerField(_("Minimum damage"), default=0)
    max_damage = models.IntegerField(_("Maximum damage"), default=0)
    two_hands = models.BooleanField(_("Two hands"), default=False)
    ammo_type = models.ForeignKey("AmmoType", null=True, blank=True)
    ability_1 = models.ForeignKey(Ability, related_name='generic_weapon_primary',
                                  verbose_name=_("Ability (1)"))
    ability_2 = models.ForeignKey(Ability, related_name='generic_weapon_secondary',
                                  verbose_name=_("Ability (2)"))
    skill_1 = models.ForeignKey(Ability, verbose_name=_("Skill (1)"),
                                 related_name='generic_weapon_skill_primary')
    skill_2 = models.ForeignKey(Ability, verbose_name=_("Skill (2)"),
                                 related_name='generic_weapon_skill_secondary')
    attack_bonus = models.IntegerField(_("Attack bonus"), default=0)
    defense_bonus = models.IntegerField(_("Defense bonus"), default=0)

class Weapon(models.Model):
    generic_weapon = models.ForeignKey(GenericWeapon)
    recharge = models.ForeignKey("Recharge", null=True, blank=True)
    item = models.ForeignKey("Item")

class GenericArmor(models.Model):
    generic_item = models.ForeignKey(GenericItem)
    defense_bonus = models.IntegerField(_("Defense bonus"), default=0)

class ArmorArea(models.Model):
    name = models.TextField(_("Name"))

class Armor(models.Model):
    item = models.OneToOneField(Item)
    areas = models.ManyToManyField(ArmorArea)

class GenericArmorDamage(models.Model):
    generic_armor = models.ForeignKey(GenericArmor)
    damage_type = models.ForeignKey(DamageType)
    min_absorption = models.IntegerField(_("Min absorption"), default=0)
    max_absorption = models.IntegerField(_("Max absorption"), default=0)

class AmmoType(GenericType):
    pass

class Ammo(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    ammo_type = models.ForeignKey("AmmoType", null=True, blank=True)
    effect = models.ForeignKey(Effect, null=True, blank=True)

class GenericRecharge(models.Model):
    generic_item = models.OneToOneField(GenericItem)
    ammo = models.ForeignKey(Ammo)
    max_number = models.IntegerField(_("Max number"), default=1)

class Recharge(models.Model):
    item = models.OneToOneField(Item)
    ammo_number = models.IntegerField(_("Ammo number"), default=1)
    ammo_used = models.BooleanField(_("Ammo used"), default=0)

