# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0001_initial'),
        ('morrigan_geography', '0001_initial'),
        ('morrigan_characters', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ammo',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AmmoType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Armor',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='ArmorArea',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='BuyGenericItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('price', models.IntegerField(verbose_name='Price (for shops)', null=True, blank=True)),
                ('max_number', models.IntegerField(verbose_name='Max number of item the buyer accept', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('index', models.IntegerField(default=1)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='GenericArmor',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('defense_bonus', models.IntegerField(verbose_name='Defense bonus', default=0)),
                ('generic_item', models.ForeignKey(to='morrigan_common.GenericItem')),
            ],
        ),
        migrations.CreateModel(
            name='GenericArmorDamage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('min_absorption', models.IntegerField(verbose_name='Min absorption', default=0)),
                ('max_absorption', models.IntegerField(verbose_name='Max absorption', default=0)),
                ('damage_type', models.ForeignKey(to='morrigan_common.DamageType')),
                ('generic_armor', models.ForeignKey(to='morrigan_inventory.GenericArmor')),
            ],
        ),
        migrations.CreateModel(
            name='GenericRecharge',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('max_number', models.IntegerField(verbose_name='Max number', default=1)),
                ('ammo', models.ForeignKey(to='morrigan_inventory.Ammo')),
                ('generic_item', models.OneToOneField(to='morrigan_common.GenericItem')),
            ],
        ),
        migrations.CreateModel(
            name='GenericWeapon',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('weapon_range', models.IntegerField(verbose_name='Range', null=True, blank=True)),
                ('min_damage', models.IntegerField(verbose_name='Minimum damage', default=0)),
                ('max_damage', models.IntegerField(verbose_name='Maximum damage', default=0)),
                ('two_hands', models.BooleanField(verbose_name='Two hands', default=False)),
                ('attack_bonus', models.IntegerField(verbose_name='Attack bonus', default=0)),
                ('defense_bonus', models.IntegerField(verbose_name='Defense bonus', default=0)),
                ('ability_1', models.ForeignKey(verbose_name='Ability (1)', related_name='generic_weapon_primary', to='morrigan_common.Ability')),
                ('ability_2', models.ForeignKey(verbose_name='Ability (2)', related_name='generic_weapon_secondary', to='morrigan_common.Ability')),
                ('ammo_type', models.ForeignKey(to='morrigan_inventory.AmmoType', blank=True, null=True)),
                ('damage_type', models.ForeignKey(to='morrigan_common.DamageType')),
                ('effect', models.ForeignKey(to='morrigan_common.Effect', blank=True, null=True)),
                ('generic_item', models.ForeignKey(to='morrigan_common.GenericItem')),
                ('skill_1', models.ForeignKey(verbose_name='Skill (1)', related_name='generic_weapon_skill_primary', to='morrigan_common.Ability')),
                ('skill_2', models.ForeignKey(verbose_name='Skill (2)', related_name='generic_weapon_skill_secondary', to='morrigan_common.Ability')),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name', null=True, blank=True)),
                ('description', models.TextField(verbose_name='Description', null=True, blank=True)),
                ('price', models.IntegerField(null=True, blank=True)),
                ('equipable', models.BooleanField(verbose_name='Equipable', default=False)),
                ('equiped', models.BooleanField(verbose_name='Equiped', default=False)),
                ('halted', models.NullBooleanField(verbose_name='Halted')),
                ('natural', models.BooleanField(verbose_name='Natural', default=False)),
                ('character', models.ForeignKey(to='morrigan_characters.Character', blank=True, null=True)),
                ('generic_item', models.ForeignKey(to='morrigan_common.GenericItem')),
                ('location', models.ForeignKey(to='morrigan_geography.Location', blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ItemSafe',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('price', models.IntegerField(verbose_name='Price (for shops)', null=True, blank=True)),
                ('currency', models.ForeignKey(verbose_name='Currency associated to price (for shops)', blank=True, to='morrigan_inventory.Currency', null=True)),
                ('item', models.ForeignKey(to='morrigan_inventory.Item', related_name='inside_a_safe')),
            ],
        ),
        migrations.CreateModel(
            name='Recharge',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('ammo_number', models.IntegerField(verbose_name='Ammo number', default=1)),
                ('ammo_used', models.BooleanField(verbose_name='Ammo used', default=0)),
                ('item', models.OneToOneField(to='morrigan_inventory.Item')),
            ],
        ),
        migrations.CreateModel(
            name='Robot',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='RobotAbility',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('score', models.IntegerField(verbose_name='Score', default=0)),
                ('ability', models.ForeignKey(to='morrigan_common.Ability')),
                ('robot', models.ForeignKey(to='morrigan_inventory.Robot')),
            ],
        ),
        migrations.CreateModel(
            name='Safe',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('size', models.IntegerField(verbose_name='Size')),
                ('opened', models.BooleanField(verbose_name='Opened', default=False)),
                ('associated_key', models.ForeignKey(to='morrigan_common.GenericItem', blank=True, null=True)),
                ('item', models.OneToOneField(related_name='safes', to='morrigan_inventory.Item')),
            ],
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', null=True, blank=True)),
                ('opened', models.BooleanField(verbose_name='Opened', default=False)),
                ('owner', models.ForeignKey(to='morrigan_characters.Character', blank=True, null=True)),
                ('robot', models.ForeignKey(to='morrigan_inventory.Robot', blank=True, null=True)),
                ('safe', models.OneToOneField(to='morrigan_inventory.Safe')),
            ],
        ),
        migrations.CreateModel(
            name='Weapon',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('generic_weapon', models.ForeignKey(to='morrigan_inventory.GenericWeapon')),
                ('item', models.ForeignKey(to='morrigan_inventory.Item')),
                ('recharge', models.ForeignKey(to='morrigan_inventory.Recharge', blank=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='itemsafe',
            name='safe',
            field=models.ForeignKey(to='morrigan_inventory.Safe', related_name='items'),
        ),
        migrations.AddField(
            model_name='buygenericitem',
            name='currency',
            field=models.ForeignKey(verbose_name='Currency associated to price (for shops)', blank=True, to='morrigan_inventory.Currency', null=True),
        ),
        migrations.AddField(
            model_name='buygenericitem',
            name='generic_item',
            field=models.ForeignKey(to='morrigan_common.GenericItem'),
        ),
        migrations.AddField(
            model_name='buygenericitem',
            name='shop',
            field=models.ForeignKey(to='morrigan_inventory.Shop'),
        ),
        migrations.AddField(
            model_name='armor',
            name='areas',
            field=models.ManyToManyField(to='morrigan_inventory.ArmorArea'),
        ),
        migrations.AddField(
            model_name='armor',
            name='item',
            field=models.OneToOneField(to='morrigan_inventory.Item'),
        ),
        migrations.AddField(
            model_name='ammo',
            name='ammo_type',
            field=models.ForeignKey(to='morrigan_inventory.AmmoType', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='ammo',
            name='effect',
            field=models.ForeignKey(to='morrigan_common.Effect', blank=True, null=True),
        ),
    ]
