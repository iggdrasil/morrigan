# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_geography', '0001_initial'),
        ('morrigan_characters', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Engaged',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('init_1', models.BooleanField(verbose_name='Character 1 has the initiative', default=True)),
                ('distant', models.BooleanField(verbose_name='Distant engagement', default=False)),
                ('character_1', models.ForeignKey(to='morrigan_characters.Character', related_name='involved_primary')),
                ('character_2', models.ForeignKey(to='morrigan_characters.Character', related_name='involved_secondary')),
                ('location', models.ForeignKey(to='morrigan_geography.Location')),
            ],
        ),
    ]
