#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models

from morrigan_geography.models import Location
from morrigan_characters.models import Character

class Engaged(models.Model):
    character_1 = models.ForeignKey(Character,
                                    related_name='involved_primary')
    character_2 = models.ForeignKey(Character,
                                     related_name='involved_secondary')
    init_1 = models.BooleanField(_("Character 1 has the initiative"),
                                 default=True)
    location = models.ForeignKey(Location)
    distant = models.BooleanField(_("Distant engagement"), default=False)
