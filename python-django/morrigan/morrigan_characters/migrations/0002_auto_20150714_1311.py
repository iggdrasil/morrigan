# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0001_initial'),
        ('morrigan_geography', '0001_initial'),
        ('morrigan_characters', '0001_initial'),
        ('morrigan_inventory', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='charactermoney',
            name='currency',
            field=models.ForeignKey(to='morrigan_inventory.Currency'),
        ),
        migrations.AddField(
            model_name='charactereffect',
            name='character',
            field=models.ForeignKey(to='morrigan_characters.Character'),
        ),
        migrations.AddField(
            model_name='charactereffect',
            name='effect',
            field=models.ForeignKey(to='morrigan_common.Effect'),
        ),
        migrations.AddField(
            model_name='characterability',
            name='ability',
            field=models.ForeignKey(to='morrigan_common.Ability'),
        ),
        migrations.AddField(
            model_name='characterability',
            name='character',
            field=models.ForeignKey(to='morrigan_characters.Character'),
        ),
        migrations.AddField(
            model_name='character',
            name='knowned_access',
            field=models.ManyToManyField(to='morrigan_geography.Path', blank=True, related_name='knowed_by'),
        ),
        migrations.AddField(
            model_name='character',
            name='knowned_generic_items',
            field=models.ManyToManyField(to='morrigan_common.GenericItem', blank=True, related_name='knowed_by'),
        ),
        migrations.AddField(
            model_name='character',
            name='knowned_rumors',
            field=models.ManyToManyField(to='morrigan_characters.Rumor', blank=True, related_name='knowed_by'),
        ),
        migrations.AddField(
            model_name='character',
            name='knowned_worlds',
            field=models.ManyToManyField(to='morrigan_geography.World', blank=True, related_name='knowed_by'),
        ),
        migrations.AddField(
            model_name='character',
            name='location',
            field=models.ForeignKey(verbose_name='Location', blank=True, to='morrigan_geography.Location', null=True),
        ),
        migrations.AddField(
            model_name='character',
            name='owner',
            field=models.ForeignKey(verbose_name='Owner', blank=True, to='morrigan_characters.Character', null=True),
        ),
        migrations.AddField(
            model_name='character',
            name='player',
            field=models.ForeignKey(verbose_name='Player', blank=True, to='morrigan_common.Player', null=True, related_name='characters'),
        ),
        migrations.AddField(
            model_name='character',
            name='type',
            field=models.ForeignKey(verbose_name='Type', to='morrigan_characters.CharacterType'),
        ),
        migrations.AddField(
            model_name='character',
            name='visible_items',
            field=models.ManyToManyField(to='morrigan_inventory.Item', blank=True, related_name='visible_to'),
        ),
        migrations.AddField(
            model_name='actionlog',
            name='character',
            field=models.ForeignKey(to='morrigan_characters.Character', related_name='logs'),
        ),
    ]
