# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0001_initial'),
        ('morrigan_geography', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionLog',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date', models.DateTimeField(verbose_name='Date', auto_now_add=True)),
                ('number', models.IntegerField(help_text='Per character dialing', verbose_name='Number', blank=True)),
                ('content', django_pgjson.fields.JsonBField(verbose_name='Content')),
            ],
        ),
        migrations.CreateModel(
            name='Character',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('main', models.BooleanField(verbose_name='Main', default=True)),
                ('initial_name', models.TextField(unique=True, verbose_name='Initial name', max_length=50)),
                ('slug', models.SlugField(verbose_name='Slug', blank=True, unique=True, null=True)),
                ('current_name', models.TextField(unique=True, verbose_name='Current name', null=True, blank=True)),
                ('background', models.TextField(help_text='Story before the game', verbose_name='Background', null=True, blank=True)),
                ('biography', models.TextField(help_text='Story in game', verbose_name='Biography', null=True, blank=True)),
                ('notes', models.TextField(help_text="Game master's notes about the character - not know by the player", verbose_name='Notes', null=True, blank=True)),
                ('registration_date', models.DateTimeField(verbose_name='Registration date', auto_now_add=True)),
                ('last_connection', models.DateTimeField(verbose_name='Last connection', null=True, blank=True)),
                ('ap', models.IntegerField(verbose_name='Action points', default=0)),
                ('max_ap', models.IntegerField(verbose_name='Action points - max', default=0)),
                ('fp', models.IntegerField(verbose_name='Food points', default=0)),
                ('max_fp', models.IntegerField(verbose_name='Food points - max', default=0)),
                ('mp', models.IntegerField(verbose_name='Magic points', default=0)),
                ('max_mp', models.IntegerField(verbose_name='Magic points - max', default=0)),
                ('hp', models.IntegerField(verbose_name='Hit points', default=0)),
                ('max_hp', models.IntegerField(verbose_name='Hit points - max', default=0)),
                ('bp', models.IntegerField(verbose_name='Battle points', default=0)),
                ('max_bp', models.IntegerField(verbose_name='Battle points - max', default=0)),
                ('arrival_date', models.DateTimeField(help_text='Arrival date in the current location', verbose_name='Arrival date', null=True, blank=True)),
                ('step_number_index', models.FloatField(verbose_name='Step number index', default=1)),
                ('delta_compass', models.IntegerField(verbose_name='Delta compass', default=0, help_text='To what extent this character is lost')),
            ],
        ),
        migrations.CreateModel(
            name='CharacterAbility',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('training_gauge', models.IntegerField(verbose_name='Training gauge', default=0)),
                ('current_xp', models.IntegerField(verbose_name='Current XP', default=0)),
                ('max_xp', models.IntegerField(verbose_name='Max XP', default=0)),
                ('score_calculated', models.IntegerField(verbose_name='Score calculated', default=0)),
                ('score_max', models.IntegerField(verbose_name='Score max', default=0)),
            ],
        ),
        migrations.CreateModel(
            name='CharacterEffect',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('start_date', models.DateField(auto_now_add=True)),
                ('end_date', models.DateField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CharacterMoney',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('amount', models.IntegerField(default=0)),
                ('character', models.ForeignKey(to='morrigan_characters.Character')),
            ],
        ),
        migrations.CreateModel(
            name='CharacterType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Following',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('start_date', models.DateTimeField(verbose_name='Start date', auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='FollowingType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='GameMaster',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(unique=True, verbose_name='Name')),
                ('last_connection', models.DateTimeField(verbose_name='Last connection', null=True, blank=True)),
                ('can_create_news', models.BooleanField(verbose_name='Can create news', default=False)),
                ('can_edit_news', models.BooleanField(verbose_name='Can edit news', default=False)),
                ('can_register', models.BooleanField(verbose_name='Can register', default=False)),
                ('can_recruit', models.BooleanField(help_text='Can pass ads for new characters', verbose_name='Can recruit', default=False)),
                ('can_edit_rules', models.BooleanField(verbose_name='Can edit rules', default=False)),
                ('can_edit_world', models.BooleanField(verbose_name='Can edit the world', default=False)),
                ('is_game_admin', models.BooleanField(verbose_name='Is game admin', default=False)),
                ('player', models.ForeignKey(to='morrigan_common.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Itinerary',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('initial_steps', models.IntegerField(verbose_name='Initial steps', null=True, blank=True)),
                ('remaining_steps', models.IntegerField(verbose_name='Remaining steps', null=True, blank=True)),
                ('date', models.DateTimeField(verbose_name='Date', auto_now_add=True)),
                ('erase_index', models.IntegerField(verbose_name='Erase index', default=1)),
                ('step_deep', models.IntegerField(verbose_name='Step deep', default=0)),
                ('step_size', models.IntegerField(verbose_name='Step size', default=0)),
                ('arrival', models.ForeignKey(to='morrigan_geography.Location', blank=True, null=True, related_name='itinerary_arrival')),
                ('path', models.ForeignKey(verbose_name='Path', blank=True, to='morrigan_geography.Path', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='KnownedStep',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('step_deep', models.IntegerField(verbose_name='Step deep', default=0)),
                ('step_size', models.IntegerField(verbose_name='Step size', default=0)),
                ('end_date', models.DateTimeField(verbose_name='End date', null=True, blank=True)),
                ('given_name', models.TextField(verbose_name='Given name', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('can_be_mount', models.BooleanField(verbose_name='Can be mount', default=False)),
                ('size_index', models.IntegerField(verbose_name='Size index', null=True, blank=True)),
                ('open_for_registration', models.BooleanField(verbose_name='Open for registration', default=True)),
                ('effect', models.ForeignKey(verbose_name='Effect', blank=True, to='morrigan_common.Effect', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Rumor',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Rumor')),
                ('content', models.TextField(verbose_name='Content')),
                ('radius', models.IntegerField(verbose_name='Radius', null=True, blank=True)),
                ('difficulty', models.FloatField(verbose_name='Difficulty', null=True, blank=True)),
                ('start_date', models.DateField(verbose_name='Start date', auto_now_add=True)),
                ('end_date', models.DateField(verbose_name='End date', null=True, blank=True)),
                ('character', models.ForeignKey(to='morrigan_characters.Character', blank=True, null=True)),
                ('language', models.ForeignKey(to='morrigan_common.Ability')),
                ('location', models.ForeignKey(to='morrigan_geography.Location')),
                ('sector', models.ForeignKey(to='morrigan_geography.Sector', blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sex',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Shape',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('size_index', models.IntegerField(verbose_name='Size index', null=True, blank=True)),
                ('is_current_shape', models.BooleanField(verbose_name='Is current shape', default=False)),
                ('description', models.TextField(verbose_name='Description', null=True, blank=True)),
                ('current_description', models.TextField(verbose_name='Current description', null=True, blank=True)),
                ('image', models.ImageField(upload_to='', null=True, verbose_name='Image', blank=True)),
                ('step_deep', models.IntegerField(verbose_name='Step deep', default=0)),
                ('step_size', models.IntegerField(verbose_name='Step size', default=0)),
                ('character', models.ForeignKey(to='morrigan_characters.Character', related_name='shapes')),
                ('race', models.ForeignKey(to='morrigan_characters.Race')),
                ('sex', models.ForeignKey(to='morrigan_characters.Sex')),
            ],
        ),
        migrations.CreateModel(
            name='StepType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='shape',
            name='step_type',
            field=models.ForeignKey(verbose_name='Step type', blank=True, to='morrigan_characters.StepType', null=True),
        ),
        migrations.AddField(
            model_name='knownedstep',
            name='followed_shape',
            field=models.ForeignKey(verbose_name='Followed shape', blank=True, to='morrigan_characters.Shape', null=True),
        ),
        migrations.AddField(
            model_name='knownedstep',
            name='following_character',
            field=models.ForeignKey(verbose_name='Following character', blank=True, to='morrigan_characters.Character', null=True),
        ),
        migrations.AddField(
            model_name='knownedstep',
            name='step_type',
            field=models.ForeignKey(to='morrigan_characters.StepType', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='itinerary',
            name='shape',
            field=models.ForeignKey(verbose_name='Shape', to='morrigan_characters.Shape'),
        ),
        migrations.AddField(
            model_name='itinerary',
            name='start',
            field=models.ForeignKey(to='morrigan_geography.Location', blank=True, null=True, related_name='itinerary_start'),
        ),
        migrations.AddField(
            model_name='itinerary',
            name='step_type',
            field=models.ForeignKey(verbose_name='Step type', to='morrigan_characters.StepType'),
        ),
        migrations.AddField(
            model_name='following',
            name='followed',
            field=models.ForeignKey(to='morrigan_characters.Shape', related_name='followed_by'),
        ),
        migrations.AddField(
            model_name='following',
            name='follower',
            field=models.ForeignKey(to='morrigan_characters.Character', related_name='following'),
        ),
        migrations.AddField(
            model_name='following',
            name='type',
            field=models.ForeignKey(to='morrigan_characters.FollowingType'),
        ),
    ]
