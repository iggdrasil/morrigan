#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from morrigan_common.views import MorriganPermission

from rest_framework import generics
from morrigan_characters import serializers, models
from rest_framework.permissions import BasePermission

class CharacterRetrieve(generics.RetrieveAPIView):
    """
    Generic retrieve view for character
    """
    lookup_field = 'slug'
    serializer_class = serializers.CharacterFullSerializer
    queryset = models.Character.objects.all()
    permission_classes = (MorriganPermission,)
    permission_type = 'full_view'

class CharacterFullInformation(CharacterRetrieve):
    pass

class CharacterMenu(CharacterRetrieve):
    serializer_class = serializers.MenuSerializer
