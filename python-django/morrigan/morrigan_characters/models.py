#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.contrib.gis.db import models
from django.db.models import Max
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from django_pgjson.fields import JsonBField

from morrigan_common.models import GenericType, Effect, Stylesheet, Player, \
                                   Ability
from morrigan_geography.models import World, Path, Location, Sector
from morrigan_inventory.models import GenericItem, Item, Currency

class Sex(GenericType):
    pass

class CharacterType(GenericType):
    pass

class Character(models.Model):
    """
    Character for player and NPC.
    Can be auxiliary character attached to one main character (main field to
    True) by owner field.
    Background is the story before the game, biography is the story during
    the game.
    """
    player = models.ForeignKey(Player, null=True, blank=True,
                           verbose_name=_("Player"), related_name='characters')
    main = models.BooleanField(_("Main"), default=True)
    owner = models.ForeignKey('self', null=True, blank=True,
                              verbose_name=_("Owner"))
    initial_name = models.TextField(_("Initial name"), max_length=50,
                                    unique=True)
    slug = models.SlugField(_("Slug"), blank=True, null=True, unique=True)
    current_name = models.TextField(_("Current name"), unique=True, null=True,
                                    blank=True)
    background = models.TextField(_("Background"), null=True, blank=True,
                                  help_text=_("Story before the game"))
    biography = models.TextField(_("Biography"), null=True, blank=True,
                                  help_text=_("Story in game"))
    notes = models.TextField(_("Notes"), null=True, blank=True,
        help_text=_("Game master's notes about the character - "\
                    "not know by the player"))
    registration_date = models.DateTimeField(_("Registration date"),
                                             auto_now_add=True)
    last_connection = models.DateTimeField(_("Last connection"), null=True,
                                           blank=True)
    ap = models.IntegerField(_("Action points"), default=0)
    max_ap = models.IntegerField(_("Action points - max"), default=0)
    fp = models.IntegerField(_("Food points"), default=0)
    max_fp = models.IntegerField(_("Food points - max"), default=0)
    mp = models.IntegerField(_("Magic points"), default=0)
    max_mp = models.IntegerField(_("Magic points - max"), default=0)
    hp = models.IntegerField(_("Hit points"), default=0)
    max_hp = models.IntegerField(_("Hit points - max"), default=0)
    bp = models.IntegerField(_("Battle points"), default=0)
    max_bp = models.IntegerField(_("Battle points - max"), default=0)
    type = models.ForeignKey(CharacterType, verbose_name=_("Type"))
    location = models.ForeignKey(Location, null=True, blank=True,
                                 verbose_name=_("Location"))
    arrival_date = models.DateTimeField(_("Arrival date"), null=True,
            blank=True, help_text=_("Arrival date in the current location"))
    step_number_index = models.FloatField(_("Step number index"), default=1)
    delta_compass = models.IntegerField(_("Delta compass"), default=0,
                help_text=_("To what extent this character is lost"))
    knowned_access = models.ManyToManyField(Path, related_name='knowed_by',
                                            blank=True)
    knowned_worlds = models.ManyToManyField(World, related_name='knowed_by',
                                            blank=True)
    knowned_generic_items = models.ManyToManyField(GenericItem,
                                        related_name='knowed_by', blank=True)
    knowned_rumors = models.ManyToManyField('Rumor', related_name='knowed_by',
                                            blank=True)
    visible_items = models.ManyToManyField(Item, related_name='visible_to',
                                           blank=True)

    class Meta:
        verbose_name = _("Character")
        verbose_name_plural = _("Characters")

    def has_permission(self, permission_type, user):
        if permission_type == 'full_view' and self.player and \
           self.player.user == user:
            return True
        return False

    def __str__(self):
        return self.name

    @property
    def name(self):
        return self.current_name or self.initial_name

    @property
    def current_shape(self):
        q = self.shapes.filter(is_current_shape=True)
        if not q.count():
            return None
        return q.all()[0]

    @property
    def is_conscious(self):
        return self.hp > 0

    @property
    def current_menu(self):
        items = []
        items.append({'label':_("Base"), 'url':''})
        if self.is_conscious:
            items.append({'label':_("Location"), 'url':''})
        items.append({'label':_("Action File"), 'url':''})
        if self.is_conscious:
            items.append({'label':_("Inventory"), 'url':''})
        items.append({'label':_("Characteristics"), 'url':''})
        if self.is_conscious:
            items.append({'label':_("Fight"), 'url':''})
            items.append({'label':_("Map"), 'url':''})
        return items

    def write_log(self, content):
        """
        Content must be a dict or string representing a dict
        """
        return ActionLog.objects.create(character=self, content=content)

@receiver(pre_save, sender=Character)
def character_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        base_slug = slugify(instance.name)
        slug = base_slug
        idx = 0
        while Character.objects.filter(slug=slug).count():
            idx += 1
            slug = base_slug + '-' + str(idx)
        instance.slug = slug

class ActionLog(models.Model):
    character = models.ForeignKey(Character, related_name='logs')
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    number = models.IntegerField(_("Number"),
                         help_text=_("Per character dialing"), blank=True)
    content = JsonBField(_("Content"))

@receiver(pre_save, sender=ActionLog)
def actionlog_number(sender, instance, *args, **kwargs):
    if not instance.number:
        v = ActionLog.objects.filter(character=instance.character
                                     ).aggregate(Max('number'))
        instance.number = (v['number__max'] or 0) + 1

class CharacterMoney(models.Model):
    character = models.ForeignKey(Character)
    currency = models.ForeignKey(Currency)
    amount = models.IntegerField(default=0)

class CharacterEffect(models.Model):
    character = models.ForeignKey("Character")
    effect = models.ForeignKey(Effect)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(null=True, blank=True)

class GameMaster(models.Model):
    player = models.ForeignKey(Player)
    name = models.TextField(_("Name"), unique=True)
    last_connection = models.DateTimeField(_("Last connection"), null=True,
                                           blank=True)
    can_create_news = models.BooleanField(_("Can create news"), default=False)
    can_edit_news = models.BooleanField(_("Can edit news"), default=False)
    can_register = models.BooleanField(_("Can register"), default=False)
    can_recruit = models.BooleanField(_("Can recruit"), default=False,
                            help_text=_("Can pass ads for new characters"))
    can_edit_rules = models.BooleanField(_("Can edit rules"), default=False)
    can_edit_world = models.BooleanField(_("Can edit the world"), default=False)
    is_game_admin = models.BooleanField(_("Is game admin"), default=False)

class Race(GenericType):
    effect = models.ForeignKey(Effect, verbose_name=_("Effect"), null=True,
                               blank=True)
    can_be_mount = models.BooleanField(_("Can be mount"), default=False)
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    open_for_registration = models.BooleanField(_("Open for registration"),
                                                default=True)

class StepType(GenericType):
    pass

class Shape(models.Model):
    """
    A shape associated to a character (one character can have multi-shape)
    """
    character = models.ForeignKey(Character, related_name='shapes')
    race = models.ForeignKey(Race)
    sex = models.ForeignKey(Sex)
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    is_current_shape = models.BooleanField(_("Is current shape"), default=False)
    description = models.TextField(_("Description"), null=True, blank=True)
    current_description = models.TextField(_("Current description"), null=True,
                                           blank=True)
    image = models.ImageField(_("Image"), blank=True, null=True)
    step_type = models.ForeignKey(StepType, verbose_name=_("Step type"),
                                  blank=True, null=True)
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)

class Itinerary(models.Model):
    """
    An itinerary used by a character (used by character tracking) following
    a path or on the map
    """
    path = models.ForeignKey(Path, verbose_name=_("Path"), null=True,
                             blank=True)
    shape = models.ForeignKey(Shape, verbose_name=_("Shape"))
    start = models.ForeignKey(Location, null=True, blank=True,
                              related_name='itinerary_start')
    arrival = models.ForeignKey(Location, null=True, blank=True,
                                related_name='itinerary_arrival')
    initial_steps = models.IntegerField(_("Initial steps"), null=True,
                                        blank=True)
    remaining_steps = models.IntegerField(_("Remaining steps"), null=True,
                                          blank=True)
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    erase_index = models.IntegerField(_("Erase index"), default=1)
    step_type = models.ForeignKey(StepType, verbose_name=_("Step type"))
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)

class CharacterAbility(models.Model):
    character = models.ForeignKey(Character)
    ability = models.ForeignKey(Ability)
    training_gauge = models.IntegerField(_("Training gauge"), default=0)
    current_xp = models.IntegerField(_("Current XP"), default=0)
    max_xp = models.IntegerField(_("Max XP"), default=0)
    score_calculated = models.IntegerField(_("Score calculated"), default=0)
    score_max = models.IntegerField(_("Score max"), default=0)

class Rumor(models.Model):
    """
    Rumors. Can be launched by players or game masters.
    """
    name = models.TextField(_("Rumor"))
    character = models.ForeignKey(Character, null=True, blank=True)
    content = models.TextField(_("Content"))
    language = models.ForeignKey(Ability)
    location = models.ForeignKey(Location)
    radius = models.IntegerField(_("Radius"), null=True, blank=True)
    sector = models.ForeignKey(Sector, null=True, blank=True)
    difficulty = models.FloatField(_("Difficulty"), null=True, blank=True)
    start_date = models.DateField(_("Start date"), auto_now_add=True)
    end_date = models.DateField(_("End date"), null=True, blank=True)

class FollowingType(GenericType):
    pass

class Following(models.Model):
    follower = models.ForeignKey(Character, related_name='following')
    followed = models.ForeignKey(Shape, related_name='followed_by')
    type = models.ForeignKey(FollowingType)
    start_date = models.DateTimeField(_("Start date"), auto_now_add=True)

class KnownedStep(models.Model):
    step_type = models.ForeignKey(StepType, null=True, blank=True)
    step_deep = models.IntegerField(_("Step deep"), default=0)
    step_size = models.IntegerField(_("Step size"), default=0)
    followed_shape = models.ForeignKey(Shape, null=True, blank=True,
                                       verbose_name=_("Followed shape"))
    following_character = models.ForeignKey(Character, null=True, blank=True,
                                        verbose_name=_("Following character"))
    end_date = models.DateTimeField(_("End date"), null=True, blank=True)
    given_name = models.TextField(_("Given name"), blank=True, null=True)
