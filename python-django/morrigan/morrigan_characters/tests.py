
import json

from django.test import Client
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APITestCase

from morrigan_characters import models

User = get_user_model()

class TestLogFile(APITestCase):
    def test_logfile_view(self):
        user = User.objects.create_user("lambda", "a@a.net", "pass")
        character_type = models.CharacterType.objects.create(
                                                name="Player Character")
        character = models.Character.objects.create(initial_name="Gros bill",
                                                    type=character_type,
                                                    player=user.profile)
        log = character.write_log({'name':'New action',
                        'description':"Something strange is happening..."})
        self.assertEqual(log.number, 1)
        log = character.write_log({'name':'New action',
                        'description':"Something VERY strange is happening..."})
        self.assertEqual(log.number, 2)
        client = Client()
        client.login(username="lambda", password="pass")

class TestChararacter(APITestCase):
    def test_character_view(self):
        user = User.objects.create_user("admin", "e@e.net", "pass")
        user.is_superuser = True
        user.save()
        user2 = User.objects.create_user("lambda", "a@a.net", "pass")
        character_type = models.CharacterType.objects.create(
                                                name="Player Character")
        character = models.Character.objects.create(initial_name="Gros bill",
                                                    type=character_type)
        url = reverse("character-full", kwargs={"slug":'gros-bill'})
        response = self.client.get(url, format='json')
        # must be authentificated
        self.assertEqual(response.status_code, 403)
        # admin can see any character
        admin_client = Client()
        admin_client.login(username="admin", password="pass")
        response = admin_client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        # lambda player cannot
        lambda_client = Client()
        lambda_client.login(username="lambda", password="pass")
        response = lambda_client.get(url, format='json')
        self.assertEqual(response.status_code, 403)
        # ...except if it is his own character
        character.player = user2.profile
        character.save()
        response = lambda_client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

    def test_character_menu(self):
        user = User.objects.create_user("lambda", "a@a.net", "pass")
        character_type = models.CharacterType.objects.create(
                                                name="Player Character")
        character = models.Character.objects.create(initial_name="Gros bill",
                                                    type=character_type,
                                                    player=user.profile)
        client = Client()
        client.login(username="lambda", password="pass")
        url = reverse("character-menu", kwargs={"slug":'gros-bill'})
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        menu = json.loads(response.content.decode("utf-8"))
        # no HP only the 3 main item in menu
        self.assertEqual(len(menu['current_menu']), 3)
        character.hp = 10
        character.save()
        # character is awake now the full menu is available
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        menu = json.loads(response.content.decode("utf-8"))
        self.assertEqual(len(menu['current_menu']), 7)
