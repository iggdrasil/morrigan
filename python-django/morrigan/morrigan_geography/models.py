#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models

from morrigan_common.models import GenericType, Effect, Ability, GenericItem, \
                                   CostFormula, Action

"""
The Morrigan geography is divided into one (or many) world(s).
Theses worlds can have sectors that contains locations.
To theses worlds can be associated maps (with x, y coordinates).
"""

class Season(GenericType):
    default = models.BooleanField(default=True)

class WorldType(GenericType):
    pass

class World(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    father = models.ForeignKey('self', null=True, blank=True)
    level = models.IntegerField(null=True, blank=True)
    effect = models.ForeignKey(Effect, null=True, blank=True)
    image = models.ImageField(blank=True, null=True)

class LocationType(GenericType):
    actions = models.ManyToManyField(Action)

class Location(models.Model):
    type = models.ForeignKey("LocationType")
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    sector = models.ForeignKey("Sector", null=True, blank=True)
    size_index = models.IntegerField(_("Size index"), null=True, blank=True)
    population_index = models.IntegerField(_("Population index"), default=0)
    factor_step_erasure = models.FloatField(_("Factor step erasure"), default=1)
    factor_step_number = models.FloatField(_("Factor step number"), default=0)
    image = models.ImageField(blank=True, null=True)
    actions = models.ManyToManyField(Action)

class MapLocation(models.Model):
    x = models.IntegerField(_("X"))
    y = models.IntegerField(_("Y"))
    location = models.ForeignKey(Location, null=True, blank=True,
                                 related_name='map_locations')
    world = models.ForeignKey(World)
    season = models.ForeignKey(Season, null=True, blank=True)
    layers = models.ManyToManyField('Layer')

class PathType(GenericType):
    pass

class PathFeature(models.Model):
    pass

class Path(CostFormula):
    key = models.ForeignKey(GenericItem, null=True, blank=True,
                                          verbose_name=_('Key'))
    path_type = models.ForeignKey("PathType")
    path_feature = models.ForeignKey("PathFeature", null=True, blank=True)
    start = models.ForeignKey(Location, related_name='path_start')
    arrival = models.ForeignKey(Location, related_name='path_arrival')
    forth_description = models.TextField(_('Forth description'), blank=True,
                                         null=True)
    back_description = models.TextField(_("Back description"), blank=True,
                                         null=True)
    forth_is_open = models.BooleanField(_("Forth is open"), default=True)
    back_is_open = models.BooleanField(_("Back is open"), default=True)
    is_open = models.BooleanField(_("Is open"), default=True)
    hidden_index = models.IntegerField(_("Hidden index"), default=0)

    step_erasement_factor = models.FloatField(_("Step erasement factor"),
                                              default=1)
    step_number_index = models.FloatField(_("Step number index"), default=1)
    size_index = models.IntegerField(_("Size index"), default=10)
    blocking_failure = models.BooleanField(_("Failure is blocking"),
                                           default=False)
    ability_1 = models.ForeignKey(Ability,
                                related_name='associated_path_primary')
    ability_2 = models.ForeignKey(Ability, null=True, blank=True,
                                related_name='associated_path_secondary')

class DescriptionLocation(models.Model):
    """
    A location can have many description to be discovored by characters
    """
    location = models.ForeignKey(Location, verbose_name=_("Location"))
    difficulty = models.IntegerField(_("Difficulty"), default=1)
    description = models.TextField(_("Description"))

class SpecificAction(models.Model):
    available = models.BooleanField(default=True)
    autorised = models.BooleanField(default=True)
    difficulty = models.IntegerField(default=0)
    ap_cost = models.IntegerField(_("Action point cost"), default=0)
    fp_cost = models.IntegerField(_("Feed point cost"), default=0)
    mp_cost = models.IntegerField(_("Magic point cost"), default=0)
    hp_cost = models.IntegerField(_("Hit point cost"), default=0)
    bp_cost = models.IntegerField(_("Battle point cost"), default=0)
    class Meta:
        abstract = True

class SectorType(GenericType):
    pass

class VehicleType(GenericType):
    fuel = models.ForeignKey(GenericItem, verbose_name=_("Fuel"), null=True,
                             blank=True)
    power = models.IntegerField(_("Power"), null=True, blank=True)
    compumption = models.IntegerField(_("Comsumption"), null=True, blank=True)
    max_wear = models.IntegerField(_("Max wear"), null=True, blank=True)
    wear = models.IntegerField(_("Wear"), null=True, blank=True)

class Vehicle(models.Model):
    type = models.ForeignKey(VehicleType, verbose_name=_("Type"))
    tank_capacity = models.IntegerField(_("Tank capacity"), null=True,
                                        blank=True)
    current_tank = models.IntegerField(_("Current tank content"), null=True,
                                       blank=True)
    current_wear = models.IntegerField(_("Current wear"), null=True, blank=True)
    docked = models.BooleanField(_("Docked"), default=False)
    engine_on = models.BooleanField(_("Engine on"), default=False)
    mass = models.IntegerField(_("Mass"), null=True, blank=True)
    engine_flange = models.IntegerField(_("Engine flange"), null=True,
                                        blank=True)
    wind_flange = models.IntegerField(_("Wind flange"), null=True, blank=True)
    direction = models.TextField(_("Direction"), blank=True)
    reset_date = models.DateField(_("Reset date"), auto_now_add=True)
    food_stockpile = models.IntegerField(_("Food stockpile"), null=True,
                                         blank=True)
    current_food_stockpile = models.IntegerField(_("Current food stockpile"),
                                                 null=True, blank=True)

class EnvironmentType(models.Model):
    name = models.TextField(_("Name"))
    ability = models.ForeignKey(Ability)

class Sector(models.Model):
    name = models.TextField(_("Name"), unique=True)
    description = models.TextField(null=True, blank=True)
    type = models.ForeignKey(SectorType)
    population_index = models.IntegerField(default=0)
    vehicle = models.ForeignKey(Vehicle, null=True, blank=True)
    fp_cost = models.IntegerField(_("Feed point cost"), default=0)
    hp_cost = models.IntegerField(_("Hit point cost"), default=0)
    rumor_difficulty = models.IntegerField(default=0)
    environment_type = models.ForeignKey(EnvironmentType)

class LayerType(GenericType):
    pass

class Layer(models.Model):
    name = models.TextField(_("Name"), unique=True)
    description = models.TextField(null=True, blank=True)
    population_index = models.IntegerField(default=0)
    type = models.ForeignKey(LayerType)
    color = models.CharField(_('Hexa color code'), max_length=7,
                             default='000000')
    step_erasement_factor = models.FloatField(_("Step erasement factor"),
                                              default=1)
    step_number_index = models.FloatField(_("Step number index"), default=1)
    size_index = models.IntegerField(_("Size index"), default=10)
    blocking_failure = models.BooleanField(_("Failure is blocking"),
                                           default=False)
    ability_1 = models.ForeignKey(Ability,
                                related_name='associated_layer_primary')
    ability_2 = models.ForeignKey(Ability, null=True, blank=True,
                                related_name='associated_layer_secondary')
