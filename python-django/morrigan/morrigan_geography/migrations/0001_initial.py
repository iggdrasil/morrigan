# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('morrigan_common', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DescriptionLocation',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('difficulty', models.IntegerField(verbose_name='Difficulty', default=1)),
                ('description', models.TextField(verbose_name='Description')),
            ],
        ),
        migrations.CreateModel(
            name='EnvironmentType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(verbose_name='Name')),
                ('ability', models.ForeignKey(to='morrigan_common.Ability')),
            ],
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(unique=True, verbose_name='Name')),
                ('description', models.TextField(null=True, blank=True)),
                ('population_index', models.IntegerField(default=0)),
                ('color', models.CharField(default='000000', verbose_name='Hexa color code', max_length=7)),
                ('step_erasement_factor', models.FloatField(verbose_name='Step erasement factor', default=1)),
                ('step_number_index', models.FloatField(verbose_name='Step number index', default=1)),
                ('size_index', models.IntegerField(verbose_name='Size index', default=10)),
                ('blocking_failure', models.BooleanField(verbose_name='Failure is blocking', default=False)),
                ('ability_1', models.ForeignKey(to='morrigan_common.Ability', related_name='associated_layer_primary')),
                ('ability_2', models.ForeignKey(to='morrigan_common.Ability', blank=True, null=True, related_name='associated_layer_secondary')),
            ],
        ),
        migrations.CreateModel(
            name='LayerType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('size_index', models.IntegerField(verbose_name='Size index', null=True, blank=True)),
                ('population_index', models.IntegerField(verbose_name='Population index', default=0)),
                ('factor_step_erasure', models.FloatField(verbose_name='Factor step erasure', default=1)),
                ('factor_step_number', models.FloatField(verbose_name='Factor step number', default=0)),
                ('image', models.ImageField(upload_to='', null=True, blank=True)),
                ('actions', models.ManyToManyField(to='morrigan_common.Action')),
            ],
        ),
        migrations.CreateModel(
            name='LocationType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('actions', models.ManyToManyField(to='morrigan_common.Action')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MapLocation',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('x', models.IntegerField(verbose_name='X')),
                ('y', models.IntegerField(verbose_name='Y')),
                ('layers', models.ManyToManyField(to='morrigan_geography.Layer')),
                ('location', models.ForeignKey(to='morrigan_geography.Location', blank=True, null=True, related_name='map_locations')),
            ],
        ),
        migrations.CreateModel(
            name='Path',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('ap_cost_formula', models.TextField(verbose_name='Action point cost formula', null=True, blank=True)),
                ('fp_cost_formula', models.TextField(verbose_name='Feed point cost formula', null=True, blank=True)),
                ('mp_cost_formula', models.TextField(verbose_name='Magic point cost formula', null=True, blank=True)),
                ('hp_cost_formula', models.TextField(verbose_name='Hit point cost formula', null=True, blank=True)),
                ('hp_failed_cost_formula', models.TextField(verbose_name='Hit point cost formula on failure', null=True, blank=True)),
                ('bp_cost_formula', models.TextField(verbose_name='Battle point cost formula', null=True, blank=True)),
                ('forth_description', models.TextField(verbose_name='Forth description', null=True, blank=True)),
                ('back_description', models.TextField(verbose_name='Back description', null=True, blank=True)),
                ('forth_is_open', models.BooleanField(verbose_name='Forth is open', default=True)),
                ('back_is_open', models.BooleanField(verbose_name='Back is open', default=True)),
                ('is_open', models.BooleanField(verbose_name='Is open', default=True)),
                ('hidden_index', models.IntegerField(verbose_name='Hidden index', default=0)),
                ('step_erasement_factor', models.FloatField(verbose_name='Step erasement factor', default=1)),
                ('step_number_index', models.FloatField(verbose_name='Step number index', default=1)),
                ('size_index', models.IntegerField(verbose_name='Size index', default=10)),
                ('blocking_failure', models.BooleanField(verbose_name='Failure is blocking', default=False)),
                ('ability_1', models.ForeignKey(to='morrigan_common.Ability', related_name='associated_path_primary')),
                ('ability_2', models.ForeignKey(to='morrigan_common.Ability', blank=True, null=True, related_name='associated_path_secondary')),
                ('arrival', models.ForeignKey(to='morrigan_geography.Location', related_name='path_arrival')),
                ('key', models.ForeignKey(verbose_name='Key', blank=True, to='morrigan_common.GenericItem', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PathFeature',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='PathType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('default', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField(unique=True, verbose_name='Name')),
                ('description', models.TextField(null=True, blank=True)),
                ('population_index', models.IntegerField(default=0)),
                ('fp_cost', models.IntegerField(verbose_name='Feed point cost', default=0)),
                ('hp_cost', models.IntegerField(verbose_name='Hit point cost', default=0)),
                ('rumor_difficulty', models.IntegerField(default=0)),
                ('environment_type', models.ForeignKey(to='morrigan_geography.EnvironmentType')),
            ],
        ),
        migrations.CreateModel(
            name='SectorType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('tank_capacity', models.IntegerField(verbose_name='Tank capacity', null=True, blank=True)),
                ('current_tank', models.IntegerField(verbose_name='Current tank content', null=True, blank=True)),
                ('current_wear', models.IntegerField(verbose_name='Current wear', null=True, blank=True)),
                ('docked', models.BooleanField(verbose_name='Docked', default=False)),
                ('engine_on', models.BooleanField(verbose_name='Engine on', default=False)),
                ('mass', models.IntegerField(verbose_name='Mass', null=True, blank=True)),
                ('engine_flange', models.IntegerField(verbose_name='Engine flange', null=True, blank=True)),
                ('wind_flange', models.IntegerField(verbose_name='Wind flange', null=True, blank=True)),
                ('direction', models.TextField(verbose_name='Direction', blank=True)),
                ('reset_date', models.DateField(verbose_name='Reset date', auto_now_add=True)),
                ('food_stockpile', models.IntegerField(verbose_name='Food stockpile', null=True, blank=True)),
                ('current_food_stockpile', models.IntegerField(verbose_name='Current food stockpile', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='VehicleType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
                ('power', models.IntegerField(verbose_name='Power', null=True, blank=True)),
                ('compumption', models.IntegerField(verbose_name='Comsumption', null=True, blank=True)),
                ('max_wear', models.IntegerField(verbose_name='Max wear', null=True, blank=True)),
                ('wear', models.IntegerField(verbose_name='Wear', null=True, blank=True)),
                ('fuel', models.ForeignKey(verbose_name='Fuel', blank=True, to='morrigan_common.GenericItem', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='World',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('level', models.IntegerField(null=True, blank=True)),
                ('image', models.ImageField(upload_to='', null=True, blank=True)),
                ('effect', models.ForeignKey(to='morrigan_common.Effect', blank=True, null=True)),
                ('father', models.ForeignKey(to='morrigan_geography.World', blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='WorldType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('order', models.IntegerField(default=10)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='vehicle',
            name='type',
            field=models.ForeignKey(verbose_name='Type', to='morrigan_geography.VehicleType'),
        ),
        migrations.AddField(
            model_name='sector',
            name='type',
            field=models.ForeignKey(to='morrigan_geography.SectorType'),
        ),
        migrations.AddField(
            model_name='sector',
            name='vehicle',
            field=models.ForeignKey(to='morrigan_geography.Vehicle', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='path',
            name='path_feature',
            field=models.ForeignKey(to='morrigan_geography.PathFeature', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='path',
            name='path_type',
            field=models.ForeignKey(to='morrigan_geography.PathType'),
        ),
        migrations.AddField(
            model_name='path',
            name='start',
            field=models.ForeignKey(to='morrigan_geography.Location', related_name='path_start'),
        ),
        migrations.AddField(
            model_name='maplocation',
            name='season',
            field=models.ForeignKey(to='morrigan_geography.Season', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maplocation',
            name='world',
            field=models.ForeignKey(to='morrigan_geography.World'),
        ),
        migrations.AddField(
            model_name='location',
            name='sector',
            field=models.ForeignKey(to='morrigan_geography.Sector', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='location',
            name='type',
            field=models.ForeignKey(to='morrigan_geography.LocationType'),
        ),
        migrations.AddField(
            model_name='layer',
            name='type',
            field=models.ForeignKey(to='morrigan_geography.LayerType'),
        ),
        migrations.AddField(
            model_name='descriptionlocation',
            name='location',
            field=models.ForeignKey(verbose_name='Location', to='morrigan_geography.Location'),
        ),
    ]
