// to be defined by the main page
var getMenuUrl;

var menuPollInterval = 60000;

var StateMenu = React.createClass({
  getInitialState: function() {
    return {data: {'current_menu':[]}};
  },
  loadMenuFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadMenuFromServer();
    setInterval(this.loadMenuFromServer, this.props.pollInterval);
  },
  render: function() {
    console.log(this.state.data);
    var menuNodes = this.state.data.current_menu.map(function (menu) {
      return (
          <li>
            <a href='{menu.url}'>{menu.label}</a>
          </li>
      );
    });
    return (
      <ul className="nav navbar-nav">
        {menuNodes}
      </ul>
    );
  }
});

React.render(
  <StateMenu url={getMenuUrl} pollInterval={menuPollInterval} />,
  document.getElementById('menu')
);
