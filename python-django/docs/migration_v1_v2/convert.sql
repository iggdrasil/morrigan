-- ./manage.py syncdb

alter TABLE joueur add column "user_id" integer UNIQUE REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED;
INSERT INTO auth_user (username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) select jou_pseudo, ' ', ' ', jou_email, 'md5$'||jou_mdp, case when (select count(m.mji_id) from mj m where m.mji_jou_id=jou_id) > 0 then True else false end, true, false, now(), now() from joueur;
UPDATE joueur set user_id=(select id from auth_user where username=jou_pseudo);

