#!/usr/bin/python
# -*- coding: utf-8 -*- 
'''
'''
import default
import re
import random

from lib.db.dbCommon import Action, Characteristic
from lib.constants import SEGMENT

def extirpFormulaItems(formula):
    tokens = re.findall('\%\((.)([0-9]*)\)(.)', formula)
    res, keys = [], []
    for token in tokens:
        if token:
            key, val = token[0], token[1]
            k = token[0] + token[1]
            label = k
            if key == 'c':
                c = Characteristic.get(int(val))
                label = c.label()
            res.append((k, label))
            keys.append(k)
    tokens = re.findall('\%\((\w*)\)(.)', formula)
    for token in tokens:
        if token and token[0] != 'segment' and token[0] not in keys:
            keys.append(token[0])
            res.append((token[0], token[0]))
    return res

def presentFormula(formula, formula_items):
    for key, lbl in formula_items:
        formula = formula.replace("%%(%s)d" % key, lbl)
    return formula

def presentXPWin(xp_win):
    if not xp_win:
        return "Nothing"
    xp = []
    res = []
    for win in xp_win.split(";"):
        nb, c_id = win.split(',')
        label = c_id
        try:
            c = Characteristic.get(int(c_id))
            label = c.label()
        except:
            pass
        if nb:
            xp.append("%s en %s" % (nb, label))
            res.append((label, nb))
    return res, ", ".join(xp)

actions = Action.select("1=1 order by act_id")
act_ids = [str(act.id) for act in actions]
while 1:
    i = None
    while i not in act_ids:
        print "Action à tester :"
        for action in actions:
            print "%d. %s" % (action.id, action.actNom)
        print "-"*72
        i = raw_input()
    action = Action.get(int(i))
    print "\n%s -- %s" % (action.actNom, action.actDesc)
    print "\tCoût PA -- %s" % action.actFormCoutPa
    print "\tCoût PN -- %s" % action.actFormCoutPn
    print "\tCoût PP -- %s" % action.actFormCoutPp
    print "\tCoût PV -- %s" % action.actFormCoutPv
    actifItems = extirpFormulaItems(action.actFormAttaque)
    print "\n\tFormule du personnage actif -- %s" % presentFormula(action.actFormAttaque, actifItems)
    passiveItems = extirpFormulaItems(action.actFormDefense)
    print "\tFormule du personnage passif -- %s" % presentFormula(action.actFormDefense, passiveItems)
    win_on_fail, label = presentXPWin(action.actFormGainMin)
    print "\n\tGain d'XP en cas d'échec -- %s" % label
    win_on_succes, label = presentXPWin(action.actFormGainMax)
    print "\tGain d'XP en cas de réussite -- %s" % label

    print "-"*72
    j = 1
    while j != '0':
        nb_roll = input("Nombre d'actions ? ")
        actifDct = {'segment':SEGMENT}
        passifDct = {'segment':SEGMENT}
        xps = {}
        if actifItems:
            print "\tPersonnage actif :"
            for key, lbl in actifItems:
                if key[0] != 'd':
                    actifDct[key] = input(lbl + " : ")
        if passiveItems:
            print "\tPersonnage passif :"
            for key, lbl in passiveItems:
                if key[0] != 'd':
                    passifDct[key] = input(lbl + " : ")

        xp_dct = actifDct
        print "\tPersonnage actif (XP) :"
        for label, nb in win_on_succes + win_on_fail:
            keys = extirpFormulaItems(nb)
            for k, lbl in keys:
                if k not in xp_dct:
                    xp_dct[k] = input(lbl + " : ")
        succes, fail, results = 0, 0, []
        d_actif = [int(key[1:]) for key, lbl in actifItems if key[0] =='d' ]
        d_passif = [int(key[1:]) for key, lbl in passiveItems if key[0] =='d' ]
        while nb_roll > 0:
            nb_roll -= 1
            res = 0
            d_keys = {}
            for d in d_actif:
                d_keys['d%d' % d] = random.randint(1, d)
            actifDct.update(d_keys)
            res = eval(action.actFormAttaque % actifDct)
            d_keys = {}
            for d in d_passif:
                d_keys['d%d' % d] = random.randint(1, d)
            passifDct.update(d_keys)
            res -= eval(action.actFormDefense % passifDct)
            xp_dct['success'] = res
            results.append(str(res))
            if res >= 0:
                succes += 1
                for label, nb in win_on_succes:
                    if label not in xps:
                        xps[label] = 0
                    xps[label] += eval(nb % xp_dct)
            else:
                fail +=1
                for label, nb in win_on_fail:
                    if label not in xps:
                        xps[label] = 0
                    xps[label] += eval(nb % xp_dct)
        print "-"*72
        print "Resultats : " + ", ".join(results)
        print "%d réussite(s), %d échec(s)" % (succes, fail)
        print "XPs gagnés : " + ", ".join(["%s : %d" % (key, xps[key]) for key in xps])
        j = raw_input()
    z = raw_input()

