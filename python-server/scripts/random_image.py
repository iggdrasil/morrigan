#!/usr/bin/python
'''
Associate random image for a type of location
'''
import default

from random import randint
from lib.db.dbGeography import lieu

clq_ids = (9, 13, 17)
img_ids = list(range(1, 11))

sql = '''SELECT lie.lie_id from lieu lie
inner join lieu_carto lic on lic.lic_lie_id=lie.lie_id
'''

for clq_id in clq_ids:
    sql += """inner join calque_lieu_carto clq_%d on clq_%d.clc_lic_id=lic.lic_id and clq_%d.clc_clq_id=%d
""" % (clq_id, clq_id, clq_id, clq_id)

lie_ids = list(lieu._connection.queryAll(sql))
for lie_id in lie_ids:
    lie_id = lie_id[0]
    sql = """update lieu set lie_img_id=%d where lie_id=%d""" % (img_ids[randint(0, len(img_ids)-1)], lie_id)
    lieu._connection.query(sql)
print "%d locations have a new image associated\n" % len(lie_ids) 
