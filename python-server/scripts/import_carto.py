#!/usr/bin/python
import sys
sys.path.append('/home/etienne/morrigan/trunk/python-server')
sys.path.append('/home/etienne/morrigan/trunk/python-server/lib')

from lib.db.dbGeography import importWorldFromFile

if len(sys.argv) == 2 and sys.argv[1] in ["-h", "help"] or len(sys.argv) < 6:
    print '''import_carto file_name line_height line_width world_id saison_id [layer_ids]
    '''
    sys.exit(2)

if len(sys.argv) > 6:
    try:
        layer_ids = [int(layer_id) for layer_id in sys.argv[6:]]
        importWorldFromFile(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), layer_ids)
    except ValueError:
        print '''line_height, line_width, world_id, saison_id and layer_ids must be integers
        '''
        sys.exit(2)
else:
    try:
        importWorldFromFile(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]))
    except ValueError:
        print '''line_height, line_width, world_id and saison_id must be integers
        '''
        sys.exit(2)

