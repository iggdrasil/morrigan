#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################
# PROJECT : Morrigan - FILE : utils.py - CREATION : 2004/09/01
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Some tools.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""

from mygettext import _
from constants import SEGMENT

from htmlentitydefs import codepoint2name
import re
import random
from db.sqlObject import conn as _connection

class MorriganError(Exception):
    """
    Error class to manage morrigan specific errors
    Attributes:
    message -- error message
    charac_id -- character id
    """

    def __init__(self, message, character_id=None):
        self.message = message
        self.character_id = character_id

    def __str__(self):
        return self.message

class MorriganActionError(Exception):
    """
    Error class to manage action specific errors
    Attributes:
    message -- error message
    items -- returned items
    """

    def __init__(self, items):
        self.msg = items[0]
        self.returnedFieldList = items[1]


def hex2RGB(hexaString):
    """
    Convert an hexadecimal string (ie: '#112233') to decimal RGB values.
    """
    try:
        if len(hexaString) == 7 :
            return int('0x'+hexaString[1:3], 16), \
                   int('0x'+hexaString[3:5], 16), int('0x'+hexaString[5:], 16)
        raise ValueError
    except ValueError:
        raise ValueError, "Bad hexadecimal value for a color : %s." % hexaString

def sqlFormat(value):
    """
    Fonction permettant d'�viter l'introduction de requete malicieuse dans
    les champs libres.
    @type value : string
    @param value : Champs � filter.
    @rtype: string
    @return: Champs filtr�.
    """
    value = value.replace("'", "\\'")
    return value.replace('"', '\\"')

def HTMLToText(html):
    """
    Conversion sommaire d'un texte HTML en un texte simple.
    @type html : string
    @param html : Texte HTML.
    @rtype: string
    @return: Texte HTML rendu en texte simple.
    """
    p = re.compile('<\/div>')
    html = p.sub('\n', html)
    p = re.compile('<br>')
    html = p.sub('\n', html)
    p = re.compile('<.*?>')
    return p.sub('', html)

def textToHTML(text):
    """
    Conversion sommaire d'un texte avec quelques bbCodes en un texte HTML.
    @type text : string
    @param text : Texte simple.
    @rtype: string
    @return: Texte rendu en HTML.
    """
    #affiner à l'occasion ...
    p = re.compile('<.*?>')
    text = p.sub('', text)
    text = '<p>' + text
    lst = text.split('\n')
    while '' in lst:
        lst.remove('')
    while '\r' in lst:
        lst.remove('\r')
    text = '</p><p>'.join(lst)
    text += '</p>'
    p = re.compile('\r')
    text, nbStartTag = p.subn('', text)
    p = re.compile('\[i\]')
    text, nbStartTag = p.subn('<i>', text)
    p = re.compile('\[\/i\]')
    text, nbEndTag = p.subn('</i>', text)

    if nbStartTag > nbEndTag:
        text = text + '</i>' * (nbStartTag - nbEndTag)

    return text

def bddToHTML(text):
    """
    Conversion sommaire d'un texte de BDD en texte HTML.
    @type text : string
    @param text : Texte sortant de BDD.
    @rtype: string
    @return: Texte rendu en HTML.
    """
    p = re.compile('&lt;')
    text = p.sub('<', text)
    p = re.compile('&gt;')
    text = p.sub('>', text)
    return text

def evalFormula(formula, characterId, extra={}):
    '''Evaluate formula.
    While parsing 3 tokens are extracted : first for the designed attribute,
    second, for the id and third for the type.
    First token used :
        - 'c' : characteristic
        - 'd' : dice, the second number is the aleas
    Extra argument can be passed with extra attribute.

    Attributes :
    formula (string) -- formula
    characterId (integer) -- character id
    extra (dict) -- extra arguments
    '''
    extra['segment'] = SEGMENT
    if 'success' not in extra:
        extra['success'] = 0
    tokens = re.findall('\%\((.)([0-9]*)\)(.)', formula)
    valuesDict = {}
    for token in tokens :
        key = token[0] + token[1]
        value = 0
        if token[0] == 'c':
            if not characterId:
                valuesDict[key] = 0
                continue
            sql = '''select pcr_score_calcule from perso_carac
where pcr_prs_id = %d and pcr_crc_id = %d''' % (characterId, int(token[1]))
            result = _connection.queryAll(sql)
            if result:
                valuesDict[key] = int(result[0][0])
            else:
                valuesDict[key] = 0
        elif token[0] == 'd':
            valuesDict[key] = random.randint(1,int(token[1]))
    valuesDict.update(extra)
    try:
        result = eval(formula % valuesDict)
    except:
        raise MorriganError, "Errors while parsing formula %s (keys: %s)" % (
                                                       formula, str(valuesDict))
    return result


def roleplayFormat(value, labels):
    '''
    Return a value to RP complient string
    '''
    label = _("Indescribable")
    for rg in labels:
        if value in rg:
            label = labels[rg]
    return label
