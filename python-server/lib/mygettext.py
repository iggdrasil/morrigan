#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gettext

gettext.bindtextdomain('morrigan', './locale/')
gettext.textdomain('morrigan')
_ = gettext.gettext

