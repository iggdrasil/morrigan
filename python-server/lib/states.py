#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : states.py - CREATION : 2005/05/06
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Different states available.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from utils import _

class State:
    '''
    Base state class. Define defaults values for a state.
    '''
    id = ''
    name = ''
    help = _('No help available for this state.')

    def __init__(self):
        pass

    def isAvailable(self, character):
        return None

class StateBase(State):
    '''
    Basic state. Always available.
    '''
    id = 'base'
    name = _("Base")
    help = _("This basic state shows nothing but the action selected.")

    def isAvailable(self, character):
        return True

class StateLocation(State):
    '''
    Location state. Available when the character is conscious.
    '''
    id = 'location'
    name = _("Location")
    help = _("This state shows a description of the environnement : the \
location and the characters visible.")

    def isAvailable(self, character):
        if character.prsPv >= 0:
            return True
        return None

class StateActionFile(State):
    '''
    Action file state. Always available.
    '''
    id = 'actionFile'
    name = _("Action File")
    help = _("This state include the action file. This is where all events \
concerning the character are refered.")

    def isAvailable(self, character):
        return True

class StateInventory(State):
    '''
    Inventory state. Available when the character is conscious.
    '''
    id = 'inventory'
    name = _("Inventory")
    help = _("This state shows all spirituals and and all physical possesion of\
 the character.")

    def isAvailable(self, character):
        if character.prsPv >= 0:
            return True
        return None

class StateCharacteristics(State):
    '''
    Characteristics state. Always available.
    '''
    id = 'characteristics'
    name = _("Characteristics")
    help = _("This state shows characteristics of the character.")

    def isAvailable(self, character):
        return True

class StateFight(State):
    '''
    Fight state. Available when a fight is declared.
    '''
    id = 'fight'
    name = _("Fight")
    help = _("This state shows the place where the fight stands.")

    def isAvailable(self, character):
        if character.prsPv < 0:
            return False
        return True

class StateMap(State):
    '''
    Map state. Available when the character is conscious.
    '''
    id = 'map'
    name = _("Map")
    help = _("This state shows a map of the world.")

    def isAvailable(self, character):
        if character.prsPv >= 0:
            return True
        return None

class StateHelp(State):
    '''
    Help state. Always available.
    '''
    id = 'help'
    name = _("Help")
    help = _("This state shows the help you are just reading.")

    def isAvailable(self, character):
        return True

class States:
    '''
    States to be displayed.
    '''
    def __init__(self, states):
        self.states = states
        self.stateDict = {}
        for state in states:
            self.stateDict[state.id] = state

    def getById(self, id):
        '''
        Get the state class by id.

        Attributes:
            - integer id : id of the state.

        Return:
            - State state : return the desired state.
        '''
        if self.stateDict.has_key(id):
            return self.stateDict[id]
        return None

    def getStates(self, session):
        '''
        Get states available for a given character.

        Attributes:
            - Session session : session of the given character

        Return :
            - list statesList : list of 2-tuples giving name and is page name
        '''
        statesList = []
        for state in self.states :
            if state.id in session.value('states', None):
                statesList.append((state.id, state.name))
        return statesList

_states = States([StateBase, StateLocation, StateActionFile, StateInventory,
StateCharacteristics, StateFight, StateHelp])
