#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : deliveries.py - CREATION : 2005/29/01
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Defining a task for deliveries.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""
from mx import DateTime

from TaskKit.Task import Task
from db.dbCharacters import personnage
#from db.dbGeography import Route, KnownedSteps
from actions.action import BaseAction
from db.dbCommon import action, CharChar, Characteristic

from lib.utils import _
import plugins.configuration as config

class Deliverie(BaseAction):
    '''
Define the deliveries'''

    _dbAction = action.getByName('remise')
    if not _dbAction:
        raise "DB_CONFIG_ERROR", "The deliverie action as not been set on the database."
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

class Deliveries(Task):
    """
    Set the different deliveries for characters.
    Steps are desappearing...
    """
    def run(self):
        characters = list(personnage.select("1=1"))
        deliverie = Deliverie()
        for character in characters:
            #TODO: remise de pn tributaire de la région
            deliverie.do(character, {}, None, {'e1':2})
            character.prsPv += 5
            if character.prsPv >= character.prsPvMax:
                character.prsPv = character.prsPvMax

        # lost of train points
        for char in CharChar.select("1=1"):
            jauge = char.pcrJaugeEntrainement
            if jauge > config.TRAINING_LOST:
                char.pcrJaugeEntrainement -= config.TRAINING_LOST
            else :
                delta = (char.pcrScoreXpMax - char.pcrScoreXp)
                charac = Characteristic.get(char.pcrCrcID)
                if delta < charac.crcDeltaMinMax :
                    if (delta + config.TRAINING_LOST) > charac.crcDeltaMinMax:
                        char.pcrScoreXp = char.pcrScoreXpMax - charac.crcDeltaMinMax
                    elif char.pcrScoreXp >= config.TRAINING_LOST:
                        char.pcrScoreXp -= config.TRAINING_LOST

        """
        # steps are desappearing...
        for route in Route.select("1=1"):
            if route.itnTracesRestantes <= route.itnIndexEffacement:
                route.destroySelf()
            else:
                route.itnTracesRestantes -= route.itnIndexEffacement
        # knowned steps are desappearing
        for ks in KnownedSteps.select("trc_date_fin < TIMESTAMP '%s'" % DateTime.now().strftime("%Y/%m/%d %H:%M:%S")):
            ks.destroySelf()
"""
