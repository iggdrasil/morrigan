#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : commonActions.py - CREATION : 2005/12/09
#
# Copyright (C) 2005-2010  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Common actions implementations.
cf. to the base class 'Action' to have the detail of the differents parameters
and methods.
Actions are evaluated with the session variable of the user.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from action import BaseAction
from actionFileManager import ActionFile
from utils import _, HTMLToText, textToHTML, sqlFormat
from db.dbCommon import action, Css, Emote, Characteristic
from db.dbCharacters import joueur, personnage, forme, race
import plugins.configuration as config
import time
import aspell
import string
import md5


class DoingNothing(BaseAction):
    '''
    Base action. Use it as an example for implementing other actions.
    '''

    _dbAction = action.getByName('doing_nothing')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

    def isAvailable(self, pers, extra={}):
        return BaseAction.isAvailable(self, pers, extra)

    def do(self, pers, fields, session, menu, extra={}):
        '''
        Simple writing on the action file.
        '''
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)

        msg = _("You're doing nothing very well. It seems that you are \
experienced at this kind of task.")
        action = _("Doing nothing")

        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

    def getForm(self, pers, fields):
        '''
        Using the default template
        '''
        formFields = {}
        formFields['validate'] = _("Do it !")
        return BaseAction.getForm(self, pers, fields, formFields)


from templates.actions import tplTalk
class Talk(BaseAction):
    '''
    Talking to each others
    '''

    _dbAction = action.getByName('talk')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTalk

    def isAvailable(self, pers, extra={}):
        '''
        Check if the talk action is available.
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if self._available and config.LANGUAGES:
            if not pers.getLanguages():
                self._available = None
                reason = _("No language available")
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        default_return = ['pers', 'type', 'verify', 'msg', 'curAction']
        if config.LANGUAGES:
            if 'language' not in fields:
                return _("You should choose a language."), default_return
            default_return += ['language']
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available:
            return result, returnedFieldList + default_return

        if not fields.has_key('msg') or not fields.has_key('type'):
            return _("Missing parameters."), default_return

        if fields.has_key('verify') and fields['verify'] == 'true':
            text = textToHTML(fields['msg'])
            #TODO: Get the language from a configuration variable
            spell = aspell.Speller('lang', 'fr')
            myText = "<h4>%s :</h4>" % _("Message outline")
            for word in text.split():
                if not word[0] == '<':
                    wordChecked = word
                    for punct in string.punctuation:
                        wordChecked = string.replace(wordChecked, punct, '', 10)
                    if wordChecked and not spell.check(wordChecked):
                        word = "<span class='erreur'>" + word + "</span>"
                myText += word + ' '
            return myText, default_return

        if fields['type'] == 'persos' and not fields.has_key('pers'):
            return _("No character selected."), default_return
        near = pers.isNear()
        targets = []
        nontargets = []
        targets_id = []

        if fields['type'] == 'all':
            targets = near
        else :
            if type(fields['pers']) == str:
                fields['pers'] = [fields['pers']]
            # verify that the character selected are already here
            for target_id in fields['pers']:
                # target : 'pers' + id
                target_id = target_id[4:]
                perso = personnage.get(target_id)
                if perso not in near:
                    #TODO: gérer le message en fonction du sexe
                    return _("%s is not here anymore.") % perso.prsNomActuel, \
                           default_return
                targets.append(perso)
            # set spectator
            for perso in near:
                if perso not in targets :
                    nontargets.append(perso)

        noms = ", ".join([target.prsNomActuel for target in targets])
        msg = textToHTML(fields['msg'])
        """action_bis = _("%(sender)s's message sent to %(addressee)s") \
% {'sender' : pers.prsNomActuel, 'addressee' : noms}"""
        action = _("Message from") + " " + pers.prsNomActuel
        action_bis = _("Message")

        if not config.LANGUAGES:
            pers.actionFile.writeAction(targets, ActionFile.TALK_ID, msg,
                                        action, action_bis, True, False, True)
        else:
            # dealing with languages...
            language = int(fields['language'])
            if language not in [lang[0] for lang in pers.getLanguages()]:
                return _("You have visibly forgot this language..."), \
                       default_return
            ps = pers.isNear(True, language)
            action += _(' in ') + Characteristic.get(language).crcNom
            intelligent_targets = []
            other_targets = []
            for targ in targets:
                if targ in ps:
                    intelligent_targets.append(targ)
                else :
                    other_targets.append(targ)
            pers.actionFile.writeAction(intelligent_targets, ActionFile.TALK_ID,
msg, action, action_bis, True, False, True, targets)
            if other_targets:
                msg2 = _("%(sender)s talking to you in an unknown language.") %\
                       {'sender' : pers.prsNomActuel}
                pers.actionFile.writeAction(other_targets, ActionFile.TALK_ID,
msg2, action, action_bis, False, False, True, targets)

        msg2 = _("You see %(sender)s talking to %(addressee)s.") % {
                             'sender' : pers.prsNomActuel, 'addressee' : noms}
        action = _("You see %s talking") % pers.prsNomActuel
        pers.actionFile.writeAction(nontargets, ActionFile.FEEDBACK_ID, msg2,
                                    action, action_bis, None)

        menu.evaluateMenu(session, pers.id)
        msg = "<h4>%s :</h4>" % _("Sent message") + msg
        return msg, None

    def getForm(self, pers, fields):
        """
        Get the form for the talk action.
        "check spelling" is checked by default. Once verified it is unchecked.
        """
        formFields = {}
        formFields['all_checked'],  formFields['indiv_checked'] = '', ''
        if fields.has_key('type') and fields['type'] == 'all':
            formFields['all_checked'] = " checked='checked'"
        else:
            formFields['indiv_checked'] = " checked='checked'"
        formFields['talking_to_label'] = _('Talking to')
        formFields['talking_to_all_label'] = _("Talking to all")

        persos = []
        for perso in pers.isNear():
            checked = ''
            if fields.has_key('pers') and str(perso.id) in fields['pers']:
                checked = "checked='checked'"
            persos.append(self._tpl.characterItem % {'id':'pers'+str(perso.id),
                                 'check':checked, 'label':perso.prsNomActuel})
        formFields['characters'] = ' ,'.join(persos)
        formFields['language'] = ''

        if config.LANGUAGES:
            langFields = {}
            langFields['lang_label'] = _("Language")
            languageItems = "<select name='language'>"
            selected_id = 0
            if fields.has_key('language'):
                selected_id = int(fields['language'])
            for language in pers.getLanguages():
                selected = ''
                if language[0] == selected_id:
                    selected = " selected='selected'"
                languageItems += "<option value='%d'%s>%s</option>" % (
                        language[0], selected, language[1])
            languageItems += "</select>"
            langFields['languageItems'] = languageItems
            formFields['language'] = self._tpl.languageForm  % langFields
        formFields['message'] = ''
        formFields['verify_check'] = ""
        if fields.has_key('msg'):
            formFields['message'] = fields['msg']
        formFields['checkSpell_label'] = _("Check spelling")
        formFields['validate'] = _("Send")
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplTakingNotes
class TakingNotes(BaseAction):
    '''
    Taking personnal notes
    '''

    _dbAction = action.getByName('taking_notes')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTakingNotes

    def isAvailable(self, pers, extra={}):
        '''
        Check if the taking notes action is available.
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        default_return = ['msg', 'curAction']
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available:
            return result, returnedFieldList + default_return

        if not fields.has_key('msg'):
            return _("Missing parameters."), default_return

        msg = textToHTML(fields['msg'])
        action = _("Personnal notes")

        pers.actionFile.writeAction([], ActionFile.TALK_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        msg = "<h4>%s :</h4>" % _("Taking personnal notes") + msg
        return msg, None

from templates.actions import tplChangeCharacterData
class ChangeCharacterData(BaseAction):
    '''
    Action to modify personal datas including : password, email, css, time-zone,
    character, description and character image.
    '''

    _name = _("Change Character Data")
    _id = 'changeCharacterData'
    _dbAction = None
    _tpl = tplChangeCharacterData

    def isAvailable(self, pers, extra={}):
        # This action is always available.
        return True, ''

    def do(self, pers, fields, session, menu, extra={}):
        jou = joueur.get(pers.prsJouID)

        pers.getCurrentShape().frmDescActuelle = textToHTML(fields['desc'])
        pers.getCurrentShape().frmUrlImage = fields['img']
        jou.jouEmail = fields['mail']
        try:
            jou.jouFuseau = int(fields['tz'])
        except ValueError:
            pass

        jou.jouCssID = int(fields['css'])
        css = Css.get(jou.jouCssID)
        session.setValue('stylesheet', (css.cssChemin, css.cssNom))

        msg = 'Information updated.'

        if fields['newPass'] != '':
            oldPass = md5.new(fields['oldPass']).hexdigest()
            if len(fields['newPass']) < 4 :
                msg = _('Password must have at least 4 characters.')
            elif oldPass == jou.jouMdp \
                 and fields['newPass'] == fields['confirmPass']:
                jou.jouMdp = md5.new(fields['newPass']).hexdigest()
            elif fields['oldPass'] != '' \
                 and fields['newPass'] == fields['confirmPass'] :
                msg = _('The password confirmation has failed.')
            elif fields['oldPass'] != '':
                msg = _('Bad old password.')

        menu.evaluateMenu(session, pers.id)
        return msg, None

    def getForm(self, pers, fields):
        forme = pers.getCurrentShape()
        if not forme.frmUrlImage:
            img = ''
        else :
            img = forme.frmUrlImage
        jou = joueur.get(pers.prsJouID)
        csss = Css.select('1=1')
        css_choice  = ""
        current_css = jou.jouCssID
        for css in csss:
            selected = ""
            if css.id == current_css:
                selected = " selected='selected'"
            css_choice += "<option value='%s'%s>%s</option>" % (css.id,
                                selected, css.cssNom)
        formFields = {'name':self._name,
        'desc':HTMLToText(forme.frmDescActuelle), 'img':img,
        'mail':jou.jouEmail, 'id':self._id,
        'changeDescription':_("Change the character description"),
        'imagePath': _("URL to an image"), 'email':_("Email"),
        'oldPass':_("Old password"), 'newPass':_('New password'),
        'confirm':_('New password confirmation'), 'update':_("Update"),
        'css':_("Theme"), 'css_choice': css_choice, 'tz':_("Time variation"),
        'ctz':jou.jouFuseau, 'hours':_("hours")}
        formFields['validate'] = _("Change")
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplChangeShape
class ChangeShape(BaseAction):
    """
    Changing shape
    """

    _dbAction = action.getByName('change_shape')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplChangeShape

    def isAvailable(self, pers, extra={}):
        """
        Available when another shape is available.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        query = "frm_prs_id = %i" % pers.id
        if len(list(forme.select(query))) > 1:
            return True,''
        else:
            return False, _("No other shape available")

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList + ['forme', 'curAction']

        query = "frm_prs_id = %i" % pers.id
        res = list(forme.select(query))
        myforme = None
        otherformes = []
        for formes in res:
            if int(fields['forme']) == formes.id:
                myforme = formes
            else:
                otherformes.append(formes)
        if not myforme :
            return _("It seems that you're trying to cheat."), None

        if succes < BaseAction.SUCCES:
            msg = _("You're trying to change your shape but you fail.")
            #if pers.sexe == 'male':
            msgAutres = _("You see %(male)s trying to change his shape but, he \
                          fails.") % {'male':pers.prsNomActuel}
        else :
            msg = "%s %s" % (_("You change yourself into a"),
                             race.get(myforme.frmRacID).racNom)
            msgAutres = _("You see %(name)s change into a %(form)s") \
                        % {'name':pers.prsNomActuel,
                           'form':race.get(myforme.frmRacID).racNom}
            myforme.frmActuelle = True
            for formes in otherformes:
                formes.sync()

        action = _("Shape changing")
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        pers.actionFile.writeAction(pers.isNear(), ActionFile.FEEDBACK_ID,
                                    msgAutres, action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, None

    def getForm(self, pers, fields):
        """
        Display available shapes.
        """
        available_shapes = ''
        query = "frm_prs_id = %i" % pers.id
        result = list(forme.select(query))
        for formes in result:
            if formes.frmActuelle != True :
                available_shapes += """<option value='%i'>%s</option>""" % (
                                 formes.id, race.get(formes.frmRacID).racNom)
        formFields = {'available_shapes':available_shapes,
                      'validate':_("Change")}
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplChangeCharacter
class ChangeCharacter(BaseAction):
    """
    Change the current character when several availables.
    Opportunity is given to control multiple creatures.
    """

    _dbAction = action.getByName('change_character')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplChangeCharacter

    def isAvailable(self, pers, extra={}):
        """
        Available if another character is available
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        query = "prs_jou_id=%i" % pers.prsJouID
        result = list(personnage.select(query))
        if len(result) > 1 :
            return True,''
        return False, 'No other characters available'

    def do(self, pers, fields, session, menu, extra={}):
        """
        Character change.
        """
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['perso', 'curAction']
        query = "prs_jou_id=%i" % pers.prsJouID
        result = list(personnage.select(query))
        persoIds = []
        for perso in result:
            persoIds.append(str(perso.id))
        if fields['perso'] not in persoIds:
            return _('This character is not yours.'), None
        session.setValue('prs_id', fields['perso'])
        menu.evaluateMenu(session, fields['perso'])
        return None, None

    def getForm(self, pers, fields):
        """
        Liste de choix de personnage possibles.
        """

        query = "prs_jou_id=%i and prs_id!=%i" % (pers.prsJouID, pers.id)
        result = list(personnage.select(query))
        available_characters = ""
        for perso in result:
            available_characters += """<option value='%i'>%s</option>""" % (
                                                perso.id, perso.prsNomActuel)
        formFields = {'available_characters':available_characters,
                      'validate':_("Change")}
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplEmotes
class Emotes(BaseAction):
    '''
    Emote.
    This action is usefull to communicate between characters with differents
    languages.
    '''

    _dbAction = action.getByName('emote')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplEmotes

    def isAvailable(self, pers, extra={}):
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, pers, fields):
        persos = []
        for perso in pers.isNear():
            value = ''
            if fields.has_key('pers') and str(perso.id) in fields['pers']:
                value = "checked='checked'"
            persos.append(self._tpl.characterCheck % {'id':'pers'+str(perso.id),
                                     'check':value, 'label':perso.prsNomActuel})
        characters_check = ' ,'.join(persos)

        simple_emotes = Emote.getSimpleEmotes()
        emotes = []
        for emt in simple_emotes:
            emotes.append("<option value='%d'>%s</option>" % (emt.id,
                                                              emt.mimNom))
        simple_emotes = "".join(emotes)

        character_emotes = Emote.getCharacterEmotes()
        emotes = []
        for emt in character_emotes:
            emotes.append("<option value='%d'>%s</option>" % (emt.id,
                                                              emt.mimNom))
        character_emotes = "".join(emotes)
        available_characters = []
        for character in pers.isNear():
            available_characters.append("<option value='%d'>%s</option>" % (
                                        character.id, character.prsNomActuel))
        characters = "".join(available_characters)

        formFields = {'showing_to_label':_('Showing to'),
                      'characters_check':characters_check,
                      'all_show_label':_("Showing to all"),
                      'simple_label':_("Simple"),
                      'simple_emotes':simple_emotes,
                      'character_label':_("Character"),
                      'character_emotes':character_emotes,
                      'target_label':_("target") ,
                      'characters':characters,
                      'validate':_("Do")}
        return BaseAction.getForm(self, pers, fields, formFields)


    def do(self, pers, fields, session, menu, extra={}):
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['curAction']

        if not fields.has_key('emote_type') or not fields.has_key('type'):
            return _("Missing parameters."), ['pers', 'type', 'emote_type',
                                              'curAction']

        if fields['type'] == 'persos' and not fields.has_key('pers'):
            return _("No character selected."), ['pers', 'type', 'emote_type',
                                                 'curAction']
        near = pers.isNear()
        targets = []
        nontargets = []
        targets_id = []

        if fields['type'] == 'all':
            targets = near
        else :
            if type(fields['pers']) == type("st"):
                fields['pers']=[fields['pers']]
            for target_id in fields['pers']:
                target_id = target_id[4:]
                perso = personnage.get(target_id)
                if perso not in near:
                    #TODO: gérer le message en fonction du sexe
                    return _("%s is not here anymore.") % perso.prsNomActuel, \
                           ['type', 'verify', 'msg', 'curAction']
                targets.append(perso)
            for perso in near:
                if perso not in targets :
                    nontargets.append(perso)

        noms = ", ".join([target.prsNomActuel for target in targets])

        msg = ""
        if fields['emote_type'] == 'simple':
            emt = Emote.get(int(fields['simple_emote']))
            msg = emt.mimLabel % {'subject' : pers.prsNomActuel}
        elif fields['emote_type'] == 'character':
            emt = Emote.get(int(fields['character_emote']))
            msg = emt.mimLabel % {'subject' : pers.prsNomActuel,
                'dest' : personnage.get(int(fields['character'])).prsNomActuel}
        """action_bis = _("%(sender)s's message sent to %(addressee)s") \
% {'sender' : pers.prsNomActuel, 'addressee' : noms}"""
        action = _("Emote from") + " " + pers.prsNomActuel
        action_bis = _("Emote")
        pers.actionFile.writeAction(targets, ActionFile.TALK_ID, msg, action, \
action_bis, True, False, True)

        msg2 = _("You see %(sender)s representing something to %(addressee)s.")\
               % {'sender' : pers.prsNomActuel, 'addressee' : noms}
        action = _("You see %s representing something") % pers.prsNomActuel
        pers.actionFile.writeAction(nontargets, ActionFile.FEEDBACK_ID, msg2,
                                    action, action_bis, None)
        menu.evaluateMenu(session, pers.id)
        msg = "<h4>%s :</h4>" % _("Emote") + msg
        return msg, None
