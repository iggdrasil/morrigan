#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : fightActions.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Fight actions implementations.
cf. to the base class 'Action' to have the detail of the differents parameters
and methods.
Actions are evaluated with the session variable of the user.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from random import randint

from utils import _, roleplayFormat
from constants import TAKEPART_MAX, BDD_CONSTANTS, VIOLENCE_LABELS, \
                      DAMAGED_LABELS

from db.dbCommon import action
from db.dbInventory import Weapon
from db.dbFight import TakePart
from db.dbCharacters import Character
from action import BaseAction
from actionFileManager import ActionFile

from templates.actions import tplTakePart
class TakingPart(BaseAction):
    '''Take part in a fight
'''

    _dbAction = action.getByName('take_part')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTakePart

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        and you are not engaged in a fight
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        tps = list(TakePart.select('eng_prs1_prs_id=%d or eng_prs2_prs_id=%d' % (
                                                             pers.id, pers.id)))
        if tps:
            self._available = None
            reason = _("You are already taking part in a fight")
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'Character': _("Character"), 'costFP':True,
                      'costAP':True, 'Fight_type': _("Fight type"),
                      'Melee':_('Melee'), 'Distant':_('Distant')}
        characters = ''
        for character in pers.isNear():
            characters += "<option value='%d'>%s</option>" % (character.id,
                                                        character.prsNomActuel)
        formFields['characters'] = characters
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        if 'character' not in fields or 'distant_fight' not in fields:
            return _("Missing parameters."), ['character', 'distant_fight']
        # verify if the selected character is always here
        near = pers.isNear()
        if not int(fields['character']) in [character.id for character in near]:
            return _("The selected character is not here anymore."), \
                   ['character', 'distant_fight']
        opponent_id = int(fields['character'])
        opponent = Character.get(opponent_id)
        if len(pers.getTakeParts()) >= TAKEPART_MAX:
            return _("You are already taking part in too many fight."), \
                   ['character', 'distant_fight']
        if len(opponent.getTakeParts()) >= TAKEPART_MAX:
            return _("Your opponent is already taking part in too many fight.")\
                    , ['character', 'distant_fight']

        distant_fight = False
        if fields['distant_fight'] == '1':
            distant_fight = True
            # when a distant fight is selected verify if a distant weapon is
            # equipped
            weapons = pers.getEquipedWeapons(None)
            distant_wp = False
            for item in weapons:
                for weapon in item.weapons:
                    if weapon.distant():
                        distant_wp = True
            if not distant_wp:
                return _("You can't take part in a distant fight if no distant \
weapon is equiped."), ['character', 'distant_fight']

        extra['e1'] = not distant_fight
        extra['opponent_id'] = opponent_id
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['e_' + key] = pers.getBaseCharacteristic(key)
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['opp_e_' + key] = opponent.getBaseCharacteristic(key)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['character', 'distant_fight']
        msg, msg2others = "", ""
        if succes < BaseAction.SUCCES:
            if distant_fight:
                msg = _("You try to take part in a distant fight with %s. But\
 you fail.") % opponent.label()
                msg2others = _("%s try to take part in a distant fight with \
%s. But he fail.") % (pers.label(), opponent.label())
            else:
                msg = _("You try to take part in a melee fight with %s. But\
 you fail.") % opponent.label()
                msg2others = _("%s try to take part in a melee fight with \
%s. But he fail.") % (pers.label(), opponent.label())
        else:
            TakePart(engPrs1PrsID=pers.id, engPrs2PrsID=opponent.id,
                engInit1=True, engLieID=pers.prsLieID, engDistant=distant_fight)
            if distant_fight:
                msg = _("You are taking part in a distant fight with %s. \
You've got the initiative.") % opponent.label()
                msg2others = _("%s is taking part in a distant fight with \
%s.") % (pers.label(), opponent.label())
            else:
                msg = _("You are taking part in a melee fight with %s. \
You've got the initiative.") % opponent.label()
                msg2others = _("%s is taking part in a melee fight with \
%s.") % (pers.label(), opponent.label())

        action = _("Taking part in a fight")
        pers.actionFile.writeAction([], ActionFile.FIGHT_ID, msg, action)
        pers.actionFile.writeAction(pers.isNear(), ActionFile.FIGHT_ID,
                                    msg2others, action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

from templates.actions import tplLeaveFight
class LeaveFight(BaseAction):
    '''Leaving a fight
'''

    _dbAction = action.getByName('leave_fight')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplLeaveFight

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point, health points and
        you are taking part to a fight
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        tps = list(TakePart.select('eng_prs1_prs_id=%d or eng_prs2_prs_id=%d' %(
                                                             pers.id, pers.id)))
        if not tps:
            self._available = None
            reason = _("You are not taking part in a fight")
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'Distant_fight': _("Distant fight with"), 'costFP':True,
            'costAP':True, 'Method':_("Method"), 'Force':_("Force"),
            'Melee_fight': _("Melee fight with"), 'Dexterity':_("Dexterity"),
             'costFiP':True}
        distant_fight = []
        melee_fight = []
        for takepart in pers.getTakeParts():
            opponent = takepart.engPrs1Prs
            if takepart.engPrs1PrsID == pers.id:
                opponent = takepart.engPrs2Prs
            option = "<option value='%d'>%s</option>" % (takepart.id,
                                                        opponent.label())
            if takepart.engDistant:
                distant_fight.append(option)
            else:
                melee_fight.append(option)
        formFields['distant_fights'] = '\n'.join(distant_fight)
        formFields['melee_fights'] = '\n'.join(melee_fight)
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        if 'melee' not in fields or fields['melee'] not in ('0', '1') or\
           (fields['melee'] == '0' and 'distant_fight' not in fields) or\
           (fields['melee'] == '1' and ('melee_fight' not in fields or\
                                        'force' not in fields)):
            return _("Missing parameters."), ['melee_fight', 'distant_fight',
                                              'force']
        melee = True
        fight_key = 'melee_fight'
        if fields['melee'] == '0':
            melee = None
            fight_key = 'distant_fight'
        takeparts = pers.getTakeParts()
        current_takepart = None
        for takepart in takeparts:
            if int(fields[fight_key]) == takepart.id:
                current_takepart = takepart
        takepart = current_takepart
        if not takepart:
            return _("You are not engaged in this fight anymore."), \
                   ['melee_fight', 'distant_fight', 'force']

        opponent = takepart.engPrs1Prs
        if opponent.id == pers.id:
            opponent = takepart.engPrs2Prs
        extra['opponent_id'] = opponent.id
        # e1 : True if force is used by the character
        # e2 : True if force is used by the opponent
        if melee:
            if fields['force'] == '1':
                extra['e1'] = True
            else:
                extra['e1'] = False
            extra['e2'] = True
        else:
            extra['e1'] = False
            extra['e2'] = False
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['e_' + key] = pers.getBaseCharacteristic(key)
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['opp_e_' + key] = opponent.getBaseCharacteristic(key)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList + ['melee_fight', 'distant_fight',
                                                'force']
        msg, msg2others = "", ""
        if succes < BaseAction.SUCCES:
            if not melee:
                msg = _("You try to leave a distant fight with %s. But\
 you fail.") % opponent.label()
                msg2others = _("%s try to leave a distant fight with \
%s. But he fail.") % (pers.label(), opponent.label())
            else:
                msg = _("You try to leave a melee fight with %s. But\
 you fail.") % opponent.label()
                msg2others = _("%s try to leave a melee fight with \
%s. But he fail.") % (pers.label(), opponent.label())
        else:
            if not melee:
                msg = _("You leave a distant fight with %s.") % opponent.label()
                msg2others = _("%s leave a distant fight with %s.") % \
                             (pers.label(), opponent.label())
            else:
                msg = _("You leave a melee fight with %s.") % opponent.label()
                msg2others = _("%s leave a melee fight with %s.") % \
                             (pers.label(), opponent.label())
            takepart.destroySelf()
        action = _("Leaving a fight")
        pers.actionFile.writeAction([], ActionFile.FIGHT_ID, msg, action)
        pers.actionFile.writeAction(pers.isNear(), ActionFile.FIGHT_ID,
                                    msg2others, action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList


from templates.actions import tplTakeInit
class TakeInit(BaseAction):
    '''Taking the initiative
    '''

    _dbAction = action.getByName('take_init')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTakeInit

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point, health points and
        you are taking part in a fight
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        tps = list(TakePart.select('eng_prs1_prs_id=%d or eng_prs2_prs_id=%d' %(
                                                             pers.id, pers.id)))
        if not tps:
            self._available = None
            reason = _("You are not taking part in a fight")
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'Fight': _("On fight with"), 'costFP':True,
                      'costAP':True, 'costFiP':True}
        fights = []
        for takepart in pers.getTakeParts():
            opponent = takepart.engPrs1Prs
            if takepart.engPrs1PrsID == pers.id:
                opponent = takepart.engPrs2Prs
            fights.append("<option value='%d'>%s</option>" % (takepart.id,
                                                        opponent.label()))
        formFields['fights'] = '\n'.join(fights)
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        if 'fight' not in fields:
            return _("Missing parameters."), ['fight']
        takeparts = pers.getTakeParts()
        current_takepart = None
        for takepart in takeparts:
            if int(fields['fight']) == takepart.id:
                current_takepart = takepart
        takepart = current_takepart
        if not takepart:
            return _("You are not engaged in this fight anymore."), ['fight']
        opponent = takepart.engPrs1Prs
        init_character = not takepart.engInit1
        if opponent.id == pers.id:
            opponent = takepart.engPrs2Prs
            init_character = takepart.engInit1
        if init_character:
            return _("You already have the initiative on this fight."), \
                    ['fight']
        # TODO
        extra['e1'] = 1
        extra['e2'] = 1
        extra['e3'] = 1
        extra['e4'] = 1
        extra['opponent_id'] = opponent.id
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['e_' + key] = pers.getBaseCharacteristic(key)
        for key in ('PC', 'PC_max', 'PF', 'PF_max'):
            extra['opp_e_' + key] = opponent.getBaseCharacteristic(key)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available:
            return succes, returnedFieldList + ['fight']
        msg, msg2others = "", ""
        if succes < BaseAction.SUCCES:
            msg = _("You try to take the initiative to %s. But\
 you fail.") % opponent.label()
            msg2others = _("%s try to take the initiative to \
%s. But he fail.") % (pers.label(), opponent.label())
        else:
            msg = _("You take the initiative to  %s.") % opponent.label()
            msg2others = _("%s take the initiative to %s.") % \
                             (pers.label(), opponent.label())
            takepart.engInit1 = not takepart.engInit1
        action = _("Taking the initiative")
        pers.actionFile.writeAction([], ActionFile.FIGHT_ID, msg, action)
        pers.actionFile.writeAction(pers.isNear(), ActionFile.FIGHT_ID,
                                    msg2others, action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

from templates.actions import tplAttack
class Attack(BaseAction):
    '''Attack someone
    '''

    _dbAction = action.getByName('attack')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplAttack

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point, health points and
        you are taking part in a fight
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        tps = list(TakePart.select('eng_prs1_prs_id=%d or eng_prs2_prs_id=%d' %(
                                                             pers.id, pers.id)))
        if not tps:
            self._available = None
            reason = _("You are not taking part in a fight")
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'Fight': _("Opponent"), 'with':_("with"), 'costFP':True,
                      'costAP':True, 'costFiP':True}
        fights = []
        distants = set()
        for takepart in pers.getTakeParts():
            opponent = takepart.engPrs1Prs
            if takepart.engPrs1PrsID == pers.id:
                opponent = takepart.engPrs2Prs
            distants.add(takepart.engDistant)
            fights.append("<option value='%d'>%s</option>" % (takepart.id,
                                                        opponent.label()))
        formFields['opponents'] = '\n'.join(fights)
        weapons = pers.getEquipedWeapons(None)
        weapons_fields = []
        natural_weapons_fields = []
        for item in weapons:
            for weapon in item.weapons:
                if weapon.distant() not in distants:
                    continue
                option = "<option value='%d'>%s</option>" % (weapon.id,
                                                             weapon.label())
                if item.elmNaturel:
                    natural_weapons_fields.append(option)
                else:
                    weapons_fields.append(option)
        # if there is non natural weapon, natural weapons are not available
        if weapons_fields:
            formFields['weapons'] = "\n".join(weapons_fields)
        else:
            formFields['weapons'] = "\n".join(natural_weapons_fields)
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        if 'opponent' not in fields or 'weapon' not in fields:
            return _("Missing parameters."), ['fight']
        takeparts = pers.getTakeParts()
        current_takepart = None
        for takepart in takeparts:
            if int(fields['opponent']) == takepart.id:
                current_takepart = takepart
        takepart = current_takepart
        if not takepart:
            return _("You are not engaged in this fight anymore."), ['fight']
        opponent = takepart.engPrs1Prs
        init_character = not takepart.engInit1
        if opponent.id == pers.id:
            opponent = takepart.engPrs2Prs
            init_character = takepart.engInit1
        if not init_character:
            return _("You don't have the initiative on this fight."), \
                    ['opponent', 'weapon']
        weapons = []
        natural_weapons = []
        for item in pers.getEquipedWeapons():
            if item.elmNaturel:
                for weapon in item.weapons:
                    natural_weapons.append(weapon)
            else:
                for weapon in item.weapons:
                    weapons.append(weapon)
        weapon = Weapon.get(int(fields['weapon']))
        if weapon != '0' and weapon not in weapons + natural_weapons:
            return _("You don't have this weapon anymore."), \
                    ['opponent', 'weapon']
        if weapon != '0' and weapon.distant() != takepart.engDistant:
            msg = ''
            if takepart.engDistant:
                msg = _("This weapon is not appropriate for distant fight")
            else:
                msg = _("This weapon is not appropriate for close fight")
            return msg, ['opponent', 'weapon']
        if weapon.armElm.elmNaturel and weapons:
            return _("You can't use this natural weapon because you have a \
non-natural weapon equiped."), ['opponent', 'weapon']
        # manage weapon with ammunitions
        if weapon.armAgn.agnTmtID:
            if not weapon.armRch or weapon.armRch.rchNbMnt < 1:
                return _("Your weapon is unloaded. You can't use it."), \
                   ['opponent', 'weapon']
            # decrease by one ammunitions
            weapon.armRch.rchNbMnt -= 1
        # e1 : weapon characteristic score
        # e2 : weapon compet score
        # e3 : weapon bonus
        # e4 : opponent weapon characteristic
        # e5 : opponent weapon compet
        # e6 : opponent armor bonus
        # e7 : opponent number of opponents
        # e8 : distant fight
        # e9 : opponent is awake
        # e10 : opponent weapon attack bonus
        # fpC : fight point cost
        # wCo : weapon compet id
        value = pers.getValue(weapon.armAgn.agnCarac1CrcID)
        if weapon.armAgn.agnCarac2CrcID:
            value = (value + pers.getValue(weapon.armAgn.agnCarac2CrcID))/2
        extra['e1'] = value
        value = pers.getValue(weapon.armAgn.agnCompet1CrcID)
        extra['wCo'] = weapon.armAgn.agnCompet1CrcID
        if weapon.armAgn.agnCompet2CrcID:
            value = (value + pers.getValue(weapon.armAgn.agnCompet2CrcID))/2
        extra['e2'] = value
        extra['e3'] = weapon.armAgn.agnBonusAttaque

        opponent_wp = None
        weapons = opponent.getEquipedWeapons()
        for wp in weapons:
            if not opponent_wp:
                opponent_wp = wp
            if opponent_wp.elmNaturel and not wp.elmNaturel:
                opponent_wp = wp
        extra['e10'] = 0
        if opponent_wp:
            opponent_wp = opponent_wp.weapons[0]
            extra['e10'] = opponent_wp.armAgn.agnBonusDefense
        value= 0
        if opponent_wp:
            value = opponent.getValue(opponent_wp.armAgn.agnCarac1CrcID)
            if opponent_wp.armAgn.agnCarac2CrcID:
                value = (value + \
                         opponent.getValue(opponent_wp.armAgn.agnCarac2CrcID))/2
        extra['e4'] = value
        value = 0
        if opponent_wp:
            value = opponent.getValue(opponent_wp.armAgn.agnCompet1CrcID)
            if opponent_wp.armAgn.agnCompet2CrcID:
                value = (value + \
                          opponent_wp.getValue(weapon.armAgn.agnCompet2CrcID))/2
        extra['e5'] = value
        armors = opponent.getEquipedArmors()
        extra['e6'] = 0
        for item in armors:
            extra['e6'] += item.base_armor.ugnBonusDefense
        extra['e7'] = len(opponent.getTakeParts())
        extra['e8'] = 0
        if takepart.engDistant:
            extra['e8'] = 1
        extra['e9'] = 0
        if opponent.isAwake():
            extra['e9'] = 1
        extra['fpC'] = int(self._dbAction.actFormCoutPc % extra)
        extra['opponent_id'] = opponent.id
        base = 5
        extra['e1'] -= base
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available:
            return succes, returnedFieldList + ['fight']
        msg, msg2others = "", ""
        if score < 0:
            msg = _("You try to attack %s. But you fail.") % opponent.label()
            msg2opponent = _("%s tries to attack you. But he fails.") % \
                                                                    pers.label()
            msg2others = _("%s tries to attack %s. But he fails.") % \
                                                (pers.label(), opponent.label())
        else:
            dices = (score + base)/base
            wounds = 0
            for i in xrange(dices):
                wounds += randint(weapon.armAgn.agnDegatMin,
                                 weapon.armAgn.agnDegatMax)
            real_wounds = opponent.hit(wounds, weapon.armAgn.agnTdgID,
                                       extra_msg=True)
            critical = None
            if dices > (opponent.getValue(BDD_CONSTANTS.CONSTITUTION_ID) - 5):
                critical = True
                critical_wounds = randint(weapon.armAgn.agnDegatMin/2,
                                         weapon.armAgn.agnDegatMax/2)
                real_wounds += opponent.hit(critical_wounds, extra_msg=True)
                wounds += critical_wounds
            msg = _("You attack %s %s. ") % (opponent.label(),
                                        roleplayFormat(wounds, VIOLENCE_LABELS))
            msg2opponent = _("%s attack you %s. ") % (pers.label(),
                                        roleplayFormat(wounds, VIOLENCE_LABELS))
            if critical:
                critical = _("It is a critical attack. ")
                msg += critical
                msg2opponent += critical
            print "wounds : %d" % wounds
            print "real_wounds : %d" % real_wounds
            damage = roleplayFormat(real_wounds, DAMAGED_LABELS).lower()
            msg += _("You hurt %s %s.") % (opponent.label(), damage)
            msg2opponent += _("%s hurts you %s.") % (pers.label(), damage)
            while opponent.current_messages[0]:
                msg2opponent += " " + opponent.current_messages[0].pop()
            msg2others = _("%s attacks %s.") % \
                             (pers.label(), opponent.label())
            while opponent.current_messages[1]:
                c_msg = opponent.current_messages[1].pop()
                msg += " " + c_msg
                msg2others += " " + c_msg
        takepart.engInit1 = not takepart.engInit1
        action = _("Attack")
        pers.actionFile.writeAction([], ActionFile.FIGHT_ID, msg, action)
        pers.actionFile.writeAction([opponent], ActionFile.FIGHT_ID,
                                    msg2opponent, action, " ", None)
        others = pers.isNear()
        if opponent in others:
            others.remove(opponent)
        pers.actionFile.writeAction(others, ActionFile.FIGHT_ID,
                                    msg2others, action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList
