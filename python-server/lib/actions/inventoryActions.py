#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Inventory actions implementations.
cf. to the base class 'Action' to have the detail of the differents parameters
and methods.
Actions are evaluated with the session variable of the user.
'''

from sqlobject.main import SQLObjectNotFound

from lib.utils import _

from db.dbCommon import action
from db.dbInventory import Item, Weapon, Loader, Chest, Shop, BuyItems, Money,\
                           VisibleItem
from db.dbCharacters import personnage, Character
from db.dbGeography import lieu
from action import BaseAction
from actionFileManager import ActionFile


from templates.actions import tplGiveObject
class GiveObject(BaseAction):
    '''
    Give an object to another character.
    '''

    _dbAction = action.getByName('give_object')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplGiveObject

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['character', 'object']

        if not fields.has_key('character') or not fields.has_key('object'):
            return _("Missing parameters."), returnedFieldList + ['character',
                                                                  'object']

        # verify if the selected character is always here
        near = pers.isNear()
        if not int(fields['character']) in [character.id for character in near]:
            return _("The selected character is not here anymore."), \
                   returnedFieldList + ['character', 'object']

        # verify if the selected object is yours
        item = Item.get(int(fields['object']))
        if item.elmPrsID != pers.id:
            return _("You don't have the object anymore."), \
                   returnedFieldList + ['character', 'object']

        # verify if the item is not equiped
        if item.elmEquipe == True and (not item.loader \
                                               or not item.loader.rchUtilise):
            return _("You can't give an item equiped. Unequip the item first.")\
                   , returnedFieldList + ['character', 'object']

        # gift of the object
        dest = Character.get(int(fields['character']))
        succes = dest.takeItem(item)
        if not succes:
            msg = _("You can't give this item.") + " " + dest.reason
            return msg, returnedFieldList
        #FIXME: gérer le sexe du personnage
        msg = _("You're giving %(object)s to %(character)s. It's the best day \
of her life !")
        msg = _("You're giving %(object)s to %(character)s. It's the best day \
of his life !") % {'object':item.name, 'character':dest.prsNomActuel}
        action = _("Gift of an object")
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)

        msg2 = _("%(character)s is giving %(object)s to you. It's the best day \
of your life !") % {'object':item.name, 'character':pers.prsNomActuel}
        dest = personnage.get(int(fields['character']))
        dest.actionFile.writeAction([], ActionFile.ACTION_ID, msg2, action)

        nontargets = []
        for character in near:
            if character.id != dest.id:
                nontargets.append(character)
        #FIXME: voir en fonction de l'attention, de l'encombrement de l'objet...
        msg2 = _("%(character)s is giving something to %(dest)s.") % \
                {'dest':dest.prsNomActuel, 'character':pers.prsNomActuel}

        pers.actionFile.writeAction(nontargets, ActionFile.FEEDBACK_ID, msg2,
                                    action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

    def getForm(self, pers, fields):
        formFields = {'object_label': _("Object"), 'to_label':_("To"),
                      'validate': _("Give"), 'costFP':True}
        objects = ''
        inventory = pers.getInventory()
        #FIXME: pour l'instant on peut donner toute possession non équipée....
        for key in inventory.keys():
            for item in inventory[key]:
                if item.elmEquipe != True and (not item.loader \
                                               or not item.loader.rchUtilise):
                    objects += "<option value='%d'>%s</option>" % (item.id,
                                                                   item.name)
        formFields['objects'] = objects
        characters = ''
        for character in pers.isNear():
            characters += "<option value='%d'>%s</option>" % (character.id,
                                                        character.prsNomActuel)
        formFields['characters'] = characters
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplDrop
class Drop(BaseAction):
    '''
    Drop an object.
    '''

    _dbAction = action.getByName('drop_object')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplDrop

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'validate':_("Drop"),'object_label':_("Object"),
                      'costFP':True}
        objects = ""
        inventory = pers.getInventory()
        #FIXME: pour l'instant on peut faire tomber toute possession ....
        for key in inventory.keys():
            for item in inventory[key]:
                if not item.elmEquipe and (not item.loader \
                                           or not item.loader.rchUtilise):
                    objects += "<option value='%d'>%s</option>" % (item.id,
                                                               item.name)
        formFields['objects'] = objects
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        if not fields.has_key('object'):
            return _("Missing parameters."), []
        # verify if the selected object is yours
        item = Item.get(int(fields['object']))
        if item.elmPrsID != pers.id:
            return _("You don't have the object anymore."), ['object']

        # verify if the item is not equiped
        if item.elmEquipe or (item.loader and item.loader.rchUtilise):
            return _("You can't drop an item equiped. Unequip the item first.")\
                   , ['object']

        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        item.elmPrsID = None
        item.elmLieID = pers.prsLieID
        for weapon in item.weapons:
            # if the weapon is loaded, the loaders are left with the weapon
            if weapon.armRch:
                weapon.armRch.rchElm.elmPrsID = None
                weapon.armRch.rchElm.elmLieID = pers.prsLieID
        VisibleItem(elvPrsID=pers.id, elvElmID=item.id)
        location = lieu.get(pers.prsLieID)
        action = _("Drop")
        msg = _("You drop a %s in %s.")  % (item.name, location.label())
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        msg = _("You see %s drop %s in %s.")  % (pers.label(), item.name,
                                                 location.label())
        near = pers.isNear()
        pers.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                    action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

from templates.actions import tplEquip
class Equip(BaseAction):
    '''
    Equip or unequip an armor or a weapon
    '''

    _dbAction = action.getByName('equip')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplEquip

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, pers, fields):
        options_form_equip, options_form_unequip = "", ""
        inventory = pers.getInventory()
        for key in inventory.keys():
            for item in inventory[key]:
                equipable = item.elmEgn.egnEquipable or item.elmEquipable
                if equipable and not item.elmEquipe :
                    options_form_equip += "<option value='%d'>%s</option>" % (
                                                           item.id, item.name)
                elif equipable:
                    options_form_unequip += "<option value='%d'>%s</option>" % (
                                                            item.id, item.name)
        form = ''
        if options_form_equip:
            formFields = {'equip_label':_("Equip an item"),
                      'object_label':_("Object"),
                      'objects':options_form_equip,
                      'equip_id':1, 'validate':_("Equip"),
                      'costAP':True, 'costFP':True}
            form += BaseAction.getForm(self, pers, fields, formFields)
        if options_form_unequip:
            title = not options_form_equip
            formFields = {'equip_label':_("Unequip an item"),
                      'object_label':_("Object"),
                      'objects':options_form_unequip,
                      'equip_id':0, 'validate':_("Unequip"),
                      'costAP':True, 'costFP':True}
            form += BaseAction.getForm(self, pers, fields, formFields, {},
                                       title)
        if not form:
            form = "<h1>%s</h1><div class='actionForm'>%s</div>" % (self._name,
                                          _("No items to equip or to unequip."))

        return form

    def do(self, pers, fields, session, menu, extra={}):
        if not fields.has_key('object') and not fields.has_key('equip'):
            return _("Missing parameters."),  ['object']

        # verify if the selected object is yours
        item = Item.get(int(fields['object']))
        if item.elmPrsID != pers.id:
            return _("You don't have the object anymore."),  ['object']

        if fields['equip'] == '1':
            unequiped_items, hands_equipped = pers.equip(int(fields['object']))
            if unequiped_items != [] or hands_equipped != []:
                msg = ""
                if unequiped_items:
                    msg += _("Before equiping this item, you have to unequip \
all these items :")
                    msg += "<ul>"
                    for item in (unequiped_items):
                        msg += "<li>" + item.name + "</li>"
                    msg += "</ul>"
                if hands_equipped:
                    if not msg:
                        msg += _("Before equiping this item, you have to \
unequip one of this items :")
                    else:
                        msg += _("And one of this items :")
                    msg += "<ul>"
                    for item in (hands_equipped):
                        msg += "<li>" + item.name + "</li>"
                    msg += "</ul>"
                return msg, ['object']

        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        equip = True
        if fields['equip'] == '0':
            equip = None
        item.equip(equip)
        msg, action = '', ''
        if equip:
            action = _("Equip")
            msg = _("You equip %s.")  % item.name
        else:
            action = _("Unequip")
            msg = _("You unequip %s.")  % item.name
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        if equip:
            msg = _("You see %s equipping %s.")  % (pers.label(), item.name)
        else:
            msg = _("You see %s unequipping %s.")  % (pers.label(), item.name)
        near = pers.isNear()
        pers.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                    action, " ", None)
        return "", ""

from templates.actions import tplReload
class Reload(BaseAction):
    '''
    Reload a weapon
    '''

    _dbAction = action.getByName('reload')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplReload

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        if 'object' not in fields:
            return _("Missing parameters."), ['object', 'loader']
        if 'object' in fields and not 'loader' in fields:
            return _("Choose a loader"), ['object']
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['object', 'loader']
        try:
            weapon = Weapon.get(int(fields['object']))
            loader = Loader.get(int(fields['loader']))
        except (ValueError, SQLObjectNotFound):
            return _("Missing parameters."), \
                   returnedFieldList + ['object', 'loader']
        if weapon.armElm.elmPrsID != pers.id or loader.rchElm.elmPrsID != pers.id:
            return _("Object selected is not available anymore."), \
                   returnedFieldList + ['object', 'loader']
        if weapon.armRch:
            return _("The selected weapon is already loaded. Unload it first."), \
                   returnedFieldList + ['object', 'loader']
        if loader.rchUtilise:
            return _("The selected loader is already loaded. Unload it first."), \
                   returnedFieldList + ['object', 'loader']
        weapon.armRch = loader
        loader.rchUtilise = True
        msg = _("You're loading %s with %s. Have fun!") % (weapon.label(),
                                                           loader.label())
        action = _("Loading of a weapon")
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)

        nontargets = []
        for character in pers.isNear():
            nontargets.append(character)
        #FIXME: voir en fonction de l'attention, de l'encombrement de l'objet...
        msg2 = _("%(character)s is loading the weapon : %(weapon)s.") % \
                {'weapon':weapon.label(), 'character':pers.prsNomActuel}
        pers.actionFile.writeAction(nontargets, ActionFile.FEEDBACK_ID, msg2,
                                    action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

    def getForm(self, pers, fields):
        if 'object' not in fields:
            formFields = {'object_label': _("Object to reload")}
            weapons = Weapon.getByOwner(pers.id)
            objects = ""
            for weapon in weapons:
                if weapon.armAgn.agnTmtID and not weapon.armRch:
                    objects += "<option value='%d'>%s</option>" % (weapon.id,
                                                                 weapon.label())
            formFields['objects'] = objects
            return BaseAction.getForm(self, pers, fields, formFields,
                                  self._tpl.getFormObject)
        try:
            weapon = Weapon.get(int(fields['object']))
        except (ValueError, SQLObjectNotFound):
            return ''

        formFields = {'object': weapon.label(), 'object_id':weapon.id,
                      'loader':_("Loader")}
        load = ""
        loaders = Loader.getByOwner(pers.id)
        for loader in loaders:
            type = loader.getAmmoType()
            # good type and not already loaded
            if type and weapon.armAgn.agnTmtID == type.id \
               and not loader.rchUtilise:
                load += "<option value='%d'>%s</option>" % (loader.id,
                                                            loader.label())
        formFields['loaders'] = load
        return BaseAction.getForm(self, pers, fields, formFields,
                                  self._tpl.getFormLoaders)

from templates.actions import tplUnload
class Unload(BaseAction):
    '''
    Unload a weapon
    '''

    _dbAction = action.getByName('unload')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplUnload

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        if 'object' not in fields:
            return _("Missing parameters."), ['object']
        result, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return result, returnedFieldList + ['object', 'loader']
        try:
            weapon = Weapon.get(int(fields['object']))
        except (ValueError, SQLObjectNotFound):
            return _("Missing parameters."), \
                   returnedFieldList + ['object']
        if weapon.armElm.elmPrsID != pers.id:
            return _("Object selected is not available anymore."), \
                   returnedFieldList + ['object', 'loader']
        if not weapon.armRch:
            return _("The selected weapon is not loaded."), \
                   returnedFieldList + ['object']
        weapon.armRch.rchUtilise = None
        weapon.armRchID = None
        msg = _("You unload %s.") % (weapon.label())
        action = _("Unloading of a weapon")
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)

        nontargets = []
        for character in pers.isNear():
            nontargets.append(character)
        #FIXME: voir en fonction de l'attention, de l'encombrement de l'objet...
        msg2 = _("%(character)s is unloading the weapon : %(weapon)s.") % \
                {'weapon':weapon.label(), 'character':pers.prsNomActuel}
        pers.actionFile.writeAction(nontargets, ActionFile.FEEDBACK_ID, msg2,
                                    action, " ", None)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

    def getForm(self, pers, fields):
        formFields = {'object_label': _("Object to unload")}
        weapons = Weapon.getByOwner(pers.id)
        objects = ""
        for weapon in weapons:
            if weapon.armAgn.agnTmtID and weapon.armRch:
                objects += "<option value='%d'>%s</option>" % (weapon.id,
                                                               weapon.label())
        formFields['objects'] = objects
        return BaseAction.getForm(self, pers, fields, formFields)

from templates.actions import tplTakeObject
class TakeObject(BaseAction):
    '''
    Get an object
    '''
    _dbAction = action.getByName('takeObject')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTakeObject

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        formFields = {'object_label': _("Object to get")}

        objects_available = character.prsLie.getObjects(character.id)
        objects = ""
        for object in objects_available:
            objects += "<option value='%d'>%s</option>" % (object.id,
                                                           object.label())
        formFields['objects'] = objects
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        #TODO: à peaufiner ......
        if not self._available :
            return succes, returnedFieldList
        action = _("Take an object")
        objects_available = character.prsLie.getObjects(character.id)
        try:
            item = Item.get(int(fields['object']))
        except ValueError, SQLObjectNotFound:
            return _("Unknow error"), returnedFieldList
        if item not in objects_available:
            return _("The object is not here anymore"), returnedFieldList
        msg = _("You take %s.")  % item.name
        succes = character.takeItem(item)
        if not succes:
            msg = _("You can't take this item.") + " " + character.reason
            return msg, returnedFieldList
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg,
                                         action)
        msg = _("You see %s taking %s.")  % (character.label(), item.name)
        near = character.isNear()
        character.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                         action, " ", None)
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList

from templates.actions import tplPutInto
class PutInto(BaseAction):
    '''
    Put an object in a chest
    '''
    _dbAction = action.getByName('putInto')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplPutInto

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        formFields = {'put_label': _("Put:"), 'into_label': _("Into:")}

        objects_available = character.getInventory(sorted=False,
                                                   sized=True)
        objects_available.sort(lambda x, y:cmp(x.label(), y.label()))
        objects = ""
        for object in objects_available:
            objects += "<option value='%d'>%s</option>" % (object.id,
                                                           object.label())
        formFields['objects'] = objects
        available_chests = character.getChest()
        chests = ''
        for chest in available_chests:
            chests += "<option value='%d'>%s</option>" % (chest.id,
                                                          chest.label())
        formFields['chests'] = chests
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        action = _("Put into")
        objects_available = character.getInventory(sorted=False,
                                                   sized=True)
        try:
            item = Item.get(int(fields['object']))
            chest = Chest.get(int(fields['chest']))
        except ValueError, SQLObjectNotFound:
            return _("Unknow error"), returnedFieldList
        if item not in objects_available:
            return _("The object is not here anymore"), returnedFieldList
        available_chests = character.getChest()
        if chest not in available_chests:
            return _("The chest is not here anymore"), returnedFieldList
        key = chest.open(character)
        if not key:
            return _("The chest is closed and you don't have the key to open \
it"), returnedFieldList
        succes = chest.putIn(item)
        if not succes:
            chest.close(character)
            msg = _("You can't put this item into this chest.")
            return msg, returnedFieldList
        msg = ''
        if type(key) != bool:
            msg += _("You unlock %s with %s. ") % (chest.cffElm.label(),
                                                   key.label())
        msg += _("You put %s into %s.")  % (item.label(), chest.cffElm.label())
        # the chest is closed if it has be opened
        if type(key) != bool:
            key = chest.close(character)
            if type(key) != bool:
                msg += _(" You lock %s. ") % chest.cffElm.label()
            if not key:
                msg += _(" You can't lock %s anymore. ") % chest.cffElm.label()
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg,
                                         action)
        msg = _("You see %s put %s into %s.")  % (character.label(),
                                             item.label(), chest.cffElm.label())
        near = character.isNear()
        character.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                         action, " ", None)
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList

from templates.actions import tplTakeInto
class TakeInto(BaseAction):
    '''
    Get an object in a chest
    '''
    _dbAction = action.getByName('takeInto')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplTakeInto

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        chest = None
        if 'chest' in fields:
            try:
                chest = Chest.get(int(fields['chest']))
            except ValueError, SQLObjectNotFound:
                pass
        if not chest:
            formFields = {'take_label': _("Take into:")}
            available_chests = character.getChest()
            chests = ''
            for chest in available_chests:
                chests += "<option value='%d'>%s</option>" % (chest.id,
                                                           chest.label())
            formFields['chests'] = chests
            return BaseAction.getForm(self, character, fields, formFields)
        formFields = {'chest_id':chest.id,
                'take_label': _("Into %s take:") % chest.cffElm.label()}
        items = ''
        for item in chest.getItems():
            items += "<option value='%d'>%s</option>" % (item.id,
                                                         item.label())
        formFields['items'] = items
        return BaseAction.getForm(self, character, fields, formFields,
                                  self._tpl.getFormItem)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'chest' not in fields:
            return _("Missing parameters"), returnedFieldList
        available_chests = character.getChest()
        try:
            chest = Chest.get(int(fields['chest']))
            assert chest in available_chests
        except:
            return _("Chest not available anymore"), returnedFieldList
        if not chest.isOpen(character):
            return _("%s is not open and you don't have the key") % (
                       chest.cffElm.label()), returnedFieldList
        if 'item' not in fields:
            return "", {'chest':chest.id}
        available_items = chest.getItems()
        try:
            item = Item.get(int(fields['item']))
            assert item in available_items
        except ValueError, SQLObjectNotFound:
            return _("This item is not available anymore"), \
                   returnedFieldList
        action = _("Take into")
        msg = _("You take %s into %s.")  % (item.name, chest.cffElm.label())
        succes = character.takeItem(item)
        if not succes:
            msg = _("You can't take this item.") + " " + character.reason
            return msg, returnedFieldList
        chest.takeOut(item)
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg,
                                         action)
        msg = _("You see %s take %s into %s.")  % (character.label(),
                                        item.label(), chest.cffElm.label())
        near = character.isNear()
        character.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                         action, " ", None)
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList

from templates.actions import tplOpenCloseChest
class OpenCloseChest(BaseAction):
    '''
    Get an object in a chest
    '''
    _dbAction = action.getByName('openCloseChest')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplOpenCloseChest

    def isAvailable(self, pers, extra={}):
        '''
        This action is available when you have health points
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        dct = {}
        available_chests = character.getChest()
        opened_chests = ''
        opened_chests_lst = [chest for chest in available_chests
                                   if chest.cffClefEgn and not chest.cffOuvert]
        if opened_chests_lst:
            opened_chests += "<option value=''>&nbsp;</option>"
        for chest in opened_chests_lst:
            opened_chests += "<option value='%d'>%s</option>" % (chest.id,
                                                           chest.label())
        dct['opened_chests'] = opened_chests
        closed_chests = ''
        closed_chests_lst = [chest for chest in available_chests
                                   if chest.cffClefEgn and chest.cffOuvert]
        if closed_chests_lst:
            closed_chests += "<option value=''>&nbsp;</option>"
        for chest in closed_chests_lst:
            closed_chests += "<option value='%d'>%s</option>" % (chest.id,
                                                           chest.label())
        dct['closed_chests'] = closed_chests
        formFields = {'msg':'', 'open':'', 'close':''}
        if not closed_chests and not opened_chests:
            formFields['msg'] = self._tpl.nothing % \
                                               _("Nothing to close or to open.")
        if closed_chests:
            dct_chest_frm = {'action':'close', 'label':_("Close:")}
            dct_chest_frm['chests'] = dct['closed_chests']
            formFields['close'] = self._tpl.getFormChest % dct_chest_frm
        if opened_chests:
            dct_chest_frm = {'action':'open', 'label':_("Open:")}
            dct_chest_frm['chests'] = dct['opened_chests']
            formFields['open'] = self._tpl.getFormChest % dct_chest_frm
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'open' not in fields and 'close' not in fields:
            return _("Missing parameters"), returnedFieldList
        chests = character.getChest()
        action = _("Open/close a chest")
        msg = ''
        if 'open' in fields:
            try:
                chest = Chest.get(int(fields['open']))
                assert chest in chests
            except:
                return _("Chest not available anymore"), returnedFieldList
            key = chest.open(character)
            if not key:
                msg += _("You can't open %s. Have you got the key? ") % (
                                                           chest.cffElm.label())
            else:
                if type(key) == bool:
                    msg += _("%s is already open. ") % chest.cffElm.label()
                else:
                    msg += _("You open %s with %s. ") % (chest.cffElm.label(),
                                                        key.label())
        if 'close' in fields:
            try:
                chest = Chest.get(int(fields['close']))
                assert chest in chests
            except:
                return _("Chest not available anymore"), returnedFieldList
            key = chest.close(character)
            if not key:
                msg += _("You can't close %s. Have you got the key? ") % (
                                                           chest.cffElm.label())
            else:
                if type(key) == bool:
                    msg += _("%s is already closed.") % chest.cffElm.label()
                else:
                    msg += _("You close %s with %s.") % (chest.cffElm.label(),
                                                        key.label())
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg,
                                         action)
        msg = _("You see %s lock (or unlock) %s.")  % (character.label(),
                                                       chest.cffElm.label())
        near = character.isNear()
        character.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                    action, " ", None)
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList

from templates.actions import tplOpenCloseShop
class OpenCloseShop(BaseAction):
    '''
    Open/close a shop
    '''
    _dbAction = action.getByName('openCloseShop')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplOpenCloseShop

    def isAvailable(self, pers, extra={}):
        '''
        This action is available when you have health points
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        dct = {}
        available_shops = character.getShops(own=True)
        available_chests = character.getChest()
        for shop in available_shops:
            if shop.btqCff in available_chests:
                available_chests.remove(shop.btqCff)
        shops = ''
        if available_shops:
            shops += "<option value=''>&nbsp;</option>"
        for shop in available_shops:
            shops += "<option value='%d'>%s</option>" % (shop.id,
                                                         shop.label())
        dct['shops'] = shops
        chests = ''
        if available_chests:
            chests += "<option value=''>&nbsp;</option>"
        for chest in available_chests:
            chests += "<option value='%d'>%s</option>" % (chest.id,
                                                          chest.label())
        dct['chests'] = chests
        formFields = {'msg':'', 'open':'', 'close':''}
        if not chests and not shops:
            formFields['msg'] = self._tpl.nothing % \
                                               _("No shop to open or close.")
        if shops:
            dct_chest_frm = {'extra':'', 'action':'close',
                             'label':_("Close the shop:")}
            dct_chest_frm['chests'] = dct['shops']
            formFields['close'] = self._tpl.getFormChest % dct_chest_frm
        if chests:
            dct_chest_frm = {'action':'open', 'label':_("Open a shop using the \
chest:")}
            dct_chest_frm['extra'] = self._tpl.name % _("Name:")
            dct_chest_frm['extra'] += self._tpl.desc % _("Description:")
            dct_chest_frm['chests'] = dct['chests']
            formFields['open'] = self._tpl.getFormChest % dct_chest_frm
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'open' not in fields and 'close' not in fields:
            return _("Missing parameters"), returnedFieldList
        shops = character.getShops(own=True)
        chests = character.getChest()
        action = _("Open/close a shop")
        msg = ''
        if 'open' in fields and fields['open']:
            try:
                chest = Chest.get(int(fields['open']))
                assert chest in chests
            except:
                return _("Chest not available anymore"), returnedFieldList
            key = chest.open(character)
            if not key:
                msg += _("You can't use %s has a shop: you don't have the key \
top open it. ") % chest.cffElm.label()
            else:
                toclose = False
                if type(key) != bool:
                    toclose = True
                name, desc = "", ""
                if 'name' in fields and fields['name']:
                    name = fields['name']
                if 'desc' in fields and fields['desc']:
                    desc = fields['desc']
                Shop(btqCffID=chest.id, btqNom=name, btqDescription=desc,
btqPrsID=character.id, btqAtmID=None)
                if toclose:
                    chest.close(character)
                if name:
                    name = ": " + name
                msg += _("You have open the new shop%s. ") % name
        if 'close' in fields and fields['close']:
            try:
                shop = Shop.get(int(fields['close']))
                assert shop in shops
            except:
                return _("Shop not available anymore"), returnedFieldList
            bis = BuyItems.byShop(shop.id)
            for bi in bis:
                sbi.destroySelf()
            name = shop.label()
            shop.destroySelf()
            msg += _("You have close the shop: %s. ") % name
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg,
                                         action)
        msg = ''
        if 'open' in fields and fields['open']:
            msg = _("%s has opened the shop %s.")  % (character.label(), name)
        if 'close' in fields and fields['close']:
            msg = _("%s has closed the shop %s.")  % (character.label(), name)
        if msg:
            near = character.isNear()
            character.actionFile.writeAction(near, ActionFile.FEEDBACK_ID, msg,
                                             action, " ", None)
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList

from templates.actions import tplManageShop
class ManageShop(BaseAction):
    '''
    Manage a shop
    '''
    _dbAction = action.getByName('manageShop')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplManageShop

    def isAvailable(self, pers, extra={}):
        '''
        This action is available when you have health points
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        shop = None
        available_shops = character.getShops(own=True)
        if 'shop' in fields:
            try:
                shop = Shop.get(int(fields['shop']))
                assert shop in available_shops
            except (ValueError, SQLObjectNotFound, AssertionError):
                shop = None
        if not shop:
            dct = {}
            shops = ''
            if available_shops:
                shops += "<option value=''>&nbsp;</option>"
            for shop in available_shops:
                shops += "<option value='%d'>%s</option>" % (shop.id,
                                                         shop.label())
            formFields = {'msg':'', 'shops':shops, 'label':''}
            if not shops:
                formFields['msg'] = self._tpl.nothing % _("No shop to manage.")
            else:
                formFields['label'] = _("Shop to manage:")
            return BaseAction.getForm(self, character, fields, formFields,
                    self._tpl.getShop)
        currency_str = ""
        selected_currency_dct = {}
        for money in Money.select("1=1 order by mnn_indice"):
            selected_currency_dct['selected_%d' % money.id] = ''
            currency_str += "<option value='%d'%%(selected_%d)s>%s</option>" % (
                                              money.id, money.id, money.label())
        chest_items = shop.btqCff.getItems(chest_item=True)
        items_str = ""
        if not chest_items:
            items_str = "<tr><td colspan='2'>%s</td></tr>" % _("No items")
        for chest_item in chest_items:
            price = ''
            dct = selected_currency_dct
            if chest_item.celPrix:
                price = chest_item.celPrix
                dct['selected_%d' % chest_item.celMnnID] = " selected='selected'"
            currency = currency_str % dct
            items_str += self._tpl.itemLine % {'currency':currency,
'name':chest_item.celElm.label(), 'price':price, 'id':chest_item.id}
        current_buy_items = BuyItems.byShop(shop.id)
        c_buy_items_str = ''
        c_buy_items_lst = []
        for buy_item in current_buy_items:
            c_buy_items_lst.append(buy_item.aegEgn)
            number = ''
            if buy_item.aegNombreMax:
                number = str(buy_item.aegNombreMax)
            dct = selected_currency_dct
            if buy_item.aegMnnId:
                dct['selected_%d' % buy_item.aegMnnID] = " selected='selected'"
            currency = currency_str % dct
            c_buy_items_str += self._tpl.buyItemLine % {'id':buy_item.id,
'name':buy_item.aegEgn.label(), 'price':buy_item.aegPrix, 'number':number,
'currency':currency}
        buy_items = [item for item in character.getKnownItems()
                          if item not in c_buy_items_lst]
        new_buy_item_str = "<option value=''>&nbsp;</option>\n"
        for buy_item in buy_items:
            new_buy_item_str += "<option value='%d'>%s</option>\n" % (
                                                  buy_item.id, buy_item.label())
        new_buy_item_str = self._tpl.newBuyItemLine % {'currency':currency_str,
                                                       'items':new_buy_item_str}
        price_label = _("Price")
        formFields = {'chest_item_label':_("To sell"), 'name_label':_("Name"),
'price_label':price_label, 'number_label':_("Max number"), 'shop_id':shop.id,
'buy_item_label':_("To buy"), 'chest_items':items_str, 'shop_name':shop.label(),
'buy_items':c_buy_items_str, 'shop_desc':shop.btqDescription,
'new_buy_items':new_buy_item_str}
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if not 'shop' in fields or not fields['shop']:
            return "", returnedFieldList
        returnedDct = {}
        returnedDct['shop'] = fields['shop']
        try:
            shop = Shop.get(int(fields['shop']))
        except (ValueError, SQLObjectNotFound):
            return _("Database error"), returnedFieldList
        available_shops = character.getShops(own=True)
        if shop not in available_shops:
            return _("Shop not available anymore"), returnedFieldList
        chest_items = shop.btqCff.getItems(chest_item=True)
        updated = None
        for chest_item in chest_items:
            key = 'sell_price_%d' % chest_item.id
            key_currency = 'currency_%d' % chest_item.id
            if key in fields and key_currency in fields:
                if fields[key] and fields[key_currency]:
                    try:
                        chest_item.celPrix = int(fields[key])
                        chest_item.celMnnID = int(fields[key_currency])
                        updated = True
                    except ValueError:
                        chest_item.celPrix = None
                        chest_item.celMnnID = None
                else:
                    chest_item.celPrix = None
                    chest_item.celMnnID = None
        msg = ""
        if updated:
            msg = _("Shop prices and items updated")
        return msg, returnedDct

from templates.actions import tplBuy
class Buy(BaseAction):
    '''
    Buy
    '''
    _dbAction = action.getByName('buy')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplBuy

    def isAvailable(self, pers, extra={}):
        '''
        This action is available when you have health points
        '''
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, character, fields):
        dct = {}
        available_shops = character.getShops()
        if not available_shops:
            formFields = {'msg':_("No shops."), 'shop_desc':''}
            return BaseAction.getForm(self, character, fields, formFields,
                                      self._tpl.nothing)
        shop = None
        if 'shop' in fields:
            try:
                shop = Shop.get(int(fields['shop']))
                assert shop in available_shops
            except (ValueError, SQLObjectNotFound, AssertionError):
                pass
        if not shop:
            formFields = {'label':_("Choose the shop:")}
            shops = "<option value=''>&nbsp;</option>"
            for shop in available_shops:
                shops += "<option value='%d'>%s</option>" % (shop.id,
                                                         shop.label())
            formFields['shops'] = shops
            return BaseAction.getForm(self, character, fields, formFields,
                                      self._tpl.getShop)

        chest_items = shop.getItems(chest_item=True)
        items_str = ""
        if not chest_items:
            formFields = {'msg':_("No items to sell in this shop."),
                          'shop_desc':shop.btqDescription}
            return BaseAction.getForm(self, character, fields, formFields,
                                      self._tpl.nothing)
        for chest_item in chest_items:
            item = chest_item.celElm
            price = str(character.evalMoney(chest_item.celPrix))
            items_str += self._tpl.item % {'id':item.id, 'name':item.label(),
                                         'desc':item.description, 'price':price}
        formFields = {'shop_name':shop.label(), 'items':items_str,
'item_lbl':_("Item"), 'desc_lbl':_("Description"), 'shop_id':shop.id,
'price_lbl':_("Price"), 'shop_desc':shop.btqDescription}
        return BaseAction.getForm(self, character, fields, formFields)

    def do(self, character, fields, session, menu, extra={}):
        if 'shop' not in fields:
            return _("Missing parameters"), {}
        available_shops = character.getShops()
        action = _("Buy")
        msg = ''
        if 'shop' in fields and fields['shop']:
            try:
                shop = Shop.get(int(fields['shop']))
                assert shop in available_shops
            except (ValueError, SQLObjectNotFound, AssertionError):
                return _("Shop not available anymore"), []
        returnedDct = {'shop':shop.id}
        if 'item' not in fields or not fields['item']:
            return '', returnedDct
        try:
            item_id = int(fields['item'])
        except ValueError:
            return '', returnedDct
        chest_items = shop.getItems(chest_item=True)
        chest_item = None
        for ci in chest_items:
            if int(fields['item']) == ci.celElmID:
                chest_item = ci
        if not chest_item:
            return _("Item not available anymore"), ['shop']
        price = None
        if chest_item.celPrix:
            price = chest_item.celPrix
        if not price or character.getBaseMoney() < price*2:
            return _("Not enough money to negociate this item"), ['shop']
        succes, score, returnedFieldList = BaseAction.do(self, character,
                                                         fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if succes < 0:
            price = int(price * (100.0 - succes/2)/100)
        if succes > 0:
            price = int(price * (100.0 + succes*4)/100)
        base_money = character.getBaseMoney()
        if base_money < price:
            return _("Not enough money to buy this item"), returnedFieldList
        seller = Character.get(shop.btqPrsID)
        character.delMoney(price)
        seller.addMoney(price)
        item = Item.get(chest_item.celElmID)
        item.elmPrsID = character.id
        chest_item.destroySelf()
        price_buyer = str(character.evalMoney(price))
        msg = _("You bought %s for %s." % (item.label(), price_buyer))
        character.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        price_seller = str(seller.evalMoney(price))
        msg2 = _("%s bought you %s for %s." % (character.label(), 
                                              item.label(), price_seller))
        seller.actionFile.writeAction([], ActionFile.FEEDBACK_ID, msg2,
                                      _("Sell"))
        menu.evaluateMenu(session, character.id)
        return msg, returnedFieldList
