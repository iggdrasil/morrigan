#!/usr/bin/env python
# -*- coding: utf-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : adminActions.py - CREATION : 2008/05/12
#
# Copyright (C) 2008 Étienne Loks <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Admin actions implementations.
cf. to the base class 'Action' to have the detail of the differents parameters
and methods.
Actions are evaluated with the session variable of the user.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

import plugins.configuration as config
from utils import _
from action import BaseAction

from db.dbCommon import action, Characteristic
from db.dbGeography import secteur, type_lieu, lieu, lieu_carto, type_chemin,\
        monde, particularite_chemin, chemin, description_lieu

from templates.actions import tplCreateLocation
class CreateLocation(BaseAction):
    """
    Create a new topologic location
    """

    _dbAction = action.getByName('create_location')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateLocation

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        try:
            lieu(lieTliID=int(fields['location_type']), lieNom=fields['name'], lieSctID=int(fields['sector']),
lieFacteurEffacementTrace=int(fields['steps_erasement']), lieIndiceTaille=int(fields['size']),
lieFacteurNbTrace=int(fields['steps_number']), lieCarto=False,
lieDesc=fields['desc'], lieIndicePopulation=int(fields['population']),
lieImgID=None)
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("New location created"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new location
        """
        db_sectors = secteur.select("1=1 order by sct_nom")
        sectors = ""
        for sector in db_sectors:
            sectors += "<option value='%d'>%s</option>\n" % (sector.id, sector.label())
        db_location_types = type_lieu.select("1=1 order by tli_id")
        location_types = ""
        for location_type in db_location_types:
            location_types += "<option value='%d'>%s</option>\n" % (location_type.id,
                                                                    location_type.label())
        formFields = {'Name':_("Name"), 'Description':_("Description"),
'Location_type':_("Location type"), 'location_types':location_types, 'Sector':_("Sector"),
'sector':sectors, 'Size':_("Size"), 'Factor_steps_erasement':_('Factor steps erasement'),
'Factor_steps_number':_('Factor steps number'),  'validate':_("Create"),
'current_name':'', 'current_desc':'', 'current_size':90, 'location_id':0,
'current_steps_erasement':100, 'current_steps_number':100,
'Population':_("Population index"), 'current_population':0}
        return BaseAction.getForm(self, pers, fields, formFields)

class ModifyLocation(BaseAction):
    """
    Modify a topologic location
    """

    _dbAction = action.getByName('modify_location')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateLocation

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available:
            return succes, returnedFieldList
        if 'delete_location' in fields:
            try:
                teleport_location = int(fields['teleport_location'])
                characters = pers.isNear(True, None, None, True)
                characters.append(pers)
                for character in characters:
                    character.prsLieID = teleport_location
                location = lieu.get(fields['location_id'])
                items = location.getObjects()
                for item in items:
                    item.elmLieID = teleport_location
                location.destroySelf()
            except:
                return _("Database error"), returnedFieldList
            menu.evaluateMenu(session, pers.id)
            return _("Current location deleted"), None

        try:
            location = lieu.get(fields['location_id'])
            location.lieTliID = int(fields['location_type'])
            location.lieNom = fields['name']
            location.lieSctID = int(fields['sector'])
            location.lieFacteurEffacementTrace = int(fields['steps_erasement'])
            location.lieIndiceTaille = int(fields['size'])
            location.lieIndicePopulation = int(fields['population'])
            location.lieFacteurNbTrace = int(fields['steps_number'])
            location.lieDesc = fields['desc']
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("Current location modified"), None

    def getForm(self, pers, fields):
        """
        Display a form to modify a location
        """
        location = lieu.get(pers.prsLieID)
        if location.lieCarto:
            return _("Current location cannot be modified")
        db_sectors = secteur.select("1=1 order by sct_nom")
        sectors = ""
        for sector in db_sectors:
            selected = ''
            if location.lieSctID == sector.id:
                selected = " selected='selected'"
            sectors += "<option value='%d'%s>%s</option>\n" % (sector.id,
                                                       selected, sector.label())
        db_location_types = type_lieu.select("1=1 order by tli_id")
        location_types = ""
        for location_type in db_location_types:
            selected = ''
            if location.lieTliID == location_type.id:
                selected = " selected='selected'"
            location_types += "<option value='%d'%s>%s</option>\n" % (
                              location_type.id, selected, location_type.label())
        formFields = {'Name':_("Name"), 'Description':_("Description"),
'Location_type':_("Location type"), 'location_types':location_types,
'Sector':_("Sector"), 'sector':sectors, 'Size':_("Size"),
'Factor_steps_erasement':_('Factor steps erasement'),
'Factor_steps_number':_('Factor steps number'),  'validate':_("Modify"),
'current_name':location.lieNom, 'current_desc':location.lieDesc,
'current_size':str(location.lieIndiceTaille),
'current_steps_erasement':int(location.lieFacteurEffacementTrace),
'current_steps_number':int(location.lieFacteurNbTrace),
'location_id':location.id, 'current_population':location.lieIndicePopulation,
'Population':_("Population index")}
        html = BaseAction.getForm(self, pers, fields, formFields)

        teleport_location = ""
        roads = chemin.getRoads(pers, None, True, True)
        # join small roads and large roads
        roads = roads[0] + roads[1]
        for road in roads:
            road_out = road.chmArriveeLieID
            if pers.prsLieID == road_out:
                road_out = road.chmDepartLieID
            road_out = lieu.get(road_out)
            teleport_location += "<option value='%d'>%s</option>" % (road_out.id,
                                                                     road_out.label())
        form_action_extra = " onsubmit='return confirm(\"%s\")'" %\
            _("Are you sure you want to delete this location and all associated\
 paths?")
        formFields =  {'teleport_location':teleport_location,
                       'location_id':location.id, 'validate':_("Delete"),
                       'Teleport_character':_("Teleport characters to"),
                        'form_action_extra':form_action_extra}

        html += BaseAction.getForm(self, pers, fields, formFields,
                            self._tpl.suppForm, _("Delete current location"))
        return html

from templates.actions import tplCreateSector
class CreateSector(BaseAction):
    """
    Create a new geographic sector
    """

    _dbAction = action.getByName('create_sector')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateSector

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        try:
            secteur(sctNom=fields['name'], sctDescription=fields['desc'], sctTscID=1,
sctCoutPn=fields['fp_cost'], sctCoutPv=fields['lp_cost'],
sctIndicePopulation=int(fields['population']))
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("New sector created"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new sector
        """

        formFields = {'Name':_("Name"), 'Description':_("Description"),
'Population':_("Population index"), 'LP_daily_cost':_("Life point daily cost"),
'FP_daily_cost':_("FP daily cost"), 'current_name':'', 'current_desc':'',
'current_population':0, 'current_fp_cost':0, 'current_lp_cost':0, 'sector_id':0}
        return BaseAction.getForm(self, pers, fields, formFields)

class ModifySector(BaseAction):
    """
    Modify a current geographic sector
    """

    _dbAction = action.getByName('modify_sector')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateSector

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        try:
            sector = secteur.get(int(fields['sector_id']))
            if 'delete_sector' in fields:
                sector.destroySelf()
                return _("Sector deleted"), None
            else:
                sector.sctNom = fields['name']
                sector.sctDescription = fields['desc']
                sector.sctCoutPn = fields['fp_cost']
                sector.sctCoutPv = fields['lp_cost']
                sector.sctIndicePopulation = int(fields['population'])
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("Sector modified"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new sector
        """
        location = lieu.get(pers.prsLieID)
        if not location.lieSctID:
            return _("Current location has no associated sector")
        sector = secteur.get(location.lieSctID)
        formFields = {'Name':_("Name"), 'Description':_("Description"),
'Population':_("Population index"), 'LP_daily_cost':_("Life point daily cost"),
'FP_daily_cost':_("FP daily cost"), 'current_name':sector.sctNom,
'current_desc':sector.sctDescription,
'current_population':int(sector.sctIndicePopulation), 'current_fp_cost':int(sector.sctCoutPn),
'current_lp_cost':int(sector.sctCoutPv), 'sector_id':sector.id}

        html = BaseAction.getForm(self, pers, fields, formFields)
        form_action_extra = " onsubmit='return confirm(\"%s\")'" %\
            _("Are you sure you want to delete this sector?")
        formFields =  {'sector_id':sector.id, 'sector_name':sector.label(),
                  'validate':_("Delete"), 'form_action_extra':form_action_extra}
        html += BaseAction.getForm(self, pers, fields, formFields,
                            self._tpl.suppForm, _("Delete current sector"))
        return html

from templates.actions import tplCreateRoad
class CreateRoad(BaseAction):
    """
    Create a new road
    """

    _dbAction = action.getByName('create_road')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateRoad

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'new_sector_id' in fields:
            return "", {'sector_id':fields['new_sector_id']}
        try:
            pch_id = None
            if fields['particularity'] == '_new_':
                if not fields['detail']:
                    return _("Database error"), returnedFieldList
                pch = particularite_chemin(pchNom=fields['detail'])
                pch_id = pch.id
            elif fields['particularity'] != '0':
                pch_id = int(fields['particularity'])
            chm_aller = True
            if fields['chm_aller'] == '0':
                chm_aller = False
            chm_retour = True
            if fields['chm_retour'] == '0':
                chm_retour = False
            loc_id = None
            if fields['topo'] == '1':
                loc_id = int(fields['loc_id'])
            else:
                lc = lieu_carto.getByPoint(int(fields['x']), int(fields['y']), int(fields['world_id']))
                loc_id = lc.licLieID
            blocking = False
            if fields['blocking'] == '1':
                blocking = True
            crc_id_2 = None
            if fields['crc_id_2']:
                crc_id_2 = int(fields['crc_id_2'])
            chemin(chmTchID=int(fields['road_type']), chmPchID=pch_id,
chmDepartLieID=pers.prsLieID, chmArriveeLieID=loc_id, chmDescAller=fields['desc_1'],
chmDescRetour=fields['desc_2'], chmAller=chm_aller, chmRetour=chm_retour,
chmDifficulte=int(fields['diff']), chmCoutPa=fields['ap_cost'], chmCoutPn=fields['fp_cost'],
chmCoutEchecPv=fields['pv_cost'], chmIndiceTaille=int(fields['size']),
chmFacteurEffacementTrace=int(fields['steps_erasement']),
chmDiscretion=int(fields['discretion']),
chmFacteurNbTrace=int(fields['steps_number']), chmEchecBloquant=blocking,
chmCrcID=int(fields['crc_id']), chmCrc2ID=crc_id_2,
chmOuvert=True)
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("New road created"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new road
        """
        db_sectors = secteur.select("1=1 order by sct_nom")
        sectors = ""
        for sector in db_sectors:
            sectors += "<option value='%d'>%s</option>\n" % (sector.id, sector.label())
        formFields = {'Change_destination_sector':_("Change destination sector"),
'Sector':_("Sector"), 'sectors':sectors}

        html = BaseAction.getForm(self, pers, fields, formFields, self._tpl.getChangeSectorForm, None)

        location = lieu.get(pers.prsLieID)
        current_loc = location.label()
        if location.lieCarto:
            lc = lieu_carto.getByLocation(location.id)
            x, y = lc.getCoordinates()
            current_loc = "%s - %s (%d, %d)" % (current_loc, monde.get(lc.licMndID).label(), x, y)
        roads_types = ""
        for rt in list(type_chemin.select("1=1")):
            roads_types += "<option value='%s'>%s</option>" % (rt.id, rt.label())

        sector_id = 0
        if 'sector_id' in fields:
            sector_id = int(fields['sector_id'])
        else:
            loc = lieu.get(pers.prsLieID)
            if loc.lieSctID:
                sector_id = loc.lieSctID

        a_loc = ""
        loc_chooser = _("Choose first a destination sector")
        if sector_id:
            sct = secteur.get(sector_id)
            a_loc = '%s %s' % (_("A location in"), sct.label())
            lc = ''
            locations = list(lieu.select('lie_sct_id=%d and lie_id!=%d and lie_carto=FALSE' % (
                                                                    sector_id, pers.prsLieID)))
            for location in locations:
                lc += "<option value='%s'>%s</option>" % (location.id, location.label())
            loc_chooser = self._tpl.locChooser % lc

        world = ''
        for wrld in list(monde.select("1=1")):
            world += "<option value='%d'>%s</option>\n" % (wrld.id, wrld.label())

        particularity = "<option value='0'></option>"
        particularity += "<option value='_new_'>%s</option>" % _("New particularity")
        for pc in list(particularite_chemin.select("1=1")):
            particularity += "<option value='%s'>%s</option>" % (pc.id, pc.label())

        characteristics = ''
        characteristics_2 = "<option value=''>Aucune</option>"
        for cr in list(Characteristic.select("1=1")):
            characteristics += "<option value='%s'>%s</option>" % (cr.id, cr.label())
        characteristics_2 += characteristics

        formFields = {'sector_id':str(sector_id), 'Road_from':_("Road from"),
'current_loc':current_loc, 'To':_("To"),
'A_location_in':a_loc, 'loc_chooser':loc_chooser, 'world':world, 'Road_types':_('Road type'),
'roads_types':roads_types, 'Particularity':_("Particularity"), 'particularity':particularity,
'One_way_desc':_('One way description'), 'Back_desc':_("Back description"),
'One_way_open':_('Open : one way'), 'Back_open':_("Open : back"), 'Difficulty':_("Difficulty"),
'Discretion':_("Discretion"), 'discretion':0,
'AP_cost':_("AP cost"), 'FP_cost':_("FP cost"), 'Failed_LP_cost':_("Life point cost when failed"),
'Size':_('Size'), 'Blocking':_("Blocking"), 'Yes':_("Yes"), 'No':_("No"),
'Associated_characteristic':_("Associated characteristic"),
'Factor_steps_erasement':_('Factor steps erasement'),
'Factor_steps_number':_('Factor steps number'),
'characteristics':characteristics, 'characteristics_2':characteristics_2,
'topo_checked':" checked='checked'", 'carto_checked':'',
'x':'', 'y':'', 'desc_1':'', 'desc_2':'', 'chm_aller':'', 'chm_aller_no':'',
'chm_retour':'', 'chm_retour_no':'', 'difficulty':0, 'ap_cost':0, 'fp_cost':0,
'pv_cost':0, 'size':60, 'current_steps_erasement':100, 'current_steps_number':100,
'blocking':'', 'blocking_no':" selected='selected'", 'road_id':0
}
        return html + BaseAction.getForm(self, pers, fields, formFields)

class ModifyRoad(BaseAction):
    """
    Modify roads
    """

    _dbAction = action.getByName('modify_road')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplCreateRoad

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'new_sector_id' in fields:
            return "", {'sector_id':fields['new_sector_id']}
        try:
            if 'delete_road' in fields:
                chemin.get(fields['road_id']).destroySelf()
                return _("Road deleted"), None
            pch_id = None
            if fields['particularity'] == '_new_':
                if not fields['detail']:
                    return _("Database error"), returnedFieldList
                pch = particularite_chemin(pchNom=fields['detail'])
                pch_id = pch.id
            elif fields['particularity'] != '0':
                pch_id = int(fields['particularity'])
            chm_aller = True
            if fields['chm_aller'] == '0':
                chm_aller = False
            chm_retour = True
            if fields['chm_retour'] == '0':
                chm_retour = False
            loc_id = None
            if fields['topo'] == '1':
                loc_id = int(fields['loc_id'])
            else:
                lc = lieu_carto.getByPoint(int(fields['x']), int(fields['y']), int(fields['world_id']))
                loc_id = lc.licLieID
            blocking = False
            if fields['blocking'] == '1':
                blocking = True
            crc_id_2 = None
            if fields['crc_id_2']:
                crc_id_2 = int(fields['crc_id_2'])

            road = chemin.get(fields['road_id'])
            road.chmTchID = int(fields['road_type'])
            road.chmPchID = pch_id
            road.chmDepartLieID = pers.prsLieID
            road.chmArriveeLieID = loc_id
            road.chmDescAller = fields['desc_1']
            road.chmDescRetour = fields['desc_2']
            road.chmAller = chm_aller
            road.chmRetour = chm_retour
            road.chmDifficulte = int(fields['diff'])
            road.chmDiscretion = int(fields['discretion'])
            road.chmCoutPa = fields['ap_cost']
            road.chmCoutPn = fields['fp_cost']
            road.chmCoutEchecPv = fields['pv_cost']
            road.chmIndiceTaille = int(fields['size'])
            road.chmFacteurEffacementTrace = int(fields['steps_erasement'])
            road.chmFacteurNbTrace = int(fields['steps_number'])
            road.chmEchecBloquant = blocking
            road.chmCrcID = int(fields['crc_id'])
            road.chmCrc2ID = crc_id_2
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("Road modified"), None

    def getForm(self, pers, fields):
        """
        Display a form to modify a road
        """
        db_sectors = secteur.select("1=1 order by sct_nom")
        sectors = ""
        for sector in db_sectors:
            sectors += "<option value='%d'>%s</option>\n" % (sector.id, sector.label())
        formFields = {'Change_destination_sector':_("Change destination sector"),
'Sector':_("Sector"), 'sectors':sectors}

        html = BaseAction.getForm(self, pers, fields, formFields, self._tpl.getChangeSectorForm, None)

        location = lieu.get(pers.prsLieID)
        current_loc = location.label()
        if location.lieCarto:
            lc = lieu_carto.getByLocation(location.id)
            x, y = lc.getCoordinates()
            current_loc = "%s - %s (%d, %d)" % (current_loc, monde.get(lc.licMndID).label(), x, y)


        roads = chemin.getRoads(pers, None, True, True)
        # join small roads and large roads
        roads = roads[0] + roads[1]
        selected_str = " selected='selected'"

        for road in roads:
            road_in = road.chmDepartLieID
            road_out = road.chmArriveeLieID
            desc_1 = road.chmDescAller
            desc_2 = road.chmDescRetour
            chm_aller, chm_aller_no = "", selected_str
            chm_retour, chm_retour_no = "", selected_str
            if road.chmAller:
                chm_aller = selected_str
                chm_aller_no = ""
            if road.chmRetour:
                chm_retour = selected_str
                chm_retour_no = ""
            blocking = ""
            blocking_no = selected_str
            if road.chmEchecBloquant:
                blocking = selected_str
                blocking_no = ""
            if pers.prsLieID == road_out:
                road_in = road_out
                road_out = road.chmDepartLieID
                desc_1 = desc_2
                desc_2 = road.chmDescAller
                aller = chm_aller
                aller_no = chm_aller_no
                chm_aller = chm_retour
                chm_aller_no = chm_retour_no
                chm_retour = aller
                chm_retour_no = aller_no
            road_out = location.get(road_out)
            roads_types = ""
            for rt in list(type_chemin.select("1=1")):
                selected = ''
                if rt.id == road.chmTchID:
                    selected = selected_str
                roads_types += "<option value='%d'%s>%s</option>" % (rt.id,
                                                           selected, rt.label())
            sector_id = 0

            if 'sector_id' in fields:
                sector_id = int(fields['sector_id'])
            else:
                if road_out.lieSctID:
                    sector_id = road_out.lieSctID
            a_loc = ""
            loc_chooser = _("Choose first a destination sector")
            if sector_id:
                sct = secteur.get(sector_id)
                a_loc = '%s %s' % (_("A location in"), sct.label())
                lc = ''
                locations = list(lieu.select('lie_sct_id=%d and lie_id!=%d and \
lie_carto=FALSE' % (sector_id, pers.prsLieID)))
                for location in locations:
                    selected = ''
                    if location.id == road_out.id:
                        selected = selected_str
                    lc += "<option value='%s'%s>%s</option>" % (location.id,
                                                    selected, location.label())
                loc_chooser = self._tpl.locChooser % lc

            x, y = '', ''
            wrld_id = 0
            topo_checked = " checked='checked'"
            carto_checked = ''
            if road_out.lieCarto:
                lc = lieu_carto.getByLocation(road_out.id)
                x, y = lc.getCoordinates()
                wrld_id = lc.licMndID
                topo_checked = ''
                carto_checked = " checked='checked'"
            world = ''
            for wrld in list(monde.select("1=1")):
                selected = ''
                if wrld_id == wrld.id:
                    selected = selected_str
                world += "<option value='%d'%s>%s</option>\n" % (wrld.id,
                                                        selected, wrld.label())
            particularity = "<option value='0'></option>"
            particularity += "<option value='_new_'>%s</option>" % \
                                                          _("New particularity")
            for pc in list(particularite_chemin.select("1=1")):
                selected = ''
                if road.chmPchID == pc.id:
                    selected = selected_str
                particularity += "<option value='%s'%s>%s</option>" % (pc.id,
                                                           selected, pc.label())
            characteristics, characteristics_2 = '', ''
            selected2 = ""
            if not road.chmCrc2ID:
                selected2 = selected_str
            characteristics_2 = "<option value=''%s>Aucune</option>" % selected2
            for cr in list(Characteristic.select("1=1")):
                selected, selected2 = '', ''
                if road.chmCrcID == cr.id:
                    selected = selected_str
                if road.chmCrc2ID == cr.id:
                    selected2 = selected_str
                characteristics += "<option value='%s'%s>%s</option>" % (cr.id,
                                                           selected, cr.label())
                characteristics_2 += "<option value='%s'%s>%s</option>" % (cr.id,
                                                          selected2, cr.label())
            discretion = 0
            if road.chmDiscretion:
                discretion = int(road.chmDiscretion)
            difficulty = 0
            if road.chmDifficulte:
                difficulty = int(road.chmDifficulte)
            ap_cost = '0'
            if road.chmCoutPa:
                ap_cost = road.chmCoutPa
            fp_cost = '0'
            if road.chmCoutPn:
                fp_cost = road.chmCoutPn
            pv_cost = '0'
            if road.chmCoutEchecPv:
                pv_cost = road.chmCoutEchecPv
            size = 60
            if road.chmIndiceTaille:
                size = int(road.chmIndiceTaille)
            current_steps_erasement = 100
            if road.chmFacteurEffacementTrace:
                current_steps_erasement = int(road.chmFacteurEffacementTrace)
            current_steps_number = 100
            if road.chmFacteurNbTrace:
                current_steps_number = int(road.chmFacteurNbTrace)
            formFields = {'sector_id':str(sector_id), 'Road_from':_("Road from"),
'current_loc':current_loc, 'To':_("To"),
'A_location_in':a_loc, 'loc_chooser':loc_chooser, 'world':world, 'Road_types':_('Road type'),
'roads_types':roads_types, 'Particularity':_("Particularity"), 'particularity':particularity,
'One_way_desc':_('One way description'), 'Back_desc':_("Back description"),
'One_way_open':_('Open : one way'), 'Back_open':_("Open : back"), 'Difficulty':_("Difficulty"),
'AP_cost':_("AP cost"), 'FP_cost':_("FP cost"), 'Failed_LP_cost':_("Life point cost when failed"),
'Size':_('Size'), 'Blocking':_("Blocking"), 'Yes':_("Yes"), 'No':_("No"),
'Associated_characteristic':_("Associated characteristic"),
'Factor_steps_erasement':_('Factor steps erasement'),
'Factor_steps_number':_('Factor steps number'),
'characteristics':characteristics, 'characteristics_2':characteristics_2,
'Discretion':_("Discretion"), 'discretion':discretion,
'topo_checked':topo_checked, 'carto_checked':carto_checked,
'x':str(x), 'y':str(y), 'desc_1': desc_1, 'desc_2':desc_2,
'chm_aller':chm_aller, 'chm_aller_no':chm_aller_no, 'chm_retour':chm_retour,
'chm_retour_no':chm_retour_no, 'difficulty':difficulty,
'ap_cost':ap_cost, 'fp_cost':fp_cost, 'pv_cost':pv_cost, 'size':size,
'current_steps_erasement':current_steps_erasement,
'current_steps_number':current_steps_number, 'blocking':blocking,
'blocking_no':blocking_no, 'road_id':road.id
}
            html += BaseAction.getForm(self, pers, fields, formFields)

            form_action_extra = " onsubmit='return confirm(\"%s\")'" %\
                _("Are you sure you want to delete this road?")
            formFields =  {'road_id':road.id, 'validate':_("Delete"),
                           'form_action_extra':form_action_extra}
            html += BaseAction.getForm(self, pers, fields, formFields,
                                       self._tpl.suppForm, None)
        return html

from templates.actions import tplAddLocationDescription
class AddLocationDescription(BaseAction):
    """
    Add a new description to a location
    """

    _dbAction = action.getByName('create_loc_desc')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplAddLocationDescription

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        try:
            description_lieu(dslLieID=int(fields['location_id']),
dslDifficulte=int(fields['diff']), dslDescription=fields['desc'])
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("New description created"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new location and associated roads
        """
        location = lieu.get(pers.prsLieID)
        formFields = {'Description':_("Description"),
'Difficulty':_("Difficulty"), 'location_name':location.label(),
'location_id': location.id, 'validate':_("Create"),
'current_desc':'', 'diff':0, 'dl_id':0}
        return BaseAction.getForm(self, pers, fields, formFields)

class ModifyLocationDescription(BaseAction):
    """
    Modify precise description of the location
    """

    _dbAction = action.getByName('modify_loc_desc')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplAddLocationDescription

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        try:
            dl = description_lieu.get(int(fields['dl_id']))
            if 'delete_desc' in fields:
                dl.destroySelf()
                return _("Precise description deleted"), None
            dl.dslDifficulte = int(fields['diff'])
            dl.dslDescription = fields['desc']
        except:
            return _("Database error"), returnedFieldList
        menu.evaluateMenu(session, pers.id)
        return _("Precise description modified"), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new location and associated roads
        """
        location = lieu.get(pers.prsLieID)
        dls = list(description_lieu.select("dsl_lie_id=%d" % location.id))
        if not dls:
            return _("No precise description on this location")
        html = ""
        for dl in dls:
            formFields = {'Description':_("Description"),
'Difficulty':_("Difficulty"), 'location_name':location.label(),
'location_id': location.id, 'validate':_("Modify"),
'current_desc':dl.dslDescription, 'diff':int(dl.dslDifficulte), 'dl_id':dl.id
}
            html += BaseAction.getForm(self, pers, fields, formFields)

            form_action_extra = " onsubmit='return confirm(\"%s\")'" %\
                _("Are you sure you want to delete this description?")
            formFields =  {'dl_id':dl.id, 'validate':_("Delete"),
                           'form_action_extra':form_action_extra}
            html += BaseAction.getForm(self, pers, fields, formFields,
                                       self._tpl.suppForm, None)
        return html

from templates.actions import tplAdminTeleport
class AdminTeleport(BaseAction):
    """
    A teleport action for admin
    """

    _dbAction = action.getByName('admin_teleport')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplAdminTeleport

    def isAvailable(self, pers, extra={}):
        """
        Available when you are an admin.
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        if not self._available:
            return self._available, reason
        return True,''

    def do(self, pers, fields, session, menu, extra={}):
        """
        Evaluate the succes of the action
        """
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            return succes, returnedFieldList
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields, session, extra)
        if not self._available :
            return succes, returnedFieldList
        if 'new_sector_id' in fields:
            return "", {'sector_id':fields['new_sector_id']}
        loc_id = None
        if fields['topo'] == '1':
            loc_id = int(fields['loc_id'])
        else:
            lc = lieu_carto.getByPoint(int(fields['x']), int(fields['y']), int(fields['world_id']))
            loc_id = lc.licLieID
        pers.prsLieID = loc_id
        menu.evaluateMenu(session, pers.id)
        return _("Shazam! Teleported to a new location."), None

    def getForm(self, pers, fields):
        """
        Display a form to create a new location and associated roads
        """
        db_sectors = secteur.select("1=1 order by sct_nom")
        sectors = ""
        for sector in db_sectors:
            sectors += "<option value='%d'>%s</option>\n" % (sector.id, sector.label())
        formFields = {'Change_destination_sector':_("Change destination sector"),
'Sector':_("Sector"), 'sectors':sectors}

        html = BaseAction.getForm(self, pers, fields, formFields,
                                  self._tpl.getChangeSectorForm, None)

        sector_id = 0
        if 'sector_id' in fields:
            sector_id = int(fields['sector_id'])
        else:
            loc = lieu.get(pers.prsLieID)
            if loc.lieSctID:
                sector_id = loc.lieSctID
        a_loc = ""
        loc_chooser = _("Choose first a destination sector")
        if sector_id:
            sct = secteur.get(sector_id)
            a_loc = '%s %s' % (_("A location in"), sct.label())
            lc = ''
            locations = list(lieu.select('lie_sct_id=%d and lie_id!=%d and lie_carto=FALSE' % (
                                                                    sector_id, pers.prsLieID)))
            for location in locations:
                lc += "<option value='%s'>%s</option>" % (location.id, location.label())
            loc_chooser = self._tpl.locChooser % lc
        world = ''
        for wrld in list(monde.select("1=1")):
            world += "<option value='%d'>%s</option>\n" % (wrld.id, wrld.label())

        formFields = {'Teleport_to':_("Teleport to"),
'sector_id':sector_id, 'topo_checked':" checked='checked'", 'carto_checked':'',
'A_location_in':a_loc, 'loc_chooser':loc_chooser, 'world':world,
'x':'', 'y':'', 'validate':_("Teleport!")}
        return html + BaseAction.getForm(self, pers, fields, formFields)

