#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

"""
Defining dynamic menus
"""

from states import _states
from db.dbCharacters import personnage

class Menu:
    """
    Agencement des différentes actions dans le menu.
    """

    def __init__(self):
        self.sections = []

    def addSection(self, section):
        """
        Ajout d'une section à un menu.

        @type  section : menuSection
        @param section : Section � ajouter � ce menu.
        """

        self.sections.append(section)

    def getSections(self):
        sections = []
        for section in self.sections:
            sections.append((section.id, section.label))
        return sections

    def getActionDict(self):
        """
        Obtention d'un dictionnaire associant le nom de l'action a sa
        classe.

        @rtype: dict
        @return: Retourne un dictionnaire associant nom de l'action � sa
        classe.
        """

        dict = {}
        for section in self.sections:
            for action in section.actions :
                dict[action._id] = action
        return dict

    def getActionList(self, session):
        """
        Obtention d'un dictionnaire associant le nom de la section du menu
        à une liste donnant les noms des actions disponibles.

        @type session : session
        @param session : Session de l'utilisateur concern�.
        @rtype: dict
        @return: Retourne un dictionnaire associant nom de l'action � sa
        classe.
        """

        if not session.hasValue('actions'):
            return None

        actions = {}
        for section in self.sections:
            actions[section.id] = []
            for action in section.actions :
                if action._id in session.value('actions', None):
                    actions[section.id].append((action._id, action._name))
            if len(actions[section.id]) < 1:
                del actions[section.id]
        return actions

    def evaluateMenu(self, session, id):
        """
        Evaluate actions and states available for a given character.
        Stock them in the session.

        Attributes :
        Session session -- player's session.
        integer id -- character's id.
        """

        actions = []
        actionDict = self.getActionDict()
        character = personnage.get(id)
        # check if the character is implicated in a fight
        character.getTakeParts()
        for action in actionDict.keys():
            myAction = actionDict[action]()
            available, trash = myAction.isAvailable(character)
            if available:
                actions.append(action)
        states = []
        for state in _states.states:
            available = state().isAvailable(character)
            if available:
                states.append(state.id)
        session.setValue('actions', actions)
        session.setValue('states', states)

class MenuSection:
    """
    Section d'un menu.
    """
    def __init__(self, id, label=''):
        """
        La classe est initialisée avec le nom de la section.
        Ce nom apparaitra dans l'entete du menu.
        Empty label for a section mean a root action
        @type name: string
        @param name: Nom de la section.
        """
        self.label = label
        self.id = id
        self.actions = []

    def addAction(self, action):
        """
        Ajout d'une action à une section.
        @type  action : action
        @param action : Action à ajouter à ce menu
        """
        self.actions.append(action)

