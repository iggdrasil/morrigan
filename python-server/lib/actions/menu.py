#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
"""
Application menu

   Menu _menu -- Menu including sections and actions
"""

from lib.utils import _

from menus import Menu, MenuSection
from states import _states
from commonActions import DoingNothing, ChangeCharacter, ChangeCharacterData, \
                          ChangeShape, Talk, Emotes, TakingNotes
from geographyActions import Move, SetRumors, GetRumors, Follow, SearchSteps, \
                             ShowWay, LookExactly, SearchPath, Dig
from inventoryActions import GiveObject, Drop, Equip, Reload, Unload, Buy,\
        TakeObject, PutInto, TakeInto, OpenCloseChest, OpenCloseShop, ManageShop
from adminActions import CreateLocation, CreateSector, CreateRoad, \
                         AddLocationDescription, ModifyLocation, ModifySector,\
                         ModifyRoad, ModifyLocationDescription, AdminTeleport
from fightActions import TakingPart, LeaveFight, TakeInit, Attack

__emptySection = MenuSection('root_menu')
__emptySection.addAction(ChangeCharacterData)
__adminSection = MenuSection('admin_menu', _("Administration"))
__adminSection.addAction(AdminTeleport)
__adminSection.addAction(CreateSector)
__adminSection.addAction(ModifySector)
__adminSection.addAction(CreateLocation)
__adminSection.addAction(ModifyLocation)
__adminSection.addAction(AddLocationDescription)
__adminSection.addAction(ModifyLocationDescription)
__adminSection.addAction(CreateRoad)
__adminSection.addAction(ModifyRoad)
__communicationSection = MenuSection('communication_menu', _("Communicate"))
__communicationSection.addAction(Talk)
__communicationSection.addAction(Emotes)
__communicationSection.addAction(SetRumors)
__communicationSection.addAction(GetRumors)
__communicationSection.addAction(TakingNotes)
__fightSection = MenuSection('fight_menu', _("Fight"))
__fightSection.addAction(TakingPart)
__fightSection.addAction(Attack)
__fightSection.addAction(TakeInit)
__fightSection.addAction(LeaveFight)
__locationSection = MenuSection('location_menu', _("Movement"))
__locationSection.addAction(Move)
__locationSection.addAction(Follow)
__locationSection.addAction(SearchSteps)
__locationSection.addAction(ShowWay)
__locationSection.addAction(Dig)
__locationSection.addAction(LookExactly)
__locationSection.addAction(SearchPath)
__inventorySection = MenuSection('inventory_menu', _("Inventory"))
__inventorySection.addAction(Equip)
__inventorySection.addAction(GiveObject)
__inventorySection.addAction(Drop)
__inventorySection.addAction(TakeObject)
__inventorySection.addAction(PutInto)
__inventorySection.addAction(TakeInto)
__inventorySection.addAction(OpenCloseChest)
__inventorySection.addAction(Reload)
__inventorySection.addAction(Unload)
__tradeSection = MenuSection('trade_menu', _("Trade"))
__tradeSection.addAction(OpenCloseShop)
__tradeSection.addAction(ManageShop)
__tradeSection.addAction(Buy)
__diversSection = MenuSection('different_things_menu', _("Different things"))
__diversSection.addAction(DoingNothing)
__diversSection.addAction(ChangeCharacter)
__diversSection.addAction(ChangeShape)
_menu = Menu()
_menu.addSection(__emptySection)
_menu.addSection(__adminSection)
_menu.addSection(__communicationSection)
_menu.addSection(__fightSection)
_menu.addSection(__locationSection)
_menu.addSection(__inventorySection)
_menu.addSection(__tradeSection)
_menu.addSection(__diversSection)


