#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################
# PROJECT : Morrigan - FILE : action.py - CREATION : 2004/09/01
#
# Copyright (C) 2004-2005 Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Fichier contenant la classe de base pour les actions.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from utils import _, evalFormula
import plugins.configuration as config

MULTIPLICATOR_TABLE_SIMPLE = [1, 2, 2, 1]
MULTIPLICATOR_TABLE_WELL = [1, 2, 3, 4]

class BaseAction:
    '''
    Base class to define actions.
    Attributes :
        - string _name : action name to display
        - string _id : id of the action (no spaces, no specials characters)
        - Action _dbAction : action fields
    '''
    _name = ""
    _id = "Action"
    _dbAction = None
    _tpl = None
    _baseFields = []
    CRITIC_FAIL = 0
    FAIL = 1
    SUCCES = 2
    CRITIC_SUCCES = 3

    def __init__(self):
        '''
        Action init
        '''
        self._available = False

    def init(self, pers, extra={}):
        '''Init apNeeded and fpNeeded
        Attributes :
        pers (character) -- designed character
        extra (dict) -- extra parameters to evaluate the formula
        '''
        self.apNeeded, self.fpNeeded, self.fipNeeded = 0, 0, 0
        if self._dbAction:
            # if there is differents cost, worst cost are taken
            if type(extra) != list:
                extra = [extra]
            for ext in extra:
                apNeeded = evalFormula(self._dbAction.actFormCoutPa, pers.id,
                                       ext)
                #TODO: "or apNeeded < 0" beta fix => change it for production
                if apNeeded > self.apNeeded or apNeeded < 0:
                    self.apNeeded = apNeeded
                fpNeeded = evalFormula(self._dbAction.actFormCoutPn, pers.id,
                                       ext)
                if abs(fpNeeded) > abs(self.fpNeeded):
                    self.fpNeeded = fpNeeded
                fipNeeded = evalFormula(self._dbAction.actFormCoutPc, pers.id,
                                       ext)
                if abs(fipNeeded) > abs(self.fipNeeded):
                    self.fipNeeded = fipNeeded

    def setVarFromField(key, fields):
        if fields.has_key(key):
            return fields[key]
        return ""
    setVarFromField = staticmethod(setVarFromField)

    def isAvailable(self, pers, extra={}):
        '''
        Define if the action is available for the designed character.
        Attributes :
        pers (character) -- designed character
        extra (dict) -- extra parameters to evaluate the formula
        Return :
        boolean, string -- availability and reason
        '''
        self._available = True
        self.init(pers, extra)
        reason = ""
        if self._dbAction:
            self._available, reason = self._dbAction.isAvailable(pers,
                                  self.apNeeded, self.fpNeeded, self.fipNeeded)
        return self._available, reason

    def do(self, pers, fields, session, extra={}):
        '''
        Action realisation.

        Attributes:
        pers (character) -- designed character
        fields (dict) -- form fields
        session (session) -- character session
        extra (dict) -- extra parameters to evaluate the formula
        Return :
        integer, integer, list -- result of the action (-1 action not
        available, 0 critical failure, 1 simple failure, 2 simple success,
        3 critical success), precise result of the action (for finest
        resolution), list of the fields to be send to the next page.
        extra has a key 'opponent_id' if there is a designed opponent
        '''
        self.init(pers, extra)
        multi = None
        if type(extra) == list:
            multi = True
        result = []
        simple = []
        fieldList = []
        if self._dbAction:
            available, reason = None, None
            if not multi:
                available, reason = self.isAvailable(pers, extra)
                extra = [extra]
                if not available:
                    fieldList.append(reason)
                    return -1, 0, fieldList
            else:
                available = None
                if not extra:
                    self.init(pers)
                    self.applyCost(pers)
                    return [-1], [0], fieldList
                for ext in extra:
                    if not available:
                        available, reason = self.isAvailable(pers, ext)
                if not available:
                    fieldList.append(reason)
                    return [-1], [0], fieldList
            score = []
            for ext in extra:
                attack = evalFormula(self._dbAction.actFormAttaque, pers.id,
                                     ext)
                if abs(attack) >= config.TRAINING_DELTA_DIFF:
                    simple.append(True)
                else:
                    simple.append(None)
                defense = 0
                opponent_id = 0
                if 'opponent_id' in ext:
                    opponent_id = ext['opponent_id']
                reverse_ext = {}
                for key in ext:
                    if key.startswith('opp_'):
                        other_key = key[4:]
                        if other_key in ext:
                            reverse_ext[key] = ext[other_key]
                        reverse_ext[other_key] = ext[key]
                    elif key in reverse_ext:
                        continue
                    else:
                        reverse_ext[key] = ext[key]
                defense = evalFormula(self._dbAction.actFormDefense,
                                      opponent_id, reverse_ext)
                score.append(attack - defense)
                ext['M'] = attack - defense
                if defense > attack:
                    if (-score[-1]) < config.CRITICISM_CEIL:
                        result.append(self.FAIL)
                    else:
                        result.append(self.CRITIC_FAIL)
                else:
                    if (score[-1]) < config.CRITICISM_CEIL:
                        result.append(self.SUCCES)
                    else:
                        result.append(self.CRITIC_SUCCES)
            print "Tests de l'action : %s (%d-%d)" % (str(score),
                                                      attack, defense)

            id = 0
            # if there is several dice throw, the best is returned
            for i in xrange(len(result)):
                if simple[i] >= simple[id] and result[i] >= result[id]:
                    id = i
                extra[i]['success'] = result[i]
            simple = simple[id]
            res = result[id]
            self.init(pers, extra[id])
            self.applyCost(pers)
            if simple and self._dbAction.actFormGainMin:
                self._dbAction.evalXPFormula(self._dbAction.actFormGainMin,
                            pers.id, MULTIPLICATOR_TABLE_SIMPLE[res], None,
                            extra[id])
            elif self._dbAction.actFormGainMax:
                self._dbAction.evalXPFormula(self._dbAction.actFormGainMax,
                        pers.id, MULTIPLICATOR_TABLE_WELL[res], True, extra[id])
            # the best formula is returned for the training
            self._dbAction.evalTrainFormula(self._dbAction.actFormGainMax %
                                            extra[id], pers.id)
        if not multi:
            return result[0], score[0], fieldList
        return result, score, fieldList

    def applyCost(self, pers):
        if pers.prsPa - self.apNeeded > pers.prsPaMax:
            pers.prsPa = pers.prsPaMax
        else:
            pers.prsPa += - self.apNeeded
        if pers.prsPn - self.fpNeeded >  pers.prsPnMax:
            pers.prsPn = pers.prsPnMax
        else:
            pers.prsPn += - self.fpNeeded
        if pers.prsPc - self.fipNeeded >  pers.prsPcMax:
            pers.prsPc = pers.prsPcMax
        else:
            pers.prsPc += - self.fipNeeded

    FORM_START = "<h1>%s</h1><div class='actionForm'>"
    FORM_ACTION = "<form action='#' method='post'%s>\n"
    FORM_END = '''<input type='hidden' name="curAction" value="%s" />
<div class='button'><input type='submit' name="_action_curAction" value="%s" />
</div></form></div>'''
    def getForm(self, pers, fields, formFields={}, altForm={}, title=True):
        '''
        Send the html for the form.

        Attributes :
        pers (character) -- character
        fields (list) -- current fields
        Return :
        string -- send the generated HTML code
        '''
        form = ""
        if title:
            if type(title) == str:
                form = self.FORM_START % title
            else:
                form = self.FORM_START % self._name
        else:
            form = "<div class='actionForm'>"
        availability, reason = True, ""
        # simple test is done without extra parameters
        if 'costAP' in formFields or 'costFP' in formFields \
           or 'costFiP' in formFields:
            availability, reason = self.isAvailable(pers)
        if not availability:
            not_available = _("Action not available")
            form += "<p>%s : %s</p></div>" % (not_available, reason)
            return form
        form_action_extra = ''
        if 'form_action_extra' in formFields:
            form_action_extra = formFields['form_action_extra']
        form += self.FORM_ACTION % form_action_extra
        cost = []
        if 'costAP' in formFields:
            apNeeded = pers.formatAP(self.apNeeded)
            cost.append("%d %% %s" % (apNeeded, _("APs")))
        if 'costFP' in formFields:
            fpNeeded = pers.formatFP(self.fpNeeded)
            cost.append("%d %% %s" % (fpNeeded, _("FPs")))
        if 'costFiP' in formFields:
            fipNeeded = pers.formatFiP(self.fipNeeded)
            cost.append("%d %% %s" % (fipNeeded, _("FiPs")))
        cost = ", ".join(cost)
        if cost:
            cost = "<p>%s : %s</p>\n" % (_("Cost"), cost)

        keys = formFields.keys()
        for field in self._baseFields:
            if field not in keys:
                formFields[field] = BaseAction.setVarFromField(field, fields)
        if altForm:
            form += altForm % formFields
        elif self._tpl:
            form += self._tpl.getForm % formFields
        if not formFields.has_key('validate'):
            formFields['validate'] = _("Validate")
        form += cost
        form += self.FORM_END % (self._id, formFields['validate'])
        return form
