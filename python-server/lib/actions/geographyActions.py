#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################
# PROJECT : Morrigan - FILE : geographyActions.py - CREATION : 2005/12/09
#
# Copyright (C) 2005-2010  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Geography actions implementations.
cf. to the base class 'Action' to have the detail of the differents parameters
and methods.
Actions are evaluated with the session variable of the user.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
import random
from mx import DateTime

from constants import BDD_CONSTANTS as BC, SEGMENT, FOLLOW_HOURS_DELTA
import plugins.configuration as config

from db.dbCommon import action, Characteristic
from db.dbInventory import Item, VisibleItem
from db.dbCharacters import personnage, forme
from db.dbGeography import lieu, secteur, monde, chemin, lieu_carto, Rumor, \
                 Route,Tailing, KnownedSteps, StepType, TRACK_TAIL, FOLLOW_TAIL,\
                 description_lieu, acces_connu

from utils import _, textToHTML, MorriganActionError, MorriganError, \
                   evalFormula
from constants import DIRECTIONS, DIRECTION_COST
from action import BaseAction
from actionFileManager import ActionFile

from templates.actions import tplMove
class Move(BaseAction):
    """
    Move action.
    """

    _dbAction = action.getByName('move')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplMove

    def isAvailable(self, pers, extra={'e_AP_cost':0, 'e_FP_cost':0}):
        """
        This action is available when you have Action Point
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def do(self, pers, fields, session, menu, extra={}):
        '''
        Make the move.
        TODO: gérer le chemin par défaut autrement qu'en statique.
        In dict 'extra' 4 keys has to be defined :
            - 'e0' : defense bonus
            - 'e1' : difficulty ceil
            - 'e2' : action points cost
            - 'e3' : food points cost
        '''
        if not fields.has_key('destination'):
            return _("Missing parameters."), ['destination', 'curAction']

        destination = fields['destination'][4:]
        path_id = None
        try:
            path_id = int(destination)
        except ValueError:
            path_id = None
        succes, dest_id, msg, hits = None, -1, "", 0
        othersStart = pers.isNear()
        try:
            # if a specific path id defined, id is not null
            if path_id:
                succes, dest_id, msg, hits = self.pathMoving(path_id, pers,
                                                         fields, session, extra)
            else:
                succes, dest_id, msg, hits = self.mapMoving(destination, pers,
                                                         fields, session, extra)
        except MorriganActionError, e:
            return e.msg, e.returnedFieldList
        action = _("Moving")
        if dest_id == -1:
            if hits:
                pers.hit(hits, extra_msg=True)
            while pers.current_messages[0]:
                msg += '\n' + pers.current_messages[0].pop()
            pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                    _("Moves"))
            msg2Others = _("%s is trying to move but fails") % pers.label()
            while pers.current_messages[1]:
                msg2Others += '\n' + pers.current_messages[1].pop()
            pers.actionFile.writeAction(pers.isNear(), ActionFile.FEEDBACK_ID,
                        msg2Others, _("Someone try to move"), _("Moves"), None)
            return msg, None
        elif succes >= self.SUCCES:
            msg += '\n' + _('The travel was nice.')
        else:
            msg += '\n' + _('Long and hard is the road.')

        msg2Others = _("You see %s going.") % pers.prsNomActuel
        msg2NewOthers = _("You see %s coming.") % pers.prsNomActuel
        msg += '\n' + pers.move(dest_id, pers.prsLieID, path_id)
        if hits:
            pers.hit(hits, extra_msg=True)
        while pers.current_messages[0]:
            msg += '\n' + pers.current_messages[0].pop()
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                    _("Moves"))
        while pers.current_messages[1]:
            c_msg = pers.current_messages[1].pop()
            msg2Others += '\n' + c_msg
            msg2NewOthers += '\n' + c_msg
        pers.actionFile.writeAction(othersStart, ActionFile.FEEDBACK_ID,
                            msg2Others, _("Someone leaving"), _("Moves"), None)
        pers.actionFile.writeAction(pers.isNear(), ActionFile.FEEDBACK_ID,
                        msg2NewOthers, _("Someone arriving"), _("Moves"), None)
        menu.evaluateMenu(session, pers.id)
        return msg, None

    def pathMoving(self, path_id, pers, fields, session, extra):
        availableRoads = chemin.getRoads(pers, True)
        permit = None
        for road_id in availableRoads :
            if road_id == path_id:
                permit = True
        if not permit:
            raise MorriganActionError,  [_("Road not available"),
                    ['destination', 'object', 'curAction']]
        myRoad = chemin.get(path_id)
        destination = ""
        bonus = 0
        extra = {}
        if myRoad.chmCrcID:
            extra['e_carac_move_1'] = \
                         pers.getCharacteristic(myRoad.chmCrcID).pcrScoreCalcule
        if myRoad.chmCrc2ID:
            extra['e_carac_move_2'] = \
                        pers.getCharacteristic(myRoad.chmCrc2ID).pcrScoreCalcule
        extra['e_carac_evn'] = myRoad.chmArriveeLie.getEnvironementBonus(pers)
        extra['e_diff'] = myRoad.chmDifficulte
        extra['e_AP_cost'] = evalFormula(myRoad.chmCoutPa, pers.id)
        extra['e_FP_cost'] = evalFormula(myRoad.chmCoutPn, pers.id)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                             session, extra)
        if not self._available :
            raise MorriganActionError, [returnedFieldList.pop(),
                returnedFieldList + ['destination',  'object', 'curAction']]
        msg, hurt_msg, hits = '', '', 0
        # if critic fail it can hurt
        if succes == self.CRITIC_FAIL:
            cost = evalFormula(myRoad.chmCoutEchecPv, pers.id, extra)
            if cost:
                hits = cost
                hurt_msg = '\n' + _("It hurts you!")
        dest_id = -1
        if myRoad.chmEchecBloquant and succes < self.SUCCES:
            msg = _("The road was too hard for you.") + hurt_msg
            return succes, dest_id, msg, hits

        # test if the appropriate key for this road is given
        if myRoad.chmEgnID :
            if 'object' not in fields:
                raise MorriganActionError, [_("No object selected."),
                    returnedFieldList + ['destination',  'object', 'curAction']]
            object = Item.get(int(fields['object']))
            # verify if the object opening the path
            # (and accessory is in user hand)
            if myRoad.chmEgnID != object.elmEgnID:
                raise MorriganActionError, [
_("Sorry. This object doesn't open this path."),
returnedFieldList + ['destination',  'object', 'curAction']]
            sql = 'select True from element where elm_prs_id=%d and \
elm_egn_id=%d' % (pers.id, object.elmEgnID)
            res = Item._connection.queryAll(sql)
            if not res:
                raise MorriganActionError, [
_("You don't have this object anymore."),
returnedFieldList + ['destination',  'object', 'curAction']]
        if myRoad.chmDepartLieID == pers.prsLieID:
            dest_id = myRoad.chmArriveeLieID
            if myRoad.chmDescAller:
                msg = _(myRoad.chmDescAller) % {'pers':pers.label()} + hurt_msg
        else:
            dest_id = myRoad.chmDepartLieID
            if myRoad.chmDescRetour:
                msg = _(myRoad.chmDescRetour) % {'pers':pers.label()} + hurt_msg
        if not msg :
            msg = _("You're walking a while to the destination.") + hurt_msg
        return succes, dest_id, msg, hits

    def mapMoving(self, destination, pers, fields, session, extra):
        if destination not in DIRECTIONS:
            raise MorriganActionError, [_("Unknown destination"),
                                        ['destination', 'curAction']]

        # rose delta
        costs = [(DIRECTION_COST[direction][2], DIRECTION_COST[direction][3],
                      DIRECTION_COST[direction][4]) for direction in DIRECTIONS]
        new_costs = costs[pers.prsDeltaRose:] + costs[:pers.prsDeltaRose]
        dest = {}
        for idx in xrange(len(new_costs)):
            dest[DIRECTIONS[idx]] = (DIRECTION_COST[DIRECTIONS[idx]][1],
                    new_costs[idx][0], new_costs[idx][1], new_costs[idx][2])

        dest_label, delta_x, delta_y, factor_cost = dest[destination]

        location = pers.prsLie
        if not location.lieCarto:
            raise MorriganActionError, [_("Move not available"), ['destination',
                                                                  'curAction']]
        geo_localisation = lieu_carto.getByLocation(location.id)
        x, y = geo_localisation.getCoordinates()
        x += delta_x
        y += delta_y
        lc = lieu_carto.getByPoint(x, y, geo_localisation.licMndID)
        AP_cost = min(1, lc.getAPAccesCost(pers.id))
        FP_cost = min(1, lc.getFPAccesCost(pers.id))
        bonus, characteristics = lc.getCharacteristicBonus(pers.id, {}, True)
        if not characteristics:
            raise MorriganError, "There is no associated characteristics in \
layers of lieu_carto : %d" % lc.id
        extra = {'e_diff':0, 'e_carac_evn':bonus, 'e_carac_move_1':0}
        extra.update({'e_AP_cost':factor_cost*AP_cost,
                      'e_FP_cost':factor_cost*FP_cost})
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if not self._available :
            raise MorriganActionError, [returnedFieldList.pop(),
                  returnedFieldList + ['destination', 'curAction']]
        hurt_msg, hits = '', 0
        # if critic fail it can hurt
        if succes == self.CRITIC_FAIL:
            cost = lc.getPVFailCost(pers.id)
            if cost:
                hits = cost
                hurt_msg = '\n' + _("It hurts you!")

        if lc.getBlocking() and succes < self.SUCCES:
            msg = _("The road was too hard for you.") + hurt_msg
            return succes, -1, msg, hits
        msg = _("You're walking (perhaps) %s.") % dest_label + hurt_msg
        if succes >= self.SUCCES:
            return succes, lc.licLieID, msg, hits
        # if failed and if you follow nothing you can go in a wrong direction
        tails = Tailing.getFromCharacters(pers.id, None)
        for tail in tails:
            route = Route.getAvailableRouteToFollow(pers, tail.fltSuiviFrmID)
            if route and route.itnArriveeLieID == lc.licLieID:
                return succes, lc.licLieID, msg, hits

        delta = 0
        if succes == self.CRITIC_FAIL:
            delta = 1
        x -= delta_x
        y -= delta_y
        idx = DIRECTIONS.index(destination)
        available_directions = []
        my_min = idx - delta
        my_max = idx + delta + 1
        if min([idx - delta, 0]):
            my_min = 0
            available_directions += DIRECTIONS[idx - delta:]
        if idx + delta >= len(DIRECTIONS):
            my_max = len(DIRECTIONS)
            available_directions += DIRECTIONS[:idx + delta - len(DIRECTIONS) + 1]
        available_directions += DIRECTIONS[my_min:my_max]
        available_locations = []
        positions = []
        if succes != self.CRITIC_FAIL:
            available_locations.append(lc)
            positions.append(0)
        base_layer_ids = lc.getLayerIDs()
        for available_direction in available_directions:
            if available_direction == destination:
                continue
            label, dest_label, delta_x, delta_y, factor_cost = \
                                            DIRECTION_COST[available_direction]
            current_lc = lieu_carto.getByPoint(x + delta_x, y + delta_y,
                                               geo_localisation.licMndID)
            # the character can only be lost on similar lands
            if base_layer_ids != current_lc.getLayerIDs():
                continue
            available_locations.append(current_lc)
            delta = DIRECTIONS.index(available_direction) \
                    - DIRECTIONS.index(destination)
            if delta > len(DIRECTIONS)/2:
                delta -= len(DIRECTIONS)
            elif delta < -len(DIRECTIONS)/2:
                delta += len(DIRECTIONS)
            positions.append(delta)
        if not available_locations:
            # if critical fail but no other location available
            available_locations.append(lc)
            positions.append(0)
        rand = random.randint(0, len(available_locations)-1)
        pers.prsDeltaRose += positions[rand]
        return succes, available_locations[rand].licLieID, msg, hits

    def getForm(self, pers, fields):
        """
        Check available roads and present them.
        """
        dct = {'availableRoadsLabel': _("Available roads"),
               'validate':_('Move')}
        availableRoads, smallRoads = chemin.getRoads(pers, None, True)
        roadForm, itemRoadForm = "", ""
        if len(availableRoads) > 0:
            for road in availableRoads:
                if road.chmEgnID :
                    itemRoadForm += self._tpl.formDestLine % {'id':road.id,
                                                  'label':road.label(pers)}
                else :
                    roadForm += self._tpl.formDestLine % {'id':road.id,
                                                  'label':road.label(pers)}
            for road in smallRoads:
                if road.chmEgnID :
                    itemRoadForm += self._tpl.formDestLine % {'id':road.id,
                                                  'label':road.label(pers)}
                else :
                    roadForm += self._tpl.formDestLine % {'id':road.id,
                                        'label':road.label(pers, None) \
                                        + " : " + _("road too small")}
            if roadForm:
                roadForm = self._tpl.roadForm % roadForm
            if itemRoadForm:
                objects = ""
                inventory = pers.getInventory()
                for key in inventory.keys():
                    for item in inventory[key]:
                        objects += "<option value='%d'>%s</option>" % (item.id,
                                                                    item.name)
                itemRoadForm = self._tpl.itemRoadForm % {
                               'title':_("Available roads with key items"),
                               'roads':itemRoadForm, 'key':_("Key"),
                               'objects':objects}
        dct.update({'roadForm':roadForm, 'itemRoadForm':itemRoadForm})
        compassRose = ''
        location = pers.prsLie
        if location.lieCarto:
            # character delta
            costs = [(DIRECTION_COST[direction][2], DIRECTION_COST[direction][3],
                      DIRECTION_COST[direction][4]) for direction in DIRECTIONS]
            new_costs = costs[pers.prsDeltaRose:] + costs[:pers.prsDeltaRose]
            dest = {}
            for idx in xrange(len(new_costs)):
                dest[DIRECTIONS[idx]] = (DIRECTION_COST[DIRECTIONS[idx]][0],
                        new_costs[idx][0], new_costs[idx][1], new_costs[idx][2])

            geo_localisation = lieu_carto.getByLocation(location.id)
            x, y = geo_localisation.getCoordinates()
            def renderDestination(id, label, x, y, cost_factor=1):
                costAP, costFP = 0, 0
                lc = lieu_carto.getByPoint(x, y, geo_localisation.licMndID)
                AP_cost = pers.formatAP(lc.getAPAccesCost(pers.id)*cost_factor)
                FP_cost = pers.formatFP(lc.getFPAccesCost(pers.id)*cost_factor)
                label += " (%s)" % lc.label()
                if AP_cost > 100 or FP_cost > 100:
                    label += " - " + _("Not available")
                    return self._tpl.formUnavailableDestCase % {'label':label}
                label += ''' - %s : %d%%, %s : %d%%''' % (_("APs"), AP_cost,
                                                          _("FPs"), FP_cost)
                return self._tpl.formDestCase % {'id':id, 'label':label}

            compassRoseDct = {'center': ''}
            for direction in DIRECTIONS:
                compassRoseDct[direction] = renderDestination(direction,
                                    dest[direction][0], x + dest[direction][1],
                                    y + dest[direction][2], dest[direction][3])
            compassRose = self._tpl.compassRose % compassRoseDct
        dct.update({'compassRose':compassRose})
        return BaseAction.getForm(self, pers, fields, dct)

from templates.actions import tplSetRumors
class SetRumors(BaseAction):
    '''
    Setting a rumor.
    '''
    _dbAction = action.getByName('buzz')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplSetRumors
    _baseFields = ["c_title", "c_text"]

    def do(self, pers, fields, session, menu, extra={}):
        '''
        Setting a rumor.
        Test if the launch of the rumor is successfull and then effective
        writing on the database
        '''
        base_fields = self._baseFields
        if config.LANGUAGES:
            base_fields.append('language')
        for field in base_fields:
            if not fields.has_key(field) or not fields[field]:
                return _("There are empty fields."), fields
        location = pers.prsLie
        extra = {}
        extra['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        extra['e_index_pop'] = pers.prsLie.getPopulationIndex()
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if succes == -1:
            return returnedFieldList.pop(), returnedFieldList
        action = _("Buzz")
        msg =''
        if not succes:
            msg = _("You are trying to launch a rumor but visibly people don't \
care about it.")
        else:
            msg = _("Your rumor seems to interess some people.")
            msgAutres = _("%(male)s seems to buzz.")  % {
                                        'male':pers.prsNomActuel}
            charism = pers.getCharacteristic(BC.CHARISM_ID)
            etiquette = pers.getCharacteristic(BC.ETIQUETTE_ID)
            length = charism.pcrScoreCalcule/2 + etiquette.pcrScoreCalcule/2 \
                     + random.randint(0,5)
            now = DateTime.gmt() + length*DateTime.oneWeek
            date = now.strftime("%Y-%m-%d")
            now = DateTime.gmt().strftime("%Y-%m-%d")
            title = textToHTML(fields['c_title'])
            txt = textToHTML(fields['c_text'])
            language = None
            if config.LANGUAGES:
                language = int(fields['language'])
                languages = pers.getLanguages()
                languages_id = [lang[0] for lang in languages]
                if language not in languages_id:
                    return _("You have visibly forgot this language..."), \
                           default_return
                else:
                    action += " %s %s" % (_("in"),
                                    languages[languages_id.index(language)][1])
            rum = None
            diff = max((0, (40 - score)/SEGMENT))
            if location.lieCarto:
                radius = charism.pcrScoreCalcule/2 + random.randint(0,5)
                rum = Rumor(rmrNom=title, rmrPrsID=pers.id, rmrContent=txt,
rmrLngID=language, rmrRayon=radius, rmrSctID=None, rmrLieID=location.id,
rmrDateDebut=now, rmrDateFin=date, rmrDiff=diff)
            else:
                rum = Rumor(rmrNom=title, rmrPrsID=pers.id, rmrContent=txt,
rmrLngID=language, rmrRayon=None, rmrSctID=location.lieSctID,
rmrLieID=location.id, rmrDateDebut=now, rmrDateFin=date, rmrDiff=diff)
            msg += '''<p>%s : %s</p><p>%s :</p>%s'''% (_("Title"), title,
                                                       _("Details"), txt)
            pers.actionFile.writeAction(pers.isNear(), ActionFile.FEEDBACK_ID,
                                        msgAutres, action, " ", None)
        pers.actionFile.writeAction([], ActionFile.TALK_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        return msg, None

    def getForm(self, pers, fields):
        formFields = {'title':_('Title'), 'text':_('Text'),
                      'validate':_('Send'), 'costAP':True, 'costFP':True}
        if not config.LANGUAGES:
            self._tpl.getForm = self._tpl.getFormSimple
            return BaseAction.getForm(self, pers, fields, formFields)
        else:
            formFields['language'] = _('Language')
            avail_language = ''
            selected_id = 0
            if fields.has_key('language'):
                selected_id = int(fields['language'])
            for language in pers.getLanguages():
                selected = ''
                if language[0] == selected_id:
                    selected = " selected='selected'"
                avail_language += "<option value='%d'%s>%s</option>" % (
                                      language[0], selected, language[1])
            formFields['avail_language'] = avail_language
            self._tpl.getForm = self._tpl.getFormLanguage
            return BaseAction.getForm(self, pers, fields, formFields)

class GetRumors(BaseAction):
    '''
    Get rumors.
    '''
    _dbAction = action.getByName('getBuzz')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

    def do(self, pers, fields, session, menu, extra={}):
        action = _("Get the buzz")
        rumors = Rumor.getAvailableRumors(pers)
        msg = ""
        if not rumors:
            msg = _("There is no new rumor available.")
        else:
            extra, extra_base = [], {}
            extra_base['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
            extra_base['e_index_pop'] = pers.prsLie.getPopulationIndex()
            for rumor in rumors:
                ext = {}
                ext.update(extra_base)
                diff = rumor.rmrDiff
                if config.LANGUAGES:
                    if rumor.rmrLngID not in [lang[0]
                                            for lang in pers.getLanguages()]:
                        diff += 1000
                ext['e_diff_rumeur'] = diff
                extra.append(ext)
            succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                                 session, extra)
            if succes == -1:
                return returnedFieldList.pop(), returnedFieldList
            res = [suc for suc in succes if suc]
            if not res:
                msg = _("You are listening to rumors but nothing come to your \
ears.")
            else:
                msg = _("There are some news...")
                for idx, rumor in enumerate(rumors):
                    if not succes[idx]:
                        continue
                    msg += "<p>%s : %s</p>" % ( _("Author"),
                                personnage.get(rumor.rmrPrsID).prsNomActuel)
                    if config.LANGUAGES:
                        msg += "<p>%s : %s</p>" % ( _("Language"),
                                    Characteristic.get(rumor.rmrLngID).crcNom)
                    msg += '''<p>%s : %s</p><p>%s :</p>%s'''% (_("Title"),
                                rumor.rmrNom, _("Details"), rumor.rmrContent)
                    sql = 'insert into rumeur_ecoute (rmc_rmr_id, \
rmc_prs_id) values (%d,%d);' % (rumor.id, pers.id)
                    Rumor._connection.query(sql)
        pers.actionFile.writeAction([], ActionFile.TALK_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        return msg, None

    def getForm(self, pers, fields):
        formFields = {'validate':_('Do it'), 'costAP':True, 'costFP':True}
        form = BaseAction.getForm(self, pers, fields, formFields)
        return form

from templates.actions import tplFollow
class Follow(BaseAction):
    '''
    Follow someone.
    '''
    _dbAction = action.getByName('follow')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplFollow
    _baseFields = ["shapes"]

    def do(self, pers, fields, session, menu, extra={}):
        action = _("Following somenone")
        msg = ""
        base_fields = self._baseFields
        for field in base_fields:
            if not fields.has_key(field) or not fields[field]:
                return _("There are empty fields."), fields
        fromDate = None
        if FOLLOW_HOURS_DELTA:
            fromDate = DateTime.now() - DateTime.oneHour * FOLLOW_HOURS_DELTA
        route = Route.getAvailableRouteToFollow(pers, int(fields['shapes']),
                                                fromDate=fromDate)
        if not route:
            return _("There is no route available to follow this character."), \
                  fields
        follow = None
        extra = {}
        extra['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        extra['e_index_pop'] = pers.prsLie.getPopulationIndex()
        extra['e_taille_lieu'] = pers.prsLie.getSizeIndex()
        if Tailing.getFromCharacters(pers.id, int(fields['shapes']),
                                     FOLLOW_TAIL):
            follow = True
            succes = True
        if not follow:
            succes, score, returnedFieldList = BaseAction.do(self, pers,
                                                        fields, session, extra)
        if succes == -1:
            msg = _('Error')
            if returnedFieldList:
                msg = returnedFieldList.pop()
            return msg, returnedFieldList
        character = personnage.get(forme.get(int(fields['shapes'])).frmPrsID)
        if not succes:
            msg = _("You try to see where %s has gone but you fail.") % \
                                                        character.prsNomActuel
        else:
            destination = None
            finish_location = lieu.get(route.itnArriveeLieID)
            if pers.prsLie.lieCarto and finish_location.lieCarto:
                destination = pers.getDirection(
               (lieu_carto.getByLocation(pers.prsLieID).getCoordinates()),
               (lieu_carto.getByLocation(finish_location.id).getCoordinates()))
            else:
                paths, smallRoads = chemin.getRoads(pers, None, True, True)
                paths += smallRoads
                hidden_roads = chemin.getHiddenRoads(pers, True)
                location_ids = []
                for p in paths:
                    if route.itnArriveeLieID in (p.chmArriveeLieID,
                                                 p.chmDepartLieID):
                        destination = lieu.get(route.itnArriveeLieID).label()
                        if p.id in hidden_roads:
                            acces_connu(accPrsID=pers.id, accChmID=p.id)
                if not destination:
                    destination = _("to an undefined destination")
            msg = _("%s has gone %s.") % (character.prsNomActuel,
                                             destination)
            if not follow:
                Tailing.create(pers.id, character.getCurrentShape().id)
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                    _("Follow"))
        menu.evaluateMenu(session, pers.id)
        return msg, None

    def getForm(self, pers, fields):
        '''
        Choose the available character to follow. A character can be follow if
        there is just a little time is gone otherwise the character have to be
        tracked.
        '''
        formFields = {'validate':_('Do it'), 'follow': _("Follow"),
                      'costAP':True, 'costFP':True}
        fromDate = None
        if FOLLOW_HOURS_DELTA:
            fromDate = DateTime.now() - DateTime.oneHour * FOLLOW_HOURS_DELTA
        frms = Route.getAvailableRouteToFollow(pers, fromDate=fromDate)
        options = ''
        near_shapes = [char.getCurrentShape().id for char in pers.isNear(True)]
        for shape_id in frms:
            if shape_id not in near_shapes:
                shape = forme.get(shape_id)
                character = personnage.get(shape.frmPrsID)
                options += "<option value='%d'>%s</option>" % (shape_id,
                                                        character.prsNomActuel)
        formFields['shapes'] = options
        form = BaseAction.getForm(self, pers, fields, formFields)
        return form

from templates.actions import tplSearchSteps
class SearchSteps(BaseAction):
    '''
    Search steps
    '''
    _dbAction = action.getByName('search_steps')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplSearchSteps
    _baseFields = []

    def do(self, pers, fields, session, menu, extra={}):
        action = _("Search for steps")
        msg = ""
        base_fields = self._baseFields
        for field in base_fields:
            if not fields.has_key(field) or not fields[field]:
                return _("There are empty fields."), fields
        if fields.has_key('rename'):
            msg = "%s : %s\n" % (_("In"), pers.prsLie.label())
            if fields.has_key('comment') and fields['comment']:
                msg += fields['comment'] + "\n"
            for key in fields.keys():
                if 'toward_' in key:
                    id = int(key.strip('toward_'))
                    towards = fields['toward_%d'%id]
                    if type(towards) != list:
                        towards = [towards]
                    ks = KnownedSteps.get(id)
                    if fields['name_%d'%id]:
                        ks.trcNomDonne = fields['name_%d'%id]
                    for toward in towards:
                        msg += "%s : %s, %s : %s, %s : %d, %s : %d, %s : %s\n" \
                               % (_("Steps"), ks.trcNomDonne, _("type"),
                                  StepType.get(ks.trcTtrID).ttrNom, _("size"),
                                  ks.trcTaille, _("deep"), ks.trcProfondeur,
                                  _("toward"), toward)
            msg = textToHTML(msg)
            pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                        _("Search for steps"))
            menu.evaluateMenu(session, pers.id)
            return msg, None
        routes = Route.getAvailableStepsToFollow(pers)
        extra, extra_base = [], {}
        extra_base['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        extra_base['e_index_pop'] = pers.prsLie.getPopulationIndex()
        current_shape_id = pers.getCurrentShape().id
        for route in routes:
            ext = {}
            ext.update(extra_base)
            ext['e_nb_max_trace_rest'] = route.itnTracesRestantes
            if route.itnFrmID == current_shape_id:
                ext['e_nb_max_trace_rest'] += 10
            extra.append(ext)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if succes == -1:
            succes = [-1]
        res = [suc for suc in succes if suc and suc != -1]
        steps = []
        if routes:
            for id in xrange(len(succes)):
                if not succes[id]:
                    continue
                steps.append(routes[id])
        if not steps or not res:
            msg = _("You try to identify somes steps but you failed.")
            pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                        _("Search for steps"))
            menu.evaluateMenu(session, pers.id)
            return msg, None
        identified_steps = []
        for step in steps:
            Tailing.create(pers.id, step.itnFrmID, TRACK_TAIL)
            start, end = step.itnDepartLie, step.itnArriveeLie
            reverse = False
            if pers.prsLie == step.itnArriveeLie:
                end, start = step.itnDepartLie, step.itnArriveeLie
                reverse = True
            toward = ''
            if start.lieCarto and end.lieCarto:
                toward = pers.getDirection(
              (lieu_carto.getByLocation(start.id).getCoordinates()),
              (lieu_carto.getByLocation(end.id).getCoordinates()))
            else:
                toward = end.label()
            knowed_step = KnownedSteps.create(pers.id, step)
            if step.itnFrmID == current_shape_id:
                knowed_step.trcNomDonne = pers.label()
            identified_steps.append((knowed_step.id, toward, reverse))
        menu.evaluateMenu(session, pers.id)
        return msg, {'steps':identified_steps}

    def getForm(self, pers, fields):
        '''
        First step is only a simple validation to start to identify steps.
        The second step is to give a name/to rename the identified steps.
        '''
        if not fields.has_key('steps'):
            formFields = {'costAP':True, 'costFP':True}
            return BaseAction.getForm(self, pers, fields, formFields)
        steps = []
        content = ''
        auto_names = {}
        currentId = 1
        for step_id, toward, reverse in fields['steps']:
            kn = KnownedSteps.get(int(step_id))
            if kn.trcPrsID != pers.id:
                continue
            tpl = self._tpl.getStepLine
            name = ""
            if kn.trcNomDonne:
                name = kn.trcNomDonne
                if auto_names.has_key(kn.id):
                    tpl = self._tpl.getKnowedStepLine
                else:
                    auto_names[kn.id] = name
            else:
                if auto_names.has_key(kn.id):
                    name = auto_names[kn.id]
                    tpl = self._tpl.getKnowedStepLine
                else:
                    name = _("Steps") + " %d" % currentId
                    auto_names[kn.id] = name
                    currentId += 1
            dct = {'name':name, 'type':StepType.get(kn.trcTtrID).ttrNom,
'size':kn.trcTaille, 'deep':kn.trcProfondeur, 'id':kn.id,
'toward':toward.replace('"', '')}
            dct['to_from'] = _('To')
            if reverse:
                dct['to_from'] = _('From')
            content += tpl % dct
        formFields = {'name':_("Name"), 'type':_("Type"), 'size':_("Size"),
'deep':_("Deep"), 'to_from':_("To/From"), 'toward':_("Toward"), 'steps':content,
'comment':_("Comment"), "help":_("To save theses steps in the Action File validate")}
        return BaseAction.getForm(self, pers, fields, formFields,
                                  self._tpl.getFormName)


from templates.actions import tplShowWay
class ShowWay(BaseAction):
    '''
    Let some people tailing you.
    '''

    _dbAction = action.getByName('show_way')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm
    _tpl = tplShowWay

    def do(self, pers, fields, session, menu, extra={}):
        '''
        Update the list of characters that can follow you.
        '''
        returnedFieldList = []
        BaseAction.do(self, pers, fields, session, extra)
        action = _("Show the way")
        followers = []
        for key in fields.keys():
            if 'character_' in key:
                followers.append(int(key.strip('character_')))
        tails = Tailing.getFromCharacters(None, pers.getCurrentShape().id,
                                          FOLLOW_TAIL)
        not_followers, c_followers, n_followers = [], [], []
        for tail in tails:
            char = personnage.get(tail.fltSuivantPrsID)
            if char.id not in followers:
                not_followers.append(char)
                tail.destroySelf()
            else:
                c_followers.append(char)
                followers.remove(char.id)
        for follower in followers:
            n_followers.append(personnage.get(follower))
            Tailing.create(follower, pers.getCurrentShape().id, FOLLOW_TAIL)
        msg = ""
        if n_followers:
            msg = _("You show the way for these new characters : %s\n") % \
                  ", ".join([char.label() for char in n_followers])
            pers.actionFile.writeAction(n_followers, ActionFile.FEEDBACK_ID,
                            _("%s invites you to follow him") % pers.label(),
                            action, " ", None)
        if c_followers:
            msg += _("You already show the way to theses characters : %s\n") % \
                   ", ".join([char.label() for char in c_followers])
        if not_followers:
            msg += _("You stop showing the way to theses characters : %s") % \
                   ", ".join([char.label() for char in not_followers])
            pers.actionFile.writeAction(not_followers, ActionFile.FEEDBACK_ID,
                    _("%s does not show you the way anymore") % pers.label(),
                    action, " ", None)
        msg = textToHTML(msg)
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList

    def getForm(self, pers, fields):
        '''
        Show characters that's currently following the main character (checked
        by default) and characters that are present in the location.
        '''
        formFields = {}
        characters = ''
        formFields['message'] = _("Characters that can follow you")
        tails = Tailing.getFromCharacters(None, pers.getCurrentShape().id,
                                          FOLLOW_TAIL)
        chars = []
        for tail in tails:
            char = personnage.get(tail.fltSuivantPrsID)
            chars.append(char.id)
            characters += tplShowWay.characterLine % {'id':char.id, \
                    'checked':" checked='checked'", 'character':char.label()}
        formFields['characters'] = characters
        for char in pers.isNear():
            if char.id in chars:
                continue
            characters += tplShowWay.characterLine % {'id':char.id, \
                    'checked':"", 'character':char.label()}
        formFields['characters'] = characters
        return BaseAction.getForm(self, pers, fields, formFields)

class LookExactly(BaseAction):
    '''
    Look the location for more details.
    '''
    _dbAction = action.getByName('look_exactly')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

    def do(self, pers, fields, session, menu, extra={}):
        action = _("Looking exactly")
        dls = description_lieu.getFromLocation(pers.prsLieID)
        extra, extra_base = [], {}
        extra_base['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        for dl in dls:
            ext = {}
            ext.update(extra_base)
            ext['e_diff_descro'] = dl.dslDifficulte
            extra.append(ext)

        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if succes == -1:
            return returnedFieldList.pop(), returnedFieldList
        res = [suc for suc in succes if suc]
        if not res or not dls:
            msg = _("After a long search nothing more comes to you")
        else:
            msg = _("A long search give you these elements:")
            descs = []
            dct = {'pers':pers.label()}
            for idx, dl in enumerate(dls):
                if not succes[idx]:
                    continue
                descs.append(dl.label() % dct)
            msg += "<br/>" + "<br/>".join(descs)
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                        _("Looking exactly"))
        menu.evaluateMenu(session, pers.id)
        return msg, {}

    def getForm(self, pers, fields):
        formFields = {'costAP':True, 'costFP':True}
        return BaseAction.getForm(self, pers, fields, formFields)

class SearchPath(BaseAction):
    '''
    Search for an hidden path
    '''
    _dbAction = action.getByName('search_path')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

    def do(self, pers, fields, session, menu, extra={}):
        action = _("Search for a path")
        paths = chemin.getHiddenRoads(pers)
        extra, extra_base = [], {}
        extra_base['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        for path in paths:
            ext = {}
            ext.update(extra_base)
            ext['e_diff_path'] = path.chmDiscretion
            extra.append(ext)
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        msg = _("After a long search you found no new path")
        res = [suc for suc in succes if suc]
        if paths and res:
            msg = _("You found one (or several) new paths")
            for idx, path in enumerate(paths):
                if not succes[idx]:
                    continue
                acces_connu(accPrsID=pers.id, accChmID=path.id)
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action,
                                        _("Search for a path"))
        menu.evaluateMenu(session, pers.id)
        return msg, {}

    def getForm(self, pers, fields):
        formFields = {'costAP':True, 'costFP':True, 'validate':_("Search")}
        return BaseAction.getForm(self, pers, fields, formFields)

class Dig(BaseAction):
    '''
    Dig a location.
    '''

    _dbAction = action.getByName('dig')
    _name = _(_dbAction.actNom)
    _id = _dbAction.actNomForm

    def isAvailable(self, pers, extra={}):
        """
        This action is available when you have Action Point and health points
        """
        self._available, reason = BaseAction.isAvailable(self, pers, extra)
        return self._available, reason

    def getForm(self, pers, fields):
        formFields = {'validate':_("Dig the current location"),
                      'costAP':True}
        return BaseAction.getForm(self, pers, fields, formFields)

    def do(self, pers, fields, session, menu, extra={}):
        extra['e_carac_evn'] = pers.prsLie.getEnvironementBonus(pers)
        extra['e_taille_lieu'] = pers.prsLie.lieIndiceTaille
        if not extra['e_taille_lieu']:
            extra['e_taille_lieu'] = 0
        location = pers.prsLie
        items = location.getObjects(character_id=pers.id, invisible=True)
        extra['e_taille_objet'] = 0
        if items:
            extra['e_taille_objet'] = max([item.getSize() for item in items])
        succes, score, returnedFieldList = BaseAction.do(self, pers, fields,
                                                         session, extra)
        if succes == -1:
            return returnedFieldList.pop(), returnedFieldList
        if not self._available :
            return succes, returnedFieldList
        action = _("Dig")
        msg = ""
        items = [item for item in items
                 if (extra['e_taille_objet']-item.getSize())*SEGMENT <= score]
        if not succes or not items:
            msg = _("After a long search, you found nothing in %s.") % \
                  location.label()
        else:
            msg = _("You have found %s in %s.")  % (
                ",".join([item.label() for item in items]), location.label())
            for item in items:
                VisibleItem(elvPrsID=pers.id, elvElmID=item.id)
        pers.actionFile.writeAction([], ActionFile.ACTION_ID, msg, action)
        menu.evaluateMenu(session, pers.id)
        return msg, returnedFieldList
