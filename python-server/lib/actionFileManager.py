#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : actionFileManager.py - CREATION : 2005/07/31
#
# Copyright (C) 2005-2010  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Action file manager

author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

import plugins.configuration as config
from mx import DateTime
import os
import codecs
import shutil
from xml.dom import minidom
from Ft.Xml.Xslt import Processor
from Ft.Xml import InputSource
from utils import _

ACTION_FILE_TEMPLATE = u"<action_file lastReadId='0'><startTime><date>%s</date></startTime></action_file>"

class ActionFile:
    '''
    File used to store the result action of a character and actions of other
    characters he is aware of.      
    '''
    FILE_PREFIX = "actionFile_"
    READ = 0
    READ_FOR_DISPLAY = -1
    WRITE = 1
    TALK_ID = 'talk'
    ACTION_ID = 'action'
    FIGHT_ID = 'fight'
    FEEDBACK_ID = 'feedback'
    ALL_ID = 'all'
    MAIL_ID = 'mail'
    xsl_files = {ALL_ID:config.XSL_PATH + 'action-file_all.xsl',
    TALK_ID:config.XSL_PATH + 'action-file_talk.xsl',
    FIGHT_ID:config.XSL_PATH + 'action-file_fight.xsl',
    ACTION_ID:config.XSL_PATH + 'action-file_actions.xsl',
    FEEDBACK_ID:config.XSL_PATH + 'action-file_feedback.xsl',
    MAIL_ID:config.XSL_PATH + 'action-file_mail.xsl'}

    def __init__(self, character_id, character_name, file_name=''):
        '''
        ActionFile(character_id)
            - character_id : id of the character
            - character_name : name of the character
        '''
        self.character_id, self.character_name = character_id, character_name
        if file_name:
            self.file_name = file_name
        else:
            self.file_name = config.ACTION_FILE_PATH + ActionFile.FILE_PREFIX +\
                             str(self.character_id) + ".xml"
        self.backup_file_name = config.ACTION_FILE_PATH + "old/%s-" + \
              ActionFile.FILE_PREFIX + str(self.character_id) + ".xml"
        if not os.path.exists(config.ACTION_FILE_PATH + "old/"):
            os.makedirs(config.ACTION_FILE_PATH + "old/")
        self.mode = 0
        self.dom_write = None
        if self.dom_write:
            self.appendNodeAction = self.appendNodeActionXml
        else:
            self.appendNodeAction = self.appendNodeActionFile

    def __str__(self):
        name = self.file_name.split('/')[-1]
        if name.startswith('actionFile'):
            return _("Current")
        date = name.split('T')[0].split('-')
        date.reverse()
        return "/".join(date)

    def getStrDate(self):
        name = str(self).split('/')
        name.reverse()
        return "-".join(name)

    @classmethod
    def getActionFileByDate(cls, character, date):
        """
        Get action file by character and date
        """
        path = config.ACTION_FILE_PATH + 'old/'
        file_names = [file_name for file_name in os.listdir(path) \
                        if file_name.endswith('_%d.xml' % character.id)]
        for file_name in file_names:
            if file_name.startswith(date):
                return cls(character.id, character.label(), 
                           config.ACTION_FILE_PATH + 'old/' + file_name)

    @classmethod
    def getActionFiles(cls, character):
        """
        Get all action files
        """
        path = config.ACTION_FILE_PATH + 'old/'
        file_names = [file_name for file_name in os.listdir(path) \
                        if file_name.endswith('_%d.xml' % character.id)]
        file_names.sort()
        file_names.reverse()
        return [cls(character.id, character.label())] + \
 [cls(character.id, character.label(), config.ACTION_FILE_PATH + 'old/' + name)\
                                                         for name in file_names]

    def createActionFile(self):
        '''initialise the action file'''
        backup_file_name = self.backup_file_name % \
                                    DateTime.gmt().strftime("%Y-%m-%dT%H-%M-%S")
        if os.path.exists(self.file_name):
            shutil.copyfile(self.file_name, backup_file_name)
        self.action_file_string = ACTION_FILE_TEMPLATE % self.getNow()
        self.document = minidom.parseString(self.action_file_string.encode('utf-8')) 
        fd = codecs.open(self.file_name, 'w', 'utf-8')
        fd.write(self.document.toxml('utf-8'))
        fd.close()

    def openActionFile(self, mode):
        '''
        Define the DOM Object for the current action file, return True if the action
        file is already open
        '''
        if not os.path.isfile(self.file_name):
            self.createActionFile()
            return
        if mode == self.WRITE:
            self.mode += mode
        if not self.dom_write and (not hasattr(self, 'document') or not self.document):
            self.document = minidom.parseString((ACTION_FILE_TEMPLATE % self.getNow()).encode('utf-8'))
        if mode == self.READ_FOR_DISPLAY or not self.dom_write:
            if not hasattr(self, 'action_file_string') or not self.action_file_string:
                fd = codecs.open(self.file_name, 'r', 'utf-8')
                self.action_file_string = fd.read()
                fd.close()
                return
            else:
                return True
        if not hasattr(self, 'document') or not self.document:
            fd = codecs.open(self.file_name, 'r', 'utf-8')
            self.action_file_string = fd.read()
            fd.close()
            self.document = minidom.parseString(self.action_file_string)
        else:
            return True

    def closeActionFile(self):
        '''if mod is set to WRITE, write the current action file to a file on the HD.
        Close the DOM Object'''
        if not self.dom_write and self.mode >= self.WRITE:
            fd = codecs.open(self.file_name, 'w', 'utf-8')
            fd.write(self.action_file_string)            
            fd.close()
            self.action_file_string = None
        elif hasattr(self, 'document') and self.document:
            if self.mode >= self.WRITE:
                fd = codecs.open(self.file_name, 'w', 'utf-8')
                fd.write(self.document.toxml('utf-8'))
                fd.close()
            self.mode = 0
            self.document.unlink()
            self.document = None
            self.action_file_string = None
        
    def getNow(self):
        '''return current time in a well formated string'''
        now = DateTime.gmt()
        return now.strftime("%Y-%m-%dT%H:%M:%S")
        
    def getLastId(self):
        '''get the last action id'''
        opened = self.openActionFile(self.READ)
        if self.dom_write:
            actions = self.document.getElementsByTagName('action')
            # the last inserted action has the higher id
            last_id = 0
            if len(actions) > 0:
                last_action = actions[len(actions) - 1]
                last_id = last_action.getAttribute('id')
        else:
            last_id = 0
            if "<action id=" in self.action_file_string:
                i = self.action_file_string.rfind("<action id=") + 12
                last_id = self.action_file_string[i]
                while self.action_file_string[i+1] != '"':
                    i += 1
                    last_id += self.action_file_string[i]
        # close the action file if not already opened
        if not opened:
            self.closeActionFile()
        return int(last_id)
        
    def getLastReadId(self):
        '''get the last read id'''
        opened = self.openActionFile(self.READ)
        lastReadIdTag = self.document.getElementsByTagName('action_file')[0]
        lastReadId = lastReadIdTag.getAttribute('lastReadId')
        if not lastReadId:
            raise 'InvalidXml', "lastReadId attribute is not available"
        # close the action file if not already opened
        if not opened:
            self.closeActionFile()
        return int(lastReadId)
        
    def setAsRead(self):
        '''set all actions as read by the player'''
        opened = self.openActionFile(self.WRITE)
        last_id = self.getLastId()
        lastReadIdTag = self.document.getElementsByTagName('action_file')[0]
        lastReadIdTag.setAttribute('lastReadId', str(last_id))
        # close the action file if not already opened
        if not opened:
            self.closeActionFile()
        
    def createTextElement(self, element_name, element_text):
        data = u'<%s>%s</%s>' % (element_name, element_text.decode('utf-8'),
                                     element_name)
        node = minidom.parseString(data.replace('<br>','').encode('utf-8'))
        return node.childNodes[0]
        
    def display(self, time_delta="PT0H", style=None):
        xsl_file = None
        if not self.xsl_files.has_key(style):
            xsl_file = self.xsl_files['default']
        else :
            xsl_file = self.xsl_files[style]
        opened = self.openActionFile(self.READ_FOR_DISPLAY)
        xsl_af = codecs.open(xsl_file, 'r', 'utf-8')
        transform = xsl_af.read()
        xsl_af.close()
        processor = Processor.Processor()
        transform = InputSource.DefaultFactory.fromString(
                                        transform.encode('utf-8'), 
                                        "http://spam.com/identity.xslt")
        action_file_string = self.action_file_string.encode('utf-8')
        action_file_string = action_file_string.replace('<<', '<')
        source = InputSource.DefaultFactory.fromString(action_file_string,
                                        "http://spam.com/doc.xml")
        processor.appendStylesheet(transform)
        params = {'time_delta':time_delta}
        result = processor.run(source, topLevelParams=params)

        if not opened:
            self.closeActionFile()
        return result

    def writeAction(self, action_targets, type, msg, action_name, 
action_heading=" ", myself=True, anonymous=False, others_visible=False,
all_action_target=None):
        '''
        Generate action message
    
        ActionFile.writeAction(self, action_targets, type, msg, action_name,
        action_heading="", myself=True, anonymous=False, others_visible=True)
            - action_targets : personnage [], target characters
            - type : string, type of the action
            - action_name : string, action summary
            - action_heading : string, a little more detail
            - msg : string, action in detail
            - myself : set to true if visible in the subject action file
            - anonymous : set to true if the subject is anonymous
            - others_visible : subjects concerned by the action are visible
            - all_action_target : if not None set the specific group of
character that seem to be concerned by the action
        '''
        mode = self.READ
        if myself:
            mode = self.WRITE
        opened = self.openActionFile(mode)
        
        #action element
        actionNode = self.document.createElement('action')
        
        date = self.getNow()
        actionNode.appendChild(self.createTextElement('date', date))
        
        actionNode.appendChild(self.createTextElement('name', action_name))
        actionNode.appendChild(self.createTextElement('type', type))
        
        # subjects elements
        if not anonymous:
            subjectNode = self.document.createElement('subject')
            subjectNode.setAttribute('id', str(self.character_id))
            subjectNode.appendChild(self.createTextElement('name', self.character_name))
            actionNode.appendChild(subjectNode)
        
        #targets elements
        if others_visible:
            targets = []
            act_target = action_targets
            if all_action_target:
                act_target = all_action_target
            for target in act_target:
                if target.id != self.character_id:
                    targetNode = self.document.createElement('target')
                    targetNode.setAttribute('id', str(target.id))
                    targetNode.appendChild(self.createTextElement('name', target.prsNomActuel))
                    targets.append(targetNode)
            for target in targets:
                actionNode.appendChild(target)
        
        actionNode.appendChild(self.createTextElement('heading', action_heading))
        if '<p>' not in msg:
            msg = '<p>' + msg + '</p>'
        actionNode.appendChild(self.createTextElement('details', msg))
        
        
        actionNodeClone = None
        if myself:
            if action_targets:
                actionNodeClone = actionNode.cloneNode(True)
            self.appendNodeAction(actionNode)
            if action_targets:
                actionNode = actionNodeClone
        for target in action_targets:
            actionNodeClone = actionNode.cloneNode(True)
            target.actionFile.appendNodeAction(actionNode)
            actionNode = actionNodeClone
        if not opened:
            self.closeActionFile()

    def appendNodeActionXml(self, actionNode):
        opened = self.openActionFile(self.WRITE)
        # FIXME : atomicité de la gestion de l'id
        actionNode.setAttribute('id', str(self.getLastId() + 1))
        self.document.getElementsByTagName('action_file')[0].appendChild(actionNode)
        # close the action file if not already opened
        if not opened:
            self.closeActionFile()
    
    def appendNodeActionFile(self, actionNode):
        date = DateTime.now()
        opened = self.openActionFile(self.WRITE)
        # FIXME : atomicité de la gestion de l'id
        actionNode.setAttribute('id', str(self.getLastId() + 1))
        self.action_file_string = self.action_file_string[0:-14]
        self.action_file_string += unicode(actionNode.toxml('utf-8'), 'utf-8')
        self.action_file_string += u"</action_file>"
        #self.document.getElementsByTagName('action_file')[0].appendChild(actionNode)
        # close the action file if not already opened
        if not opened:
            self.closeActionFile()
