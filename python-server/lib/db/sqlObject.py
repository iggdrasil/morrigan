#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : sqlObject.py - CREATION : 2005/02/12
#
# Copyright (C) 2005  Etienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Adding functions to SQLObject.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from sqlobject.postgres.pgconnection import PostgresConnection
from sqlobject import sqlhub, MixedCaseUnderscoreStyle
import plugins.configuration as config

class MorriganStyle(MixedCaseUnderscoreStyle):
    '''
    A database style for SQLObject adapted to the morrigan database
    '''

    def instanceAttrToIDAttr(self, attr):
        if not attr.upper().endswith('ID'):
            return attr + 'ID'
        return attr


# old sqlobject pg connector use 'passwd' and new use 'password'
try:
    conn = PostgresConnection(user=config.BDD_USER, db=config.BDD_DB,
host=config.BDD_HOST, password=config.BDD_PASSWORD)
except:
    conn = PostgresConnection(user=config.BDD_USER, db=config.BDD_DB,
host=config.BDD_HOST, passwd=config.BDD_PASSWORD)

conn.style = MorriganStyle()

sqlhub.processConnection = conn
