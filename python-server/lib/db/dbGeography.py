#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################
# PROJECT : Morrigan - FILE : dbGeography.py - CREATION : 2005/09/10
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Mapping of database objects directly related to the world and his
geography.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
from sqlobject.main import SQLObject
from sqlobject.col import ForeignKey
import sqlObject
from mx import DateTime

import plugins.configuration as config
from dbInventory import Item, VisibleItem, coffre_element
from dbCommon import image

from constants import MIN_VISIBLE_SIZE
from utils import _, hex2RGB, MorriganError, evalFormula

POSTGIS_POINT_TPL = "GeomFromText('POINT(%d %d)', -1)"

class Season(SQLObject):
    """Season

    Main attributes :
    id (integer) -- id
    ssnNom (string) -- name
    ssnOrdre (integer) -- order of the season
    ssnDefault (boolean) -- current default season
    """
    class sqlmeta:
        idName = 'ssn_id'
        fromDatabase = True
        table = 'saison'
        style = sqlObject.MorriganStyle()

    def getCurrentSaisonId():
        """Get the current season
        """
        try:
            season_id = Season.select('"ssn_default"=True')[0].id
        except IndexError:
            raise MorriganError, "There is no default season set \
in database"
        return season_id
    getCurrentSaisonId = staticmethod(getCurrentSaisonId)

class monde(SQLObject):
    """A world of the game.

    Main attributes :
    id (integer) -- world's id,
    mndNom (string) -- name of the world,
    mndTmnID (integer) -- type of the world,
    mndDescription (text) -- world's description,
    mndPereMndID (integer) -- id of the father world,
    mndPerePoint (point) -- point in the father world map,
    mndNiveau (integer) -- value to represent the importance of the world,
    mndMldID (integer) -- id of effect applied to this world
    mndImage (string) -- image of the world
    """
    class sqlmeta:
        idName = 'mnd_id'
        fromDatabase = True
        table = 'monde'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.mndNom

class lieu(SQLObject):
    """Location.

    Main attributes :
    id (integer) -- id
    lieTliID (integer) -- location type
    lieNom (string) -- name
    lieSctID (integer) -- sector (id)
    lieFacteurEffacementTrace (float) -- steps erasing factor
    lieFacteurNbTrace (float) -- steps number factor
    lieCarto (boolean) -- True if the location is a geaographic location
    lieImg -- image associated to the location
    """
    CURRENT_SEASON = Season.getCurrentSaisonId()
    class sqlmeta:
        idName = 'lie_id'
        fromDatabase = True
        table = 'lieu'
        style = sqlObject.MorriganStyle()
    lieSct = ForeignKey('secteur')
    lieTli = ForeignKey('type_lieu')
    lieImg = ForeignKey('image')

    def label(self):
        '''Get the label for the location.
        '''
        if not self.lieCarto:
            return self.lieNom
        label = lieu_carto.getByLocation(self.id).label()
        if self.lieNom:
            label += " - " + self.lieNom
        return label

    def getDescription(self, pers=None):
        """Get the description of the location depending of the
        current season.

        Return :
        (list) -- get tuples with name and description of the
            location depending of the current season.
        """
        desc = []
        dct = {}
        if pers:
            dct['pers'] = pers.label()
        if self.lieDesc:
            name = self.lieNom
            if not self.lieNom:
                name = _("Lieu")
            desc = [(name, self.lieDesc % dct)]
        if self.lieCarto:
            for lc in lieu_carto.getByLocation(self.id).getDescription(pers):
                desc.append((lc[0] % dct, lc[1]))
        return desc

    def getObjects(self, character_id=None, invisible=None,
                   into_chest=False):
        '''
        Get objects of the location.

            character_id -- if not None return objects visible for this
        character or invisible to this character if invisible set to True
        '''
        items = []
        for item in list(Item.select('elm_lie_id=%d' % self.id)):
            if not item.loader or not item.loader.rchUtilise:
                if not into_chest:
                    ces = list(coffre_element.select('cel_elm_id=%d' % item.id))
                    if ces and ces[0]:
                        continue
                if not character_id:
                    items.append(item)
                    continue
                size = item.getSize()
                if not size or size >= MIN_VISIBLE_SIZE:
                    if not invisible:
                        items.append(item)
                    continue
                vi = list(VisibleItem.select("elv_prs_id=%d and \
elv_elm_id=%d" % (character_id, item.id)))
                if (vi and not invisible) or (not vi and invisible):
                    items.append(item)
        return items

    def getEraseFactor(self):
        '''Get erase factor
        '''
        erase_factor = self.lieFacteurEffacementTrace
        if self.lieCarto:
            lc = lieu_carto.getByLocation(self.id)
            erase_factor = erase_factor*lc.getEraseFactor()
        return erase_factor

    def getStepsNumberFactor(self):
        '''Get steps number factor
        '''
        steps_factor = self.lieFacteurNbTrace
        if self.lieCarto:
            lc = lieu_carto.getByLocation(self.id)
            steps_factor = steps_factor*lc.getStepsNumberFactor()
        return steps_factor

    def getRumorDifficulty(self, character):
        '''Get rumor difficulty for a designed character
        '''
        if self.lieCarto:
            lic = lieu_carto.getByLocation(self.id)
            return lic.getRumorDifficulty(character)
        else:
            return self.lieSct.sctDifficulteRumeur

    def getEnvironementBonus(self, character):
        """Get the environnemental bonus for a character
        """
        if self.lieSct and self.lieSct.sctTev:
            c =  character.getCharacteristic(self.lieSct.sctTev.tevCrcID)
            return c.pcrScoreCalcule
        lc = lieu_carto.getByLocation(self.id)
        bonus, characteristics = lc.getCharacteristicBonus(character.id, {},
                                                           True)
        return bonus

    def getPopulationIndex(self):
        """Get the population index
        """
        idx = self.lieIndicePopulation
        if not idx and self.lieSctID:
            idx = self.lieSct.sctIndicePopulation
        if not idx:
            idx = 10
        return idx

    def getSizeIndex(self):
        """Get the size index
        """
        idx = self.lieIndiceTaille
        if not idx:
            idx = self.getPopulationIndex() * 10
        return idx

    def destroySelf(self):
        '''Destroy associated items to
        '''
        if self.lieCarto:
            raise "A cartographic location cannot be destroyed"
        #TODO:
        #lieu_cyclique
        #acces_connu
        #lieu_action
        for dl in description_lieu.getFromLocation(self.id):
            dl.destroySelf()
        for route in Route.getRouteFromLocation(self.id):
            route.destroySelf()
        for road in chemin.getRoadsFromLocation(self.id):
            road.destroySelf()
        SQLObject.destroySelf(self)

class type_lieu(SQLObject):
    """
    Location type
    """
    class sqlmeta:
        idName = 'tli_id'
        fromDatabase = True
        table = 'type_lieu'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tliNom

class acces_connu(SQLObject):
    """
    Location type
    """
    class sqlmeta:
        idName = 'acc_id'
        fromDatabase = True
        table = 'acces_connu'
        style = sqlObject.MorriganStyle()

class description_lieu(SQLObject):
    """Description of a location.

    Main attributes :
    id (integer) -- id
    dslLieID (integer) -- id of the associated location
    dslDifficulte (integer) -- difficulty of the description
    dslDescription (integer) -- description
    """
    CURRENT_SEASON = Season.getCurrentSaisonId()
    class sqlmeta:
        idName = 'dsl_id'
        fromDatabase = True
        table = 'description_lieu'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.dslDescription

    def getFromLocation(location_id):
        '''
        Get all the description of a location
        '''
        return list(description_lieu.select('dsl_lie_id=%d' % location_id))
    getFromLocation = staticmethod(getFromLocation)

class secteur(SQLObject):
    """
    Virtual part of a world attached to him with roads.

    Main attributes:
    id (integer) -- sector's id
    sctNom (string) -- name of the sector
    sctDescription (string) -- description of the sector
    sctTscID (integer) -- type of the sector
    sctChargeMax (integer) -- maximum load
    sctDifficulteRumeur (integer) -- difficulty to launch a rumor
    """
    class sqlmeta:
        idName = 'sct_id'
        fromDatabase = True
        table = 'secteur'
        style = sqlObject.MorriganStyle()
    sctTsc = ForeignKey('type_secteur')
    sctTev = ForeignKey('type_environnement')

    def label(self):
        return self.sctNom

    def destroySelf(self):
        locations = lieu.select('lie_sct_id = %d' % self.id)
        for location in locations:
            location.lieSctID = None
        SQLObject.destroySelf(self)

class SectorType(SQLObject):
    """
    Type of a sector.

    Main attributes:
    id (integer) -- type's id,
    tscNom (string) -- name of the type.
    """
    class sqlmeta:
        idName = 'tsc_id'
        fromDatabase = True
        table = 'type_secteur'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tscNom

class type_environnement(SQLObject):
    """
    Environement type.
    """
    class sqlmeta:
        idName = 'tev_id'
        fromDatabase = True
        table = 'type_environnement'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tevNom

class Layer(SQLObject):
    '''Geographic layers

    Main attributes:
    id (integer) -- Id
    clqNom (string) -- Name
    clqDescription (string) -- Description
    clqFacteurEffacementTrace (float) -- steps erase factor
    clqFacteurNbTrace (float) -- steps number factor
    clqCoutPn (integer) -- daily food point cost
    clqCoutAccesPn (integer) -- acces food point cost
    clqCoutPv (integer) -- daily life point cost
    clqCoutAccesPa (integer) -- acces action point cost
    clqCouleur (integer) -- color code
    '''
    class sqlmeta:
        idName = 'clq_id'
        fromDatabase = True
        table = 'calque'
        style = sqlObject.MorriganStyle()

# a change of the world dimensions needs a restart of the service
WORLD_DIMS = {}
for wrld in list(monde.select("1=1")):
    sql = "select max(X(lic_localisation)), max(Y(lic_localisation)) \
from lieu_carto where lic_mnd_id=%d" % (wrld.id)
    x, y = monde._connection.queryOne(sql)
    if x and y:
        WORLD_DIMS[wrld.id] = (int(x), int(y))

class lieu_carto(SQLObject):
    '''Geographic location

    Main attributes:
    licLocalisation (postgis point) -- coordinates of the location
    licMndID (integer) -- Id of the world
    licSsnID (integer) -- id of the season
    licLieID (integer) -- id of the location
    '''
    licSsn = ForeignKey('saison')
    licLie = ForeignKey('lieu')
    class sqlmeta:
        idName = 'lic_id'
        fromDatabase = True
        table = 'lieu_carto'
        style = sqlObject.MorriganStyle()

    def getByLocation(location_id):
        '''Get a geographic location by a location
        '''
        lc = lieu_carto.select("lic_lie_id=%d and lic_ssn_id=%d" % (
                                location_id, lieu.CURRENT_SEASON))
        return lc[0]
    getByLocation = staticmethod(getByLocation)

    def getCoordinates(self):
        '''Get the geographic coordinates
        '''
        sql = "SELECT X(lic_localisation), Y(lic_localisation) from lieu_carto \
where lic_id=%d" % (self.id)
        res = lieu_carto._connection.queryOne(sql)
        return int(res[0]), int(res[1])

    def getByPoint(x, y, wrld_id):
        '''Get a geographic location by point
        '''
        # "spherical" world...
        if x > WORLD_DIMS[wrld_id][0]:
            x = 0
        elif x < 0:
            x = WORLD_DIMS[wrld_id][0]
        if y > WORLD_DIMS[wrld_id][1]:
            y = 0
        elif y < 0:
            y = WORLD_DIMS[wrld_id][1]
        #lc = lieu_carto.select("lic_localisation ~= %s and lic_ssn_id=%d"\
        #     % (POSTGIS_POINT_TPL % (x, y), lieu.CURRENT_SEASON))
        lc = list(lieu_carto.select("lic_localisation ~= %s and lic_mnd_id=%d"\
             % (POSTGIS_POINT_TPL % (x, y), wrld_id)))
        if lc:
            return lc[0]
    getByPoint = staticmethod(getByPoint)

    def getDescription(self, character):
        """Return a list of tuple : name, description for each layer
        """
        dct = {}
        if character:
            dct['pers'] = character.label()
        sql = '''select distinct clq.clq_id, clq.clq_tcq_id,
clq.clq_nom, clq.clq_description from calque clq
inner join calque_lieu_carto clc on clc.clc_clq_id=clq.clq_id and
clc.clc_lic_id=%d
order by clq.clq_tcq_id''' % self.id
        res = self._connection.queryAll(sql)
        desc = []
        if res:
            for r in res:
                desc.append((r[2], r[3] % dct))
        return desc

    def label(self):
        """Return the name of the main layer
        """
        sql = '''select distinct clq.clq_id, clq.clq_nom, tcq.tcq_id
from calque clq
inner join calque_lieu_carto clc on clc.clc_clq_id=clq.clq_id and
clc.clc_lic_id=%d
inner join type_calque tcq on clq.clq_tcq_id=tcq.tcq_id and
tcq.tcq_principal = True order by tcq.tcq_id''' % self.id
        res = self._connection.queryAll(sql)
        return " - ".join([r[1] for r in res])

    def _getLayerItem(self, item_field):
        '''Get a type of item from each layers of the geographic
        location
        '''
        sql = '''select distinct clq.clq_id, clq.%s from calque clq
inner join calque_lieu_carto clc on clc.clc_clq_id=clq.clq_id and
clc.clc_lic_id=%d
''' % (item_field, self.id)
        res = self._connection.queryAll(sql)
        return [r[1] for r in res]

    def getLayerIDs(self):
        '''Get a list of id of each layers of the geographic location.
        This function is available to compare if two geographic location are the
        same
        '''
        sql = '''select distinct clq.clq_tcq_id, clq.clq_id from calque clq
inner join calque_lieu_carto clc on clc.clc_clq_id=clq.clq_id and
clc.clc_lic_id=%d order by clq.clq_tcq_id
''' % self.id
        res = self._connection.queryAll(sql)
        return [(r[0], r[1]) for r in res]

    def getEraseFactor(self):
        '''Get erase factor
        '''
        erase_factor = 1
        res = self._getLayerItem("clq_facteur_effacement_trace")
        for r in res:
            erase_factor = erase_factor*r
        return erase_factor

    def getStepsNumberFactor(self):
        '''Get steps number factor
        '''
        steps_factor = 1
        res = self._getLayerItem("clq_facteur_nb_trace")
        for r in res:
            steps_factor = steps_factor*r
        return steps_factor

    def getRumorDifficulty(self, character):
        '''Get rumor difficulty for a designed character
        '''
        difficulty = 0
        res = self._getLayerItem("clq_indice_population")
        for r in res:
            difficulty -= r
        res = self.getAssocietedCharacteristics()
        cache = {}
        characteristics = []
        for r in res:
            if r not in cache:
                characteristic = character.getCharacteristic(r)
                cache[r] = characteristic
                characteristics.append(characteristic)
            else:
                characteristics.append(cache[r])
        for characteristic in characteristics:
            difficulty -= characteristic.pcrScoreCalcule
        return difficulty

    def getBlocking(self):
        '''Get if the path to the location is blocking
        '''
        res = self._getLayerItem("clq_echec_bloquant")
        blocking = False
        for r in res:
            blocking = blocking or r
        return blocking

    def getAPAccesCost(self, character_id, extra={}):
        '''Get the action point acces cost
        '''
        res = self._getLayerItem("clq_cout_acces_pa")
        return evalFormula("+".join(res), character_id, extra)

    def getFPAccesCost(self, character_id, extra={}):
        '''Get the food point acces cost
        '''
        res = self._getLayerItem("clq_cout_acces_pn")
        return evalFormula("+".join(res), character_id, extra)

    def getFPAccesCost(self, character_id, extra={}):
        '''Get the food point acces cost
        '''
        res = self._getLayerItem("clq_cout_acces_pn")
        return evalFormula("+".join(res), character_id, extra)

    def getPVFailCost(self, character_id, extra={}):
        '''Get the life point cost when failed
        '''
        res = self._getLayerItem("clq_cout_echec_pv")
        return evalFormula("+".join(res), character_id, extra)

    def getAssocietedCharacteristics(self):
        '''Get the characteristic bonus
        '''
        res = []
        for r in self._getLayerItem("clq_crc_id"):
            if r:
                res.append(r)
        return res

    def getAssocietedCharacteristics2(self):
        '''Get seconds characteristic bonus
        '''
        res = []
        for r in self._getLayerItem("clq_crc2_crc_id"):
            if r:
                res.append(r)
        return res

    def getCharacteristicBonus(self, character_id, extra={}, get_characs=None):
        '''Get the characteristic bonus
        '''
        res = self.getAssocietedCharacteristics()
        res += self.getAssocietedCharacteristics2()
        formula = '0'
        if res:
            formula = "+".join("%%(c%d)d" % r for r in res)
        result = evalFormula(formula, character_id, extra)
        if not get_characs:
            return result
        return result, res

    def getDiff(self, character_id):
        '''Get the difficulty to reach the location
        '''
        res = self._getLayerItem("clq_difficulte")
        return evalFormula("+".join(res), character_id)

def importWorldFromFile(file_path, nb_rows, nb_cols, wrld_id, ssn_id,
                        layer_ids=[4, 3, 2, 1], lie_tli_id=1):
    now = DateTime.now()
    layers = []
    file = open(file_path, 'r')
    rows = file.readlines()
    if nb_rows != len(rows):
        raise "MorriganImportError", "Invalid nomber of rows"
    file.close()
    row_id = nb_rows
    clq_ids = {}
    for row in rows:
        row_id -= 1
        points = row.split(',')
        # delete windows line breaks
        if points[-1] == '\n':
            points = points[:-1]
        if nb_cols != len(points):
            raise MorriganError, "Invalid nomber of cols (%d) on line \
%d" % (len(points), nb_rows - row_id)
        col_id = 0
        for point in points:
            if point[-1] == '\n':
                point = point[:-1]
            location = None
            geographic_location = list(lieu_carto.select("lic_mnd_id=\
%d and lic_localisation ~= %s and lic_ssn_id=%d" % (wrld_id,
            POSTGIS_POINT_TPL % (col_id, row_id), ssn_id)))
            if not geographic_location:
                location = lieu(lieTliID=lie_tli_id, lieNom=None,
                    lieSctID=None, lieIndiceTaille=None, lieCarto=True,
                    lieFacteurEffacementTrace=1, lieFacteurNbTrace=1)
                sql = '''INSERT INTO lieu_carto (lic_ssn_id,
lic_mnd_id, lic_lie_id, lic_localisation) VALUES (%d, %d, %d, %s)''' \
% (ssn_id, wrld_id, location.id, POSTGIS_POINT_TPL % (col_id, row_id))
                lieu._connection.query(sql)
                sql ='''select max(lic_id) from lieu_carto'''
                res = lieu._connection.queryOne(sql)
                geographic_location = lieu_carto.get(res[0])
            else:
                geographic_location = geographic_location[0]
                location = geographic_location.licLie
            updates = []
            idx = len(layer_ids) - len(point)
            for layer_id in point:
                if (layer_ids[idx], int(layer_id)) not in clq_ids:
                    clq = list(Layer.select("clq_id_externe=%d and \
clq_tcq_id=%d" % (int(layer_id), layer_ids[idx])))
                    if not clq:
                        raise MorriganError, "External id not found in\
 database"
                    clq_ids[(layer_ids[idx], int(layer_id))] = clq[0].id

                sql = '''select clc.clc_id from calque_lieu_carto clc
inner join calque clq on clq.clq_id=clc_clq_id and clq_tcq_id=%d
where clc.clc_lic_id=%d''' % (layer_ids[idx], geographic_location.id)

                res = lieu._connection.queryAll(sql)
                sql = ""
                if res:
                    sql = '''update calque_lieu_carto set
                    clc_clq_id=%d where clc_id=%d''' % (
                    clq_ids[(layer_ids[idx], int(layer_id))], res[0][0])
                else:
                    sql = '''insert into calque_lieu_carto (clc_clq_id,
clc_lic_id) values (%d, %d);''' % (clq_ids[(layer_ids[idx],
                    int(layer_id))], geographic_location.id)
                lieu._connection.query(sql)
                idx += 1
            col_id += 1
        print "Line %d inserted" % (nb_rows - row_id)
    print "Done in : " + str(DateTime.now() - now)




class chemin(SQLObject):
    """
    Roads between locations.
    A sector is an ensemble of locations
    To connect between them special types of connection are needed :
    - access 'pont' and 'trappe' are connections between sectors and
    location,
    - access 'sas' are connection between sectors,
    - connection between location can be done with access 'classique'.

    Main attributes :

    id (integer) -- road's id,
    chmEgnID (integer) -- element needed to use this road,
    chmDepartLieID (integer) -- starting location,
    chmArriveeLieID (integer) -- finish location,
    chmDescAller (string) -- description of one side of the road,
    chmDescRetour (string) -- description of other side of the road,
    chmAller (boolean) -- set to true if from start the road is open,
    chmRetour (boolean) -- set to true if from finish the road is open,
    chmOuvert (boolean) -- set to true if the road is open,
    chmDiscretion (integer) -- discretion of a road,
    chmDifficulte (integer) -- difficulty ceil of the road,
    chmCoutPa (integer) : action point cost,
    chmCoutPn (integer) : food point cost,
    chmTchID (integer) : type of road,
    chmPchID (integer) : particularity of a road,
    chmTllID (integer) : size max accorded by the road,
    chmFacteurEffacementTrace (float) : steps erase factor
    chmFacteurNbTrace (float) : steps factor
    """

    class sqlmeta:
        idName = 'chm_id'
        fromDatabase = True
        table = 'chemin'
        style = sqlObject.MorriganStyle()
    chmDepartLie = ForeignKey('lieu')
    chmArriveeLie = ForeignKey('lieu')

    def destroySelf(self):
        for route in list(Route.select('itn_chm_id=%d' % self.id)):
            route.destroySelf()
        SQLObject.destroySelf(self)

    def label(self, character, show_cost=True):
        """
        label(self, character)

        character (character) : character
        Return a print of the road to the character view.

        return : string, print a view of the road to the character view
        """
        end = None
        if character.prsLieID == self.chmDepartLieID:
            end = self.chmArriveeLieID
        elif character.prsLieID == self.chmArriveeLieID:
            end = self.chmDepartLieID
        string = lieu.get(end).label()
        if self.chmPchID:
            sql = "select pch_nom from particularite_chemin where pch_id=%d" %\
                                                                  self.chmPchID
            string = "%s - %s" % (chemin._connection.queryOne(sql)[0], string)
        if show_cost:
            string += " : %s %% %s, %s %% %s" % (
                character.formatAP(evalFormula(self.chmCoutPa, character.id)),
                _("APs"),
                character.formatFP(evalFormula(self.chmCoutPn, character.id)),
                _("FPs"))
        return string

    def getRoadsFromLocation(location_id):
        """
        getRoadsFromLocation

        location_id -- id of the location
        Return roads available from a location
        """
        query = "chm_depart_lie_id=%d or chm_arrivee_lie_id=%d" % (location_id,
                                                                   location_id)
        return list(chemin.select(query))
    getRoadsFromLocation = staticmethod(getRoadsFromLocation)

    def getHiddenRoads(character, id_only=None):
        """
        Get hidden roads not yet found by the character

        character (character) -- character
        """
        query = "select chm.chm_id, acc.acc_id is null from chemin chm"
        query += " inner join personnage chr on chr.prs_id=%d" % character.id
        query += " left join acces_connu acc on acc.acc_chm_id=chm.chm_id and \
chr.prs_id=acc.acc_prs_id"
        query += " where chm.chm_discretion > 0"
        query += " and chm.chm_ouvert=TRUE and (\
(chm.chm_depart_lie_id=%(prsLieID)s and chm.chm_aller=TRUE) or\
(chm.chm_arrivee_lie_id=%(prsLieID)s and chm.chm_retour=TRUE))\
" % {'prsLieID': character.prsLieID}
        res = list(chemin._connection.queryAll(query))
        roads = []
        for r in res:
            result = r[0]
            if not id_only:
                result = chemin.get(result)
            if r[1] == True:
                roads.append(result)
        return roads
    getHiddenRoads = staticmethod(getHiddenRoads)

    def getRoads(character, id_only=None, show_small_roads=None,
                 no_discretion=None):
        """
        getRoads(character)

        character (character) -- character
        id_only (boolean) -- if set too true return id only
        show_small_roads (boolean) -- if set to true return roads that are too
small for the character
        no_discretion (boolean) -- if set to true the discretion is not evaluated
        Return roads available for a character.

        return : list, get available roads for the character
        TODO : Gérer les élements nécessaires
        """
        query = "select chm.chm_id, (chm.chm_indice_taille >= \
frm.frm_indice_taille or chm.chm_indice_taille is null)%s from chemin \
chm"
        query += " inner join personnage chr on chr.prs_id=%d" % character.id
        query += " inner join forme frm on chr.prs_id=frm.frm_prs_id and \
frm.frm_actuelle=True"

        if not show_small_roads:
            query += " and (chm.chm_indice_taille >= frm.frm_indice_taille\
 or chm.chm_indice_taille is null)"
        if not no_discretion:
            query = query % ", (chm.chm_discretion = 0 or acc.acc_id is not null)"
            query += " left join acces_connu acc on chm.chm_id=acc.acc_chm_id \
and chr.prs_id=acc.acc_prs_id"
        else:
            query = query % ''
        query += " where chm.chm_ouvert = TRUE and (\
(chm.chm_depart_lie_id = %(prsLieID)s and chm.chm_aller = TRUE) or\
(chm.chm_arrivee_lie_id = %(prsLieID)s and chm.chm_retour = TRUE))\
" % {'prsLieID': character.prsLieID}
        res = list(chemin._connection.queryAll(query))
        roads, smallRoads = [], []
        for r in res:
            result = r[0]
            if not id_only:
                result = chemin.get(result)
            if r[1] == True and (no_discretion or r[2] == True):
                roads.append(result)
            elif no_discretion or r[2] == True:
                smallRoads.append(result)
        if show_small_roads:
            return roads, smallRoads
        return roads
    getRoads = staticmethod(getRoads)

class particularite_chemin(SQLObject):
    """
    Only a specific detail for a road ("by the window", "by the roof")
    """

    class sqlmeta:
        idName = 'pch_id'
        fromDatabase = True
        table = 'particularite_chemin'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.pchNom

class type_chemin(SQLObject):
    """
    Road type
    """
    class sqlmeta:
        idName = 'tch_id'
        fromDatabase = True
        table = 'type_chemin'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tchNom

class Rumor(SQLObject):
    '''
    Rumors.

    Main attributes :
    id (integer) -- id
    rmrNom (string) -- name
    rmrPrsID (integer) -- id of the sender
    rmrLngID (integer) -- id of the language
    rmrContent (string) -- content of the rumor
    rmrLieID (integer) -- id of the location of the start of the rumor
    rmrSecID (integer) -- optional id of the sector concerned by the rumor
    rmrRayon (integer) -- optional radius of the rumor (if the start point is on
    a map)
    rmrDateDebut (date) -- start date for the rumor
    rmrDateFin (date) -- end date of the rumor
    '''
    class sqlmeta:
        idName = 'rmr_id'
        fromDatabase = True
        table = 'rumeur'
        style = sqlObject.MorriganStyle()

    def getAvailableRumors(pers):
        '''Get rumor available for a character
        '''
        character_location = pers.prsLie
        sql = ''
        if character_location.lieCarto:
            map_location = lieu_carto.getByLocation(character_location.id)
            sql = '''select rmr.rmr_id from rumeur rmr
inner join lieu_carto lcprs on lcprs.lic_id=%d
inner join lieu_carto lic on lic.lic_lie_id=rmr.rmr_lie_id
and distance(lic.lic_localisation, lcprs.lic_localisation)<=rmr.rmr_rayon
where rmr.rmr_rayon is not null and (rmr_date_fin >= DATE('%s') or
rmr_date_fin is NULL) and (rmr_date_debut <= NOW() or rmr_date_debut is NULL)
and rmr_prs_id <> %d;''' % (
map_location.id, DateTime.gmt().strftime("%Y-%m-%d"), pers.id)
        else:
            sql = '''select rmr.rmr_id from rumeur rmr
where rmr.rmr_sct_id=%d and (rmr_date_fin >= DATE('%s') or
rmr_date_fin is NULL) and (rmr_date_debut <= NOW() or rmr_date_debut is NULL)
and rmr_prs_id <> %d;''' % (
character_location.lieSctID, DateTime.gmt().strftime("%Y-%m-%d"), pers.id)
        res = list(Rumor._connection.queryAll(sql))
        rumors = []
        for r in res:
            rumors.append(Rumor.get(r[0]))
        not_known = []
        for rumor in rumors:
            sql = 'select * from rumeur_ecoute where rmc_rmr_id=%d and \
rmc_prs_id=%d' % (rumor.id, pers.id)
            res = Rumor._connection.queryAll(sql)
            if not res:
                not_known.append(rumor)
        return not_known
    getAvailableRumors = staticmethod(getAvailableRumors)


class Route(SQLObject):
    '''
    Route

    Main attributes:
    itnFrmID (integer) -- shape's id
    itnDepartLieID (integer) -- start location's id
    itnArriveeLieID (integer) -- finish location's id
    itnChmID (integer) -- road's id
    itnTracesInitiales (integer) -- original steps quantity
    itnTracesRestantes (integer) -- steps left
    itnDate (date) -- date of the move
    itnIndexEffacement (integer) -- number of steps to delete at the
    next delivery
    itnTtrID (integer) -- step's type id
    itnProfondTrace (integer) -- step's deep
    itnTailleTrace (integer) -- step's size
    '''
    class sqlmeta:
        idName = 'itn_id'
        fromDatabase = True
        table = 'itineraire'
        style = sqlObject.MorriganStyle()
    itnDepartLie = ForeignKey('lieu')
    itnArriveeLie = ForeignKey('lieu')
    itnChmID = ForeignKey('chemin')

    def getRouteFromLocation(location_id):
        '''
        Get all available routes from a location
        '''
        sql = 'itn_depart_lie_id = %d or itn_arrivee_lie_id = %d' % (location_id,
                                                                     location_id)
        return list(Route.select(sql))
    getRouteFromLocation = staticmethod(getRouteFromLocation)

    def getAvailableRouteToFollow(character, target_id=None, tail=None,
                                  track=None, fromDate=''):
        '''
        If no target_id specified get the id of characters that can be
        followed otherwise get the route.
        If tail is set to true, there is no need to check in the
        available tails.
        If track is set to True a bidirectionnal check is done.
        If fromDate is set to True, the search is limited to this date.
        '''
        #TODO: gérer traces
        extra = ''
        if fromDate:
            fromDate = fromDate.strftime("%Y-%m-%d %H:%M:%S")
            fromDate = " and itn_date > '%s' " % fromDate
        if not tail and not target_id:
            sql = "flt_suivant_prs_id=%d" % character.id
            if target_id:
                sql += " and flt_suivi_frm_id=%d" % target_id
            tailing = list(Tailing.select(sql))
            if len(tailing) == 1:
                extra = "(itn_frm_id = %d%s) or " % (
                                    tailing[0].fltSuiviFrmID, fromDate)
            elif len(tailing) > 1:
                extra = "(itn_frm_id in (%s)%s) or " % (
                                    ",".join([str(tail.fltSuiviFrmID)
                                              for tail in tailing]),
                                    fromDate)
        elif target_id:
            extra = "(itn_frm_id = %d%s) or " % (target_id, fromDate)
        sql = "(%sitn_date > '%s') " % (extra,
                character.prsDateArrivee.strftime("%Y-%m-%d %H:%M:%S"))
        sql += 'and (itn_depart_lie_id = %d ' % character.prsLieID
        # birectionnal search ?
        if not track:
            sql += ") "
        else:
            sql += ' or itn_arrivee_lie_id = %d) ' % character.prsLieID
        if target_id:
            sql += "and itn_frm_id = %d order by itn_date desc" % \
                                                              target_id
        else:
            sql += "and itn_frm_id != %d" % character.id
        routes = list(Route.select(sql))
        if target_id:
            if routes and not track:
                return routes[0]
            elif routes:
                return routes
            return
        frms = []
        for route in routes:
            if route.itnFrmID not in frms:
                frms.append(route.itnFrmID)
        return frms
    getAvailableRouteToFollow = staticmethod(getAvailableRouteToFollow)

    @classmethod
    def getAvailableStepsToFollow(cls, character, get_self_steps=True):
        '''
        Get steps to follow.
        '''
        sql = 'itn_depart_lie_id = %d or itn_arrivee_lie_id = %d ' % (
                                    character.prsLieID, character.prsLieID)
        if not get_self_steps:
            sql += "and itn_frm_id != %s " % character.getCurrentShape().id
        sql += "order by itn_date asc"
        return list(cls.select(sql))

    @staticmethod
    def newRoute(character, path_id, startLocaId, endLocaId):
        '''
        Create a new route from a character, start and finish point.

        Arguments :
        character (personnage) -- the character
        path_id (integer) -- path used
        startLocaId (integer) -- start location id
        endLocaId (integer) -- endlocation id
        '''
        steps, erase_factor = config.STEPS_NUMBER, 1
        # location modifier
        start = lieu.get(startLocaId)
        steps = steps*start.getStepsNumberFactor()
        erase_factor = erase_factor*start.getEraseFactor()
        # path modifier
        if path_id:
            path = chemin.get(path_id)
            steps = steps*path.chmFacteurNbTrace
            erase_factor = erase_factor*path.chmFacteurEffacementTrace
        # character modifier
        steps = steps*character.prsCalcFacteurNbTrace
        steps = int(round(steps))
        erase_factor = int(round(erase_factor))
        frm = character.getCurrentShape()
        Route(itnFrmID=frm.id, itnChmID=path_id,
itnDepartLieID=startLocaId, itnArriveeLieID=endLocaId,
itnTracesInitiales=steps, itnTracesRestantes=steps,
itnDate=DateTime.now(), itnIndexEffacement=erase_factor,
itnTtrID=frm.frmTtrID, itnProfondTrace=frm.frmProfondTrace,
itnTailleTrace=frm.frmTailleTrace)

class TailType(SQLObject):
    '''
    Type of tail.
    '''
    class sqlmeta:
        idName = 'tfl_id'
        fromDatabase = True
        table = 'type_filature'
        style = sqlObject.MorriganStyle()

SIMPLE_TAIL = list(TailType.select("tfl_nom='Filature'"))[0].id
TRACK_TAIL = list(TailType.select("tfl_nom='Pistage'"))[0].id
FOLLOW_TAIL = list(TailType.select("tfl_nom='Suite'"))[0].id

class Tailing(SQLObject):
    '''
    Available tailing.

    Main attributes:
    fltSuivantPrsID -- id of the prosecutor
    fltSuiviFrmID -- id of the pursued shape
    fltTflID -- id of the type of tailing
    fltDebut -- date of start of tailing
    '''
    class sqlmeta:
        idName = 'flt_id'
        fromDatabase = True
        table = 'filature'
        style = sqlObject.MorriganStyle()
    default_type = SIMPLE_TAIL

    def create(prosecutor_id, pursued_id, type=default_type):
        '''
        Create a new entry for this tailing if none exist.
        '''
        res = list(Tailing.select("flt_suivant_prs_id=%d and \
flt_suivi_frm_id=%d and flt_tfl_id=%d" % (prosecutor_id, pursued_id,
                                          type)))
        if res:
            return
        Tailing(fltSuivantPrsID=prosecutor_id,
                fltSuiviFrmID=pursued_id,
                fltTflID=type, fltDebut=DateTime.now())
    create = staticmethod(create)

    def getFromCharacters(prosecutor_id, pursued_id, type=None):
        '''
        Get tails.
        '''
        sql = []
        if prosecutor_id:
            sql.append('flt_suivant_prs_id=%d' % prosecutor_id)
        if pursued_id:
            sql.append('flt_suivi_frm_id=%d' % pursued_id)
        if type:
            sql.append('flt_tfl_id=%d' % type)
        return list(Tailing.select(" and ".join(sql)))
    getFromCharacters = staticmethod(getFromCharacters)

class StepType(SQLObject):
    '''
    Type of step.
    '''
    class sqlmeta:
        idName = 'ttr_id'
        fromDatabase = True
        table = 'type_trace'
        style = sqlObject.MorriganStyle()

class KnownedSteps(SQLObject):
    '''
    Steps recognized by the character.

    Main attributes:
    trcTtrID -- id of the steps type
    trcPrsID -- id of the character who has discover the steps
    trcFrmTraceID -- id of the shape who has left the steps
    trcDateFin -- date of end of the steps
    trcNomDonne -- name given to this steps
    trcProfondeur -- deep of the steps
    trcTaille -- size of the steps
    '''
    class sqlmeta:
        idName = 'trc_id'
        fromDatabase = True
        table = 'trace_connue'
        style = sqlObject.MorriganStyle()
    delay = DateTime.oneWeek

    def create(character_id, route):
        '''
        Create a new entry for this recognize step if none exist.
        '''
        res = list(KnownedSteps.select("trc_ttr_id=%d and \
trc_prs_id=%d and trc_frm_trace_id=%d and trc_profondeur=%d and \
trc_taille=%d" % (route.itnTtrID, character_id, route.itnFrmID,
                  route.itnProfondTrace, route.itnTailleTrace)))
        if res:
            res[0].trcDateFin = (DateTime.now() + KnownedSteps.delay)
            return res[0]
        return KnownedSteps(trcTtrID=route.itnTtrID,
               trcPrsID=character_id, trcFrmTraceID=route.itnFrmID,
               trcProfondeur=route.itnProfondTrace,
               trcTaille=route.itnTailleTrace,
               trcDateFin=(DateTime.now() + KnownedSteps.delay))
    create = staticmethod(create)
