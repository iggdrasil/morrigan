#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : dbFight.py
#
# Copyright (C) 2008 Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Object specific to fight.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from sqlobject.main import SQLObject
from sqlobject.col import ForeignKey
import plugins.configuration as config
import sqlObject
from utils import _
from utils import *

class engagement(SQLObject):
    '''
    Taking part in a fight
    Main attributes :
        engPrs1Prs -- first character taking part
        engPrs2Prs -- second character taking part
        engInit1 -- True if the first character has the initiative otherwise
    the second character has it
        engLie -- location where the takepart take place
    '''
    class sqlmeta:
        idName = 'eng_id'
        fromDatabase = True
        table = 'engagement'
        style = sqlObject.MorriganStyle()
    engPrs1Prs = ForeignKey('personnage')
    engPrs2Prs = ForeignKey('personnage')
    engLie = ForeignKey('location')

    @classmethod
    def getByCharacter(cls, character_id):
        '''
        Get TakePart by character involved
        '''
        tps = list(cls.select('eng_prs1_prs_id=%d or eng_prs2_prs_id=%d' %\
                                              (character_id, character_id)))
        return tps

    @classmethod
    def getByLocation(cls, location_id):
        '''
        Get TakePart by location
        '''
        tps = list(cls.select('eng_lie_id=%d' % location_id))
        return tps
TakePart = engagement
