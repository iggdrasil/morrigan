#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : dbCommon.py - CREATION : 2005/09/10
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Mapping of common database objects.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

import random
from sqlobject.main import SQLObject
from sqlobject.col import ForeignKey
import plugins.configuration as config
import sqlObject
from utils import _
from utils import *
import constants

class Css(SQLObject):
    '''
    Cascading Stylesheets available.
    Main attributes :
        - cssNom : label of the css
        - cssChemin : path of the css
        - cssDefaut : True if it is the default CSS
    '''
    class sqlmeta:
        idName = 'css_id'
        fromDatabase = True
        table = 'css'
        style = sqlObject.MorriganStyle()

class image(SQLObject):
    """
    Image

    imgNom -- name of the image
    imgAdress -- address of the image
    """
    class sqlmeta:
        idName = 'img_id'
        fromDatabase = True
        table = 'image'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.imgNom

Image = image

class Emote(SQLObject):
    '''
    emote available.
    Main attributes:
        - mimNom : short name of the emote
        - mimLabel : label of the emote
        - mimTypeID : type of the emote
    '''
    class sqlmeta:
        idName = 'mim_id'
        fromDatabase = True
        table = 'mime'
        style = sqlObject.MorriganStyle()

    def getSimpleEmotes():
        '''
        Get simple emotes.
        '''
        return Emote.select("mim_tmm_id=1 order by mim_nom")
    getSimpleEmotes = staticmethod(getSimpleEmotes)

    def getCharacterEmotes():
        '''
        Get emotes that implies characters.
        '''
        return Emote.select("mim_tmm_id=2 order by mim_nom")
    getCharacterEmotes = staticmethod(getCharacterEmotes)

class Characteristic(SQLObject):
    """
    Characteristics.

     - id (integer) : id,
     - crcNom (string) : name of the characteristic,
     - crcDescription (string) : description of the characteristic
     - crcTcrID (integer) : id of the type of characteristic
     - crcNcrID (integer) : id of the nature of characteristic
    """
    class sqlmeta:
        idName = 'crc_id'
        fromDatabase = True
        table = 'caracteristique'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.crcNom

class CharChar(SQLObject):
    """
    Characteristic of a character.

     id (integer) -- id
     pcrPrsID (integer) -- id of the character
     pcrCrcID (integer) -- id of the characteristic
     pcrJaugeEntrainement (integer) -- current state of the training measure
     pcrScoreXp (string) -- current XP
     pcrScoreXpMax (string) -- max XP reached
     pcrScoreCalcule (string) -- current level
     pcrScoreMax (string) : level max
    """
    class sqlmeta:
        idName = 'pcr_id'
        fromDatabase = True
        table = 'perso_carac'
        style = sqlObject.MorriganStyle()

    def getCharChar(prs_id, crc_id):
        '''
        Get the CharChar instance associated with the given prs_id and crc_id
        '''
        res = list(CharChar.select('pcr_prs_id=%d and pcr_crc_id=%d' % (prs_id,
                                                                      crc_id)))
        if res:
            return res[0]
        else:
            return CharChar(pcrPrsID=prs_id, pcrCrcID=crc_id,
        pcrJaugeEntrainement=0, pcrScoreXp=0, pcrScoreXpMax=0,
        pcrScoreCalcule=0, pcrScoreMax=0)
    getCharChar = staticmethod(getCharChar)

    def value(self):
        return self.pcrScoreCalcule

    def addXP(self, xp):
        '''
        Add XP to the current characteristic
        '''
        self.pcrScoreXp += xp
        if (self.pcrScoreMax * constants.XP_PER_POINT) < self.pcrScoreXp:
            self.pcrScoreXp = self.pcrScoreMax * constants.XP_PER_POINT
        if self.pcrScoreXpMax < self.pcrScoreXp:
            self.pcrScoreXpMax = self.pcrScoreXp

    def training(self):
        '''
        Train on this characteristic
        '''
        if not self.pcrJaugeEntrainement and self.pcrJaugeEntrainement != 0:
            self.pcrJaugeEntrainement = 0
        if self.pcrJaugeEntrainement < config.TRAINING_MAX:
            self.pcrJaugeEntrainement += config.TRAINING_GAIN

class action(SQLObject):
    '''
    Action parameters.
    Main attributes:
        - actNom : name of the action
        - actNomForm : name used for forms. It is unique and have no specials
    characters and no spaces
        - actDesc : description of the action
        - actFormAttaque : action formula
        - actFormDefense : defense formula
        - actFormCoutPa : formula for action point cost
        - actFormCoutPn : formula for food point cost
        - actFormCoutPp : formula for paralysie point cost
        - actFormCoutPv : formula for vital point cost
        - actFormGainMin : min XP wins
        - actFormGainMax : max XP wins
        - actCourante : base action
        - actAide : help for this action
    '''
    class sqlmeta:
        idName = 'act_id'
        fromDatabase = True
        table = 'action'
        style = sqlObject.MorriganStyle()

    def getByName(formName):
        '''
        Get action instance by name.
        Attribute:
            - string formName : form name of the action
        Return : Desired action or None
        '''
        result = list(action.select("act_nom_form = '%s'" % formName))
        if result:
            return result[0]

    def isAvailable(self, character, ap_needed, fp_needed, fip_needed):
        '''
        Check if the action is available for a designed character.prsPa
        character -- Character
        ap_needed -- number of action points needed for this action
        fp_needed -- number of food points needed for this action
        fip_needed -- number of fight points needed for this action
        '''
        sql = "select cna_autorise from classeniveau_action cna \
inner join perso_classe pce on pce.pce_cln_id=cna.cna_cln_id \
and pce.pce_prs_id=%d where cna.cna_act_id=%d" % (character.id, self.id)
        availabilities = list(self._connection.queryAll(sql))
        if not self.actCourante and not availabilities:
            return False, _("The character don't have the correct class")
        if availabilities:
            available = None
            for availability in availabilities:
                if not availability[0]:
                    return False, _("This action is forbid for the character")
                else:
                    available = True
            if not self.actCourante and not available:
                return False, _("The character don't have the correct class")
        available, reason = True, ""
        # verify if it is a combat action
        if not self.actCombat and character.getTakeParts():
            available = False
            reason = _("You are taking part in a fight. \
This action is not available")
        if ap_needed > character.prsPa:
            available = False
            reason = _("Not enough action points")
        if fp_needed > character.prsPn:
            available = False
            reason = _("Too much fatigue points")
        if fip_needed > character.prsPc:
            available = False
            reason = _("Not enough fight points")
        return available, reason

    def evalXPFormula(formula, characterId, multiply=1, train=None, extra={}):
        '''
        Attributes :
            - string formula : formula
            - integer characterId : character id
            - integer multiply : multiply factor
        Evaluate XP formula. The different characteristic concerned are separed
        by a ';'.
        Then separated by a comma first the multiply factor is given then the
        characteristic id.
        '''
        for formul in formula.split(";"):
            multip, attribute_id = formul.split(',')
            if attribute_id == '0':
                return
            attribute_id = attribute_id % extra
            characteristic = CharChar.getCharChar(characterId,
                                                  int(attribute_id))
            multip = eval(multip % extra)
            if characteristic:
                characteristic.addXP(int(multip)*multiply)
                print "%s : %d XP" % \
         (Characteristic.get(characteristic.pcrCrcID).crcNom, int(multip)*multiply)
                if train:
                    characteristic.training()

    def evalTrainFormula(formula, characterId):
        '''
        Attributes :
            - string formula : formula
            - integer characterId : character id
        Evaluate XP formula for training.
        '''
        for formul in formula.split(";"):
            multip, attribute_id = formul.split(',')
            if attribute_id == '0':
                return
            characteristic = CharChar.getCharChar(characterId,
                                                  int(attribute_id))
            if characteristic:
                characteristic.training()

    evalXPFormula = staticmethod(evalXPFormula)
    evalTrainFormula = staticmethod(evalTrainFormula)
    getByName = staticmethod(getByName)
Action = action
