#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Mapping of database objects directly related to players and characters.
'''
from sqlobject.main import SQLObject
from sqlobject.col import ForeignKey
import sqlObject
from mx import DateTime
import plugins.configuration as config
import md5

from utils import _, roleplayFormat, sqlFormat, evalFormula
import constants
from constants import DIRECTIONS, DIRECTION_COST, HEALTH_LABELS, \
        FATIGUE_LABELS, BDD_CONSTANTS as BDD, MONEY_LABELS, MIN_VISIBLE_SIZE,\
        DEFAULT_CORPSE_DESCRIPTION, HELL_ID
from actionFileManager import ActionFile
from dbCommon import CharChar
from dbFight import TakePart
from dbInventory import Item, Weapon, taille, perso_monnaie, VisibleItem, \
                        Chest, Shop, KnownItems, monnaie, Material, BaseItem
from dbGeography import Tailing, Route, chemin, lieu, monde, \
                        SIMPLE_TAIL, TRACK_TAIL, FOLLOW_TAIL

class personnage(SQLObject):
    '''
    Character of a player.

    Main attributes :
    id (integer) -- id,
    prsJouID (integer) -- id of the player,
    prsPrincipal (boolean) -- Main character of the player, if it is true,
    prsPossesseurPrsID (integer) -- id of the owner,
    prsLieID (integer) -- id of the location where is the character,
    prsCmpID (integer) -- id of the company,
    prsNomOriginel (string) -- base name,
    prsNomActuel (string) -- current name,
    prsBackground (string) -- background,
    prsBiographie (string) -- biography,
    prsDerniereConnexion (date) -- date of last connection,
    prsPa (integer) -- current action points,
    prsPaMax (integer) -- max action points,
    prsPn (integer) -- feed points,
    prsPnMax (integer) -- max feed points,
    prsPp (integer) -- paralysis points,
    prsPpMax (integer) -- max paralysis points,
    prsPv (integer) -- health points,
    prsPvMax (integer) -- max health points,
    prsTprId (integer) -- character type (PC, NPC, etc.),
    prsCalcFacteurNbTrace (float) -- steps factor
    actionFile -- action file of the character
    '''
    class sqlmeta:
        idName = 'prs_id'
        fromDatabase = True
        table = 'personnage'
        style = sqlObject.MorriganStyle()
    prsLie = ForeignKey('lieu')

    def _init(self, id, connection=None, selectResults=None):
        SQLObject._init(self, id, connection, selectResults)
        self.actionFile = ActionFile(self.id, self.prsNomActuel)
        self.takeParts = []
        # reason for a fail
        self.reason = ""
        # first list for the character, second for the near characters
        self.current_messages = [[], []]

    def label(self):
        return self.prsNomActuel

    def isAdmin(self):
        return Player.get(self.prsJouID).isAdmin()

    def isNear(self, childs=True, language=None, id_only=None,
               no_discretion=None):
        '''
        Definition of characters near the given character.

        Attributes :
        childs (boolean) -- if set to true return also the dependant characters
        language (int) -- if not null, specify a language known by the character
        id_only (boolean) -- if true return only IDs
        no_discretion (boolean) -- if true hidden characters are taken

        Return :
        result (list) -- return the list of characters near the given character.
        '''
        if not language:
            if childs:
                query = "prs_lie_id=%d and prs_id <> %i" % (self.prsLieID,
                                                            self.id)
            else :
                query = "prs_lie_id = %(loca)d and prs_id <> %(id)d and \
prs_possesseur_prs_id is null or prs_possesseur_prs_id not in (select prs_id \
from  personnage where prs_lie_id = %(loca)s)" % {'loca':self.prslieID,
                                                  'id':self.id}
            if not id_only:
                result = list(personnage.select(query))
                return result
            else:
                query = "select prs_id from personnage where " + query
                res = list(self._connection.queryAll(query))
                return [r[0] for r in res]
        sql = '''select prs.prs_id from personnage prs
inner join perso_carac pcr on pcr.pcr_crc_id = %d and pcr.pcr_prs_id =
prs.prs_id and pcr.pcr_score_calcule > 0 where ''' % language
        if childs:
            sql += "prs.prs_lie_id=%d and prs.prs_id <> %i" % (self.prsLieID,
                                                               self.id)
        else :
            sql += '''prs.prs_lie_id = %(loca)d and prs.prs_id <> %(id)d and
prs.prs_possesseur_prs_id is null or prs.prs_possesseur_prs_id
not in (select prs_id from personnage where prs_lie_id = %(loca)s)''' \
                % {'loca':self.prslieID, 'id':self.id}
        result = self._connection.queryAll(sql)
        if not result:
            return []
        if id_only:
            return [r[0] for r in list(result)]
        res = []
        for r in result:
            res.append(personnage.get(int(r[0])))
        return res

    def verifyPresence(self, targets_ids):
        '''
        Verifying the presence of givens characters.

        Attributes :
        targets_ids (int[]) -- if set to true return also the dependant
        characters

        Return :
        error -- None if no problems otherwise return the error message.
        '''
        possible_targets = self.isNear(True, None, True)
        for target_id in targets_ids:
            if target_id not in possible_targets:
                #TODO: gérer le message en fonction du sexe
                return _("%s is not here anymore.") % \
                       personnage.get(target_id).prsNomActuel
        return None

    def isAwake(self):
        '''
        Check if the character is awake
        '''
        if self.prsPv > 0:
            return True
        return False

    def getActionFiles(self):
        return ActionFile.getActionFiles(self)

    def getCurrentShape(self):
        '''
        Get the current form associated to the character.

        Return :
        return (forme) -- the current form associated to the character.
        '''
        query = "frm_prs_id=%i and frm_actuelle=TRUE" % self.id
        result = list(forme.select(query))
        if not result:
            return
        return result[0]

    def getCharacteristic(self, crc_id):
        '''
        Get one characteristic for current character.
        '''
        return CharChar.getCharChar(self.id, crc_id)

    def getBaseCharacteristic(self, key):
        '''
        Get one main characteristic for current character by key
        '''
        dct = {'PA':'prsPa', 'PA_max':'prsPaMax', 'PF':'prsPn',
               'PF_max':'prsPnMax', 'PV':'prsPv', 'PV_max':'prsPvMax', 
               'PC':'prsPc', 'PC_max':'prsPcMax'}
        if key not in dct:
            raise "MorriganError", "Base key %s not in base dict" % key
        return getattr(self, dct[key])

    def getValue(self, crc_id):
        '''
        Get one characteristic value for current character.
        '''
        return self.getCharacteristic(crc_id).value()

    def getCharacteristics(self, only_visible=True):
        '''
        Get the characteristics of the character.

        Return :
        characteristics (dict) -- dict associating characteristics categories
        to another dict associeting types to 2-tuples with name of the
        characteristic and the score.
        '''

        sql = '''select tcr.tcr_nom, ncr.ncr_nom, crc.crc_nom,
pcr.pcr_score_calcule from perso_carac pcr
inner join caracteristique crc on crc.crc_id = pcr.pcr_crc_id
inner join type_caracteristique tcr on tcr.tcr_id = crc.crc_tcr_id
inner join nature_caracteristique ncr on ncr.ncr_id = crc.crc_ncr_id
where pcr.pcr_prs_id = %d''' % self.id
        if only_visible:
            sql += " and crc.crc_visible=True"
        result = self._connection.queryAll(sql)
        characteristics = {}
        if result:
            for res in result:
                if characteristics.has_key(res[0]):
                    if characteristics[res[0]].has_key(res[1]):
                        characteristics[res[0]][res[1]].append((res[2],res[3]))
                    else:
                        characteristics[res[0]][res[1]] = [(res[2],res[3])]
                else :
                    characteristics[res[0]] = {}
                    characteristics[res[0]][res[1]] = [(res[2],res[3])]
        return characteristics

    def getLanguages(self):
        '''
        Get languages known by the character.

        Return :
            - list of tuples containing id, name and score in this language
        '''

        sql = '''select crc.crc_id, crc.crc_nom, pcr.pcr_score_calcule
from perso_carac pcr
inner join caracteristique crc on crc.crc_id = pcr.pcr_crc_id
inner join type_caracteristique tcr on tcr.tcr_id = crc.crc_tcr_id
where pcr.pcr_prs_id = %d and tcr.tcr_nom = 'Langue'
''' % self.id
        result = self._connection.queryAll(sql)
        characteristics = []
        if result:
            for res in result:
                if res[2] >= 1:
                    characteristics.append((res[0], res[1], res[2]))
        return characteristics

    def getBaseMoney(self):
        '''
        Get the total amount of money
        '''
        sql = "select pmn.pmn_montant, mnn.mnn_indice from perso_monnaie pmn \
inner join monnaie mnn on mnn.mnn_id=pmn.pmn_mnn_id where pmn.pmn_prs_id=%d" \
                                                                    % self.id
        total = 0
        for amount, idx in list(self._connection.queryAll(sql)):
            total += amount*idx
        return total

    def delMoney(self, value):
        '''
        Remove money
        '''
        amount = self.getBaseMoney()
        self.setMoney(amount-value)

    def addMoney(self, value):
        '''
        Add money
        '''
        amount = self.getBaseMoney()
        self.setMoney(amount+value)

    def setMoney(self, value):
        '''
        Set an amount of money
        '''
        for pmn in list(perso_monnaie.select('pmn_prs_id=%d' % self.id)):
            pmn.destroySelf()
        base = list(monnaie.select('mnn_indice=1'))[0]
        perso_monnaie(pmnPrsID=self.id, pmnMnnID=base.id, pmnMontant=value)

    def getInventory(self, sorted=True, visible=None, into_chest=False,
                     sized=False):
        '''
        Get the inventory of the character. If visible is set return only items
        directly visible. If intoChest return items also in a chest. If sized
        return items that have a size.
        Return :
            if sorted:
                - dict items : dict associating items categories to item list.
            else:
                - list items : available items
        '''
        items = list(Item.select('elm_prs_id=%d' % self.id))
        if sized:
            items = [item for item in items if item.getSize()]
        if not into_chest:
            sql = """select cel.cel_elm_id from coffre_element cel
inner join element elm on elm.elm_id=cel.cel_elm_id and elm.elm_prs_id=%d""" % \
                                                                         self.id
            res = list(self._connection.queryAll(sql))
            res = [r[0] for r in res]
            items = [item for item in items
                          if item.id not in res]
        if not sorted:
            if not visible:
                return items
            r_items = []
            for item in items:
                size = item.getSize()
                if (not item.elmNaturel \
                   and (item.elmEquipe or (size and size >= MIN_VISIBLE_SIZE))
                   and (not item.loader or not item.loader.rchUtilise)):
                    r_items.append(item)
            return r_items
        inventory =  {}
        for item in items:
            size = item.getSize()
            if not visible or (not item.elmNaturel \
              and (item.elmEquipe or (size and size >= config.MIN_VISIBLE_SIZE))
                   and (not item.loader or not item.loader.rchUtilise)):
                item_type = item.elmEgn.egnTelID
                if inventory.has_key(item_type):
                    inventory[item_type].append(item)
                else:
                    inventory[item_type] = [item]
        return inventory

    def getEquipedItems(self, except_natural=None):
        '''
        Get a tuple of list of equiped items : weapons and armors
        '''
        sql = 'elm_prs_id=%d and elm_equipe=True' % self.id
        if except_natural:
            sql += ' and elm_naturel=False'
        items = list(Item.select(sql))
        weapons, armors = [], []
        for item in items:
            if item.weapons:
                weapons.append(item)
            if item.armor:
                armors.append(item)
        return (weapons, armors)

    def getMoney(self):
        '''
        Get character's money
        '''
        return list(perso_monnaie.select("pmn_prs_id=%d" % self.id))

    def evalMoney(self, amount):
        '''
        Eval an amount of money for a character
        '''
        crc = self.getCharacteristic(BDD.ECONOMY_ID)
        if crc.pcrScoreCalcule:
            return amount
        return roleplayFormat(amount, MONEY_LABELS)

    def getEquipedArmors(self, except_natural=None):
        '''
        Get equiped armors
        '''
        return self.getEquipedItems(except_natural)[1]

    def getEquipedWeapons(self, except_natural=None):
        '''
        Get equiped weapons
        '''
        return self.getEquipedItems(except_natural)[0]

    def equip(self, item_id):
        '''
        Test the equipement of an item.

        Return :
            - list items : list of items who must be unequiped
            - list items : list of items whom one if them have to be unequiped
        '''
        item = Item.get(item_id)
        if not item.elmEgn.egnEquipable:
            raise "MorriganError", "Item not equipable."
        zones = []
        if item.armor:
            zones = [db_zone[0] for db_zone in item.base_armor.zones]
        if item.weapons:
            zones.append('main_1')
            for weapon in item.weapons:
                if weapon.armAgn.agnDeuxMains:
                    zones.append('main_2')

        if not zones:
            print "À corriger..."
            #return []

        items = self.getInventory(sorted=None)

        hands_equipped = []
        unequiped_items = []
        for item in items:
            if item.elmEquipe and not item.elmNaturel:
                if item.armor:
                    for db_zone in item.base_armor.zones:
                        if db_zone[0] in zones:
                            if item not in unequiped_items:
                                unequiped_items.append(item)
                if item.weapons:
                    hands_equipped.append(item)

        if 'main_1' in zones and not 'main_2' in zones:
            if len(hands_equipped) == 1:
                for weapon in hands_equipped[0].weapons:
                    if weapon.armAgn.agnDeuxMains:
                        unequiped_items += hands_equipped
                        hands_equipped = []
                else:
                    hands_equipped = []
        elif 'main_1' in zones and 'main_2' in zones:
            unequiped_items += hands_equipped
            hands_equipped = []
        else:
            hands_equipped = []
        return unequiped_items, hands_equipped

    def getMaxLoad(self):
        '''Get max character's load
        '''
        return evalFormula(constants.MAX_LOAD_FORMULA, self.id)

    def getChest(self, location=True):
        '''Get chest available in character's inventory or on the current
        location (if location set to True)
        '''
        extra = ''
        if location:
            items_visible = [str(item.id)
                            for item in self.prsLie.getObjects(self.id)]
            if items_visible:
                extra = " or elm_id in (%s)" % ",".join(items_visible)
        sql = '''select cff.cff_id from coffre cff
inner join element elm on elm.elm_id=cff.cff_elm_id and (elm.elm_prs_id=%d%s)
''' % (self.id, extra)
        res = Chest._connection.queryAll(sql)
        if not res:
            return []
        chests = []
        for r in res:
            chest_id = r[0]
            chests.append(Chest.get(chest_id))
        return chests

    def getLoad(self):
        '''Get the character's load
        '''
        sql = "select sum(egn.egn_taille) from element_gen egn \n\
inner join element elm on elm.elm_egn_id=egn.egn_id and elm.elm_prs_id=%d;" % (
                                                                        self.id)
        res = self._connection.queryOne(sql)
        if not res or not res[0]:
            return 0
        return res[0]

    def takeItem(self, item):
        '''Take an item in inventory
        '''
        load = self.getLoad()
        size = item.getSize()
        if not size or load + size > self.getMaxLoad():
            self.reason = _("%s is too heavy to be taken.") % item.label()
            return
        for weapon in item.weapons:
            # if the weapon is loaded, the loaders are give with the weapon
            if weapon.armRch:
                self.takeItem(weapon.armRch.rchElm)
        chest = list(Chest.select('cff_elm_id=%d' % item.id))
        if chest and chest[0]:
            # if the item is a chest all the item inside the chest are given
            chest_items = chest[0].getItems()
            for chest_item in chest_items:
                self.takeItem(chest_item)
        item.elmLieID = None
        item.elmPrsID = self.id
        vis = list(VisibleItem.select("elv_elm_id=%d" % item.id))
        for vi in vis:
            vi.destroySelf()
        return True

    def getKnownItems(self):
        """Return the items the character has identified
        """
        kis = list(KnownItems.select('egc_prs_id=%d' % self.id))
        return [ki.egcEgn for ki in kis]

    def formatAP(self, value):
        '''Get the pourcent of an action point cost
        '''
        if self.prsPa:
            return value*100/self.prsPa
        return 10000

    def formatFP(self, value):
        '''Get the pourcent of a fatigue point cost
        '''
        if self.prsPn:
            return value*100/self.prsPn
        return 10000

    def formatFiP(self, value):
        '''Get the pourcent of a fight point cost
        '''
        if self.prsPc:
            return value*100/self.prsPc
        return 10000

    def getDescription(self, short=None):
        '''
        Get an HTML description of the character.
            short (boolean) -- if set to True descriptions strings are not
        written
        Return :
            html (string) -- return the HTML code.
        '''
        forme = self.getCurrentShape()
        html = "<div class='nom'>%s</div>" % self.prsNomActuel
        if forme:
            if forme.frmUrlImage :
                html += '''
<img class='description' src="%s" alt='description' />''' % forme.frmUrlImage
            if not short:
                html += "\n<div class='description'>%s</div>" % \
                                                        forme.frmDescription
            html += "\n<div class='description'>%s</div>" % \
                                                        forme.frmDescActuelle
        pourc_pv = self.prsPv*100/self.prsPvMax
        html += "<dl class='character_states'>\n"
        html += "<dt>%s : </dt><dd>%s</dd>\n" % (_("Health"),
                                        roleplayFormat(pourc_pv, HEALTH_LABELS))
        pourc_pf = self.prsPn*100/self.prsPnMax
        html += "<dt>%s : </dt><dd>%s</dd>\n" % (_("Fatigue"),
                                       roleplayFormat(pourc_pf, FATIGUE_LABELS))
        html += "</dl>\n"
        visible_items = self.getInventory(sorted=None, visible=True)
        if visible_items:
            equiped_items = ""
            items = ""
            for item in visible_items:
                if item.elmEquipe:
                    equiped_items += "<li>" + item.name + "</li>"
                else:
                    items += "<li>" + item.name + "</li>"
            if equiped_items:
                html += "<div class='infoPerso'>%s :" \
                        % _("This character has got these items equiped")
                html += "<ul>" + equiped_items + "</ul></div>"
            if items:
                html += "<div class='infoPerso'>%s :" \
                        % _("This character visibly possess these items")
                html += "<ul>" + items + "</ul></div>"
        html += "<div class='spacer'>&nbsp;</div>"
        return html

    def move(self, end_location_id, start_location_id=None, path_id=None):
        '''
        Move the character to the defined place.
        '''
        extra = ''

        tails = list(Tailing.select("flt_suivant_prs_id = %d" % \
                                                              self.id))
        for tail in tails:
            track = tail.fltTflID == TRACK_TAIL
            date = ''
            if not track:
                date = tail.fltDebut
            currentRoutes = Route.getAvailableRouteToFollow(self,
                                 tail.fltSuiviFrmID, True, track, date)
            if not track:
                currentRoutes = [currentRoutes]
            failed = True
            if currentRoutes:
                for currentRoute in currentRoutes:
                    if currentRoute and \
                       currentRoute.itnArriveeLieID == end_location_id:
                        failed = False
            if failed:
                if tail.fltTflID == SIMPLE_TAIL or \
                   tail.fltTflID == FOLLOW_TAIL:
                    tailed_shape = forme.get(tail.fltSuiviFrmID)
                    followed_prs = personnage.get(tailed_shape.frmPrsID)
                    extra += _("%s is not in sight anymore.") \
                             % followed_prs.prsNomActuel + "\n"
                    if tail.fltTflID == FOLLOW_TAIL:
                        msg = _("%s is not following you anymore.") \
                            % self.prsNomActuel
                        followed_prs.actionFile.writeAction([],
                            ActionFile.FEEDBACK_ID, msg, _("Tailing"))
                elif tail.fltTflID == TRACK_TAIL:
                    extra += _("You lost some steps.") + "\n"
                tail.destroySelf()
        # reinitialise the delta with the compass rose
        if not lieu.get(end_location_id).lieCarto:
            self.prsDeltaRose = 0
        self.prsLieID = end_location_id
        # update the date of the last mouvement
        self.prsDateArrivee = DateTime.now()
        if start_location_id:
            Route.newRoute(self, path_id, start_location_id, end_location_id)
        return extra

    def getDirection(self, start, end):
        """
        With two couples of coordinates get the direction.
        """

        direction = ''
        dirY = end[1] - start[1]
        if dirY < 0:
            direction = "south"
        elif dirY > 0:
            direction = "north"
        dirX = end[0] - start[0]
        if dirX > 0:
            direction += "east"
        elif dirX < 0:
            direction += "west"
        relative_dir = DIRECTIONS[-self.prsDeltaRose:] \
                       + DIRECTIONS[:-self.prsDeltaRose]
        return DIRECTION_COST[relative_dir[DIRECTIONS.index(direction)]][1]

    def getShops(self, own=False):
        '''Get shops available in character's inventory or on the current
        location
        '''
        extra = ''
        items_visible = [str(item.id)
                         for item in self.prsLie.getObjects(self.id)]
        if items_visible:
            extra = " or elm.elm_id in (%s)" % ",".join(items_visible)
        where = ''
        prs_ids = ''
        if own:
            where = "\nwhere btq.btq_prs_id=%d" % self.id
            prs_ids = 'elm.elm_prs_id=%d' % self.id
        else:
            lst_ids = [str(prs_id) for prs_id in self.isNear(id_only=True)]
            if lst_ids:
                prs_ids = 'elm.elm_prs_id in (%s)' % ",".join(lst_ids)
            else:
                return []
        sql = '''select btq.btq_id from boutique btq
inner join coffre cff on cff.cff_id=btq.btq_cff_id
inner join element elm on elm.elm_id=cff.cff_elm_id and (%s%s)%s
''' % (prs_ids, extra, where)
        res = Shop._connection.queryAll(sql)
        if not res:
            return []
        shops = []
        for r in res:
            shop_id = r[0]
            shops.append(Shop.get(shop_id))
        return shops

    def hit(self, points, type=None, extra_msg=None):
        '''Hit the character
        extra_msg -- if set to True the message won't be displayed directly but
        stocked in the current_messages var
        '''
        if not type:
            self.lostHP(points, extra_msg)
            return points
        armors = self.getEquipedArmors()
        protection = 0
        for item in armors:
            for dmg_type in item.base_armor.damage_types:
                if dmg_type.agdTdgID == type:
                    protection += randint(dmg_type.agdEncaissementMin,
                                          dmg_type.agdEncaissementMax)
        wounds = points - protection
        if wounds > 0:
            self.lostHP(wounds, extra_msg)
            return wounds
        return 0

    def lostHP(self, points, extra_msg=None):
        """Lost some HP and perhaps die...
        extra_msg -- if set to True the message won't be displayed directly but
        stocked in the current_messages var
        """
        base_hp = self.prsPv
        self.prsPv -= points
        dead_ceil = -int(self.prsPvMax/3.0)
        if base_hp > dead_ceil and self.prsPv <= dead_ceil:
            msg = _("You are dead.")
            msg2 = _("%s is dead." % self.label())
            if extra_msg:
                self.current_messages[0].append(msg)
                self.current_messages[1].append(msg2)
            else:
                self.actionFile.writeAction([], ActionFile.FIGHT_ID, msg,
                                            action)
                others = self.isNear()
                self.actionFile.writeAction(others, ActionFile.FIGHT_ID, msg,
                                        action)
            self.die()
        elif base_hp >= 0 and self.prsPv < 0:
            msg = _("You are dying.")
            msg2 = _("%s is dying." % self.label())
            if extra_msg:
                self.current_messages[0].append(msg)
                self.current_messages[1].append(msg2)
            else:
                action = _("Dying")
                self.actionFile.writeAction([], ActionFile.FIGHT_ID, msg,
                                            action)
                others = self.isNear()
                self.actionFile.writeAction(others, ActionFile.FIGHT_ID, msg2,
                                        action)

    def die(self):
        """
        Dying
        """
        # create a dead corpse
        flesh = Material.select("mtr_nom='Organique'")[0].id
        shape = self.getCurrentShape()
        race = shape.frmRac.label().lower()
        base_item = list(BaseItem.select("egn_mtr_id=%d and egn_nom='%s'" % (
                                            flesh, race.replace("'", "''"))))
        if not base_item:
            base_item = BaseItem.create(race, material_id=flesh,
                                        size_idx=shape.frmIndiceTaille)
        else:
            base_item = base_item[0]
        desc = DEFAULT_CORPSE_DESCRIPTION % {'name':self.label(), 'race':race}
        corpse = Item.create(base_item.id, name=_("Dead corpse of %s") % \
self.label(), description=desc, location_id=self.prsLieID)
        chest = Chest.create(corpse.id, size_idx=self.getMaxLoad())
        # leave all items in the dead corpse
        items = list(Item.select('elm_prs_id=%d and elm_naturel is not True' % \
                                                                       self.id))
        for item in items:
            item.elmPrsID = None
            item.elmEquipe = False
            chest.putIn(item)
        # every fight where the character is implicated is now over...
        for takepart in self.getTakeParts():
            takepart.destroySelf()
        # teleport the soul to his last destination
        # hell of course because it is a rolist
        self.prsLieID = HELL_ID

    def getTakeParts(self):
        '''
        Update the current list of fight the character is taking part
        '''
        self.takeParts = TakePart.getByCharacter(self.id)
        return self.takeParts
Character = personnage

class joueur(SQLObject):
    """
    Player

    Main attributes
    jouPseudo (string) -- login of the player,
    jouMdp (string) -- md5 password of the player,
    jouEmail (string) -- email of the player,
    jouCssID (int) -- current stylesheet of the player.
    jouFuseau (int) -- time zone of the player
    jouCarteGrilleVisible (boolean) -- True if the grid has to be display on the
    map
    jouCarteRegionVisible (boolean) -- True if lands have to be display on the
    map
    jouCarteRouteVisible (boolean) -- True if roads have to be display on the
    map
    jouCarteLegendeVisible (boolean) -- True if legend has to be display on the
    map
    """
    class sqlmeta:
        idName = 'jou_id'
        fromDatabase = True
        table = 'joueur'
        style = sqlObject.MorriganStyle()

    @classmethod
    def checkLogin(cls, login, password):
        """
        Check if a login and password couple correspond to a player
        """
        username = sqlFormat(login)
        password = md5.new(password).hexdigest()
        query =  "jou_pseudo = '%s' and jou_mdp = '%s'" % (username, password)
        result = list(cls.select(query))
        if result:
            return result[0]

    def isAdmin(self):
        if Gamemaster.select('mji_jou_id=%d' % self.id).count():
            return True
Player = joueur


class mj(SQLObject):
    """
    Game master
    """
    class sqlmeta:
        idName = 'mji_id'
        fromDatabase = True
        table = 'mj'
        style = sqlObject.MorriganStyle()
Gamemaster = mj

class forme(SQLObject):
    """
    Shape of the character

    Main attributes
    id (integer) -- id
    frmPrsID (integer) -- id of the character
    frmRacID (integer) -- id of the race
    frmActuelle (boolean) -- True if it is the current shape of the character
    frmDescription (string) -- description (player cannot modify it)
    frmDescActuelle (string) -- current description (player cannot modify it)
    frmUrlImage (string) -- URL of a picture
    """
    class sqlmeta:
        idName = 'frm_id'
        fromDatabase = True
        table = 'forme'
        style = sqlObject.MorriganStyle()
    frmRac = ForeignKey('race')

class race(SQLObject):
    """Race of the shape

    Main attributes :
    racNom (string) -- race name
    racMldID (integer) -- id of an effect
    racDesc (string) -- description
    racMonture (boolean) -- True if it is a « monture »
    racTllID (integer) -- id of the size
    """
    class sqlmeta:
        idName = 'rac_id'
        fromDatabase = True
        table = 'race'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.racNom
