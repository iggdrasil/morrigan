#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Mapping of database objects directly related to possessions.
'''
from sqlobject.main import SQLObject, SQLObjectNotFound
from sqlobject.col import ForeignKey
from sqlobject.sqlbuilder import INNERJOINConditional, AND
import sqlObject
from constants import DAMAGE_LABELS, RANGE_LABELS, PROTECTION_LABELS
from utils import _, roleplayFormat
from utils import *

class monnaie(SQLObject):
    """
    Money
    """
    class sqlmeta:
        idName = 'mnn_id'
        fromDatabase = True
        table = 'monnaie'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.mnnNom
Money = monnaie

class perso_monnaie(SQLObject):
    """
    Character's money
    """
    class sqlmeta:
        idName = 'pmn_id'
        fromDatabase = True
        table = 'perso_monnaie'
        style = sqlObject.MorriganStyle()
    pmnMnn = ForeignKey('monnaie')

class element(SQLObject):
    '''
    Item parameters.
    Main attributes:
        name -- name of the item
        description -- description of the item
        price -- price of the item
        equipable -- capacity of being equipped
        elmEquipe -- set to True if equipped
        elmEnraye -- set to True if
        elmNaturel -- set to true if the item is a natural part of the character
        elmPrs -- character
        elmEgn -- base item
        elmLie -- location
    '''
    class sqlmeta:
        idName = 'elm_id'
        fromDatabase = True
        table = 'element'
        style = sqlObject.MorriganStyle()
    elmEgn = ForeignKey('element_gen')
    elmPrs = ForeignKey('personnage')
    elmLie = ForeignKey('lieu')

    @classmethod
    def create(cls, generic_item_id, name='', description='', price=None,
           equipable=False, natural=False, character_id=None, location_id=None):
        return cls(elmNom=name, elmDesc=description, elmPrix=price,
elmEquipable=equipable, elmEnraye=False, elmNaturel=natural,
elmPrsID=character_id, elmEgnID=generic_item_id, elmLieID=location_id)

    def _init(self, *args, **kw):
        SQLObject._init(self, *args, **kw)
        self.name = self.elmNom
        if not self.name:
            self.name = self.elmEgn.egnNom
        self.description = self.elmDesc
        if not self.description:
            self.description = self.elmEgn.egnDesc
        self.price = self.elmPrix
        if not self.price:
            self.price = self.elmEgn.egnPrix
        self.equipable = self.elmEquipable
        if not self.equipable:
            self.equipable = self.elmEgn.egnEquipable
        self.weapons = Weapon.getByItem(self.id)
        self.armor = Armor.getByItem(self.id)
        if self.armor:
            self.armor = self.armor[0]
            self.base_armor = BaseArmor.getByBaseElmId(self.elmEgn.id)
        self.loader = Loader.getByItem(self.id)
        if self.loader:
            self.loader = self.loader[0]

    def getSize(self):
        '''Get the size of an object'''
        size = self.elmEgn.egnTaille
        for weapon in self.weapons:
            if weapon.armRch:
                # manual getting of size before initialisation
                sql = "select egn_taille from element_gen egn \
inner join element elm on elm.elm_egn_id=egn.egn_id \
inner join recharge rch on rch.rch_elm_id=elm.elm_id"
                res = self._connection.queryOne(sql)
                if res and res[0]:
                    if not size:
                        size = 0
                    size += res[0]
        return size

    def equip(self, equip=True):
        '''
        Equip or unequip (if equip param set to False) the item
        '''
        if not self.equipable:
            return
        self.elmEquipe = equip

    def label(self):
        return self.name
Item = element

class element_visible(SQLObject):
    '''
    Visible items for a character.
    Main attributes:
        elvPrs -- character
        elvElm -- item
    '''
    class sqlmeta:
        idName = 'elv_id'
        fromDatabase = True
        table = 'element_visible'
        style = sqlObject.MorriganStyle()
    elvElm = ForeignKey('element')
    elvPrs = ForeignKey('personnage')
VisibleItem = element_visible

class element_gen_connu(SQLObject):
    '''
    Known items for a character.
    Main attributes:
        egcPrs -- character
        egcEgn -- generic item
    '''
    class sqlmeta:
        idName = 'egc_id'
        fromDatabase = True
        table = 'element_gen_connu'
        style = sqlObject.MorriganStyle()
    egcEgn = ForeignKey('element_gen')
    egcPrs = ForeignKey('personnage')
KnownItems = element_gen_connu

class element_gen(SQLObject):
    '''
    Item parameters.
    Main attributes:
        egnNom -- name of the base item
        egnDesc -- description of the base item
        egnTelID -- id of the type of item
        egnPrix -- price
        egnEquipable -- ability to be equiped
        egnPossessionMldID -- id of a desease due to possession
        egnEquipeMldID -- id of a desease due to equipement of the item
        egnTchID -- id of the technologie of the equipement
        egnMtrID -- material of the item
        egnTaille -- size index of the item
    '''
    class sqlmeta:
        idName = 'egn_id'
        fromDatabase = True
        table = 'element_gen'
        style = sqlObject.MorriganStyle()

    @classmethod
    def create(cls, name, description='', type_id=1, price=None,
               equipable=False, equiped_state_id=None, possession_state_id=None,
               tech_id=None, material_id=None, size_idx=5):
        return cls(egnNom=name, egnDesc=description, egnTelID=type_id,
egnPrix=price, egnEquipable=equipable, egnEquipeMldID=equiped_state_id,
egnPossessionMldID=possession_state_id, egnTcnID=tech_id, egnMtrID=material_id,
egnTaille=size_idx)
BaseItem = element_gen

class type_element(SQLObject):
    '''
    Type of items
    Main attributes:
        telNom -- name of the element
    '''
    class sqlmeta:
        idName = 'tel_id'
        fromDatabase = True
        table = 'type_element'
        style = sqlObject.MorriganStyle()
ItemType = type_element

class matiere(SQLObject):
    '''
    Material
    Main attributes:
        mtrNom -- name of the material
        mtrDesc -- description
        mtrResistance -- resistance (integer)
    '''
    class sqlmeta:
        idName = 'mtr_id'
        fromDatabase = True
        table = 'matiere'
        style = sqlObject.MorriganStyle()
Material = matiere

class taille(SQLObject):
    '''
    Size.
    Main attributes :
        tllNom -- name of the size
        tllDescription -- description of the size
        tllIndice -- level of the size
    '''
    class sqlmeta:
        idName = 'tll_id'
        fromDatabase = True
        table = 'taille'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tllNom
Size = taille

SIZE_LABELS = {}
last = 0
for size in Size.select("1=1 order by tll_indice"):
    SIZE_LABELS[tuple(range(last, size.tllIndice + 1))] = size.label()

class coffre(SQLObject):
    '''
    Chest
    '''
    class sqlmeta:
        idName = 'cff_id'
        fromDatabase = True
        table = 'coffre'
        style = sqlObject.MorriganStyle()
    cffElm = ForeignKey('element')
    cffClefEgn = ForeignKey('element_gen')

    @classmethod
    def create(cls, item_id, size_idx, opened=True, key_id=None):
        return cls(cffElmID=item_id, cffTaille=size_idx, cffOuvert=opened,
                   cffClefEgnID=key_id)

    def label(self):
        label = self.cffElm.label()
        if self.cffOuvert:
            label += " (%s)" % _("open")
        else:
            label += " (%s)" % _("closed")
        return label

    def getItems(self, chest_item=False):
        '''Get items in the chest.
        Before giving this information don't forget to verify that the chest is
        open are that the character has a correct key
        '''
        ces = coffre_element.select('cel_cff_id=%d' % self.id)
        if chest_item:
            return list(ces)
        items = []
        for ce in ces:
            items.append(ce.celElm)
        return items

    def getLoad(self):
        '''Get the chest's load
        '''
        sql = "select sum(egn.egn_taille) from element_gen egn \n\
inner join element elm on elm.elm_egn_id=egn.egn_id \
inner join coffre_element cel on cel.cel_elm_id=elm.elm_id and cel.cel_cff_id=%d"% (
                                                                        self.id)
        res = self._connection.queryOne(sql)
        if not res or not res[0]:
            return 0
        return res[0]

    def isOpen(self, character=None):
        '''
        Return True if the chest is open or if the designated character has the
        key to open it
        '''
        if self.cffOuvert:
            return True
        if not character:
            return False
        items = character.getInventory(sorted=False)
        sql = """select elm.elm_id from element elm
inner join element_gen egn on egn.egn_id=elm.elm_egn_id
inner join coffre cff on cff.cff_clef_egn_id=egn.egn_id and cff.cff_id=%d""" % (
                                                                        self.id)
        res = list(self._connection.queryAll(sql))
        keys = [r[0] for r in res]
        for item in items:
            if item.id in keys:
                return item
        return False

    def isClosed(self, character=None):
        '''
        Return True if the chest is closed or if the designated character has
        the key to close it
        '''
        if not self.cffOuvert:
            return True
        if not character:
            return False
        items = character.getInventory(sorted=False)
        sql = """select elm.elm_id from element elm
inner join element_gen egn on egn.egn_id=elm.elm_egn_id
inner join coffre cff on cff.cff_clef_egn_id=egn.egn_id and cff.cff_id=%d""" % (
                                                                        self.id)
        res = list(self._connection.queryAll(sql))
        keys = [r[0] for r in res]
        for item in items:
            if item.id in keys:
                return item
        return False

    def open(self, character):
        '''
        Try to open a chest. If it is open return True or the key used to open
        it
        '''
        key = self.isOpen(character)
        if key:
            if not self.cffOuvert:
                self.cffOuvert = True
                return key
            return True
        return False

    def close(self, character):
        '''
        Try to close a chest. If it is close return True or the key used to
        close it
        '''
        key = self.isClosed(character)
        if key:
            if self.cffOuvert:
                self.cffOuvert = False
                return key
            return True
        return False

    def _attributeTo(self, item):
        '''
        Give the same attribution to items inside a chest
        '''
        chest = list(Chest.select('cff_elm_id=%d' % item.id))
        if chest and chest[0]:
            chest_items = chest[0].getItems()
            for chest_item in chest_items:
                self._attributeTo(chest_item)
        item.elmPrsID = self.cffElm.elmPrsID
        item.elmLieID = self.cffElm.elmLieID

    def putIn(self, item):
        '''Put an item in the chest
        '''
        # can't put a chest inside himself
        if self.cffElm == item:
            return
        size = item.getSize()
        if size + self.getLoad() > self.cffTaille:
            return
        self._attributeTo(item)
        coffre_element(celCffID=self.id, celElmID=item.id)
        return True

    def takeOut(self, item):
        '''Take out an item from a chest
        '''
        ces = list(coffre_element.select("cel_cff_id=%d and cel_elm_id=%d" % (
                                                       self.id, item.id)))
        if ces:
            for ce in ces:
                ce.destroySelf()
Chest = coffre

class coffre_element(SQLObject):
    '''
    Chest
    '''
    class sqlmeta:
        idName = 'cel_id'
        fromDatabase = True
        table = 'coffre_element'
        style = sqlObject.MorriganStyle()
    celCff = ForeignKey('coffre')
    celElm = ForeignKey('element')
ChestItem = coffre_element

class automate(SQLObject):
    '''
    Robot
    '''
    class sqlmeta:
        idName = 'atm_id'
        fromDatabase = True
        table = 'automate'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.atmNom
Robot = automate

class boutique(SQLObject):
    '''
    Shop
    '''
    class sqlmeta:
        idName = 'btq_id'
        fromDatabase = True
        table = 'boutique'
        style = sqlObject.MorriganStyle()
    btqCff = ForeignKey('coffre')
    btqAtm = ForeignKey('automate')
    btqPrs = ForeignKey('personnage')

    def label(self):
        if self.btqNom:
            return self.btqNom
        return self.btqCff.label()

    def getItems(self, *args, **kws):
        '''Get associated items
        '''
        return self.btqCff.getItems(*args, **kws)
Shop = boutique

class achat_element_gen(SQLObject):
    '''
    Items available to be sell in a shop
    '''
    class sqlmeta:
        idName = 'aeg_id'
        fromDatabase = True
        table = 'achat_element_gen'
        style = sqlObject.MorriganStyle()
    aegEgnId = ForeignKey('element_gen')
    aegBtqId = ForeignKey('boutique')

    @classmethod
    def byShop(cls, shop_id):
        '''
        Get by shop id.
        '''
        return list(cls.select('aeg_btq_id=%d'% shop_id))
BuyItems = achat_element_gen

class arme_gen(SQLObject):
    '''
    Base weapon.
    Main attributes :
        agmTdgID -- id of the damage type
        agmEgnID -- id of the base item associated
        agmMldID -- id of the desease sent if succesfull
        agnPortee -- range of the weapon
        agnDegatMin -- minimum damage
        agnDegatMax -- maximum damage
        agnDeuxMains --  two hands weapon
        agnTmtID -- ammunition to use
    '''
    class sqlmeta:
        idName = 'agn_id'
        fromDatabase = True
        table = 'arme_gen'
        style = sqlObject.MorriganStyle()
    agnTmt = ForeignKey('type_munition')
    agnTdg = ForeignKey('type_degat')

    def getDamageLabel(self):
        '''
        Return a formated label for damages
        '''
        label = roleplayFormat(self.agnDegatMin, DAMAGE_LABELS)
        label += " - "
        label += roleplayFormat(self.agnDegatMax, DAMAGE_LABELS)
        return label

    def getRangeLabel(self):
        '''
        Return a formated label for range
        '''
        if not self.agnPortee:
            return "-"
        return roleplayFormat(self.agnPortee, RANGE_LABELS)

BaseWeapon = arme_gen

class arme(SQLObject):
    '''
    Weapon.
    Main attributes :
        armAgn -- base weapon
        armRch -- current ammunitions
        armElm -- associated item
    '''
    class sqlmeta:
        idName = 'arm_id'
        fromDatabase = True
        table = 'arme'
        style = sqlObject.MorriganStyle()
    armAgn = ForeignKey('arme_gen')
    armRch = ForeignKey('recharge')
    armElm = ForeignKey('element')

    def getByItem(item_id):
        wp = list(Weapon.select('arm_elm_id=%d'% item_id))
        return wp
    getByItem = staticmethod(getByItem)

    def getByOwner(character_id):
        '''
        Get by owner
        '''
        res = list(arme.select(join=INNERJOINConditional(None, element,
                               AND(arme.q.armElmID==element.q.id,
                                   element.q.elmPrsID == character_id))))
        return res
    getByOwner = staticmethod(getByOwner)

    def label(self):
        name = self.armElm.name
        if self.armAgn.agnTmt:
            name += " (%s)" % self.armAgn.agnTmt.label()
        return name

    def distant(self):
        '''Return True if the weapon is for distant fight
        '''
        if self.armAgn.agnPortee and self.armAgn.agnPortee > 1:
            return True
        return False
Weapon = arme

class armure(SQLObject):
    '''
    Armor.
    '''
    class sqlmeta:
        idName = 'aru_id'
        fromDatabase = True
        table = 'armure'
        style = sqlObject.MorriganStyle()
    def getByItem(id):
        armors =  list(Armor.select("aru_elm_id=%d" % id))
        return armors
    getByItem = staticmethod(getByItem)
Armor = armure

class armure_gen_zone(SQLObject):
    '''
    Base armor zone.
    Main attributes :
        agzUgnID -- id of a base armor
        agzZonID -- id of an armor zone
    '''
    class sqlmeta:
        idName = 'agz_id'
        fromDatabase = True
        table = 'armure_gen_zone'
        style = sqlObject.MorriganStyle()
BaseArmorZone = armure_gen_zone

class PhysicalArea(SQLObject):
    '''
    Physical zone.
    Main attribute :
        zonNom -- name of the zone
    '''
    class sqlmeta:
        idName = 'zon_id'
        fromDatabase = True
        table = 'zone_armure'
        style = sqlObject.MorriganStyle()

class armure_gen(SQLObject):
    '''
    Base armor.
    Main attribute :
        ugnEgnID -- generic element id
    '''
    class sqlmeta:
        idName = 'ugn_id'
        fromDatabase = True
        table = 'armure_gen'
        style = sqlObject.MorriganStyle()

    def _init(self, *args, **kw):
        SQLObject._init(self, *args, **kw)
        self.zones, self.protections = [], []
        zones = BaseArmorZone.select("agz_ugn_id=%d" % self.id)
        for zone in zones:
            self.zones.append((zone.agzZonID,
                               PhysicalArea.get(zone.agzZonID).zonNom))
        damages = BaseArmorDamage.select("agd_ugn_id=%d" % self.id)
        self.damage_types = list(damages)
        for damage in damages:
            self.protections.append((damage.agdTdgID,
                                     DamageType.get(damage.agdTdgID).tdgNom,
                                     damage.agdEncaissementMin,
                                     damage.agdEncaissementMax))

    @classmethod
    def getByBaseElmId(cls, id):
        try:
            return list(cls.select("ugn_egn_id=%d" % id))[0]
        except IndexError:
            return
BaseArmor = armure_gen

class armure_gen_degat(SQLObject):
    '''
    Damage absorbs by the armor.
    Main attributes:
        agdTgdID -- id of the damage type
        agdEncaissementMin -- minimum damage absorbs by the armor
        agdEncaissementMax -- maximum damage absorbs by the armor
    '''
    class sqlmeta:
        idName = 'agd_id'
        fromDatabase = True
        table = 'armure_gen_degat'
        style = sqlObject.MorriganStyle()
BaseArmorDamage = armure_gen_degat

class type_degat(SQLObject):
    '''
    Type of damages.
    Main attributes:
        tdgNom -- name of the damage type
    '''
    class sqlmeta:
        idName = 'tdg_id'
        fromDatabase = True
        table = 'type_degat'
        style = sqlObject.MorriganStyle()
DamageType = type_degat

class type_munition(SQLObject):
    '''
    Ammunition type.
    Main attributes:
        tmtNom -- name
    '''
    class sqlmeta:
        idName = 'tmt_id'
        fromDatabase = True
        table = 'type_munition'
        style = sqlObject.MorriganStyle()

    def label(self):
        return self.tmtNom
AmmunitionType = type_munition

class munition(SQLObject):
    '''
    Ammunition.
    Main attributes:
        mntNom -- name
        mntDesc -- description
        mntTmt -- associated ammunition type
    '''
    class sqlmeta:
        idName = 'mnt_id'
        fromDatabase = True
        table = 'munition'
        style = sqlObject.MorriganStyle()
    mntTmt = ForeignKey('type_munition')
Ammunition = munition

class recharge_gen(SQLObject):
    '''
    Base loader.
    Main attributes:
        cgnEgn -- associated base item
        cgnMnt -- associated ammunition
        cgnNbMaxMnt -- max number of ammos
    '''
    class sqlmeta:
        idName = 'cgn_id'
        fromDatabase = True
        table = 'recharge_gen'
        style = sqlObject.MorriganStyle()
    cgnEgn = ForeignKey('element_gen')
    cgnMnt = ForeignKey('munition')

    def getAmmoType(self):
        base_item = self.rchElm.elmEgn
        base_loader = list(BaseLoader.select("cgn_egn_id"))[0]
BaseLoader = recharge_gen

class recharge(SQLObject):
    '''
    Loader.
    Main attributes:
        rchElmID -- id of the associated item
        rchNbMnt -- number of ammos
    '''
    class sqlmeta:
        idName = 'rch_id'
        fromDatabase = True
        table = 'recharge'
        style = sqlObject.MorriganStyle()
    rchElm = ForeignKey('element')

    def getByOwner(character_id):
        '''
        Get by owner
        '''
        res = list(recharge.select(join=INNERJOINConditional(None, element,
                                   AND(recharge.q.rchElmID==element.q.id,
                                       element.q.elmPrsID==character_id))))
        return res
    getByOwner = staticmethod(getByOwner)

    def getByItem(item_id):
        loader = list(recharge.select('rch_elm_id=%d'% item_id))
        return loader
    getByItem = staticmethod(getByItem)

    def getAmmoType(self):
        '''
        Get the ammunition type
        '''
        try:
            base_item = self.rchElm.elmEgn
            base_loader = list(BaseLoader.select("cgn_egn_id=%d" %\
                                                 base_item.id))[0]
            ammunition_type = base_loader.cgnMnt.mntTmt
        except (AttributeError, IndexError, SQLObjectNotFound):
            return
        return ammunition_type

    def label(self):
        label = "%s -- %s" % (self.rchElm.name, self.getAmmoType().label())
        label += " (%d %s)" % (self.rchNbMnt, _("left"))
        return label
Loader = recharge
