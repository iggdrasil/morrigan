#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : constants.py - CREATION : 2006/11/26
#
# Copyright (C) 2006  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Basic constants

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from mygettext import _

class BDD_CONSTANTS:
    CHARISM_ID = 6
    ETIQUETTE_ID = 6
    CONSTITUTION_ID = 3
    ECONOMY_ID = 22

# if you change this value don't forget to change the associated sql function
XP_PER_POINT = 128

MAX_LOAD_FORMULA = "12 + %%(c%d)d*3" % BDD_CONSTANTS.CONSTITUTION_ID

DIRECTIONS = ['north', 'northeast', 'east', 'southeast', 'south',
                        'southwest', 'west', 'northwest']
DIRECTION_COST = {'north': (_("North"), _("the north"), 0, 1, 1),
           'south': (_("South"), _("the south"), 0, -1, 1),
           'east': (_("East"), _("the east"), 1, 0, 1),
           'west': (_("West"), _("the west"), -1, 0, 1),
           'northeast': (_("North-east"), _("the north-east"), 1, 1, 1.5),
           'northwest': (_("North-west"), _("the north-west"), -1, 1, 1.5),
           'southeast': (_("South-east"), _("the south-east"), 1, -1, 1.5),
           'southwest': (_("South-west"), _("the south-west"), -1, -1, 1.5)}

DAMAGE_LABELS = {tuple(range(2)):_('Weak'),
                 tuple(range(2,4)):_('Average'),
                 tuple(range(4,1000)):_('Serious')}

DAMAGED_LABELS = {tuple(range(2)):_('Weakly'),
                 tuple(range(2,4)):_('Averagely'),
                 tuple(range(4,1000)):_('Seriously')}

RANGE_LABELS = {tuple(range(2)):_('Low'),
                tuple(range(2,4)):_('Average'),
                tuple(range(4,1000)):_('High')}

PROTECTION_LABELS = {tuple(range(2)):_('Low'),
                     tuple(range(2,4)):_('Average'),
                     tuple(range(4,1000)):_('High')}

HEALTH_LABELS = {tuple(range(-300, -33)):_('Dead'),
                 tuple(range(-33, 0)):_('Dying'),
                 tuple(range(33)):_('Seriously wounded'),
                 tuple(range(33,66)):_('Wounded'),
                 tuple(range(66,101)):_('Good')}

FATIGUE_LABELS = {tuple(range(33)):_('Very tired'),
                  tuple(range(33,66)):_('Tired'),
                  tuple(range(66,101)):_('Good')}

VIOLENCE_LABELS = {tuple(range(2)):_('weakly'),
                   tuple(range(2,5)):_('averagely'),
                   tuple(range(5,10)):_('violently'),
                   tuple(range(10,1000)):_('very violently'),
                   }

MONEY_LABELS = {tuple(range(1)):_('Nothing'),
                tuple(range(1, 10)):_('Very little'),
                tuple(range(10, 30)):_('Little'),
                tuple(range(30, 60)):_('Moderately'),
                tuple(range(60, 100)):_('Slightly more than moderately'),
                tuple(range(100, 300)):_('Quite a lot'),
                tuple(range(300, 100000)):_('A lot'),
                }

TAKEPART_MAX = 6
SEGMENT = 5
MIN_VISIBLE_SIZE = 16
FOLLOW_HOURS_DELTA = 24

DEFAULT_CORPSE_DESCRIPTION = _("This is a corpse of a %(race)s. If you used to\
 know it, you badly recognise %(name)s.")
HELL_ID = 1699
