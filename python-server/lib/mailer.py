#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : mailer.py - CREATION : 2004/09/01
#
# Copyright (C) 2004  Etienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Définition d'une classe pour envoyer des mails.

version: 0.1
author: Etienne Loks (ELO) <etienne.loks_AT_laposte.net>

_smtpServer (string) : Adress of the SMTP server.
"""

import smtplib
import StringIO
import sys
import MimeWriter
import base64
import mimetypes
import os

_smtpServer = "localhost"
_admin = 'etienne@peacefrogs.net'
_encoding = "base64"
_charset = "utf8"


class Mailer:
    """
    Mailer.
    """
    
    def __init__(self):
        pass
    
    def send(self, From, To, Subject, msg):
        """
        send(self, From, To, Subject, msg)
        
        Send the email.
        params :
         - From (string) : From adress
         - To (string[]) : To adresses
         - Subject (string) : subject
         - msg (string) : content of the mail
        """
        smtp = smtplib.SMTP(_smtpServer)
        for to in To:
            message = StringIO.StringIO()
            writer = MimeWriter.MimeWriter(message)
            writer.addheader('From', From)
            writer.addheader('To', to)
            writer.addheader('Subject', Subject)
            body = writer.startbody('text/plain', [("charset", _charset)])
            body.write(msg)
            smtp.sendmail(From, to, message.getvalue())
        
        smtp.quit()
