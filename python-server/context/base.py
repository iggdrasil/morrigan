#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : base.py - CREATION : 2005/03/12
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Empty page in order to present only the action forms.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""

from layout.menu import Menu
from states import StateBase

class base(Menu):
    """
    Empty page in order to present only the action forms.
    """
    _current_state = StateBase.id
    _emptyPage = 1

    def writeContent(self):
        return ""
