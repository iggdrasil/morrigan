import gettext

def contextInitialize(appServer, path):
    # You could put initialization code here to be executed when
    # the context is loaded into WebKit.
    gettext.bindtextdomain('morrigan', './locale/')
    gettext.textdomain('morrigan')
    _ = gettext.gettext



