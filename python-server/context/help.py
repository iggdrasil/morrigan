#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : base.py - CREATION : 2006/03/12
#
# Copyright (C) 2006  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Help page presenting the help for available actions and states

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""

from utils import _

from layout.menu import Menu
from states import StateHelp
from actions.menu import _menu
from states import _states
from db.dbCommon import action

class help(Menu):
    """
    Help page generated for available actions and states
    """
    _current_state = StateHelp.id

    def writeContent(self):
        html = "<div class='contenu'><h1>" + _("Actions") + "</h1>"
        for section in _menu.sections:
            if self.myActionList.has_key(section.id):
                html += "<h2>" + section.label + "</h2>"
                for actionId, actionLabel in self.myActionList[section.id]:
                    html += "<h3>" + actionLabel+ "</h3>"
                    act = action.getByName(actionId)
                    if act and act.actAide:
                        html += act.actAide
                    else:
                        html += _("No help available for this action.")

        states = _states.getStates(self.session())

        html += "</div><div class='contenu'><h1>" + _("States") + "</h1>"
        for state in states:
            html += '<h2>' + state[1] + '</h2>'
            html += _states.getById(state[0]).help
        html += "</div>"
        return html
