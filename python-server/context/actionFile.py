#!/usr/bin/env python
# -*- coding: utf-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : actionFile.py - CREATION : 2004/09/01
#
# Copyright (C) 2004-2010  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Displaying action file.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
from layout.menu import Menu, _
from mailer import Mailer, _admin
from smtplib import SMTPRecipientsRefused
from utils import HTMLToText
from actionFileManager import ActionFile
from db.dbCharacters import joueur, Character
from states import StateActionFile

class actionFile(Menu):
    '''
    Displaying action file.
    '''
    _current_state = StateActionFile.id

    def awake(self, transaction):
        Menu.awake(self, transaction)
        current_display = self.session().value('current_display', None)
        if not current_display:
            current_display = 'all'
            self.session().setValue('current_display', 'all')
        self.current_display = current_display
        tz = joueur.get(self._perso.prsJouID).jouFuseau
        self.time_delta = "PT0H"
        if tz:
            self.time_delta = ""
            if tz < 0:
                time_delta = "-"
            self.time_delta += "PT%dH" % abs(tz)
        fields = self.request().fields()
        self.current_character = self._perso
        if self._perso.isAdmin():
            if 'character_id' in fields:
                self.session().setValue('character_id', fields['character_id'])
            character_id = self.session().value('character_id', None)
            try:
                self.current_character = Character.get(int(character_id))
            except:
                pass
        if 'action_file' in fields:
            self.session().setValue('action_file', fields['action_file'])
        action_file = self.session().value('action_file', None)
        self.actionFile = None
        if action_file:
            self.actionFile = ActionFile.getActionFileByDate(
                                        self.current_character, action_file)
        if not self.actionFile:
            self.actionFile = self.current_character.actionFile

    def writeHeadParts(self):
        Menu.writeHeadParts(self)
        ecmascript = '''<script type="text/javascript">
<!--
function initFa(id){
    tab = [];
    for(i=0;i<=id;i++){tab.push('af'+i);}
    hideSeveral(tab);
}
currentId = %d;
function closeAll(){
    tab = [];
    for(i=0;i<currentId;i++){tab.push('af'+i);}
    hideSeveral(tab);
}
function openAll(){
    tab = [];
    for(i=0;i<currentId;i++){tab.push('af'+i);}
    showSeveral(tab);
}
-->
</script>
'''  % (self.actionFile.getLastId() + 1)
        self.writeln(ecmascript)

    def writeFilters(self):
        filters = [(ActionFile.ALL_ID, _('All')),
(ActionFile.TALK_ID, _('Talk')), (ActionFile.ACTION_ID, _('Actions')),
(ActionFile.FIGHT_ID, _('Fight')), (ActionFile.FEEDBACK_ID, _('Feedback')),]
        html = '''<ul class='menu-top'>'''
        extra = '&amp;action_file=' + self.actionFile.getStrDate()
        for filter in filters :
            if filter[0] == self.current_display:
                html += '<li class="menu-selected"><a href="#">%s</a></li>' % \
                                                                      filter[1]
            else :
                html += '<li><a href="?_action_changeFilter=&amp;\
current_display=%s%s">%s</a></li>' % (filter[0], extra, filter[1])
        html += "</ul>"
        return html

    def writeContent(self):
        '''
        Set buttons and the action file.
        '''
        html = "<div class='contenu'>\n"
        if self._perso.isAdmin():
            html += "<div class='button'><form method='get'><p>\n"
            html += "<label for='character_id'>%s</label> \
<select id='character_id' name='character_id' \
onchange='javascript:this.form.submit()'>\n" % _("Character:")
            html += "<option value='%d'>%s</option>" % (self._perso.id,
                                                          self._perso.label())
            for character in self._perso.isNear(no_discretion=True):
                selected = ''
                if character.id == self.current_character.id:
                    selected = ' selected="selected"'
                html += "<option value='%d'%s>%s</option>" % (character.id,
                                                    selected, character.label())
            html += "</select>\n"
            html += '</p></form></div>\n'
        AFs = self.current_character.getActionFiles()
        html += "<div class='button'><form method='get'><p>\n"
        html += "<label for='action_file'>%s</label> <select id='action_file' \
name='action_file' onchange='javascript:this.form.submit()'>\n" % \
                                                               _("Action File:")
        for AF in AFs:
            selected = ''
            if str(AF) == str(self.actionFile):
                selected = ' selected="selected"'
            html += "<option value='%s'%s>%s</option>" % (AF.getStrDate(),
                                                          selected, str(AF))
        html += "</select>\n"
        html += '</p></form></div>\n'
        html += "<div class='button'><input type='button' value='%(open_all)s' \
onclick='openAll();' /> <input type='button' value='%(close_all)s' \
onclick='closeAll();' /></div>\n" % {'open_all':_("Open all"),
                                   'close_all':_("Close all")}
        html += self.writeFilters()
        html += self.actionFile.display(self.time_delta,
                                        self.current_display)
        html += '''</div><script type="text/javascript">
<!--
initFa(%d);
-->
</script>''' % self.actionFile.getLastReadId()
        html += "<div class='contenu'><form action='#' method='post'>\n"
        html += "<div class='button'>\n"
        if 'old' not in self.actionFile.file_name \
           and self._perso == self.current_character:
            html += "<input type='submit' name='_action_setAsRead' \
value='%(set_read)s' /> \n\
<input type='submit' name='_action_archive' value='%(archive)s' />\n" % {
                          'archive':_('Archive'), 'set_read':_("Set as read")}
        html += "<input type='submit' name='_action_mail' value='%s' />\n" % \
                                                                    _("Email")
        html += "</div></form>\n"
        html += "</div>"
        return html

    def actions(self):
        '''
        Actions availables :
            - changeFilter : change the current filter for the action file
            - clear : clear the action file
            - mailAndClear : mail and clear the action file
            - setAsRead : define current read message
        '''
        return Menu.actions(self) + ["changeFilter", "archive", "mail",
                                     "setAsRead"]

    def changeFilter(self):
        '''
        Change the current filter for the action file
        '''
        fields = self.request().fields()
        self.session().setValue('current_display', fields['current_display'])
        available_actions = self.actions()
        for field in fields.keys():
            if field[:8] == "_action_":
                action = field[8:]
                if action in available_actions and action != "changeFilter":
                    return eval("self." + action + "()")
        self.request().fields().clear()
        url = self.request().urlPath()
        self.application().forward(self.transaction(), url)

    def archive(self):
        '''
        Archive the action file
        '''
        self._perso.actionFile.createActionFile()
        self._perso.actionFile.closeActionFile()
        self.request().fields().clear()
        self.request().fields()['extra'] = _("Action file archived.")
        self.application().forward(self.transaction(), self.request().urlPath())

    def mail(self):
        '''
        Mail the action file
        '''
        subject = '[Morrigan] %(message)s %(character)s' % {
                              'character':self.current_character.prsNomOriginel,
                              'message':_("Action file of")}
        self.request().fields().clear()
        content = self.actionFile.display(self.time_delta,
                                          ActionFile.MAIL_ID)
        try :
            Mailer().send(_admin, [joueur.get(self._perso.prsJouID).jouEmail],
                          subject, content)
        except SMTPRecipientsRefused:
            self.request().fields()['extra'] = _("Error while sending mail.")
        else:
            self.request().fields()['extra'] = _("A copy of this action file \
has been sent to your mail box.")
        self.application().forward(self.transaction(), self.request().urlPath())

    def setAsRead(self):
        '''
        Set messages as read.
        '''
        self.request().fields().clear()
        self._perso.actionFile.setAsRead()
        self.application().forward(self.transaction(), self.request().urlPath())
