/*
#######################################################################
# PROJECT : Morrigan - FILE : tools.js - CREATION : 2005/03/12
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################
*/

function hide(id){
	if(document.getElementById){
        element = document.getElementById(id);
    } else if(document.all) {
        element = document.all[id];
    } else return;

    if(element.style) {
    	element.style.display = 'none';
	}
}

function show(id){
	if(document.getElementById){
        element = document.getElementById(id);
    } else if(document.all) {
        element = document.all[id];
    } else return;

    if(element.style) {
    	element.style.display = 'block';
    }
}

function showHide(id) {
    if(document.getElementById){
        element = document.getElementById(id);
    } else if(document.all) {
        element = document.all[id];
    } else return;
    
    if(element.style) {
        if(document.getElementById(id).style.display == 'none' || document.getElementById(id).style.display == ''){show(id);}
        else{hide(id);}
    }
}

function checkShowHide(input, id){
    if (input.checked==true){
        show(id);
    }else{
        hide(id);
    }
}
