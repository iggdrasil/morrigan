#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : inventory.py - CREATION : 2004/09/01
#
# Copyright (C) 2004-2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Psychical and materials items of the character.

author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
from layout.menu import Menu
from states import StateInventory
from db.dbInventory import ItemType, SIZE_LABELS
from constants import PROTECTION_LABELS
from utils import _, roleplayFormat
from templates import tplInventory, tplMenu
import plugins.configuration as config


HTML_BULLET = '*'

class inventory(Menu):
    '''
    Psychical and materials items of the character.
    '''
    _current_state = StateInventory.id

    def writeContent(self):
        inventory = self._perso.getInventory()
        main_dct = {'content':''}
        max_load = self._perso.getMaxLoad()
        load = max_load - self._perso.getLoad()
        main_dct['load'] = self.defineBar(load, max_load, _("Load"), True)

        money_tpl = ''
        my_money = self._perso.getMoney()
        if my_money:
            money_detail = '<ul>\n'
            for money in my_money:
                money_detail += "<li>%s : %s</li>\n" % (money.pmnMnn.label(),
                                       self._perso.evalMoney(money.pmnMontant))
            money_detail += "</ul>\n"
            money_tpl = tplMenu.state_bar % {'name':_("Money"),
                                  'id':" id='money'", 'definition':money_detail}
        main_dct['money'] = money_tpl
        if not inventory:
            main_dct['content'] = _('No items available.')
            html = tplInventory.main % main_dct
            return html
        categories = inventory.keys()
        categories.sort()
        # write items by categories
        for category in inventory.keys():
            weapons_items, armor_items = None, None
            table_dct = {'title':ItemType.get(category).telNom, 'lines':"",
                         'Name':_('Name'), 'Description':_("Description"),
                         'Size':_("Size")}
            tpl_line = tplInventory.object_line
            tpl_table = tplInventory.object_table
            for item in inventory[category]:
                line_dct = {'size':''}
                size = item.getSize()
                if size:
                    if config.FULL_RP:
                        line_dct['size'] = roleplayFormat(size, SIZE_LABELS)
                    else:
                        line_dct['size'] = size
                line_dct['equipped'] = ""
                line_dct['style'] = ""
                if item.elmEquipe:
                    line_dct['equipped'] = HTML_BULLET
                    line_dct['style'] = " class='item_equipped'"
                line_dct['name'] = item.name
                line_dct['description'] = item.description
                line_dct['armor'] = ''
                if item.loader:
                    if item.loader.rchUtilise:
                        line_dct['equipped'] = HTML_BULLET
                        line_dct['style'] = " class='item_equipped'"
                    line_dct['name'] = item.loader.label()
                elif item.weapons:
                    # weapon table
                    if not weapons_items:
                        tpl_line = tplInventory.weapon_line
                        tpl_table = tplInventory.weapon_table
                        table_dct.update({'Range':_('Range'),
                            'Damage':_('Damage'), 'Two_hands':_('Two hands'),
                            'Loaded':_("Loaded")})
                        weapons_items = True
                    rg, damage, two_hands, loaded = [], [], [], []
                    for weapon in item.weapons:
                        base_weapon = weapon.armAgn
                        rg.append(base_weapon.getRangeLabel())
                        damage.append(base_weapon.getDamageLabel())
                        if base_weapon.agnDeuxMains:
                            two_hands.append(HTML_BULLET)
                        else:
                            two_hands.append('')
                        if weapon.armRch:
                            loaded.append(weapon.armRch.label())
                        else:
                            loaded.append('')
                    line_dct['range'] = "<br/>".join(rg)
                    line_dct['damage'] = "<br/>".join(damage)
                    line_dct['two_hands'] = "<br/>".join(two_hands)
                    line_dct['loaded'] = "<br/>".join(loaded)
                elif item.armor:
                    # armor table
                    if not armor_items:
                        tpl_line = tplInventory.armor_line
                        tpl_table = tplInventory.armor_table
                        armor_items = True
                        table_dct.update({'Zone':_('Zone'), 'Type':_("Type"),
                            'Range':_("Range")})
                    zone = []
                    for db_zone in item.base_armor.zones:
                        zone.append(db_zone[1])
                    line_dct['zone'] = "<br/>".join(zone)
                    line_dct['range'] = ""
                    line_dct['type'] = ""
                    for db_protec in item.base_armor.protections:
                        if line_dct['range']:
                            line_dct['range'] += "<br/>"
                            line_dct['type'] += "<br/>"
                        line_dct['range'] += "%s - %s" % (
                               roleplayFormat(db_protec[2], PROTECTION_LABELS),
                               roleplayFormat(db_protec[3], PROTECTION_LABELS))
                        line_dct['type'] += db_protec[1]
                table_dct['lines'] += tpl_line % line_dct
            main_dct['content'] += tpl_table % table_dct
        chests = self._perso.getChest(location=False)
        if chests:
            main_dct['content'] += tplInventory.object_table_chest_head % {
                                                            'title':_("Chests")}
            tpl_table = tplInventory.object_table_chest
            for chest in chests:
                label = chest.label()
                table_dct = {'title':label, 'lines':'',
                         'Name':_('Name'), 'Description':_("Description"),
                         'Size':_("Size")}
                lines = ''
                if not chest.isOpen(self._perso):
                    label = _("Closed and you don't have the key to open it.")
                    lines = "<tr><td colspan='3'>%s</td></tr>" % label
                    table_dct['lines'] = lines
                    main_dct['content'] += tpl_table % table_dct
                    continue
                for item in chest.getItems():
                    line_dct = {'size':''}
                    size = item.getSize()
                    if size:
                        line_dct['size'] = roleplayFormat(size, SIZE_LABELS)
                    line_dct['name'] = item.name
                    line_dct['description'] = item.description
                    lines += tplInventory.basic_line % line_dct
                table_dct['lines'] = lines
                main_dct['content'] += tpl_table % table_dct
        return tplInventory.main % main_dct
