#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################
# PROJECT : Morrigan - FILE : securePage.py - CREATION : 2004/09/01
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Authentification page.
Each page needing authentification has to overload this page.
Login and logout action are define here.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

import string, types
import md5
from commands import getoutput
from re import sub
from types import *

from sitePage import SitePage, _
from db.dbCharacters import personnage, joueur
from utils import sqlFormat
from actions.menu import _menu

class securePage(SitePage):
    '''
    Authentification page.
    Each page needing authentification has to overload this page.
    Login and logout action are define here.
    '''

    def writeHTML(self):
        '''
        - redirect to the current page if the user is authentificated
        - define login & logout actions
        - redirect to the login page, if necessary
        '''
        session = self.session()
        request = self.request()
        trans = self.transaction()
        fields = request.fields()
        app = self.application()

        # Get login id and clear it from the session
        # Are they logging out ?
        if request.hasField('_action_logout'):
            # They are logging out.  Clear all session variables.
            prs_id = session.value('prs_id', None)
            if fields['_action_logout'] != '0':
                try:
                    pers = personnage.get(int(prs_id))
                    joueur.get(pers.prsJouID).jouSessionID = None
                except:
                    pass
            session.values().clear()
            request.fields().clear()
            request.fields()['extra'] = _('You have been logged out.')
            self.sendRedirectAndEnd('base')
            return
        elif request.hasField('_action_login') and request.hasField('username')\
             and request.hasField('password'):
            self.loginAction()
        # They aren't logging in; are they already logged in?
        elif session.value('authenticated_user', None):
            # They are already logged in; write the HTML for this page.
            SitePage.writeHTML(self)
        elif request.hasField('_action_destroy') and request.hasField('jou_id')\
            and request.hasField('session_id'):
            # request for destroying an old session
            jou = joueur.get(int(fields['jou_id']))
            if jou.jouSessionID == fields['session_id']:
                jou.jouSessionID = None
                if 'username' in fields and 'password' in fields:
                    self.loginAction()
                else:
                    self.forward('loginPage')
            else:
                self.forward('loginPage')
            return
        else:
            # They need to log in.
            session.values().clear()
            request.fields().clear()
            self.forward('loginPage')
            return

    def loginAction(self):
        session = self.session()
        request = self.request()
        fields = self.request().fields()
        loginid = session.value('loginid', None)
        if loginid: session.delValue('loginid')
        # They are logging in.  Clear session
        session.values().clear()
        # Check if this is a valid user/password
        username = fields['username']
        password = fields['password']
        id, extra_id = self.isValidUserAndPassword(username, password)

        if id >= 0 and fields['loginid'] == loginid:
            self.initSession(id, username, extra_id)
            #self.forward('base')
            self.sendRedirectAndEnd('location')
        elif extra_id and type(extra_id) == tuple and len(extra_id) == 2:
            # old session already active
            request.fields().clear()
            request.fields()['jou_id'] = extra_id[0]
            request.fields()['session_id'] = extra_id[1]
            fields['base_username'] = username
            fields['base_password'] = password
            self.forward('loginPage')
            return
        else:
            # Failed login attempt; have them try again
            request.fields().clear()
            request.fields()['extra'] = _('Login failed. Please try again.')
            self.forward('loginPage')
            return

    def isValidUserAndPassword(self, username, password):
        '''
        Verify user & password validity

        Attributes :
        string username -- User name
        string password -- Password

        Return :
        integer -- character id
        '''

        username = sqlFormat(username)
        password = md5.new(password).hexdigest()
        query =  "jou_pseudo = '%s' and jou_mdp = '%s'" % (username, password)
        result = list(joueur.select(query))
        if result :
            if result[0].jouSessionID:
                return -1, (result[0].id, result[0].jouSessionID)
            query =  "prs_jou_id = %d and prs_principal = True" % result[0].id
            r = list(personnage.select(query))
            if r:
                return r[0].id, result[0].id
            else:
                return -1, None
        else:
            return -1, None

    def initSession(self, id, username, player_id):
        self.request().fields().clear()
        self.session().values().clear()
        # Success; log them in and send the page
        self.session().setValue('authenticated_user', username)
        self.session().setValue('prs_id', id)
        joueur.get(player_id).jouSessionID = self.session().identifier()
        fortune = sub('\\n', '<br />', getoutput("fortune"))
        self.request().fields()['extra'] = fortune
        _menu.evaluateMenu(self.session(), id)
