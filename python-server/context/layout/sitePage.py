#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : SitePage.py - CREATION : 2004/09/01
#
# Copyright (C) 2004-2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Base page of the site

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from WebKit.Page import Page
from utils import _
import plugins.configuration as config
from db.dbCharacters import joueur
from db.dbCommon import Css

class SitePage(Page):
    '''
    Base page of the site
    '''

    _pageName = 'Morrigan'
        
    def awake(self, transaction):
        ''''
        Awake of the page. Set unicode encoding.
        '''
        Page.awake(self, transaction)
        self.setEncoding("utf-8")
        
    def writeHTML(self):
        '''
        Write the whole document.
        '''
        self.writeDocType()
        self.writeln('<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg">')
        self.writeHead()
        self.writeBody()
        self.writeln('</html>')

    def setEncoding(self, encoding = "latin-1"):
        '''
        Sent in header the desired encoding
        '''
        self._encoding = encoding
        self._response.setHeader("Content-type", "%s; charset=%s" % ('text/html', encoding)) 

    def htBodyArgs(self):
        '''
        Return args to include in the body
        '''
        return ''
    
    def writeDocType(self):
        '''
        Write the doc type of the document.
        '''
        self.writeln('''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">''')

    def title(self):
        '''
        Get the main title of the page.
        '''
        return '~ To be defined ... ~'
    
    def writeStyleSheet(self):
        '''
        Write the different stylesheets
        '''
        if not self.session().value('stylesheet', None):
            css = None
            if hasattr(self, '_perso') and self._perso:
                jou = joueur.get(self._perso.prsJouID)
                if jou.jouCssID:
                    css = Css.get(jou.jouCssID)
                    self.session().setValue('stylesheet', (css.cssChemin, css.cssNom))
            if not css:
                css = list(Css.select("css_defaut=TRUE"))[0]
                self.session().setValue('stylesheet', (css.cssChemin, css.cssNom))
        self.writeln('''<link href="%s" rel="stylesheet" type="text/css" title="%s" />''' % self.session().value('stylesheet', None))
    
    def writeHeadParts(self):
        '''
        Write the head parts of the document including the content-type
        '''
        Page.writeHeadParts(self)
        self.writeln('''<meta http-equiv="content-type" content="text-html; charset=utf-8" />''')

    def writeBodyParts(self):
        '''
        Defining some border and some area in the body parts.
        '''
        self.writeln('''<div class='cadre'>
<div class='page'>
<div class='head'>%s</div>
<div class='principal'>''' % self._pageName)
        extra = self.request().field('extra', None)
        if extra:
            self.writeln("<div class='extra'>%s</div>" % self.request().field('extra'))
        self.writeContent()
        self.writeln('</div>')
        self.writeBottom()
        self.writeln('</div></div>')

    def writeContent(self):
        '''
        Write the main content of the page : to be overloaded.
        '''
        pass

    def writeBottom(self):
        '''
        Write the bottom of the page : to be overloaded.
        '''
        pass

    def actions(self):
        '''
        Get the action of the page.
        '''
        return Page.actions(self)
