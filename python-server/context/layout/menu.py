#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : menu.py - CREATION : 2004/09/01
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Class to implement in order to get basic interface.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from securePage import securePage, _
from db.dbCharacters import personnage, joueur
from actions.menu import _menu
from states import _states, StateBase
from templates import tplMenu as tpl
import plugins.configuration as config

class Menu(securePage):

    _current_state = ''
    _pageName = 'Morrigan'

    def awake(self, transaction):
        securePage.awake(self, transaction)
        prs_id = self.session().value('prs_id', None)
        self._menu = _menu
        if prs_id:
            self._perso = personnage.get(prs_id)
            if '_action_logout' not in self.request().fields() and \
joueur.get(self._perso.prsJouID).jouSessionID != self.session().identifier():
                self.sendRedirectAndEnd('base?_action_logout=0')
            state = _states.getById(self._current_state)
            if state:
                if not state().isAvailable(self._perso):
                    self._menu.evaluateMenu(self.session(), prs_id)
                    self.application().forward(transaction, 'base')
        elif hasattr(self, '_current_state') and \
             self._current_state != StateBase.id:
            self.sendRedirectAndEnd('base')

    def writeHeadParts(self):
        securePage.writeHeadParts(self)
        self.myActionList = _menu.getActionList(self.session())
        if not self.myActionList:
            if not self.request().fields().has_key('_action_logout'):
                self.sendRedirectAndEnd('base?_action_logout=1')
            return
        tab = "['" + "', '".join([key for key in self.myActionList]) + "']"
        self.writeln(tpl.head % {'js_path':config.JS_PATH, 'initTab':tab})

    def htBodyArgs(self):
        return 'onload="hideSeveral(initTab)"'

    def writeBodyParts(self):
        extra_content = ''
        if self.request().field('extra', None):
            extra_content = tpl.content % "<div class='extra'>%s</div>" % \
                                        self.request().field('extra')
        action_content = ''
        curAction = self.request().field('curAction', None)
        if curAction:
            action_content = self.writeAction(curAction)
        dct = {'page_name':self._pageName,
               'state_menu':self.writeStateMenu(),
               'character_menu':self.writeCharacterMenu(),
               'action_menu':self.writeActionMenu(),
               'extra':extra_content,
               'action_content':action_content,
               'content':self.writeContent(),
               'bottom':self.writeBottom()}
        self.writeln(tpl.main % dct)

    def writeContent(self):
        pass

    def writeStateMenu(self):
        states_content = ''
        states = _states.getStates(self.session())
        for state in states:
            selected = ''
            if self._current_state == state[0] :
                selected = " class='menu-selected'"
            states_content += "<li%s><a href='%s'>%s</a></li>" % (selected,
                                                         state[0], state[1])
        return tpl.state_menu % {'states':states_content}

    def writeAction(self, action):
        myAction = _menu.getActionDict()[action]()
        return tpl.content % myAction.getForm(self._perso,
                                              self.request().fields())

    def writeCharacterMenu(self):
        image = ''
        forme = self._perso.getCurrentShape()
        if forme and forme.frmUrlImage :
            image += """<img class='descMenu' src="%s" alt='' />""" % \
                                                        forme.frmUrlImage
        dct = {'name':self._perso.prsNomActuel,
               'image':image,
               'action_points':self.defineBar(self._perso.prsPa,
                                self._perso.prsPaMax, _('Action points')),
               'health_bar':self.defineBar(self._perso.prsPv,
                                self._perso.prsPvMax, _("Health")),
               'fatigue_bar':self.defineBar(self._perso.prsPn,
                                self._perso.prsPnMax, _("Fatigue"), True),
               'fight_bar':self.defineBar(self._perso.prsPc,
                                self._perso.prsPcMax, _("Fight ability")),
               }
        return tpl.character_menu % dct

    def writeActionMenu(self):
        dct = {"action_label":_("Actions"),
               "logout_label":_("Logout")}
        items = ""
        basePath = self.request().urlPath().split('/')[-1]
        for sectionId, sectionLabel in _menu.getSections():
            if self.myActionList.has_key(sectionId):
                # empty label for a section mean a root action
                if sectionLabel != '':
                    items += tpl.menu_section % {'section_id':sectionId,
                                                 'section_label':sectionLabel}
                for actionId, actionLabel in self.myActionList[sectionId]:
                    items += tpl.menu_item % {'base_path':basePath,
                           'action_id':actionId, 'action_label':actionLabel}
                if sectionLabel != '':
                    items += tpl.end_menu_section
        dct['items'] = items
        return tpl.action_menu % dct

    def writeBottom(self):
        return tpl.bottom

    def actions(self):
        return securePage.actions(self) + ["logout", "action", "curAction"]

    def logout(self):
        self.application().forward(self.transaction(), 'layout/securePage')

    def action(self):
        curAction = self.request().fields()['_action_action']
        self.request().fields().clear()
        self.request().fields()['curAction'] = curAction
        self.application().forward(self.transaction(), self.request().urlPath())

    def curAction(self):
        fields = self.request().fields()
        if 'curAction' not in fields:
            return
        curAction = fields['curAction']
        if type(curAction) == list:
            curAction = curAction[0]
        myAction = _menu.getActionDict()[curAction]()
        result, fields = myAction.do(self._perso, self.request().fields(),
                                     self.session(), self._menu)
        myfields = {}
        if fields:
            if type(fields) == dict:
                myfields = fields
            else:
                for field in fields:
                    if self.request().fields().has_key(field):
                        myfields[field] = self.request().fields()[field]
        self.request().fields().clear()
        self.request().fields()['extra'] = result
        if fields:
            for field in fields:
                if myfields.has_key(field):
                    self.request().fields()[field] = myfields[field]
        pref_util_const = 1
        if pref_util_const == 1:
            self.request().fields()['curAction'] = myAction._id
        self.application().forward(self.transaction(), self.request().urlPath())

    def defineBar(self, current, max, message, reverse=None):
        if reverse:
            current = max - current
        definition = "<img class='%(barStartImage)s' \
src='%(image)s' alt='%(start)d' />%(ratioFull)s%(ratioEmpty)s\
<img class='%(barEndImage)s' src='%(image)s' alt='100' />"
        name = "%(message)s"
        if not config.FULL_RP:
            name += " %(current)s / %(max)s"
        bar = tpl.state_bar % {'definition':definition, 'name':name, 'id':''}
        image = config.IMAGE_PATH + "empty.png"
        barStartImage = 'barStartImageFull'
        barEndImage = 'barEndImageEmpty'
        ratioFull = ""
        ratioEmpty = ""
        ratio = 0

        if current == max:
            barEndImage = 'barEndImageFull'
            ratio = 100
            ratioFull = "<img class='barMidImageFull' src='%s' width='%dpx' \
alt=' / ' />" % (image, 100)
            ratioEmpty = ""
        elif current > 0:
            ratio = int(round(current*100.0/max))
            ratioFull = "<img class='barMidImageFull' src='%s' width='%dpx' \
alt=' / ' />" % (image, ratio)
            ratioEmpty = "<img class='barMidImageEmpty' src='%s' width='%dpx' \
alt='' />" % (image, 100 - ratio)
        else:
            ratio = 0
            barStartImage = 'barStartImageEmpty'
            ratioEmpty = "<img class='barMidImageEmpty' src='%s' width='%dpx' \
alt='' />" % (image, 100)

        return str(bar % {'message':message, 'image':image ,
'start':ratio, 'ratioFull':ratioFull, 'ratioEmpty':ratioEmpty,
'current':current, 'max':max, 'barStartImage':barStartImage,
'barEndImage':barEndImage})
