#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : inventory.py - CREATION : 2005/05/06
#
# Copyright (C) 2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Page displaying attributes and skills of the character.

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""
from layout.menu import Menu
from utils import _
from states import StateCharacteristics

class characteristics(Menu):
    '''
Page displaying attributes and skills of the character.'''

    _current_state = StateCharacteristics.id

    def writeContent(self):
        characteristics = self._perso.getCharacteristics()
        shtml = ""
        type_characteristics = [('Attribut', _('Attributes')), \
                                ('Competence', _('Skills'))]
        shtml += "<div class='contenu'>"
        for type_characteristic in type_characteristics:
            if characteristics.has_key(type_characteristic[0]):
                empty = True
                type = "<h1>%s</h1>" % type_characteristic[1]
                for cat_characteristic in \
                                        characteristics[type_characteristic[0]]:
                    type += '''<div class='category'><h2>%s</h2>
    <table>''' % cat_characteristic
                    for characteristic in \
                   characteristics[type_characteristic[0]][cat_characteristic]:
                        if characteristic[1] > 0:
                            empty = None
                            type += '''<tr><td>%s</td><td>%d</td></tr>''' % \
                                        (characteristic[0], characteristic[1])
                    type += "</table></div>"
                if not empty:
                    shtml += type
                    shtml += "<div class='spacer'>&nbsp;</div>"
        shtml += "</div>"
        return shtml
