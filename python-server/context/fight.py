#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : fight.py
#
# Copyright (C) 2008  Etienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Fight page

@version: 0.1
@author: U{Etienne Loks (ELO) <etienne.loks_AT_laposte.net>}
"""
from layout.menu import Menu
from states import StateFight
from templates import tplFight
from utils import _

from db.dbFight import TakePart

class fight(Menu):
    """
    Fight page
    """
    _current_state = StateFight.id

    def writeContent(self):
        content = ''
        # update of personnal takeparts
        self._perso.getTakeParts()
        character_takeparts = {}
        tps = TakePart.getByLocation(self._perso.prsLieID)
        if not tps:
            content = _("No fight in this place.")
            return tplFight.main % {'content':content}
        for takepart in tps:
            initiative = takepart.engInit1
            opponent = takepart.engPrs2Prs
            # first member of the list : the character
            # second member of the list : a tuple with initiative boolean
            # distant fight boolean and opponent name
            if takepart.engPrs1Prs.id not in character_takeparts:
                character_takeparts[takepart.engPrs1Prs.id] = \
                                                       [takepart.engPrs1Prs, []]
            character_takeparts[takepart.engPrs1Prs.id][1].append(
                                (takepart.engInit1, takepart.engDistant,
                                 takepart.engPrs2Prs.label()))
            if takepart.engPrs2Prs.id not in character_takeparts:
                character_takeparts[takepart.engPrs2Prs.id] = \
                                                       [takepart.engPrs2Prs, []]
            character_takeparts[takepart.engPrs2Prs.id][1].append(
                                (not takepart.engInit1, takepart.engDistant,
                                 takepart.engPrs1Prs.label()))
        lines = []
        init_msg = _("%s has got the initiative.")
        taking_part_msg = _("%s is taking part in a:")
        melee_msg = _("Melee fight with %s. ")
        distant_msg = _("Distant fight with %s. ")
        for character_id in character_takeparts:
            character, takeparts = character_takeparts[character_id]
            line_dct = {}
            line_dct['desc'] = character.getDescription(True)
            line_dct['Taking_part'] = taking_part_msg % character.label()
            take_part_list = []
            for init, distant, opponent in takeparts:
                string = ""
                if distant:
                    string += distant_msg % opponent
                else:
                    string += melee_msg % opponent
                if init:
                    string += init_msg % character.label()
                else:
                    string += init_msg % opponent
                take_part_list.append(string)
            line_dct['take_part_list'] = "<li>%s</li>" % \
                                         "</li>\n<li>".join(take_part_list)
            if character.id == self._perso.id:
                lines.insert(0, tplFight.line % line_dct)
            else:
                lines.append(tplFight.line % line_dct)
        return tplFight.main % {'content':"\n".join(lines)}
