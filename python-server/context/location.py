#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : location.py - CREATION : 2004/09/01
#
# Copyright (C) 2004-2005  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Display of current location and near characters

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""
import plugins.configuration as config

from layout.menu import Menu, _
from states import StateLocation
from db.dbGeography import lieu, secteur, monde

class location(Menu):
    """
    Display of current location and near characters
    """

    _current_state = StateLocation.id

    def awake(self, transaction):
        mylist = []
        Menu.awake(self, transaction)

    def writeHeadParts(self):
        Menu.writeHeadParts(self)
        ecmascript = '''<script type="text/javascript">
<!--
tabsElement = ['%s'];
function changeTab(id){
    except = id;
    hideSeveral(tabsElement);
    show(id);
    for (i=0;i<tabsElement.length;i++){
        element = tabsElement[i] + '_tab'
        if(document.getElementById(element)){
            document.getElementById(element).className = 'classicTab'
        }
    }
    element = id + '_tab';
    if(document.getElementById(element)){
        document.getElementById(element).className = 'selectedTab'
    }
}
-->
</script>''' % "','".join(['desc_item_%d' % i for i in xrange(10)])
        self.writeln(ecmascript)

    def writeContent(self):
        """
        Affichage des descriptions du lieu et des personnages.
        """

        string = ""
        if self._perso.prsLieID:
            string += self.writeLocation()
        else:
            string += self.writeLand()

        string += '''<script type="text/javascript">
<!--
changeTab('location', 'spacerLoca');
-->
</script>'''

        liste = []
        liste.append(self._perso)
        liste += self._perso.isNear()

        dependPersos = []
        mainPersos = []

        for perso in liste:
            if perso.prsPossesseurPrsID > 0:
                dependPersos.append(perso)
            else :
                mainPersos.append(perso)

        persosDecrits = []

        if len(mainPersos) > 0:
            string += "<div class='contenu'>"

        for perso in mainPersos:
            string += "<div class='perso'>"
            string += perso.getDescription()
            first = True
            for dPerso in dependPersos:
                if dPerso.prsPossesseurPrsID == perso.id:
                    if first :
                        first = None
                        string += "<div class='infoPerso'>%s :</div>" % \
                                        _("This character has got near him")
                    string += "<div class='dependPerso'>"
                    persosDecrits.append(dPerso)
                    string += dPerso.getDescription()
                    string += "</div>"
            string += "</div>"

        for perso in dependPersos:
            if perso not in persosDecrits:
                string += "<div class='perso'>"
                string += perso.getDescription()
                string == "</div>"
        string += '</div>'
        return string

    def writeLocation(self):
        location = lieu.get(self._perso.prsLieID)
        string = """<div class='contenu'><div class="container">
"""
        if location.lieImg:
            label = str(location.lieImg.label().replace('"', '\\"'))
            string += """<div id='location_img'>
<img alt="%s" src='%s%s'/>
</div>
""" % (label, config.IMAGE_PATH, location.lieImg.imgAdresse)

        string += """<div id='location_desc'>&nbsp;<dl class="tab">"""
        desc_items = location.getDescription(self._perso)
        if location.lieSctID:
            sector = secteur.get(location.lieSctID)
            desc_items.insert(0, (sector.sctNom, sector.sctDescription))
        i = 0
        TPL = '''<dt onclick="changeTab('desc_item_%d');" \
id='desc_item_%d_tab'>%s</dt>
<dd class='content' id='desc_item_%d'>%s</dd>'''
        for label, description in desc_items:
            string += TPL % (i, i, label, i, description)
            i += 1
        string += "</dl></div>\n<div class='spacer'>&nbsp;</div>\n"
        items = location.getObjects(self._perso.id)
        if items:
            string += "<p>%s</p>\n" % _("You can see in this place:")
            string += "<ul class='items'>\n"
            for item in items:
                string += "<li>%s: %s</li>" % (item.label(),
                                               item.description)
            string += "</ul>\n"
        string += "</div></div>"
        return string

    def writeLand(self):
        '''
        myRegion = region.getLand(self._perso.prsMndID, self._perso.prsLocalisation)
        myWorld = monde.get(self._perso.prsMndID)

        string = """<div class='contenu'><div class="container">
<dl class="tab">
<dt onclick="changeTab('secteur', 'spacerLoca');" id='secteurTab'>%(nomSecteur)s</dt>
<dd class='content' id='secteur'>%(descSecteur)s</dd>
<dt onclick="changeTab('location', 'spacerLoca');" id='locationTab'>%(nom)s</dt>
<dd class='content' id='location'>%(desc)s</dd>
</dl>
<div class='spacer' id='spacerLoca'>&nbsp;</div>
</div></div>""" % {'nomSecteur':myWorld.mndNom, 'descSecteur':\
myWorld.mndDescription, 'nom':myRegion.rgnNom, 'desc':myRegion.rgnDescription}
        '''
        return "TODO..."
