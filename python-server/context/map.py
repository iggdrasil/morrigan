#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : map.py - CREATION : 2005/01/16
#
# Copyright (C) 2004  Etienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Map of the known world.

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
"""
from layout.menu import Menu, _
import plugins.configuration as config
from db.dbGeography import monde
from db.dbCharacters import joueur
from utils import _, pointSplit
from states import StateMap

class map(Menu):
    """
    Map of the known world.
    """

    _current_state = StateMap.id
    
    def writeStyleSheet(self):
        Menu.writeStyleSheet(self)
        self.writeln('''<link href="%s" rel="stylesheet" type="text/css" title="svg-stylesheet" />''' % (config.CSS_PATH+'svg-style.css'))
    
    def setEncoding(self, encoding = "latin-1"):
        '''Sent in header the desired encoding'''
        self._encoding = encoding
        self._response.setHeader("Content-type", "%s; charset=%s" % ('application/xhtml+xml', encoding)) 

    def writeContent(self):
        if not self._perso.prsLocalisation :
            return
        world = monde.get(self._perso.prsMndID)
        img = '''<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height ="400px" width="400px">'''
        player = joueur.get(self._perso.prsJouID)
        img += world.getMap(player.jouCarteGrilleVisible, player.jouCarteRegionVisible, player.jouCarteRouteVisible, player.jouCarteLegendeVisible)
        x, y = pointSplit(self._perso.prsLocalisation)
        x = world.padding + world.delta*(x+0.5)
        y = world.padding + world.delta*(y+0.5)
        ray = world.delta*0.4
        img += '''<circle cx="%d" cy="%d" r="%d" fill="blue"/>''' % (x, y, ray)
        img += "</svg>"
        html = "<div class='contenu'><div class='map'>"
        self.writeln(html)
        html = "<div class='map-content' style='background-repeat: no-repeat;background-image:url(%s);width:400px;height:400px;'>"  % (config.IMAGE_PATH+world.mndImage)
        self.writeln(html)
        self.writeln(img)
        self.writeln("</div><div class='legend'>")
        selected = ''
        if player.jouCarteGrilleVisible == True:
            selected = " checked='checked'"
        html = '''<form action='#' method='post'><ul class='checklist'><li><input id='check-svg-grid' name='check-svg-grid' %s type='checkbox' onchange="javascript:checkShowHide(this, 'svg-grid');"/><label for='check-svg-grid'>%s</label></li>''' % (selected, _("Show/hide grid"))
        selected = ''
        if player.jouCarteRegionVisible == True:
            selected = " checked='checked'"
        html += '''<li><input id='check-svg-land' name='check-svg-land'%s type='checkbox' onchange="javascript:checkShowHide(this, 'svg-land');"/><label for='check-svg-land'>%s</label></li>''' % (selected, _("Show/hide lands"))
        selected = ''
        if player.jouCarteRouteVisible == True:
            selected = " checked='checked'"
        html += '''<li><input id='check-svg-road' name='check-svg-road'%s type='checkbox' onchange="javascript:checkShowHide(this, 'svg-road');"/><label for='check-svg-road'>%s</label></li>''' % (selected, _("Show/hide roads"))
        selected = ''
        if player.jouCarteLegendeVisible == True:
            selected = " checked='checked'"
        html += '''<li><input id='check-svg-legend' name='check-svg-legend'%s type='checkbox' onchange="javascript:checkShowHide(this, 'svg-legend');checkShowHide(this, 'loc-legend');"/><label for='check-svg-legend'>%s</label></li>''' % (selected, _("Show/hide legends"))
        html += '''<br/><input type='submit' name="_action_changeDefaultMap" value='%s' /></ul></form>''' % _("Change default")
        self.writeln(html)
        display = ''
        if not player.jouCarteLegendeVisible == True:
            display = " style='display:none'"
        self.writeln("<div id='loc-legend' class='loc-legend'%s>" % display)
        self.writeln(world.legend)
        self.writeln("</div></div><div class='spacer'>&nbsp;</div></div></div>")
    
    def actions(self):
        return Menu.actions(self) + ['changeDefaultMap']

    def changeDefaultMap(self):
        fields = self.request().fields()
        player = joueur.get(self._perso.prsJouID)
        if fields.has_key('check-svg-grid') and fields['check-svg-grid'] == 'on':
            player.jouCarteGrilleVisible = True
        else:
            player.jouCarteGrilleVisible = False
        if fields.has_key('check-svg-land') and  fields['check-svg-land'] == 'on':
            player.jouCarteRegionVisible = True
        else:
            player.jouCarteRegionVisible = False
        if fields.has_key('check-svg-road') and  fields['check-svg-road'] == 'on':
            player.jouCarteRouteVisible = True
        else:
            player.jouCarteRouteVisible = False
        if fields.has_key('check-svg-legend') and  fields['check-svg-legend'] == 'on':
            player.jouCarteLegendeVisible = True
        else:
            player.jouCarteLegendeVisible = False
        fields.clear()
        fields['extra'] = _('Preferences for the map updated.')
        self.application().forward(self.transaction(), self.request().urlPath())
