#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : loginPage.py - CREATION : 2004/09/01
#
# Copyright (C) 2004  Etienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Login page. Never point to it directly, it is load by default when there is no authentification.
Be careful : when installing, the random id generator formula should be change.

@version: 0.1
@author: U{Etienne Loks (ELO) <etienne.loks_AT_laposte.net>}
"""

from layout.sitePage import SitePage, _
from db.dbCharacters import joueur
from time import time, localtime
import string, types, random

class loginPage(SitePage):
    """
    Login page.
    2 fields : login, password.
    """
    
    def awake(self, transaction):
        """
Random id generation in order to put him on the session variable and on the form
in order to control the authentification source.
        """
        SitePage.awake(self, transaction)
        self.loginid = string.join(map(lambda x: '%02d' % x, localtime(time())[:6]), '') + str(random.randint(10000, 99999))
        self.session().setValue('loginid', self.loginid)
    
    def writeHeadParts(self):
        """
Disable the browser cache.
        """
        SitePage.writeHeadParts(self)
        self.writeln('''
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" /> ''')

    def writeContent(self):
        """
Writting the form.
        """
        fields = self.request().fields()
        # test if an old session has been identified
        if 'jou_id' in fields and 'session_id' in fields:
            try:
                jou = joueur.get(int(fields['jou_id']))
                if jou.jouSessionID == fields['session_id']:
                    self.destroySession()
                    return
            except:
                pass
        self.writeln('''<form action="#" method="post">
<div class='form'>
<table class='form' cellspacing='20'>
<tr><td>%(login)s :</td>
<td><input type="text" name="username" value="" /></td>
</tr>
<tr><td>%(password)s :</td>
<td><input type="password" name="password" value="" /></td>
</tr></table>
<input type="hidden" name="loginid" value="%(loginid)s" />
</div>
<div class='button'><input type="submit" name="_action_login" value="%(log_in)s" /></div>
</form>''' % {'login':_('Login'), 'log_in':_('Log in'), 'password':_('Password'), \
'loginid':self.loginid})
    
    def destroySession(self):
        """
        Form to destroy an old session.
        """
        fields = self.request().fields()
        wl = self.writeln
        hiddens = '''<input type="hidden" name="loginid" value="%s" />
<input type="hidden" name="session_id" value="%s" />
<input type="hidden" name="jou_id" value="%s" />
''' % (self.loginid, fields['session_id'], fields['jou_id'])
        if 'base_username' in fields and 'base_password' in fields:
            hiddens += '''<input type="hidden" name="username" value="%s" />
<input type="hidden" name="password" value="%s" />
''' % (fields['base_username'], fields['base_password'])
            
        self.writeln('''<div class='form'>

<table class='form' cellspacing='20'>
<tr><td colspan='2'>%s</td></tr>
<tr><td><form action="#" method="post"><input type="submit" name="_action_destroy" value="%s"/>%s</form></td>
<td><form action="#" method="post"><input type="submit" name="_action_back" value="%s"/></form></td></tr>
</table>
</div>
''' % (_("An old session is already active. Destroy it to log in ?"),
_("Yes"), hiddens, _("No")))


    def actions(self):
        """
Login and destroy are the available action.
        """
        return ['login', 'destroy']

    def login(self):
        pass
    
    def destroy(self):
        pass
