#!/usr/bin/python
# -*- coding: utf-8 -*-

# on se met dans l'environnement de morrigan
import sys
BASE_PATH = '/home/morrigan/'
sys.path.append(BASE_PATH)
sys.path.append(BASE_PATH + 'lib/')

from lib.db.dbCharacters import Player, Gamemaster
import plugins.configuration as config

# header
print "Content-Type: text/plain\n"
import cgi

def checkPassword(form):
    player = Player.checkLogin(form['login'].value, form['password'].value)
    if player:
        gm = list(Gamemaster.select('mji_jou_id=%d' % player.id))
        if gm:
            return True

def display_info():
    datas = (('databasetype', 'postgresql'), ('user', config.BDD_USER),
             ('password', config.BDD_PASSWORD), ('host', config.BDD_HOST),
             ('port', '5432'), ('db', config.BDD_DB),)
    for data in datas:
        print ":".join(data)

def main():
    form = cgi.FieldStorage()
    if 'login' in form and 'password' in form and checkPassword(form):
        display_info()
main()
