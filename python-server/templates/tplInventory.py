#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplInventory.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Inventory template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
from utils import _

main = """<div class='contenu'>
<div id='inventory_misc'>%(load)s%(money)s</div>
%(content)s
</div>"""

basic_line = '''
<tr><td>%(name)s</td><td>%(description)s</td><td>%(size)s</td>'''
object_line = '''
<tr%(style)s><td>%(equipped)s</td><td>%(name)s</td><td>%(description)s</td><td>%(size)s</td>'''
weapon_line = object_line + '''<td>%(range)s</td><td>%(damage)s</td><td>%(two_hands)s</td><td>%(loaded)s</td></tr>'''
armor_line = object_line + '''<td>%(zone)s</td><td>%(type)s</td><td>%(range)s</td></tr>'''
object_line += "</tr>"

object_table = '''<h1>%(title)s</h1><table class='items'>
<tr><th>&nbsp;</th><th>%(Name)s</th><th>%(Description)s</th><th>%(Size)s</th>'''
weapon_table = object_table + '''<th>%(Range)s</th><th>%(Damage)s</th><th>%(Two_hands)s</th><th>%(Loaded)s</th>'''
armor_table = object_table + '''<th>%(Zone)s</th><th>%(Type)s</th><th>%(Range)s</th>'''

object_table_chest_head = '''<h1>%(title)s</h1>'''
object_table_chest = '''<table class='items'>
<caption>%(title)s</caption>
<tr><th>%(Name)s</th><th>%(Description)s</th><th>%(Size)s</th>'''

end_table = '''</tr>%(lines)s</table><div class='spacer'>&nbsp;</div>'''
object_table += end_table
weapon_table += end_table
armor_table += end_table
object_table_chest += end_table