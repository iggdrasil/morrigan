#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplMenu.py
#
# Copyright (C) 2009  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Main menu template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_peacefrogs.net>
'''

head = '''<script type="text/javascript" src="%(js_path)stools.js"></script>
<script type="text/javascript">
<!--
initTab = %(initTab)s;
except = '';
function showSeveral(tab){
    for (i=0;i<tab.length;i++){if(document.getElementById(tab[i]) && tab[i]!=except){show(tab[i]);}}
}
function hideSeveral(tab){
    for (i=0;i<tab.length;i++){if(document.getElementById(tab[i]) && tab[i]!=except){hide(tab[i]);}}
}
-->
</script>'''

content = """<div class='contenu'>
%s
</div>"""

main = """<div class='cadre'><div class='page'>
<div class='head'>%(page_name)s</div>
%(state_menu)s
<div class='principal'>
%(character_menu)s
%(action_menu)s
<div class='contenu-right'>
%(extra)s
%(action_content)s
%(content)s
</div>
<div class='spacer'>&nbsp;</div>
</div>
%(bottom)s
</div></div>
"""

state_menu = """<ul class='menu-top'>
%(states)s
</ul>
"""

character_menu = """<div class='menu-left'><div class='menu-left-contenu'>
<div class='nom'>%(name)s</div>
%(image)s
%(action_points)s
%(health_bar)s
%(fatigue_bar)s
%(fight_bar)s
</div>
"""

action_menu = """<div class='menu-left-contenu' id='actions'><form action="#" method="get">
<h3>%(action_label)s</h3>
<ul>
%(items)s
<li class='button'><a href='?_action_logout=True'>%(logout_label)s</a></li></ul></form>
</div></div><script type="text/javascript">
<!--
hideSeveral(initTab);
-->
</script>
"""

menu_section = """<li><div class='menuSection'>
<div class='menuHeader' onclick="javascript:showHide('%(section_id)s');">%(section_label)s</div>
<ul class='menuContainer' id='%(section_id)s'>"""

menu_item = "<li class='button'><a href='%(base_path)s?curAction=%(action_id)s'>%(action_label)s</a></li>\n"

end_menu_section = "</ul></div></li>"

bottom = "<div class='tail'>Copyright : Blue Team Corporation</div>"

state_bar = """<dl class='statebar'%(id)s>
<dt>%(name)s</dt>
<dd>%(definition)s</dd>
</dl>
"""