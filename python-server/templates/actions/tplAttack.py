#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplAttack.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Attack template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<table class='form' cellspacing='10'>
<tr>
<td>%(Fight)s :</td>
<td><select name='opponent'>
%(opponents)s
</select></td>
</tr>
<tr>
<td>%(with)s :</td>
<td><select name='weapon'>
%(weapons)s
</select></td>
</tr>
</table>'''
