#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplSetRumors.py - CREATION : 2007/02/22
#
# Copyright (C) 2007  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Follow templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<div class='form'><table class='form'>
<tr><td>%(follow)s : </td><td><select name='shapes'>%(shapes)s</select></td></tr>
</table></div>'''
