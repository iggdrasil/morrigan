#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-
#
# Copyright (C) 2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Put in template
'''

getForm = '''<table class='form' cellspacing='10'>
<tr><td>%(put_label)s</td><td><select name='object'>
%(objects)s
</select></td></tr>
<tr><td>%(into_label)s</td><td><select name='chest'>
%(chests)s
</select></td></tr>
</table>'''
