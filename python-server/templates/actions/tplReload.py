#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplReload.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Reload templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getFormObject = '''<table class='form' cellspacing='10'>
<tr><td>%(object_label)s :</td><td><select name='object'>
%(objects)s
</select></td></tr>
</table>'''

getFormLoaders = '''<input type='hidden' name='object' value='%(object_id)d'/>
<table class='form' cellspacing='10'>
<tr><td>%(object)s :</td><td><select name='loader'>
%(loaders)s
</select></td></tr></table>'''
