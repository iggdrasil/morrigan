#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplSearchSteps.py - CREATION : 2007/02/22
#
# Copyright (C) 2007  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
SearchSteps templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''
getForm = ""

getFormName = '''<input type='hidden' name='rename' /><table class='items'>
<tr><th>%(name)s</th><th>%(type)s</th><th>%(size)s</th><th>%(deep)s</th><th>%(to_from)s</th><th>%(toward)s</th></tr>
%(steps)s</table>%(comment)s : <input name='comment' type='text'/>
<p>%(help)s</p>'''

getStepLine = '''<tr><td><input type='text' name='name_%(id)d' value='%(name)s' /></td><td>%(type)s</td><td>%(size)s</td><td>%(deep)s</td><td>%(to_from)s</td><td>%(toward)s<input type='hidden' name='toward_%(id)d' value="%(to_from)s %(toward)s"/></td></tr>'''

getKnowedStepLine = '''<tr><td>%(name)s</td><td>%(type)s</td><td>%(size)s</td><td>%(deep)s</td><td>%(to_from)s</td><td>%(toward)s<input type='hidden' name='toward_%(id)d' value="%(to_from)s %(toward)s"/></td></tr>'''
