#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplTalk.py - CREATION : 2008/03/06
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Talk templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = """<div class='actionForm'>
<input type='radio' name='type' value='persos' %(indiv_checked)s id='talk2' />
<label for='talk2'>%(talking_to_label)s :</label><br />
%(characters)s
<br /><br />
<input type='radio' id='allTalk' name='type' value='all' %(all_checked)s />
<label for='allTalk'>%(talking_to_all_label)s</label>
%(language)s
<br /><br />
<textarea name='msg' rows='15' cols='57'>%(message)s</textarea>
<br /><br />
<input type='checkbox'%(verify_check)s name="verify" value="true" id='checkSpell' />
<label for='checkSpell'>%(checkSpell_label)s</label>"""

characterItem = "<input type='checkbox' name='pers' id='%(id)s' value='%(id)s' \
%(check)s /><label for='%(id)s'>%(label)s</label>" 

languageForm = """<br/><br/><label for='language'>%(lang_label)s</label> :
%(languageItems)s"""
       
