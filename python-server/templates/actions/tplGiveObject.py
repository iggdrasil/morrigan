#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplGiveObject.py - CREATION : 2008/03/14
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Give object templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<table class='form' cellspacing='10'>
<tr><td>%(object_label)s :</td><td><select name='object'>
%(objects)s
</select></td></tr>
<tr><td>%(to_label)s :</td><td><select name='character'>
%(characters)s
</select></td></tr></table>'''
