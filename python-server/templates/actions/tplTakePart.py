#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplTakePart.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
TakePart template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<table class='form' cellspacing='10'>
<tr><td>%(Fight_type)s :</td><td><select name='distant_fight'>
<option value='0'>%(Melee)s</option>
<option value='1'>%(Distant)s</option>
</select></td></tr>
<tr><td>%(Character)s :</td><td><select name='character'>
%(characters)s
</select></td></tr>
</table>'''
