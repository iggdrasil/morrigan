#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplCreateRoad.py - CREATION : 2007/02/22
#
# Copyright (C) 2007  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Create road templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getChangeSectorForm = '''<div class='form'><table class='form'>
<tr><th>%(Change_destination_sector)s</th></tr>
<tr><td>%(Sector)s : </td><td><select name='new_sector_id'>%(sectors)s</select></td></tr>
</table></div>'''

locChooser = '''<select name='loc_id'>%s</select>'''

getForm = '''<div class='form'>
<input type='hidden' name='sector_id' value='%(sector_id)s'/>
<table class='form'>
<tr><th colspan='3'>%(Road_from)s</th><th>%(current_loc)s</th></tr>
<tr><td>%(To)s : </td><td><input type='radio' name='topo' value='1'%(topo_checked)s></td><td>%(A_location_in)s : </td><td>%(loc_chooser)s</td></tr>
<tr><td>&nbsp;</td><td><input type='radio' name='topo' value='0'%(carto_checked)s></td><td><select name='world_id'>%(world)s</select> : </td>
<td>X : <input type='text' name='x' size='3' value='%(x)s'/> - Y : <input type='text' name='y' size='3' value='%(y)s'/></td></tr>
<tr><td colspan='3'>%(Road_types)s : </td><td><select name='road_type'>%(roads_types)s</select></td></tr>
<tr><td colspan='3'>%(Particularity)s : </td><td><select name='particularity'>%(particularity)s</select> <input type='text' name='detail'/></td></tr>
<tr><td colspan='3'>%(One_way_desc)s : </td><td><textarea name='desc_1' rows='5' cols='30'>%(desc_1)s</textarea></td></tr>
<tr><td colspan='3'>%(Back_desc)s : </td><td><textarea name='desc_2' rows='5' cols='30'>%(desc_2)s</textarea></td></tr>
<tr><td colspan='3'>%(One_way_open)s : </td><td><select name='chm_aller'><option value='1'%(chm_aller)s>%(Yes)s</option><option value='0'%(chm_aller_no)s>%(No)s</option></select></td></tr>
<tr><td colspan='3'>%(Back_open)s : </td><td><select name='chm_retour'><option value='1'%(chm_retour)s>%(Yes)s</option><option value='0'%(chm_retour_no)s>%(No)s</option></select></td></tr>
<tr><td colspan='3'>%(Difficulty)s : </td><td><input type='text' name='diff' value='%(difficulty)d'/></td></tr>
<tr><td colspan='3'>%(Discretion)s : </td><td><input type='text' name='discretion' value='%(discretion)d'/></td></tr>
<tr><td colspan='3'>%(AP_cost)s : </td><td><input type='text' name='ap_cost' value='%(ap_cost)s'/></td></tr>
<tr><td colspan='3'>%(FP_cost)s : </td><td><input type='text' name='fp_cost' value='%(fp_cost)s'/></td></tr>
<tr><td colspan='3'>%(Failed_LP_cost)s : </td><td><input type='text' name='pv_cost' value='%(pv_cost)s'/></td></tr>
<tr><td colspan='3'>%(Size)s : </td><td><input type='text' name='size' value='%(size)d'/></td></tr>
<tr><td colspan='3'>%(Factor_steps_erasement)s : </td><td><input type='text' name='steps_erasement' value='%(current_steps_erasement)d'/></td></tr>
<tr><td colspan='3'>%(Factor_steps_number)s : </td><td><input type='text' name='steps_number' value='%(current_steps_number)d'/></td></tr>
<tr><td colspan='3'>%(Blocking)s : </td><td><select name='blocking'><option value='1'%(blocking)s>%(Yes)s</option><option value='0'%(blocking_no)s>%(No)s</option></select></td></tr>
<tr><td colspan='3'>%(Associated_characteristic)s : </td><td><select name='crc_id'>%(characteristics)s</select></td></tr>
<tr><td colspan='3'>&nbsp;</td><td><select name='crc_id_2'>%(characteristics_2)s</select></td></tr>
</table>
<input type='hidden' name='road_id' value='%(road_id)d'>
</div>'''

suppForm = '''<div class='form'>
<input type='hidden' name='road_id' value='%(road_id)d'/>
<input type='hidden' name='delete_road' value='1'/>
</div>
'''



