#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplCreateLocation.py - CREATION : 2008/05/12
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Create and modify location templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<div class='form'><table class='form'>
<tr><td>%(Location_type)s : </td><td><select name='location_type'>%(location_types)s</select></td></tr>
<tr><td>%(Name)s : </td><td><input name='name' type='text' value="%(current_name)s"/></td></tr>
<tr><td>%(Description)s : </td><td><textarea name='desc' rows='10' cols='30'>%(current_desc)s</textarea></td></tr>
<tr><td>%(Sector)s : </td><td><select name='sector'>%(sector)s</select></td></tr>
<tr><td>%(Size)s : </td><td><input name='size' type='text' value='%(current_size)s'/></td></tr>
<tr><td>%(Population)s : </td><td><input name='population' type='text' value='%(current_population)s'/></td></tr>
<tr><td>%(Factor_steps_erasement)s : </td><td><input name='steps_erasement' type='text' value='%(current_steps_erasement)d'/></td></tr>
<tr><td>%(Factor_steps_number)s : </td><td><input name='steps_number' type='text' value='%(current_steps_number)d'/></td></tr>
</table>
<input type='hidden' name='location_id' value='%(location_id)d'/>
</div>'''

suppForm = '''<div class='form'><table class='form'>
<tr><td>%(Teleport_character)s : </td><td><select name='teleport_location'>%(teleport_location)s</select></td></tr>
</table>
<input type='hidden' name='location_id' value='%(location_id)d'/>
<input type='hidden' name='delete_location' value='1'/>
</div>
'''
