#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplLeaveFight.py
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
LeaveFight template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<table class='form' cellspacing='10'>
<tr><td><input type='radio' name='melee' value='1' checked='checked'/></td>
<td>%(Melee_fight)s :</td><td><select name='melee_fight'>
%(melee_fights)s
</select></td></tr>
<tr><td></td><td>%(Method)s :</td>
<td><select name='force'>
<option value='1'/>%(Force)s</option>
<option value='0'/>%(Dexterity)s</option>
</select></td>
</tr>
<tr><td><input type='radio' name='melee' value='0'/></td>
<td>%(Distant_fight)s :</td><td><select name='distant_fight'>
%(distant_fights)s
</select></td></tr>
</table>'''
