#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplSetRumors.py - CREATION : 2006/11/24
#
# Copyright (C) 2006  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Rumors templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getFormLanguage = '''<div class='form'><table class='form'>
<tr><td>%(title)s : </td><td><input type='text' name='c_title' value='%(c_title)s'/></td></tr>
<tr><td>%(language)s : </td><td><select name='language'>%(avail_language)s</select></td></tr>
</table></div>
<p>%(text)s : </p>
<textarea name='c_text' rows='15' cols='57'>%(c_text)s</textarea>'''

getFormSimple = '''<div class='form'><table class='form'>
<tr><td>%(title)s : </td><td><input type='text' name='c_title' value='%(c_title)s'/></td></tr>
<tr><td>%(language)s</td><td><select name='language'>%(avail_language)s</select></td></tr>
</table></div>
<p>%(text)s : </p>
<textarea name='c_text' rows='15' cols='57'>%(c_text)s</textarea>'''
