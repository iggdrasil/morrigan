#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplChangeCharacterDate.py - CREATION : 2008/03/08
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Change character data templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''%(changeDescription)s :<br /><br />
<textarea name='desc' rows='15' cols='57'>%(desc)s</textarea><br />
<div class='form'>
<table class='form' cellspacing='10'>
<tr><td>%(imagePath)s :</td>
<td><input type="text" name="img" value="%(img)s" /></td></tr>
<tr><td>%(css)s :</td>
<td><select name='css'>%(css_choice)s</select></td></tr>
<tr><td>%(tz)s :</td>
<td><input type="text" name="tz" value="%(ctz)d" size='3'/> %(hours)s</td></tr>
<tr><td>%(email)s :</td>
<td><input type="text" name="mail" value="%(mail)s" /></td></tr>
<tr><td>%(oldPass)s :</td>
<td><input type="password" name="oldPass" value="" /></td></tr>
<tr><td>%(newPass)s :</td>
<td><input type="password" name="newPass" value="" /></td></tr>
<tr><td>%(confirm)s :</td>
<td><input type="password" name="confirmPass" value="" /></td></tr>
</table>
</div>'''
