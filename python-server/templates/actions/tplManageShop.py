#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Open/close shop template
'''

nothing = '<p>%s</p>'

getShop = '''<table class='form' cellspacing='10'>
<tr><td>%(label)s</td><td><select name='shop'>
%(shops)s
</select></td></tr>
</table>'''

getForm = """<h3>%(shop_name)s</h3>
<p>%(shop_desc)s</p>
<table class='items'><caption>%(chest_item_label)s</caption>
<tr><th>%(name_label)s</th><th>%(price_label)s</th></tr>
%(chest_items)s
</table>
<table class='items'><caption>%(buy_item_label)s</caption>
<tr><th>%(name_label)s</th><th>%(price_label)s</th><th>%(number_label)s</th></tr>
%(buy_items)s
%(new_buy_items)s
</table>
<input type='hidden' name='shop' value='%(shop_id)d'/>
"""

itemLine = "<tr><td>%(name)s</td><td><input type='text' value='%(price)s'\
 name='sell_price_%(id)s'/> <select name='currency_%(id)d'>%(currency)s\
</select></td></tr>\n"

buyItemLine = "<tr><td>%(name)s</td>\
<td><input type='text' value='%(price)s' name='buy_price_%(id)s'/>\
 <select name='buy_currency_%(id)d'>%(currency)s</select></td>\
<td><input type='text' value='%(number)s' name='buy_number_%(id)s'/></td>\
</tr>\n"

newBuyItemLine = "<tr><td><select name='new_buy_item'>\n%(items)s</select></td>\
<td><input type='text' name='new_price'/> <select name='new_buy_currency'>\
%(currency)s</select></td>\
<td><input type='text' name='new_number'/></td></tr>\n"
