#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplMove.py - CREATION : 2008/03/14
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Move templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<h3>%(availableRoadsLabel)s :</h3>
%(roadForm)s
%(itemRoadForm)s
%(compassRose)s'''

formDestLine = "<tr><td></td><td><input type='radio' name='destination' \
id='dest%(id)d' value='dest%(id)d' /></td>\
<td><label for='dest%(id)d'>%(label)s</label></td><td></td></tr>"

formDestCase = "<td><input type='radio' name='destination' id='dest%(id)s' \
value='dest%(id)s'/></td><td><label for='dest%(id)s'>%(label)s</label></td>"
formUnavailableDestCase = "<td colspan='2'>%(label)s</td>"

roadForm = '''<table>%s</table>'''
itemRoadForm = '''<hr/><h3>%(title)s :</h3>
<table>%(roads)s
<tr>
<td></td><td>%(key)s :</td><td><select name='object'>%(objects)s</select></td>
<td></td>
</tr>
</table>'''

compassRose = '''<table class='compassRose'>
<tr><th class='tableCheck'></th><th></th><th class='tableCheck'></th><th></th>
<th class='tableCheck'></th><th></th></tr>
<tr>%(northwest)s%(north)s%(northeast)s</tr>
<tr>%(west)s<td colspan='2'>+<br />%(center)s</td>%(east)s</tr>
<tr>%(southwest)s%(south)s%(southeast)s</tr></table>'''
