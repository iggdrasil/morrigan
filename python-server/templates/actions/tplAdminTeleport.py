#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplAdminTeleport.py - CREATION : 2008/06/02
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Teleport template (admin)

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getChangeSectorForm = '''<div class='form'><table class='form'>
<tr><th>%(Change_destination_sector)s</th></tr>
<tr><td>%(Sector)s : </td><td><select name='new_sector_id'>%(sectors)s</select></td></tr>
</table></div>'''

locChooser = '''<select name='loc_id'>%s</select>'''

getForm = '''<div class='form'>
<input type='hidden' name='sector_id' value='%(sector_id)s'/>
<table class='form'>
<tr><td>%(Teleport_to)s : </td><td><input type='radio' name='topo' value='1'%(topo_checked)s></td><td>%(A_location_in)s : </td><td>%(loc_chooser)s</td></tr>
<tr><td>&nbsp;</td><td><input type='radio' name='topo' value='0'%(carto_checked)s></td><td><select name='world_id'>%(world)s</select> : </td>
<td>X : <input type='text' name='x' size='3' value='%(x)s'/> - Y : <input type='text' name='y' size='3' value='%(y)s'/></td></tr>
</table>
</div>'''
