#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplAddLocationDescription.py - CREATION : 2008/05/14
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Create location templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<div class='form'>
<input type='hidden' name='location_id' value='%(location_id)d'/>
<table class='form'>
<tr><th>%(location_name)s</th></tr>
<tr><td>%(Description)s : </td><td><textarea name='desc' rows='10' cols='30'>%(current_desc)s</textarea></td></tr>
<tr><td>%(Difficulty)s : </td><td><input name='diff' type='text' value='%(diff)d'/></td></tr>
</table>
<input type='hidden' name='dl_id' value='%(dl_id)d'/>
</div>'''

suppForm = '''<div class='form'>
<input type='hidden' name='dl_id' value='%(dl_id)d'/>
<input type='hidden' name='delete_desc' value='1'/>
</div>
'''
