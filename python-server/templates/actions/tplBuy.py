#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.

'''
Buy template
'''

nothing = '<p>%(shop_desc)s</p><p>%(msg)s</p>'

getShop = '''<table class='form' cellspacing='10'>
<tr><td>%(label)s</td><td><select name='shop'>
%(shops)s
</select></td></tr>
</table>'''

getForm = """<h3>%(shop_name)s</h3>
<p>%(shop_desc)s</p>
<table class='items'>
<tr><td></td><th>%(item_lbl)s</th><th>%(desc_lbl)s</th><th>%(price_lbl)s</th>\
</tr>
%(items)s
</table>
<input type='hidden' name='shop' value='%(shop_id)d'/>
"""

item = "<tr><td><input type='radio' name='item' value='%(id)d'></td>\
<td>%(name)s</td><td>%(desc)s</td><td>%(price)s</td></tr>\n"
