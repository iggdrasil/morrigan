#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplShowWay.py - CREATION : 2007/06/03
#
# Copyright (C) 2007  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Show the way templates

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<div class='form'><table class='form'><tr><td colspan='2'>%(message)s</td></tr>
%(characters)s</table></div>'''

characterLine = '''<tr><td><input type='checkbox' id='character_%(id)d' name='character_%(id)d'%(checked)s /></td><td><label for='character_%(id)d'>%(character)s</label></td></tr>
'''
