#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplCreateSector.py - CREATION : 2008/05/12
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Create sector template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<div class='form'><table class='form'>
<tr><td>%(Name)s : </td><td><input name='name' type='text' value="%(current_name)s"/></td></tr>
<tr><td>%(Description)s : </td><td><textarea name='desc' rows='10' cols='30'>%(current_desc)s</textarea></td></tr>
<tr><td>%(Population)s : </td><td><input name='population' type='text' value='%(current_population)d'/></td></tr>
<tr><td>%(LP_daily_cost)s : </td><td><input name='lp_cost' type='text' value='%(current_lp_cost)d'/></td></tr>
<tr><td>%(FP_daily_cost)s : </td><td><input name='fp_cost' type='text' value='%(current_fp_cost)d'/></td></tr>
</table>
<input type='hidden' name='sector_id' value='%(sector_id)d'/>
</div>'''

suppForm = '''<div class='form'><table class='form'>
<tr><th>%(sector_name)s</th></tr>
</table>
<input type='hidden' name='sector_id' value='%(sector_id)d'/>
<input type='hidden' name='delete_sector' value='1'/>
</div>
'''
