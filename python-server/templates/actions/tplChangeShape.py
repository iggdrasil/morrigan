#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplChangeShape.py - CREATION : 2008/03/08
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Change shape template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = "<select name='forme'>%(available_shapes)s</select><br/><br/>"
