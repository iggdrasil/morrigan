#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : tplEmotes.py - CREATION : 2008/03/08
#
# Copyright (C) 2008  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Change character template

 - version: 0.1
 - author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

getForm = '''<input type='radio' name='type' value='persos' id='show2'/>
<label for='show2'>%(showing_to_label)s</label> :
<br />
%(characters_check)s
<br/><input type='radio' id='allShow' name='type' value='all'/>
<label for='allShow'>%(all_show_label)s</label>
<h2>%(simple_label)s</h2>
<input type='radio' name='emote_type' value='simple'/>
<select name='simple_emote'>%(simple_emotes)s</select>
<h2>%(character_label)s</h2>
<input type='radio' name='emote_type' value='character'/>
<select name='character_emote'>%(character_emotes)s</select>
%(target_label)s : <select name='character'>%(characters)s</select>
'''

characterCheck = "<input type='checkbox' name='pers' id='%(id)s' \
value='%(id)s' %(check)s /><label for='%(id)s'>%(label)s</label>"
