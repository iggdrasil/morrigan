#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : __init__.py - CREATION : 2005/1+/10
#
# Copyright (C) 2005  �tienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Differents initialisation tasks.

@version: 0.1
@author: U{�tienne Loks (ELO) <etienne.loks_AT_laposte.net>}
"""

from  config import *

def InstallInWebKit(appserver):
    pass

