#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : config.py - CREATION : 2005/03/28
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

'''
Several config attributes

version: 0.1
author: Étienne Loks (ELO) <etienne.loks_AT_laposte.net>
'''

from Ft.Xml.Domlette import NonvalidatingReader
from Ft.Xml.XPath import Evaluate

import os

def getDocument(file_name):
    fd = open(file_name, 'r')
    file_string = fd.read()
    fd.close()
    document = NonvalidatingReader.parseString(file_string)
    return document

config_file = 'xml/config.xml'
if not os.path.isfile(config_file):
    raise 'ConfigurationError', "The file xml/config.xml is not available,"
doc = getDocument(config_file)

BDD_USER = Evaluate('//configuration/database/user/text()', doc.documentElement)[0].nodeValue
BDD_DB = Evaluate('//configuration/database/name/text()', doc.documentElement)[0].nodeValue
BDD_HOST = Evaluate('//configuration/database/host/text()', doc.documentElement)[0].nodeValue
BDD_PASSWORD = Evaluate('//configuration/database/password/text()', doc.documentElement)[0].nodeValue
FULL_RP = False
if Evaluate('//configuration/game/full_rp/text()', doc.documentElement)[0].nodeValue == 'True':
    FULL_RP = True
ACTION_FILE_PATH = Evaluate('//configuration/paths/action_files/text()', doc.documentElement)[0].nodeValue
XSL_PATH = Evaluate('//configuration/paths/xsl/text()', doc.documentElement)[0].nodeValue
IMAGE_PATH = str(Evaluate('//configuration/paths/image/text()', doc.documentElement)[0].nodeValue)
JS_PATH = Evaluate('//configuration/paths/js/text()', doc.documentElement)[0].nodeValue
CSS_PATH = Evaluate('//configuration/paths/css/text()', doc.documentElement)[0].nodeValue
DEBUG = False
if Evaluate('//configuration/debug/text()', doc.documentElement)[0].nodeValue == 'True':
    DEBUG = True
LANGUAGES = False
if Evaluate('//configuration/game/languages/text()', doc.documentElement)[0].nodeValue == 'True':
    LANGUAGES = True

TRAINING_LOST = int(Evaluate('//configuration/game/training_lost/text()', doc.documentElement)[0].nodeValue)
TRAINING_GAIN = int(Evaluate('//configuration/game/training_gain/text()', doc.documentElement)[0].nodeValue)
TRAINING_MAX = int(Evaluate('//configuration/game/training_max/text()', doc.documentElement)[0].nodeValue)
TRAINING_DELTA_DIFF = int(Evaluate('//configuration/game/training_delta_diff/text()', doc.documentElement)[0].nodeValue)
CRITICISM_CEIL = int(Evaluate('//configuration/game/criticism_ceil/text()', doc.documentElement)[0].nodeValue)
STEPS_NUMBER = int(Evaluate('//configuration/game/steps_number/text()', doc.documentElement)[0].nodeValue)

