#!/usr/bin/env python
# -*- coding: UTF-8 -*- mode:python ; tab-width:4 -*- ex:set tabstop=4 shiftwidth=4 expandtab: -*-

#######################################################################
# PROJECT : Morrigan - FILE : scheduler.py - CREATION : 2005/29/01
#
# Copyright (C) 2004  Étienne Loks  <etienne.loks_AT_laposte.net>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file COPYING.
#######################################################################

"""
Define a scheduler. Add differents deliveries (action points, food points, etc)

@version: 0.1
@author: U{Étienne Loks (ELO) <etienne.loks_AT_laposte.net>}
"""


from TaskKit.Scheduler import Scheduler
from lib.tasks.deliveries import Deliveries
from time import *

def scheduler():
    scheduler = Scheduler()
    scheduler.start()
    scheduler.addPeriodicAction(time() + 3600, 3600, Deliveries(), 'deliveries')
