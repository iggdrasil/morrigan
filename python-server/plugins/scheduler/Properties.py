name = 'Morrigan'

version = (0, 0, 1)

docs = []

status = 'alpha'

requiredPyVersion = (2, 0, 0)

synopsis = """Plugin for the morrigan project. Its aim is to launch scheduled tasks.""" 
