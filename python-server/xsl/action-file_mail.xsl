<?xml version="1.0" encoding="utf-8"?>

<xsl:transform 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:date="http://exslt.org/dates-and-times"
version="1.0">

<xsl:output method="text" indent="no" encoding="utf-8"/>
<xsl:param name="time_delta" />

<xsl:template match="/">
<xsl:for-each select="action_file">
  <xsl:for-each select="action">&#160;

<xsl:variable name='modified_date' select="date:add(date, $time_delta)"/>
<xsl:value-of select="date:day-name($modified_date)"/>&#160;<xsl:value-of select="date:day-in-month($modified_date)"/>&#160;<xsl:value-of select="date:month-name($modified_date)"/>&#160;<xsl:value-of select="date:year($modified_date)"/> - <xsl:value-of select="date:hour-in-day($modified_date)"/>:<xsl:value-of select="date:minute-in-hour($modified_date)"/>:<xsl:value-of select="date:second-in-minute($modified_date)"/>&#160;
<xsl:value-of select="name"/>&#160;
Action<xsl:choose>
<xsl:when test="subject"> de <xsl:for-each select="subject">
<xsl:value-of select="name"/><xsl:if test="position()!=last()">, </xsl:if></xsl:for-each> </xsl:when><xsl:otherwise> anonyme</xsl:otherwise></xsl:choose><xsl:if test="target"> destinée à <xsl:for-each select="target">
   <xsl:value-of select="name"/>
   <xsl:choose>
     <xsl:when test="position()!=last()">, </xsl:when>
     <xsl:otherwise>.</xsl:otherwise>
   </xsl:choose>
</xsl:for-each></xsl:if>&#160;
<xsl:for-each select="details/p"><p><xsl:value-of select="."/></p></xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:template>

</xsl:transform>
