
-- Trigger
-- Un seul personnage d un joueur peut avoir le flag prs_principal à 1
CREATE OR REPLACE FUNCTION trg_prs_principal() RETURNS trigger AS '
DECLARE
	v_prs_id	    personnage.prs_id%TYPE;
	v_prs_nom	    personnage.prs_nom_originel%TYPE;
	v_jou_pseudo	joueur.jou_pseudo%TYPE;
BEGIN

	-- Recherche du personnage principal (autre que celui-ci)
	SELECT prs_id, prs_nom_originel, jou_pseudo
		INTO v_prs_id, v_prs_nom, v_jou_pseudo
	FROM personnage, joueur
	WHERE	prs_principal   = true
		AND prs_id    != NEW.prs_id
		AND prs_jou_id = NEW.prs_jou_id
		AND jou_id     = NEW.prs_jou_id;

	-- S il en existe un et qu on essaie de mettre un deuxième principal, l ancien est rétrogradé :)
	IF (FOUND AND NEW.prs_principal = true) THEN
		RAISE NOTICE ''Le personnage % (id=%) est maintenant principal pour le joueur % (id=%).'', NEW.prs_nom_originel, NEW.prs_id, v_jou_pseudo, NEW.prs_jou_id;

		-- Mise a jour du personnage principal
		UPDATE	personnage
		SET	prs_principal = false
		WHERE	prs_id  = v_prs_id;


	-- Sinon, si pas d autre perso principal et celui-ci non plus, on le force
	ELSIF (NOT FOUND AND NEW.prs_principal = false) THEN

		SELECT jou_pseudo
			INTO v_jou_pseudo
		FROM joueur
		WHERE	jou_id = NEW.prs_jou_id;
		
		RAISE NOTICE ''Le personnage % (id=%) est force principal pour le joueur % (id=%).'', NEW.prs_nom_originel, NEW.prs_id, v_jou_pseudo, NEW.prs_jou_id;
		
		-- La forme insérée/updatée est forcée par defaut
		UPDATE	personnage
		SET	prs_principal = true
		WHERE	prs_id  = NEW.prs_id;
	END IF;
	RETURN NEW;
END;
' LANGUAGE plpgsql;




-- Une suppression d un personnage contrôle que ce n est pas le personnage principal.
CREATE OR REPLACE FUNCTION trg_prs_principal_del() RETURNS trigger AS '
BEGIN
	IF (OLD.prs_principal = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer le personnage principal !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;





-- Trigger
-- A un instant donné, un perso a UNE et UNE SEULE forme. Prendre une forme revient à quitter une ancienne.
CREATE OR REPLACE FUNCTION trg_frm_actuelle() RETURNS trigger AS '
DECLARE
	v_frm_id	forme.frm_id%TYPE;
	v_prs_id	personnage.prs_id%TYPE;
	v_prs_nom	personnage.prs_nom_originel%TYPE;

BEGIN
	-- Recherche de la forme actuelle
	SELECT frm_prs_id, prs_nom_originel, frm_id
		INTO v_prs_id, v_prs_nom, v_frm_id
	FROM personnage, forme
	WHERE	frm_actuelle   = true
		AND frm_id    != NEW.frm_id
		AND frm_prs_id = NEW.frm_prs_id
		AND prs_id     = NEW.frm_prs_id;

	IF (FOUND AND NEW.frm_actuelle = true) THEN
		-- Mise a jour de la forme actuelle
		UPDATE	forme
		SET	frm_actuelle = false
		WHERE	frm_id  = v_frm_id;

		RAISE NOTICE ''La forme id=% est maintenant la forme actuelle du personnage % (id=%).'', NEW.frm_id, v_prs_nom, v_prs_id;
	ELSIF (NOT FOUND AND NEW.frm_actuelle = false) THEN

		SELECT prs_id, prs_nom_originel
			INTO v_prs_id, v_prs_nom
		FROM personnage
		WHERE	prs_id = NEW.frm_prs_id;
		
		RAISE NOTICE ''La forme id=% est forcee comme forme actuelle du personnage % (id=%).'', NEW.frm_id, v_prs_nom, v_prs_id;
		
		-- La forme insérée/updatée est forcée par defaut
		UPDATE	forme
		SET	frm_actuelle = true
		WHERE	frm_id  = NEW.frm_id;
	END IF;
	RETURN NEW;
END;
' LANGUAGE plpgsql;

-- Une suppression d une forme contrôle que ce n est pas la forme actuelle
CREATE OR REPLACE FUNCTION trg_frm_actuelle_del() RETURNS trigger AS '
BEGIN
	IF (OLD.frm_actuelle = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer la forme actuelle !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;







-- Une suppression d un lieu saisonnier contrôle que ce n est pas le lieu saisonnier par défaut
CREATE OR REPLACE FUNCTION trg_lis_defaut_del() RETURNS trigger AS '
BEGIN
	IF (OLD.frm_actuelle = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer le lieu saisonnier par défaut  !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;





-- Un personnage ne peut avoir qu un niveau par classe. Leur nombre est egalement limité ?
CREATE OR REPLACE FUNCTION trg_pcl_classe() RETURNS trigger AS '
DECLARE
	v_cls_id	classe.cls_id%TYPE;
	v_cls_nom	classe.cls_nom%TYPE;
	v_cln_niveau	classe_niveau.cln_niveau%TYPE;
	v_prs_nom	personnage.prs_nom_originel%TYPE;
BEGIN
	SELECT cl1.cln_niveau, prs_nom_originel, cls_id, cls_nom
		INTO v_cln_niveau, v_prs_nom, v_cls_id, v_cls_nom
	FROM perso_classe, classe_niveau cl1, classe_niveau cl2, personnage, classe
	WHERE		pcl_cln_id     = cl1.cln_id
		AND	pcl_prs_id     = NEW.pcl_prs_id
		AND	pcl_id        != NEW.pcl_id
		AND	cl2.cln_id     = NEW.pcl_cln_id
		AND	cl2.cln_cls_id = cl1.cln_cls_id
		AND	prs_id         = pcl_prs_id
		AND	cls_id         = cl1.cln_cls_id;

	IF FOUND THEN
		RAISE EXCEPTION ''Le personnage % (id=%) a deja le niveau % de la classe % (id=%)'', v_prs_nom, NEW.pcl_prs_id, v_cln_niveau, v_cls_nom, v_cls_id;
	END IF;

	RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Mettre a jour les scores
CREATE OR REPLACE FUNCTION trg_pcr_update_score() RETURNS trigger AS '
BEGIN
		-- si les XP ont bougé on recalcule les scores
		IF NEW.pcr_score_xp != OLD.pcr_score_xp THEN
			NEW.pcr_score_calcule := calcul_score(NEW.pcr_score_xp);
		END IF;
		RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Apres une insertion d'une carac (table "caracteristique"), il faut modifier la table perso_carac (pour 1 personnage, il a un score dans CHAQUE caracteristique)
CREATE OR REPLACE FUNCTION trg_pcr_insert_carac() RETURNS trigger AS $$
DECLARE
	c_perso		CURSOR FOR SELECT prs_id FROM personnage;
	v_prs_id	personnage.prs_id%TYPE;
BEGIN

	-- Si une carac est rajoutee, on rajoute une ligne dans perso_carac pour chaque personnage.
	RAISE NOTICE 'trg_pcr_insert_carac : Ajout de caracteristique "% (%)" => Ajout de lignes dans perso_carac', NEW.crc_nom, NEW.crc_id;
	OPEN c_perso;

	WHILE 1 LOOP
		FETCH c_perso INTO v_prs_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		
		INSERT INTO perso_carac (pcr_prs_id, pcr_crc_id)
			VALUES (v_prs_id, NEW.crc_id);
			
	END LOOP;

	CLOSE c_perso;

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;





-- Apres une insertion d'un personnage (table "personnage"), il faut modifier la table perso_carac (chaque personnage a un score dans CHAQUE caracteristique)
CREATE OR REPLACE FUNCTION trg_pcr_insert_perso() RETURNS trigger AS '
DECLARE
	c_carac		CURSOR FOR SELECT crc_id FROM caracteristique;
	v_crc_id	caracteristique.crc_id%TYPE;
BEGIN

	-- Si un personnage est rajoute, on rajoute une ligne dans perso_carac pour chaque caracteristique.
	RAISE NOTICE ''trg_pcr_insert_perso : Ajout de personnage "% (%)" => Ajout de lignes dans perso_carac'', NEW.prs_nom_originel, NEW.prs_id;
	OPEN c_carac;

	WHILE 1 LOOP
		FETCH c_carac INTO v_crc_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		
		INSERT INTO perso_carac (pcr_prs_id, pcr_crc_id)
			VALUES (NEW.prs_id, v_crc_id);
			
	END LOOP;

	CLOSE c_carac;

	RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Lors d'une insertion d'un joueur, il faut lui mettre un style css par defaut (le premier qui est trouve)
CREATE OR REPLACE FUNCTION trg_jou_css_defaut() RETURNS trigger AS '
DECLARE
	c_css		CURSOR FOR SELECT css_nom, css_id FROM css;
	v_css_id	css.css_id%TYPE;
	v_css_nom	css.css_nom%TYPE;
BEGIN

	-- Si aucun style CSS n est defini pour le joueur, on prend le premier qu on trouve
	IF NEW.jou_css_id IS NULL THEN
		OPEN c_css;

		FETCH c_css INTO v_css_nom, v_css_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		CLOSE c_css;

		RAISE NOTICE ''trg_jou_css_defaut : Ajout du joueur "% (%)" sans CSS => Attribution du style "% (%)"'', NEW.jou_pseudo, NEW.jou_id, v_css_nom, v_css_id;
		NEW.jou_css_id = v_css_id;
		
	END IF;

	RETURN NEW;
END;
' LANGUAGE plpgsql;

