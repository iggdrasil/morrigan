-- Natures de carac

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (1, 'Physique');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (2, 'Intellectuel');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (3, 'Combat');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (4, 'Magie');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (5, 'Déplacement');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (6, 'Technique');

-- Caractéristiques (attributs)

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (1, 'Force', 'Fort comme un troll, dit-on. Devinez pourquoi.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (2, 'Dextérité', 'Adroit comme un elfe. La dextérité est l''habileté â manier une arme, un instrument.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (3, 'Constitution', 'Solide comme un ent. La constitution est la résistance, l''endurance.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (4, 'Intelligence', 'Intelligent comme un dragon. L''intelligence est la capacité à savoir réfléchir.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (5, 'Sagesse', 'Sage comme un poisson. Vous avez déjà vu un poisson sortir tout seul de l''eau ? Non, et bien voilà.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (6, 'Charisme', 'Beau comme moi. Cela veut tout dire.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (7, 'Perception', 'Beau comme moi. Cela veut tout dire.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (8, 'Karma', 'Good ou bad...', 1,
        2, 0, False);

-- Caractéristiques (compétences)

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (9, 'Mains nues/arme improvisée', 'Vraiment ? Tu penses avoir un don pour foutre des pains ?', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (10, 'Arme légère', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (11, 'Arme classique', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (12, 'Arme à deux mains', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (13, 'Combat à deux armes', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (14, 'Combat monté', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (15, 'Arme de tir', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (16, 'Arme de jet', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (17, 'Technique offensive', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (18, 'Technique defensive', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (19, 'Nécromancie', '', 2,
        4, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (20, 'Shamanisme', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (21, 'Magie élémentaire', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (22, 'Commerce', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (23, 'Vol à la tire', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (24, 'Crochetage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (25, 'Détection des pièges', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (26, 'Discrétion', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (27, 'Pistage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (28, 'Alchimie', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (29, 'Artisanat', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (30, 'Soins', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (31, 'Dépeçage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (32, 'Connaissance de la faune', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (33, 'Connaissance de la flore', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (34, 'Connaissance des roches', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (35, 'Connaissance des métaux', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (36, 'Survie en cité', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (37, 'Survie extérieure : climat chaud', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (38, 'Survie extérieure : climat froid', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (39, 'Survie extérieure : climat tempéré', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (40, 'Pédagogie', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (41, 'Lecture/écriture', '', 2,
        2, 0, True);
INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (42, 'Astronomie/cartographie', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (43, 'Escalade', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (44, 'Navigation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (45, 'Équitation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (46, 'Natation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (47, 'Marche/course', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (48, 'Millianais', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (49, 'Septimien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (50, 'Septimien ancien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (51, 'Légion ancienne', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (52, 'Karhnais', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (53, 'Avresolien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (54, 'Elfique', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (55, 'Nain', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (56, 'Orc', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (57, 'Draconique', '', 3,
        2, 0, False);
