
-- Generated SQL Constraints Drop statements
-- --------------------------------------------------------------------

drop index idxu_prs_nom_originel;
drop index idxu_prs_nom_actuel;
drop index idxu_jou_pseudo;
drop index idxu_frm_prs_rac;
drop index idxu_cln_titre;
drop index idx_cln_clsniv;
drop index idxu_cls_nom;
drop index idxu_pml_prsmld;
drop index idxu_mld_nom;
drop index idxu_crc_nom;
drop index idxu_pcr_prscrc;
drop index idxu_ctg_libelle;
drop index idxu_pci_prscls;
drop index idxu_pqc_clncls;
drop index idxu_mnd_nom;
drop index idxu_lie_nom;
drop index idxu_sct_nom;
drop index idxu_ssn_nom;
drop index idxu_mnn_nom;
drop index idxu_pmn_prsmnn;
drop index idx_cmp_nom;
drop index idxu_plcprs;
drop index idxu_plc_cmpprs;
drop index idxu_nrc_nom;
drop index idxu_act_nom;
drop index idxu_egn_nom;
drop index idxu_arm_agnelm;
drop index idxu_cgn_egn;
drop index idxu_vgn_egn;
drop index idxu_agd_ugntdg;
drop index idxu_agz_ugnzon;
drop index idxu_vgp_vgntpt;
drop index idxu_aru_elm;
drop index idxu_rch_elm;
drop index idxu_vss_sct;
drop index idxu_vss_elm;
drop index idxu_inc_clsint;
drop index idxu_pqa_cln;
drop index idxu_rac_nom;
drop index idxu_acc_chmprs;
drop index idxu_mnc_mndprs;
drop index idxu_lng_nom;
drop index idx_lic_lie_id;
drop index idx_clc_clq_id;
drop index idx_clc_lic_id;
drop index idxu_cff_elm;
drop index idxu_cel_cffelm;
drop index idxu_elv_prselm;
drop index idxu_btq_cff;
drop index idxu_egc_egnprs;
drop index idxu_acr_atmcrc;
drop index idxu_aeg_egnbtq;
drop index idxu_pcl_procls;
drop index idxu_pra_prorac;
drop index idxu_psx_prosex;
-- alter table personnage drop constraint fk_pj_joueur-- (is implicitly done)
-- alter table mj drop constraint fk_mj_joueur-- (is implicitly done)
-- alter table perso_classe drop constraint fk_ppc_perso-- (is implicitly done)
-- alter table forme drop constraint fk_rf_race-- (is implicitly done)
-- alter table classe_niveau drop constraint fk_ccn_classe-- (is implicitly done)
-- alter table perso_maladie drop constraint fk_ppm_perso-- (is implicitly done)
-- alter table classe_niveau drop constraint fk_mcn_maladie-- (is implicitly done)
-- alter table race drop constraint fk_mr_maladie-- (is implicitly done)
-- alter table perso_maladie drop constraint fk_mpm_maladie-- (is implicitly done)
-- alter table perso_carac drop constraint fk_cpc_carac-- (is implicitly done)
-- alter table perso_carac drop constraint fk_ppc_carac-- (is implicitly done)
-- alter table parametre drop constraint fk_cp_categorie-- (is implicitly done)
-- alter table personnage drop constraint fk_prs_possesseur-- (is implicitly done)
-- alter table perso_classe_int drop constraint fk_pci_perso-- (is implicitly done)
-- alter table perso_classe_int drop constraint fk_pci_cls-- (is implicitly done)
-- alter table classe drop constraint fk_tcl_typcls-- (is implicitly done)
-- alter table monde drop constraint fk_mnd_pere-- (is implicitly done)
-- alter table lieu drop constraint fk_scl_secteur-- (is implicitly done)
-- alter table chemin drop constraint fk_lch_depart-- (is implicitly done)
-- alter table chemin drop constraint fk_lch_arrivee-- (is implicitly done)
-- alter table chemin drop constraint fk_ec_cle-- (is implicitly done)
-- alter table personnage drop constraint fk_pl_depart-- (is implicitly done)
-- alter table perso_classe drop constraint fk_pcn_clniv-- (is implicitly done)
-- alter table forme drop constraint fk_pf_perso-- (is implicitly done)
-- alter table perso_monnaie drop constraint fk_pm_perso-- (is implicitly done)
-- alter table perso_monnaie drop constraint fk_pm_monnaie-- (is implicitly done)
-- alter table place_compagnie drop constraint fk_pcp_perso-- (is implicitly done)
-- alter table place_compagnie drop constraint fk_pcp_cmp-- (is implicitly done)
-- alter table maladie drop constraint fk_mtm_typmal-- (is implicitly done)
-- alter table secteur drop constraint fk_sts_typsct-- (is implicitly done)
-- alter table chemin drop constraint fk_ctc_typchm-- (is implicitly done)
-- alter table personnage drop constraint fk_ptp_typprs-- (is implicitly done)
-- alter table caracteristique drop constraint fk_ctc_typcrc-- (is implicitly done)
-- alter table lieu_action drop constraint fk_lac_action-- (is implicitly done)
-- alter table typelieu_action drop constraint fk_tla_action-- (is implicitly done)
-- alter table lieu_action drop constraint fk_lac_lieu-- (is implicitly done)
-- alter table lieu drop constraint fk_tli_typelieu-- (is implicitly done)
-- alter table typelieu_action drop constraint fk_tla_typelieu-- (is implicitly done)
-- alter table debit_element drop constraint fk_dli_dbtlie-- (is implicitly done)
-- alter table vente_element drop constraint fk_vli_vntlie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dlie_dptlie-- (is implicitly done)
-- alter table banque drop constraint fk_bli_bnqlie-- (is implicitly done)
-- alter table caracteristique drop constraint fk_ccc_catcrc-- (is implicitly done)
-- alter table element drop constraint fk_ege_generique-- (is implicitly done)
-- alter table arme drop constraint fk_ar_recharge-- (is implicitly done)
-- alter table arme drop constraint fk_aga_generique-- (is implicitly done)
-- alter table arme_gen drop constraint fk_egag_arme-- (is implicitly done)
-- alter table element_gen drop constraint fk_egm_matiere-- (is implicitly done)
-- alter table element_gen drop constraint fk_egt_techno-- (is implicitly done)
-- alter table armure_gen drop constraint fk_egug_armure-- (is implicitly done)
-- alter table recharge_gen drop constraint fk_egcg_recharge-- (is implicitly done)
-- alter table vaisseau_gen drop constraint fk_egvg_vaisseau-- (is implicitly done)
-- alter table vaisseau_gen drop constraint fk_tvg_typevaisseau-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agm_maladie-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agtmt_type_munition-- (is implicitly done)
-- alter table recharge_gen drop constraint fk_cgmn_munition-- (is implicitly done)
-- alter table munition drop constraint fk_mnm_maladie-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agtd_degat-- (is implicitly done)
-- alter table armure_gen_zone drop constraint fk_agz_zone-- (is implicitly done)
-- alter table armure_gen_zone drop constraint fk_agz_zones-- (is implicitly done)
-- alter table armure_gen_degat drop constraint fk_agd_degat-- (is implicitly done)
-- alter table armure_gen_degat drop constraint fk_agd_degats-- (is implicitly done)
-- alter table vaisseau_gen_poste drop constraint fk_vgtp_poste-- (is implicitly done)
-- alter table vaisseau_gen_poste drop constraint fk_tvp_typeposte-- (is implicitly done)
-- alter table element_gen drop constraint fk_egt_type-- (is implicitly done)
-- alter table armure drop constraint fk_aue_armure-- (is implicitly done)
-- alter table recharge drop constraint fk_re_recharge-- (is implicitly done)
-- alter table vaisseau drop constraint fk_ve_vaisseau-- (is implicitly done)
-- alter table vaisseau drop constraint fk_vs_vaisseau-- (is implicitly done)
-- alter table element drop constraint fk_ep_prs-- (is implicitly done)
-- alter table forme drop constraint fk_sf_sexe-- (is implicitly done)
-- alter table arme drop constraint fk_ae_arme-- (is implicitly done)
-- alter table interdit_classe drop constraint fk_cli_cls-- (is implicitly done)
-- alter table interdit_classe drop constraint fk_cli_clsint-- (is implicitly done)
-- alter table prerequis_classe drop constraint fk_pqc_cls-- (is implicitly done)
-- alter table prerequis_carac drop constraint fk_pqa_clsniv-- (is implicitly done)
-- alter table prerequis_classe drop constraint fk_pqc_cln-- (is implicitly done)
-- alter table element drop constraint fk_el_lieu-- (is implicitly done)
-- alter table acces_connu drop constraint fk_acp_prs-- (is implicitly done)
-- alter table acces_connu drop constraint fk_ach_sct-- (is implicitly done)
-- alter table monde_connu drop constraint fk_mcp_prs-- (is implicitly done)
-- alter table monde_connu drop constraint fk_mcm_mnd-- (is implicitly done)
-- alter table munition drop constraint fk_cgtmt_type_munition-- (is implicitly done)
-- alter table joueur drop constraint fk_jc_joueurcss-- (is implicitly done)
-- alter table itineraire drop constraint fk_ich_itineraire-- (is implicitly done)
-- alter table itineraire drop constraint fk_frmit_itineraire-- (is implicitly done)
-- alter table filature drop constraint fk_ftf_typflt-- (is implicitly done)
-- alter table filature drop constraint fk_ftprs_prssuivant-- (is implicitly done)
-- alter table filature drop constraint fk_ftfrm_prssuivi-- (is implicitly done)
-- alter table mime drop constraint fk_mtm_typmim-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrprs_auteur-- (is implicitly done)
-- alter table rumeur_ecoute drop constraint fk_rmc_ecouteur-- (is implicitly done)
-- alter table rumeur_ecoute drop constraint fk_rmc_rumeur-- (is implicitly done)
-- alter table langue drop constraint fk_ctl_conlangue-- (is implicitly done)
-- alter table itineraire drop constraint fk_itt_type_trace-- (is implicitly done)
-- alter table trace_connue drop constraint fk_tctt_type_trace-- (is implicitly done)
-- alter table trace_connue drop constraint fk_ptc_itineraire-- (is implicitly done)
-- alter table trace_connue drop constraint fk_ptct_itineraire-- (is implicitly done)
-- alter table forme drop constraint fk_fot_type_trace-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrlng_langue-- (is implicitly done)
-- alter table calque_lieu_carto drop constraint fk_lcclc_lieucalque-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_liel_lieucarto-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_lcm_lieucmonde-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_lcs_lieucsaison-- (is implicitly done)
-- alter table monde drop constraint fk_mtm_typmnd-- (is implicitly done)
-- alter table cycle drop constraint fk_ccl_cycle-- (is implicitly done)
-- alter table cycle_monde drop constraint fk_cclm_cycle_monde-- (is implicitly done)
-- alter table cycle_monde drop constraint fk_cmn_monde-- (is implicitly done)
-- alter table cycle_secteur drop constraint fk_cclsc_cycle_secteur-- (is implicitly done)
-- alter table cycle_secteur drop constraint fk_csc_secteur-- (is implicitly done)
-- alter table lieu_cycle drop constraint fk_ccll_lieu_cycle-- (is implicitly done)
-- alter table lieu_cycle drop constraint fk_lcdl_lieu-- (is implicitly done)
-- alter table periode drop constraint fk_ccl_cperiode-- (is implicitly done)
-- alter table lieu_cyclique drop constraint fk_lccl_lieu-- (is implicitly done)
-- alter table lieu_cyclique drop constraint fk_lcdp_lcycle-- (is implicitly done)
-- alter table calque drop constraint fk_tcq_calque_tcalque-- (is implicitly done)
-- alter table calque_lieu_carto drop constraint fk_clqclc_cartocalque-- (is implicitly done)
-- alter table vent_lieu_carto drop constraint fk_lctvt_lieuvent-- (is implicitly done)
-- alter table vent_lieu_carto drop constraint fk_vlctv_cartovent-- (is implicitly done)
-- alter table calque drop constraint fk_clcrc_calquecarac-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrsct_secteur-- (is implicitly done)
-- alter table restriction_calque drop constraint fk_rtc_calque-- (is implicitly done)
-- alter table influence_vent drop constraint fk_tvi_typevent-- (is implicitly done)
-- alter table influence_vent drop constraint fk_tvi_typevehicule-- (is implicitly done)
-- alter table restriction_calque drop constraint fk_tvr_typevehicule-- (is implicitly done)
-- alter table vehicule drop constraint fk_vht_typevehiculeposte-- (is implicitly done)
-- alter table secteur drop constraint fk_svh_vehicule-- (is implicitly done)
-- alter table type_vehicule_poste drop constraint fk_ttp_typevehicule-- (is implicitly done)
-- alter table type_vehicule_poste drop constraint fk_ttp_typeposte-- (is implicitly done)
-- alter table poste drop constraint fk_pst_typeposte-- (is implicitly done)
-- alter table poste drop constraint fk_pst_vehicule-- (is implicitly done)
-- alter table monture drop constraint fk_monture_cavperso-- (is implicitly done)
-- alter table monture drop constraint fk_mon_perso-- (is implicitly done)
-- alter table monture drop constraint fk_monture_element-- (is implicitly done)
-- alter table monture drop constraint fk_monture_elementgen-- (is implicitly done)
-- alter table chemin drop constraint fk_chcrc_carac-- (is implicitly done)
-- alter table chemin drop constraint fk_ctc_parchm-- (is implicitly done)
-- alter table description_lieu drop constraint fk_desclieu-- (is implicitly done)
-- alter table classeniveau_action drop constraint fk_cna_action-- (is implicitly done)
-- alter table classeniveau_action drop constraint fk_cnacln_action-- (is implicitly done)
-- alter table chemin drop constraint fk_chcrc_carac_2-- (is implicitly done)
-- alter table calque drop constraint fk_clcrc_calquecarac2-- (is implicitly done)
-- alter table poste drop constraint fk_pst_perso-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_perso_un-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_perso_deux-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_lieu-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_1-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_2-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_3-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_4-- (is implicitly done)
-- alter table element_gen drop constraint fk_egn_maladie_possession-- (is implicitly done)
-- alter table element_gen drop constraint fk_egn_maladie_equipe-- (is implicitly done)
-- alter table type_vehicule drop constraint fk_tveg_carburant-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrlie_lieu-- (is implicitly done)
-- alter table itineraire drop constraint fk_depiti_lieu-- (is implicitly done)
-- alter table itineraire drop constraint fk_arriti_lieu-- (is implicitly done)
-- alter table monde drop constraint fk_mtm_maladie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dpt_element-- (is implicitly done)
-- alter table vente_element drop constraint fk_vnt_element-- (is implicitly done)
-- alter table banque drop constraint fk_bnq_possesseur-- (is implicitly done)
-- alter table banque drop constraint fk_bnq_monnaie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dpt_possesseur-- (is implicitly done)
-- alter table debit_element drop constraint fk_dbt_element-- (is implicitly done)
-- alter table coffre drop constraint fk_ce_coffre-- (is implicitly done)
-- alter table coffre_element drop constraint fk_cee_coffre-- (is implicitly done)
-- alter table coffre_element drop constraint fk_cce_coffre-- (is implicitly done)
-- alter table coffre drop constraint fk_ceg_cle_coffre-- (is implicitly done)
-- alter table element_visible drop constraint fk_elv_elm-- (is implicitly done)
-- alter table element_visible drop constraint fk_elv_prs-- (is implicitly done)
-- alter table lieu drop constraint fk_iml_image-- (is implicitly done)
-- alter table element_gen_connu drop constraint fk_egc_element_gen_connu-- (is implicitly done)
-- alter table element_gen_connu drop constraint fk_egc_personnage-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_personnage-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_automate-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_coffre-- (is implicitly done)
-- alter table automate_carac drop constraint fk_acr_automate-- (is implicitly done)
-- alter table automate_carac drop constraint fk_acr_carac-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_aeg_boutique-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_aeg_element_gen-- (is implicitly done)
-- alter table coffre_element drop constraint fk_mce_monnaie-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_mae_monnaie-- (is implicitly done)
-- alter table secteur drop constraint fk_ste_typevn-- (is implicitly done)
-- alter table type_environnement drop constraint fk_tec_carac-- (is implicitly done)
-- alter table reponse drop constraint fk_rq_question-- (is implicitly done)
-- alter table news drop constraint fk_mn_mj-- (is implicitly done)
-- alter table candidature drop constraint fk_jc_joueur-- (is implicitly done)
-- alter table profil_annonce drop constraint fk_map_mj-- (is implicitly done)
-- alter table candidature drop constraint fk_pac_profil-- (is implicitly done)
-- alter table profil_annonce drop constraint fk_cpp_catpro-- (is implicitly done)
-- alter table candidature drop constraint fk_cxc_sexe-- (is implicitly done)
-- alter table profil_classe drop constraint fk_pac_proann-- (is implicitly done)
-- alter table profil_race drop constraint fk_par_proann-- (is implicitly done)
-- alter table profil_sexe drop constraint fk_pas_proann-- (is implicitly done)
-- alter table candidature drop constraint fk_cr_race-- (is implicitly done)
-- alter table candidature drop constraint fk_cc_classe-- (is implicitly done)
-- alter table profil_classe drop constraint fk_cpc_classe-- (is implicitly done)
-- alter table profil_race drop constraint fk_pr_race-- (is implicitly done)
-- alter table profil_sexe drop constraint fk_ps_sexe-- (is implicitly done)

