#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import with_statement
import gzip
import optparse
import sys
import os
import shutil
from xml.dom import minidom

usage = """usage: %prog [options] diafile.dia

Remove placeholders of a dia file and connect reference to theses placeholder
to real class."""

parser = optparse.OptionParser(usage)
(options, args) = parser.parse_args()

if not args:
    parser.error("A dia file must be provided.")

filename = args[0]
try:
    assert os.path.isfile(filename)
except AssertionError:
    parser.error("The file %s doesn't exist." % filename)

# decompress
new_filename = filename + '.xml'
f = gzip.open(filename, 'rb')
with open(new_filename, 'wb') as f_out:
    f_out.write(f.read())
f.close()

def isPlaceholder(node):
    for child in node.childNodes:
        if child.nodeName != u'dia:attribute' or \
           child.getAttribute(u'name') != u'stereotype':
            continue
        for subchild in child.childNodes:
            if subchild.nodeName == u'dia:string':
                for textNode in subchild.childNodes:
                    if textNode.nodeValue == u'#placeholder#':
                        return True

def getName(node):
    for child in node.childNodes:
        if child.nodeName != u'dia:attribute' or \
           child.getAttribute(u'name') != u'name':
            continue
        for subchild in child.childNodes:
            if subchild.nodeName == u'dia:string':
                for textNode in subchild.childNodes:
                    return textNode.nodeValue

def correctConnections(node, placeholders):
    for connection in node.getElementsByTagName(u'dia:connection'):
        node_id = connection.getAttribute(u'to')
        if node_id in placeholders:
            connection.setAttribute(u'to', placeholders[node_id])

doc = minidom.parse(new_filename)
# store class and placeholder ids
placeholders, klass = {}, {}
for node in doc.getElementsByTagName(u'dia:object'):
    node_id = node.getAttribute(u'id')
    name = getName(node)
    if isPlaceholder(node):
        placeholders[node_id] = name
    else:
        klass[name] = node_id

# convert placeholder name to ids
for node_id in placeholders:
    placeholders[node_id] = klass[placeholders[node_id]]

# correct DOM
for node in doc.getElementsByTagName(u'dia:object'):
    if isPlaceholder(node):
        node.parentNode.removeChild(node)
    elif node.getAttribute(u'type') == u"UML - Association":
        correctConnections(node, placeholders)

# recompress
new_filename = filename[:-4] + '-corrected.dia'

f = gzip.open(new_filename, 'wb')
f.write(doc.toxml().encode('utf8'))
f.close()

