
-- Generated SQL Constraints Drop statements
-- --------------------------------------------------------------------

drop index idxu_prs_nom_originel;
drop index idxu_prs_nom_actuel;
drop index idxu_jou_pseudo;
drop index idxu_frm_prs_rac;
drop index idxu_cln_titre;
drop index idx_cln_clsniv;
drop index idxu_cls_nom;
drop index idxu_pml_prsmld;
drop index idxu_mld_nom;
drop index idxu_crc_nom;
drop index idxu_pcr_prscrc;
drop index idxu_ctg_libelle;
drop index idxu_pci_prscls;
drop index idxu_pqc_clncls;
drop index idxu_mnd_nom;
drop index idxu_lie_nom;
drop index idxu_sct_nom;
drop index idxu_ssn_nom;
drop index idxu_mnn_nom;
drop index idxu_pmn_prsmnn;
drop index idx_cmp_nom;
drop index idxu_plcprs;
drop index idxu_plc_cmpprs;
drop index idxu_nrc_nom;
drop index idxu_act_nom;
drop index idxu_egn_nom;
drop index idxu_arm_agnelm;
drop index idxu_cgn_egn;
drop index idxu_vgn_egn;
drop index idxu_agd_ugntdg;
drop index idxu_agz_ugnzon;
drop index idxu_vgp_vgntpt;
drop index idxu_aru_elm;
drop index idxu_rch_elm;
drop index idxu_vss_sct;
drop index idxu_vss_elm;
drop index idxu_inc_clsint;
drop index idxu_pqa_cln;
drop index idxu_rac_nom;
drop index idxu_acc_chmprs;
drop index idxu_mnc_mndprs;
drop index idxu_lng_nom;
drop index idx_lic_lie_id;
drop index idx_clc_clq_id;
drop index idx_clc_lic_id;
drop index idxu_cff_elm;
drop index idxu_cel_cffelm;
drop index idxu_elv_prselm;
drop index idxu_btq_cff;
drop index idxu_egc_egnprs;
drop index idxu_acr_atmcrc;
drop index idxu_aeg_egnbtq;
drop index idxu_pcl_procls;
drop index idxu_pra_prorac;
drop index idxu_psx_prosex;
-- alter table personnage drop constraint fk_pj_joueur-- (is implicitly done)
-- alter table mj drop constraint fk_mj_joueur-- (is implicitly done)
-- alter table perso_classe drop constraint fk_ppc_perso-- (is implicitly done)
-- alter table forme drop constraint fk_rf_race-- (is implicitly done)
-- alter table classe_niveau drop constraint fk_ccn_classe-- (is implicitly done)
-- alter table perso_maladie drop constraint fk_ppm_perso-- (is implicitly done)
-- alter table classe_niveau drop constraint fk_mcn_maladie-- (is implicitly done)
-- alter table race drop constraint fk_mr_maladie-- (is implicitly done)
-- alter table perso_maladie drop constraint fk_mpm_maladie-- (is implicitly done)
-- alter table perso_carac drop constraint fk_cpc_carac-- (is implicitly done)
-- alter table perso_carac drop constraint fk_ppc_carac-- (is implicitly done)
-- alter table parametre drop constraint fk_cp_categorie-- (is implicitly done)
-- alter table personnage drop constraint fk_prs_possesseur-- (is implicitly done)
-- alter table perso_classe_int drop constraint fk_pci_perso-- (is implicitly done)
-- alter table perso_classe_int drop constraint fk_pci_cls-- (is implicitly done)
-- alter table classe drop constraint fk_tcl_typcls-- (is implicitly done)
-- alter table monde drop constraint fk_mnd_pere-- (is implicitly done)
-- alter table lieu drop constraint fk_scl_secteur-- (is implicitly done)
-- alter table chemin drop constraint fk_lch_depart-- (is implicitly done)
-- alter table chemin drop constraint fk_lch_arrivee-- (is implicitly done)
-- alter table chemin drop constraint fk_ec_cle-- (is implicitly done)
-- alter table personnage drop constraint fk_pl_depart-- (is implicitly done)
-- alter table perso_classe drop constraint fk_pcn_clniv-- (is implicitly done)
-- alter table forme drop constraint fk_pf_perso-- (is implicitly done)
-- alter table perso_monnaie drop constraint fk_pm_perso-- (is implicitly done)
-- alter table perso_monnaie drop constraint fk_pm_monnaie-- (is implicitly done)
-- alter table place_compagnie drop constraint fk_pcp_perso-- (is implicitly done)
-- alter table place_compagnie drop constraint fk_pcp_cmp-- (is implicitly done)
-- alter table maladie drop constraint fk_mtm_typmal-- (is implicitly done)
-- alter table secteur drop constraint fk_sts_typsct-- (is implicitly done)
-- alter table chemin drop constraint fk_ctc_typchm-- (is implicitly done)
-- alter table personnage drop constraint fk_ptp_typprs-- (is implicitly done)
-- alter table caracteristique drop constraint fk_ctc_typcrc-- (is implicitly done)
-- alter table lieu_action drop constraint fk_lac_action-- (is implicitly done)
-- alter table typelieu_action drop constraint fk_tla_action-- (is implicitly done)
-- alter table lieu_action drop constraint fk_lac_lieu-- (is implicitly done)
-- alter table lieu drop constraint fk_tli_typelieu-- (is implicitly done)
-- alter table typelieu_action drop constraint fk_tla_typelieu-- (is implicitly done)
-- alter table debit_element drop constraint fk_dli_dbtlie-- (is implicitly done)
-- alter table vente_element drop constraint fk_vli_vntlie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dlie_dptlie-- (is implicitly done)
-- alter table banque drop constraint fk_bli_bnqlie-- (is implicitly done)
-- alter table caracteristique drop constraint fk_ccc_catcrc-- (is implicitly done)
-- alter table element drop constraint fk_ege_generique-- (is implicitly done)
-- alter table arme drop constraint fk_ar_recharge-- (is implicitly done)
-- alter table arme drop constraint fk_aga_generique-- (is implicitly done)
-- alter table arme_gen drop constraint fk_egag_arme-- (is implicitly done)
-- alter table element_gen drop constraint fk_egm_matiere-- (is implicitly done)
-- alter table element_gen drop constraint fk_egt_techno-- (is implicitly done)
-- alter table armure_gen drop constraint fk_egug_armure-- (is implicitly done)
-- alter table recharge_gen drop constraint fk_egcg_recharge-- (is implicitly done)
-- alter table vaisseau_gen drop constraint fk_egvg_vaisseau-- (is implicitly done)
-- alter table vaisseau_gen drop constraint fk_tvg_typevaisseau-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agm_maladie-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agtmt_type_munition-- (is implicitly done)
-- alter table recharge_gen drop constraint fk_cgmn_munition-- (is implicitly done)
-- alter table munition drop constraint fk_mnm_maladie-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agtd_degat-- (is implicitly done)
-- alter table armure_gen_zone drop constraint fk_agz_zone-- (is implicitly done)
-- alter table armure_gen_zone drop constraint fk_agz_zones-- (is implicitly done)
-- alter table armure_gen_degat drop constraint fk_agd_degat-- (is implicitly done)
-- alter table armure_gen_degat drop constraint fk_agd_degats-- (is implicitly done)
-- alter table vaisseau_gen_poste drop constraint fk_vgtp_poste-- (is implicitly done)
-- alter table vaisseau_gen_poste drop constraint fk_tvp_typeposte-- (is implicitly done)
-- alter table element_gen drop constraint fk_egt_type-- (is implicitly done)
-- alter table armure drop constraint fk_aue_armure-- (is implicitly done)
-- alter table recharge drop constraint fk_re_recharge-- (is implicitly done)
-- alter table vaisseau drop constraint fk_ve_vaisseau-- (is implicitly done)
-- alter table vaisseau drop constraint fk_vs_vaisseau-- (is implicitly done)
-- alter table element drop constraint fk_ep_prs-- (is implicitly done)
-- alter table forme drop constraint fk_sf_sexe-- (is implicitly done)
-- alter table arme drop constraint fk_ae_arme-- (is implicitly done)
-- alter table interdit_classe drop constraint fk_cli_cls-- (is implicitly done)
-- alter table interdit_classe drop constraint fk_cli_clsint-- (is implicitly done)
-- alter table prerequis_classe drop constraint fk_pqc_cls-- (is implicitly done)
-- alter table prerequis_carac drop constraint fk_pqa_clsniv-- (is implicitly done)
-- alter table prerequis_classe drop constraint fk_pqc_cln-- (is implicitly done)
-- alter table element drop constraint fk_el_lieu-- (is implicitly done)
-- alter table acces_connu drop constraint fk_acp_prs-- (is implicitly done)
-- alter table acces_connu drop constraint fk_ach_sct-- (is implicitly done)
-- alter table monde_connu drop constraint fk_mcp_prs-- (is implicitly done)
-- alter table monde_connu drop constraint fk_mcm_mnd-- (is implicitly done)
-- alter table munition drop constraint fk_cgtmt_type_munition-- (is implicitly done)
-- alter table joueur drop constraint fk_jc_joueurcss-- (is implicitly done)
-- alter table itineraire drop constraint fk_ich_itineraire-- (is implicitly done)
-- alter table itineraire drop constraint fk_frmit_itineraire-- (is implicitly done)
-- alter table filature drop constraint fk_ftf_typflt-- (is implicitly done)
-- alter table filature drop constraint fk_ftprs_prssuivant-- (is implicitly done)
-- alter table filature drop constraint fk_ftfrm_prssuivi-- (is implicitly done)
-- alter table mime drop constraint fk_mtm_typmim-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrprs_auteur-- (is implicitly done)
-- alter table rumeur_ecoute drop constraint fk_rmc_ecouteur-- (is implicitly done)
-- alter table rumeur_ecoute drop constraint fk_rmc_rumeur-- (is implicitly done)
-- alter table langue drop constraint fk_ctl_conlangue-- (is implicitly done)
-- alter table itineraire drop constraint fk_itt_type_trace-- (is implicitly done)
-- alter table trace_connue drop constraint fk_tctt_type_trace-- (is implicitly done)
-- alter table trace_connue drop constraint fk_ptc_itineraire-- (is implicitly done)
-- alter table trace_connue drop constraint fk_ptct_itineraire-- (is implicitly done)
-- alter table forme drop constraint fk_fot_type_trace-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrlng_langue-- (is implicitly done)
-- alter table calque_lieu_carto drop constraint fk_lcclc_lieucalque-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_liel_lieucarto-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_lcm_lieucmonde-- (is implicitly done)
-- alter table lieu_carto drop constraint fk_lcs_lieucsaison-- (is implicitly done)
-- alter table monde drop constraint fk_mtm_typmnd-- (is implicitly done)
-- alter table cycle drop constraint fk_ccl_cycle-- (is implicitly done)
-- alter table cycle_monde drop constraint fk_cclm_cycle_monde-- (is implicitly done)
-- alter table cycle_monde drop constraint fk_cmn_monde-- (is implicitly done)
-- alter table cycle_secteur drop constraint fk_cclsc_cycle_secteur-- (is implicitly done)
-- alter table cycle_secteur drop constraint fk_csc_secteur-- (is implicitly done)
-- alter table lieu_cycle drop constraint fk_ccll_lieu_cycle-- (is implicitly done)
-- alter table lieu_cycle drop constraint fk_lcdl_lieu-- (is implicitly done)
-- alter table periode drop constraint fk_ccl_cperiode-- (is implicitly done)
-- alter table lieu_cyclique drop constraint fk_lccl_lieu-- (is implicitly done)
-- alter table lieu_cyclique drop constraint fk_lcdp_lcycle-- (is implicitly done)
-- alter table calque drop constraint fk_tcq_calque_tcalque-- (is implicitly done)
-- alter table calque_lieu_carto drop constraint fk_clqclc_cartocalque-- (is implicitly done)
-- alter table vent_lieu_carto drop constraint fk_lctvt_lieuvent-- (is implicitly done)
-- alter table vent_lieu_carto drop constraint fk_vlctv_cartovent-- (is implicitly done)
-- alter table calque drop constraint fk_clcrc_calquecarac-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrsct_secteur-- (is implicitly done)
-- alter table restriction_calque drop constraint fk_rtc_calque-- (is implicitly done)
-- alter table influence_vent drop constraint fk_tvi_typevent-- (is implicitly done)
-- alter table influence_vent drop constraint fk_tvi_typevehicule-- (is implicitly done)
-- alter table restriction_calque drop constraint fk_tvr_typevehicule-- (is implicitly done)
-- alter table vehicule drop constraint fk_vht_typevehiculeposte-- (is implicitly done)
-- alter table secteur drop constraint fk_svh_vehicule-- (is implicitly done)
-- alter table type_vehicule_poste drop constraint fk_ttp_typevehicule-- (is implicitly done)
-- alter table type_vehicule_poste drop constraint fk_ttp_typeposte-- (is implicitly done)
-- alter table poste drop constraint fk_pst_typeposte-- (is implicitly done)
-- alter table poste drop constraint fk_pst_vehicule-- (is implicitly done)
-- alter table monture drop constraint fk_monture_cavperso-- (is implicitly done)
-- alter table monture drop constraint fk_mon_perso-- (is implicitly done)
-- alter table monture drop constraint fk_monture_element-- (is implicitly done)
-- alter table monture drop constraint fk_monture_elementgen-- (is implicitly done)
-- alter table chemin drop constraint fk_chcrc_carac-- (is implicitly done)
-- alter table chemin drop constraint fk_ctc_parchm-- (is implicitly done)
-- alter table description_lieu drop constraint fk_desclieu-- (is implicitly done)
-- alter table classeniveau_action drop constraint fk_cna_action-- (is implicitly done)
-- alter table classeniveau_action drop constraint fk_cnacln_action-- (is implicitly done)
-- alter table chemin drop constraint fk_chcrc_carac_2-- (is implicitly done)
-- alter table calque drop constraint fk_clcrc_calquecarac2-- (is implicitly done)
-- alter table poste drop constraint fk_pst_perso-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_perso_un-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_perso_deux-- (is implicitly done)
-- alter table engagement drop constraint fk_eng_lieu-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_1-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_2-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_3-- (is implicitly done)
-- alter table arme_gen drop constraint fk_agcrc_4-- (is implicitly done)
-- alter table element_gen drop constraint fk_egn_maladie_possession-- (is implicitly done)
-- alter table element_gen drop constraint fk_egn_maladie_equipe-- (is implicitly done)
-- alter table type_vehicule drop constraint fk_tveg_carburant-- (is implicitly done)
-- alter table rumeur drop constraint fk_rmrlie_lieu-- (is implicitly done)
-- alter table itineraire drop constraint fk_depiti_lieu-- (is implicitly done)
-- alter table itineraire drop constraint fk_arriti_lieu-- (is implicitly done)
-- alter table monde drop constraint fk_mtm_maladie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dpt_element-- (is implicitly done)
-- alter table vente_element drop constraint fk_vnt_element-- (is implicitly done)
-- alter table banque drop constraint fk_bnq_possesseur-- (is implicitly done)
-- alter table banque drop constraint fk_bnq_monnaie-- (is implicitly done)
-- alter table depot_element drop constraint fk_dpt_possesseur-- (is implicitly done)
-- alter table debit_element drop constraint fk_dbt_element-- (is implicitly done)
-- alter table coffre drop constraint fk_ce_coffre-- (is implicitly done)
-- alter table coffre_element drop constraint fk_cee_coffre-- (is implicitly done)
-- alter table coffre_element drop constraint fk_cce_coffre-- (is implicitly done)
-- alter table coffre drop constraint fk_ceg_cle_coffre-- (is implicitly done)
-- alter table element_visible drop constraint fk_elv_elm-- (is implicitly done)
-- alter table element_visible drop constraint fk_elv_prs-- (is implicitly done)
-- alter table lieu drop constraint fk_iml_image-- (is implicitly done)
-- alter table element_gen_connu drop constraint fk_egc_element_gen_connu-- (is implicitly done)
-- alter table element_gen_connu drop constraint fk_egc_personnage-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_personnage-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_automate-- (is implicitly done)
-- alter table boutique drop constraint fk_btq_coffre-- (is implicitly done)
-- alter table automate_carac drop constraint fk_acr_automate-- (is implicitly done)
-- alter table automate_carac drop constraint fk_acr_carac-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_aeg_boutique-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_aeg_element_gen-- (is implicitly done)
-- alter table coffre_element drop constraint fk_mce_monnaie-- (is implicitly done)
-- alter table achat_element_gen drop constraint fk_mae_monnaie-- (is implicitly done)
-- alter table secteur drop constraint fk_ste_typevn-- (is implicitly done)
-- alter table type_environnement drop constraint fk_tec_carac-- (is implicitly done)
-- alter table reponse drop constraint fk_rq_question-- (is implicitly done)
-- alter table news drop constraint fk_mn_mj-- (is implicitly done)
-- alter table candidature drop constraint fk_jc_joueur-- (is implicitly done)
-- alter table profil_annonce drop constraint fk_map_mj-- (is implicitly done)
-- alter table candidature drop constraint fk_pac_profil-- (is implicitly done)
-- alter table profil_annonce drop constraint fk_cpp_catpro-- (is implicitly done)
-- alter table candidature drop constraint fk_cxc_sexe-- (is implicitly done)
-- alter table profil_classe drop constraint fk_pac_proann-- (is implicitly done)
-- alter table profil_race drop constraint fk_par_proann-- (is implicitly done)
-- alter table profil_sexe drop constraint fk_pas_proann-- (is implicitly done)
-- alter table candidature drop constraint fk_cr_race-- (is implicitly done)
-- alter table candidature drop constraint fk_cc_classe-- (is implicitly done)
-- alter table profil_classe drop constraint fk_cpc_classe-- (is implicitly done)
-- alter table profil_race drop constraint fk_pr_race-- (is implicitly done)
-- alter table profil_sexe drop constraint fk_ps_sexe-- (is implicitly done)


-- Generated Permissions Drops
-- --------------------------------------------------------------------

revoke all on personnage from morrigan ;
revoke select on personnage from GROUP meuji ;
revoke select on personnage from GROUP general ;
revoke select, insert, delete, update on personnage from supermj ;
revoke all on joueur from morrigan ;
revoke select on joueur from GROUP meuji ;
revoke select on joueur from GROUP general ;
revoke select, insert, delete, update on joueur from supermj ;
revoke all on mj from morrigan ;
revoke select on mj from GROUP meuji ;
revoke select on mj from GROUP general ;
revoke select, insert, delete, update on mj from supermj ;
revoke all on perso_classe from morrigan ;
revoke select on perso_classe from GROUP meuji ;
revoke select on perso_classe from GROUP general ;
revoke select, insert, delete, update on perso_classe from supermj ;
revoke all on forme from morrigan ;
revoke select on forme from GROUP meuji ;
revoke select on forme from GROUP general ;
revoke select, insert, delete, update on forme from supermj ;
revoke all on classe_niveau from morrigan ;
revoke select on classe_niveau from GROUP meuji ;
revoke select on classe_niveau from GROUP general ;
revoke select, insert, delete, update on classe_niveau from supermj ;
revoke all on classe from morrigan ;
revoke select on classe from GROUP meuji ;
revoke select on classe from GROUP general ;
revoke select, insert, delete, update on classe from supermj ;
revoke all on perso_maladie from morrigan ;
revoke select on perso_maladie from GROUP meuji ;
revoke select on perso_maladie from GROUP general ;
revoke select, insert, delete, update on perso_maladie from supermj ;
revoke all on maladie from morrigan ;
revoke select on maladie from GROUP meuji ;
revoke select on maladie from GROUP general ;
revoke select, insert, delete, update on maladie from supermj ;
revoke all on caracteristique from morrigan ;
revoke select on caracteristique from GROUP meuji ;
revoke select on caracteristique from GROUP general ;
revoke select, insert, delete, update on caracteristique from supermj ;
revoke all on perso_carac from morrigan ;
revoke select on perso_carac from GROUP meuji ;
revoke select on perso_carac from GROUP general ;
revoke select, insert, delete, update on perso_carac from supermj ;
revoke all on parametre from morrigan ;
revoke select on parametre from GROUP meuji ;
revoke select on parametre from GROUP general ;
revoke select, insert, delete, update on parametre from supermj ;
revoke all on categorie from morrigan ;
revoke select on categorie from GROUP meuji ;
revoke select on categorie from GROUP general ;
revoke select, insert, delete, update on categorie from supermj ;
revoke all on perso_classe_int from morrigan ;
revoke select on perso_classe_int from GROUP meuji ;
revoke select on perso_classe_int from GROUP general ;
revoke select, insert, delete, update on perso_classe_int from supermj ;
revoke all on prerequis_classe from morrigan ;
revoke select on prerequis_classe from GROUP meuji ;
revoke select on prerequis_classe from GROUP general ;
revoke select, insert, delete, update on prerequis_classe from supermj ;
revoke all on monde from morrigan ;
revoke select on monde from GROUP meuji ;
revoke select on monde from GROUP general ;
revoke select, insert, delete, update on monde from supermj ;
revoke all on lieu from morrigan ;
revoke select on lieu from GROUP meuji ;
revoke select on lieu from GROUP general ;
revoke select, insert, delete, update on lieu from supermj ;
revoke all on secteur from morrigan ;
revoke select on secteur from GROUP meuji ;
revoke select on secteur from GROUP general ;
revoke select, insert, delete, update on secteur from supermj ;
revoke all on chemin from morrigan ;
revoke select on chemin from GROUP meuji ;
revoke select on chemin from GROUP general ;
revoke select, insert, delete, update on chemin from supermj ;
revoke all on saison from morrigan ;
revoke select on saison from GROUP meuji ;
revoke select on saison from GROUP general ;
revoke select, insert, delete, update on saison from supermj ;
revoke all on element from morrigan ;
revoke select on element from GROUP meuji ;
revoke select on element from GROUP general ;
revoke select, insert, delete, update on element from supermj ;
revoke all on monnaie from morrigan ;
revoke select on monnaie from GROUP meuji ;
revoke select on monnaie from GROUP general ;
revoke select, insert, delete, update on monnaie from supermj ;
revoke all on perso_monnaie from morrigan ;
revoke select on perso_monnaie from GROUP meuji ;
revoke select on perso_monnaie from GROUP general ;
revoke select, insert, delete, update on perso_monnaie from supermj ;
revoke all on compagnie from morrigan ;
revoke select on compagnie from GROUP meuji ;
revoke select on compagnie from GROUP general ;
revoke select, insert, delete, update on compagnie from supermj ;
revoke all on place_compagnie from morrigan ;
revoke select on place_compagnie from GROUP meuji ;
revoke select on place_compagnie from GROUP general ;
revoke select, insert, delete, update on place_compagnie from supermj ;
revoke all on sexe from morrigan ;
revoke select on sexe from GROUP meuji ;
revoke select on sexe from GROUP general ;
revoke select, insert, delete, update on sexe from supermj ;
revoke all on type_personnage from morrigan ;
revoke select on type_personnage from GROUP meuji ;
revoke select on type_personnage from GROUP general ;
revoke select, insert, delete, update on type_personnage from supermj ;
revoke all on type_maladie from morrigan ;
revoke select on type_maladie from GROUP meuji ;
revoke select on type_maladie from GROUP general ;
revoke select, insert, delete, update on type_maladie from supermj ;
revoke all on type_caracteristique from morrigan ;
revoke select on type_caracteristique from GROUP meuji ;
revoke select on type_caracteristique from GROUP general ;
revoke select, insert, delete, update on type_caracteristique from supermj ;
revoke all on nature_caracteristique from morrigan ;
revoke select on nature_caracteristique from GROUP meuji ;
revoke select on nature_caracteristique from GROUP general ;
revoke select, insert, delete, update on nature_caracteristique from supermj ;
revoke all on type_secteur from morrigan ;
revoke select on type_secteur from GROUP meuji ;
revoke select on type_secteur from GROUP general ;
revoke select, insert, delete, update on type_secteur from supermj ;
revoke all on type_monde from morrigan ;
revoke select on type_monde from GROUP meuji ;
revoke select on type_monde from GROUP general ;
revoke select, insert, delete, update on type_monde from supermj ;
revoke all on type_chemin from morrigan ;
revoke select on type_chemin from GROUP meuji ;
revoke select on type_chemin from GROUP general ;
revoke select, insert, delete, update on type_chemin from supermj ;
revoke all on action from morrigan ;
revoke select on action from GROUP meuji ;
revoke select on action from GROUP general ;
revoke select, insert, delete, update on action from supermj ;
revoke all on lieu_action from morrigan ;
revoke select on lieu_action from GROUP meuji ;
revoke select on lieu_action from GROUP general ;
revoke select, insert, delete, update on lieu_action from supermj ;
revoke all on typelieu_action from morrigan ;
revoke select on typelieu_action from GROUP meuji ;
revoke select on typelieu_action from GROUP general ;
revoke select, insert, delete, update on typelieu_action from supermj ;
revoke all on type_lieu from morrigan ;
revoke select on type_lieu from GROUP meuji ;
revoke select on type_lieu from GROUP general ;
revoke select, insert, delete, update on type_lieu from supermj ;
revoke all on debit_element from morrigan ;
revoke select on debit_element from GROUP meuji ;
revoke select on debit_element from GROUP general ;
revoke select, insert, delete, update on debit_element from supermj ;
revoke all on vente_element from morrigan ;
revoke select on vente_element from GROUP meuji ;
revoke select on vente_element from GROUP general ;
revoke select, insert, delete, update on vente_element from supermj ;
revoke all on depot_element from morrigan ;
revoke select on depot_element from GROUP meuji ;
revoke select on depot_element from GROUP general ;
revoke select, insert, delete, update on depot_element from supermj ;
revoke all on banque from morrigan ;
revoke select on banque from GROUP meuji ;
revoke select on banque from GROUP general ;
revoke select, insert, delete, update on banque from supermj ;
revoke all on element_gen from morrigan ;
revoke select on element_gen from GROUP meuji ;
revoke select on element_gen from GROUP general ;
revoke select, insert, delete, update on element_gen from supermj ;
revoke all on arme from morrigan ;
revoke select on arme from GROUP meuji ;
revoke select on arme from GROUP general ;
revoke select, insert, delete, update on arme from supermj ;
revoke all on arme_gen from morrigan ;
revoke select on arme_gen from GROUP meuji ;
revoke select on arme_gen from GROUP general ;
revoke select, insert, delete, update on arme_gen from supermj ;
revoke all on matiere from morrigan ;
revoke select on matiere from GROUP meuji ;
revoke select on matiere from GROUP general ;
revoke select, insert, delete, update on matiere from supermj ;
revoke all on technologie from morrigan ;
revoke select on technologie from GROUP meuji ;
revoke select on technologie from GROUP general ;
revoke select, insert, delete, update on technologie from supermj ;
revoke all on armure_gen from morrigan ;
revoke select on armure_gen from GROUP meuji ;
revoke select on armure_gen from GROUP general ;
revoke select, insert, delete, update on armure_gen from supermj ;
revoke all on recharge_gen from morrigan ;
revoke select on recharge_gen from GROUP meuji ;
revoke select on recharge_gen from GROUP general ;
revoke select, insert, delete, update on recharge_gen from supermj ;
revoke all on vaisseau_gen from morrigan ;
revoke select on vaisseau_gen from GROUP meuji ;
revoke select on vaisseau_gen from GROUP general ;
revoke select, insert, delete, update on vaisseau_gen from supermj ;
revoke all on type_vaisseau from morrigan ;
revoke select on type_vaisseau from GROUP meuji ;
revoke select on type_vaisseau from GROUP general ;
revoke select, insert, delete, update on type_vaisseau from supermj ;
revoke all on munition from morrigan ;
revoke select on munition from GROUP meuji ;
revoke select on munition from GROUP general ;
revoke select, insert, delete, update on munition from supermj ;
revoke all on type_degat from morrigan ;
revoke select on type_degat from GROUP meuji ;
revoke select on type_degat from GROUP general ;
revoke select, insert, delete, update on type_degat from supermj ;
revoke all on armure_gen_degat from morrigan ;
revoke select on armure_gen_degat from GROUP meuji ;
revoke select on armure_gen_degat from GROUP general ;
revoke select, insert, delete, update on armure_gen_degat from supermj ;
revoke all on armure_gen_zone from morrigan ;
revoke select on armure_gen_zone from GROUP meuji ;
revoke select on armure_gen_zone from GROUP general ;
revoke select, insert, delete, update on armure_gen_zone from supermj ;
revoke all on zone_armure from morrigan ;
revoke select on zone_armure from GROUP meuji ;
revoke select on zone_armure from GROUP general ;
revoke select, insert, delete, update on zone_armure from supermj ;
revoke all on vaisseau_gen_poste from morrigan ;
revoke select on vaisseau_gen_poste from GROUP meuji ;
revoke select on vaisseau_gen_poste from GROUP general ;
revoke select, insert, delete, update on vaisseau_gen_poste from supermj ;
revoke all on type_poste_tmp from morrigan ;
revoke select on type_poste_tmp from GROUP meuji ;
revoke select on type_poste_tmp from GROUP general ;
revoke select, insert, delete, update on type_poste_tmp from supermj ;
revoke all on type_element from morrigan ;
revoke select on type_element from GROUP meuji ;
revoke select on type_element from GROUP general ;
revoke select, insert, delete, update on type_element from supermj ;
revoke all on armure from morrigan ;
revoke select on armure from GROUP meuji ;
revoke select on armure from GROUP general ;
revoke select, insert, delete, update on armure from supermj ;
revoke all on recharge from morrigan ;
revoke select on recharge from GROUP meuji ;
revoke select on recharge from GROUP general ;
revoke select, insert, delete, update on recharge from supermj ;
revoke all on vaisseau from morrigan ;
revoke select on vaisseau from GROUP meuji ;
revoke select on vaisseau from GROUP general ;
revoke select, insert, delete, update on vaisseau from supermj ;
revoke all on type_classe from morrigan ;
revoke select on type_classe from GROUP meuji ;
revoke select on type_classe from GROUP general ;
revoke select, insert, delete, update on type_classe from supermj ;
revoke all on interdit_classe from morrigan ;
revoke select on interdit_classe from GROUP meuji ;
revoke select on interdit_classe from GROUP general ;
revoke select, insert, delete, update on interdit_classe from supermj ;
revoke all on prerequis_carac from morrigan ;
revoke select on prerequis_carac from GROUP meuji ;
revoke select on prerequis_carac from GROUP general ;
revoke select, insert, delete, update on prerequis_carac from supermj ;
revoke all on taille from morrigan ;
revoke select on taille from GROUP meuji ;
revoke select on taille from GROUP general ;
revoke select, insert, delete, update on taille from supermj ;
revoke all on race from morrigan ;
revoke select on race from GROUP meuji ;
revoke select on race from GROUP general ;
revoke select, insert, delete, update on race from supermj ;
revoke all on acces_connu from morrigan ;
revoke select on acces_connu from GROUP meuji ;
revoke select on acces_connu from GROUP general ;
revoke select, insert, delete, update on acces_connu from supermj ;
revoke all on monde_connu from morrigan ;
revoke select on monde_connu from GROUP meuji ;
revoke select on monde_connu from GROUP general ;
revoke select, insert, delete, update on monde_connu from supermj ;
revoke all on type_munition from morrigan ;
revoke select on type_munition from GROUP meuji ;
revoke select on type_munition from GROUP general ;
revoke select, insert, delete, update on type_munition from supermj ;
revoke all on css from morrigan ;
revoke select on css from GROUP meuji ;
revoke select on css from GROUP general ;
revoke select, insert, delete, update on css from supermj ;
revoke all on itineraire from morrigan ;
revoke select on itineraire from GROUP meuji ;
revoke select on itineraire from GROUP general ;
revoke select, insert, delete, update on itineraire from supermj ;
revoke all on type_filature from morrigan ;
revoke select on type_filature from GROUP meuji ;
revoke select on type_filature from GROUP general ;
revoke select, insert, delete, update on type_filature from supermj ;
revoke all on mime from morrigan ;
revoke select on mime from GROUP meuji ;
revoke select on mime from GROUP general ;
revoke select, insert, delete, update on mime from supermj ;
revoke all on type_mime from morrigan ;
revoke select on type_mime from GROUP meuji ;
revoke select on type_mime from GROUP general ;
revoke select, insert, delete, update on type_mime from supermj ;
revoke all on rumeur from morrigan ;
revoke select on rumeur from GROUP meuji ;
revoke select on rumeur from GROUP general ;
revoke select, insert, delete, update on rumeur from supermj ;
revoke all on rumeur_ecoute from morrigan ;
revoke select on rumeur_ecoute from GROUP meuji ;
revoke select on rumeur_ecoute from GROUP general ;
revoke select, insert, delete, update on rumeur_ecoute from supermj ;
revoke all on langue from morrigan ;
revoke select on langue from GROUP meuji ;
revoke select on langue from GROUP general ;
revoke select, insert, delete, update on langue from supermj ;
revoke all on filature from morrigan ;
revoke select on filature from GROUP meuji ;
revoke select on filature from GROUP general ;
revoke select, insert, delete, update on filature from supermj ;
revoke all on type_trace from morrigan ;
revoke select on type_trace from GROUP meuji ;
revoke select on type_trace from GROUP general ;
revoke select, insert, delete, update on type_trace from supermj ;
revoke all on trace_connue from morrigan ;
revoke select on trace_connue from GROUP meuji ;
revoke select on trace_connue from GROUP general ;
revoke select, insert, delete, update on trace_connue from supermj ;
revoke all on lieu_carto from morrigan ;
revoke select on lieu_carto from GROUP meuji ;
revoke select on lieu_carto from GROUP general ;
revoke select, insert, delete, update on lieu_carto from supermj ;
revoke all on cycle from morrigan ;
revoke select on cycle from GROUP meuji ;
revoke select on cycle from GROUP general ;
revoke select, insert, delete, update on cycle from supermj ;
revoke all on cycle_monde from morrigan ;
revoke select on cycle_monde from GROUP meuji ;
revoke select on cycle_monde from GROUP general ;
revoke select, insert, delete, update on cycle_monde from supermj ;
revoke all on cycle_secteur from morrigan ;
revoke select on cycle_secteur from GROUP meuji ;
revoke select on cycle_secteur from GROUP general ;
revoke select, insert, delete, update on cycle_secteur from supermj ;
revoke all on periode from morrigan ;
revoke select on periode from GROUP meuji ;
revoke select on periode from GROUP general ;
revoke select, insert, delete, update on periode from supermj ;
revoke all on lieu_cycle from morrigan ;
revoke select on lieu_cycle from GROUP meuji ;
revoke select on lieu_cycle from GROUP general ;
revoke select, insert, delete, update on lieu_cycle from supermj ;
revoke all on lieu_cyclique from morrigan ;
revoke select on lieu_cyclique from GROUP meuji ;
revoke select on lieu_cyclique from GROUP general ;
revoke select, insert, delete, update on lieu_cyclique from supermj ;
revoke all on calque from morrigan ;
revoke select on calque from GROUP meuji ;
revoke select on calque from GROUP general ;
revoke select, insert, delete, update on calque from supermj ;
revoke all on type_calque from morrigan ;
revoke select on type_calque from GROUP meuji ;
revoke select on type_calque from GROUP general ;
revoke select, insert, delete, update on type_calque from supermj ;
revoke all on calque_lieu_carto from morrigan ;
revoke select on calque_lieu_carto from GROUP meuji ;
revoke select on calque_lieu_carto from GROUP general ;
revoke select, insert, delete, update on calque_lieu_carto from supermj ;
revoke all on type_vehicule from morrigan ;
revoke select on type_vehicule from GROUP meuji ;
revoke select on type_vehicule from GROUP general ;
revoke select, insert, delete, update on type_vehicule from supermj ;
revoke all on vent_lieu_carto from morrigan ;
revoke select on vent_lieu_carto from GROUP meuji ;
revoke select on vent_lieu_carto from GROUP general ;
revoke select, insert, delete, update on vent_lieu_carto from supermj ;
revoke all on restriction_calque from morrigan ;
revoke select on restriction_calque from GROUP meuji ;
revoke select on restriction_calque from GROUP general ;
revoke select, insert, delete, update on restriction_calque from supermj ;
revoke all on influence_vent from morrigan ;
revoke select on influence_vent from GROUP meuji ;
revoke select on influence_vent from GROUP general ;
revoke select, insert, delete, update on influence_vent from supermj ;
revoke all on vehicule from morrigan ;
revoke select on vehicule from GROUP meuji ;
revoke select on vehicule from GROUP general ;
revoke select, insert, delete, update on vehicule from supermj ;
revoke all on type_vent from morrigan ;
revoke select on type_vent from GROUP meuji ;
revoke select on type_vent from GROUP general ;
revoke select, insert, delete, update on type_vent from supermj ;
revoke all on type_poste from morrigan ;
revoke select on type_poste from GROUP meuji ;
revoke select on type_poste from GROUP general ;
revoke select, insert, delete, update on type_poste from supermj ;
revoke all on poste from morrigan ;
revoke select on poste from GROUP meuji ;
revoke select on poste from GROUP general ;
revoke select, insert, delete, update on poste from supermj ;
revoke all on type_vehicule_poste from morrigan ;
revoke select on type_vehicule_poste from GROUP meuji ;
revoke select on type_vehicule_poste from GROUP general ;
revoke select, insert, delete, update on type_vehicule_poste from supermj ;
revoke all on monture from morrigan ;
revoke select on monture from GROUP meuji ;
revoke select on monture from GROUP general ;
revoke select, insert, delete, update on monture from supermj ;
revoke all on particularite_chemin from morrigan ;
revoke select on particularite_chemin from GROUP meuji ;
revoke select on particularite_chemin from GROUP general ;
revoke select, insert, delete, update on particularite_chemin from supermj ;
revoke all on description_lieu from morrigan ;
revoke select on description_lieu from GROUP meuji ;
revoke select on description_lieu from GROUP general ;
revoke select, insert, delete, update on description_lieu from supermj ;
revoke all on classeniveau_action from morrigan ;
revoke select on classeniveau_action from GROUP meuji ;
revoke select on classeniveau_action from GROUP general ;
revoke select, insert, delete, update on classeniveau_action from supermj ;
revoke all on engagement from morrigan ;
revoke select on engagement from GROUP meuji ;
revoke select on engagement from GROUP general ;
revoke select, insert, delete, update on engagement from supermj ;
revoke all on coffre from morrigan ;
revoke select on coffre from GROUP meuji ;
revoke select on coffre from GROUP general ;
revoke select, insert, delete, update on coffre from supermj ;
revoke all on coffre_element from morrigan ;
revoke select on coffre_element from GROUP meuji ;
revoke select on coffre_element from GROUP general ;
revoke select, insert, delete, update on coffre_element from supermj ;
revoke all on element_visible from morrigan ;
revoke select on element_visible from GROUP meuji ;
revoke select on element_visible from GROUP general ;
revoke select, insert, delete, update on element_visible from supermj ;
revoke all on image from morrigan ;
revoke select on image from GROUP meuji ;
revoke select on image from GROUP general ;
revoke select, insert, delete, update on image from supermj ;
revoke all on boutique from morrigan ;
revoke select on boutique from GROUP meuji ;
revoke select on boutique from GROUP general ;
revoke select, insert, delete, update on boutique from supermj ;
revoke all on automate from morrigan ;
revoke select on automate from GROUP meuji ;
revoke select on automate from GROUP general ;
revoke select, insert, delete, update on automate from supermj ;
revoke all on element_gen_connu from morrigan ;
revoke select on element_gen_connu from GROUP meuji ;
revoke select on element_gen_connu from GROUP general ;
revoke select, insert, delete, update on element_gen_connu from supermj ;
revoke all on automate_carac from morrigan ;
revoke select on automate_carac from GROUP meuji ;
revoke select on automate_carac from GROUP general ;
revoke select, insert, delete, update on automate_carac from supermj ;
revoke all on achat_element_gen from morrigan ;
revoke select on achat_element_gen from GROUP meuji ;
revoke select on achat_element_gen from GROUP general ;
revoke select, insert, delete, update on achat_element_gen from supermj ;
revoke all on type_environnement from morrigan ;
revoke select on type_environnement from GROUP meuji ;
revoke select on type_environnement from GROUP general ;
revoke select, insert, delete, update on type_environnement from supermj ;
revoke all on profil_annonce from morrigan ;
revoke select on profil_annonce from GROUP meuji ;
revoke select on profil_annonce from GROUP general ;
revoke select, insert, delete, update on profil_annonce from supermj ;
revoke all on news from morrigan ;
revoke select on news from GROUP meuji ;
revoke select on news from GROUP general ;
revoke select, insert, delete, update on news from supermj ;
revoke all on categorie_profil from morrigan ;
revoke select on categorie_profil from GROUP meuji ;
revoke select on categorie_profil from GROUP general ;
revoke select, insert, delete, update on categorie_profil from supermj ;
revoke all on candidature from morrigan ;
revoke select on candidature from GROUP meuji ;
revoke select on candidature from GROUP general ;
revoke select, insert, delete, update on candidature from supermj ;
revoke all on question from morrigan ;
revoke select on question from GROUP meuji ;
revoke select on question from GROUP general ;
revoke select, insert, delete, update on question from supermj ;
revoke all on reponse from morrigan ;
revoke select on reponse from GROUP meuji ;
revoke select on reponse from GROUP general ;
revoke select, insert, delete, update on reponse from supermj ;
revoke all on profil_classe from morrigan ;
revoke select on profil_classe from GROUP meuji ;
revoke select on profil_classe from GROUP general ;
revoke select, insert, delete, update on profil_classe from supermj ;
revoke all on profil_race from morrigan ;
revoke select on profil_race from GROUP meuji ;
revoke select on profil_race from GROUP general ;
revoke select, insert, delete, update on profil_race from supermj ;
revoke all on profil_sexe from morrigan ;
revoke select on profil_sexe from GROUP meuji ;
revoke select on profil_sexe from GROUP general ;
revoke select, insert, delete, update on profil_sexe from supermj ;


-- Special statements for postgres:pre databases
  DROP SEQUENCE personnage_prs_id_seq CASCADE;
CREATE SEQUENCE personnage_prs_id_seq START 100;
  DROP SEQUENCE itineraire_itn_id_seq CASCADE;
CREATE SEQUENCE itineraire_itn_id_seq START 100;
  DROP SEQUENCE type_trace_ttr_id_seq CASCADE;
CREATE SEQUENCE type_trace_ttr_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE forme_frm_id_seq CASCADE;
CREATE SEQUENCE forme_frm_id_seq START 100;
  DROP SEQUENCE race_rac_id_seq CASCADE;
CREATE SEQUENCE race_rac_id_seq START 100;
  DROP SEQUENCE sexe_sex_id_seq CASCADE;
CREATE SEQUENCE sexe_sex_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE classe_cls_id_seq CASCADE;
CREATE SEQUENCE classe_cls_id_seq START 100;
  DROP SEQUENCE classe_niveau_cln_id_seq CASCADE;
CREATE SEQUENCE classe_niveau_cln_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE chemin_chm_id_seq CASCADE;
CREATE SEQUENCE chemin_chm_id_seq START 100;
  DROP SEQUENCE saison_ssn_id_seq CASCADE;
CREATE SEQUENCE saison_ssn_id_seq START 100;
  DROP SEQUENCE lieu_lie_id_seq CASCADE;
CREATE SEQUENCE lieu_lie_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE secteur_sct_id_seq CASCADE;
CREATE SEQUENCE secteur_sct_id_seq START 100;
  DROP SEQUENCE type_environnement_tev_id_seq CASCADE;
CREATE SEQUENCE type_environnement_tev_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE monde_mnd_id_seq CASCADE;
CREATE SEQUENCE monde_mnd_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE element_elm_id_seq CASCADE;
CREATE SEQUENCE element_elm_id_seq START 100;
  DROP SEQUENCE technologie_tcn_id_seq CASCADE;
CREATE SEQUENCE technologie_tcn_id_seq START 100;
  DROP SEQUENCE matiere_mtr_id_seq CASCADE;
CREATE SEQUENCE matiere_mtr_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE caracteristique_crc_id_seq CASCADE;
CREATE SEQUENCE caracteristique_crc_id_seq START 100;
  DROP SEQUENCE categorie_caracteristique_ccr_id_seq CASCADE;
CREATE SEQUENCE categorie_caracteristique_ccr_id_seq START 100;
  DROP SEQUENCE langue_lng_id_seq CASCADE;
CREATE SEQUENCE langue_lng_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE maladie_mld_id_seq CASCADE;
CREATE SEQUENCE maladie_mld_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE monnaie_mnn_id_seq CASCADE;
CREATE SEQUENCE monnaie_mnn_id_seq START 100;
  DROP SEQUENCE compagnie_cmp_id_seq CASCADE;
CREATE SEQUENCE compagnie_cmp_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE joueur_jou_id_seq CASCADE;
CREATE SEQUENCE joueur_jou_id_seq START 100;
  DROP SEQUENCE mj_mji_id_seq CASCADE;
CREATE SEQUENCE mj_mji_id_seq START 100;
  DROP SEQUENCE css_css_id_seq CASCADE;
CREATE SEQUENCE css_css_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE action_act_id_seq CASCADE;
CREATE SEQUENCE action_act_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE element_gen_egn_id_seq CASCADE;
CREATE SEQUENCE element_gen_egn_id_seq START 100;
  DROP SEQUENCE arme_gen_agn_id_seq CASCADE;
CREATE SEQUENCE arme_gen_agn_id_seq START 100;
  DROP SEQUENCE armure_gen_ugn_id_seq CASCADE;
CREATE SEQUENCE armure_gen_ugn_id_seq START 100;
  DROP SEQUENCE vaisseau_gen_vgn_id_seq CASCADE;
CREATE SEQUENCE vaisseau_gen_vgn_id_seq START 100;
  DROP SEQUENCE automate_atm_id_seq CASCADE;
CREATE SEQUENCE automate_atm_id_seq START 100;
  DROP SEQUENCE boutique_btq_id_seq CASCADE;
CREATE SEQUENCE boutique_btq_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_munition_tmt_id_seq CASCADE;
CREATE SEQUENCE type_munition_tmt_id_seq START 100;
  DROP SEQUENCE munition_mnt_id_seq CASCADE;
CREATE SEQUENCE munition_mnt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_element_tel_id_seq CASCADE;
CREATE SEQUENCE type_element_tel_id_seq START 100;
  DROP SEQUENCE type_vaisseau_tvs_id_seq CASCADE;
CREATE SEQUENCE type_vaisseau_tvs_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE vaisseau_gen_vsg_id_seq CASCADE;
CREATE SEQUENCE vaisseau_gen_vsg_id_seq START 100;
  DROP SEQUENCE type_vaisseau_tvs_id_seq CASCADE;
CREATE SEQUENCE type_vaisseau_tvs_id_seq START 100;
  DROP SEQUENCE type_poste_tpt_id_seq CASCADE;
CREATE SEQUENCE type_poste_tpt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE zone_zon_id_seq CASCADE;
CREATE SEQUENCE zone_zon_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_degat_tdg_id_seq CASCADE;
CREATE SEQUENCE type_degat_tdg_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE filature_flt_id_seq CASCADE;
CREATE SEQUENCE filature_flt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE mime_mim_id_seq CASCADE;
CREATE SEQUENCE mime_mim_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE rumeur_rmr_id_seq CASCADE;
CREATE SEQUENCE rumeur_rmr_id_seq START 100;
  DROP SEQUENCE rumeur_ecoute_rmc_id_seq CASCADE;
CREATE SEQUENCE rumeur_ecoute_rmc_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_lieu_tpl_id_seq CASCADE;
CREATE SEQUENCE type_lieu_tpl_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE debit_element_dbt_id_seq CASCADE;
CREATE SEQUENCE debit_element_dbt_id_seq START 100;
  DROP SEQUENCE vente_element_vnt_id_seq CASCADE;
CREATE SEQUENCE vente_element_vnt_id_seq START 100;
  DROP SEQUENCE depot_element_dpt_id_seq CASCADE;
CREATE SEQUENCE depot_element_dpt_id_seq START 100;
  DROP SEQUENCE banque_bnq_id_seq CASCADE;
CREATE SEQUENCE banque_bnq_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE arme_arm_id_seq CASCADE;
CREATE SEQUENCE arme_arm_id_seq START 100;
  DROP SEQUENCE armure_aru_id_seq CASCADE;
CREATE SEQUENCE armure_aru_id_seq START 100;
  DROP SEQUENCE recharge_rch_id_seq CASCADE;
CREATE SEQUENCE recharge_rch_id_seq START 100;
  DROP SEQUENCE vaisseau_vss_id_seq CASCADE;
CREATE SEQUENCE vaisseau_vss_id_seq START 100;
  DROP SEQUENCE coffre_cff_id_seq CASCADE;
CREATE SEQUENCE coffre_cff_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE calque_clq_id_seq CASCADE;
CREATE SEQUENCE calque_clq_id_seq START 100;
  DROP SEQUENCE type_calque_tcq_id_seq CASCADE;
CREATE SEQUENCE type_calque_tcq_id_seq START 100;
  DROP SEQUENCE type_vent_tvt_id_seq CASCADE;
CREATE SEQUENCE type_vent_tvt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE lieu_lie_id_seq CASCADE;
CREATE SEQUENCE lieu_lie_id_seq START 1000;
  DROP SEQUENCE image_img_id_seq CASCADE;
CREATE SEQUENCE image_img_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE vehicule_vhc_id_seq CASCADE;
CREATE SEQUENCE vehicule_vhc_id_seq START 100;
  DROP SEQUENCE type_vehicule_tvh_id_seq CASCADE;
CREATE SEQUENCE type_vehicule_tvh_id_seq START 1000;
  DROP SEQUENCE type_poste_tps_id_seq CASCADE;
CREATE SEQUENCE type_poste_tps_id_seq START 100;

-- Special statements for postgres:pre databases
DROP INDEX idx_lic_localisation;


-- Generated SQL View Drop Statements
-- --------------------------------------------------------------------



-- Generated SQL Schema Drop statements
-- --------------------------------------------------------------------

drop table personnage cascade ;
drop table joueur cascade ;
drop table mj cascade ;
drop table perso_classe cascade ;
drop table forme cascade ;
drop table classe_niveau cascade ;
drop table classe cascade ;
drop table perso_maladie cascade ;
drop table maladie cascade ;
drop table caracteristique cascade ;
drop table perso_carac cascade ;
drop table parametre cascade ;
drop table categorie cascade ;
drop table perso_classe_int cascade ;
drop table prerequis_classe cascade ;
drop table monde cascade ;
drop table lieu cascade ;
drop table secteur cascade ;
drop table chemin cascade ;
drop table saison cascade ;
drop table element cascade ;
drop table monnaie cascade ;
drop table perso_monnaie cascade ;
drop table compagnie cascade ;
drop table place_compagnie cascade ;
drop table sexe cascade ;
drop table type_personnage cascade ;
drop table type_maladie cascade ;
drop table type_caracteristique cascade ;
drop table nature_caracteristique cascade ;
drop table type_secteur cascade ;
drop table type_monde cascade ;
drop table type_chemin cascade ;
drop table action cascade ;
drop table lieu_action cascade ;
drop table typelieu_action cascade ;
drop table type_lieu cascade ;
drop table debit_element cascade ;
drop table vente_element cascade ;
drop table depot_element cascade ;
drop table banque cascade ;
drop table element_gen cascade ;
drop table arme cascade ;
drop table arme_gen cascade ;
drop table matiere cascade ;
drop table technologie cascade ;
drop table armure_gen cascade ;
drop table recharge_gen cascade ;
drop table vaisseau_gen cascade ;
drop table type_vaisseau cascade ;
drop table munition cascade ;
drop table type_degat cascade ;
drop table armure_gen_degat cascade ;
drop table armure_gen_zone cascade ;
drop table zone_armure cascade ;
drop table vaisseau_gen_poste cascade ;
drop table type_poste_tmp cascade ;
drop table type_element cascade ;
drop table armure cascade ;
drop table recharge cascade ;
drop table vaisseau cascade ;
drop table type_classe cascade ;
drop table interdit_classe cascade ;
drop table prerequis_carac cascade ;
drop table taille cascade ;
drop table race cascade ;
drop table acces_connu cascade ;
drop table monde_connu cascade ;
drop table type_munition cascade ;
drop table css cascade ;
drop table itineraire cascade ;
drop table type_filature cascade ;
drop table mime cascade ;
drop table type_mime cascade ;
drop table rumeur cascade ;
drop table rumeur_ecoute cascade ;
drop table langue cascade ;
drop table filature cascade ;
drop table type_trace cascade ;
drop table trace_connue cascade ;
drop table lieu_carto cascade ;
drop table cycle cascade ;
drop table cycle_monde cascade ;
drop table cycle_secteur cascade ;
drop table periode cascade ;
drop table lieu_cycle cascade ;
drop table lieu_cyclique cascade ;
drop table calque cascade ;
drop table type_calque cascade ;
drop table calque_lieu_carto cascade ;
drop table type_vehicule cascade ;
drop table vent_lieu_carto cascade ;
drop table restriction_calque cascade ;
drop table influence_vent cascade ;
drop table vehicule cascade ;
drop table type_vent cascade ;
drop table type_poste cascade ;
drop table poste cascade ;
drop table type_vehicule_poste cascade ;
drop table monture cascade ;
drop table particularite_chemin cascade ;
drop table description_lieu cascade ;
drop table classeniveau_action cascade ;
drop table engagement cascade ;
drop table coffre cascade ;
drop table coffre_element cascade ;
drop table element_visible cascade ;
drop table image cascade ;
drop table boutique cascade ;
drop table automate cascade ;
drop table element_gen_connu cascade ;
drop table automate_carac cascade ;
drop table achat_element_gen cascade ;
drop table type_environnement cascade ;
drop table profil_annonce cascade ;
drop table news cascade ;
drop table categorie_profil cascade ;
drop table candidature cascade ;
drop table question cascade ;
drop table reponse cascade ;
drop table profil_classe cascade ;
drop table profil_race cascade ;
drop table profil_sexe cascade ;


-- Generated SQL Schema
-- --------------------------------------------------------------------


-- personnage
create table personnage (
  prs_id                    integer default nextval('personnage_prs_id_seq') not null,	-- Identifiant du personnage
  prs_jou_id                integer,	-- Identifiant du joueur jouant le personnage
  prs_principal             boolean NOT NULL,	-- Personnage par defaut du joueur (oui/non)
  prs_possesseur_prs_id     integer,	-- Identifiant du personnage possedant celui-ci
  prs_lie_id                integer,	-- Identifiant du lieu
  prs_nom_originel          varchar(50) NOT NULL,	-- Nom de depart
  prs_nom_actuel            varchar(50),	-- Nom actuel
  prs_background            varchar(5000),	-- Passe du personnage
  prs_biographie            varchar(5000),	-- Histoire du personnage
  prs_inscription           timestamp,	-- Date d'inscription
  prs_derniere_connexion    timestamp,	-- Date de derniere connexion
  prs_pa                    smallint NOT NULL,	-- Nombre de Points d'Action
  prs_pa_max                smallint NOT NULL,	-- Nombre de Points d'Action maximum
  prs_pn                    smallint NOT NULL,	-- Nombre de Points de Nourriture
  prs_pn_max                smallint NOT NULL,	-- Nombre de Points de Nourriture maximum
  prs_pp                    smallint NOT NULL,	-- Nombre de Points de Paralysie
  prs_pp_max                smallint NOT NULL,	-- Nombre de Points de Paralysie maximum
  prs_pv                    smallint NOT NULL,	-- Nombre de Points de Vie
  prs_pv_max                smallint NOT NULL,	-- Nombre de Points de Vie maximum
  prs_pc                    smallint NOT NULL,	-- Points de combat
  prs_pc_max                smallint NOT NULL,	-- Maximum de point de combat
  prs_tpr_id                smallint NOT NULL,	-- Type de personnage: pj, pnj, familier, etc.
  prs_calc_facteur_nb_trace float default 1 NOT NULL,	-- Facteur influant sur le nombre de trace calculé en fonction de ses attributs, d'éventuelles compétences, de son équipement, etc.
  prs_date_arrivee          timestamp not null,	-- Date d'arrivée dans un lieu
  prs_delta_rose            integer default 0 NOT NULL,	-- Delta de décalage par rapport à la rose des vents
  constraint pk_Personnage primary key (prs_id)
) ;

-- joueur
create table joueur (
  jou_id                    integer default nextval('joueur_jou_id_seq') not null,	-- Identifiant
  jou_pseudo                varchar(32) NOT NULL,	-- Pseudonyme
  jou_mdp                   varchar(32) NOT NULL,	-- Mot de passe (md5)
  jou_email                 varchar(50) NOT NULL,	-- Adresse mail
  jou_css_id                integer,	-- Style CSS utilisé par le joueur
  jou_fuseau                smallint,	-- Decalage horaire par rapport a l'heure GMT
  jou_session_id            varchar(64),	-- Identifiant de session
  jou_scorequizz            integer,	-- Score au quizz
  jou_ipinitiale            inet,	-- IP d'inscription
  jou_ipcourante            inet,	-- Dernière IP connue
  jou_commentaire           text,	-- Éventuel commentaire de l'utilisateur
  constraint pk_Joueur primary key (jou_id)
) ;

-- mj
create table mj (
  mji_id                    integer default nextval('mj_mji_id_seq') not null,	-- Identifiant
  mji_jou_id                integer NOT NULL,	-- Identifiant du joueur
  mji_nom                   varchar(50) NOT NULL,	-- Nom
  mji_derniere_connexion    timestamp with time zone,	-- Date de derniere connexion
  mji_droits_news           boolean default False,	-- Rédacteur de news (O/N)
  mji_droits_all_news       boolean default False,	-- Administrateur des news
  mji_droits_inscription    boolean default False,	-- Droit de proposition de profil
  mji_droits_recrutement    boolean default False,	-- Administrateur des incriptions
  mji_droits_edit_regles    boolean default False,	-- Rédacteur wiki règles
  mji_droits_edit_monde     boolean default False,	-- Rédacteur wiki background
  mji_droits_edit_admin     boolean default False,	-- Administrateur technique
  constraint pk_Mj primary key (mji_id)
) ;

-- perso_classe
create table perso_classe (
  pce_id                    serial not null,	-- Identifiant abstrait
  pce_prs_id                integer NOT NULL,	-- Personnage
  pce_cln_id                integer NOT NULL,	-- Niveau de classe
  pce_titre                 varchar(64),	-- Titre accordé au personnage (si différent de celui de classe)
  constraint pk_Perso_classe primary key (pce_id)
) ;

-- forme
create table forme (
  frm_id                    integer default nextval('forme_frm_id_seq') not null,	-- Identifiant
  frm_prs_id                integer NOT NULL,	-- Identifiant du personnage pouvant prendre cette forme
  frm_rac_id                smallint NOT NULL,	-- Identifiant de la race
  frm_sex_id                integer NOT NULL,	-- Sexe
  frm_indice_taille         integer NOT NULL,	-- Indice de taille de la forme
  frm_actuelle              boolean,	-- Definit la forme actuelle du personnage (oui/non)
  frm_description           character varying(2048),	-- Description physique
  frm_desc_actuelle         varchar(1024),	-- Description de l'etat actuel
  frm_url_image             varchar(128),	-- URL de l'image illustrant le personnage sous cette forme
  frm_ttr_id                integer default 1 NOT NULL,	-- Type de trace de la forme
  frm_profond_trace         integer default 4 NOT NULL,	-- Profondeur de la trace laissée
  frm_taille_trace          integer default 4 NOT NULL,
  constraint pk_Forme primary key (frm_id)
) ;

-- classe_niveau
create table classe_niveau (
  cln_id                    integer default nextval('classe_niveau_cln_id_seq') not null,	-- Identifiant
  cln_cls_id                integer NOT NULL,	-- Classe
  cln_niveau                smallint NOT NULL,	-- Niveau dans la classe
  cln_mld_id                smallint,	-- Maladie de classe
  cln_titre                 varchar(50) NOT NULL,	-- Titre donné au détenteur de ce niveau de classe
  cln_titre_visible         boolean,	-- Titre visible par les autres (oui/non)
  cln_desc_mj               varchar(128),	-- Description pour MJ
  constraint pk_Classe_niveau primary key (cln_id)
) ;

-- classe
create table classe (
  cls_id                    integer default nextval('classe_cls_id_seq') not null,	-- Identifiant
  cls_nom                   varchar(50) NOT NULL,	-- Nom
  cls_desc_mj               varchar(128),	-- Description à l'adresse des MJ
  cls_tcl_id                integer NOT NULL,	-- Type de classe: classe, aptitude
  cls_insc_ouverte          boolean default TRUE NOT NULL,	-- Inscription ouverte pour cette classe
  constraint pk_Classe primary key (cls_id)
) ;

-- perso_maladie
create table perso_maladie (
  pml_id                    serial not null,	-- Identifiant abstrait
  pml_prs_id                integer NOT NULL,	-- Personnage malade
  pml_mld_id                integer NOT NULL,	-- Maladie
  pml_debut                 date NOT NULL,	-- Date d'apparition de la maladie
  pml_fin                   date,	-- Eventuelle date de fin de la maladie
  constraint pk_Perso_maladie primary key (pml_id)
) ;

-- maladie
create table maladie (
  mld_id                    integer default nextval('maladie_mld_id_seq') not null,	-- Identifiant
  mld_nom                   varchar(32) NOT NULL,	-- Nom
  mld_description           varchar(1500),	-- Description generale
  mld_tml_id                integer NOT NULL,	-- Type de maladie
  mld_duree                 smallint,	-- Duree en tours
  mld_periode               integer,	-- Periode en tours entre 2 effets
  mld_description_physique  varchar(200),	-- Description perçue par un tiers sur le malade (vide si rien de visible)
  mld_formule               varchar(500),	-- Formule à appliquer pour trouver les caractéristiques actuelles
  constraint pk_Maladie primary key (mld_id)
) ;

-- caracteristique
create table caracteristique (
  crc_id                    integer default nextval('caracteristique_crc_id_seq') not null,	-- Identifiant
  crc_nom                   varchar(50) NOT NULL,	-- Nom
  crc_description           varchar(1500),	-- Description de son utilisation
  crc_tcr_id                integer,	-- Type de caracteristique
  crc_ncr_id                integer,	-- Nature
  crc_delta_min_max         integer default 0 NOT NULL,	-- Delta minimum et maximum
  crc_visible               boolean default TRUE NOT NULL,	-- À vrai si le score dans cette caractéristiques est visible par le joueur
  constraint pk_Caracteristique primary key (crc_id)
) ;

-- perso_carac
create table perso_carac (
  pcr_id                    serial not null,	-- Identifiant abstrait
  pcr_prs_id                integer NOT NULL,	-- Personnage
  pcr_crc_id                integer NOT NULL,	-- Caracteristique
  pcr_jauge_entrainement    integer default 0 NOT NULL,	-- Jauge d'entrainement (entre 0 et 100)
  pcr_score_xp              integer default 0 NOT NULL,	-- Nombre de points d'experience actuel dans la caracteristique
  pcr_score_xp_max          integer default 0 NOT NULL,	-- Nombre de points d'experience maximum atteint dans la caracteristique
  pcr_score_calcule         smallint default 0 NOT NULL,	-- Score apres calcul a partir de pcr_score_xp apres application des differentes maladies, classes, etc.
  pcr_score_max             integer default 0 NOT NULL,	-- Score absolu pouvant etre atteint en fonction de l'age, la race, le sexe du personnage
  constraint pk_Perso_carac primary key (pcr_id)
) ;

-- parametre
create table parametre (
  prm_id                    smallint not null,	-- Identifiant
  prm_ctg_id                smallint NOT NULL,	-- Ensemble d'appartenance
  prm_libelle               varchar(50) NOT NULL,	-- Libelle explicitant a quoi il sert
  prm_valeur                varchar(30) NOT NULL,	-- Valeur du parametre (c'est un varchar mais des entiers ou des dates peuvent y etre entres)
  prm_description           varchar(500) NOT NULL,	-- Description de ce qui peut etre mis dans la valeur
  constraint pk_Parametre primary key (prm_id)
) ;

-- categorie
create table categorie (
  ctg_id                    smallint not null,	-- Identifiant
  ctg_libelle               varchar(50) NOT NULL,	-- Libelle de l'ensemble de parametres
  ctg_systeme               boolean NOT NULL,	-- Regroupement de parametres systemes (oui/non : modifiable ou non)
  constraint pk_Categorie primary key (ctg_id)
) ;

-- perso_classe_int
create table perso_classe_int (
  pci_id                    serial not null,	-- Identifiant abstrait
  pci_prs_id                integer NOT NULL,	-- Personnage
  pci_cls_id                integer NOT NULL,	-- Classe autorisée/interdite
  pci_type_interdit         smallint NOT NULL,	-- Interdiction de la classe pour le personnage (oui/non)
  constraint pk_Perso_classe_int primary key (pci_id)
) ;

-- prerequis_classe
create table prerequis_classe (
  pqc_id                    serial not null,	-- Identifiant abstrait
  pqc_cln_id                integer NOT NULL,	-- Niveau de classe a atteindre
  pqc_cls_id                integer NOT NULL,	-- Classe necessaire
  pqc_niveau_min            integer,	-- Niveau minimum a avoir dans la classe necessaire 
  constraint pk_Prerequis_classe primary key (pqc_id)
) ;

-- monde
create table monde (
  mnd_id                    integer default nextval('monde_mnd_id_seq') not null,	-- Identifiant
  mnd_nom                   varchar(64) NOT NULL,	-- Nom
  mnd_tmn_id                integer NOT NULL,	-- Type de monde
  mnd_description           varchar(1500),	-- Description générale du monde
  mnd_pere_mnd_id           integer,	-- Monde contenant celui-ci
  mnd_niveau                integer,	-- Indice de niveau
  mnd_mld_id                integer,	-- Maladie régnant sur ce monde
  mnd_image                 varchar(64),	-- Image du monde (fond de la carte)
  constraint pk_Monde primary key (mnd_id)
) ;

-- lieu
create table lieu (
  lie_id                    integer default nextval('lieu_lie_id_seq') not null,	-- Identifiant
  lie_tli_id                integer NOT NULL,	-- Type de lieu
  lie_nom                   varchar(50),	-- Nom
  lie_sct_id                integer,	-- Secteur d'appartenance
  lie_indice_taille         integer,	-- Taille du lieu
  lie_indice_population     integer default 0 NOT NULL,	-- Indice de densité de population
  lie_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  lie_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de trace
  lie_carto                 boolean default FALSE,	-- À vrai si il s'agit d'un lieu carto
  lie_desc                  varchar(1500),	-- Précision éventuelle sur la description du lieu
  lie_img_id                integer,	-- Éventuelle image associée
  constraint pk_Lieu primary key (lie_id)
) ;

-- secteur
create table secteur (
  sct_id                    integer default nextval('secteur_sct_id_seq') not null,	-- Identifiant
  sct_nom                   varchar(50) NOT NULL,	-- Nom
  sct_description           varchar(1500),	-- Description destinée aux personnages se trouvant dans le secteur
  sct_tsc_id                integer NOT NULL,	-- Type de secteur
  sct_charge_max            integer,	-- Charge maximale supportée par le secteur (null signifie infini)
  sct_indice_population     integer default 0 NOT NULL,	-- Indice de densité de population
  sct_vhc_id                integer,	-- Identifiant de l'éventuel véhicule associé à un secteur
  sct_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout journalier en pn
  sct_cout_pv               varchar(64) default '0' NOT NULL,	-- Cout journalier en PV
  sct_difficulte_rumeur     integer default 0 NOT NULL,	-- Difficulté de lancement d'une rumeur
  sct_tev_id                integer NOT NULL,
  constraint pk_Secteur primary key (sct_id)
) ;

-- chemin
-- Lien entre 2 endroits (lieu <-> lieu ou lieu <->monde)
create table chemin (
  chm_id                    integer default nextval('chemin_chm_id_seq') not null,	-- Identifiant
  chm_egn_id                integer,	-- Element generique servant de clé
  chm_tch_id                smallint NOT NULL,	-- Type de chemin (d'acces)
  chm_pch_id                integer,	-- Éventuelle particularité associée à un chemin (champs textuel du style « par la fenêtre »,  « par la porte »). Permet notamment de différencier différents chemins venant du même endroit et allant au même endroit.
  chm_depart_lie_id         integer NOT NULL,	-- Lieu de départ
  chm_arrivee_lie_id        integer NOT NULL,	-- Lieu d'arrivée
  chm_desc_aller            varchar(1500),	-- Description du trajet dans le sens aller
  chm_desc_retour           varchar(1500),	-- Description du trajet dans le sens retour
  chm_aller                 boolean,	-- Trajet possible dans le sens aller (oui/non)
  chm_retour                boolean,	-- Trajet possible dans le sens retour (oui/non)
  chm_ouvert                boolean,	-- Trajet actuellement ouvert (oui/non) (prévaut sur chm_aller et chm_retour)
  chm_discretion            smallint default 0 NOT NULL,	-- Discretion
  chm_difficulte            integer default 0 NOT NULL,	-- Seuil de difficulté
  chm_cout_pa               varchar(64) default '0' NOT NULL,	-- Cout du trajet en Points d'Action
  chm_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout en point de nourriture
  chm_cout_echec_pv         varchar(64) default '0' NOT NULL,	-- Cout en PV en cas d'échec
  chm_indice_taille         integer,	-- Indice de taille du passage accordé par le chemin
  chm_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  chm_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de traces
  chm_echec_bloquant        boolean default FALSE NOT NULL,	-- À vrai si l'échec empeche d'emprunter le chemin
  chm_crc_id                integer NOT NULL,	-- Caractéristique associée
  chm_crc_2_id              integer,	-- Éventuelle seconde caractéristique intervenant dans la formule
  constraint pk_Chemin primary key (chm_id)
) ;

-- saison
create table saison (
  ssn_id                    integer default nextval('saison_ssn_id_seq') not null,	-- Identifiant
  ssn_nom                   varchar(32) NOT NULL,	-- Nom
  ssn_ordre                 smallint,	-- Ordre pendant un cycle
  ssn_default               boolean,	-- Saison par défaut
  constraint pk_Saison primary key (ssn_id)
) ;

-- element
create table element (
  elm_id                    integer default nextval('element_elm_id_seq') not null,	-- Identifiant
  elm_egn_id                integer NOT NULL,	-- Identifiant de l'element generique associe
  elm_nom                   varchar(64),	-- Nom
  elm_desc                  varchar(500),	-- Description de l'element
  elm_prix                  integer,	-- Prix de l'element en monnaie arbitraire
  elm_equipable             boolean,	-- Definit si l'element peut etre equipe ou non
  elm_equipe                boolean,	-- Definit si l'element est equipe ou non
  elm_enraye                boolean,	-- Definit si l'element est enraye ou non
  elm_naturel               boolean,	-- Definit si l'element est naturel chez son possesseur
  elm_prs_id                integer,	-- Identifiant de son possesseur
  elm_lie_id                integer,	-- Lieu ou a ete jete l'element
  constraint pk_Element primary key (elm_id)
) ;

-- monnaie
create table monnaie (
  mnn_id                    integer default nextval('monnaie_mnn_id_seq') not null,	-- Identifiant de la monnaie
  mnn_nom                   varchar(50) NOT NULL,	-- Nom de la monnaie
  mnn_indice                smallint NOT NULL,	-- Indice arbitraire de la monnaie (voire cote)
  constraint pk_Monnaie primary key (mnn_id)
) ;

-- perso_monnaie
-- Monnaie que porte le personnage
create table perso_monnaie (
  pmn_id                    serial not null,	-- Identifiant abstrait
  pmn_prs_id                integer NOT NULL,	-- Identifiant du personnage
  pmn_mnn_id                integer NOT NULL,	-- Identifiant de la monnaie
  pmn_montant               smallint,	-- Qte de monnaie du personnage
  constraint pk_Perso_monnaie primary key (pmn_id)
) ;

-- compagnie
-- Compagnie de personnages
create table compagnie (
  cmp_id                    integer default nextval('compagnie_cmp_id_seq') not null,	-- Identifiant de la compagnie
  cmp_nom                   varchar(100) NOT NULL,	-- Nom de la compagnie
  constraint pk_Compagnie primary key (cmp_id)
) ;

-- place_compagnie
-- Place des personnages dans la compagnie
create table place_compagnie (
  plc_id                    serial not null,
  plc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  plc_cmp_id                integer NOT NULL,	-- Identifiant de la compagnie
  plc_place                 smallint,	-- Place du personnage ds la compagnie
  constraint pk_Place_compagnie primary key (plc_id)
) ;

-- sexe
create table sexe (
  sex_id                    integer default nextval('sexe_sex_id_seq') not null,	-- Identifiant
  sex_nom                   varchar(50) NOT NULL,	-- Nom du sexe (masculin, féminin, asexué, ...)
  sex_insc_ouverte          boolean default TRUE NOT NULL,	-- Sexe disponible pour l'inscription
  constraint pk_Sexe primary key (sex_id)
) ;

-- type_personnage
create table type_personnage (
  tpr_id                    integer not null,	-- Identifiant
  tpr_nom                   varchar(50) NOT NULL,	-- Nom du type (PJ, PNJ, familier, Gob, ...)
  constraint pk_Type_personnage primary key (tpr_id)
) ;

-- type_maladie
create table type_maladie (
  tml_id                    integer not null,	-- Identifiant
  tml_nom                   varchar(50) NOT NULL,	-- Nom du type (classe, race, instantanée, définitive, temporaire)
  constraint pk_Type_maladie primary key (tml_id)
) ;

-- type_caracteristique
create table type_caracteristique (
  tcr_id                    integer not null,	-- Identifiant
  tcr_nom                   varchar(50) NOT NULL,	-- Nom du type (attribut, compétence)
  constraint pk_Type_caracteristique primary key (tcr_id)
) ;

-- nature_caracteristique
create table nature_caracteristique (
  ncr_id                    integer not null,	-- Identifiant
  ncr_nom                   varchar(50) NOT NULL,	-- Nom de la nature (intellectuelle, physique, spirituelle, ...)
  constraint pk_Nature_caracteristique primary key (ncr_id)
) ;

-- type_secteur
create table type_secteur (
  tsc_id                    integer not null,	-- Identifiant
  tsc_nom                   varchar(50) NOT NULL,	-- Nom du type: immobile, mobile (monde), mobile (galaxie), mobile (véhicule)
  constraint pk_Type_secteur primary key (tsc_id)
) ;

-- type_monde
create table type_monde (
  tmn_id                    integer not null,	-- Identifiant
  tmn_nom                   varchar(50) NOT NULL,	-- Nom du type: galaxie (sans appui au sol), planète (avec appui)
  constraint pk_Type_monde primary key (tmn_id)
) ;

-- type_chemin
create table type_chemin (
  tch_id                    integer not null,	-- Identifiant
  tch_nom                   varchar(50) NOT NULL,	-- Nom du type: classique, accès trappe, accès sas, accès pont
  constraint pk_Type_chemin primary key (tch_id)
) ;

-- action
create table action (
  act_id                    integer default nextval('action_act_id_seq') not null,	-- Identifiant
  act_nom                   varchar(64) NOT NULL,	-- Nom
  act_nom_form              varchar(20) NOT NULL,	-- Nom de l'action dans le formulaire
  act_desc                  varchar(100),	-- Description de l'action
  act_form_attaque          varchar(128) NOT NULL,	-- Formule a resoudre pour l'attaque
  act_form_defense          varchar(128) NOT NULL,	-- Formule a resoudre pour la defense
  act_form_cout_pa          varchar(64),	-- Formule determinant le cout en PA
  act_form_cout_pn          varchar(64),	-- Formule determinant le cout en PN
  act_form_cout_pp          varchar(64),	-- Formule determinant le cout en PP
  act_form_cout_pv          varchar(64),	-- Formule determinant le cout en PV
  act_form_cout_pc          varchar(64),	-- Cout en point de combat de l'action
  act_form_gain_min         varchar(128) NOT NULL,	-- Calcul du gain minimum de Points d''eXperience
  act_form_gain_max         varchar(128) NOT NULL,	-- Calcul du gain maximum de Points d''eXperience
  act_courante              boolean NOT NULL,	-- Action proposee en tout lieu/pour toute classe ? (oui/non)
  act_aide                  varchar(1000),
  act_combat                boolean default FALSE,	-- À vrai si disponible en situation de combat
  constraint pk_Action primary key (act_id)
) ;

-- lieu_action
create table lieu_action (
  lac_id                    serial not null,
  lac_act_id                integer NOT NULL,	-- Identifiant de l'action spécialement considérée en ce lieu
  lac_lie_id                integer NOT NULL,	-- Identifiant du lieu ayant une action particuliere
  lac_possible              boolean,	-- Action possible physiquement en ce lieu ? (oui/non) (rempli si différent du standard)
  lac_autorisee             boolean,	-- Action autorisée (moralement) en ce lieu ? (oui/non) (rempli si différent du standard)
  lac_difficulte            integer,	-- Difficulte de l'action en ce lieu (rempli si différent du standard)
  lac_cout_pa               integer,	-- Coût en PA de l'action en ce lieu (rempli si différent du standard)
  lac_cout_pn               integer,	-- Cout en PN de l'action
  lac_cout_pp               integer,	-- Cout en PP de l'action
  lac_cout_pv               integer,	-- Cout en PV de l'action
  constraint pk_Lieu_action primary key (lac_id)
) ;

-- typelieu_action
create table typelieu_action (
  tla_id                    serial not null,
  tla_act_id                integer NOT NULL,	-- Identifiant de l'action spécialement considérée en ce type de lieu
  tla_tli_id                integer NOT NULL,	-- Identifiant du type de lieu ayant une action particuliere
  tla_possible              boolean,	-- Action possible physiquement en ce type de lieu ? (oui/non) (rempli si différent du standard)
  tla_autorisee             boolean,	-- Action autorisée (moralement) en ce type de lieu ? (oui/non) (rempli si différent du standard)
  tla_difficulte            integer,	-- Difficulte de l'action en ce type de lieu (rempli si différent du standard)
  tla_cout_pa               integer,	-- Coût en PA de l'action en ce type de lieu (rempli si différent du standard)
  tla_cout_pn               varchar(64),	-- Coût en PN de l'action en ce type de lieu (rempli si différent du standard)
  tla_cout_pp               varchar(64),	-- Coût en PP de l'action en ce type de lieu (rempli si différent du standard)
  tla_cout_pv               varchar(64),	-- Coût en PV de l'action en ce type de lieu (rempli si différent du standard)
  constraint pk_Typelieu_action primary key (tla_id)
) ;

-- type_lieu
create table type_lieu (
  tli_id                    integer default nextval('type_lieu_tpl_id_seq') not null,	-- Identifiant
  tli_nom                   varchar(50) NOT NULL,	-- Nom du type de lieu
  constraint pk_Type_lieu primary key (tli_id)
) ;

-- debit_element
create table debit_element (
  dbt_id                    integer default nextval('debit_element_dbt_id_seq') not null,	-- Identifiant
  dbt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  dbt_egn_id                integer NOT NULL,	-- Identifiant de l'élément générique créé dans ce lieu
  constraint pk_Debit_element primary key (dbt_id)
) ;

-- vente_element
create table vente_element (
  vnt_id                    integer default nextval('vente_element_vnt_id_seq') not null,	-- Identifiant
  vnt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  vnt_elm_id                integer NOT NULL,	-- Identifiant de l'élément vendu en ce lieu
  constraint pk_Vente_element primary key (vnt_id)
) ;

-- depot_element
create table depot_element (
  dpt_id                    integer default nextval('depot_element_dpt_id_seq') not null,	-- Identifiant
  dpt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  dpt_elm_id                integer NOT NULL,	-- Identifiant de l'élément déposé en ce lieu
  dpt_prs_id                integer,	-- Identifiant du personnage possédant l'élement
  constraint pk_Depot_element primary key (dpt_id)
) ;

-- banque
create table banque (
  bnq_id                    integer default nextval('banque_bnq_id_seq') not null,	-- Identifiant
  bnq_lie_id                integer NOT NULL,	-- Identifiant du lieu
  bnq_prs_id                integer NOT NULL,	-- Identifiant du personnage possédant l'argent
  bnq_mnn_id                integer NOT NULL,	-- Identifiant de la monnaie déposée
  bnq_somme                 integer,	-- Somme d'argent déposé
  constraint pk_Banque primary key (bnq_id)
) ;

-- element_gen
create table element_gen (
  egn_id                    integer default nextval('element_gen_egn_id_seq') not null,	-- Identifiant
  egn_nom                   varchar(64) NOT NULL,	-- Nom de l'element generique
  egn_desc                  varchar(500) NOT NULL,	-- Description de l'element generique
  egn_tel_id                integer NOT NULL,	-- Type d'element generique
  egn_prix                  integer,	-- Prix en monnaie arbitraire
  egn_equipable             boolean NOT NULL,	-- Definit si l'element peut etre equipe ou non
  egn_possession_mld_id     integer,	-- Maladie liee a la simple possession de l'element
  egn_equipe_mld_id         integer,	-- Maladie liee au fait d'equiper l'element
  egn_tcn_id                integer,	-- Technologie de l'element
  egn_mtr_id                integer,	-- Type de materiau
  egn_taille                integer,	-- Taille de l'élément générique
  constraint pk_Element_gen primary key (egn_id)
) ;

-- arme
create table arme (
  arm_id                    integer default nextval('arme_arm_id_seq') not null,	-- Identifiant
  arm_agn_id                integer NOT NULL,	-- Identifiant de l'arme generique
  arm_rch_id                integer,	-- Identifiant de la recharge actuelle
  arm_elm_id                integer NOT NULL,	-- Identifiant de l'element
  constraint pk_Arme primary key (arm_id)
) ;

-- arme_gen
create table arme_gen (
  agn_id                    integer default nextval('arme_gen_agn_id_seq') not null,	-- Identifiant
  agn_tdg_id                integer NOT NULL,	-- Type de degat
  agn_egn_id                integer NOT NULL,	-- Element generique associe
  agn_mld_id                integer,	-- Maladie donnee en cas de succes
  agn_portee                integer,	-- Portee de l'arme
  agn_degat_min             integer NOT NULL,	-- Degat minimum en cas de succes
  agn_degat_max             integer NOT NULL,	-- Degat max en cas de succes
  agn_deux_mains            boolean,	-- Arme a deux mains ?
  agn_tmt_id                integer,	-- Type de munition a utiliser pour les recharges
  agn_carac1_crc_id         integer NOT NULL,	-- Première caractéristique
  agn_carac2_crc_id         integer,	-- Éventuelle seconde caracteristique
  agn_compet1_crc_id        integer NOT NULL,	-- Compétence associée à cette arme
  agn_compet2_crc_id        integer,	-- Éventuelle seconde compétence
  agn_bonus_attaque         integer default 0 NOT NULL,	-- Bonus à l'attaque
  agn_bonus_defense         integer default 0 NOT NULL,	-- Bonus à la défense
  constraint pk_Arme_gen primary key (agn_id)
) ;

-- matiere
create table matiere (
  mtr_id                    integer default nextval('matiere_mtr_id_seq') not null,	-- Identifiant
  mtr_nom                   varchar(64) NOT NULL,	-- Nom de la matiere
  mtr_desc                  varchar(500),	-- Description de la matiere
  mtr_resistance            integer,	-- Indice de resistance
  constraint pk_Matiere primary key (mtr_id)
) ;

-- technologie
create table technologie (
  tcn_id                    integer default nextval('technologie_tcn_id_seq') not null,	-- Identifiant
  tcn_nom                   varchar(64) NOT NULL,	-- Nom de la technologie
  tcn_desc                  varchar(500),	-- Description de la technologie
  tcn_taux_enrayage         integer,	-- Taux d'enrayage de la technologie
  constraint pk_Technologie primary key (tcn_id)
) ;

-- armure_gen
create table armure_gen (
  ugn_id                    integer default nextval('armure_gen_ugn_id_seq') not null,	-- Identifiant
  ugn_egn_id                integer NOT NULL,	-- Element generique associe
  ugn_bonus_defense         integer default 0 NOT NULL,	-- Éventuel bonus donné à la defense par cette arme
  constraint pk_Armure_gen primary key (ugn_id)
) ;

-- recharge_gen
create table recharge_gen (
  cgn_id                    serial not null,	-- Identifiant abstrait
  cgn_egn_id                integer NOT NULL,	-- Identifiant de l'element dont il est issu
  cgn_mnt_id                integer NOT NULL,	-- Munition contenue dans ce type de chargeur
  cgn_nb_max_mnt            integer,	-- Nombre maximal de munitions contenues dans ce type de chargeur
  constraint pk_Recharge_gen primary key (cgn_id)
) ;

-- vaisseau_gen
create table vaisseau_gen (
  vgn_id                    integer default nextval('vaisseau_gen_vsg_id_seq') not null,	-- Identifiant
  vgn_egn_id                integer NOT NULL,	-- Identifiant de l'element generique
  vgn_charge_max            integer NOT NULL,	-- Charge maximale contenue par ce modele de vaisseau
  vgn_tvs_id                integer NOT NULL,	-- Type du vaisseau (terrestre, aeroglisseur, sous-marin, spatiale)
  constraint pk_Vaisseau_gen primary key (vgn_id)
) ;

-- type_vaisseau
create table type_vaisseau (
  tvs_id                    integer default nextval('type_vaisseau_tvs_id_seq') not null,	-- Identifiant
  tvs_nom                   varchar(50) NOT NULL,	-- Nom du type de vaisseau
  constraint pk_Type_vaisseau primary key (tvs_id)
) ;

-- munition
create table munition (
  mnt_id                    integer not null,	-- Identifiant
  mnt_nom                   varchar(50) NOT NULL,	-- Nom de la munition
  mnt_desc                  varchar(500),	-- Description de la munition
  mnt_tmt_id                integer,	-- Type de munition
  mnt_mld_id                integer,	-- Maladie associee a ce type de munition
  constraint pk_Munition primary key (mnt_id)
) ;

-- type_degat
create table type_degat (
  tdg_id                    integer default nextval('type_degat_tdg_id_seq') not null,	-- Identifiant
  tdg_nom                   varchar(50) NOT NULL,	-- Nom de la munition
  constraint pk_Type_degat primary key (tdg_id)
) ;

-- armure_gen_degat
create table armure_gen_degat (
  agd_id                    serial not null,	-- Identifiant abstrait
  agd_ugn_id                integer NOT NULL,	-- Identifiant de l'armure generique
  agd_tdg_id                integer NOT NULL,	-- Type de degat encaisse
  agd_encaissement_min      integer NOT NULL,	-- Encaissement minimal
  agd_encaissement_max      integer NOT NULL,	-- Encaissement maximal
  constraint pk_Armure_gen_degat primary key (agd_id)
) ;

-- armure_gen_zone
create table armure_gen_zone (
  agz_id                    serial not null,	-- Identifiant abstrait
  agz_ugn_id                integer NOT NULL,	-- Identifiant de l'armure generique
  agz_zon_id                integer NOT NULL,	-- Zone protegee
  constraint pk_Armure_gen_zone primary key (agz_id)
) ;

-- zone_armure
create table zone_armure (
  zon_id                    integer default nextval('zone_zon_id_seq') not null,	-- Identifiant de la zone corporelle
  zon_nom                   varchar(50) NOT NULL,	-- Nom de la zone protegee
  constraint pk_Zone_armure primary key (zon_id)
) ;

-- vaisseau_gen_poste
create table vaisseau_gen_poste (
  vgp_id                    serial not null,	-- Identifiant abstrait
  vgp_vgn_id                integer NOT NULL,	-- Vaisseau generique dans lequel est ce poste
  vgp_tpt_id                integer NOT NULL,	-- Type de poste
  vgp_nb_perso              integer,	-- Nombre de personnes qui doivent occuper le poste
  constraint pk_Vaisseau_gen_poste primary key (vgp_id)
) ;

-- type_poste_tmp
create table type_poste_tmp (
  tpt_id                    integer default nextval('type_poste_tpt_id_seq') not null,	-- Identifiant
  tpt_nom                   varchar(50) NOT NULL,	-- Nom du type de vaisseau
  constraint pk_Type_poste_tmp primary key (tpt_id)
) ;

-- type_element
create table type_element (
  tel_id                    integer default nextval('type_element_tel_id_seq') not null,	-- Identifiant
  tel_nom                   varchar(64) NOT NULL,	-- Nom de la technologie
  constraint pk_Type_element primary key (tel_id)
) ;

-- armure
create table armure (
  aru_id                    integer default nextval('armure_aru_id_seq') not null,	-- Identifiant
  aru_elm_id                integer NOT NULL,	-- Identifiant de l'element
  constraint pk_Armure primary key (aru_id)
) ;

-- recharge
create table recharge (
  rch_id                    integer default nextval('recharge_rch_id_seq') not null,	-- Identifiant
  rch_elm_id                integer NOT NULL,	-- Identifiant de l'element
  rch_nb_mnt                integer,	-- Nb de munitions
  rch_utilise               boolean,	-- Recharge utilisée dans une arme ?
  constraint pk_Recharge primary key (rch_id)
) ;

-- vaisseau
create table vaisseau (
  vss_id                    integer default nextval('vaisseau_vss_id_seq') not null,	-- Identifiant
  vss_elm_id                integer NOT NULL,	-- Identifiant de l'element
  vss_sct_id                integer NOT NULL,	-- Secteur representant le vaisseau
  constraint pk_Vaisseau primary key (vss_id)
) ;

-- type_classe
create table type_classe (
  tcl_id                    integer not null,	-- Identifiant
  tcl_nom                   varchar(50) NOT NULL,	-- Nom du type (classe, aptitude)
  constraint pk_Type_classe primary key (tcl_id)
) ;

-- interdit_classe
create table interdit_classe (
  inc_id                    serial not null,	-- Identifiant
  inc_cls_id                smallint NOT NULL,	-- Classe interdisant
  inc_interdit_cls_id       smallint NOT NULL,	-- Classe interdite
  constraint pk_Interdit_classe primary key (inc_id)
) ;

-- prerequis_carac
create table prerequis_carac (
  pqa_id                    serial not null,	-- Identifiant abstrait
  pqa_cln_id                integer NOT NULL,	-- Niveau de classe a atteindre
  pqa_form_carac            integer NOT NULL,	-- Formule a remplir pour avoir le niveau de classe
  constraint pk_Prerequis_carac primary key (pqa_id)
) ;

-- taille
create table taille (
  tll_id                    integer not null,	-- Identifiant
  tll_nom                   varchar(15) NOT NULL,	-- Nom de la taille
  tll_description           varchar(150) NOT NULL,	-- Description
  tll_indice                integer NOT NULL,	-- Indice
  constraint pk_Taille primary key (tll_id)
) ;

-- race
create table race (
  rac_id                    integer default nextval('race_rac_id_seq') not null,	-- Identifiant
  rac_nom                   varchar(50) NOT NULL,	-- Nom
  rac_mld_id                smallint,	-- Maladie (malus/bonus) apportée
  rac_desc                  varchar(1500),	-- Description physique des représentants de cette race
  rac_monture               boolean default false,	-- Peut porter des personnages (oui/non)
  rac_indice_taille         integer NOT NULL,	-- Indice de base de taille d'un membre de la race
  rac_insc_ouverte          boolean default TRUE NOT NULL,	-- Race disponible à l'inscription
  constraint pk_Race primary key (rac_id)
) ;

-- acces_connu
-- Chemin vers un secteur immobile connu par un personnage
create table acces_connu (
  acc_id                    serial not null,	-- Identifiant abstrait
  acc_chm_id                integer NOT NULL,	-- Identifiant du chemin connu vers un secteur immobile
  acc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  constraint pk_Acces_connu primary key (acc_id)
) ;

-- monde_connu
-- Monde connu par un personnage
create table monde_connu (
  mnc_id                    serial not null,	-- Identifiant abstrait
  mnc_mnd_id                integer NOT NULL,	-- Identifiant du monde connu
  mnc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  constraint pk_Monde_connu primary key (mnc_id)
) ;

-- type_munition
create table type_munition (
  tmt_id                    integer default nextval('type_munition_tmt_id_seq') not null,	-- Identifiant
  tmt_nom                   varchar(50) NOT NULL,	-- Nom du type de munition
  constraint pk_Type_munition primary key (tmt_id)
) ;

-- css
-- Styles CSS de l affichage web
create table css (
  css_id                    integer default nextval('css_css_id_seq') not null,	-- Identifiant
  css_nom                   varchar(32) NOT NULL,	-- Nom du style
  css_defaut                boolean default false NOT NULL,	-- Style par defaut du jeu
  css_chemin                varchar(128) NOT NULL,	-- Chemin du fichier CSS
  constraint pk_Css primary key (css_id)
) ;

-- itineraire
-- Itineraires empruntes par les personnages
create table itineraire (
  itn_id                    integer default nextval('itineraire_itn_id_seq') not null,	-- Identifiant
  itn_chm_id                integer,	-- Identifiant eventuel du chemin emprunté (deplacement lieu-lieu)
  itn_frm_id                integer NOT NULL,	-- Identifiant de la forme du personnage
  itn_depart_lie_id         integer,	-- Identifiant du lieu de depart
  itn_arrivee_lie_id        integer,	-- Identifiant du lieu d'arrivee
  itn_traces_initiales      integer,	-- Quantite de traces initialement laissees par le suivi
  itn_traces_restantes      integer,	-- Quantite de traces restantes.
  itn_date                  timestamp,	-- Date du deplacement.
  itn_index_effacement      integer default 1 NOT NULL,	-- Nombre de traces qui vont disparaître à la prochaine "remise générale"
  itn_ttr_id                integer NOT NULL,	-- Identifiant du type de trace
  itn_profond_trace         integer default 1 NOT NULL,	-- Profondeur de la trace
  itn_taille_trace          integer default 1 NOT NULL,	-- Pointure de la trace
  constraint pk_Itineraire primary key (itn_id)
) ;

-- type_filature
create table type_filature (
  tfl_id                    integer not null,	-- Identifiant
  tfl_nom                   varchar(50) NOT NULL,	-- Nom du type: filature (voleur, de visu), pistage (par trace), suite (pour guide, volontaire)
  constraint pk_Type_filature primary key (tfl_id)
) ;

-- mime
-- Table de mimes (actions gestuelles)
create table mime (
  mim_id                    integer default nextval('mime_mim_id_seq') not null,	-- Identifiant
  mim_nom                   varchar(24) NOT NULL,	-- Nom du mime
  mim_label                 varchar(64) NOT NULL,	-- Chaine de caracteres symbolisant le mime
  mim_tmm_id                integer NOT NULL,	-- Type de mime
  constraint pk_Mime primary key (mim_id)
) ;

-- type_mime
create table type_mime (
  tmm_id                    integer not null,	-- Identifiant
  tmm_nom                   varchar(50) NOT NULL,	-- Nom du type: simple, lié à un personnage
  constraint pk_Type_mime primary key (tmm_id)
) ;

-- rumeur
-- Tables des rumeurs
create table rumeur (
  rmr_id                    integer default nextval('rumeur_rmr_id_seq') not null,	-- Identifiant
  rmr_nom                   varchar(50) NOT NULL,	-- Nom de la rumeur
  rmr_prs_id                integer NOT NULL,	-- Identifiant du personnage a l'origine de la rumeur
  rmr_content               varchar(5000) NOT NULL,	-- Contenu de la rumeur
  rmr_lng_id                integer NOT NULL,	-- Langue de la rumeur
  rmr_lie_id                integer NOT NULL,	-- Lieu d'origine de la rumeur
  rmr_rayon                 float,	-- Rayon de la rumeur (pour les lieux géographiques)
  rmr_sct_id                integer,	-- Éventuel secteur d'influence si la rumeur a été lancée depuis un lieu topologique
  rmr_diff                  float,	-- Difficulté pour découvrir la rumeur
  rmr_date_debut            date,	-- Date d'apparition de la rumeur
  rmr_date_fin              date,	-- Date de fin de la rumeur
  constraint pk_Rumeur primary key (rmr_id)
) ;

-- rumeur_ecoute
-- Tables des ecoutes de rumeurs
create table rumeur_ecoute (
  rmc_id                    integer default nextval('rumeur_ecoute_rmc_id_seq') not null,	-- Identifiant
  rmc_prs_id                integer NOT NULL,	-- Identifiant du personnage ayant entendu la rumeur
  rmc_rmr_id                integer NOT NULL,	-- Identifiant de la rumeur
  constraint pk_Rumeur_ecoute primary key (rmc_id)
) ;

-- langue
-- Langue
create table langue (
  lng_id                    integer not null,	-- Identifiant
  lng_nom                   varchar(50) NOT NULL,	-- Nom
  lng_description           varchar(500),	-- Description de son histoire, son utilisation, ...
  constraint pk_Langue primary key (lng_id)
) ;

-- filature
-- Tables des filatures, pistages, guides
create table filature (
  flt_id                    integer default nextval('filature_flt_id_seq') not null,	-- Identifiant
  flt_suivant_prs_id        integer NOT NULL,	-- Identifiant du personnage qui fait l'action de suivre.
  flt_suivi_frm_id          integer NOT NULL,	-- Identifiant de la forme suivie.
  flt_tfl_id                integer NOT NULL,	-- Type de filature
  flt_debut                 timestamp NOT NULL,	-- Date de début de la filature
  constraint pk_Filature primary key (flt_id)
) ;

-- type_trace
-- Types de trace disponibles
create table type_trace (
  ttr_id                    integer default nextval('type_trace_ttr_id_seq') not null,	-- Identifiant abstrait
  ttr_nom                   varchar(64),	-- Nom de la trace
  constraint pk_Type_trace primary key (ttr_id)
) ;

-- trace_connue
-- Traces connues par les personnages
create table trace_connue (
  trc_id                    serial not null,	-- Identifiant
  trc_ttr_id                integer,	-- Identifiant du type de trace
  trc_taille                integer,	-- Pointure
  trc_profondeur            integer,	-- Profondeur
  trc_frm_trace_id          integer,	-- Identifiant de la forme qui a laissé les traces
  trc_prs_id                integer,	-- Identifiant du personnage qui a découvert les traces
  trc_date_fin              timestamp,	-- Date jusqu'à laquelle on se souviendra de ces traces
  trc_nom_donne             varchar(64),	-- Nom donné à ces traces
  constraint pk_Trace_connue primary key (trc_id)
) ;

-- lieu_carto
create table lieu_carto (
  lic_id                    serial not null,	-- Identifiant
  lic_lie_id                integer NOT NULL,	-- Identifiant du leiu associé
  lic_mnd_id                integer NOT NULL,	-- Identifiant du monde associé
  lic_ssn_id                integer,	-- Saison du lieu
  constraint pk_Lieu_carto primary key (lic_id)
) ;

-- cycle
-- Succession de periodes ou de cycles
create table cycle (
  ccl_id                    integer not null,	-- Identifiant
  ccl_nom                   varchar(50) NOT NULL,	-- Nom
  ccl_nombre_periode        integer NOT NULL,	-- Nombre de periodes du cycle
  ccl_duree_periode         integer,	-- Duree d une periode en heure
  ccl_duree_nombre_cycles   integer,	-- La periode du cycle correspond a n autres cycles
  ccl_duree_ccl_id          integer,	-- Identifiant du cycle etalon
  ccl_duree_nombre_cycles_ecoules integer default 0 NOT NULL,	-- Nombre de cycles ecoules
  ccl_pourcentage_alea      integer,	-- Alea eventuel: 0% pour perfection; 100% pour un alea complet
  constraint pk_Cycle primary key (ccl_id)
) ;

-- cycle_monde
-- Rattachement de cycles aux mondes
create table cycle_monde (
  cmn_id                    serial not null,	-- Identifiant
  cmn_mnd_id                integer NOT NULL,	-- Identifiant du monde associé
  cmn_ccl_id                integer NOT NULL,	-- Identifiant du cycle associé
  constraint pk_Cycle_monde primary key (cmn_id)
) ;

-- cycle_secteur
-- Rattachement de cycles aux secteurs
create table cycle_secteur (
  csc_id                    serial not null,	-- Identifiant
  csc_sct_id                integer NOT NULL,	-- Identifiant du secteur associé
  csc_ccl_id                integer NOT NULL,	-- Identifiant du cycle associé
  constraint pk_Cycle_secteur primary key (csc_id)
) ;

-- periode
-- Detail d une periode d un cycle
create table periode (
  prd_id                    integer not null,	-- Identifiant
  prd_nom                   varchar(50) NOT NULL,	-- Nom
  prd_ccl_id                integer NOT NULL,	-- Cycle correspondant
  prd_min                   integer,	-- Minimum de periode
  prd_max                   integer,	-- Maximum de periode
  prd_complement_description varchar(1500),	-- Complement de description de la periode
  constraint pk_Periode primary key (prd_id)
) ;

-- lieu_cycle
-- Rattachement de lieux aux cycles
create table lieu_cycle (
  lcd_id                    serial not null,	-- Identifiant
  lcd_ccl_id                integer NOT NULL,	-- Identifiant du cycle associé
  lcd_lie_id                integer NOT NULL,	-- Identifiant du lieu associé
  constraint pk_Lieu_cycle primary key (lcd_id)
) ;

-- lieu_cyclique
-- Aspect complémentaire d un lieu à un cycle donné
-- né
-- ©
create table lieu_cyclique (
  lcc_id                    serial not null,	-- Identifiant
  lcc_prd_id                integer NOT NULL,	-- Identifiant de la période associée
  lcc_lie_id                integer NOT NULL,	-- Identifiant du lieu associé
  lcc_description           varchar(1500),	-- Description complémentaire à cette période donnée
  constraint pk_Lieu_cyclique primary key (lcc_id)
) ;

-- calque
-- Calque géographique représentant selon : le relief, le climat, les
-- voieries, etc.
create table calque (
  clq_id                    integer default nextval('calque_clq_id_seq') not null,	-- Identifiant
  clq_nom                   varchar(64) NOT NULL,	-- Nom
  clq_description           varchar(1500),	-- Description
  clq_indice_population     integer default 0 NOT NULL,	-- Indice de densité de population
  clq_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  clq_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de traces
  clq_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout journalier en pn
  clq_cout_acces_pn         varchar(64) default '0' NOT NULL,	-- Cout d'accès en pn
  clq_cout_pv               varchar(64) default '0' NOT NULL,	-- Cout journalier en point de vie
  clq_cout_acces_pa         varchar(64) default '0' NOT NULL,	-- Cout d'accès en point d'action
  clq_cout_echec_pv         varchar(64) default '0' NOT NULL,	-- Cout en pv en cas d'échec
  clq_couleur               char(7),	-- Code couleur associé
  clq_tcq_id                integer NOT NULL,	-- Type de calque
  clq_id_externe            integer NOT NULL,	-- Identifiant externe à l'application utilisé lors des imports
  clq_difficulte            varchar(64) default '0' NOT NULL,	-- Formule de difficulté de déplacement dans ces zones et de fait facilité à se perdre
  clq_crc_id                integer,	-- Compétence associée
  clq_crc2_crc_id           integer,	-- Éventuelle deuxième caractéristique associée
  clq_echec_bloquant        boolean default FALSE NOT NULL,	-- Si à vrai, échouer sur son déplacement ne fait pas progresser
  constraint pk_Calque primary key (clq_id)
) ;

-- type_calque
-- Type de calque
create table type_calque (
  tcq_id                    integer default nextval('type_calque_tcq_id_seq') not null,	-- Identifiant
  tcq_nom                   varchar(50) NOT NULL,	-- Nom du type de calque
  tcq_principal             boolean default FALSE NOT NULL,	-- Un type de calque est désigné comme le type de calque principal. Le label de base sera prit sur le calque correspondant au type de calque principal.
  constraint pk_Type_calque primary key (tcq_id)
) ;

-- calque_lieu_carto
-- Rattachement de calques aux lieux carto
create table calque_lieu_carto (
  clc_id                    serial not null,	-- Identifiant
  clc_clq_id                integer NOT NULL,	-- Identifiant du calque associé
  clc_lic_id                integer NOT NULL,	-- Identifiant du lieu carto associé
  constraint pk_Calque_lieu_carto primary key (clc_id)
) ;

-- type_vehicule
-- Type de vehicule
create table type_vehicule (
  tvh_id                    integer default nextval('type_vehicule_tvh_id_seq') not null,	-- Identifiant
  tvh_nom                   varchar(50) NOT NULL,	-- Nom du type de véhicule
  tvh_carburant_egn_id      integer,	-- Type de carburant employé
  tvh_puissance             integer,	-- Puissance developpée lors d'un déplacement
  tvh_consommation          integer,	-- Nombre d'unité de carburant utilisé par tour
  tvh_usure_max             integer,	-- Maximum de point d'usure pour un vehicule réparé ou neuf
  tvh_usure                 integer,	-- Point d'usure gagné à chaque tour lorsque en marche
  constraint pk_Type_vehicule primary key (tvh_id)
) ;

-- vent_lieu_carto
-- Rattachement de vents aux lieux carto
create table vent_lieu_carto (
  cvt_id                    serial not null,	-- Identifiant
  cvt_tvt_id                integer NOT NULL,	-- Identifiant du type de vent associé
  cvt_lic_id                integer NOT NULL,	-- Identifiant du lieu carto associé
  cvt_force                 point,	-- Force du vent
  constraint pk_Vent_lieu_carto primary key (cvt_id)
) ;

-- restriction_calque
-- Restriction d un type de vehicule à un calque en particulier
-- r
create table restriction_calque (
  rtc_id                    serial not null,	-- Identifiant
  rtc_tvh_id                integer NOT NULL,	-- Identifiant du type de véhicule
  rtc_clq_id                integer NOT NULL,	-- Identifiant du calque de restriction
  rtc_rendement             integer default 100 NOT NULL,	-- Rendement associé à ce type de terrain
  constraint pk_Restriction_calque primary key (rtc_id)
) ;

-- influence_vent
-- Vent influençant les déplacements d un type de vehicule
-- le
create table influence_vent (
  inv_id                    serial not null,	-- Identifiant
  inv_tvh_id                integer NOT NULL,	-- Identifiant du type de véhicule
  inv_tvt_id                integer NOT NULL,	-- Identifiant du type de vent
  inv_puissance             integer NOT NULL,	-- Puissance associée à ce moyen de propulsion
  constraint pk_Influence_vent primary key (inv_id)
) ;

-- vehicule
-- Véhicule
-- e
create table vehicule (
  vhc_id                    integer default nextval('vehicule_vhc_id_seq') not null,	-- Identifiant
  vhc_tvh_id                integer NOT NULL,	-- Identifiant du type de véhicule
  vhc_reservoir             integer,	-- Maximum de ressource pouvant être stocké
  vhc_reservoir_actuel      integer,	-- Niveau de ressource actuel dans le réservoir
  vhc_usure_actuelle        integer,	-- Niveau d'usure actuel
  vhc_amarre                boolean,	-- À vrai si le véhicule est amarré
  vhc_moteur_fonctionnel    boolean,	-- À vrai si le moteur est en route
  vhc_masse                 integer,	-- Masse à déplacer
  vhc_bride_moteur          integer,	-- Pourcentage de puissance laissée pour le moteur
  vhc_bride_vent            integer,	-- Pourcentage de puissance laissée pour la propulsion passive
  vhc_direction             point,	-- Direction souhaitée
  vhc_date_remise           date,	-- Date de remise
  vhc_reserve_nourriture    integer,	-- Réserve de nourriture max dans le véhicule
  vhc_reserve_nourriture_actuel integer,	-- Niveau actuel de la réserve de nourriture
  constraint pk_Vehicule primary key (vhc_id)
) ;

-- type_vent
-- Type de vent
create table type_vent (
  tvt_id                    integer default nextval('type_vent_tvt_id_seq') not null,	-- Identifiant
  tvt_nom                   varchar(50) NOT NULL,	-- Nom du type de vent
  constraint pk_Type_vent primary key (tvt_id)
) ;

-- type_poste
-- Type de poste
create table type_poste (
  tps_id                    integer default nextval('type_poste_tps_id_seq') not null,	-- Identifiant
  tps_nom                   varchar(50) NOT NULL,	-- Nom du type de poste
  tps_rendement_min         integer default 0 NOT NULL,	-- Rendement minimum d'un poste
  tps_propulsion_active     boolean default FALSE,	-- Poste pour une propulsion active ?
  tps_formule_difficulte    varchar(60) default '1' NOT NULL,	-- Formule de difficulté de tenue du poste
  constraint pk_Type_poste primary key (tps_id)
) ;

-- poste
-- Poste en place sur un véhicule
-- e
create table poste (
  pst_id                    serial not null,	-- Identifiant
  pst_tps_id                integer NOT NULL,	-- Identifiant du type de poste
  pst_prs_id                integer,	-- Identifiant de l'éventuelle personne assurant le poste
  pst_vhc_id                integer NOT NULL,	-- Identifiant du véhicule associé
  constraint pk_Poste primary key (pst_id)
) ;

-- type_vehicule_poste
-- Type poste en place sur un type de véhicule
-- e
create table type_vehicule_poste (
  ttp_id                    serial not null,	-- Identifiant
  ttp_tvh_id                integer NOT NULL,	-- Identifiant du type de véhicule
  ttp_tps_id                integer,	-- Identifiant du type de poste
  constraint pk_Type_vehicule_poste primary key (ttp_id)
) ;

-- monture
-- Monture (cheval, éléphant, moto...)
-- .)
create table monture (
  mon_id                    serial not null,	-- Identifiant de la monture
  mon_prs_id                integer NOT NULL,	-- Personnage associé à la monture
  mon_cavalier_prs_id       integer,	-- Éventuel identifiant du cavalier
  mon_clef_elm_id           integer,	-- Éventuelle clé nécessaire pour faire fonctionner la monture
  mon_vivante               boolean NOT NULL,	-- Indique si une créature est vivante ou non (et notamment si elle consomme des PNs à l'arrêt)
  mon_egn_id                integer,	-- Éventuel type de carburant à associer à la monture
  mon_consomation           integer,	-- Consomation par case franchie
  mon_usure                 integer,	-- Dans le cas de monture inanimée, usure (en « PV ») par déplacement
  mon_surplus_cout_pn       integer,	-- Éventuel surplus de cout de PN en charge
  mon_formule_difficulte    varchar(60) default '1' NOT NULL,	-- Formule de maitrise de la monture
  constraint pk_Monture primary key (mon_id)
) ;

-- particularite_chemin
-- Particularité associée à un chemin (champs textuel du style « par la
-- fenêtre »,  « par la porte »)
-- »)
-- )
create table particularite_chemin (
  pch_id                    serial not null,	-- Identifiant
  pch_nom                   varchar(128) NOT NULL,	-- Nom de la particularité
  constraint pk_Particularite_chemin primary key (pch_id)
) ;

-- description_lieu
-- Description plus précises de certains lieux
-- x
create table description_lieu (
  dsl_id                    serial not null,	-- Identifiant
  dsl_lie_id                integer NOT NULL,	-- 	
  dsl_difficulte            integer NOT NULL,	-- Difficulté associée à la connaissance de cette description
  dsl_description           varchar(1500) NOT NULL,	-- Complément de description
  constraint pk_Description_lieu primary key (dsl_id)
) ;

-- classeniveau_action
-- Limitation des actions pour certains niveaux de certaines classes
create table classeniveau_action (
  cna_id                    serial not null,	-- Identifiant
  cna_act_id                integer NOT NULL,	-- Identifiant de l'action spécialement considérée pour cette classe
  cna_cln_id                integer NOT NULL,	-- Identifiant du niveau de classe autorisé
  cna_autorise              boolean,	-- Action autorisée (True) ou interdite (False) spécifiquement pour cette classe
  constraint pk_Classeniveau_action primary key (cna_id)
) ;

-- engagement
-- Engagement entre personnage
create table engagement (
  eng_id                    serial not null,	-- Identifiant
  eng_prs1_prs_id           integer NOT NULL,	-- Identifiant du premier personnage impliqué dans l'engagement
  eng_prs2_prs_id           integer NOT NULL,	-- Identifiant du deuxième personnage impliqué dans l'engagement
  eng_init_1                boolean,	-- À vrai si le premier personnage a l'initiative sinon l'initiative appartient au second personnage
  eng_lie_id                integer NOT NULL,	-- Lieu où se passe la confrontation
  eng_distant               boolean,	-- À vrai si engagement distant sinon engagement de corps à corps
  constraint pk_Engagement primary key (eng_id)
) ;

-- coffre
-- Conteneur d''objet
create table coffre (
  cff_id                    integer default nextval('coffre_cff_id_seq') not null,	-- Identifiant
  cff_elm_id                integer NOT NULL,	-- Identifiant de l'élément associé
  cff_clef_egn_id           integer,	-- Clef générique du coffre
  cff_taille                integer NOT NULL,	-- Indice de la contenance de ce coffre
  cff_ouvert                boolean default True,	-- État ouvert/fermé
  constraint pk_Coffre primary key (cff_id)
) ;

-- coffre_element
create table coffre_element (
  cel_id                    serial not null,	-- Identifiant
  cel_cff_id                integer NOT NULL,	-- Coffre associé
  cel_elm_id                integer NOT NULL,	-- Élément associé
  cel_prix                  integer,	-- Éventuel prix associé si un magasin est associé au coffre
  cel_mnn_id                integer,	-- Monnaie associée
  constraint pk_Coffre_element primary key (cel_id)
) ;

-- element_visible
-- Éléments visible pour un personnage
-- ge
create table element_visible (
  elv_id                    serial not null,	-- Identifiant
  elv_prs_id                integer NOT NULL,	-- Personnage associé
  elv_elm_id                integer NOT NULL,	-- Élément associé
  constraint pk_Element_visible primary key (elv_id)
) ;

-- image
-- Image
create table image (
  img_id                    integer default nextval('image_img_id_seq') not null,	-- Identifiant
  img_nom                   varchar,	-- Nom associé
  img_adresse               varchar NOT NULL,	-- Adresse relative de l'image
  constraint pk_Image primary key (img_id)
) ;

-- boutique
-- Boutique
create table boutique (
  btq_id                    integer default nextval('boutique_btq_id_seq') not null,	-- Identifiant
  btq_cff_id                integer NOT NULL,	-- Identifiant du coffre associé
  btq_nom                   varchar(500),	-- Nom de la boutique
  btq_description           varchar(2000),	-- Éventuelle description associée à la boutique
  btq_prs_id                integer,	-- Éventuel gérant de la boutique
  btq_atm_id                integer,	-- Éventuel automate associé
  btq_ouverte               boolean,	-- À vrai si la boutique est ouverte
  constraint pk_Boutique primary key (btq_id)
) ;

-- automate
-- Automate similant un personnage. Des caractéristiques lui sont
-- attribuées.
-- .
create table automate (
  atm_id                    integer default nextval('automate_atm_id_seq') not null,	-- Identifiant
  atm_nom                   varchar(500),	-- Éventuel nom associé
  constraint pk_Automate primary key (atm_id)
) ;

-- element_gen_connu
-- Éléments génériques identifiés par un personnage
-- nnage
create table element_gen_connu (
  egc_id                    serial not null,	-- Identifiant
  egc_egn_id                integer NOT NULL,	-- Élément générique associé
  egc_prs_id                integer NOT NULL,	-- Personnage associé
  constraint pk_Element_gen_connu primary key (egc_id)
) ;

-- automate_carac
-- Caractéristiques associées à un automate
-- ate
create table automate_carac (
  acr_id                    serial not null,	-- Identifiant abstrait
  acr_atm_id                integer NOT NULL,	-- Automate associé
  acr_crc_id                integer NOT NULL,	-- Caracteristique associée
  acr_score                 smallint default 0 NOT NULL,	-- Score dans la caractéristique.
  constraint pk_Automate_carac primary key (acr_id)
) ;

-- achat_element_gen
-- Éléments génériques achetables par une boutique donnée
-- nnée
-- e
create table achat_element_gen (
  aeg_id                    serial not null,	-- Identifiant
  aeg_egn_id                integer NOT NULL,	-- Élément générique associé
  aeg_btq_id                integer NOT NULL,	-- Boutique associée
  aeg_prix                  integer,	-- Prix de base
  aeg_mnn_id                integer,	-- Monnaie associée au prix
  aeg_nombre_max            integer,	-- Nombre maximum d'éléments achetables
  constraint pk_Achat_element_gen primary key (aeg_id)
) ;

-- type_environnement
-- Environnement d''un secteur
create table type_environnement (
  tev_id                    integer default nextval('type_environnement_tev_id_seq') not null,	-- Identifiant
  tev_nom                   varchar(50) NOT NULL,	-- Nom de l'environnement
  tev_crc_id                integer NOT NULL,	-- Caractéristique associée
  constraint pk_Type_environnement primary key (tev_id)
) ;

-- profil_annonce
-- Profil mis à disposition
-- n
create table profil_annonce (
  pro_id                    serial not null,	-- Identifiant
  pro_nom                   character varying(255) NOT NULL,	-- Nom
  pro_statut                integer default 0 NOT NULL,	-- Entier pointant vers des statuts
  pro_ctp_id                integer NOT NULL,	-- Catégorie de profil
  pro_mji_id                integer NOT NULL,	-- MJ associé
  pro_desc                  text NOT NULL,	-- Description
  pro_debat                 text,	-- Discussion entre MJs
  pro_nbpersos              integer default 1 NOT NULL,	-- Nombre de personnages à pourvoir
  pro_historique            text,	-- Historique
  pro_demandeaction         integer,	-- Demande d'action sur un profil
  constraint pk_Profil_annonce primary key (pro_id)
) ;

-- news
-- Nouvelles
create table news (
  new_id                    serial not null,	-- Identifiant
  new_titre                 text NOT NULL,	-- Titre de la nouvelle
  new_txt                   text NOT NULL,	-- Texte de la nouvelle
  new_date                  timestamp without time zone NOT NULL,	-- Date de publication
  new_dateedit              timestamp without time zone,	-- Date de dernière modification
  new_mji_id                integer NOT NULL,	-- MJ associé
  constraint pk_News primary key (new_id)
) ;

-- categorie_profil
-- Catégories des profils
-- s
create table categorie_profil (
  ctp_id                    serial not null,	-- Identifiant
  ctp_nom                   character varying(255) NOT NULL,	-- Nom
  constraint pk_Categorie_profil primary key (ctp_id)
) ;

-- candidature
-- Candidature
create table candidature (
  can_id                    serial not null,	-- Identifiant
  can_jou_id                integer NOT NULL,	-- Joueur associé
  can_pro_id                integer NOT NULL,	-- Profil associé
  can_statut                integer NOT NULL,	-- Statut de la candidature
  can_nomperso              character varying(255) NOT NULL,	-- Nom du personnage
  can_rac_id                integer NOT NULL,	-- Race associée
  can_cls_id                integer NOT NULL,	-- Classe associée
  can_sex_id                integer NOT NULL,	-- Sexe associé
  can_descriptionperso      text NOT NULL,	-- Description du personnage
  can_backgroundperso       text NOT NULL,	-- Background du personnage
  can_butsperso             integer NOT NULL,	-- Buts du personnage
  can_debatjoumj            text,	-- Débat entre le recruteur et le joueur
  can_debatmj               text,	-- Débat entre le recruteur et le MJ qui a proposé le profil
  can_historique            text,	-- Historique de la candidature
  can_commentaire           text,	-- Pense-bête pour le recruteur
  can_datemodification      timestamp without time zone,	-- Date de dernière modification
  constraint pk_Candidature primary key (can_id)
) ;

-- question
-- Question du quizz
create table question (
  que_id                    serial not null,	-- Identifiant
  que_intitule              text NOT NULL,	-- Intitulé de la question
  constraint pk_Question primary key (que_id)
) ;

-- reponse
-- Proposition de réponse à une question du quizz
-- zz
create table reponse (
  rep_id                    serial not null,	-- Identifiant
  rep_que_id                integer NOT NULL,	-- Question associée
  rep_intitule              text NOT NULL,	-- Intitulé de la réponse
  rep_bonne                 boolean default False NOT NULL,	-- À vrai si la réponse est bonne
  constraint pk_Reponse primary key (rep_id)
) ;

-- profil_classe
create table profil_classe (
  pcl_id                    serial not null,	-- Identifiant abstrait
  pcl_pro_id                integer NOT NULL,	-- Profil associé
  pcl_cls_id                integer NOT NULL,	-- Classe associée
  constraint pk_Profil_classe primary key (pcl_id)
) ;

-- profil_race
create table profil_race (
  pra_id                    serial not null,	-- Identifiant abstrait
  pra_pro_id                integer NOT NULL,	-- Profil associé
  pra_rac_id                integer NOT NULL,	-- Race associée
  constraint pk_Profil_race primary key (pra_id)
) ;

-- profil_sexe
create table profil_sexe (
  psx_id                    serial not null,	-- Identifiant abstrait
  psx_pro_id                integer NOT NULL,	-- Profil associé
  psx_sex_id                integer NOT NULL,	-- Sexe associé
  constraint pk_Profil_sexe primary key (psx_id)
) ;
comment on column personnage.prs_id is 'Identifiant du personnage';
comment on column personnage.prs_jou_id is 'Identifiant du joueur jouant le personnage';
comment on column personnage.prs_principal is 'Personnage par defaut du joueur (oui/non)';
comment on column personnage.prs_possesseur_prs_id is 'Identifiant du personnage possedant celui-ci';
comment on column personnage.prs_lie_id is 'Identifiant du lieu';
comment on column personnage.prs_nom_originel is 'Nom de depart';
comment on column personnage.prs_nom_actuel is 'Nom actuel';
comment on column personnage.prs_background is 'Passe du personnage';
comment on column personnage.prs_biographie is 'Histoire du personnage';
comment on column personnage.prs_inscription is 'Date d''inscription';
comment on column personnage.prs_derniere_connexion is 'Date de derniere connexion';
comment on column personnage.prs_pa is 'Nombre de Points d''Action';
comment on column personnage.prs_pa_max is 'Nombre de Points d''Action maximum';
comment on column personnage.prs_pn is 'Nombre de Points de Nourriture';
comment on column personnage.prs_pn_max is 'Nombre de Points de Nourriture maximum';
comment on column personnage.prs_pp is 'Nombre de Points de Paralysie';
comment on column personnage.prs_pp_max is 'Nombre de Points de Paralysie maximum';
comment on column personnage.prs_pv is 'Nombre de Points de Vie';
comment on column personnage.prs_pv_max is 'Nombre de Points de Vie maximum';
comment on column personnage.prs_pc is 'Points de combat';
comment on column personnage.prs_pc_max is 'Maximum de point de combat';
comment on column personnage.prs_tpr_id is 'Type de personnage: pj, pnj, familier, etc.';
comment on column personnage.prs_calc_facteur_nb_trace is 'Facteur influant sur le nombre de trace calculé en fonction de ses attributs, d''éventuelles compétences, de son équipement, etc.';
comment on column personnage.prs_date_arrivee is 'Date d''arrivée dans un lieu';
comment on column personnage.prs_delta_rose is 'Delta de décalage par rapport à la rose des vents';

comment on column joueur.jou_id is 'Identifiant';
comment on column joueur.jou_pseudo is 'Pseudonyme';
comment on column joueur.jou_mdp is 'Mot de passe (md5)';
comment on column joueur.jou_email is 'Adresse mail';
comment on column joueur.jou_css_id is 'Style CSS utilisé par le joueur';
comment on column joueur.jou_fuseau is 'Decalage horaire par rapport a l''heure GMT';
comment on column joueur.jou_session_id is 'Identifiant de session';
comment on column joueur.jou_scorequizz is 'Score au quizz';
comment on column joueur.jou_ipinitiale is 'IP d''inscription';
comment on column joueur.jou_ipcourante is 'Dernière IP connue';
comment on column joueur.jou_commentaire is 'Éventuel commentaire de l''utilisateur';

comment on column mj.mji_id is 'Identifiant';
comment on column mj.mji_jou_id is 'Identifiant du joueur';
comment on column mj.mji_nom is 'Nom';
comment on column mj.mji_derniere_connexion is 'Date de derniere connexion';
comment on column mj.mji_droits_news is 'Rédacteur de news (O/N)';
comment on column mj.mji_droits_all_news is 'Administrateur des news';
comment on column mj.mji_droits_inscription is 'Droit de proposition de profil';
comment on column mj.mji_droits_recrutement is 'Administrateur des incriptions';
comment on column mj.mji_droits_edit_regles is 'Rédacteur wiki règles';
comment on column mj.mji_droits_edit_monde is 'Rédacteur wiki background';
comment on column mj.mji_droits_edit_admin is 'Administrateur technique';

comment on column perso_classe.pce_id is 'Identifiant abstrait';
comment on column perso_classe.pce_prs_id is 'Personnage';
comment on column perso_classe.pce_cln_id is 'Niveau de classe';
comment on column perso_classe.pce_titre is 'Titre accordé au personnage (si différent de celui de classe)';

comment on column forme.frm_id is 'Identifiant';
comment on column forme.frm_prs_id is 'Identifiant du personnage pouvant prendre cette forme';
comment on column forme.frm_rac_id is 'Identifiant de la race';
comment on column forme.frm_sex_id is 'Sexe';
comment on column forme.frm_indice_taille is 'Indice de taille de la forme';
comment on column forme.frm_actuelle is 'Definit la forme actuelle du personnage (oui/non)';
comment on column forme.frm_description is 'Description physique';
comment on column forme.frm_desc_actuelle is 'Description de l''etat actuel';
comment on column forme.frm_url_image is 'URL de l''image illustrant le personnage sous cette forme';
comment on column forme.frm_ttr_id is 'Type de trace de la forme';
comment on column forme.frm_profond_trace is 'Profondeur de la trace laissée';

comment on column classe_niveau.cln_id is 'Identifiant';
comment on column classe_niveau.cln_cls_id is 'Classe';
comment on column classe_niveau.cln_niveau is 'Niveau dans la classe';
comment on column classe_niveau.cln_mld_id is 'Maladie de classe';
comment on column classe_niveau.cln_titre is 'Titre donné au détenteur de ce niveau de classe';
comment on column classe_niveau.cln_titre_visible is 'Titre visible par les autres (oui/non)';
comment on column classe_niveau.cln_desc_mj is 'Description pour MJ';

comment on column classe.cls_id is 'Identifiant';
comment on column classe.cls_nom is 'Nom';
comment on column classe.cls_desc_mj is 'Description à l''adresse des MJ';
comment on column classe.cls_tcl_id is 'Type de classe: classe, aptitude';
comment on column classe.cls_insc_ouverte is 'Inscription ouverte pour cette classe';

comment on column perso_maladie.pml_id is 'Identifiant abstrait';
comment on column perso_maladie.pml_prs_id is 'Personnage malade';
comment on column perso_maladie.pml_mld_id is 'Maladie';
comment on column perso_maladie.pml_debut is 'Date d''apparition de la maladie';
comment on column perso_maladie.pml_fin is 'Eventuelle date de fin de la maladie';

comment on column maladie.mld_id is 'Identifiant';
comment on column maladie.mld_nom is 'Nom';
comment on column maladie.mld_description is 'Description generale';
comment on column maladie.mld_tml_id is 'Type de maladie';
comment on column maladie.mld_duree is 'Duree en tours';
comment on column maladie.mld_periode is 'Periode en tours entre 2 effets';
comment on column maladie.mld_description_physique is 'Description perçue par un tiers sur le malade (vide si rien de visible)';
comment on column maladie.mld_formule is 'Formule à appliquer pour trouver les caractéristiques actuelles';

comment on column caracteristique.crc_id is 'Identifiant';
comment on column caracteristique.crc_nom is 'Nom';
comment on column caracteristique.crc_description is 'Description de son utilisation';
comment on column caracteristique.crc_tcr_id is 'Type de caracteristique';
comment on column caracteristique.crc_ncr_id is 'Nature';
comment on column caracteristique.crc_delta_min_max is 'Delta minimum et maximum';
comment on column caracteristique.crc_visible is 'À vrai si le score dans cette caractéristiques est visible par le joueur';

comment on column perso_carac.pcr_id is 'Identifiant abstrait';
comment on column perso_carac.pcr_prs_id is 'Personnage';
comment on column perso_carac.pcr_crc_id is 'Caracteristique';
comment on column perso_carac.pcr_jauge_entrainement is 'Jauge d''entrainement (entre 0 et 100)';
comment on column perso_carac.pcr_score_xp is 'Nombre de points d''experience actuel dans la caracteristique';
comment on column perso_carac.pcr_score_xp_max is 'Nombre de points d''experience maximum atteint dans la caracteristique';
comment on column perso_carac.pcr_score_calcule is 'Score apres calcul a partir de pcr_score_xp apres application des differentes maladies, classes, etc.';
comment on column perso_carac.pcr_score_max is 'Score absolu pouvant etre atteint en fonction de l''age, la race, le sexe du personnage';

comment on column parametre.prm_id is 'Identifiant';
comment on column parametre.prm_ctg_id is 'Ensemble d''appartenance';
comment on column parametre.prm_libelle is 'Libelle explicitant a quoi il sert';
comment on column parametre.prm_valeur is 'Valeur du parametre (c''est un varchar mais des entiers ou des dates peuvent y etre entres)';
comment on column parametre.prm_description is 'Description de ce qui peut etre mis dans la valeur';

comment on column categorie.ctg_id is 'Identifiant';
comment on column categorie.ctg_libelle is 'Libelle de l''ensemble de parametres';
comment on column categorie.ctg_systeme is 'Regroupement de parametres systemes (oui/non : modifiable ou non)';

comment on column perso_classe_int.pci_id is 'Identifiant abstrait';
comment on column perso_classe_int.pci_prs_id is 'Personnage';
comment on column perso_classe_int.pci_cls_id is 'Classe autorisée/interdite';
comment on column perso_classe_int.pci_type_interdit is 'Interdiction de la classe pour le personnage (oui/non)';

comment on column prerequis_classe.pqc_id is 'Identifiant abstrait';
comment on column prerequis_classe.pqc_cln_id is 'Niveau de classe a atteindre';
comment on column prerequis_classe.pqc_cls_id is 'Classe necessaire';
comment on column prerequis_classe.pqc_niveau_min is 'Niveau minimum a avoir dans la classe necessaire ';

comment on column monde.mnd_id is 'Identifiant';
comment on column monde.mnd_nom is 'Nom';
comment on column monde.mnd_tmn_id is 'Type de monde';
comment on column monde.mnd_description is 'Description générale du monde';
comment on column monde.mnd_pere_mnd_id is 'Monde contenant celui-ci';
comment on column monde.mnd_niveau is 'Indice de niveau';
comment on column monde.mnd_mld_id is 'Maladie régnant sur ce monde';
comment on column monde.mnd_image is 'Image du monde (fond de la carte)';

comment on column lieu.lie_id is 'Identifiant';
comment on column lieu.lie_tli_id is 'Type de lieu';
comment on column lieu.lie_nom is 'Nom';
comment on column lieu.lie_sct_id is 'Secteur d''appartenance';
comment on column lieu.lie_indice_taille is 'Taille du lieu';
comment on column lieu.lie_indice_population is 'Indice de densité de population';
comment on column lieu.lie_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column lieu.lie_facteur_nb_trace is 'Facteur influant sur le nombre de trace';
comment on column lieu.lie_carto is 'À vrai si il s''agit d''un lieu carto';
comment on column lieu.lie_desc is 'Précision éventuelle sur la description du lieu';
comment on column lieu.lie_img_id is 'Éventuelle image associée';

comment on column secteur.sct_id is 'Identifiant';
comment on column secteur.sct_nom is 'Nom';
comment on column secteur.sct_description is 'Description destinée aux personnages se trouvant dans le secteur';
comment on column secteur.sct_tsc_id is 'Type de secteur';
comment on column secteur.sct_charge_max is 'Charge maximale supportée par le secteur (null signifie infini)';
comment on column secteur.sct_indice_population is 'Indice de densité de population';
comment on column secteur.sct_vhc_id is 'Identifiant de l''éventuel véhicule associé à un secteur';
comment on column secteur.sct_cout_pn is 'Cout journalier en pn';
comment on column secteur.sct_cout_pv is 'Cout journalier en PV';
comment on column secteur.sct_difficulte_rumeur is 'Difficulté de lancement d''une rumeur';

comment on table chemin is 'Lien entre 2 endroits (lieu <-> lieu ou lieu <->monde)';
comment on column chemin.chm_id is 'Identifiant';
comment on column chemin.chm_egn_id is 'Element generique servant de clé';
comment on column chemin.chm_tch_id is 'Type de chemin (d''acces)';
comment on column chemin.chm_pch_id is 'Éventuelle particularité associée à un chemin (champs textuel du style « par la fenêtre »,  « par la porte »). Permet notamment de différencier différents chemins venant du même endroit et allant au même endroit.';
comment on column chemin.chm_depart_lie_id is 'Lieu de départ';
comment on column chemin.chm_arrivee_lie_id is 'Lieu d''arrivée';
comment on column chemin.chm_desc_aller is 'Description du trajet dans le sens aller';
comment on column chemin.chm_desc_retour is 'Description du trajet dans le sens retour';
comment on column chemin.chm_aller is 'Trajet possible dans le sens aller (oui/non)';
comment on column chemin.chm_retour is 'Trajet possible dans le sens retour (oui/non)';
comment on column chemin.chm_ouvert is 'Trajet actuellement ouvert (oui/non) (prévaut sur chm_aller et chm_retour)';
comment on column chemin.chm_discretion is 'Discretion';
comment on column chemin.chm_difficulte is 'Seuil de difficulté';
comment on column chemin.chm_cout_pa is 'Cout du trajet en Points d''Action';
comment on column chemin.chm_cout_pn is 'Cout en point de nourriture';
comment on column chemin.chm_cout_echec_pv is 'Cout en PV en cas d''échec';
comment on column chemin.chm_indice_taille is 'Indice de taille du passage accordé par le chemin';
comment on column chemin.chm_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column chemin.chm_facteur_nb_trace is 'Facteur influant sur le nombre de traces';
comment on column chemin.chm_echec_bloquant is 'À vrai si l''échec empeche d''emprunter le chemin';
comment on column chemin.chm_crc_id is 'Caractéristique associée';
comment on column chemin.chm_crc_2_id is 'Éventuelle seconde caractéristique intervenant dans la formule';

comment on column saison.ssn_id is 'Identifiant';
comment on column saison.ssn_nom is 'Nom';
comment on column saison.ssn_ordre is 'Ordre pendant un cycle';
comment on column saison.ssn_default is 'Saison par défaut';

comment on column element.elm_id is 'Identifiant';
comment on column element.elm_egn_id is 'Identifiant de l''element generique associe';
comment on column element.elm_nom is 'Nom';
comment on column element.elm_desc is 'Description de l''element';
comment on column element.elm_prix is 'Prix de l''element en monnaie arbitraire';
comment on column element.elm_equipable is 'Definit si l''element peut etre equipe ou non';
comment on column element.elm_equipe is 'Definit si l''element est equipe ou non';
comment on column element.elm_enraye is 'Definit si l''element est enraye ou non';
comment on column element.elm_naturel is 'Definit si l''element est naturel chez son possesseur';
comment on column element.elm_prs_id is 'Identifiant de son possesseur';
comment on column element.elm_lie_id is 'Lieu ou a ete jete l''element';

comment on column monnaie.mnn_id is 'Identifiant de la monnaie';
comment on column monnaie.mnn_nom is 'Nom de la monnaie';
comment on column monnaie.mnn_indice is 'Indice arbitraire de la monnaie (voire cote)';

comment on table perso_monnaie is 'Monnaie que porte le personnage';
comment on column perso_monnaie.pmn_id is 'Identifiant abstrait';
comment on column perso_monnaie.pmn_prs_id is 'Identifiant du personnage';
comment on column perso_monnaie.pmn_mnn_id is 'Identifiant de la monnaie';
comment on column perso_monnaie.pmn_montant is 'Qte de monnaie du personnage';

comment on table compagnie is 'Compagnie de personnages';
comment on column compagnie.cmp_id is 'Identifiant de la compagnie';
comment on column compagnie.cmp_nom is 'Nom de la compagnie';

comment on table place_compagnie is 'Place des personnages dans la compagnie';
comment on column place_compagnie.plc_prs_id is 'Identifiant du personnage';
comment on column place_compagnie.plc_cmp_id is 'Identifiant de la compagnie';
comment on column place_compagnie.plc_place is 'Place du personnage ds la compagnie';

comment on column sexe.sex_id is 'Identifiant';
comment on column sexe.sex_nom is 'Nom du sexe (masculin, féminin, asexué, ...)';
comment on column sexe.sex_insc_ouverte is 'Sexe disponible pour l''inscription';

comment on column type_personnage.tpr_id is 'Identifiant';
comment on column type_personnage.tpr_nom is 'Nom du type (PJ, PNJ, familier, Gob, ...)';

comment on column type_maladie.tml_id is 'Identifiant';
comment on column type_maladie.tml_nom is 'Nom du type (classe, race, instantanée, définitive, temporaire)';

comment on column type_caracteristique.tcr_id is 'Identifiant';
comment on column type_caracteristique.tcr_nom is 'Nom du type (attribut, compétence)';

comment on column nature_caracteristique.ncr_id is 'Identifiant';
comment on column nature_caracteristique.ncr_nom is 'Nom de la nature (intellectuelle, physique, spirituelle, ...)';

comment on column type_secteur.tsc_id is 'Identifiant';
comment on column type_secteur.tsc_nom is 'Nom du type: immobile, mobile (monde), mobile (galaxie), mobile (véhicule)';

comment on column type_monde.tmn_id is 'Identifiant';
comment on column type_monde.tmn_nom is 'Nom du type: galaxie (sans appui au sol), planète (avec appui)';

comment on column type_chemin.tch_id is 'Identifiant';
comment on column type_chemin.tch_nom is 'Nom du type: classique, accès trappe, accès sas, accès pont';

comment on column action.act_id is 'Identifiant';
comment on column action.act_nom is 'Nom';
comment on column action.act_nom_form is 'Nom de l''action dans le formulaire';
comment on column action.act_desc is 'Description de l''action';
comment on column action.act_form_attaque is 'Formule a resoudre pour l''attaque';
comment on column action.act_form_defense is 'Formule a resoudre pour la defense';
comment on column action.act_form_cout_pa is 'Formule determinant le cout en PA';
comment on column action.act_form_cout_pn is 'Formule determinant le cout en PN';
comment on column action.act_form_cout_pp is 'Formule determinant le cout en PP';
comment on column action.act_form_cout_pv is 'Formule determinant le cout en PV';
comment on column action.act_form_cout_pc is 'Cout en point de combat de l''action';
comment on column action.act_form_gain_min is 'Calcul du gain minimum de Points d''''eXperience';
comment on column action.act_form_gain_max is 'Calcul du gain maximum de Points d''''eXperience';
comment on column action.act_courante is 'Action proposee en tout lieu/pour toute classe ? (oui/non)';
comment on column action.act_combat is 'À vrai si disponible en situation de combat';

comment on column lieu_action.lac_act_id is 'Identifiant de l''action spécialement considérée en ce lieu';
comment on column lieu_action.lac_lie_id is 'Identifiant du lieu ayant une action particuliere';
comment on column lieu_action.lac_possible is 'Action possible physiquement en ce lieu ? (oui/non) (rempli si différent du standard)';
comment on column lieu_action.lac_autorisee is 'Action autorisée (moralement) en ce lieu ? (oui/non) (rempli si différent du standard)';
comment on column lieu_action.lac_difficulte is 'Difficulte de l''action en ce lieu (rempli si différent du standard)';
comment on column lieu_action.lac_cout_pa is 'Coût en PA de l''action en ce lieu (rempli si différent du standard)';
comment on column lieu_action.lac_cout_pn is 'Cout en PN de l''action';
comment on column lieu_action.lac_cout_pp is 'Cout en PP de l''action';
comment on column lieu_action.lac_cout_pv is 'Cout en PV de l''action';

comment on column typelieu_action.tla_act_id is 'Identifiant de l''action spécialement considérée en ce type de lieu';
comment on column typelieu_action.tla_tli_id is 'Identifiant du type de lieu ayant une action particuliere';
comment on column typelieu_action.tla_possible is 'Action possible physiquement en ce type de lieu ? (oui/non) (rempli si différent du standard)';
comment on column typelieu_action.tla_autorisee is 'Action autorisée (moralement) en ce type de lieu ? (oui/non) (rempli si différent du standard)';
comment on column typelieu_action.tla_difficulte is 'Difficulte de l''action en ce type de lieu (rempli si différent du standard)';
comment on column typelieu_action.tla_cout_pa is 'Coût en PA de l''action en ce type de lieu (rempli si différent du standard)';
comment on column typelieu_action.tla_cout_pn is 'Coût en PN de l''action en ce type de lieu (rempli si différent du standard)';
comment on column typelieu_action.tla_cout_pp is 'Coût en PP de l''action en ce type de lieu (rempli si différent du standard)';
comment on column typelieu_action.tla_cout_pv is 'Coût en PV de l''action en ce type de lieu (rempli si différent du standard)';

comment on column type_lieu.tli_id is 'Identifiant';
comment on column type_lieu.tli_nom is 'Nom du type de lieu';

comment on column debit_element.dbt_id is 'Identifiant';
comment on column debit_element.dbt_lie_id is 'Identifiant du lieu';
comment on column debit_element.dbt_egn_id is 'Identifiant de l''élément générique créé dans ce lieu';

comment on column vente_element.vnt_id is 'Identifiant';
comment on column vente_element.vnt_lie_id is 'Identifiant du lieu';
comment on column vente_element.vnt_elm_id is 'Identifiant de l''élément vendu en ce lieu';

comment on column depot_element.dpt_id is 'Identifiant';
comment on column depot_element.dpt_lie_id is 'Identifiant du lieu';
comment on column depot_element.dpt_elm_id is 'Identifiant de l''élément déposé en ce lieu';
comment on column depot_element.dpt_prs_id is 'Identifiant du personnage possédant l''élement';

comment on column banque.bnq_id is 'Identifiant';
comment on column banque.bnq_lie_id is 'Identifiant du lieu';
comment on column banque.bnq_prs_id is 'Identifiant du personnage possédant l''argent';
comment on column banque.bnq_mnn_id is 'Identifiant de la monnaie déposée';
comment on column banque.bnq_somme is 'Somme d''argent déposé';

comment on column element_gen.egn_id is 'Identifiant';
comment on column element_gen.egn_nom is 'Nom de l''element generique';
comment on column element_gen.egn_desc is 'Description de l''element generique';
comment on column element_gen.egn_tel_id is 'Type d''element generique';
comment on column element_gen.egn_prix is 'Prix en monnaie arbitraire';
comment on column element_gen.egn_equipable is 'Definit si l''element peut etre equipe ou non';
comment on column element_gen.egn_possession_mld_id is 'Maladie liee a la simple possession de l''element';
comment on column element_gen.egn_equipe_mld_id is 'Maladie liee au fait d''equiper l''element';
comment on column element_gen.egn_tcn_id is 'Technologie de l''element';
comment on column element_gen.egn_mtr_id is 'Type de materiau';
comment on column element_gen.egn_taille is 'Taille de l''élément générique';

comment on column arme.arm_id is 'Identifiant';
comment on column arme.arm_agn_id is 'Identifiant de l''arme generique';
comment on column arme.arm_rch_id is 'Identifiant de la recharge actuelle';
comment on column arme.arm_elm_id is 'Identifiant de l''element';

comment on column arme_gen.agn_id is 'Identifiant';
comment on column arme_gen.agn_tdg_id is 'Type de degat';
comment on column arme_gen.agn_egn_id is 'Element generique associe';
comment on column arme_gen.agn_mld_id is 'Maladie donnee en cas de succes';
comment on column arme_gen.agn_portee is 'Portee de l''arme';
comment on column arme_gen.agn_degat_min is 'Degat minimum en cas de succes';
comment on column arme_gen.agn_degat_max is 'Degat max en cas de succes';
comment on column arme_gen.agn_deux_mains is 'Arme a deux mains ?';
comment on column arme_gen.agn_tmt_id is 'Type de munition a utiliser pour les recharges';
comment on column arme_gen.agn_carac1_crc_id is 'Première caractéristique';
comment on column arme_gen.agn_carac2_crc_id is 'Éventuelle seconde caracteristique';
comment on column arme_gen.agn_compet1_crc_id is 'Compétence associée à cette arme';
comment on column arme_gen.agn_compet2_crc_id is 'Éventuelle seconde compétence';
comment on column arme_gen.agn_bonus_attaque is 'Bonus à l''attaque';
comment on column arme_gen.agn_bonus_defense is 'Bonus à la défense';

comment on column matiere.mtr_id is 'Identifiant';
comment on column matiere.mtr_nom is 'Nom de la matiere';
comment on column matiere.mtr_desc is 'Description de la matiere';
comment on column matiere.mtr_resistance is 'Indice de resistance';

comment on column technologie.tcn_id is 'Identifiant';
comment on column technologie.tcn_nom is 'Nom de la technologie';
comment on column technologie.tcn_desc is 'Description de la technologie';
comment on column technologie.tcn_taux_enrayage is 'Taux d''enrayage de la technologie';

comment on column armure_gen.ugn_id is 'Identifiant';
comment on column armure_gen.ugn_egn_id is 'Element generique associe';
comment on column armure_gen.ugn_bonus_defense is 'Éventuel bonus donné à la defense par cette arme';

comment on column recharge_gen.cgn_id is 'Identifiant abstrait';
comment on column recharge_gen.cgn_egn_id is 'Identifiant de l''element dont il est issu';
comment on column recharge_gen.cgn_mnt_id is 'Munition contenue dans ce type de chargeur';
comment on column recharge_gen.cgn_nb_max_mnt is 'Nombre maximal de munitions contenues dans ce type de chargeur';

comment on column vaisseau_gen.vgn_id is 'Identifiant';
comment on column vaisseau_gen.vgn_egn_id is 'Identifiant de l''element generique';
comment on column vaisseau_gen.vgn_charge_max is 'Charge maximale contenue par ce modele de vaisseau';
comment on column vaisseau_gen.vgn_tvs_id is 'Type du vaisseau (terrestre, aeroglisseur, sous-marin, spatiale)';

comment on column type_vaisseau.tvs_id is 'Identifiant';
comment on column type_vaisseau.tvs_nom is 'Nom du type de vaisseau';

comment on column munition.mnt_id is 'Identifiant';
comment on column munition.mnt_nom is 'Nom de la munition';
comment on column munition.mnt_desc is 'Description de la munition';
comment on column munition.mnt_tmt_id is 'Type de munition';
comment on column munition.mnt_mld_id is 'Maladie associee a ce type de munition';

comment on column type_degat.tdg_id is 'Identifiant';
comment on column type_degat.tdg_nom is 'Nom de la munition';

comment on column armure_gen_degat.agd_id is 'Identifiant abstrait';
comment on column armure_gen_degat.agd_ugn_id is 'Identifiant de l''armure generique';
comment on column armure_gen_degat.agd_tdg_id is 'Type de degat encaisse';
comment on column armure_gen_degat.agd_encaissement_min is 'Encaissement minimal';
comment on column armure_gen_degat.agd_encaissement_max is 'Encaissement maximal';

comment on column armure_gen_zone.agz_id is 'Identifiant abstrait';
comment on column armure_gen_zone.agz_ugn_id is 'Identifiant de l''armure generique';
comment on column armure_gen_zone.agz_zon_id is 'Zone protegee';

comment on column zone_armure.zon_id is 'Identifiant de la zone corporelle';
comment on column zone_armure.zon_nom is 'Nom de la zone protegee';

comment on column vaisseau_gen_poste.vgp_id is 'Identifiant abstrait';
comment on column vaisseau_gen_poste.vgp_vgn_id is 'Vaisseau generique dans lequel est ce poste';
comment on column vaisseau_gen_poste.vgp_tpt_id is 'Type de poste';
comment on column vaisseau_gen_poste.vgp_nb_perso is 'Nombre de personnes qui doivent occuper le poste';

comment on column type_poste_tmp.tpt_id is 'Identifiant';
comment on column type_poste_tmp.tpt_nom is 'Nom du type de vaisseau';

comment on column type_element.tel_id is 'Identifiant';
comment on column type_element.tel_nom is 'Nom de la technologie';

comment on column armure.aru_id is 'Identifiant';
comment on column armure.aru_elm_id is 'Identifiant de l''element';

comment on column recharge.rch_id is 'Identifiant';
comment on column recharge.rch_elm_id is 'Identifiant de l''element';
comment on column recharge.rch_nb_mnt is 'Nb de munitions';
comment on column recharge.rch_utilise is 'Recharge utilisée dans une arme ?';

comment on column vaisseau.vss_id is 'Identifiant';
comment on column vaisseau.vss_elm_id is 'Identifiant de l''element';
comment on column vaisseau.vss_sct_id is 'Secteur representant le vaisseau';

comment on column type_classe.tcl_id is 'Identifiant';
comment on column type_classe.tcl_nom is 'Nom du type (classe, aptitude)';

comment on column interdit_classe.inc_id is 'Identifiant';
comment on column interdit_classe.inc_cls_id is 'Classe interdisant';
comment on column interdit_classe.inc_interdit_cls_id is 'Classe interdite';

comment on column prerequis_carac.pqa_id is 'Identifiant abstrait';
comment on column prerequis_carac.pqa_cln_id is 'Niveau de classe a atteindre';
comment on column prerequis_carac.pqa_form_carac is 'Formule a remplir pour avoir le niveau de classe';

comment on column taille.tll_id is 'Identifiant';
comment on column taille.tll_nom is 'Nom de la taille';
comment on column taille.tll_description is 'Description';
comment on column taille.tll_indice is 'Indice';

comment on column race.rac_id is 'Identifiant';
comment on column race.rac_nom is 'Nom';
comment on column race.rac_mld_id is 'Maladie (malus/bonus) apportée';
comment on column race.rac_desc is 'Description physique des représentants de cette race';
comment on column race.rac_monture is 'Peut porter des personnages (oui/non)';
comment on column race.rac_indice_taille is 'Indice de base de taille d''un membre de la race';
comment on column race.rac_insc_ouverte is 'Race disponible à l''inscription';

comment on table acces_connu is 'Chemin vers un secteur immobile connu par un personnage';
comment on column acces_connu.acc_id is 'Identifiant abstrait';
comment on column acces_connu.acc_chm_id is 'Identifiant du chemin connu vers un secteur immobile';
comment on column acces_connu.acc_prs_id is 'Identifiant du personnage';

comment on table monde_connu is 'Monde connu par un personnage';
comment on column monde_connu.mnc_id is 'Identifiant abstrait';
comment on column monde_connu.mnc_mnd_id is 'Identifiant du monde connu';
comment on column monde_connu.mnc_prs_id is 'Identifiant du personnage';

comment on column type_munition.tmt_id is 'Identifiant';
comment on column type_munition.tmt_nom is 'Nom du type de munition';

comment on table css is 'Styles CSS de l affichage web';
comment on column css.css_id is 'Identifiant';
comment on column css.css_nom is 'Nom du style';
comment on column css.css_defaut is 'Style par defaut du jeu';
comment on column css.css_chemin is 'Chemin du fichier CSS';

comment on table itineraire is 'Itineraires empruntes par les personnages';
comment on column itineraire.itn_id is 'Identifiant';
comment on column itineraire.itn_chm_id is 'Identifiant eventuel du chemin emprunté (deplacement lieu-lieu)';
comment on column itineraire.itn_frm_id is 'Identifiant de la forme du personnage';
comment on column itineraire.itn_depart_lie_id is 'Identifiant du lieu de depart';
comment on column itineraire.itn_arrivee_lie_id is 'Identifiant du lieu d''arrivee';
comment on column itineraire.itn_traces_initiales is 'Quantite de traces initialement laissees par le suivi';
comment on column itineraire.itn_traces_restantes is 'Quantite de traces restantes.';
comment on column itineraire.itn_date is 'Date du deplacement.';
comment on column itineraire.itn_index_effacement is 'Nombre de traces qui vont disparaître à la prochaine "remise générale"';
comment on column itineraire.itn_ttr_id is 'Identifiant du type de trace';
comment on column itineraire.itn_profond_trace is 'Profondeur de la trace';
comment on column itineraire.itn_taille_trace is 'Pointure de la trace';

comment on column type_filature.tfl_id is 'Identifiant';
comment on column type_filature.tfl_nom is 'Nom du type: filature (voleur, de visu), pistage (par trace), suite (pour guide, volontaire)';

comment on table mime is 'Table de mimes (actions gestuelles)';
comment on column mime.mim_id is 'Identifiant';
comment on column mime.mim_nom is 'Nom du mime';
comment on column mime.mim_label is 'Chaine de caracteres symbolisant le mime';
comment on column mime.mim_tmm_id is 'Type de mime';

comment on column type_mime.tmm_id is 'Identifiant';
comment on column type_mime.tmm_nom is 'Nom du type: simple, lié à un personnage';

comment on table rumeur is 'Tables des rumeurs';
comment on column rumeur.rmr_id is 'Identifiant';
comment on column rumeur.rmr_nom is 'Nom de la rumeur';
comment on column rumeur.rmr_prs_id is 'Identifiant du personnage a l''origine de la rumeur';
comment on column rumeur.rmr_content is 'Contenu de la rumeur';
comment on column rumeur.rmr_lng_id is 'Langue de la rumeur';
comment on column rumeur.rmr_lie_id is 'Lieu d''origine de la rumeur';
comment on column rumeur.rmr_rayon is 'Rayon de la rumeur (pour les lieux géographiques)';
comment on column rumeur.rmr_sct_id is 'Éventuel secteur d''influence si la rumeur a été lancée depuis un lieu topologique';
comment on column rumeur.rmr_diff is 'Difficulté pour découvrir la rumeur';
comment on column rumeur.rmr_date_debut is 'Date d''apparition de la rumeur';
comment on column rumeur.rmr_date_fin is 'Date de fin de la rumeur';

comment on table rumeur_ecoute is 'Tables des ecoutes de rumeurs';
comment on column rumeur_ecoute.rmc_id is 'Identifiant';
comment on column rumeur_ecoute.rmc_prs_id is 'Identifiant du personnage ayant entendu la rumeur';
comment on column rumeur_ecoute.rmc_rmr_id is 'Identifiant de la rumeur';

comment on table langue is 'Langue';
comment on column langue.lng_id is 'Identifiant';
comment on column langue.lng_nom is 'Nom';
comment on column langue.lng_description is 'Description de son histoire, son utilisation, ...';

comment on table filature is 'Tables des filatures, pistages, guides';
comment on column filature.flt_id is 'Identifiant';
comment on column filature.flt_suivant_prs_id is 'Identifiant du personnage qui fait l''action de suivre.';
comment on column filature.flt_suivi_frm_id is 'Identifiant de la forme suivie.';
comment on column filature.flt_tfl_id is 'Type de filature';
comment on column filature.flt_debut is 'Date de début de la filature';

comment on table type_trace is 'Types de trace disponibles';
comment on column type_trace.ttr_id is 'Identifiant abstrait';
comment on column type_trace.ttr_nom is 'Nom de la trace';

comment on table trace_connue is 'Traces connues par les personnages';
comment on column trace_connue.trc_id is 'Identifiant';
comment on column trace_connue.trc_ttr_id is 'Identifiant du type de trace';
comment on column trace_connue.trc_taille is 'Pointure';
comment on column trace_connue.trc_profondeur is 'Profondeur';
comment on column trace_connue.trc_frm_trace_id is 'Identifiant de la forme qui a laissé les traces';
comment on column trace_connue.trc_prs_id is 'Identifiant du personnage qui a découvert les traces';
comment on column trace_connue.trc_date_fin is 'Date jusqu''à laquelle on se souviendra de ces traces';
comment on column trace_connue.trc_nom_donne is 'Nom donné à ces traces';

comment on column lieu_carto.lic_id is 'Identifiant';
comment on column lieu_carto.lic_lie_id is 'Identifiant du leiu associé';
comment on column lieu_carto.lic_mnd_id is 'Identifiant du monde associé';
comment on column lieu_carto.lic_ssn_id is 'Saison du lieu';

comment on table cycle is 'Succession de periodes ou de cycles';
comment on column cycle.ccl_id is 'Identifiant';
comment on column cycle.ccl_nom is 'Nom';
comment on column cycle.ccl_nombre_periode is 'Nombre de periodes du cycle';
comment on column cycle.ccl_duree_periode is 'Duree d une periode en heure';
comment on column cycle.ccl_duree_nombre_cycles is 'La periode du cycle correspond a n autres cycles';
comment on column cycle.ccl_duree_ccl_id is 'Identifiant du cycle etalon';
comment on column cycle.ccl_duree_nombre_cycles_ecoules is 'Nombre de cycles ecoules';
comment on column cycle.ccl_pourcentage_alea is 'Alea eventuel: 0% pour perfection; 100% pour un alea complet';

comment on table cycle_monde is 'Rattachement de cycles aux mondes';
comment on column cycle_monde.cmn_id is 'Identifiant';
comment on column cycle_monde.cmn_mnd_id is 'Identifiant du monde associé';
comment on column cycle_monde.cmn_ccl_id is 'Identifiant du cycle associé';

comment on table cycle_secteur is 'Rattachement de cycles aux secteurs';
comment on column cycle_secteur.csc_id is 'Identifiant';
comment on column cycle_secteur.csc_sct_id is 'Identifiant du secteur associé';
comment on column cycle_secteur.csc_ccl_id is 'Identifiant du cycle associé';

comment on table periode is 'Detail d une periode d un cycle';
comment on column periode.prd_id is 'Identifiant';
comment on column periode.prd_nom is 'Nom';
comment on column periode.prd_ccl_id is 'Cycle correspondant';
comment on column periode.prd_min is 'Minimum de periode';
comment on column periode.prd_max is 'Maximum de periode';
comment on column periode.prd_complement_description is 'Complement de description de la periode';

comment on table lieu_cycle is 'Rattachement de lieux aux cycles';
comment on column lieu_cycle.lcd_id is 'Identifiant';
comment on column lieu_cycle.lcd_ccl_id is 'Identifiant du cycle associé';
comment on column lieu_cycle.lcd_lie_id is 'Identifiant du lieu associé';

comment on table lieu_cyclique is 'Aspect complémentaire d un lieu à un cycle donné';
comment on column lieu_cyclique.lcc_id is 'Identifiant';
comment on column lieu_cyclique.lcc_prd_id is 'Identifiant de la période associée';
comment on column lieu_cyclique.lcc_lie_id is 'Identifiant du lieu associé';
comment on column lieu_cyclique.lcc_description is 'Description complémentaire à cette période donnée';

comment on table calque is 'Calque géographique représentant selon : le relief, le climat, les voieries, etc.';
comment on column calque.clq_id is 'Identifiant';
comment on column calque.clq_nom is 'Nom';
comment on column calque.clq_description is 'Description';
comment on column calque.clq_indice_population is 'Indice de densité de population';
comment on column calque.clq_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column calque.clq_facteur_nb_trace is 'Facteur influant sur le nombre de traces';
comment on column calque.clq_cout_pn is 'Cout journalier en pn';
comment on column calque.clq_cout_acces_pn is 'Cout d''accès en pn';
comment on column calque.clq_cout_pv is 'Cout journalier en point de vie';
comment on column calque.clq_cout_acces_pa is 'Cout d''accès en point d''action';
comment on column calque.clq_cout_echec_pv is 'Cout en pv en cas d''échec';
comment on column calque.clq_couleur is 'Code couleur associé';
comment on column calque.clq_tcq_id is 'Type de calque';
comment on column calque.clq_id_externe is 'Identifiant externe à l''application utilisé lors des imports';
comment on column calque.clq_difficulte is 'Formule de difficulté de déplacement dans ces zones et de fait facilité à se perdre';
comment on column calque.clq_crc_id is 'Compétence associée';
comment on column calque.clq_crc2_crc_id is 'Éventuelle deuxième caractéristique associée';
comment on column calque.clq_echec_bloquant is 'Si à vrai, échouer sur son déplacement ne fait pas progresser';

comment on table type_calque is 'Type de calque';
comment on column type_calque.tcq_id is 'Identifiant';
comment on column type_calque.tcq_nom is 'Nom du type de calque';
comment on column type_calque.tcq_principal is 'Un type de calque est désigné comme le type de calque principal. Le label de base sera prit sur le calque correspondant au type de calque principal.';

comment on table calque_lieu_carto is 'Rattachement de calques aux lieux carto';
comment on column calque_lieu_carto.clc_id is 'Identifiant';
comment on column calque_lieu_carto.clc_clq_id is 'Identifiant du calque associé';
comment on column calque_lieu_carto.clc_lic_id is 'Identifiant du lieu carto associé';

comment on table type_vehicule is 'Type de vehicule';
comment on column type_vehicule.tvh_id is 'Identifiant';
comment on column type_vehicule.tvh_nom is 'Nom du type de véhicule';
comment on column type_vehicule.tvh_carburant_egn_id is 'Type de carburant employé';
comment on column type_vehicule.tvh_puissance is 'Puissance developpée lors d''un déplacement';
comment on column type_vehicule.tvh_consommation is 'Nombre d''unité de carburant utilisé par tour';
comment on column type_vehicule.tvh_usure_max is 'Maximum de point d''usure pour un vehicule réparé ou neuf';
comment on column type_vehicule.tvh_usure is 'Point d''usure gagné à chaque tour lorsque en marche';

comment on table vent_lieu_carto is 'Rattachement de vents aux lieux carto';
comment on column vent_lieu_carto.cvt_id is 'Identifiant';
comment on column vent_lieu_carto.cvt_tvt_id is 'Identifiant du type de vent associé';
comment on column vent_lieu_carto.cvt_lic_id is 'Identifiant du lieu carto associé';
comment on column vent_lieu_carto.cvt_force is 'Force du vent';

comment on table restriction_calque is 'Restriction d un type de vehicule à un calque en particulier';
comment on column restriction_calque.rtc_id is 'Identifiant';
comment on column restriction_calque.rtc_tvh_id is 'Identifiant du type de véhicule';
comment on column restriction_calque.rtc_clq_id is 'Identifiant du calque de restriction';
comment on column restriction_calque.rtc_rendement is 'Rendement associé à ce type de terrain';

comment on table influence_vent is 'Vent influençant les déplacements d un type de vehicule';
comment on column influence_vent.inv_id is 'Identifiant';
comment on column influence_vent.inv_tvh_id is 'Identifiant du type de véhicule';
comment on column influence_vent.inv_tvt_id is 'Identifiant du type de vent';
comment on column influence_vent.inv_puissance is 'Puissance associée à ce moyen de propulsion';

comment on table vehicule is 'Véhicule';
comment on column vehicule.vhc_id is 'Identifiant';
comment on column vehicule.vhc_tvh_id is 'Identifiant du type de véhicule';
comment on column vehicule.vhc_reservoir is 'Maximum de ressource pouvant être stocké';
comment on column vehicule.vhc_reservoir_actuel is 'Niveau de ressource actuel dans le réservoir';
comment on column vehicule.vhc_usure_actuelle is 'Niveau d''usure actuel';
comment on column vehicule.vhc_amarre is 'À vrai si le véhicule est amarré';
comment on column vehicule.vhc_moteur_fonctionnel is 'À vrai si le moteur est en route';
comment on column vehicule.vhc_masse is 'Masse à déplacer';
comment on column vehicule.vhc_bride_moteur is 'Pourcentage de puissance laissée pour le moteur';
comment on column vehicule.vhc_bride_vent is 'Pourcentage de puissance laissée pour la propulsion passive';
comment on column vehicule.vhc_direction is 'Direction souhaitée';
comment on column vehicule.vhc_date_remise is 'Date de remise';
comment on column vehicule.vhc_reserve_nourriture is 'Réserve de nourriture max dans le véhicule';
comment on column vehicule.vhc_reserve_nourriture_actuel is 'Niveau actuel de la réserve de nourriture';

comment on table type_vent is 'Type de vent';
comment on column type_vent.tvt_id is 'Identifiant';
comment on column type_vent.tvt_nom is 'Nom du type de vent';

comment on table type_poste is 'Type de poste';
comment on column type_poste.tps_id is 'Identifiant';
comment on column type_poste.tps_nom is 'Nom du type de poste';
comment on column type_poste.tps_rendement_min is 'Rendement minimum d''un poste';
comment on column type_poste.tps_propulsion_active is 'Poste pour une propulsion active ?';
comment on column type_poste.tps_formule_difficulte is 'Formule de difficulté de tenue du poste';

comment on table poste is 'Poste en place sur un véhicule';
comment on column poste.pst_id is 'Identifiant';
comment on column poste.pst_tps_id is 'Identifiant du type de poste';
comment on column poste.pst_prs_id is 'Identifiant de l''éventuelle personne assurant le poste';
comment on column poste.pst_vhc_id is 'Identifiant du véhicule associé';

comment on table type_vehicule_poste is 'Type poste en place sur un type de véhicule';
comment on column type_vehicule_poste.ttp_id is 'Identifiant';
comment on column type_vehicule_poste.ttp_tvh_id is 'Identifiant du type de véhicule';
comment on column type_vehicule_poste.ttp_tps_id is 'Identifiant du type de poste';

comment on table monture is 'Monture (cheval, éléphant, moto...)';
comment on column monture.mon_id is 'Identifiant de la monture';
comment on column monture.mon_prs_id is 'Personnage associé à la monture';
comment on column monture.mon_cavalier_prs_id is 'Éventuel identifiant du cavalier';
comment on column monture.mon_clef_elm_id is 'Éventuelle clé nécessaire pour faire fonctionner la monture';
comment on column monture.mon_vivante is 'Indique si une créature est vivante ou non (et notamment si elle consomme des PNs à l''arrêt)';
comment on column monture.mon_egn_id is 'Éventuel type de carburant à associer à la monture';
comment on column monture.mon_consomation is 'Consomation par case franchie';
comment on column monture.mon_usure is 'Dans le cas de monture inanimée, usure (en « PV ») par déplacement';
comment on column monture.mon_surplus_cout_pn is 'Éventuel surplus de cout de PN en charge';
comment on column monture.mon_formule_difficulte is 'Formule de maitrise de la monture';

comment on table particularite_chemin is 'Particularité associée à un chemin (champs textuel du style « par la fenêtre »,  « par la porte »)';
comment on column particularite_chemin.pch_id is 'Identifiant';
comment on column particularite_chemin.pch_nom is 'Nom de la particularité';

comment on table description_lieu is 'Description plus précises de certains lieux';
comment on column description_lieu.dsl_id is 'Identifiant';
comment on column description_lieu.dsl_lie_id is '	';
comment on column description_lieu.dsl_difficulte is 'Difficulté associée à la connaissance de cette description';
comment on column description_lieu.dsl_description is 'Complément de description';

comment on table classeniveau_action is 'Limitation des actions pour certains niveaux de certaines classes';
comment on column classeniveau_action.cna_id is 'Identifiant';
comment on column classeniveau_action.cna_act_id is 'Identifiant de l''action spécialement considérée pour cette classe';
comment on column classeniveau_action.cna_cln_id is 'Identifiant du niveau de classe autorisé';
comment on column classeniveau_action.cna_autorise is 'Action autorisée (True) ou interdite (False) spécifiquement pour cette classe';

comment on table engagement is 'Engagement entre personnage';
comment on column engagement.eng_id is 'Identifiant';
comment on column engagement.eng_prs1_prs_id is 'Identifiant du premier personnage impliqué dans l''engagement';
comment on column engagement.eng_prs2_prs_id is 'Identifiant du deuxième personnage impliqué dans l''engagement';
comment on column engagement.eng_init_1 is 'À vrai si le premier personnage a l''initiative sinon l''initiative appartient au second personnage';
comment on column engagement.eng_lie_id is 'Lieu où se passe la confrontation';
comment on column engagement.eng_distant is 'À vrai si engagement distant sinon engagement de corps à corps';

comment on table coffre is 'Conteneur d''objet';
comment on column coffre.cff_id is 'Identifiant';
comment on column coffre.cff_elm_id is 'Identifiant de l''élément associé';
comment on column coffre.cff_clef_egn_id is 'Clef générique du coffre';
comment on column coffre.cff_taille is 'Indice de la contenance de ce coffre';
comment on column coffre.cff_ouvert is 'État ouvert/fermé';

comment on column coffre_element.cel_id is 'Identifiant';
comment on column coffre_element.cel_cff_id is 'Coffre associé';
comment on column coffre_element.cel_elm_id is 'Élément associé';
comment on column coffre_element.cel_prix is 'Éventuel prix associé si un magasin est associé au coffre';
comment on column coffre_element.cel_mnn_id is 'Monnaie associée';

comment on table element_visible is 'Éléments visible pour un personnage';
comment on column element_visible.elv_id is 'Identifiant';
comment on column element_visible.elv_prs_id is 'Personnage associé';
comment on column element_visible.elv_elm_id is 'Élément associé';

comment on table image is 'Image';
comment on column image.img_id is 'Identifiant';
comment on column image.img_nom is 'Nom associé';
comment on column image.img_adresse is 'Adresse relative de l''image';

comment on table boutique is 'Boutique';
comment on column boutique.btq_id is 'Identifiant';
comment on column boutique.btq_cff_id is 'Identifiant du coffre associé';
comment on column boutique.btq_nom is 'Nom de la boutique';
comment on column boutique.btq_description is 'Éventuelle description associée à la boutique';
comment on column boutique.btq_prs_id is 'Éventuel gérant de la boutique';
comment on column boutique.btq_atm_id is 'Éventuel automate associé';
comment on column boutique.btq_ouverte is 'À vrai si la boutique est ouverte';

comment on table automate is 'Automate similant un personnage. Des caractéristiques lui sont attribuées.';
comment on column automate.atm_id is 'Identifiant';
comment on column automate.atm_nom is 'Éventuel nom associé';

comment on table element_gen_connu is 'Éléments génériques identifiés par un personnage';
comment on column element_gen_connu.egc_id is 'Identifiant';
comment on column element_gen_connu.egc_egn_id is 'Élément générique associé';
comment on column element_gen_connu.egc_prs_id is 'Personnage associé';

comment on table automate_carac is 'Caractéristiques associées à un automate';
comment on column automate_carac.acr_id is 'Identifiant abstrait';
comment on column automate_carac.acr_atm_id is 'Automate associé';
comment on column automate_carac.acr_crc_id is 'Caracteristique associée';
comment on column automate_carac.acr_score is 'Score dans la caractéristique.';

comment on table achat_element_gen is 'Éléments génériques achetables par une boutique donnée';
comment on column achat_element_gen.aeg_id is 'Identifiant';
comment on column achat_element_gen.aeg_egn_id is 'Élément générique associé';
comment on column achat_element_gen.aeg_btq_id is 'Boutique associée';
comment on column achat_element_gen.aeg_prix is 'Prix de base';
comment on column achat_element_gen.aeg_mnn_id is 'Monnaie associée au prix';
comment on column achat_element_gen.aeg_nombre_max is 'Nombre maximum d''éléments achetables';

comment on table type_environnement is 'Environnement d''un secteur';
comment on column type_environnement.tev_id is 'Identifiant';
comment on column type_environnement.tev_nom is 'Nom de l''environnement';
comment on column type_environnement.tev_crc_id is 'Caractéristique associée';

comment on table profil_annonce is 'Profil mis à disposition';
comment on column profil_annonce.pro_id is 'Identifiant';
comment on column profil_annonce.pro_nom is 'Nom';
comment on column profil_annonce.pro_statut is 'Entier pointant vers des statuts';
comment on column profil_annonce.pro_ctp_id is 'Catégorie de profil';
comment on column profil_annonce.pro_mji_id is 'MJ associé';
comment on column profil_annonce.pro_desc is 'Description';
comment on column profil_annonce.pro_debat is 'Discussion entre MJs';
comment on column profil_annonce.pro_nbpersos is 'Nombre de personnages à pourvoir';
comment on column profil_annonce.pro_historique is 'Historique';
comment on column profil_annonce.pro_demandeaction is 'Demande d''action sur un profil';

comment on table news is 'Nouvelles';
comment on column news.new_id is 'Identifiant';
comment on column news.new_titre is 'Titre de la nouvelle';
comment on column news.new_txt is 'Texte de la nouvelle';
comment on column news.new_date is 'Date de publication';
comment on column news.new_dateedit is 'Date de dernière modification';
comment on column news.new_mji_id is 'MJ associé';

comment on table categorie_profil is 'Catégories des profils';
comment on column categorie_profil.ctp_id is 'Identifiant';
comment on column categorie_profil.ctp_nom is 'Nom';

comment on table candidature is 'Candidature';
comment on column candidature.can_id is 'Identifiant';
comment on column candidature.can_jou_id is 'Joueur associé';
comment on column candidature.can_pro_id is 'Profil associé';
comment on column candidature.can_statut is 'Statut de la candidature';
comment on column candidature.can_nomperso is 'Nom du personnage';
comment on column candidature.can_rac_id is 'Race associée';
comment on column candidature.can_cls_id is 'Classe associée';
comment on column candidature.can_sex_id is 'Sexe associé';
comment on column candidature.can_descriptionperso is 'Description du personnage';
comment on column candidature.can_backgroundperso is 'Background du personnage';
comment on column candidature.can_butsperso is 'Buts du personnage';
comment on column candidature.can_debatjoumj is 'Débat entre le recruteur et le joueur';
comment on column candidature.can_debatmj is 'Débat entre le recruteur et le MJ qui a proposé le profil';
comment on column candidature.can_historique is 'Historique de la candidature';
comment on column candidature.can_commentaire is 'Pense-bête pour le recruteur';
comment on column candidature.can_datemodification is 'Date de dernière modification';

comment on table question is 'Question du quizz';
comment on column question.que_id is 'Identifiant';
comment on column question.que_intitule is 'Intitulé de la question';

comment on table reponse is 'Proposition de réponse à une question du quizz';
comment on column reponse.rep_id is 'Identifiant';
comment on column reponse.rep_que_id is 'Question associée';
comment on column reponse.rep_intitule is 'Intitulé de la réponse';
comment on column reponse.rep_bonne is 'À vrai si la réponse est bonne';

comment on column profil_classe.pcl_id is 'Identifiant abstrait';
comment on column profil_classe.pcl_pro_id is 'Profil associé';
comment on column profil_classe.pcl_cls_id is 'Classe associée';

comment on column profil_race.pra_id is 'Identifiant abstrait';
comment on column profil_race.pra_pro_id is 'Profil associé';
comment on column profil_race.pra_rac_id is 'Race associée';

comment on column profil_sexe.psx_id is 'Identifiant abstrait';
comment on column profil_sexe.psx_pro_id is 'Profil associé';
comment on column profil_sexe.psx_sex_id is 'Sexe associé';



-- Generated SQL Views
-- --------------------------------------------------------------------



-- ProcÃ©dure
-- Retour    : le score aprÃšs calcul (intervention des maladies...)
-- Parametre : score avant prise en compte des maladies
-- Retour	 : score aprÃšs traitement
CREATE OR REPLACE FUNCTION calcul_score(integer) RETURNS integer AS '
BEGIN
    RETURN $1/128;
END;
' LANGUAGE plpgsql;


-- Trigger
-- Un seul personnage d un joueur peut avoir le flag prs_principal Ã  1
CREATE OR REPLACE FUNCTION trg_prs_principal() RETURNS trigger AS '
DECLARE
	v_prs_id	    personnage.prs_id%TYPE;
	v_prs_nom	    personnage.prs_nom_originel%TYPE;
	v_jou_pseudo	joueur.jou_pseudo%TYPE;
BEGIN

	-- Recherche du personnage principal (autre que celui-ci)
	SELECT prs_id, prs_nom_originel, jou_pseudo
		INTO v_prs_id, v_prs_nom, v_jou_pseudo
	FROM personnage, joueur
	WHERE	prs_principal   = true
		AND prs_id    != NEW.prs_id
		AND prs_jou_id = NEW.prs_jou_id
		AND jou_id     = NEW.prs_jou_id;

	-- S il en existe un et qu on essaie de mettre un deuxiÃšme principal, l ancien est rÃ©trogradÃ© :)
	IF (FOUND AND NEW.prs_principal = true) THEN
		RAISE NOTICE ''Le personnage % (id=%) est maintenant principal pour le joueur % (id=%).'', NEW.prs_nom_originel, NEW.prs_id, v_jou_pseudo, NEW.prs_jou_id;

		-- Mise a jour du personnage principal
		UPDATE	personnage
		SET	prs_principal = false
		WHERE	prs_id  = v_prs_id;


	-- Sinon, si pas d autre perso principal et celui-ci non plus, on le force
	ELSIF (NOT FOUND AND NEW.prs_principal = false) THEN

		SELECT jou_pseudo
			INTO v_jou_pseudo
		FROM joueur
		WHERE	jou_id = NEW.prs_jou_id;
		
		RAISE NOTICE ''Le personnage % (id=%) est force principal pour le joueur % (id=%).'', NEW.prs_nom_originel, NEW.prs_id, v_jou_pseudo, NEW.prs_jou_id;
		
		-- La forme insÃ©rÃ©e/updatÃ©e est forcÃ©e par defaut
		UPDATE	personnage
		SET	prs_principal = true
		WHERE	prs_id  = NEW.prs_id;
	END IF;
	RETURN NEW;
END;
' LANGUAGE plpgsql;




-- Une suppression d un personnage contrÃŽle que ce n est pas le personnage principal.
CREATE OR REPLACE FUNCTION trg_prs_principal_del() RETURNS trigger AS '
BEGIN
	IF (OLD.prs_principal = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer le personnage principal !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;





-- Trigger
-- A un instant donnÃ©, un perso a UNE et UNE SEULE forme. Prendre une forme revient Ã  quitter une ancienne.
CREATE OR REPLACE FUNCTION trg_frm_actuelle() RETURNS trigger AS '
DECLARE
	v_frm_id	forme.frm_id%TYPE;
	v_prs_id	personnage.prs_id%TYPE;
	v_prs_nom	personnage.prs_nom_originel%TYPE;

BEGIN
	-- Recherche de la forme actuelle
	SELECT frm_prs_id, prs_nom_originel, frm_id
		INTO v_prs_id, v_prs_nom, v_frm_id
	FROM personnage, forme
	WHERE	frm_actuelle   = true
		AND frm_id    != NEW.frm_id
		AND frm_prs_id = NEW.frm_prs_id
		AND prs_id     = NEW.frm_prs_id;

	IF (FOUND AND NEW.frm_actuelle = true) THEN
		-- Mise a jour de la forme actuelle
		UPDATE	forme
		SET	frm_actuelle = false
		WHERE	frm_id  = v_frm_id;

		RAISE NOTICE ''La forme id=% est maintenant la forme actuelle du personnage % (id=%).'', NEW.frm_id, v_prs_nom, v_prs_id;
	ELSIF (NOT FOUND AND NEW.frm_actuelle = false) THEN

		SELECT prs_id, prs_nom_originel
			INTO v_prs_id, v_prs_nom
		FROM personnage
		WHERE	prs_id = NEW.frm_prs_id;
		
		RAISE NOTICE ''La forme id=% est forcee comme forme actuelle du personnage % (id=%).'', NEW.frm_id, v_prs_nom, v_prs_id;
		
		-- La forme insÃ©rÃ©e/updatÃ©e est forcÃ©e par defaut
		UPDATE	forme
		SET	frm_actuelle = true
		WHERE	frm_id  = NEW.frm_id;
	END IF;
	RETURN NEW;
END;
' LANGUAGE plpgsql;

-- Une suppression d une forme contrÃŽle que ce n est pas la forme actuelle
CREATE OR REPLACE FUNCTION trg_frm_actuelle_del() RETURNS trigger AS '
BEGIN
	IF (OLD.frm_actuelle = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer la forme actuelle !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;







-- Une suppression d un lieu saisonnier contrÃŽle que ce n est pas le lieu saisonnier par dÃ©faut
CREATE OR REPLACE FUNCTION trg_lis_defaut_del() RETURNS trigger AS '
BEGIN
	IF (OLD.frm_actuelle = true) THEN
		RAISE EXCEPTION ''On ne peut pas supprimer le lieu saisonnier par dÃ©faut  !'';
	END IF;
	
	RETURN OLD;
END;
' LANGUAGE plpgsql;





-- Un personnage ne peut avoir qu un niveau par classe. Leur nombre est egalement limitÃ© ?
CREATE OR REPLACE FUNCTION trg_pcl_classe() RETURNS trigger AS '
DECLARE
	v_cls_id	classe.cls_id%TYPE;
	v_cls_nom	classe.cls_nom%TYPE;
	v_cln_niveau	classe_niveau.cln_niveau%TYPE;
	v_prs_nom	personnage.prs_nom_originel%TYPE;
BEGIN
	SELECT cl1.cln_niveau, prs_nom_originel, cls_id, cls_nom
		INTO v_cln_niveau, v_prs_nom, v_cls_id, v_cls_nom
	FROM perso_classe, classe_niveau cl1, classe_niveau cl2, personnage, classe
	WHERE		pcl_cln_id     = cl1.cln_id
		AND	pcl_prs_id     = NEW.pcl_prs_id
		AND	pcl_id        != NEW.pcl_id
		AND	cl2.cln_id     = NEW.pcl_cln_id
		AND	cl2.cln_cls_id = cl1.cln_cls_id
		AND	prs_id         = pcl_prs_id
		AND	cls_id         = cl1.cln_cls_id;

	IF FOUND THEN
		RAISE EXCEPTION ''Le personnage % (id=%) a deja le niveau % de la classe % (id=%)'', v_prs_nom, NEW.pcl_prs_id, v_cln_niveau, v_cls_nom, v_cls_id;
	END IF;

	RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Mettre a jour les scores
CREATE OR REPLACE FUNCTION trg_pcr_update_score() RETURNS trigger AS '
BEGIN
		-- si les XP ont bougÃ© on recalcule les scores
		IF NEW.pcr_score_xp != OLD.pcr_score_xp THEN
			NEW.pcr_score_calcule := calcul_score(NEW.pcr_score_xp);
		END IF;
		RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Apres une insertion d'une carac (table "caracteristique"), il faut modifier la table perso_carac (pour 1 personnage, il a un score dans CHAQUE caracteristique)
CREATE OR REPLACE FUNCTION trg_pcr_insert_carac() RETURNS trigger AS $$
DECLARE
	c_perso		CURSOR FOR SELECT prs_id FROM personnage;
	v_prs_id	personnage.prs_id%TYPE;
BEGIN

	-- Si une carac est rajoutee, on rajoute une ligne dans perso_carac pour chaque personnage.
	RAISE NOTICE 'trg_pcr_insert_carac : Ajout de caracteristique "% (%)" => Ajout de lignes dans perso_carac', NEW.crc_nom, NEW.crc_id;
	OPEN c_perso;

	WHILE 1 LOOP
		FETCH c_perso INTO v_prs_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		
		INSERT INTO perso_carac (pcr_prs_id, pcr_crc_id)
			VALUES (v_prs_id, NEW.crc_id);
			
	END LOOP;

	CLOSE c_perso;

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;





-- Apres une insertion d'un personnage (table "personnage"), il faut modifier la table perso_carac (chaque personnage a un score dans CHAQUE caracteristique)
CREATE OR REPLACE FUNCTION trg_pcr_insert_perso() RETURNS trigger AS '
DECLARE
	c_carac		CURSOR FOR SELECT crc_id FROM caracteristique;
	v_crc_id	caracteristique.crc_id%TYPE;
BEGIN

	-- Si un personnage est rajoute, on rajoute une ligne dans perso_carac pour chaque caracteristique.
	RAISE NOTICE ''trg_pcr_insert_perso : Ajout de personnage "% (%)" => Ajout de lignes dans perso_carac'', NEW.prs_nom_originel, NEW.prs_id;
	OPEN c_carac;

	WHILE 1 LOOP
		FETCH c_carac INTO v_crc_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		
		INSERT INTO perso_carac (pcr_prs_id, pcr_crc_id)
			VALUES (NEW.prs_id, v_crc_id);
			
	END LOOP;

	CLOSE c_carac;

	RETURN NEW;
END;
' LANGUAGE plpgsql;





-- Lors d'une insertion d'un joueur, il faut lui mettre un style css par defaut (le premier qui est trouve)
CREATE OR REPLACE FUNCTION trg_jou_css_defaut() RETURNS trigger AS '
DECLARE
	c_css		CURSOR FOR SELECT css_nom, css_id FROM css;
	v_css_id	css.css_id%TYPE;
	v_css_nom	css.css_nom%TYPE;
BEGIN

	-- Si aucun style CSS n est defini pour le joueur, on prend le premier qu on trouve
	IF NEW.jou_css_id IS NULL THEN
		OPEN c_css;

		FETCH c_css INTO v_css_nom, v_css_id;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		CLOSE c_css;

		RAISE NOTICE ''trg_jou_css_defaut : Ajout du joueur "% (%)" sans CSS => Attribution du style "% (%)"'', NEW.jou_pseudo, NEW.jou_id, v_css_nom, v_css_id;
		NEW.jou_css_id = v_css_id;
		
	END IF;

	RETURN NEW;
END;
' LANGUAGE plpgsql;


-- Special statements for postgres:post databases
-- Un perso a UNE et UNE seule forme
CREATE TRIGGER trg_frm_actuelle 
 AFTER INSERT OR UPDATE ON forme 
 FOR EACH ROW EXECUTE PROCEDURE trg_frm_actuelle();
CREATE TRIGGER trg_frm_actuelle_del
 BEFORE DELETE ON forme
 FOR EACH ROW EXECUTE PROCEDURE trg_frm_actuelle_del();

-- Special statements for postgres:post databases
-- Un joueur a UN personnage principal
CREATE TRIGGER trg_prs_principal 
 AFTER INSERT OR UPDATE ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_prs_principal();
CREATE TRIGGER trg_prs_principal_del
 BEFORE DELETE ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_prs_principal_del();

-- Special statements for postgres:post databases
-- Un et un seul lieu saisonnier par lieu
--CREATE TRIGGER trg_lis_defaut
-- AFTER INSERT OR UPDATE ON lieu_saisonnier
-- FOR EACH ROW EXECUTE PROCEDURE trg_lis_defaut();
--CREATE TRIGGER trg_lis_defaut_del
-- BEFORE DELETE ON lieu_saisonnier
-- FOR EACH ROW EXECUTE PROCEDURE trg_lis_defaut_del();

-- Special statements for postgres:post databases
-- Un perso a au plus un niveau d'une classe
CREATE TRIGGER trg_pcl_classe 
 BEFORE INSERT OR UPDATE ON perso_classe
 FOR EACH ROW EXECUTE PROCEDURE trg_pcl_classe();

-- Special statements for postgres:post databases
-- Mise a jour des scores calcules a chaque modif
CREATE TRIGGER trg_pcr_update_score 
 BEFORE UPDATE ON perso_carac
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_update_score();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_pcr_insert_carac 
 AFTER INSERT ON caracteristique
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_insert_carac();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_pcr_insert_perso
 AFTER INSERT ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_insert_perso();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_jou_css_defaut
 BEFORE INSERT ON joueur
 FOR EACH ROW EXECUTE PROCEDURE trg_jou_css_defaut();

-- Special statements for postgres:post databases
SELECT AddGeometryColumn('lieu_carto', 'lic_localisation',
                         -1, 'POINT', 2);
CREATE INDEX idx_lic_localisation ON lieu_carto
   USING gist(lic_localisation gist_geometry_ops);


-- Generated Permissions
-- --------------------------------------------------------------------

grant all on personnage to morrigan ;
grant select on personnage to GROUP meuji ;
grant select on personnage to GROUP general ;
grant select, insert, delete, update on personnage to supermj ;
grant all on joueur to morrigan ;
grant select on joueur to GROUP meuji ;
grant select on joueur to GROUP general ;
grant select, insert, delete, update on joueur to supermj ;
grant all on mj to morrigan ;
grant select on mj to GROUP meuji ;
grant select on mj to GROUP general ;
grant select, insert, delete, update on mj to supermj ;
grant all on perso_classe to morrigan ;
grant select on perso_classe to GROUP meuji ;
grant select on perso_classe to GROUP general ;
grant select, insert, delete, update on perso_classe to supermj ;
grant all on forme to morrigan ;
grant select on forme to GROUP meuji ;
grant select on forme to GROUP general ;
grant select, insert, delete, update on forme to supermj ;
grant all on classe_niveau to morrigan ;
grant select on classe_niveau to GROUP meuji ;
grant select on classe_niveau to GROUP general ;
grant select, insert, delete, update on classe_niveau to supermj ;
grant all on classe to morrigan ;
grant select on classe to GROUP meuji ;
grant select on classe to GROUP general ;
grant select, insert, delete, update on classe to supermj ;
grant all on perso_maladie to morrigan ;
grant select on perso_maladie to GROUP meuji ;
grant select on perso_maladie to GROUP general ;
grant select, insert, delete, update on perso_maladie to supermj ;
grant all on maladie to morrigan ;
grant select on maladie to GROUP meuji ;
grant select on maladie to GROUP general ;
grant select, insert, delete, update on maladie to supermj ;
grant all on caracteristique to morrigan ;
grant select on caracteristique to GROUP meuji ;
grant select on caracteristique to GROUP general ;
grant select, insert, delete, update on caracteristique to supermj ;
grant all on perso_carac to morrigan ;
grant select on perso_carac to GROUP meuji ;
grant select on perso_carac to GROUP general ;
grant select, insert, delete, update on perso_carac to supermj ;
grant all on parametre to morrigan ;
grant select on parametre to GROUP meuji ;
grant select on parametre to GROUP general ;
grant select, insert, delete, update on parametre to supermj ;
grant all on categorie to morrigan ;
grant select on categorie to GROUP meuji ;
grant select on categorie to GROUP general ;
grant select, insert, delete, update on categorie to supermj ;
grant all on perso_classe_int to morrigan ;
grant select on perso_classe_int to GROUP meuji ;
grant select on perso_classe_int to GROUP general ;
grant select, insert, delete, update on perso_classe_int to supermj ;
grant all on prerequis_classe to morrigan ;
grant select on prerequis_classe to GROUP meuji ;
grant select on prerequis_classe to GROUP general ;
grant select, insert, delete, update on prerequis_classe to supermj ;
grant all on monde to morrigan ;
grant select on monde to GROUP meuji ;
grant select on monde to GROUP general ;
grant select, insert, delete, update on monde to supermj ;
grant all on lieu to morrigan ;
grant select on lieu to GROUP meuji ;
grant select on lieu to GROUP general ;
grant select, insert, delete, update on lieu to supermj ;
grant all on secteur to morrigan ;
grant select on secteur to GROUP meuji ;
grant select on secteur to GROUP general ;
grant select, insert, delete, update on secteur to supermj ;
grant all on chemin to morrigan ;
grant select on chemin to GROUP meuji ;
grant select on chemin to GROUP general ;
grant select, insert, delete, update on chemin to supermj ;
grant all on saison to morrigan ;
grant select on saison to GROUP meuji ;
grant select on saison to GROUP general ;
grant select, insert, delete, update on saison to supermj ;
grant all on element to morrigan ;
grant select on element to GROUP meuji ;
grant select on element to GROUP general ;
grant select, insert, delete, update on element to supermj ;
grant all on monnaie to morrigan ;
grant select on monnaie to GROUP meuji ;
grant select on monnaie to GROUP general ;
grant select, insert, delete, update on monnaie to supermj ;
grant all on perso_monnaie to morrigan ;
grant select on perso_monnaie to GROUP meuji ;
grant select on perso_monnaie to GROUP general ;
grant select, insert, delete, update on perso_monnaie to supermj ;
grant all on compagnie to morrigan ;
grant select on compagnie to GROUP meuji ;
grant select on compagnie to GROUP general ;
grant select, insert, delete, update on compagnie to supermj ;
grant all on place_compagnie to morrigan ;
grant select on place_compagnie to GROUP meuji ;
grant select on place_compagnie to GROUP general ;
grant select, insert, delete, update on place_compagnie to supermj ;
grant all on sexe to morrigan ;
grant select on sexe to GROUP meuji ;
grant select on sexe to GROUP general ;
grant select, insert, delete, update on sexe to supermj ;
grant all on type_personnage to morrigan ;
grant select on type_personnage to GROUP meuji ;
grant select on type_personnage to GROUP general ;
grant select, insert, delete, update on type_personnage to supermj ;
grant all on type_maladie to morrigan ;
grant select on type_maladie to GROUP meuji ;
grant select on type_maladie to GROUP general ;
grant select, insert, delete, update on type_maladie to supermj ;
grant all on type_caracteristique to morrigan ;
grant select on type_caracteristique to GROUP meuji ;
grant select on type_caracteristique to GROUP general ;
grant select, insert, delete, update on type_caracteristique to supermj ;
grant all on nature_caracteristique to morrigan ;
grant select on nature_caracteristique to GROUP meuji ;
grant select on nature_caracteristique to GROUP general ;
grant select, insert, delete, update on nature_caracteristique to supermj ;
grant all on type_secteur to morrigan ;
grant select on type_secteur to GROUP meuji ;
grant select on type_secteur to GROUP general ;
grant select, insert, delete, update on type_secteur to supermj ;
grant all on type_monde to morrigan ;
grant select on type_monde to GROUP meuji ;
grant select on type_monde to GROUP general ;
grant select, insert, delete, update on type_monde to supermj ;
grant all on type_chemin to morrigan ;
grant select on type_chemin to GROUP meuji ;
grant select on type_chemin to GROUP general ;
grant select, insert, delete, update on type_chemin to supermj ;
grant all on action to morrigan ;
grant select on action to GROUP meuji ;
grant select on action to GROUP general ;
grant select, insert, delete, update on action to supermj ;
grant all on lieu_action to morrigan ;
grant select on lieu_action to GROUP meuji ;
grant select on lieu_action to GROUP general ;
grant select, insert, delete, update on lieu_action to supermj ;
grant all on typelieu_action to morrigan ;
grant select on typelieu_action to GROUP meuji ;
grant select on typelieu_action to GROUP general ;
grant select, insert, delete, update on typelieu_action to supermj ;
grant all on type_lieu to morrigan ;
grant select on type_lieu to GROUP meuji ;
grant select on type_lieu to GROUP general ;
grant select, insert, delete, update on type_lieu to supermj ;
grant all on debit_element to morrigan ;
grant select on debit_element to GROUP meuji ;
grant select on debit_element to GROUP general ;
grant select, insert, delete, update on debit_element to supermj ;
grant all on vente_element to morrigan ;
grant select on vente_element to GROUP meuji ;
grant select on vente_element to GROUP general ;
grant select, insert, delete, update on vente_element to supermj ;
grant all on depot_element to morrigan ;
grant select on depot_element to GROUP meuji ;
grant select on depot_element to GROUP general ;
grant select, insert, delete, update on depot_element to supermj ;
grant all on banque to morrigan ;
grant select on banque to GROUP meuji ;
grant select on banque to GROUP general ;
grant select, insert, delete, update on banque to supermj ;
grant all on element_gen to morrigan ;
grant select on element_gen to GROUP meuji ;
grant select on element_gen to GROUP general ;
grant select, insert, delete, update on element_gen to supermj ;
grant all on arme to morrigan ;
grant select on arme to GROUP meuji ;
grant select on arme to GROUP general ;
grant select, insert, delete, update on arme to supermj ;
grant all on arme_gen to morrigan ;
grant select on arme_gen to GROUP meuji ;
grant select on arme_gen to GROUP general ;
grant select, insert, delete, update on arme_gen to supermj ;
grant all on matiere to morrigan ;
grant select on matiere to GROUP meuji ;
grant select on matiere to GROUP general ;
grant select, insert, delete, update on matiere to supermj ;
grant all on technologie to morrigan ;
grant select on technologie to GROUP meuji ;
grant select on technologie to GROUP general ;
grant select, insert, delete, update on technologie to supermj ;
grant all on armure_gen to morrigan ;
grant select on armure_gen to GROUP meuji ;
grant select on armure_gen to GROUP general ;
grant select, insert, delete, update on armure_gen to supermj ;
grant all on recharge_gen to morrigan ;
grant select on recharge_gen to GROUP meuji ;
grant select on recharge_gen to GROUP general ;
grant select, insert, delete, update on recharge_gen to supermj ;
grant all on vaisseau_gen to morrigan ;
grant select on vaisseau_gen to GROUP meuji ;
grant select on vaisseau_gen to GROUP general ;
grant select, insert, delete, update on vaisseau_gen to supermj ;
grant all on type_vaisseau to morrigan ;
grant select on type_vaisseau to GROUP meuji ;
grant select on type_vaisseau to GROUP general ;
grant select, insert, delete, update on type_vaisseau to supermj ;
grant all on munition to morrigan ;
grant select on munition to GROUP meuji ;
grant select on munition to GROUP general ;
grant select, insert, delete, update on munition to supermj ;
grant all on type_degat to morrigan ;
grant select on type_degat to GROUP meuji ;
grant select on type_degat to GROUP general ;
grant select, insert, delete, update on type_degat to supermj ;
grant all on armure_gen_degat to morrigan ;
grant select on armure_gen_degat to GROUP meuji ;
grant select on armure_gen_degat to GROUP general ;
grant select, insert, delete, update on armure_gen_degat to supermj ;
grant all on armure_gen_zone to morrigan ;
grant select on armure_gen_zone to GROUP meuji ;
grant select on armure_gen_zone to GROUP general ;
grant select, insert, delete, update on armure_gen_zone to supermj ;
grant all on zone_armure to morrigan ;
grant select on zone_armure to GROUP meuji ;
grant select on zone_armure to GROUP general ;
grant select, insert, delete, update on zone_armure to supermj ;
grant all on vaisseau_gen_poste to morrigan ;
grant select on vaisseau_gen_poste to GROUP meuji ;
grant select on vaisseau_gen_poste to GROUP general ;
grant select, insert, delete, update on vaisseau_gen_poste to supermj ;
grant all on type_poste_tmp to morrigan ;
grant select on type_poste_tmp to GROUP meuji ;
grant select on type_poste_tmp to GROUP general ;
grant select, insert, delete, update on type_poste_tmp to supermj ;
grant all on type_element to morrigan ;
grant select on type_element to GROUP meuji ;
grant select on type_element to GROUP general ;
grant select, insert, delete, update on type_element to supermj ;
grant all on armure to morrigan ;
grant select on armure to GROUP meuji ;
grant select on armure to GROUP general ;
grant select, insert, delete, update on armure to supermj ;
grant all on recharge to morrigan ;
grant select on recharge to GROUP meuji ;
grant select on recharge to GROUP general ;
grant select, insert, delete, update on recharge to supermj ;
grant all on vaisseau to morrigan ;
grant select on vaisseau to GROUP meuji ;
grant select on vaisseau to GROUP general ;
grant select, insert, delete, update on vaisseau to supermj ;
grant all on type_classe to morrigan ;
grant select on type_classe to GROUP meuji ;
grant select on type_classe to GROUP general ;
grant select, insert, delete, update on type_classe to supermj ;
grant all on interdit_classe to morrigan ;
grant select on interdit_classe to GROUP meuji ;
grant select on interdit_classe to GROUP general ;
grant select, insert, delete, update on interdit_classe to supermj ;
grant all on prerequis_carac to morrigan ;
grant select on prerequis_carac to GROUP meuji ;
grant select on prerequis_carac to GROUP general ;
grant select, insert, delete, update on prerequis_carac to supermj ;
grant all on taille to morrigan ;
grant select on taille to GROUP meuji ;
grant select on taille to GROUP general ;
grant select, insert, delete, update on taille to supermj ;
grant all on race to morrigan ;
grant select on race to GROUP meuji ;
grant select on race to GROUP general ;
grant select, insert, delete, update on race to supermj ;
grant all on acces_connu to morrigan ;
grant select on acces_connu to GROUP meuji ;
grant select on acces_connu to GROUP general ;
grant select, insert, delete, update on acces_connu to supermj ;
grant all on monde_connu to morrigan ;
grant select on monde_connu to GROUP meuji ;
grant select on monde_connu to GROUP general ;
grant select, insert, delete, update on monde_connu to supermj ;
grant all on type_munition to morrigan ;
grant select on type_munition to GROUP meuji ;
grant select on type_munition to GROUP general ;
grant select, insert, delete, update on type_munition to supermj ;
grant all on css to morrigan ;
grant select on css to GROUP meuji ;
grant select on css to GROUP general ;
grant select, insert, delete, update on css to supermj ;
grant all on itineraire to morrigan ;
grant select on itineraire to GROUP meuji ;
grant select on itineraire to GROUP general ;
grant select, insert, delete, update on itineraire to supermj ;
grant all on type_filature to morrigan ;
grant select on type_filature to GROUP meuji ;
grant select on type_filature to GROUP general ;
grant select, insert, delete, update on type_filature to supermj ;
grant all on mime to morrigan ;
grant select on mime to GROUP meuji ;
grant select on mime to GROUP general ;
grant select, insert, delete, update on mime to supermj ;
grant all on type_mime to morrigan ;
grant select on type_mime to GROUP meuji ;
grant select on type_mime to GROUP general ;
grant select, insert, delete, update on type_mime to supermj ;
grant all on rumeur to morrigan ;
grant select on rumeur to GROUP meuji ;
grant select on rumeur to GROUP general ;
grant select, insert, delete, update on rumeur to supermj ;
grant all on rumeur_ecoute to morrigan ;
grant select on rumeur_ecoute to GROUP meuji ;
grant select on rumeur_ecoute to GROUP general ;
grant select, insert, delete, update on rumeur_ecoute to supermj ;
grant all on langue to morrigan ;
grant select on langue to GROUP meuji ;
grant select on langue to GROUP general ;
grant select, insert, delete, update on langue to supermj ;
grant all on filature to morrigan ;
grant select on filature to GROUP meuji ;
grant select on filature to GROUP general ;
grant select, insert, delete, update on filature to supermj ;
grant all on type_trace to morrigan ;
grant select on type_trace to GROUP meuji ;
grant select on type_trace to GROUP general ;
grant select, insert, delete, update on type_trace to supermj ;
grant all on trace_connue to morrigan ;
grant select on trace_connue to GROUP meuji ;
grant select on trace_connue to GROUP general ;
grant select, insert, delete, update on trace_connue to supermj ;
grant all on lieu_carto to morrigan ;
grant select on lieu_carto to GROUP meuji ;
grant select on lieu_carto to GROUP general ;
grant select, insert, delete, update on lieu_carto to supermj ;
grant all on cycle to morrigan ;
grant select on cycle to GROUP meuji ;
grant select on cycle to GROUP general ;
grant select, insert, delete, update on cycle to supermj ;
grant all on cycle_monde to morrigan ;
grant select on cycle_monde to GROUP meuji ;
grant select on cycle_monde to GROUP general ;
grant select, insert, delete, update on cycle_monde to supermj ;
grant all on cycle_secteur to morrigan ;
grant select on cycle_secteur to GROUP meuji ;
grant select on cycle_secteur to GROUP general ;
grant select, insert, delete, update on cycle_secteur to supermj ;
grant all on periode to morrigan ;
grant select on periode to GROUP meuji ;
grant select on periode to GROUP general ;
grant select, insert, delete, update on periode to supermj ;
grant all on lieu_cycle to morrigan ;
grant select on lieu_cycle to GROUP meuji ;
grant select on lieu_cycle to GROUP general ;
grant select, insert, delete, update on lieu_cycle to supermj ;
grant all on lieu_cyclique to morrigan ;
grant select on lieu_cyclique to GROUP meuji ;
grant select on lieu_cyclique to GROUP general ;
grant select, insert, delete, update on lieu_cyclique to supermj ;
grant all on calque to morrigan ;
grant select on calque to GROUP meuji ;
grant select on calque to GROUP general ;
grant select, insert, delete, update on calque to supermj ;
grant all on type_calque to morrigan ;
grant select on type_calque to GROUP meuji ;
grant select on type_calque to GROUP general ;
grant select, insert, delete, update on type_calque to supermj ;
grant all on calque_lieu_carto to morrigan ;
grant select on calque_lieu_carto to GROUP meuji ;
grant select on calque_lieu_carto to GROUP general ;
grant select, insert, delete, update on calque_lieu_carto to supermj ;
grant all on type_vehicule to morrigan ;
grant select on type_vehicule to GROUP meuji ;
grant select on type_vehicule to GROUP general ;
grant select, insert, delete, update on type_vehicule to supermj ;
grant all on vent_lieu_carto to morrigan ;
grant select on vent_lieu_carto to GROUP meuji ;
grant select on vent_lieu_carto to GROUP general ;
grant select, insert, delete, update on vent_lieu_carto to supermj ;
grant all on restriction_calque to morrigan ;
grant select on restriction_calque to GROUP meuji ;
grant select on restriction_calque to GROUP general ;
grant select, insert, delete, update on restriction_calque to supermj ;
grant all on influence_vent to morrigan ;
grant select on influence_vent to GROUP meuji ;
grant select on influence_vent to GROUP general ;
grant select, insert, delete, update on influence_vent to supermj ;
grant all on vehicule to morrigan ;
grant select on vehicule to GROUP meuji ;
grant select on vehicule to GROUP general ;
grant select, insert, delete, update on vehicule to supermj ;
grant all on type_vent to morrigan ;
grant select on type_vent to GROUP meuji ;
grant select on type_vent to GROUP general ;
grant select, insert, delete, update on type_vent to supermj ;
grant all on type_poste to morrigan ;
grant select on type_poste to GROUP meuji ;
grant select on type_poste to GROUP general ;
grant select, insert, delete, update on type_poste to supermj ;
grant all on poste to morrigan ;
grant select on poste to GROUP meuji ;
grant select on poste to GROUP general ;
grant select, insert, delete, update on poste to supermj ;
grant all on type_vehicule_poste to morrigan ;
grant select on type_vehicule_poste to GROUP meuji ;
grant select on type_vehicule_poste to GROUP general ;
grant select, insert, delete, update on type_vehicule_poste to supermj ;
grant all on monture to morrigan ;
grant select on monture to GROUP meuji ;
grant select on monture to GROUP general ;
grant select, insert, delete, update on monture to supermj ;
grant all on particularite_chemin to morrigan ;
grant select on particularite_chemin to GROUP meuji ;
grant select on particularite_chemin to GROUP general ;
grant select, insert, delete, update on particularite_chemin to supermj ;
grant all on description_lieu to morrigan ;
grant select on description_lieu to GROUP meuji ;
grant select on description_lieu to GROUP general ;
grant select, insert, delete, update on description_lieu to supermj ;
grant all on classeniveau_action to morrigan ;
grant select on classeniveau_action to GROUP meuji ;
grant select on classeniveau_action to GROUP general ;
grant select, insert, delete, update on classeniveau_action to supermj ;
grant all on engagement to morrigan ;
grant select on engagement to GROUP meuji ;
grant select on engagement to GROUP general ;
grant select, insert, delete, update on engagement to supermj ;
grant all on coffre to morrigan ;
grant select on coffre to GROUP meuji ;
grant select on coffre to GROUP general ;
grant select, insert, delete, update on coffre to supermj ;
grant all on coffre_element to morrigan ;
grant select on coffre_element to GROUP meuji ;
grant select on coffre_element to GROUP general ;
grant select, insert, delete, update on coffre_element to supermj ;
grant all on element_visible to morrigan ;
grant select on element_visible to GROUP meuji ;
grant select on element_visible to GROUP general ;
grant select, insert, delete, update on element_visible to supermj ;
grant all on image to morrigan ;
grant select on image to GROUP meuji ;
grant select on image to GROUP general ;
grant select, insert, delete, update on image to supermj ;
grant all on boutique to morrigan ;
grant select on boutique to GROUP meuji ;
grant select on boutique to GROUP general ;
grant select, insert, delete, update on boutique to supermj ;
grant all on automate to morrigan ;
grant select on automate to GROUP meuji ;
grant select on automate to GROUP general ;
grant select, insert, delete, update on automate to supermj ;
grant all on element_gen_connu to morrigan ;
grant select on element_gen_connu to GROUP meuji ;
grant select on element_gen_connu to GROUP general ;
grant select, insert, delete, update on element_gen_connu to supermj ;
grant all on automate_carac to morrigan ;
grant select on automate_carac to GROUP meuji ;
grant select on automate_carac to GROUP general ;
grant select, insert, delete, update on automate_carac to supermj ;
grant all on achat_element_gen to morrigan ;
grant select on achat_element_gen to GROUP meuji ;
grant select on achat_element_gen to GROUP general ;
grant select, insert, delete, update on achat_element_gen to supermj ;
grant all on type_environnement to morrigan ;
grant select on type_environnement to GROUP meuji ;
grant select on type_environnement to GROUP general ;
grant select, insert, delete, update on type_environnement to supermj ;
grant all on profil_annonce to morrigan ;
grant select on profil_annonce to GROUP meuji ;
grant select on profil_annonce to GROUP general ;
grant select, insert, delete, update on profil_annonce to supermj ;
grant all on news to morrigan ;
grant select on news to GROUP meuji ;
grant select on news to GROUP general ;
grant select, insert, delete, update on news to supermj ;
grant all on categorie_profil to morrigan ;
grant select on categorie_profil to GROUP meuji ;
grant select on categorie_profil to GROUP general ;
grant select, insert, delete, update on categorie_profil to supermj ;
grant all on candidature to morrigan ;
grant select on candidature to GROUP meuji ;
grant select on candidature to GROUP general ;
grant select, insert, delete, update on candidature to supermj ;
grant all on question to morrigan ;
grant select on question to GROUP meuji ;
grant select on question to GROUP general ;
grant select, insert, delete, update on question to supermj ;
grant all on reponse to morrigan ;
grant select on reponse to GROUP meuji ;
grant select on reponse to GROUP general ;
grant select, insert, delete, update on reponse to supermj ;
grant all on profil_classe to morrigan ;
grant select on profil_classe to GROUP meuji ;
grant select on profil_classe to GROUP general ;
grant select, insert, delete, update on profil_classe to supermj ;
grant all on profil_race to morrigan ;
grant select on profil_race to GROUP meuji ;
grant select on profil_race to GROUP general ;
grant select, insert, delete, update on profil_race to supermj ;
grant all on profil_sexe to morrigan ;
grant select on profil_sexe to GROUP meuji ;
grant select on profil_sexe to GROUP general ;
grant select, insert, delete, update on profil_sexe to supermj ;


-- Generated SQL Insert statements
-- --------------------------------------------------------------------


-- inserts for categorie
insert into categorie values ( 1, 'Univers', false ) ;
insert into categorie values ( 2, 'Joueur', false ) ;
insert into categorie values ( 3, 'Combat', false ) ;

-- inserts for parametre
insert into parametre values ( 1, 1, 'Saison', '1', 'Id de la saison en cours' ) ;
insert into parametre values ( 2, 2, 'Nb classes/perso', '2', 'Nombre de classes par personnage' ) ;
insert into parametre values ( 3, 2, 'Nb aptitudes/perso', '5', 'Nombre d''aptitudes par personnage' ) ;
insert into parametre values ( 4, 2, 'Remise de PA', 'int(%(c5)d/2)+5', 'Nombre de points d''action a la remise' ) ;
insert into parametre values ( 5, 2, 'Remise de PV', 'int(%(c3)d/4)+1', 'Nombre de points de vie a la remise' ) ;
insert into parametre values ( 6, 2, 'Remise de PN', '%(remise_pn)d', 'Nombre de points de nourriture a la remise' ) ;
insert into parametre values ( 7, 2, 'Remise de PP', 'int((%(c1)d+%(c2)d)/4)', 'Nombre de points de paralysie a la remise' ) ;

-- inserts for type_personnage
insert into type_personnage values ( 1, 'PJ' ) ;
insert into type_personnage values ( 2, 'PNJ' ) ;
insert into type_personnage values ( 3, 'Familier' ) ;
insert into type_personnage values ( 4, 'Gob' ) ;
insert into type_personnage values ( 5, 'Monture' ) ;

-- inserts for type_caracteristique
insert into type_caracteristique values ( 1, 'Attribut' ) ;
insert into type_caracteristique values ( 2, 'Competence' ) ;
insert into type_caracteristique values ( 3, 'Langue' ) ;

-- inserts for type_maladie
insert into type_maladie values ( 1, 'Classe' ) ;
insert into type_maladie values ( 2, 'Race' ) ;
insert into type_maladie values ( 3, 'Definitif' ) ;
insert into type_maladie values ( 4, 'Temporaire' ) ;
insert into type_maladie values ( 5, 'Instantane' ) ;
insert into type_maladie values ( 6, 'Lieu' ) ;

-- inserts for type_chemin
insert into type_chemin values ( 1, 'Classique' ) ;
insert into type_chemin values ( 2, 'Acces trappe' ) ;
insert into type_chemin values ( 3, 'Acces sas' ) ;
insert into type_chemin values ( 4, 'Acces pont' ) ;

-- inserts for type_secteur
insert into type_secteur values ( 1, 'Immobile' ) ;
insert into type_secteur values ( 2, 'Mobile (planete)' ) ;
insert into type_secteur values ( 3, 'Mobile (galaxie)' ) ;
insert into type_secteur values ( 4, 'Vaisseau (terre)' ) ;
insert into type_secteur values ( 5, 'Vaisseau (tout-terrain)' ) ;
insert into type_secteur values ( 6, 'Vaisseau spatial' ) ;

-- inserts for type_monde
insert into type_monde values ( 1, 'Galaxie' ) ;
insert into type_monde values ( 2, 'Planete' ) ;

-- inserts for type_vaisseau
insert into type_vaisseau values ( 1, 'Terrestre' ) ;
insert into type_vaisseau values ( 2, 'Aeroglisseur' ) ;
insert into type_vaisseau values ( 3, 'Sous-marin' ) ;
insert into type_vaisseau values ( 4, 'Marin' ) ;
insert into type_vaisseau values ( 5, 'Spatial' ) ;

-- inserts for type_element
insert into type_element values ( 1, 'Objet' ) ;
insert into type_element values ( 2, 'Arme' ) ;
insert into type_element values ( 3, 'Armure' ) ;
insert into type_element values ( 4, 'Sort' ) ;
insert into type_element values ( 5, 'Composite' ) ;

-- inserts for type_classe
insert into type_classe values ( 1, 'Classe' ) ;
insert into type_classe values ( 2, 'Aptitude' ) ;

-- inserts for taille
insert into taille values ( 1, 'Microscopique', 'Indiscernable à l''oeil nu.', 0 ) ;
insert into taille values ( 2, 'Minuscule', 'Tout piti-piti.', 1 ) ;
insert into taille values ( 3, 'Très petite', 'On commence à peine à le remarquer.', 2 ) ;
insert into taille values ( 4, 'Petite', 'On le remarque', 4 ) ;
insert into taille values ( 5, 'Plutôt petite', 'Taille d''un chat', 8 ) ;
insert into taille values ( 6, 'Plutôt moyenne', 'Taille d''un grand chien', 16 ) ;
insert into taille values ( 7, 'Moyenne', 'Taille d''un humanoïde', 32 ) ;
insert into taille values ( 8, 'Plutôt grande', 'Taille d''un géant.', 64 ) ;
insert into taille values ( 9, 'Grande', 'Taille d''une maison', 128 ) ;
insert into taille values ( 10, 'Imposante', 'Aussi imposant qu''une cathédrale, un vaisseau', 256 ) ;
insert into taille values ( 11, 'Très grande', 'Taille d''un quartier', 512 ) ;
insert into taille values ( 12, 'Immense', 'Taille d''une petite ville', 1024 ) ;
insert into taille values ( 13, 'Colossale', 'Taille d''une grande ville', 2048 ) ;
insert into taille values ( 14, 'Infinie', 'Inconcevable.', 10000 ) ;

-- inserts for type_filature
insert into type_filature values ( 1, 'Filature' ) ;
insert into type_filature values ( 2, 'Pistage' ) ;
insert into type_filature values ( 3, 'Suite' ) ;

-- inserts for type_mime
insert into type_mime values ( 1, 'Simple' ) ;
insert into type_mime values ( 2, 'Personnage' ) ;


-- Generated SQL Constraints
-- --------------------------------------------------------------------

create unique index idxu_prs_nom_originel on personnage  (prs_nom_originel) ;
create unique index idxu_prs_nom_actuel on personnage  (prs_nom_actuel) ;
create unique index idxu_jou_pseudo on joueur  (jou_pseudo) ;
create unique index idxu_frm_prs_rac on forme  (frm_prs_id,frm_rac_id) ;
create unique index idxu_cln_titre on classe_niveau  (cln_titre) ;
create unique index idx_cln_clsniv on classe_niveau  (cln_cls_id,cln_niveau) ;
create unique index idxu_cls_nom on classe  (cls_nom) ;
create unique index idxu_pml_prsmld on perso_maladie  (pml_prs_id,pml_mld_id) ;
create unique index idxu_mld_nom on maladie  (mld_nom) ;
create unique index idxu_crc_nom on caracteristique  (crc_nom) ;
create unique index idxu_pcr_prscrc on perso_carac  (pcr_prs_id,pcr_crc_id) ;
create unique index idxu_ctg_libelle on categorie  (ctg_libelle) ;
create unique index idxu_pci_prscls on perso_classe_int  (pci_prs_id,pci_cls_id) ;
create unique index idxu_pqc_clncls on prerequis_classe  (pqc_cln_id,pqc_cls_id) ;
create unique index idxu_mnd_nom on monde  (mnd_nom) ;
create unique index idxu_lie_nom on lieu  (lie_nom) ;
create unique index idxu_sct_nom on secteur  (sct_nom) ;
create unique index idxu_ssn_nom on saison  (ssn_nom) ;
create unique index idxu_mnn_nom on monnaie  (mnn_nom) ;
create unique index idxu_pmn_prsmnn on perso_monnaie  (pmn_prs_id,pmn_mnn_id) ;
create unique index idx_cmp_nom on compagnie  (cmp_nom) ;
create unique index idxu_plcprs on place_compagnie  (plc_prs_id) ;
create unique index idxu_plc_cmpprs on place_compagnie  (plc_cmp_id,plc_prs_id) ;
create unique index idxu_nrc_nom on nature_caracteristique  (ncr_nom) ;
create unique index idxu_act_nom on action  (act_nom) ;
create unique index idxu_egn_nom on element_gen  (egn_nom) ;
create unique index idxu_arm_agnelm on arme  (arm_agn_id,arm_elm_id) ;
create unique index idxu_cgn_egn on recharge_gen  (cgn_egn_id) ;
create unique index idxu_vgn_egn on vaisseau_gen  (vgn_egn_id) ;
create unique index idxu_agd_ugntdg on armure_gen_degat  (agd_ugn_id,agd_tdg_id) ;
create unique index idxu_agz_ugnzon on armure_gen_zone  (agz_ugn_id,agz_zon_id) ;
create unique index idxu_vgp_vgntpt on vaisseau_gen_poste  (vgp_vgn_id,vgp_tpt_id) ;
create unique index idxu_aru_elm on armure  (aru_elm_id) ;
create unique index idxu_rch_elm on recharge  (rch_elm_id) ;
create unique index idxu_vss_sct on vaisseau  (vss_sct_id) ;
create unique index idxu_vss_elm on vaisseau  (vss_elm_id) ;
create unique index idxu_inc_clsint on interdit_classe  (inc_cls_id,inc_interdit_cls_id) ;
create unique index idxu_pqa_cln on prerequis_carac  (pqa_cln_id) ;
create unique index idxu_rac_nom on race  (rac_nom) ;
create unique index idxu_acc_chmprs on acces_connu  (acc_chm_id,acc_prs_id) ;
create unique index idxu_mnc_mndprs on monde_connu  (mnc_mnd_id,mnc_prs_id) ;
create unique index idxu_lng_nom on langue  (lng_nom) ;
create index idx_lic_lie_id on lieu_carto  (lic_lie_id) ;
create index idx_clc_clq_id on calque_lieu_carto  (clc_clq_id) ;
create index idx_clc_lic_id on calque_lieu_carto  (clc_lic_id) ;
create unique index idxu_cff_elm on coffre  (cff_elm_id) ;
create unique index idxu_cel_cffelm on coffre_element  (cel_cff_id,cel_elm_id) ;
create unique index idxu_elv_prselm on element_visible  (elv_prs_id,elv_elm_id) ;
create unique index idxu_btq_cff on boutique  (btq_cff_id) ;
create unique index idxu_egc_egnprs on element_gen_connu  (egc_egn_id,egc_prs_id) ;
create unique index idxu_acr_atmcrc on automate_carac  (acr_atm_id,acr_crc_id) ;
create unique index idxu_aeg_egnbtq on achat_element_gen  (aeg_egn_id,aeg_btq_id) ;
create unique index idxu_pcl_procls on profil_classe  (pcl_pro_id,pcl_cls_id) ;
create unique index idxu_pra_prorac on profil_race  (pra_pro_id,pra_rac_id) ;
create unique index idxu_psx_prosex on profil_sexe  (psx_pro_id,psx_sex_id) ;
alter table personnage add constraint fk_pj_joueur
  foreign key (prs_jou_id)
  references joueur (jou_id)  ;
alter table mj add constraint fk_mj_joueur
  foreign key (mji_jou_id)
  references joueur (jou_id)  ;
alter table perso_classe add constraint fk_ppc_perso
  foreign key (pce_prs_id)
  references personnage (prs_id)  ;
alter table forme add constraint fk_rf_race
  foreign key (frm_rac_id)
  references race (rac_id)  ;
alter table classe_niveau add constraint fk_ccn_classe
  foreign key (cln_cls_id)
  references classe (cls_id)  ;
alter table perso_maladie add constraint fk_ppm_perso
  foreign key (pml_prs_id)
  references personnage (prs_id)  ;
alter table classe_niveau add constraint fk_mcn_maladie
  foreign key (cln_mld_id)
  references maladie (mld_id)  ;
alter table race add constraint fk_mr_maladie
  foreign key (rac_mld_id)
  references maladie (mld_id)  ;
alter table perso_maladie add constraint fk_mpm_maladie
  foreign key (pml_mld_id)
  references maladie (mld_id)  ;
alter table perso_carac add constraint fk_cpc_carac
  foreign key (pcr_crc_id)
  references caracteristique (crc_id) on update cascade on delete cascade ;
alter table perso_carac add constraint fk_ppc_carac
  foreign key (pcr_prs_id)
  references personnage (prs_id) on update cascade on delete cascade ;
alter table parametre add constraint fk_cp_categorie
  foreign key (prm_ctg_id)
  references categorie (ctg_id)  ;
alter table personnage add constraint fk_prs_possesseur
  foreign key (prs_possesseur_prs_id)
  references personnage (prs_id)  ;
alter table perso_classe_int add constraint fk_pci_perso
  foreign key (pci_prs_id)
  references personnage (prs_id)  ;
alter table perso_classe_int add constraint fk_pci_cls
  foreign key (pci_cls_id)
  references classe (cls_id)  ;
alter table classe add constraint fk_tcl_typcls
  foreign key (cls_tcl_id)
  references type_classe (tcl_id)  ;
alter table monde add constraint fk_mnd_pere
  foreign key (mnd_pere_mnd_id)
  references monde (mnd_id)  ;
alter table lieu add constraint fk_scl_secteur
  foreign key (lie_sct_id)
  references secteur (sct_id)  ;
alter table chemin add constraint fk_lch_depart
  foreign key (chm_arrivee_lie_id)
  references lieu (lie_id)  ;
alter table chemin add constraint fk_lch_arrivee
  foreign key (chm_depart_lie_id)
  references lieu (lie_id)  ;
alter table chemin add constraint fk_ec_cle
  foreign key (chm_egn_id)
  references element_gen (egn_id)  ;
alter table personnage add constraint fk_pl_depart
  foreign key (prs_lie_id)
  references lieu (lie_id)  ;
alter table perso_classe add constraint fk_pcn_clniv
  foreign key (pce_cln_id)
  references classe_niveau (cln_id)  ;
alter table forme add constraint fk_pf_perso
  foreign key (frm_prs_id)
  references personnage (prs_id)  ;
alter table perso_monnaie add constraint fk_pm_perso
  foreign key (pmn_prs_id)
  references personnage (prs_id)  ;
alter table perso_monnaie add constraint fk_pm_monnaie
  foreign key (pmn_mnn_id)
  references monnaie (mnn_id)  ;
alter table place_compagnie add constraint fk_pcp_perso
  foreign key (plc_prs_id)
  references personnage (prs_id)  ;
alter table place_compagnie add constraint fk_pcp_cmp
  foreign key (plc_cmp_id)
  references compagnie (cmp_id)  ;
alter table maladie add constraint fk_mtm_typmal
  foreign key (mld_tml_id)
  references type_maladie (tml_id)  ;
alter table secteur add constraint fk_sts_typsct
  foreign key (sct_tsc_id)
  references type_secteur (tsc_id)  ;
alter table chemin add constraint fk_ctc_typchm
  foreign key (chm_tch_id)
  references type_chemin (tch_id)  ;
alter table personnage add constraint fk_ptp_typprs
  foreign key (prs_tpr_id)
  references type_personnage (tpr_id)  ;
alter table caracteristique add constraint fk_ctc_typcrc
  foreign key (crc_tcr_id)
  references type_caracteristique (tcr_id)  ;
alter table lieu_action add constraint fk_lac_action
  foreign key (lac_act_id)
  references action (act_id)  ;
alter table typelieu_action add constraint fk_tla_action
  foreign key (tla_act_id)
  references action (act_id)  ;
alter table lieu_action add constraint fk_lac_lieu
  foreign key (lac_lie_id)
  references lieu (lie_id)  ;
alter table lieu add constraint fk_tli_typelieu
  foreign key (lie_tli_id)
  references type_lieu (tli_id)  ;
alter table typelieu_action add constraint fk_tla_typelieu
  foreign key (tla_tli_id)
  references type_lieu (tli_id)  ;
alter table debit_element add constraint fk_dli_dbtlie
  foreign key (dbt_lie_id)
  references lieu (lie_id)  ;
alter table vente_element add constraint fk_vli_vntlie
  foreign key (vnt_lie_id)
  references lieu (lie_id)  ;
alter table depot_element add constraint fk_dlie_dptlie
  foreign key (dpt_lie_id)
  references lieu (lie_id)  ;
alter table banque add constraint fk_bli_bnqlie
  foreign key (bnq_lie_id)
  references lieu (lie_id)  ;
alter table caracteristique add constraint fk_ccc_catcrc
  foreign key (crc_ncr_id)
  references nature_caracteristique (ncr_id)  ;
alter table element add constraint fk_ege_generique
  foreign key (elm_egn_id)
  references element_gen (egn_id)  ;
alter table arme add constraint fk_ar_recharge
  foreign key (arm_rch_id)
  references recharge (rch_id)  ;
alter table arme add constraint fk_aga_generique
  foreign key (arm_agn_id)
  references arme_gen (agn_id)  ;
alter table arme_gen add constraint fk_egag_arme
  foreign key (agn_egn_id)
  references element_gen (egn_id)  ;
alter table element_gen add constraint fk_egm_matiere
  foreign key (egn_mtr_id)
  references matiere (mtr_id)  ;
alter table element_gen add constraint fk_egt_techno
  foreign key (egn_tcn_id)
  references technologie (tcn_id)  ;
alter table armure_gen add constraint fk_egug_armure
  foreign key (ugn_egn_id)
  references element_gen (egn_id)  ;
alter table recharge_gen add constraint fk_egcg_recharge
  foreign key (cgn_egn_id)
  references element_gen (egn_id)  ;
alter table vaisseau_gen add constraint fk_egvg_vaisseau
  foreign key (vgn_egn_id)
  references element_gen (egn_id)  ;
alter table vaisseau_gen add constraint fk_tvg_typevaisseau
  foreign key (vgn_tvs_id)
  references type_vaisseau (tvs_id)  ;
alter table arme_gen add constraint fk_agm_maladie
  foreign key (agn_mld_id)
  references maladie (mld_id)  ;
alter table arme_gen add constraint fk_agtmt_type_munition
  foreign key (agn_tmt_id)
  references type_munition (tmt_id)  ;
alter table recharge_gen add constraint fk_cgmn_munition
  foreign key (cgn_mnt_id)
  references munition (mnt_id)  ;
alter table munition add constraint fk_mnm_maladie
  foreign key (mnt_mld_id)
  references maladie (mld_id)  ;
alter table arme_gen add constraint fk_agtd_degat
  foreign key (agn_tdg_id)
  references type_degat (tdg_id)  ;
alter table armure_gen_zone add constraint fk_agz_zone
  foreign key (agz_zon_id)
  references zone_armure (zon_id)  ;
alter table armure_gen_zone add constraint fk_agz_zones
  foreign key (agz_ugn_id)
  references armure_gen (ugn_id)  ;
alter table armure_gen_degat add constraint fk_agd_degat
  foreign key (agd_ugn_id)
  references armure_gen (ugn_id)  ;
alter table armure_gen_degat add constraint fk_agd_degats
  foreign key (agd_tdg_id)
  references type_degat (tdg_id)  ;
alter table vaisseau_gen_poste add constraint fk_vgtp_poste
  foreign key (vgp_vgn_id)
  references vaisseau_gen (vgn_id)  ;
alter table vaisseau_gen_poste add constraint fk_tvp_typeposte
  foreign key (vgp_tpt_id)
  references type_poste_tmp (tpt_id)  ;
alter table element_gen add constraint fk_egt_type
  foreign key (egn_tel_id)
  references type_element (tel_id)  ;
alter table armure add constraint fk_aue_armure
  foreign key (aru_elm_id)
  references element (elm_id)  ;
alter table recharge add constraint fk_re_recharge
  foreign key (rch_elm_id)
  references element (elm_id)  ;
alter table vaisseau add constraint fk_ve_vaisseau
  foreign key (vss_elm_id)
  references element (elm_id)  ;
alter table vaisseau add constraint fk_vs_vaisseau
  foreign key (vss_sct_id)
  references secteur (sct_id)  ;
alter table element add constraint fk_ep_prs
  foreign key (elm_prs_id)
  references personnage (prs_id)  ;
alter table forme add constraint fk_sf_sexe
  foreign key (frm_sex_id)
  references sexe (sex_id)  ;
alter table arme add constraint fk_ae_arme
  foreign key (arm_elm_id)
  references element (elm_id)  ;
alter table interdit_classe add constraint fk_cli_cls
  foreign key (inc_cls_id)
  references classe (cls_id)  ;
alter table interdit_classe add constraint fk_cli_clsint
  foreign key (inc_interdit_cls_id)
  references classe (cls_id)  ;
alter table prerequis_classe add constraint fk_pqc_cls
  foreign key (pqc_cls_id)
  references classe (cls_id)  ;
alter table prerequis_carac add constraint fk_pqa_clsniv
  foreign key (pqa_cln_id)
  references classe_niveau (cln_id)  ;
alter table prerequis_classe add constraint fk_pqc_cln
  foreign key (pqc_cln_id)
  references classe_niveau (cln_id)  ;
alter table element add constraint fk_el_lieu
  foreign key (elm_lie_id)
  references lieu (lie_id)  ;
alter table acces_connu add constraint fk_acp_prs
  foreign key (acc_prs_id)
  references personnage (prs_id)  ;
alter table acces_connu add constraint fk_ach_sct
  foreign key (acc_chm_id)
  references chemin (chm_id)  ;
alter table monde_connu add constraint fk_mcp_prs
  foreign key (mnc_prs_id)
  references personnage (prs_id)  ;
alter table monde_connu add constraint fk_mcm_mnd
  foreign key (mnc_mnd_id)
  references monde (mnd_id)  ;
alter table munition add constraint fk_cgtmt_type_munition
  foreign key (mnt_tmt_id)
  references type_munition (tmt_id)  ;
alter table joueur add constraint fk_jc_joueurcss
  foreign key (jou_css_id)
  references css (css_id)  ;
alter table itineraire add constraint fk_ich_itineraire
  foreign key (itn_chm_id)
  references chemin (chm_id)  ;
alter table itineraire add constraint fk_frmit_itineraire
  foreign key (itn_frm_id)
  references forme (frm_id)  ;
alter table filature add constraint fk_ftf_typflt
  foreign key (flt_tfl_id)
  references type_filature (tfl_id)  ;
alter table filature add constraint fk_ftprs_prssuivant
  foreign key (flt_suivant_prs_id)
  references personnage (prs_id)  ;
alter table filature add constraint fk_ftfrm_prssuivi
  foreign key (flt_suivi_frm_id)
  references forme (frm_id)  ;
alter table mime add constraint fk_mtm_typmim
  foreign key (mim_tmm_id)
  references type_mime (tmm_id)  ;
alter table rumeur add constraint fk_rmrprs_auteur
  foreign key (rmr_prs_id)
  references personnage (prs_id)  ;
alter table rumeur_ecoute add constraint fk_rmc_ecouteur
  foreign key (rmc_prs_id)
  references personnage (prs_id)  ;
alter table rumeur_ecoute add constraint fk_rmc_rumeur
  foreign key (rmc_rmr_id)
  references rumeur (rmr_id)  ;
alter table langue add constraint fk_ctl_conlangue
  foreign key (lng_id)
  references caracteristique (crc_id)  ;
alter table itineraire add constraint fk_itt_type_trace
  foreign key (itn_ttr_id)
  references type_trace (ttr_id)  ;
alter table trace_connue add constraint fk_tctt_type_trace
  foreign key (trc_ttr_id)
  references type_trace (ttr_id)  ;
alter table trace_connue add constraint fk_ptc_itineraire
  foreign key (trc_prs_id)
  references personnage (prs_id)  ;
alter table trace_connue add constraint fk_ptct_itineraire
  foreign key (trc_frm_trace_id)
  references forme (frm_id)  ;
alter table forme add constraint fk_fot_type_trace
  foreign key (frm_ttr_id)
  references type_trace (ttr_id)  ;
alter table rumeur add constraint fk_rmrlng_langue
  foreign key (rmr_lng_id)
  references langue (lng_id)  ;
alter table calque_lieu_carto add constraint fk_lcclc_lieucalque
  foreign key (clc_lic_id)
  references lieu_carto (lic_id)  ;
alter table lieu_carto add constraint fk_liel_lieucarto
  foreign key (lic_lie_id)
  references lieu (lie_id)  ;
alter table lieu_carto add constraint fk_lcm_lieucmonde
  foreign key (lic_mnd_id)
  references monde (mnd_id)  ;
alter table lieu_carto add constraint fk_lcs_lieucsaison
  foreign key (lic_ssn_id)
  references saison (ssn_id)  ;
alter table monde add constraint fk_mtm_typmnd
  foreign key (mnd_tmn_id)
  references type_monde (tmn_id)  ;
alter table cycle add constraint fk_ccl_cycle
  foreign key (ccl_duree_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_monde add constraint fk_cclm_cycle_monde
  foreign key (cmn_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_monde add constraint fk_cmn_monde
  foreign key (cmn_mnd_id)
  references monde (mnd_id)  ;
alter table cycle_secteur add constraint fk_cclsc_cycle_secteur
  foreign key (csc_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_secteur add constraint fk_csc_secteur
  foreign key (csc_sct_id)
  references secteur (sct_id)  ;
alter table lieu_cycle add constraint fk_ccll_lieu_cycle
  foreign key (lcd_ccl_id)
  references cycle (ccl_id)  ;
alter table lieu_cycle add constraint fk_lcdl_lieu
  foreign key (lcd_lie_id)
  references lieu (lie_id)  ;
alter table periode add constraint fk_ccl_cperiode
  foreign key (prd_ccl_id)
  references cycle (ccl_id)  ;
alter table lieu_cyclique add constraint fk_lccl_lieu
  foreign key (lcc_lie_id)
  references lieu (lie_id)  ;
alter table lieu_cyclique add constraint fk_lcdp_lcycle
  foreign key (lcc_prd_id)
  references periode (prd_id)  ;
alter table calque add constraint fk_tcq_calque_tcalque
  foreign key (clq_tcq_id)
  references type_calque (tcq_id)  ;
alter table calque_lieu_carto add constraint fk_clqclc_cartocalque
  foreign key (clc_clq_id)
  references calque (clq_id)  ;
alter table vent_lieu_carto add constraint fk_lctvt_lieuvent
  foreign key (cvt_lic_id)
  references lieu_carto (lic_id)  ;
alter table vent_lieu_carto add constraint fk_vlctv_cartovent
  foreign key (cvt_tvt_id)
  references type_vent (tvt_id)  ;
alter table calque add constraint fk_clcrc_calquecarac
  foreign key (clq_crc_id)
  references caracteristique (crc_id)  ;
alter table rumeur add constraint fk_rmrsct_secteur
  foreign key (rmr_sct_id)
  references secteur (sct_id)  ;
alter table restriction_calque add constraint fk_rtc_calque
  foreign key (rtc_clq_id)
  references calque (clq_id)  ;
alter table influence_vent add constraint fk_tvi_typevent
  foreign key (inv_tvt_id)
  references type_vent (tvt_id)  ;
alter table influence_vent add constraint fk_tvi_typevehicule
  foreign key (inv_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table restriction_calque add constraint fk_tvr_typevehicule
  foreign key (rtc_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table vehicule add constraint fk_vht_typevehiculeposte
  foreign key (vhc_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table secteur add constraint fk_svh_vehicule
  foreign key (sct_vhc_id)
  references vehicule (vhc_id)  ;
alter table type_vehicule_poste add constraint fk_ttp_typevehicule
  foreign key (ttp_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table type_vehicule_poste add constraint fk_ttp_typeposte
  foreign key (ttp_tps_id)
  references type_poste (tps_id)  ;
alter table poste add constraint fk_pst_typeposte
  foreign key (pst_tps_id)
  references type_poste (tps_id)  ;
alter table poste add constraint fk_pst_vehicule
  foreign key (pst_vhc_id)
  references vehicule (vhc_id)  ;
alter table monture add constraint fk_monture_cavperso
  foreign key (mon_cavalier_prs_id)
  references personnage (prs_id)  ;
alter table monture add constraint fk_mon_perso
  foreign key (mon_prs_id)
  references personnage (prs_id)  ;
alter table monture add constraint fk_monture_element
  foreign key (mon_clef_elm_id)
  references element (elm_id)  ;
alter table monture add constraint fk_monture_elementgen
  foreign key (mon_egn_id)
  references element_gen (egn_id)  ;
alter table chemin add constraint fk_chcrc_carac
  foreign key (chm_crc_id)
  references caracteristique (crc_id)  ;
alter table chemin add constraint fk_ctc_parchm
  foreign key (chm_pch_id)
  references particularite_chemin (pch_id)  ;
alter table description_lieu add constraint fk_desclieu
  foreign key (dsl_lie_id)
  references lieu (lie_id)  ;
alter table classeniveau_action add constraint fk_cna_action
  foreign key (cna_act_id)
  references action (act_id)  ;
alter table classeniveau_action add constraint fk_cnacln_action
  foreign key (cna_cln_id)
  references classe_niveau (cln_id)  ;
alter table chemin add constraint fk_chcrc_carac_2
  foreign key (chm_crc_2_id)
  references caracteristique (crc_id)  ;
alter table calque add constraint fk_clcrc_calquecarac2
  foreign key (clq_crc2_crc_id)
  references caracteristique (crc_id)  ;
alter table poste add constraint fk_pst_perso
  foreign key (pst_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_perso_un
  foreign key (eng_prs1_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_perso_deux
  foreign key (eng_prs2_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_lieu
  foreign key (eng_lie_id)
  references lieu (lie_id)  ;
alter table arme_gen add constraint fk_agcrc_1
  foreign key (agn_carac1_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_2
  foreign key (agn_carac2_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_3
  foreign key (agn_compet1_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_4
  foreign key (agn_compet2_crc_id)
  references caracteristique (crc_id)  ;
alter table element_gen add constraint fk_egn_maladie_possession
  foreign key (egn_possession_mld_id)
  references maladie (mld_id)  ;
alter table element_gen add constraint fk_egn_maladie_equipe
  foreign key (egn_equipe_mld_id)
  references maladie (mld_id)  ;
alter table type_vehicule add constraint fk_tveg_carburant
  foreign key (tvh_carburant_egn_id)
  references element_gen (egn_id)  ;
alter table rumeur add constraint fk_rmrlie_lieu
  foreign key (rmr_lie_id)
  references lieu (lie_id)  ;
alter table itineraire add constraint fk_depiti_lieu
  foreign key (itn_depart_lie_id)
  references lieu (lie_id)  ;
alter table itineraire add constraint fk_arriti_lieu
  foreign key (itn_arrivee_lie_id)
  references lieu (lie_id)  ;
alter table monde add constraint fk_mtm_maladie
  foreign key (mnd_mld_id)
  references maladie (mld_id)  ;
alter table depot_element add constraint fk_dpt_element
  foreign key (dpt_elm_id)
  references element (elm_id)  ;
alter table vente_element add constraint fk_vnt_element
  foreign key (vnt_elm_id)
  references element (elm_id)  ;
alter table banque add constraint fk_bnq_possesseur
  foreign key (bnq_prs_id)
  references personnage (prs_id)  ;
alter table banque add constraint fk_bnq_monnaie
  foreign key (bnq_mnn_id)
  references monnaie (mnn_id)  ;
alter table depot_element add constraint fk_dpt_possesseur
  foreign key (dpt_prs_id)
  references personnage (prs_id)  ;
alter table debit_element add constraint fk_dbt_element
  foreign key (dbt_egn_id)
  references element_gen (egn_id)  ;
alter table coffre add constraint fk_ce_coffre
  foreign key (cff_elm_id)
  references element (elm_id)  ;
alter table coffre_element add constraint fk_cee_coffre
  foreign key (cel_elm_id)
  references element (elm_id)  ;
alter table coffre_element add constraint fk_cce_coffre
  foreign key (cel_cff_id)
  references coffre (cff_id)  ;
alter table coffre add constraint fk_ceg_cle_coffre
  foreign key (cff_clef_egn_id)
  references element_gen (egn_id)  ;
alter table element_visible add constraint fk_elv_elm
  foreign key (elv_elm_id)
  references element (elm_id)  ;
alter table element_visible add constraint fk_elv_prs
  foreign key (elv_prs_id)
  references personnage (prs_id)  ;
alter table lieu add constraint fk_iml_image
  foreign key (lie_img_id)
  references image (img_id)  ;
alter table element_gen_connu add constraint fk_egc_element_gen_connu
  foreign key (egc_egn_id)
  references element_gen (egn_id)  ;
alter table element_gen_connu add constraint fk_egc_personnage
  foreign key (egc_prs_id)
  references personnage (prs_id)  ;
alter table boutique add constraint fk_btq_personnage
  foreign key (btq_prs_id)
  references personnage (prs_id)  ;
alter table boutique add constraint fk_btq_automate
  foreign key (btq_atm_id)
  references automate (atm_id)  ;
alter table boutique add constraint fk_btq_coffre
  foreign key (btq_cff_id)
  references coffre (cff_id)  ;
alter table automate_carac add constraint fk_acr_automate
  foreign key (acr_atm_id)
  references automate (atm_id)  ;
alter table automate_carac add constraint fk_acr_carac
  foreign key (acr_crc_id)
  references caracteristique (crc_id)  ;
alter table achat_element_gen add constraint fk_aeg_boutique
  foreign key (aeg_btq_id)
  references boutique (btq_id)  ;
alter table achat_element_gen add constraint fk_aeg_element_gen
  foreign key (aeg_egn_id)
  references element_gen (egn_id)  ;
alter table coffre_element add constraint fk_mce_monnaie
  foreign key (cel_mnn_id)
  references monnaie (mnn_id)  ;
alter table achat_element_gen add constraint fk_mae_monnaie
  foreign key (aeg_mnn_id)
  references monnaie (mnn_id)  ;
alter table secteur add constraint fk_ste_typevn
  foreign key (sct_tev_id)
  references type_environnement (tev_id)  ;
alter table type_environnement add constraint fk_tec_carac
  foreign key (tev_crc_id)
  references caracteristique (crc_id)  ;
alter table reponse add constraint fk_rq_question
  foreign key (rep_que_id)
  references question (que_id)  ;
alter table news add constraint fk_mn_mj
  foreign key (new_mji_id)
  references mj (mji_id)  ;
alter table candidature add constraint fk_jc_joueur
  foreign key (can_jou_id)
  references joueur (jou_id)  ;
alter table profil_annonce add constraint fk_map_mj
  foreign key (pro_mji_id)
  references mj (mji_id)  ;
alter table candidature add constraint fk_pac_profil
  foreign key (can_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_annonce add constraint fk_cpp_catpro
  foreign key (pro_ctp_id)
  references categorie_profil (ctp_id)  ;
alter table candidature add constraint fk_cxc_sexe
  foreign key (can_sex_id)
  references sexe (sex_id)  ;
alter table profil_classe add constraint fk_pac_proann
  foreign key (pcl_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_race add constraint fk_par_proann
  foreign key (pra_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_sexe add constraint fk_pas_proann
  foreign key (psx_pro_id)
  references profil_annonce (pro_id)  ;
alter table candidature add constraint fk_cr_race
  foreign key (can_rac_id)
  references race (rac_id)  ;
alter table candidature add constraint fk_cc_classe
  foreign key (can_cls_id)
  references classe (cls_id)  ;
alter table profil_classe add constraint fk_cpc_classe
  foreign key (pcl_cls_id)
  references classe (cls_id)  ;
alter table profil_race add constraint fk_pr_race
  foreign key (pra_rac_id)
  references race (rac_id)  ;
alter table profil_sexe add constraint fk_ps_sexe
  foreign key (psx_sex_id)
  references sexe (sex_id)  ;

-- Application maladroite des droits de lecture des sequenceurs aux mj
GRANT SELECT, UPDATE ON personnage_prs_id_seq TO supermj;
GRANT SELECT, UPDATE ON itineraire_itn_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_trace_ttr_id_seq TO supermj;
GRANT SELECT, UPDATE ON forme_frm_id_seq TO supermj;
GRANT SELECT, UPDATE ON race_rac_id_seq TO supermj;
GRANT SELECT, UPDATE ON sexe_sex_id_seq TO supermj;
GRANT SELECT, UPDATE ON classe_cls_id_seq TO supermj;
GRANT SELECT, UPDATE ON classe_niveau_cln_id_seq TO supermj;
GRANT SELECT, UPDATE ON chemin_chm_id_seq TO supermj;
GRANT SELECT, UPDATE ON saison_ssn_id_seq TO supermj;
GRANT SELECT, UPDATE ON lieu_lie_id_seq TO supermj;
GRANT SELECT, UPDATE ON secteur_sct_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_environnement_tev_id_seq TO supermj;
GRANT SELECT, UPDATE ON monde_mnd_id_seq TO supermj;
GRANT SELECT, UPDATE ON element_elm_id_seq TO supermj;
GRANT SELECT, UPDATE ON technologie_tcn_id_seq TO supermj;
GRANT SELECT, UPDATE ON matiere_mtr_id_seq TO supermj;
GRANT SELECT, UPDATE ON caracteristique_crc_id_seq TO supermj;
GRANT SELECT, UPDATE ON categorie_caracteristique_ccr_id_seq TO supermj;
GRANT SELECT, UPDATE ON langue_lng_id_seq TO supermj;
GRANT SELECT, UPDATE ON maladie_mld_id_seq TO supermj;
GRANT SELECT, UPDATE ON monnaie_mnn_id_seq TO supermj;
GRANT SELECT, UPDATE ON compagnie_cmp_id_seq TO supermj;
GRANT SELECT, UPDATE ON joueur_jou_id_seq TO supermj;
GRANT SELECT, UPDATE ON mj_mji_id_seq TO supermj;
GRANT SELECT, UPDATE ON css_css_id_seq TO supermj;
GRANT SELECT, UPDATE ON action_act_id_seq TO supermj;
GRANT SELECT, UPDATE ON element_gen_egn_id_seq TO supermj;
GRANT SELECT, UPDATE ON arme_gen_agn_id_seq TO supermj;
GRANT SELECT, UPDATE ON armure_gen_ugn_id_seq TO supermj;
GRANT SELECT, UPDATE ON vaisseau_gen_vgn_id_seq TO supermj;
GRANT SELECT, UPDATE ON automate_atm_id_seq TO supermj;
GRANT SELECT, UPDATE ON boutique_btq_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_munition_tmt_id_seq TO supermj;
GRANT SELECT, UPDATE ON munition_mnt_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_element_tel_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_vaisseau_tvs_id_seq TO supermj;
GRANT SELECT, UPDATE ON vaisseau_gen_vsg_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_vaisseau_tvs_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_poste_tpt_id_seq TO supermj;
GRANT SELECT, UPDATE ON zone_zon_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_degat_tdg_id_seq TO supermj;
GRANT SELECT, UPDATE ON filature_flt_id_seq TO supermj;
GRANT SELECT, UPDATE ON mime_mim_id_seq TO supermj;
GRANT SELECT, UPDATE ON rumeur_rmr_id_seq TO supermj;
GRANT SELECT, UPDATE ON rumeur_ecoute_rmc_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_lieu_tpl_id_seq TO supermj;
GRANT SELECT, UPDATE ON debit_element_dbt_id_seq TO supermj;
GRANT SELECT, UPDATE ON vente_element_vnt_id_seq TO supermj;
GRANT SELECT, UPDATE ON depot_element_dpt_id_seq TO supermj;
GRANT SELECT, UPDATE ON banque_bnq_id_seq TO supermj;
GRANT SELECT, UPDATE ON arme_arm_id_seq TO supermj;
GRANT SELECT, UPDATE ON armure_aru_id_seq TO supermj;
GRANT SELECT, UPDATE ON recharge_rch_id_seq TO supermj;
GRANT SELECT, UPDATE ON vaisseau_vss_id_seq TO supermj;
GRANT SELECT, UPDATE ON coffre_cff_id_seq TO supermj;
GRANT SELECT, UPDATE ON calque_clq_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_calque_tcq_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_vent_tvt_id_seq TO supermj;
GRANT SELECT, UPDATE ON lieu_lie_id_seq TO supermj;
GRANT SELECT, UPDATE ON image_img_id_seq TO supermj;
GRANT SELECT, UPDATE ON vehicule_vhc_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_vehicule_tvh_id_seq TO supermj;
GRANT SELECT, UPDATE ON type_poste_tps_id_seq TO supermj;
