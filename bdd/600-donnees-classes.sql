-- Mage (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (1, 'Mage', 'Le mage est un lanceur de sort', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (1, 1, 1, 'Mage débutant', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (2, 1, 2, 'Mage initié', null, false);

-- Guerrier (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (2, 'Guerrier', 'Le guerrier peut etre une brute ou un tactitien', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (3, 2, 1, 'Tapouilleur', null, false);

-- Prêtre (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (3, 'Pretre', 'Un prêtre se doit de prier son dieu.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (4, 3, 1, 'Fidèle', null, true);

-- Acrobate (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (4, 'Acrobate', 'L''acrobate est doué pour sauter dans tous les sens.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (5, 4, 1, 'Gesticuleur', null, true);

-- Lumineux (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (5, 'Lumineux', 'Avoir un comportement louable tend au coté lumineux.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (6, 5, 1, 'Bon', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (7, 5, 2, 'Blanc', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (8, 5, 3, 'Lumineux', null, true);

-- Obscur (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (6, 'Obscur', 'Egoiste, attiré par la puissance, le pouvoir: tendre vers l''obscurité.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (9, 6, 1, 'Mauvais', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (10, 6, 2, 'Noir', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (11, 6, 3, 'Obscur', null, true);

-- Tank (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (7, 'Tank', 'Encaisser les coups n''est pas donné à tout le monde.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (12, 7, 1, 'Résistant', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (13, 7, 2, 'Tank', null, false);

-- Meneur (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (8, 'Meneur', 'LE meneur d''hommes, le vrai, pret à envoyer se battre !', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (14, 8, 1, 'Causeur public', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (15, 8, 2, 'Meneur d''hommes', null, false);

-- MJ lieu (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
    VALUES (9, 'MJ Lieu', 'Le MJ lieu peut créer des chemins, des lieux.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
    VALUES (16, 9, 1, 'MJ Lieu', null, false);

-- Marchand (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (10, 'Marchand', 'Le marchand achète/vends, gère des boutiques', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
        VALUES (17, 10, 1, 'Colporteur en herbe', null, true);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
        VALUES (18, 10, 2, 'Marchand', null, true);

