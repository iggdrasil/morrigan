-- Chemin PLACE <-> TAVERNE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (1, 1, 2, 'Vous poussez la porte de la taverne. La joie de l''alcool s''offre à vous.', 'Vous quittez les bruits et conversations pour gagner la place.', true, true, true, null, 0, 0, 1, 1, 22);


-- Chemin TAVERNE <-> REMISE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (2, 2, 3, 'Vous utilisez la clé de la remise pour y pénétrer.', 'Vous utilisez la clé de la remise pour la quitter.', true, true, true, 1, 0, 0, 1, 1, 22);

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (3, 2, 3, 'Vous quittez la taverne pour la remise.', 'Vous quittez la remise pour la taverne.', true, true, true, null, 0, 0, 3, 1, 22);


-- Chemin REMISE -> PLACE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (4, 3, 1, 'Vous vous glissez par la lucarne et retombez en contre-bas sur une ruelle adjacente à la place.', null, true, false, true, null, 30, 10, 4, 1, 22);


-- Forêt <-> New-Seattle

--INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
--	VALUES (5, 1, 1395, 'Sortant de la cité en poussant le petit portillon de bois vermoulu, vous gagnez une jolie forêt...', 'Après avoir erré  dans la forêt, passant ses portes magnifiques vous entrez de la cité...', TRUE, TRUE, TRUE, null, 0, 0, 1, 1, 22);



