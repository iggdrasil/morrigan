
--- des scores moyens pour tous

UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 1; -- Force
UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 2; -- Dex
UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 3; -- Const


-- Siltaom est un mago de l'eau

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 1 AND pcr_crc_id = 4; -- Int

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 1 AND pcr_crc_id = 5; -- Sag

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 1 AND pcr_crc_id = 6; -- Charisme (mieux en vampire)

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 1 AND pcr_crc_id = 13; -- Eau

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 1 AND pcr_crc_id = 7; -- Occultisme

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 1 AND pcr_crc_id = 22; -- Étiquette

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 1 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 1 AND pcr_crc_id = 24; -- Langue elfique


-- Nimnae est une prêtresse
UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 2 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 512, pcr_score_xp_max = 512 WHERE pcr_prs_id = 2 AND pcr_crc_id = 2;

UPDATE perso_carac SET pcr_score_xp = 768, pcr_score_xp_max = 768 WHERE pcr_prs_id = 2 AND pcr_crc_id = 3; -- Const

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 2 AND pcr_crc_id = 4; -- Pas très fute-fute

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 2 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 2 AND pcr_crc_id = 6; -- Mais elle impose

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 9; -- Epée

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 12; -- Ténèbres

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 14; -- Herboriste

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 2 AND pcr_crc_id = 7; -- Occultisme

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 20; -- Un petit pêché ...

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 2 AND pcr_crc_id = 22; -- Étiquette

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 24; -- Langue elfique

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 25; -- Draconique


-- Arkenlond est un bourrin

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 2; -- Dext

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 3; -- Const

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 3 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 8; -- Mains nues

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 10; -- Et les deux en plus !

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 3 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 0 WHERE pcr_prs_id = 3 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 0 WHERE pcr_prs_id = 3 AND pcr_crc_id = 12;


-- Aegon est un petit mago chétif (mais attention il déchire grâce à sa maladie de dragon)

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 4 AND pcr_crc_id = 1; -- Force
UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 4 AND pcr_crc_id = 3; -- Constitution

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 4 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 4 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 4 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 4 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 4 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 4 AND pcr_crc_id = 24; -- Langue elfique

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 4 AND pcr_crc_id = 25; -- Draconique


-- Syl est... heu... un gringalet qui sait tirer à l'arc (le bigre, c'est bien le seul)
UPDATE perso_carac SET pcr_score_xp = 640, pcr_score_xp_max = 640 WHERE pcr_prs_id = 5 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 5 AND pcr_crc_id = 2; -- Dext

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 5 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 512 WHERE pcr_prs_id = 5 AND pcr_crc_id = 19;

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 5 AND pcr_crc_id = 8;

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 5 AND pcr_crc_id = 21;

UPDATE perso_carac SET pcr_score_xp = 512 WHERE pcr_prs_id = 5 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 5 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 5 AND pcr_crc_id = 24; -- Langue elfique


-- Sulfure est une brute (ne pas négliger les menues réparations de vêtements)
-- Epee, const et force sont ses points forts.

UPDATE perso_carac SET pcr_score_xp = 1580, pcr_score_xp_max = 1580 WHERE pcr_prs_id = 6 AND pcr_crc_id = 1;

UPDATE perso_carac SET pcr_score_xp = 640, pcr_score_xp_max = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 2;

UPDATE perso_carac SET pcr_score_xp = 1152, pcr_score_xp_max = 1152 WHERE pcr_prs_id = 6 AND pcr_crc_id = 3;

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 6 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 6 AND pcr_crc_id = 8;

UPDATE perso_carac SET pcr_score_xp = 1408 WHERE pcr_prs_id = 6 AND pcr_crc_id = 9;

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 6 AND pcr_crc_id = 10;

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 6 AND pcr_crc_id = 20;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 23; -- Langue humaine


-- Tout le monde a un minimum en Millianais histoire de causer un peu
UPDATE perso_carac set pcr_score_xp=1024, pcr_score_xp_max=1024 where pcr_crc_id=48;
