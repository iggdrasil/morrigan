
INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (1, 'Elfe', null, 'L''elfe peut s''apparenter a l''humain. De taille superieur a ce dernier, il se differencie plus particulierement de lui par ses oreilles pointues.', false, 35);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (2, 'Orc', null, 'L''orc est une creature de physionomie musclee. Sa sale tete et son manque de savoir-vivre le font regulierement remarquer.', false, 32);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (3, 'Humain', null, 'L''humain est de taille moyenne. Plus trapu que l''elfe, moins costaud que l''orc, il presente des facultes transversales comme la couture, les bains de boue ou la maroquinerie.', false, 30);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (4, 'Vampire', null, 'Le vampire est a part. Par son affinite avec la nuit comme son gout pour le sang, il n''est guere ouvertement apprecie. Sauf parmi ses congeneres...', false, 30);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (5, 'Dragon', null, 'Le dragon est un animal aujourd''hui domestique. Dote de quatre pattes (dont deux ridicules), il peut tres bien courir et voler partout dans l''appartement. Mefiance...', true, 50);
	
insert into race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille) 
    values (6, 'Nain', NULL, 'Trapu voire même trapu, franc et têtu, gagner l''amitié d''un nain est précieux, s''en faire un ennemi est mortel.', FALSE, 22);
