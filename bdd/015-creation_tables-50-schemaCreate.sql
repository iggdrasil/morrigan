
-- Generated SQL Schema
-- --------------------------------------------------------------------


-- personnage
create table personnage (
  prs_id                    integer default nextval('personnage_prs_id_seq') not null,	-- Identifiant du personnage
  prs_jou_id                integer,	-- Identifiant du joueur jouant le personnage
  prs_principal             boolean NOT NULL,	-- Personnage par defaut du joueur (oui/non)
  prs_possesseur_prs_id     integer,	-- Identifiant du personnage possedant celui-ci
  prs_lie_id                integer,	-- Identifiant du lieu
  prs_nom_originel          varchar(50) NOT NULL,	-- Nom de depart
  prs_nom_actuel            varchar(50),	-- Nom actuel
  prs_background            varchar(5000),	-- Passe du personnage
  prs_biographie            varchar(5000),	-- Histoire du personnage
  prs_inscription           timestamp,	-- Date d'inscription
  prs_derniere_connexion    timestamp,	-- Date de derniere connexion
  prs_pa                    smallint NOT NULL,	-- Nombre de Points d'Action
  prs_pa_max                smallint NOT NULL,	-- Nombre de Points d'Action maximum
  prs_pn                    smallint NOT NULL,	-- Nombre de Points de Nourriture
  prs_pn_max                smallint NOT NULL,	-- Nombre de Points de Nourriture maximum
  prs_pp                    smallint NOT NULL,	-- Nombre de Points de Paralysie
  prs_pp_max                smallint NOT NULL,	-- Nombre de Points de Paralysie maximum
  prs_pv                    smallint NOT NULL,	-- Nombre de Points de Vie
  prs_pv_max                smallint NOT NULL,	-- Nombre de Points de Vie maximum
  prs_pc                    smallint NOT NULL,	-- Points de combat
  prs_pc_max                smallint NOT NULL,	-- Maximum de point de combat
  prs_tpr_id                smallint NOT NULL,	-- Type de personnage: pj, pnj, familier, etc.
  prs_calc_facteur_nb_trace float default 1 NOT NULL,	-- Facteur influant sur le nombre de trace calcul� en fonction de ses attributs, d'�ventuelles comp�tences, de son �quipement, etc.
  prs_date_arrivee          timestamp not null,	-- Date d'arriv�e dans un lieu
  prs_delta_rose            integer default 0 NOT NULL,	-- Delta de d�calage par rapport � la rose des vents
  constraint pk_Personnage primary key (prs_id)
) ;

-- joueur
create table joueur (
  jou_id                    integer default nextval('joueur_jou_id_seq') not null,	-- Identifiant
  jou_pseudo                varchar(32) NOT NULL,	-- Pseudonyme
  jou_mdp                   varchar(32) NOT NULL,	-- Mot de passe (md5)
  jou_email                 varchar(50) NOT NULL,	-- Adresse mail
  jou_css_id                integer,	-- Style CSS utilis� par le joueur
  jou_fuseau                smallint,	-- Decalage horaire par rapport a l'heure GMT
  jou_session_id            varchar(64),	-- Identifiant de session
  jou_scorequizz            integer,	-- Score au quizz
  jou_ipinitiale            inet,	-- IP d'inscription
  jou_ipcourante            inet,	-- Derni�re IP connue
  jou_commentaire           text,	-- �ventuel commentaire de l'utilisateur
  constraint pk_Joueur primary key (jou_id)
) ;

-- mj
create table mj (
  mji_id                    integer default nextval('mj_mji_id_seq') not null,	-- Identifiant
  mji_jou_id                integer NOT NULL,	-- Identifiant du joueur
  mji_nom                   varchar(50) NOT NULL,	-- Nom
  mji_derniere_connexion    timestamp with time zone,	-- Date de derniere connexion
  mji_droits_news           boolean default False,	-- R�dacteur de news (O/N)
  mji_droits_all_news       boolean default False,	-- Administrateur des news
  mji_droits_inscription    boolean default False,	-- Droit de proposition de profil
  mji_droits_recrutement    boolean default False,	-- Administrateur des incriptions
  mji_droits_edit_regles    boolean default False,	-- R�dacteur wiki r�gles
  mji_droits_edit_monde     boolean default False,	-- R�dacteur wiki background
  mji_droits_edit_admin     boolean default False,	-- Administrateur technique
  constraint pk_Mj primary key (mji_id)
) ;

-- perso_classe
create table perso_classe (
  pce_id                    serial not null,	-- Identifiant abstrait
  pce_prs_id                integer NOT NULL,	-- Personnage
  pce_cln_id                integer NOT NULL,	-- Niveau de classe
  pce_titre                 varchar(64),	-- Titre accord� au personnage (si diff�rent de celui de classe)
  constraint pk_Perso_classe primary key (pce_id)
) ;

-- forme
create table forme (
  frm_id                    integer default nextval('forme_frm_id_seq') not null,	-- Identifiant
  frm_prs_id                integer NOT NULL,	-- Identifiant du personnage pouvant prendre cette forme
  frm_rac_id                smallint NOT NULL,	-- Identifiant de la race
  frm_sex_id                integer NOT NULL,	-- Sexe
  frm_indice_taille         integer NOT NULL,	-- Indice de taille de la forme
  frm_actuelle              boolean,	-- Definit la forme actuelle du personnage (oui/non)
  frm_description           character varying(2048),	-- Description physique
  frm_desc_actuelle         varchar(1024),	-- Description de l'etat actuel
  frm_url_image             varchar(128),	-- URL de l'image illustrant le personnage sous cette forme
  frm_ttr_id                integer default 1 NOT NULL,	-- Type de trace de la forme
  frm_profond_trace         integer default 4 NOT NULL,	-- Profondeur de la trace laiss�e
  frm_taille_trace          integer default 4 NOT NULL,
  constraint pk_Forme primary key (frm_id)
) ;

-- classe_niveau
create table classe_niveau (
  cln_id                    integer default nextval('classe_niveau_cln_id_seq') not null,	-- Identifiant
  cln_cls_id                integer NOT NULL,	-- Classe
  cln_niveau                smallint NOT NULL,	-- Niveau dans la classe
  cln_mld_id                smallint,	-- Maladie de classe
  cln_titre                 varchar(50) NOT NULL,	-- Titre donn� au d�tenteur de ce niveau de classe
  cln_titre_visible         boolean,	-- Titre visible par les autres (oui/non)
  cln_desc_mj               varchar(128),	-- Description pour MJ
  constraint pk_Classe_niveau primary key (cln_id)
) ;

-- classe
create table classe (
  cls_id                    integer default nextval('classe_cls_id_seq') not null,	-- Identifiant
  cls_nom                   varchar(50) NOT NULL,	-- Nom
  cls_desc_mj               varchar(128),	-- Description � l'adresse des MJ
  cls_tcl_id                integer NOT NULL,	-- Type de classe: classe, aptitude
  cls_insc_ouverte          boolean default TRUE NOT NULL,	-- Inscription ouverte pour cette classe
  constraint pk_Classe primary key (cls_id)
) ;

-- perso_maladie
create table perso_maladie (
  pml_id                    serial not null,	-- Identifiant abstrait
  pml_prs_id                integer NOT NULL,	-- Personnage malade
  pml_mld_id                integer NOT NULL,	-- Maladie
  pml_debut                 date NOT NULL,	-- Date d'apparition de la maladie
  pml_fin                   date,	-- Eventuelle date de fin de la maladie
  constraint pk_Perso_maladie primary key (pml_id)
) ;

-- maladie
create table maladie (
  mld_id                    integer default nextval('maladie_mld_id_seq') not null,	-- Identifiant
  mld_nom                   varchar(32) NOT NULL,	-- Nom
  mld_description           varchar(1500),	-- Description generale
  mld_tml_id                integer NOT NULL,	-- Type de maladie
  mld_duree                 smallint,	-- Duree en tours
  mld_periode               integer,	-- Periode en tours entre 2 effets
  mld_description_physique  varchar(200),	-- Description per�ue par un tiers sur le malade (vide si rien de visible)
  mld_formule               varchar(500),	-- Formule � appliquer pour trouver les caract�ristiques actuelles
  constraint pk_Maladie primary key (mld_id)
) ;

-- caracteristique
create table caracteristique (
  crc_id                    integer default nextval('caracteristique_crc_id_seq') not null,	-- Identifiant
  crc_nom                   varchar(50) NOT NULL,	-- Nom
  crc_description           varchar(1500),	-- Description de son utilisation
  crc_tcr_id                integer,	-- Type de caracteristique
  crc_ncr_id                integer,	-- Nature
  crc_delta_min_max         integer default 0 NOT NULL,	-- Delta minimum et maximum
  crc_visible               boolean default TRUE NOT NULL,	-- � vrai si le score dans cette caract�ristiques est visible par le joueur
  constraint pk_Caracteristique primary key (crc_id)
) ;

-- perso_carac
create table perso_carac (
  pcr_id                    serial not null,	-- Identifiant abstrait
  pcr_prs_id                integer NOT NULL,	-- Personnage
  pcr_crc_id                integer NOT NULL,	-- Caracteristique
  pcr_jauge_entrainement    integer default 0 NOT NULL,	-- Jauge d'entrainement (entre 0 et 100)
  pcr_score_xp              integer default 0 NOT NULL,	-- Nombre de points d'experience actuel dans la caracteristique
  pcr_score_xp_max          integer default 0 NOT NULL,	-- Nombre de points d'experience maximum atteint dans la caracteristique
  pcr_score_calcule         smallint default 0 NOT NULL,	-- Score apres calcul a partir de pcr_score_xp apres application des differentes maladies, classes, etc.
  pcr_score_max             integer default 0 NOT NULL,	-- Score absolu pouvant etre atteint en fonction de l'age, la race, le sexe du personnage
  constraint pk_Perso_carac primary key (pcr_id)
) ;

-- parametre
create table parametre (
  prm_id                    smallint not null,	-- Identifiant
  prm_ctg_id                smallint NOT NULL,	-- Ensemble d'appartenance
  prm_libelle               varchar(50) NOT NULL,	-- Libelle explicitant a quoi il sert
  prm_valeur                varchar(30) NOT NULL,	-- Valeur du parametre (c'est un varchar mais des entiers ou des dates peuvent y etre entres)
  prm_description           varchar(500) NOT NULL,	-- Description de ce qui peut etre mis dans la valeur
  constraint pk_Parametre primary key (prm_id)
) ;

-- categorie
create table categorie (
  ctg_id                    smallint not null,	-- Identifiant
  ctg_libelle               varchar(50) NOT NULL,	-- Libelle de l'ensemble de parametres
  ctg_systeme               boolean NOT NULL,	-- Regroupement de parametres systemes (oui/non : modifiable ou non)
  constraint pk_Categorie primary key (ctg_id)
) ;

-- perso_classe_int
create table perso_classe_int (
  pci_id                    serial not null,	-- Identifiant abstrait
  pci_prs_id                integer NOT NULL,	-- Personnage
  pci_cls_id                integer NOT NULL,	-- Classe autoris�e/interdite
  pci_type_interdit         smallint NOT NULL,	-- Interdiction de la classe pour le personnage (oui/non)
  constraint pk_Perso_classe_int primary key (pci_id)
) ;

-- prerequis_classe
create table prerequis_classe (
  pqc_id                    serial not null,	-- Identifiant abstrait
  pqc_cln_id                integer NOT NULL,	-- Niveau de classe a atteindre
  pqc_cls_id                integer NOT NULL,	-- Classe necessaire
  pqc_niveau_min            integer,	-- Niveau minimum a avoir dans la classe necessaire 
  constraint pk_Prerequis_classe primary key (pqc_id)
) ;

-- monde
create table monde (
  mnd_id                    integer default nextval('monde_mnd_id_seq') not null,	-- Identifiant
  mnd_nom                   varchar(64) NOT NULL,	-- Nom
  mnd_tmn_id                integer NOT NULL,	-- Type de monde
  mnd_description           varchar(1500),	-- Description g�n�rale du monde
  mnd_pere_mnd_id           integer,	-- Monde contenant celui-ci
  mnd_niveau                integer,	-- Indice de niveau
  mnd_mld_id                integer,	-- Maladie r�gnant sur ce monde
  mnd_image                 varchar(64),	-- Image du monde (fond de la carte)
  constraint pk_Monde primary key (mnd_id)
) ;

-- lieu
create table lieu (
  lie_id                    integer default nextval('lieu_lie_id_seq') not null,	-- Identifiant
  lie_tli_id                integer NOT NULL,	-- Type de lieu
  lie_nom                   varchar(50),	-- Nom
  lie_sct_id                integer,	-- Secteur d'appartenance
  lie_indice_taille         integer,	-- Taille du lieu
  lie_indice_population     integer default 0 NOT NULL,	-- Indice de densit� de population
  lie_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  lie_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de trace
  lie_carto                 boolean default FALSE,	-- � vrai si il s'agit d'un lieu carto
  lie_desc                  varchar(1500),	-- Pr�cision �ventuelle sur la description du lieu
  lie_img_id                integer,	-- �ventuelle image associ�e
  constraint pk_Lieu primary key (lie_id)
) ;

-- secteur
create table secteur (
  sct_id                    integer default nextval('secteur_sct_id_seq') not null,	-- Identifiant
  sct_nom                   varchar(50) NOT NULL,	-- Nom
  sct_description           varchar(1500),	-- Description destin�e aux personnages se trouvant dans le secteur
  sct_tsc_id                integer NOT NULL,	-- Type de secteur
  sct_charge_max            integer,	-- Charge maximale support�e par le secteur (null signifie infini)
  sct_indice_population     integer default 0 NOT NULL,	-- Indice de densit� de population
  sct_vhc_id                integer,	-- Identifiant de l'�ventuel v�hicule associ� � un secteur
  sct_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout journalier en pn
  sct_cout_pv               varchar(64) default '0' NOT NULL,	-- Cout journalier en PV
  sct_difficulte_rumeur     integer default 0 NOT NULL,	-- Difficult� de lancement d'une rumeur
  sct_tev_id                integer NOT NULL,
  constraint pk_Secteur primary key (sct_id)
) ;

-- chemin
-- Lien entre 2 endroits (lieu <-> lieu ou lieu <->monde)
create table chemin (
  chm_id                    integer default nextval('chemin_chm_id_seq') not null,	-- Identifiant
  chm_egn_id                integer,	-- Element generique servant de cl�
  chm_tch_id                smallint NOT NULL,	-- Type de chemin (d'acces)
  chm_pch_id                integer,	-- �ventuelle particularit� associ�e � un chemin (champs textuel du style � par la fen�tre �,  � par la porte �). Permet notamment de diff�rencier diff�rents chemins venant du m�me endroit et allant au m�me endroit.
  chm_depart_lie_id         integer NOT NULL,	-- Lieu de d�part
  chm_arrivee_lie_id        integer NOT NULL,	-- Lieu d'arriv�e
  chm_desc_aller            varchar(1500),	-- Description du trajet dans le sens aller
  chm_desc_retour           varchar(1500),	-- Description du trajet dans le sens retour
  chm_aller                 boolean,	-- Trajet possible dans le sens aller (oui/non)
  chm_retour                boolean,	-- Trajet possible dans le sens retour (oui/non)
  chm_ouvert                boolean,	-- Trajet actuellement ouvert (oui/non) (pr�vaut sur chm_aller et chm_retour)
  chm_discretion            smallint default 0 NOT NULL,	-- Discretion
  chm_difficulte            integer default 0 NOT NULL,	-- Seuil de difficult�
  chm_cout_pa               varchar(64) default '0' NOT NULL,	-- Cout du trajet en Points d'Action
  chm_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout en point de nourriture
  chm_cout_echec_pv         varchar(64) default '0' NOT NULL,	-- Cout en PV en cas d'�chec
  chm_indice_taille         integer,	-- Indice de taille du passage accord� par le chemin
  chm_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  chm_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de traces
  chm_echec_bloquant        boolean default FALSE NOT NULL,	-- � vrai si l'�chec empeche d'emprunter le chemin
  chm_crc_id                integer NOT NULL,	-- Caract�ristique associ�e
  chm_crc_2_id              integer,	-- �ventuelle seconde caract�ristique intervenant dans la formule
  constraint pk_Chemin primary key (chm_id)
) ;

-- saison
create table saison (
  ssn_id                    integer default nextval('saison_ssn_id_seq') not null,	-- Identifiant
  ssn_nom                   varchar(32) NOT NULL,	-- Nom
  ssn_ordre                 smallint,	-- Ordre pendant un cycle
  ssn_default               boolean,	-- Saison par d�faut
  constraint pk_Saison primary key (ssn_id)
) ;

-- element
create table element (
  elm_id                    integer default nextval('element_elm_id_seq') not null,	-- Identifiant
  elm_egn_id                integer NOT NULL,	-- Identifiant de l'element generique associe
  elm_nom                   varchar(64),	-- Nom
  elm_desc                  varchar(500),	-- Description de l'element
  elm_prix                  integer,	-- Prix de l'element en monnaie arbitraire
  elm_equipable             boolean,	-- Definit si l'element peut etre equipe ou non
  elm_equipe                boolean,	-- Definit si l'element est equipe ou non
  elm_enraye                boolean,	-- Definit si l'element est enraye ou non
  elm_naturel               boolean,	-- Definit si l'element est naturel chez son possesseur
  elm_prs_id                integer,	-- Identifiant de son possesseur
  elm_lie_id                integer,	-- Lieu ou a ete jete l'element
  constraint pk_Element primary key (elm_id)
) ;

-- monnaie
create table monnaie (
  mnn_id                    integer default nextval('monnaie_mnn_id_seq') not null,	-- Identifiant de la monnaie
  mnn_nom                   varchar(50) NOT NULL,	-- Nom de la monnaie
  mnn_indice                smallint NOT NULL,	-- Indice arbitraire de la monnaie (voire cote)
  constraint pk_Monnaie primary key (mnn_id)
) ;

-- perso_monnaie
-- Monnaie que porte le personnage
create table perso_monnaie (
  pmn_id                    serial not null,	-- Identifiant abstrait
  pmn_prs_id                integer NOT NULL,	-- Identifiant du personnage
  pmn_mnn_id                integer NOT NULL,	-- Identifiant de la monnaie
  pmn_montant               smallint,	-- Qte de monnaie du personnage
  constraint pk_Perso_monnaie primary key (pmn_id)
) ;

-- compagnie
-- Compagnie de personnages
create table compagnie (
  cmp_id                    integer default nextval('compagnie_cmp_id_seq') not null,	-- Identifiant de la compagnie
  cmp_nom                   varchar(100) NOT NULL,	-- Nom de la compagnie
  constraint pk_Compagnie primary key (cmp_id)
) ;

-- place_compagnie
-- Place des personnages dans la compagnie
create table place_compagnie (
  plc_id                    serial not null,
  plc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  plc_cmp_id                integer NOT NULL,	-- Identifiant de la compagnie
  plc_place                 smallint,	-- Place du personnage ds la compagnie
  constraint pk_Place_compagnie primary key (plc_id)
) ;

-- sexe
create table sexe (
  sex_id                    integer default nextval('sexe_sex_id_seq') not null,	-- Identifiant
  sex_nom                   varchar(50) NOT NULL,	-- Nom du sexe (masculin, f�minin, asexu�, ...)
  sex_insc_ouverte          boolean default TRUE NOT NULL,	-- Sexe disponible pour l'inscription
  constraint pk_Sexe primary key (sex_id)
) ;

-- type_personnage
create table type_personnage (
  tpr_id                    integer not null,	-- Identifiant
  tpr_nom                   varchar(50) NOT NULL,	-- Nom du type (PJ, PNJ, familier, Gob, ...)
  constraint pk_Type_personnage primary key (tpr_id)
) ;

-- type_maladie
create table type_maladie (
  tml_id                    integer not null,	-- Identifiant
  tml_nom                   varchar(50) NOT NULL,	-- Nom du type (classe, race, instantan�e, d�finitive, temporaire)
  constraint pk_Type_maladie primary key (tml_id)
) ;

-- type_caracteristique
create table type_caracteristique (
  tcr_id                    integer not null,	-- Identifiant
  tcr_nom                   varchar(50) NOT NULL,	-- Nom du type (attribut, comp�tence)
  constraint pk_Type_caracteristique primary key (tcr_id)
) ;

-- nature_caracteristique
create table nature_caracteristique (
  ncr_id                    integer not null,	-- Identifiant
  ncr_nom                   varchar(50) NOT NULL,	-- Nom de la nature (intellectuelle, physique, spirituelle, ...)
  constraint pk_Nature_caracteristique primary key (ncr_id)
) ;

-- type_secteur
create table type_secteur (
  tsc_id                    integer not null,	-- Identifiant
  tsc_nom                   varchar(50) NOT NULL,	-- Nom du type: immobile, mobile (monde), mobile (galaxie), mobile (v�hicule)
  constraint pk_Type_secteur primary key (tsc_id)
) ;

-- type_monde
create table type_monde (
  tmn_id                    integer not null,	-- Identifiant
  tmn_nom                   varchar(50) NOT NULL,	-- Nom du type: galaxie (sans appui au sol), plan�te (avec appui)
  constraint pk_Type_monde primary key (tmn_id)
) ;

-- type_chemin
create table type_chemin (
  tch_id                    integer not null,	-- Identifiant
  tch_nom                   varchar(50) NOT NULL,	-- Nom du type: classique, acc�s trappe, acc�s sas, acc�s pont
  constraint pk_Type_chemin primary key (tch_id)
) ;

-- action
create table action (
  act_id                    integer default nextval('action_act_id_seq') not null,	-- Identifiant
  act_nom                   varchar(64) NOT NULL,	-- Nom
  act_nom_form              varchar(20) NOT NULL,	-- Nom de l'action dans le formulaire
  act_desc                  varchar(100),	-- Description de l'action
  act_form_attaque          varchar(128) NOT NULL,	-- Formule a resoudre pour l'attaque
  act_form_defense          varchar(128) NOT NULL,	-- Formule a resoudre pour la defense
  act_form_cout_pa          varchar(64),	-- Formule determinant le cout en PA
  act_form_cout_pn          varchar(64),	-- Formule determinant le cout en PN
  act_form_cout_pp          varchar(64),	-- Formule determinant le cout en PP
  act_form_cout_pv          varchar(64),	-- Formule determinant le cout en PV
  act_form_cout_pc          varchar(64),	-- Cout en point de combat de l'action
  act_form_gain_min         varchar(128) NOT NULL,	-- Calcul du gain minimum de Points d''eXperience
  act_form_gain_max         varchar(128) NOT NULL,	-- Calcul du gain maximum de Points d''eXperience
  act_courante              boolean NOT NULL,	-- Action proposee en tout lieu/pour toute classe ? (oui/non)
  act_aide                  varchar(1000),
  act_combat                boolean default FALSE,	-- � vrai si disponible en situation de combat
  constraint pk_Action primary key (act_id)
) ;

-- lieu_action
create table lieu_action (
  lac_id                    serial not null,
  lac_act_id                integer NOT NULL,	-- Identifiant de l'action sp�cialement consid�r�e en ce lieu
  lac_lie_id                integer NOT NULL,	-- Identifiant du lieu ayant une action particuliere
  lac_possible              boolean,	-- Action possible physiquement en ce lieu ? (oui/non) (rempli si diff�rent du standard)
  lac_autorisee             boolean,	-- Action autoris�e (moralement) en ce lieu ? (oui/non) (rempli si diff�rent du standard)
  lac_difficulte            integer,	-- Difficulte de l'action en ce lieu (rempli si diff�rent du standard)
  lac_cout_pa               integer,	-- Co�t en PA de l'action en ce lieu (rempli si diff�rent du standard)
  lac_cout_pn               integer,	-- Cout en PN de l'action
  lac_cout_pp               integer,	-- Cout en PP de l'action
  lac_cout_pv               integer,	-- Cout en PV de l'action
  constraint pk_Lieu_action primary key (lac_id)
) ;

-- typelieu_action
create table typelieu_action (
  tla_id                    serial not null,
  tla_act_id                integer NOT NULL,	-- Identifiant de l'action sp�cialement consid�r�e en ce type de lieu
  tla_tli_id                integer NOT NULL,	-- Identifiant du type de lieu ayant une action particuliere
  tla_possible              boolean,	-- Action possible physiquement en ce type de lieu ? (oui/non) (rempli si diff�rent du standard)
  tla_autorisee             boolean,	-- Action autoris�e (moralement) en ce type de lieu ? (oui/non) (rempli si diff�rent du standard)
  tla_difficulte            integer,	-- Difficulte de l'action en ce type de lieu (rempli si diff�rent du standard)
  tla_cout_pa               integer,	-- Co�t en PA de l'action en ce type de lieu (rempli si diff�rent du standard)
  tla_cout_pn               varchar(64),	-- Co�t en PN de l'action en ce type de lieu (rempli si diff�rent du standard)
  tla_cout_pp               varchar(64),	-- Co�t en PP de l'action en ce type de lieu (rempli si diff�rent du standard)
  tla_cout_pv               varchar(64),	-- Co�t en PV de l'action en ce type de lieu (rempli si diff�rent du standard)
  constraint pk_Typelieu_action primary key (tla_id)
) ;

-- type_lieu
create table type_lieu (
  tli_id                    integer default nextval('type_lieu_tpl_id_seq') not null,	-- Identifiant
  tli_nom                   varchar(50) NOT NULL,	-- Nom du type de lieu
  constraint pk_Type_lieu primary key (tli_id)
) ;

-- debit_element
create table debit_element (
  dbt_id                    integer default nextval('debit_element_dbt_id_seq') not null,	-- Identifiant
  dbt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  dbt_egn_id                integer NOT NULL,	-- Identifiant de l'�l�ment g�n�rique cr�� dans ce lieu
  constraint pk_Debit_element primary key (dbt_id)
) ;

-- vente_element
create table vente_element (
  vnt_id                    integer default nextval('vente_element_vnt_id_seq') not null,	-- Identifiant
  vnt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  vnt_elm_id                integer NOT NULL,	-- Identifiant de l'�l�ment vendu en ce lieu
  constraint pk_Vente_element primary key (vnt_id)
) ;

-- depot_element
create table depot_element (
  dpt_id                    integer default nextval('depot_element_dpt_id_seq') not null,	-- Identifiant
  dpt_lie_id                integer NOT NULL,	-- Identifiant du lieu
  dpt_elm_id                integer NOT NULL,	-- Identifiant de l'�l�ment d�pos� en ce lieu
  dpt_prs_id                integer,	-- Identifiant du personnage poss�dant l'�lement
  constraint pk_Depot_element primary key (dpt_id)
) ;

-- banque
create table banque (
  bnq_id                    integer default nextval('banque_bnq_id_seq') not null,	-- Identifiant
  bnq_lie_id                integer NOT NULL,	-- Identifiant du lieu
  bnq_prs_id                integer NOT NULL,	-- Identifiant du personnage poss�dant l'argent
  bnq_mnn_id                integer NOT NULL,	-- Identifiant de la monnaie d�pos�e
  bnq_somme                 integer,	-- Somme d'argent d�pos�
  constraint pk_Banque primary key (bnq_id)
) ;

-- element_gen
create table element_gen (
  egn_id                    integer default nextval('element_gen_egn_id_seq') not null,	-- Identifiant
  egn_nom                   varchar(64) NOT NULL,	-- Nom de l'element generique
  egn_desc                  varchar(500) NOT NULL,	-- Description de l'element generique
  egn_tel_id                integer NOT NULL,	-- Type d'element generique
  egn_prix                  integer,	-- Prix en monnaie arbitraire
  egn_equipable             boolean NOT NULL,	-- Definit si l'element peut etre equipe ou non
  egn_possession_mld_id     integer,	-- Maladie liee a la simple possession de l'element
  egn_equipe_mld_id         integer,	-- Maladie liee au fait d'equiper l'element
  egn_tcn_id                integer,	-- Technologie de l'element
  egn_mtr_id                integer,	-- Type de materiau
  egn_taille                integer,	-- Taille de l'�l�ment g�n�rique
  constraint pk_Element_gen primary key (egn_id)
) ;

-- arme
create table arme (
  arm_id                    integer default nextval('arme_arm_id_seq') not null,	-- Identifiant
  arm_agn_id                integer NOT NULL,	-- Identifiant de l'arme generique
  arm_rch_id                integer,	-- Identifiant de la recharge actuelle
  arm_elm_id                integer NOT NULL,	-- Identifiant de l'element
  constraint pk_Arme primary key (arm_id)
) ;

-- arme_gen
create table arme_gen (
  agn_id                    integer default nextval('arme_gen_agn_id_seq') not null,	-- Identifiant
  agn_tdg_id                integer NOT NULL,	-- Type de degat
  agn_egn_id                integer NOT NULL,	-- Element generique associe
  agn_mld_id                integer,	-- Maladie donnee en cas de succes
  agn_portee                integer,	-- Portee de l'arme
  agn_degat_min             integer NOT NULL,	-- Degat minimum en cas de succes
  agn_degat_max             integer NOT NULL,	-- Degat max en cas de succes
  agn_deux_mains            boolean,	-- Arme a deux mains ?
  agn_tmt_id                integer,	-- Type de munition a utiliser pour les recharges
  agn_carac1_crc_id         integer NOT NULL,	-- Premi�re caract�ristique
  agn_carac2_crc_id         integer,	-- �ventuelle seconde caracteristique
  agn_compet1_crc_id        integer NOT NULL,	-- Comp�tence associ�e � cette arme
  agn_compet2_crc_id        integer,	-- �ventuelle seconde comp�tence
  agn_bonus_attaque         integer default 0 NOT NULL,	-- Bonus � l'attaque
  agn_bonus_defense         integer default 0 NOT NULL,	-- Bonus � la d�fense
  constraint pk_Arme_gen primary key (agn_id)
) ;

-- matiere
create table matiere (
  mtr_id                    integer default nextval('matiere_mtr_id_seq') not null,	-- Identifiant
  mtr_nom                   varchar(64) NOT NULL,	-- Nom de la matiere
  mtr_desc                  varchar(500),	-- Description de la matiere
  mtr_resistance            integer,	-- Indice de resistance
  constraint pk_Matiere primary key (mtr_id)
) ;

-- technologie
create table technologie (
  tcn_id                    integer default nextval('technologie_tcn_id_seq') not null,	-- Identifiant
  tcn_nom                   varchar(64) NOT NULL,	-- Nom de la technologie
  tcn_desc                  varchar(500),	-- Description de la technologie
  tcn_taux_enrayage         integer,	-- Taux d'enrayage de la technologie
  constraint pk_Technologie primary key (tcn_id)
) ;

-- armure_gen
create table armure_gen (
  ugn_id                    integer default nextval('armure_gen_ugn_id_seq') not null,	-- Identifiant
  ugn_egn_id                integer NOT NULL,	-- Element generique associe
  ugn_bonus_defense         integer default 0 NOT NULL,	-- �ventuel bonus donn� � la defense par cette arme
  constraint pk_Armure_gen primary key (ugn_id)
) ;

-- recharge_gen
create table recharge_gen (
  cgn_id                    serial not null,	-- Identifiant abstrait
  cgn_egn_id                integer NOT NULL,	-- Identifiant de l'element dont il est issu
  cgn_mnt_id                integer NOT NULL,	-- Munition contenue dans ce type de chargeur
  cgn_nb_max_mnt            integer,	-- Nombre maximal de munitions contenues dans ce type de chargeur
  constraint pk_Recharge_gen primary key (cgn_id)
) ;

-- vaisseau_gen
create table vaisseau_gen (
  vgn_id                    integer default nextval('vaisseau_gen_vsg_id_seq') not null,	-- Identifiant
  vgn_egn_id                integer NOT NULL,	-- Identifiant de l'element generique
  vgn_charge_max            integer NOT NULL,	-- Charge maximale contenue par ce modele de vaisseau
  vgn_tvs_id                integer NOT NULL,	-- Type du vaisseau (terrestre, aeroglisseur, sous-marin, spatiale)
  constraint pk_Vaisseau_gen primary key (vgn_id)
) ;

-- type_vaisseau
create table type_vaisseau (
  tvs_id                    integer default nextval('type_vaisseau_tvs_id_seq') not null,	-- Identifiant
  tvs_nom                   varchar(50) NOT NULL,	-- Nom du type de vaisseau
  constraint pk_Type_vaisseau primary key (tvs_id)
) ;

-- munition
create table munition (
  mnt_id                    integer not null,	-- Identifiant
  mnt_nom                   varchar(50) NOT NULL,	-- Nom de la munition
  mnt_desc                  varchar(500),	-- Description de la munition
  mnt_tmt_id                integer,	-- Type de munition
  mnt_mld_id                integer,	-- Maladie associee a ce type de munition
  constraint pk_Munition primary key (mnt_id)
) ;

-- type_degat
create table type_degat (
  tdg_id                    integer default nextval('type_degat_tdg_id_seq') not null,	-- Identifiant
  tdg_nom                   varchar(50) NOT NULL,	-- Nom de la munition
  constraint pk_Type_degat primary key (tdg_id)
) ;

-- armure_gen_degat
create table armure_gen_degat (
  agd_id                    serial not null,	-- Identifiant abstrait
  agd_ugn_id                integer NOT NULL,	-- Identifiant de l'armure generique
  agd_tdg_id                integer NOT NULL,	-- Type de degat encaisse
  agd_encaissement_min      integer NOT NULL,	-- Encaissement minimal
  agd_encaissement_max      integer NOT NULL,	-- Encaissement maximal
  constraint pk_Armure_gen_degat primary key (agd_id)
) ;

-- armure_gen_zone
create table armure_gen_zone (
  agz_id                    serial not null,	-- Identifiant abstrait
  agz_ugn_id                integer NOT NULL,	-- Identifiant de l'armure generique
  agz_zon_id                integer NOT NULL,	-- Zone protegee
  constraint pk_Armure_gen_zone primary key (agz_id)
) ;

-- zone_armure
create table zone_armure (
  zon_id                    integer default nextval('zone_zon_id_seq') not null,	-- Identifiant de la zone corporelle
  zon_nom                   varchar(50) NOT NULL,	-- Nom de la zone protegee
  constraint pk_Zone_armure primary key (zon_id)
) ;

-- vaisseau_gen_poste
create table vaisseau_gen_poste (
  vgp_id                    serial not null,	-- Identifiant abstrait
  vgp_vgn_id                integer NOT NULL,	-- Vaisseau generique dans lequel est ce poste
  vgp_tpt_id                integer NOT NULL,	-- Type de poste
  vgp_nb_perso              integer,	-- Nombre de personnes qui doivent occuper le poste
  constraint pk_Vaisseau_gen_poste primary key (vgp_id)
) ;

-- type_poste_tmp
create table type_poste_tmp (
  tpt_id                    integer default nextval('type_poste_tpt_id_seq') not null,	-- Identifiant
  tpt_nom                   varchar(50) NOT NULL,	-- Nom du type de vaisseau
  constraint pk_Type_poste_tmp primary key (tpt_id)
) ;

-- type_element
create table type_element (
  tel_id                    integer default nextval('type_element_tel_id_seq') not null,	-- Identifiant
  tel_nom                   varchar(64) NOT NULL,	-- Nom de la technologie
  constraint pk_Type_element primary key (tel_id)
) ;

-- armure
create table armure (
  aru_id                    integer default nextval('armure_aru_id_seq') not null,	-- Identifiant
  aru_elm_id                integer NOT NULL,	-- Identifiant de l'element
  constraint pk_Armure primary key (aru_id)
) ;

-- recharge
create table recharge (
  rch_id                    integer default nextval('recharge_rch_id_seq') not null,	-- Identifiant
  rch_elm_id                integer NOT NULL,	-- Identifiant de l'element
  rch_nb_mnt                integer,	-- Nb de munitions
  rch_utilise               boolean,	-- Recharge utilis�e dans une arme ?
  constraint pk_Recharge primary key (rch_id)
) ;

-- vaisseau
create table vaisseau (
  vss_id                    integer default nextval('vaisseau_vss_id_seq') not null,	-- Identifiant
  vss_elm_id                integer NOT NULL,	-- Identifiant de l'element
  vss_sct_id                integer NOT NULL,	-- Secteur representant le vaisseau
  constraint pk_Vaisseau primary key (vss_id)
) ;

-- type_classe
create table type_classe (
  tcl_id                    integer not null,	-- Identifiant
  tcl_nom                   varchar(50) NOT NULL,	-- Nom du type (classe, aptitude)
  constraint pk_Type_classe primary key (tcl_id)
) ;

-- interdit_classe
create table interdit_classe (
  inc_id                    serial not null,	-- Identifiant
  inc_cls_id                smallint NOT NULL,	-- Classe interdisant
  inc_interdit_cls_id       smallint NOT NULL,	-- Classe interdite
  constraint pk_Interdit_classe primary key (inc_id)
) ;

-- prerequis_carac
create table prerequis_carac (
  pqa_id                    serial not null,	-- Identifiant abstrait
  pqa_cln_id                integer NOT NULL,	-- Niveau de classe a atteindre
  pqa_form_carac            integer NOT NULL,	-- Formule a remplir pour avoir le niveau de classe
  constraint pk_Prerequis_carac primary key (pqa_id)
) ;

-- taille
create table taille (
  tll_id                    integer not null,	-- Identifiant
  tll_nom                   varchar(15) NOT NULL,	-- Nom de la taille
  tll_description           varchar(150) NOT NULL,	-- Description
  tll_indice                integer NOT NULL,	-- Indice
  constraint pk_Taille primary key (tll_id)
) ;

-- race
create table race (
  rac_id                    integer default nextval('race_rac_id_seq') not null,	-- Identifiant
  rac_nom                   varchar(50) NOT NULL,	-- Nom
  rac_mld_id                smallint,	-- Maladie (malus/bonus) apport�e
  rac_desc                  varchar(1500),	-- Description physique des repr�sentants de cette race
  rac_monture               boolean default false,	-- Peut porter des personnages (oui/non)
  rac_indice_taille         integer NOT NULL,	-- Indice de base de taille d'un membre de la race
  rac_insc_ouverte          boolean default TRUE NOT NULL,	-- Race disponible � l'inscription
  constraint pk_Race primary key (rac_id)
) ;

-- acces_connu
-- Chemin vers un secteur immobile connu par un personnage
create table acces_connu (
  acc_id                    serial not null,	-- Identifiant abstrait
  acc_chm_id                integer NOT NULL,	-- Identifiant du chemin connu vers un secteur immobile
  acc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  constraint pk_Acces_connu primary key (acc_id)
) ;

-- monde_connu
-- Monde connu par un personnage
create table monde_connu (
  mnc_id                    serial not null,	-- Identifiant abstrait
  mnc_mnd_id                integer NOT NULL,	-- Identifiant du monde connu
  mnc_prs_id                integer NOT NULL,	-- Identifiant du personnage
  constraint pk_Monde_connu primary key (mnc_id)
) ;

-- type_munition
create table type_munition (
  tmt_id                    integer default nextval('type_munition_tmt_id_seq') not null,	-- Identifiant
  tmt_nom                   varchar(50) NOT NULL,	-- Nom du type de munition
  constraint pk_Type_munition primary key (tmt_id)
) ;

-- css
-- Styles CSS de l affichage web
create table css (
  css_id                    integer default nextval('css_css_id_seq') not null,	-- Identifiant
  css_nom                   varchar(32) NOT NULL,	-- Nom du style
  css_defaut                boolean default false NOT NULL,	-- Style par defaut du jeu
  css_chemin                varchar(128) NOT NULL,	-- Chemin du fichier CSS
  constraint pk_Css primary key (css_id)
) ;

-- itineraire
-- Itineraires empruntes par les personnages
create table itineraire (
  itn_id                    integer default nextval('itineraire_itn_id_seq') not null,	-- Identifiant
  itn_chm_id                integer,	-- Identifiant eventuel du chemin emprunt� (deplacement lieu-lieu)
  itn_frm_id                integer NOT NULL,	-- Identifiant de la forme du personnage
  itn_depart_lie_id         integer,	-- Identifiant du lieu de depart
  itn_arrivee_lie_id        integer,	-- Identifiant du lieu d'arrivee
  itn_traces_initiales      integer,	-- Quantite de traces initialement laissees par le suivi
  itn_traces_restantes      integer,	-- Quantite de traces restantes.
  itn_date                  timestamp,	-- Date du deplacement.
  itn_index_effacement      integer default 1 NOT NULL,	-- Nombre de traces qui vont dispara�tre � la prochaine "remise g�n�rale"
  itn_ttr_id                integer NOT NULL,	-- Identifiant du type de trace
  itn_profond_trace         integer default 1 NOT NULL,	-- Profondeur de la trace
  itn_taille_trace          integer default 1 NOT NULL,	-- Pointure de la trace
  constraint pk_Itineraire primary key (itn_id)
) ;

-- type_filature
create table type_filature (
  tfl_id                    integer not null,	-- Identifiant
  tfl_nom                   varchar(50) NOT NULL,	-- Nom du type: filature (voleur, de visu), pistage (par trace), suite (pour guide, volontaire)
  constraint pk_Type_filature primary key (tfl_id)
) ;

-- mime
-- Table de mimes (actions gestuelles)
create table mime (
  mim_id                    integer default nextval('mime_mim_id_seq') not null,	-- Identifiant
  mim_nom                   varchar(24) NOT NULL,	-- Nom du mime
  mim_label                 varchar(64) NOT NULL,	-- Chaine de caracteres symbolisant le mime
  mim_tmm_id                integer NOT NULL,	-- Type de mime
  constraint pk_Mime primary key (mim_id)
) ;

-- type_mime
create table type_mime (
  tmm_id                    integer not null,	-- Identifiant
  tmm_nom                   varchar(50) NOT NULL,	-- Nom du type: simple, li� � un personnage
  constraint pk_Type_mime primary key (tmm_id)
) ;

-- rumeur
-- Tables des rumeurs
create table rumeur (
  rmr_id                    integer default nextval('rumeur_rmr_id_seq') not null,	-- Identifiant
  rmr_nom                   varchar(50) NOT NULL,	-- Nom de la rumeur
  rmr_prs_id                integer NOT NULL,	-- Identifiant du personnage a l'origine de la rumeur
  rmr_content               varchar(5000) NOT NULL,	-- Contenu de la rumeur
  rmr_lng_id                integer NOT NULL,	-- Langue de la rumeur
  rmr_lie_id                integer NOT NULL,	-- Lieu d'origine de la rumeur
  rmr_rayon                 float,	-- Rayon de la rumeur (pour les lieux g�ographiques)
  rmr_sct_id                integer,	-- �ventuel secteur d'influence si la rumeur a �t� lanc�e depuis un lieu topologique
  rmr_diff                  float,	-- Difficult� pour d�couvrir la rumeur
  rmr_date_debut            date,	-- Date d'apparition de la rumeur
  rmr_date_fin              date,	-- Date de fin de la rumeur
  constraint pk_Rumeur primary key (rmr_id)
) ;

-- rumeur_ecoute
-- Tables des ecoutes de rumeurs
create table rumeur_ecoute (
  rmc_id                    integer default nextval('rumeur_ecoute_rmc_id_seq') not null,	-- Identifiant
  rmc_prs_id                integer NOT NULL,	-- Identifiant du personnage ayant entendu la rumeur
  rmc_rmr_id                integer NOT NULL,	-- Identifiant de la rumeur
  constraint pk_Rumeur_ecoute primary key (rmc_id)
) ;

-- langue
-- Langue
create table langue (
  lng_id                    integer not null,	-- Identifiant
  lng_nom                   varchar(50) NOT NULL,	-- Nom
  lng_description           varchar(500),	-- Description de son histoire, son utilisation, ...
  constraint pk_Langue primary key (lng_id)
) ;

-- filature
-- Tables des filatures, pistages, guides
create table filature (
  flt_id                    integer default nextval('filature_flt_id_seq') not null,	-- Identifiant
  flt_suivant_prs_id        integer NOT NULL,	-- Identifiant du personnage qui fait l'action de suivre.
  flt_suivi_frm_id          integer NOT NULL,	-- Identifiant de la forme suivie.
  flt_tfl_id                integer NOT NULL,	-- Type de filature
  flt_debut                 timestamp NOT NULL,	-- Date de d�but de la filature
  constraint pk_Filature primary key (flt_id)
) ;

-- type_trace
-- Types de trace disponibles
create table type_trace (
  ttr_id                    integer default nextval('type_trace_ttr_id_seq') not null,	-- Identifiant abstrait
  ttr_nom                   varchar(64),	-- Nom de la trace
  constraint pk_Type_trace primary key (ttr_id)
) ;

-- trace_connue
-- Traces connues par les personnages
create table trace_connue (
  trc_id                    serial not null,	-- Identifiant
  trc_ttr_id                integer,	-- Identifiant du type de trace
  trc_taille                integer,	-- Pointure
  trc_profondeur            integer,	-- Profondeur
  trc_frm_trace_id          integer,	-- Identifiant de la forme qui a laiss� les traces
  trc_prs_id                integer,	-- Identifiant du personnage qui a d�couvert les traces
  trc_date_fin              timestamp,	-- Date jusqu'� laquelle on se souviendra de ces traces
  trc_nom_donne             varchar(64),	-- Nom donn� � ces traces
  constraint pk_Trace_connue primary key (trc_id)
) ;

-- lieu_carto
create table lieu_carto (
  lic_id                    serial not null,	-- Identifiant
  lic_lie_id                integer NOT NULL,	-- Identifiant du leiu associ�
  lic_mnd_id                integer NOT NULL,	-- Identifiant du monde associ�
  lic_ssn_id                integer,	-- Saison du lieu
  constraint pk_Lieu_carto primary key (lic_id)
) ;

-- cycle
-- Succession de periodes ou de cycles
create table cycle (
  ccl_id                    integer not null,	-- Identifiant
  ccl_nom                   varchar(50) NOT NULL,	-- Nom
  ccl_nombre_periode        integer NOT NULL,	-- Nombre de periodes du cycle
  ccl_duree_periode         integer,	-- Duree d une periode en heure
  ccl_duree_nombre_cycles   integer,	-- La periode du cycle correspond a n autres cycles
  ccl_duree_ccl_id          integer,	-- Identifiant du cycle etalon
  ccl_duree_nombre_cycles_ecoules integer default 0 NOT NULL,	-- Nombre de cycles ecoules
  ccl_pourcentage_alea      integer,	-- Alea eventuel: 0% pour perfection; 100% pour un alea complet
  constraint pk_Cycle primary key (ccl_id)
) ;

-- cycle_monde
-- Rattachement de cycles aux mondes
create table cycle_monde (
  cmn_id                    serial not null,	-- Identifiant
  cmn_mnd_id                integer NOT NULL,	-- Identifiant du monde associ�
  cmn_ccl_id                integer NOT NULL,	-- Identifiant du cycle associ�
  constraint pk_Cycle_monde primary key (cmn_id)
) ;

-- cycle_secteur
-- Rattachement de cycles aux secteurs
create table cycle_secteur (
  csc_id                    serial not null,	-- Identifiant
  csc_sct_id                integer NOT NULL,	-- Identifiant du secteur associ�
  csc_ccl_id                integer NOT NULL,	-- Identifiant du cycle associ�
  constraint pk_Cycle_secteur primary key (csc_id)
) ;

-- periode
-- Detail d une periode d un cycle
create table periode (
  prd_id                    integer not null,	-- Identifiant
  prd_nom                   varchar(50) NOT NULL,	-- Nom
  prd_ccl_id                integer NOT NULL,	-- Cycle correspondant
  prd_min                   integer,	-- Minimum de periode
  prd_max                   integer,	-- Maximum de periode
  prd_complement_description varchar(1500),	-- Complement de description de la periode
  constraint pk_Periode primary key (prd_id)
) ;

-- lieu_cycle
-- Rattachement de lieux aux cycles
create table lieu_cycle (
  lcd_id                    serial not null,	-- Identifiant
  lcd_ccl_id                integer NOT NULL,	-- Identifiant du cycle associ�
  lcd_lie_id                integer NOT NULL,	-- Identifiant du lieu associ�
  constraint pk_Lieu_cycle primary key (lcd_id)
) ;

-- lieu_cyclique
-- Aspect compl�mentaire d un lieu � un cycle donn�
-- n�
-- �
create table lieu_cyclique (
  lcc_id                    serial not null,	-- Identifiant
  lcc_prd_id                integer NOT NULL,	-- Identifiant de la p�riode associ�e
  lcc_lie_id                integer NOT NULL,	-- Identifiant du lieu associ�
  lcc_description           varchar(1500),	-- Description compl�mentaire � cette p�riode donn�e
  constraint pk_Lieu_cyclique primary key (lcc_id)
) ;

-- calque
-- Calque g�ographique repr�sentant selon : le relief, le climat, les
-- voieries, etc.
create table calque (
  clq_id                    integer default nextval('calque_clq_id_seq') not null,	-- Identifiant
  clq_nom                   varchar(64) NOT NULL,	-- Nom
  clq_description           varchar(1500),	-- Description
  clq_indice_population     integer default 0 NOT NULL,	-- Indice de densit� de population
  clq_facteur_effacement_trace float default 1 NOT NULL,	-- Facteur influant sur l'effacement des traces
  clq_facteur_nb_trace      float default 1 NOT NULL,	-- Facteur influant sur le nombre de traces
  clq_cout_pn               varchar(64) default '0' NOT NULL,	-- Cout journalier en pn
  clq_cout_acces_pn         varchar(64) default '0' NOT NULL,	-- Cout d'acc�s en pn
  clq_cout_pv               varchar(64) default '0' NOT NULL,	-- Cout journalier en point de vie
  clq_cout_acces_pa         varchar(64) default '0' NOT NULL,	-- Cout d'acc�s en point d'action
  clq_cout_echec_pv         varchar(64) default '0' NOT NULL,	-- Cout en pv en cas d'�chec
  clq_couleur               char(7),	-- Code couleur associ�
  clq_tcq_id                integer NOT NULL,	-- Type de calque
  clq_id_externe            integer NOT NULL,	-- Identifiant externe � l'application utilis� lors des imports
  clq_difficulte            varchar(64) default '0' NOT NULL,	-- Formule de difficult� de d�placement dans ces zones et de fait facilit� � se perdre
  clq_crc_id                integer,	-- Comp�tence associ�e
  clq_crc2_crc_id           integer,	-- �ventuelle deuxi�me caract�ristique associ�e
  clq_echec_bloquant        boolean default FALSE NOT NULL,	-- Si � vrai, �chouer sur son d�placement ne fait pas progresser
  constraint pk_Calque primary key (clq_id)
) ;

-- type_calque
-- Type de calque
create table type_calque (
  tcq_id                    integer default nextval('type_calque_tcq_id_seq') not null,	-- Identifiant
  tcq_nom                   varchar(50) NOT NULL,	-- Nom du type de calque
  tcq_principal             boolean default FALSE NOT NULL,	-- Un type de calque est d�sign� comme le type de calque principal. Le label de base sera prit sur le calque correspondant au type de calque principal.
  constraint pk_Type_calque primary key (tcq_id)
) ;

-- calque_lieu_carto
-- Rattachement de calques aux lieux carto
create table calque_lieu_carto (
  clc_id                    serial not null,	-- Identifiant
  clc_clq_id                integer NOT NULL,	-- Identifiant du calque associ�
  clc_lic_id                integer NOT NULL,	-- Identifiant du lieu carto associ�
  constraint pk_Calque_lieu_carto primary key (clc_id)
) ;

-- type_vehicule
-- Type de vehicule
create table type_vehicule (
  tvh_id                    integer default nextval('type_vehicule_tvh_id_seq') not null,	-- Identifiant
  tvh_nom                   varchar(50) NOT NULL,	-- Nom du type de v�hicule
  tvh_carburant_egn_id      integer,	-- Type de carburant employ�
  tvh_puissance             integer,	-- Puissance developp�e lors d'un d�placement
  tvh_consommation          integer,	-- Nombre d'unit� de carburant utilis� par tour
  tvh_usure_max             integer,	-- Maximum de point d'usure pour un vehicule r�par� ou neuf
  tvh_usure                 integer,	-- Point d'usure gagn� � chaque tour lorsque en marche
  constraint pk_Type_vehicule primary key (tvh_id)
) ;

-- vent_lieu_carto
-- Rattachement de vents aux lieux carto
create table vent_lieu_carto (
  cvt_id                    serial not null,	-- Identifiant
  cvt_tvt_id                integer NOT NULL,	-- Identifiant du type de vent associ�
  cvt_lic_id                integer NOT NULL,	-- Identifiant du lieu carto associ�
  cvt_force                 point,	-- Force du vent
  constraint pk_Vent_lieu_carto primary key (cvt_id)
) ;

-- restriction_calque
-- Restriction d un type de vehicule � un calque en particulier
-- r
create table restriction_calque (
  rtc_id                    serial not null,	-- Identifiant
  rtc_tvh_id                integer NOT NULL,	-- Identifiant du type de v�hicule
  rtc_clq_id                integer NOT NULL,	-- Identifiant du calque de restriction
  rtc_rendement             integer default 100 NOT NULL,	-- Rendement associ� � ce type de terrain
  constraint pk_Restriction_calque primary key (rtc_id)
) ;

-- influence_vent
-- Vent influen�ant les d�placements d un type de vehicule
-- le
create table influence_vent (
  inv_id                    serial not null,	-- Identifiant
  inv_tvh_id                integer NOT NULL,	-- Identifiant du type de v�hicule
  inv_tvt_id                integer NOT NULL,	-- Identifiant du type de vent
  inv_puissance             integer NOT NULL,	-- Puissance associ�e � ce moyen de propulsion
  constraint pk_Influence_vent primary key (inv_id)
) ;

-- vehicule
-- V�hicule
-- e
create table vehicule (
  vhc_id                    integer default nextval('vehicule_vhc_id_seq') not null,	-- Identifiant
  vhc_tvh_id                integer NOT NULL,	-- Identifiant du type de v�hicule
  vhc_reservoir             integer,	-- Maximum de ressource pouvant �tre stock�
  vhc_reservoir_actuel      integer,	-- Niveau de ressource actuel dans le r�servoir
  vhc_usure_actuelle        integer,	-- Niveau d'usure actuel
  vhc_amarre                boolean,	-- � vrai si le v�hicule est amarr�
  vhc_moteur_fonctionnel    boolean,	-- � vrai si le moteur est en route
  vhc_masse                 integer,	-- Masse � d�placer
  vhc_bride_moteur          integer,	-- Pourcentage de puissance laiss�e pour le moteur
  vhc_bride_vent            integer,	-- Pourcentage de puissance laiss�e pour la propulsion passive
  vhc_direction             point,	-- Direction souhait�e
  vhc_date_remise           date,	-- Date de remise
  vhc_reserve_nourriture    integer,	-- R�serve de nourriture max dans le v�hicule
  vhc_reserve_nourriture_actuel integer,	-- Niveau actuel de la r�serve de nourriture
  constraint pk_Vehicule primary key (vhc_id)
) ;

-- type_vent
-- Type de vent
create table type_vent (
  tvt_id                    integer default nextval('type_vent_tvt_id_seq') not null,	-- Identifiant
  tvt_nom                   varchar(50) NOT NULL,	-- Nom du type de vent
  constraint pk_Type_vent primary key (tvt_id)
) ;

-- type_poste
-- Type de poste
create table type_poste (
  tps_id                    integer default nextval('type_poste_tps_id_seq') not null,	-- Identifiant
  tps_nom                   varchar(50) NOT NULL,	-- Nom du type de poste
  tps_rendement_min         integer default 0 NOT NULL,	-- Rendement minimum d'un poste
  tps_propulsion_active     boolean default FALSE,	-- Poste pour une propulsion active ?
  tps_formule_difficulte    varchar(60) default '1' NOT NULL,	-- Formule de difficult� de tenue du poste
  constraint pk_Type_poste primary key (tps_id)
) ;

-- poste
-- Poste en place sur un v�hicule
-- e
create table poste (
  pst_id                    serial not null,	-- Identifiant
  pst_tps_id                integer NOT NULL,	-- Identifiant du type de poste
  pst_prs_id                integer,	-- Identifiant de l'�ventuelle personne assurant le poste
  pst_vhc_id                integer NOT NULL,	-- Identifiant du v�hicule associ�
  constraint pk_Poste primary key (pst_id)
) ;

-- type_vehicule_poste
-- Type poste en place sur un type de v�hicule
-- e
create table type_vehicule_poste (
  ttp_id                    serial not null,	-- Identifiant
  ttp_tvh_id                integer NOT NULL,	-- Identifiant du type de v�hicule
  ttp_tps_id                integer,	-- Identifiant du type de poste
  constraint pk_Type_vehicule_poste primary key (ttp_id)
) ;

-- monture
-- Monture (cheval, �l�phant, moto...)
-- .)
create table monture (
  mon_id                    serial not null,	-- Identifiant de la monture
  mon_prs_id                integer NOT NULL,	-- Personnage associ� � la monture
  mon_cavalier_prs_id       integer,	-- �ventuel identifiant du cavalier
  mon_clef_elm_id           integer,	-- �ventuelle cl� n�cessaire pour faire fonctionner la monture
  mon_vivante               boolean NOT NULL,	-- Indique si une cr�ature est vivante ou non (et notamment si elle consomme des PNs � l'arr�t)
  mon_egn_id                integer,	-- �ventuel type de carburant � associer � la monture
  mon_consomation           integer,	-- Consomation par case franchie
  mon_usure                 integer,	-- Dans le cas de monture inanim�e, usure (en � PV �) par d�placement
  mon_surplus_cout_pn       integer,	-- �ventuel surplus de cout de PN en charge
  mon_formule_difficulte    varchar(60) default '1' NOT NULL,	-- Formule de maitrise de la monture
  constraint pk_Monture primary key (mon_id)
) ;

-- particularite_chemin
-- Particularit� associ�e � un chemin (champs textuel du style � par la
-- fen�tre �,  � par la porte �)
-- �)
-- )
create table particularite_chemin (
  pch_id                    serial not null,	-- Identifiant
  pch_nom                   varchar(128) NOT NULL,	-- Nom de la particularit�
  constraint pk_Particularite_chemin primary key (pch_id)
) ;

-- description_lieu
-- Description plus pr�cises de certains lieux
-- x
create table description_lieu (
  dsl_id                    serial not null,	-- Identifiant
  dsl_lie_id                integer NOT NULL,	-- 	
  dsl_difficulte            integer NOT NULL,	-- Difficult� associ�e � la connaissance de cette description
  dsl_description           varchar(1500) NOT NULL,	-- Compl�ment de description
  constraint pk_Description_lieu primary key (dsl_id)
) ;

-- classeniveau_action
-- Limitation des actions pour certains niveaux de certaines classes
create table classeniveau_action (
  cna_id                    serial not null,	-- Identifiant
  cna_act_id                integer NOT NULL,	-- Identifiant de l'action sp�cialement consid�r�e pour cette classe
  cna_cln_id                integer NOT NULL,	-- Identifiant du niveau de classe autoris�
  cna_autorise              boolean,	-- Action autoris�e (True) ou interdite (False) sp�cifiquement pour cette classe
  constraint pk_Classeniveau_action primary key (cna_id)
) ;

-- engagement
-- Engagement entre personnage
create table engagement (
  eng_id                    serial not null,	-- Identifiant
  eng_prs1_prs_id           integer NOT NULL,	-- Identifiant du premier personnage impliqu� dans l'engagement
  eng_prs2_prs_id           integer NOT NULL,	-- Identifiant du deuxi�me personnage impliqu� dans l'engagement
  eng_init_1                boolean,	-- � vrai si le premier personnage a l'initiative sinon l'initiative appartient au second personnage
  eng_lie_id                integer NOT NULL,	-- Lieu o� se passe la confrontation
  eng_distant               boolean,	-- � vrai si engagement distant sinon engagement de corps � corps
  constraint pk_Engagement primary key (eng_id)
) ;

-- coffre
-- Conteneur d''objet
create table coffre (
  cff_id                    integer default nextval('coffre_cff_id_seq') not null,	-- Identifiant
  cff_elm_id                integer NOT NULL,	-- Identifiant de l'�l�ment associ�
  cff_clef_egn_id           integer,	-- Clef g�n�rique du coffre
  cff_taille                integer NOT NULL,	-- Indice de la contenance de ce coffre
  cff_ouvert                boolean default True,	-- �tat ouvert/ferm�
  constraint pk_Coffre primary key (cff_id)
) ;

-- coffre_element
create table coffre_element (
  cel_id                    serial not null,	-- Identifiant
  cel_cff_id                integer NOT NULL,	-- Coffre associ�
  cel_elm_id                integer NOT NULL,	-- �l�ment associ�
  cel_prix                  integer,	-- �ventuel prix associ� si un magasin est associ� au coffre
  cel_mnn_id                integer,	-- Monnaie associ�e
  constraint pk_Coffre_element primary key (cel_id)
) ;

-- element_visible
-- �l�ments visible pour un personnage
-- ge
create table element_visible (
  elv_id                    serial not null,	-- Identifiant
  elv_prs_id                integer NOT NULL,	-- Personnage associ�
  elv_elm_id                integer NOT NULL,	-- �l�ment associ�
  constraint pk_Element_visible primary key (elv_id)
) ;

-- image
-- Image
create table image (
  img_id                    integer default nextval('image_img_id_seq') not null,	-- Identifiant
  img_nom                   varchar,	-- Nom associ�
  img_adresse               varchar NOT NULL,	-- Adresse relative de l'image
  constraint pk_Image primary key (img_id)
) ;

-- boutique
-- Boutique
create table boutique (
  btq_id                    integer default nextval('boutique_btq_id_seq') not null,	-- Identifiant
  btq_cff_id                integer NOT NULL,	-- Identifiant du coffre associ�
  btq_nom                   varchar(500),	-- Nom de la boutique
  btq_description           varchar(2000),	-- �ventuelle description associ�e � la boutique
  btq_prs_id                integer,	-- �ventuel g�rant de la boutique
  btq_atm_id                integer,	-- �ventuel automate associ�
  btq_ouverte               boolean,	-- � vrai si la boutique est ouverte
  constraint pk_Boutique primary key (btq_id)
) ;

-- automate
-- Automate similant un personnage. Des caract�ristiques lui sont
-- attribu�es.
-- .
create table automate (
  atm_id                    integer default nextval('automate_atm_id_seq') not null,	-- Identifiant
  atm_nom                   varchar(500),	-- �ventuel nom associ�
  constraint pk_Automate primary key (atm_id)
) ;

-- element_gen_connu
-- �l�ments g�n�riques identifi�s par un personnage
-- nnage
create table element_gen_connu (
  egc_id                    serial not null,	-- Identifiant
  egc_egn_id                integer NOT NULL,	-- �l�ment g�n�rique associ�
  egc_prs_id                integer NOT NULL,	-- Personnage associ�
  constraint pk_Element_gen_connu primary key (egc_id)
) ;

-- automate_carac
-- Caract�ristiques associ�es � un automate
-- ate
create table automate_carac (
  acr_id                    serial not null,	-- Identifiant abstrait
  acr_atm_id                integer NOT NULL,	-- Automate associ�
  acr_crc_id                integer NOT NULL,	-- Caracteristique associ�e
  acr_score                 smallint default 0 NOT NULL,	-- Score dans la caract�ristique.
  constraint pk_Automate_carac primary key (acr_id)
) ;

-- achat_element_gen
-- �l�ments g�n�riques achetables par une boutique donn�e
-- nn�e
-- e
create table achat_element_gen (
  aeg_id                    serial not null,	-- Identifiant
  aeg_egn_id                integer NOT NULL,	-- �l�ment g�n�rique associ�
  aeg_btq_id                integer NOT NULL,	-- Boutique associ�e
  aeg_prix                  integer,	-- Prix de base
  aeg_mnn_id                integer,	-- Monnaie associ�e au prix
  aeg_nombre_max            integer,	-- Nombre maximum d'�l�ments achetables
  constraint pk_Achat_element_gen primary key (aeg_id)
) ;

-- type_environnement
-- Environnement d''un secteur
create table type_environnement (
  tev_id                    integer default nextval('type_environnement_tev_id_seq') not null,	-- Identifiant
  tev_nom                   varchar(50) NOT NULL,	-- Nom de l'environnement
  tev_crc_id                integer NOT NULL,	-- Caract�ristique associ�e
  constraint pk_Type_environnement primary key (tev_id)
) ;

-- profil_annonce
-- Profil mis � disposition
-- n
create table profil_annonce (
  pro_id                    serial not null,	-- Identifiant
  pro_nom                   character varying(255) NOT NULL,	-- Nom
  pro_statut                integer default 0 NOT NULL,	-- Entier pointant vers des statuts
  pro_ctp_id                integer NOT NULL,	-- Cat�gorie de profil
  pro_mji_id                integer NOT NULL,	-- MJ associ�
  pro_desc                  text NOT NULL,	-- Description
  pro_debat                 text,	-- Discussion entre MJs
  pro_nbpersos              integer default 1 NOT NULL,	-- Nombre de personnages � pourvoir
  pro_historique            text,	-- Historique
  pro_demandeaction         integer,	-- Demande d'action sur un profil
  constraint pk_Profil_annonce primary key (pro_id)
) ;

-- news
-- Nouvelles
create table news (
  new_id                    serial not null,	-- Identifiant
  new_titre                 text NOT NULL,	-- Titre de la nouvelle
  new_txt                   text NOT NULL,	-- Texte de la nouvelle
  new_date                  timestamp without time zone NOT NULL,	-- Date de publication
  new_dateedit              timestamp without time zone,	-- Date de derni�re modification
  new_mji_id                integer NOT NULL,	-- MJ associ�
  constraint pk_News primary key (new_id)
) ;

-- categorie_profil
-- Cat�gories des profils
-- s
create table categorie_profil (
  ctp_id                    serial not null,	-- Identifiant
  ctp_nom                   character varying(255) NOT NULL,	-- Nom
  constraint pk_Categorie_profil primary key (ctp_id)
) ;

-- candidature
-- Candidature
create table candidature (
  can_id                    serial not null,	-- Identifiant
  can_jou_id                integer NOT NULL,	-- Joueur associ�
  can_pro_id                integer NOT NULL,	-- Profil associ�
  can_statut                integer NOT NULL,	-- Statut de la candidature
  can_nomperso              character varying(255) NOT NULL,	-- Nom du personnage
  can_rac_id                integer NOT NULL,	-- Race associ�e
  can_cls_id                integer NOT NULL,	-- Classe associ�e
  can_sex_id                integer NOT NULL,	-- Sexe associ�
  can_descriptionperso      text NOT NULL,	-- Description du personnage
  can_backgroundperso       text NOT NULL,	-- Background du personnage
  can_butsperso             integer NOT NULL,	-- Buts du personnage
  can_debatjoumj            text,	-- D�bat entre le recruteur et le joueur
  can_debatmj               text,	-- D�bat entre le recruteur et le MJ qui a propos� le profil
  can_historique            text,	-- Historique de la candidature
  can_commentaire           text,	-- Pense-b�te pour le recruteur
  can_datemodification      timestamp without time zone,	-- Date de derni�re modification
  constraint pk_Candidature primary key (can_id)
) ;

-- question
-- Question du quizz
create table question (
  que_id                    serial not null,	-- Identifiant
  que_intitule              text NOT NULL,	-- Intitul� de la question
  constraint pk_Question primary key (que_id)
) ;

-- reponse
-- Proposition de r�ponse � une question du quizz
-- zz
create table reponse (
  rep_id                    serial not null,	-- Identifiant
  rep_que_id                integer NOT NULL,	-- Question associ�e
  rep_intitule              text NOT NULL,	-- Intitul� de la r�ponse
  rep_bonne                 boolean default False NOT NULL,	-- � vrai si la r�ponse est bonne
  constraint pk_Reponse primary key (rep_id)
) ;

-- profil_classe
create table profil_classe (
  pcl_id                    serial not null,	-- Identifiant abstrait
  pcl_pro_id                integer NOT NULL,	-- Profil associ�
  pcl_cls_id                integer NOT NULL,	-- Classe associ�e
  constraint pk_Profil_classe primary key (pcl_id)
) ;

-- profil_race
create table profil_race (
  pra_id                    serial not null,	-- Identifiant abstrait
  pra_pro_id                integer NOT NULL,	-- Profil associ�
  pra_rac_id                integer NOT NULL,	-- Race associ�e
  constraint pk_Profil_race primary key (pra_id)
) ;

-- profil_sexe
create table profil_sexe (
  psx_id                    serial not null,	-- Identifiant abstrait
  psx_pro_id                integer NOT NULL,	-- Profil associ�
  psx_sex_id                integer NOT NULL,	-- Sexe associ�
  constraint pk_Profil_sexe primary key (psx_id)
) ;
comment on column personnage.prs_id is 'Identifiant du personnage';
comment on column personnage.prs_jou_id is 'Identifiant du joueur jouant le personnage';
comment on column personnage.prs_principal is 'Personnage par defaut du joueur (oui/non)';
comment on column personnage.prs_possesseur_prs_id is 'Identifiant du personnage possedant celui-ci';
comment on column personnage.prs_lie_id is 'Identifiant du lieu';
comment on column personnage.prs_nom_originel is 'Nom de depart';
comment on column personnage.prs_nom_actuel is 'Nom actuel';
comment on column personnage.prs_background is 'Passe du personnage';
comment on column personnage.prs_biographie is 'Histoire du personnage';
comment on column personnage.prs_inscription is 'Date d''inscription';
comment on column personnage.prs_derniere_connexion is 'Date de derniere connexion';
comment on column personnage.prs_pa is 'Nombre de Points d''Action';
comment on column personnage.prs_pa_max is 'Nombre de Points d''Action maximum';
comment on column personnage.prs_pn is 'Nombre de Points de Nourriture';
comment on column personnage.prs_pn_max is 'Nombre de Points de Nourriture maximum';
comment on column personnage.prs_pp is 'Nombre de Points de Paralysie';
comment on column personnage.prs_pp_max is 'Nombre de Points de Paralysie maximum';
comment on column personnage.prs_pv is 'Nombre de Points de Vie';
comment on column personnage.prs_pv_max is 'Nombre de Points de Vie maximum';
comment on column personnage.prs_pc is 'Points de combat';
comment on column personnage.prs_pc_max is 'Maximum de point de combat';
comment on column personnage.prs_tpr_id is 'Type de personnage: pj, pnj, familier, etc.';
comment on column personnage.prs_calc_facteur_nb_trace is 'Facteur influant sur le nombre de trace calcul� en fonction de ses attributs, d''�ventuelles comp�tences, de son �quipement, etc.';
comment on column personnage.prs_date_arrivee is 'Date d''arriv�e dans un lieu';
comment on column personnage.prs_delta_rose is 'Delta de d�calage par rapport � la rose des vents';

comment on column joueur.jou_id is 'Identifiant';
comment on column joueur.jou_pseudo is 'Pseudonyme';
comment on column joueur.jou_mdp is 'Mot de passe (md5)';
comment on column joueur.jou_email is 'Adresse mail';
comment on column joueur.jou_css_id is 'Style CSS utilis� par le joueur';
comment on column joueur.jou_fuseau is 'Decalage horaire par rapport a l''heure GMT';
comment on column joueur.jou_session_id is 'Identifiant de session';
comment on column joueur.jou_scorequizz is 'Score au quizz';
comment on column joueur.jou_ipinitiale is 'IP d''inscription';
comment on column joueur.jou_ipcourante is 'Derni�re IP connue';
comment on column joueur.jou_commentaire is '�ventuel commentaire de l''utilisateur';

comment on column mj.mji_id is 'Identifiant';
comment on column mj.mji_jou_id is 'Identifiant du joueur';
comment on column mj.mji_nom is 'Nom';
comment on column mj.mji_derniere_connexion is 'Date de derniere connexion';
comment on column mj.mji_droits_news is 'R�dacteur de news (O/N)';
comment on column mj.mji_droits_all_news is 'Administrateur des news';
comment on column mj.mji_droits_inscription is 'Droit de proposition de profil';
comment on column mj.mji_droits_recrutement is 'Administrateur des incriptions';
comment on column mj.mji_droits_edit_regles is 'R�dacteur wiki r�gles';
comment on column mj.mji_droits_edit_monde is 'R�dacteur wiki background';
comment on column mj.mji_droits_edit_admin is 'Administrateur technique';

comment on column perso_classe.pce_id is 'Identifiant abstrait';
comment on column perso_classe.pce_prs_id is 'Personnage';
comment on column perso_classe.pce_cln_id is 'Niveau de classe';
comment on column perso_classe.pce_titre is 'Titre accord� au personnage (si diff�rent de celui de classe)';

comment on column forme.frm_id is 'Identifiant';
comment on column forme.frm_prs_id is 'Identifiant du personnage pouvant prendre cette forme';
comment on column forme.frm_rac_id is 'Identifiant de la race';
comment on column forme.frm_sex_id is 'Sexe';
comment on column forme.frm_indice_taille is 'Indice de taille de la forme';
comment on column forme.frm_actuelle is 'Definit la forme actuelle du personnage (oui/non)';
comment on column forme.frm_description is 'Description physique';
comment on column forme.frm_desc_actuelle is 'Description de l''etat actuel';
comment on column forme.frm_url_image is 'URL de l''image illustrant le personnage sous cette forme';
comment on column forme.frm_ttr_id is 'Type de trace de la forme';
comment on column forme.frm_profond_trace is 'Profondeur de la trace laiss�e';

comment on column classe_niveau.cln_id is 'Identifiant';
comment on column classe_niveau.cln_cls_id is 'Classe';
comment on column classe_niveau.cln_niveau is 'Niveau dans la classe';
comment on column classe_niveau.cln_mld_id is 'Maladie de classe';
comment on column classe_niveau.cln_titre is 'Titre donn� au d�tenteur de ce niveau de classe';
comment on column classe_niveau.cln_titre_visible is 'Titre visible par les autres (oui/non)';
comment on column classe_niveau.cln_desc_mj is 'Description pour MJ';

comment on column classe.cls_id is 'Identifiant';
comment on column classe.cls_nom is 'Nom';
comment on column classe.cls_desc_mj is 'Description � l''adresse des MJ';
comment on column classe.cls_tcl_id is 'Type de classe: classe, aptitude';
comment on column classe.cls_insc_ouverte is 'Inscription ouverte pour cette classe';

comment on column perso_maladie.pml_id is 'Identifiant abstrait';
comment on column perso_maladie.pml_prs_id is 'Personnage malade';
comment on column perso_maladie.pml_mld_id is 'Maladie';
comment on column perso_maladie.pml_debut is 'Date d''apparition de la maladie';
comment on column perso_maladie.pml_fin is 'Eventuelle date de fin de la maladie';

comment on column maladie.mld_id is 'Identifiant';
comment on column maladie.mld_nom is 'Nom';
comment on column maladie.mld_description is 'Description generale';
comment on column maladie.mld_tml_id is 'Type de maladie';
comment on column maladie.mld_duree is 'Duree en tours';
comment on column maladie.mld_periode is 'Periode en tours entre 2 effets';
comment on column maladie.mld_description_physique is 'Description per�ue par un tiers sur le malade (vide si rien de visible)';
comment on column maladie.mld_formule is 'Formule � appliquer pour trouver les caract�ristiques actuelles';

comment on column caracteristique.crc_id is 'Identifiant';
comment on column caracteristique.crc_nom is 'Nom';
comment on column caracteristique.crc_description is 'Description de son utilisation';
comment on column caracteristique.crc_tcr_id is 'Type de caracteristique';
comment on column caracteristique.crc_ncr_id is 'Nature';
comment on column caracteristique.crc_delta_min_max is 'Delta minimum et maximum';
comment on column caracteristique.crc_visible is '� vrai si le score dans cette caract�ristiques est visible par le joueur';

comment on column perso_carac.pcr_id is 'Identifiant abstrait';
comment on column perso_carac.pcr_prs_id is 'Personnage';
comment on column perso_carac.pcr_crc_id is 'Caracteristique';
comment on column perso_carac.pcr_jauge_entrainement is 'Jauge d''entrainement (entre 0 et 100)';
comment on column perso_carac.pcr_score_xp is 'Nombre de points d''experience actuel dans la caracteristique';
comment on column perso_carac.pcr_score_xp_max is 'Nombre de points d''experience maximum atteint dans la caracteristique';
comment on column perso_carac.pcr_score_calcule is 'Score apres calcul a partir de pcr_score_xp apres application des differentes maladies, classes, etc.';
comment on column perso_carac.pcr_score_max is 'Score absolu pouvant etre atteint en fonction de l''age, la race, le sexe du personnage';

comment on column parametre.prm_id is 'Identifiant';
comment on column parametre.prm_ctg_id is 'Ensemble d''appartenance';
comment on column parametre.prm_libelle is 'Libelle explicitant a quoi il sert';
comment on column parametre.prm_valeur is 'Valeur du parametre (c''est un varchar mais des entiers ou des dates peuvent y etre entres)';
comment on column parametre.prm_description is 'Description de ce qui peut etre mis dans la valeur';

comment on column categorie.ctg_id is 'Identifiant';
comment on column categorie.ctg_libelle is 'Libelle de l''ensemble de parametres';
comment on column categorie.ctg_systeme is 'Regroupement de parametres systemes (oui/non : modifiable ou non)';

comment on column perso_classe_int.pci_id is 'Identifiant abstrait';
comment on column perso_classe_int.pci_prs_id is 'Personnage';
comment on column perso_classe_int.pci_cls_id is 'Classe autoris�e/interdite';
comment on column perso_classe_int.pci_type_interdit is 'Interdiction de la classe pour le personnage (oui/non)';

comment on column prerequis_classe.pqc_id is 'Identifiant abstrait';
comment on column prerequis_classe.pqc_cln_id is 'Niveau de classe a atteindre';
comment on column prerequis_classe.pqc_cls_id is 'Classe necessaire';
comment on column prerequis_classe.pqc_niveau_min is 'Niveau minimum a avoir dans la classe necessaire ';

comment on column monde.mnd_id is 'Identifiant';
comment on column monde.mnd_nom is 'Nom';
comment on column monde.mnd_tmn_id is 'Type de monde';
comment on column monde.mnd_description is 'Description g�n�rale du monde';
comment on column monde.mnd_pere_mnd_id is 'Monde contenant celui-ci';
comment on column monde.mnd_niveau is 'Indice de niveau';
comment on column monde.mnd_mld_id is 'Maladie r�gnant sur ce monde';
comment on column monde.mnd_image is 'Image du monde (fond de la carte)';

comment on column lieu.lie_id is 'Identifiant';
comment on column lieu.lie_tli_id is 'Type de lieu';
comment on column lieu.lie_nom is 'Nom';
comment on column lieu.lie_sct_id is 'Secteur d''appartenance';
comment on column lieu.lie_indice_taille is 'Taille du lieu';
comment on column lieu.lie_indice_population is 'Indice de densit� de population';
comment on column lieu.lie_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column lieu.lie_facteur_nb_trace is 'Facteur influant sur le nombre de trace';
comment on column lieu.lie_carto is '� vrai si il s''agit d''un lieu carto';
comment on column lieu.lie_desc is 'Pr�cision �ventuelle sur la description du lieu';
comment on column lieu.lie_img_id is '�ventuelle image associ�e';

comment on column secteur.sct_id is 'Identifiant';
comment on column secteur.sct_nom is 'Nom';
comment on column secteur.sct_description is 'Description destin�e aux personnages se trouvant dans le secteur';
comment on column secteur.sct_tsc_id is 'Type de secteur';
comment on column secteur.sct_charge_max is 'Charge maximale support�e par le secteur (null signifie infini)';
comment on column secteur.sct_indice_population is 'Indice de densit� de population';
comment on column secteur.sct_vhc_id is 'Identifiant de l''�ventuel v�hicule associ� � un secteur';
comment on column secteur.sct_cout_pn is 'Cout journalier en pn';
comment on column secteur.sct_cout_pv is 'Cout journalier en PV';
comment on column secteur.sct_difficulte_rumeur is 'Difficult� de lancement d''une rumeur';

comment on table chemin is 'Lien entre 2 endroits (lieu <-> lieu ou lieu <->monde)';
comment on column chemin.chm_id is 'Identifiant';
comment on column chemin.chm_egn_id is 'Element generique servant de cl�';
comment on column chemin.chm_tch_id is 'Type de chemin (d''acces)';
comment on column chemin.chm_pch_id is '�ventuelle particularit� associ�e � un chemin (champs textuel du style � par la fen�tre �,  � par la porte �). Permet notamment de diff�rencier diff�rents chemins venant du m�me endroit et allant au m�me endroit.';
comment on column chemin.chm_depart_lie_id is 'Lieu de d�part';
comment on column chemin.chm_arrivee_lie_id is 'Lieu d''arriv�e';
comment on column chemin.chm_desc_aller is 'Description du trajet dans le sens aller';
comment on column chemin.chm_desc_retour is 'Description du trajet dans le sens retour';
comment on column chemin.chm_aller is 'Trajet possible dans le sens aller (oui/non)';
comment on column chemin.chm_retour is 'Trajet possible dans le sens retour (oui/non)';
comment on column chemin.chm_ouvert is 'Trajet actuellement ouvert (oui/non) (pr�vaut sur chm_aller et chm_retour)';
comment on column chemin.chm_discretion is 'Discretion';
comment on column chemin.chm_difficulte is 'Seuil de difficult�';
comment on column chemin.chm_cout_pa is 'Cout du trajet en Points d''Action';
comment on column chemin.chm_cout_pn is 'Cout en point de nourriture';
comment on column chemin.chm_cout_echec_pv is 'Cout en PV en cas d''�chec';
comment on column chemin.chm_indice_taille is 'Indice de taille du passage accord� par le chemin';
comment on column chemin.chm_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column chemin.chm_facteur_nb_trace is 'Facteur influant sur le nombre de traces';
comment on column chemin.chm_echec_bloquant is '� vrai si l''�chec empeche d''emprunter le chemin';
comment on column chemin.chm_crc_id is 'Caract�ristique associ�e';
comment on column chemin.chm_crc_2_id is '�ventuelle seconde caract�ristique intervenant dans la formule';

comment on column saison.ssn_id is 'Identifiant';
comment on column saison.ssn_nom is 'Nom';
comment on column saison.ssn_ordre is 'Ordre pendant un cycle';
comment on column saison.ssn_default is 'Saison par d�faut';

comment on column element.elm_id is 'Identifiant';
comment on column element.elm_egn_id is 'Identifiant de l''element generique associe';
comment on column element.elm_nom is 'Nom';
comment on column element.elm_desc is 'Description de l''element';
comment on column element.elm_prix is 'Prix de l''element en monnaie arbitraire';
comment on column element.elm_equipable is 'Definit si l''element peut etre equipe ou non';
comment on column element.elm_equipe is 'Definit si l''element est equipe ou non';
comment on column element.elm_enraye is 'Definit si l''element est enraye ou non';
comment on column element.elm_naturel is 'Definit si l''element est naturel chez son possesseur';
comment on column element.elm_prs_id is 'Identifiant de son possesseur';
comment on column element.elm_lie_id is 'Lieu ou a ete jete l''element';

comment on column monnaie.mnn_id is 'Identifiant de la monnaie';
comment on column monnaie.mnn_nom is 'Nom de la monnaie';
comment on column monnaie.mnn_indice is 'Indice arbitraire de la monnaie (voire cote)';

comment on table perso_monnaie is 'Monnaie que porte le personnage';
comment on column perso_monnaie.pmn_id is 'Identifiant abstrait';
comment on column perso_monnaie.pmn_prs_id is 'Identifiant du personnage';
comment on column perso_monnaie.pmn_mnn_id is 'Identifiant de la monnaie';
comment on column perso_monnaie.pmn_montant is 'Qte de monnaie du personnage';

comment on table compagnie is 'Compagnie de personnages';
comment on column compagnie.cmp_id is 'Identifiant de la compagnie';
comment on column compagnie.cmp_nom is 'Nom de la compagnie';

comment on table place_compagnie is 'Place des personnages dans la compagnie';
comment on column place_compagnie.plc_prs_id is 'Identifiant du personnage';
comment on column place_compagnie.plc_cmp_id is 'Identifiant de la compagnie';
comment on column place_compagnie.plc_place is 'Place du personnage ds la compagnie';

comment on column sexe.sex_id is 'Identifiant';
comment on column sexe.sex_nom is 'Nom du sexe (masculin, f�minin, asexu�, ...)';
comment on column sexe.sex_insc_ouverte is 'Sexe disponible pour l''inscription';

comment on column type_personnage.tpr_id is 'Identifiant';
comment on column type_personnage.tpr_nom is 'Nom du type (PJ, PNJ, familier, Gob, ...)';

comment on column type_maladie.tml_id is 'Identifiant';
comment on column type_maladie.tml_nom is 'Nom du type (classe, race, instantan�e, d�finitive, temporaire)';

comment on column type_caracteristique.tcr_id is 'Identifiant';
comment on column type_caracteristique.tcr_nom is 'Nom du type (attribut, comp�tence)';

comment on column nature_caracteristique.ncr_id is 'Identifiant';
comment on column nature_caracteristique.ncr_nom is 'Nom de la nature (intellectuelle, physique, spirituelle, ...)';

comment on column type_secteur.tsc_id is 'Identifiant';
comment on column type_secteur.tsc_nom is 'Nom du type: immobile, mobile (monde), mobile (galaxie), mobile (v�hicule)';

comment on column type_monde.tmn_id is 'Identifiant';
comment on column type_monde.tmn_nom is 'Nom du type: galaxie (sans appui au sol), plan�te (avec appui)';

comment on column type_chemin.tch_id is 'Identifiant';
comment on column type_chemin.tch_nom is 'Nom du type: classique, acc�s trappe, acc�s sas, acc�s pont';

comment on column action.act_id is 'Identifiant';
comment on column action.act_nom is 'Nom';
comment on column action.act_nom_form is 'Nom de l''action dans le formulaire';
comment on column action.act_desc is 'Description de l''action';
comment on column action.act_form_attaque is 'Formule a resoudre pour l''attaque';
comment on column action.act_form_defense is 'Formule a resoudre pour la defense';
comment on column action.act_form_cout_pa is 'Formule determinant le cout en PA';
comment on column action.act_form_cout_pn is 'Formule determinant le cout en PN';
comment on column action.act_form_cout_pp is 'Formule determinant le cout en PP';
comment on column action.act_form_cout_pv is 'Formule determinant le cout en PV';
comment on column action.act_form_cout_pc is 'Cout en point de combat de l''action';
comment on column action.act_form_gain_min is 'Calcul du gain minimum de Points d''''eXperience';
comment on column action.act_form_gain_max is 'Calcul du gain maximum de Points d''''eXperience';
comment on column action.act_courante is 'Action proposee en tout lieu/pour toute classe ? (oui/non)';
comment on column action.act_combat is '� vrai si disponible en situation de combat';

comment on column lieu_action.lac_act_id is 'Identifiant de l''action sp�cialement consid�r�e en ce lieu';
comment on column lieu_action.lac_lie_id is 'Identifiant du lieu ayant une action particuliere';
comment on column lieu_action.lac_possible is 'Action possible physiquement en ce lieu ? (oui/non) (rempli si diff�rent du standard)';
comment on column lieu_action.lac_autorisee is 'Action autoris�e (moralement) en ce lieu ? (oui/non) (rempli si diff�rent du standard)';
comment on column lieu_action.lac_difficulte is 'Difficulte de l''action en ce lieu (rempli si diff�rent du standard)';
comment on column lieu_action.lac_cout_pa is 'Co�t en PA de l''action en ce lieu (rempli si diff�rent du standard)';
comment on column lieu_action.lac_cout_pn is 'Cout en PN de l''action';
comment on column lieu_action.lac_cout_pp is 'Cout en PP de l''action';
comment on column lieu_action.lac_cout_pv is 'Cout en PV de l''action';

comment on column typelieu_action.tla_act_id is 'Identifiant de l''action sp�cialement consid�r�e en ce type de lieu';
comment on column typelieu_action.tla_tli_id is 'Identifiant du type de lieu ayant une action particuliere';
comment on column typelieu_action.tla_possible is 'Action possible physiquement en ce type de lieu ? (oui/non) (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_autorisee is 'Action autoris�e (moralement) en ce type de lieu ? (oui/non) (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_difficulte is 'Difficulte de l''action en ce type de lieu (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_cout_pa is 'Co�t en PA de l''action en ce type de lieu (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_cout_pn is 'Co�t en PN de l''action en ce type de lieu (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_cout_pp is 'Co�t en PP de l''action en ce type de lieu (rempli si diff�rent du standard)';
comment on column typelieu_action.tla_cout_pv is 'Co�t en PV de l''action en ce type de lieu (rempli si diff�rent du standard)';

comment on column type_lieu.tli_id is 'Identifiant';
comment on column type_lieu.tli_nom is 'Nom du type de lieu';

comment on column debit_element.dbt_id is 'Identifiant';
comment on column debit_element.dbt_lie_id is 'Identifiant du lieu';
comment on column debit_element.dbt_egn_id is 'Identifiant de l''�l�ment g�n�rique cr�� dans ce lieu';

comment on column vente_element.vnt_id is 'Identifiant';
comment on column vente_element.vnt_lie_id is 'Identifiant du lieu';
comment on column vente_element.vnt_elm_id is 'Identifiant de l''�l�ment vendu en ce lieu';

comment on column depot_element.dpt_id is 'Identifiant';
comment on column depot_element.dpt_lie_id is 'Identifiant du lieu';
comment on column depot_element.dpt_elm_id is 'Identifiant de l''�l�ment d�pos� en ce lieu';
comment on column depot_element.dpt_prs_id is 'Identifiant du personnage poss�dant l''�lement';

comment on column banque.bnq_id is 'Identifiant';
comment on column banque.bnq_lie_id is 'Identifiant du lieu';
comment on column banque.bnq_prs_id is 'Identifiant du personnage poss�dant l''argent';
comment on column banque.bnq_mnn_id is 'Identifiant de la monnaie d�pos�e';
comment on column banque.bnq_somme is 'Somme d''argent d�pos�';

comment on column element_gen.egn_id is 'Identifiant';
comment on column element_gen.egn_nom is 'Nom de l''element generique';
comment on column element_gen.egn_desc is 'Description de l''element generique';
comment on column element_gen.egn_tel_id is 'Type d''element generique';
comment on column element_gen.egn_prix is 'Prix en monnaie arbitraire';
comment on column element_gen.egn_equipable is 'Definit si l''element peut etre equipe ou non';
comment on column element_gen.egn_possession_mld_id is 'Maladie liee a la simple possession de l''element';
comment on column element_gen.egn_equipe_mld_id is 'Maladie liee au fait d''equiper l''element';
comment on column element_gen.egn_tcn_id is 'Technologie de l''element';
comment on column element_gen.egn_mtr_id is 'Type de materiau';
comment on column element_gen.egn_taille is 'Taille de l''�l�ment g�n�rique';

comment on column arme.arm_id is 'Identifiant';
comment on column arme.arm_agn_id is 'Identifiant de l''arme generique';
comment on column arme.arm_rch_id is 'Identifiant de la recharge actuelle';
comment on column arme.arm_elm_id is 'Identifiant de l''element';

comment on column arme_gen.agn_id is 'Identifiant';
comment on column arme_gen.agn_tdg_id is 'Type de degat';
comment on column arme_gen.agn_egn_id is 'Element generique associe';
comment on column arme_gen.agn_mld_id is 'Maladie donnee en cas de succes';
comment on column arme_gen.agn_portee is 'Portee de l''arme';
comment on column arme_gen.agn_degat_min is 'Degat minimum en cas de succes';
comment on column arme_gen.agn_degat_max is 'Degat max en cas de succes';
comment on column arme_gen.agn_deux_mains is 'Arme a deux mains ?';
comment on column arme_gen.agn_tmt_id is 'Type de munition a utiliser pour les recharges';
comment on column arme_gen.agn_carac1_crc_id is 'Premi�re caract�ristique';
comment on column arme_gen.agn_carac2_crc_id is '�ventuelle seconde caracteristique';
comment on column arme_gen.agn_compet1_crc_id is 'Comp�tence associ�e � cette arme';
comment on column arme_gen.agn_compet2_crc_id is '�ventuelle seconde comp�tence';
comment on column arme_gen.agn_bonus_attaque is 'Bonus � l''attaque';
comment on column arme_gen.agn_bonus_defense is 'Bonus � la d�fense';

comment on column matiere.mtr_id is 'Identifiant';
comment on column matiere.mtr_nom is 'Nom de la matiere';
comment on column matiere.mtr_desc is 'Description de la matiere';
comment on column matiere.mtr_resistance is 'Indice de resistance';

comment on column technologie.tcn_id is 'Identifiant';
comment on column technologie.tcn_nom is 'Nom de la technologie';
comment on column technologie.tcn_desc is 'Description de la technologie';
comment on column technologie.tcn_taux_enrayage is 'Taux d''enrayage de la technologie';

comment on column armure_gen.ugn_id is 'Identifiant';
comment on column armure_gen.ugn_egn_id is 'Element generique associe';
comment on column armure_gen.ugn_bonus_defense is '�ventuel bonus donn� � la defense par cette arme';

comment on column recharge_gen.cgn_id is 'Identifiant abstrait';
comment on column recharge_gen.cgn_egn_id is 'Identifiant de l''element dont il est issu';
comment on column recharge_gen.cgn_mnt_id is 'Munition contenue dans ce type de chargeur';
comment on column recharge_gen.cgn_nb_max_mnt is 'Nombre maximal de munitions contenues dans ce type de chargeur';

comment on column vaisseau_gen.vgn_id is 'Identifiant';
comment on column vaisseau_gen.vgn_egn_id is 'Identifiant de l''element generique';
comment on column vaisseau_gen.vgn_charge_max is 'Charge maximale contenue par ce modele de vaisseau';
comment on column vaisseau_gen.vgn_tvs_id is 'Type du vaisseau (terrestre, aeroglisseur, sous-marin, spatiale)';

comment on column type_vaisseau.tvs_id is 'Identifiant';
comment on column type_vaisseau.tvs_nom is 'Nom du type de vaisseau';

comment on column munition.mnt_id is 'Identifiant';
comment on column munition.mnt_nom is 'Nom de la munition';
comment on column munition.mnt_desc is 'Description de la munition';
comment on column munition.mnt_tmt_id is 'Type de munition';
comment on column munition.mnt_mld_id is 'Maladie associee a ce type de munition';

comment on column type_degat.tdg_id is 'Identifiant';
comment on column type_degat.tdg_nom is 'Nom de la munition';

comment on column armure_gen_degat.agd_id is 'Identifiant abstrait';
comment on column armure_gen_degat.agd_ugn_id is 'Identifiant de l''armure generique';
comment on column armure_gen_degat.agd_tdg_id is 'Type de degat encaisse';
comment on column armure_gen_degat.agd_encaissement_min is 'Encaissement minimal';
comment on column armure_gen_degat.agd_encaissement_max is 'Encaissement maximal';

comment on column armure_gen_zone.agz_id is 'Identifiant abstrait';
comment on column armure_gen_zone.agz_ugn_id is 'Identifiant de l''armure generique';
comment on column armure_gen_zone.agz_zon_id is 'Zone protegee';

comment on column zone_armure.zon_id is 'Identifiant de la zone corporelle';
comment on column zone_armure.zon_nom is 'Nom de la zone protegee';

comment on column vaisseau_gen_poste.vgp_id is 'Identifiant abstrait';
comment on column vaisseau_gen_poste.vgp_vgn_id is 'Vaisseau generique dans lequel est ce poste';
comment on column vaisseau_gen_poste.vgp_tpt_id is 'Type de poste';
comment on column vaisseau_gen_poste.vgp_nb_perso is 'Nombre de personnes qui doivent occuper le poste';

comment on column type_poste_tmp.tpt_id is 'Identifiant';
comment on column type_poste_tmp.tpt_nom is 'Nom du type de vaisseau';

comment on column type_element.tel_id is 'Identifiant';
comment on column type_element.tel_nom is 'Nom de la technologie';

comment on column armure.aru_id is 'Identifiant';
comment on column armure.aru_elm_id is 'Identifiant de l''element';

comment on column recharge.rch_id is 'Identifiant';
comment on column recharge.rch_elm_id is 'Identifiant de l''element';
comment on column recharge.rch_nb_mnt is 'Nb de munitions';
comment on column recharge.rch_utilise is 'Recharge utilis�e dans une arme ?';

comment on column vaisseau.vss_id is 'Identifiant';
comment on column vaisseau.vss_elm_id is 'Identifiant de l''element';
comment on column vaisseau.vss_sct_id is 'Secteur representant le vaisseau';

comment on column type_classe.tcl_id is 'Identifiant';
comment on column type_classe.tcl_nom is 'Nom du type (classe, aptitude)';

comment on column interdit_classe.inc_id is 'Identifiant';
comment on column interdit_classe.inc_cls_id is 'Classe interdisant';
comment on column interdit_classe.inc_interdit_cls_id is 'Classe interdite';

comment on column prerequis_carac.pqa_id is 'Identifiant abstrait';
comment on column prerequis_carac.pqa_cln_id is 'Niveau de classe a atteindre';
comment on column prerequis_carac.pqa_form_carac is 'Formule a remplir pour avoir le niveau de classe';

comment on column taille.tll_id is 'Identifiant';
comment on column taille.tll_nom is 'Nom de la taille';
comment on column taille.tll_description is 'Description';
comment on column taille.tll_indice is 'Indice';

comment on column race.rac_id is 'Identifiant';
comment on column race.rac_nom is 'Nom';
comment on column race.rac_mld_id is 'Maladie (malus/bonus) apport�e';
comment on column race.rac_desc is 'Description physique des repr�sentants de cette race';
comment on column race.rac_monture is 'Peut porter des personnages (oui/non)';
comment on column race.rac_indice_taille is 'Indice de base de taille d''un membre de la race';
comment on column race.rac_insc_ouverte is 'Race disponible � l''inscription';

comment on table acces_connu is 'Chemin vers un secteur immobile connu par un personnage';
comment on column acces_connu.acc_id is 'Identifiant abstrait';
comment on column acces_connu.acc_chm_id is 'Identifiant du chemin connu vers un secteur immobile';
comment on column acces_connu.acc_prs_id is 'Identifiant du personnage';

comment on table monde_connu is 'Monde connu par un personnage';
comment on column monde_connu.mnc_id is 'Identifiant abstrait';
comment on column monde_connu.mnc_mnd_id is 'Identifiant du monde connu';
comment on column monde_connu.mnc_prs_id is 'Identifiant du personnage';

comment on column type_munition.tmt_id is 'Identifiant';
comment on column type_munition.tmt_nom is 'Nom du type de munition';

comment on table css is 'Styles CSS de l affichage web';
comment on column css.css_id is 'Identifiant';
comment on column css.css_nom is 'Nom du style';
comment on column css.css_defaut is 'Style par defaut du jeu';
comment on column css.css_chemin is 'Chemin du fichier CSS';

comment on table itineraire is 'Itineraires empruntes par les personnages';
comment on column itineraire.itn_id is 'Identifiant';
comment on column itineraire.itn_chm_id is 'Identifiant eventuel du chemin emprunt� (deplacement lieu-lieu)';
comment on column itineraire.itn_frm_id is 'Identifiant de la forme du personnage';
comment on column itineraire.itn_depart_lie_id is 'Identifiant du lieu de depart';
comment on column itineraire.itn_arrivee_lie_id is 'Identifiant du lieu d''arrivee';
comment on column itineraire.itn_traces_initiales is 'Quantite de traces initialement laissees par le suivi';
comment on column itineraire.itn_traces_restantes is 'Quantite de traces restantes.';
comment on column itineraire.itn_date is 'Date du deplacement.';
comment on column itineraire.itn_index_effacement is 'Nombre de traces qui vont dispara�tre � la prochaine "remise g�n�rale"';
comment on column itineraire.itn_ttr_id is 'Identifiant du type de trace';
comment on column itineraire.itn_profond_trace is 'Profondeur de la trace';
comment on column itineraire.itn_taille_trace is 'Pointure de la trace';

comment on column type_filature.tfl_id is 'Identifiant';
comment on column type_filature.tfl_nom is 'Nom du type: filature (voleur, de visu), pistage (par trace), suite (pour guide, volontaire)';

comment on table mime is 'Table de mimes (actions gestuelles)';
comment on column mime.mim_id is 'Identifiant';
comment on column mime.mim_nom is 'Nom du mime';
comment on column mime.mim_label is 'Chaine de caracteres symbolisant le mime';
comment on column mime.mim_tmm_id is 'Type de mime';

comment on column type_mime.tmm_id is 'Identifiant';
comment on column type_mime.tmm_nom is 'Nom du type: simple, li� � un personnage';

comment on table rumeur is 'Tables des rumeurs';
comment on column rumeur.rmr_id is 'Identifiant';
comment on column rumeur.rmr_nom is 'Nom de la rumeur';
comment on column rumeur.rmr_prs_id is 'Identifiant du personnage a l''origine de la rumeur';
comment on column rumeur.rmr_content is 'Contenu de la rumeur';
comment on column rumeur.rmr_lng_id is 'Langue de la rumeur';
comment on column rumeur.rmr_lie_id is 'Lieu d''origine de la rumeur';
comment on column rumeur.rmr_rayon is 'Rayon de la rumeur (pour les lieux g�ographiques)';
comment on column rumeur.rmr_sct_id is '�ventuel secteur d''influence si la rumeur a �t� lanc�e depuis un lieu topologique';
comment on column rumeur.rmr_diff is 'Difficult� pour d�couvrir la rumeur';
comment on column rumeur.rmr_date_debut is 'Date d''apparition de la rumeur';
comment on column rumeur.rmr_date_fin is 'Date de fin de la rumeur';

comment on table rumeur_ecoute is 'Tables des ecoutes de rumeurs';
comment on column rumeur_ecoute.rmc_id is 'Identifiant';
comment on column rumeur_ecoute.rmc_prs_id is 'Identifiant du personnage ayant entendu la rumeur';
comment on column rumeur_ecoute.rmc_rmr_id is 'Identifiant de la rumeur';

comment on table langue is 'Langue';
comment on column langue.lng_id is 'Identifiant';
comment on column langue.lng_nom is 'Nom';
comment on column langue.lng_description is 'Description de son histoire, son utilisation, ...';

comment on table filature is 'Tables des filatures, pistages, guides';
comment on column filature.flt_id is 'Identifiant';
comment on column filature.flt_suivant_prs_id is 'Identifiant du personnage qui fait l''action de suivre.';
comment on column filature.flt_suivi_frm_id is 'Identifiant de la forme suivie.';
comment on column filature.flt_tfl_id is 'Type de filature';
comment on column filature.flt_debut is 'Date de d�but de la filature';

comment on table type_trace is 'Types de trace disponibles';
comment on column type_trace.ttr_id is 'Identifiant abstrait';
comment on column type_trace.ttr_nom is 'Nom de la trace';

comment on table trace_connue is 'Traces connues par les personnages';
comment on column trace_connue.trc_id is 'Identifiant';
comment on column trace_connue.trc_ttr_id is 'Identifiant du type de trace';
comment on column trace_connue.trc_taille is 'Pointure';
comment on column trace_connue.trc_profondeur is 'Profondeur';
comment on column trace_connue.trc_frm_trace_id is 'Identifiant de la forme qui a laiss� les traces';
comment on column trace_connue.trc_prs_id is 'Identifiant du personnage qui a d�couvert les traces';
comment on column trace_connue.trc_date_fin is 'Date jusqu''� laquelle on se souviendra de ces traces';
comment on column trace_connue.trc_nom_donne is 'Nom donn� � ces traces';

comment on column lieu_carto.lic_id is 'Identifiant';
comment on column lieu_carto.lic_lie_id is 'Identifiant du leiu associ�';
comment on column lieu_carto.lic_mnd_id is 'Identifiant du monde associ�';
comment on column lieu_carto.lic_ssn_id is 'Saison du lieu';

comment on table cycle is 'Succession de periodes ou de cycles';
comment on column cycle.ccl_id is 'Identifiant';
comment on column cycle.ccl_nom is 'Nom';
comment on column cycle.ccl_nombre_periode is 'Nombre de periodes du cycle';
comment on column cycle.ccl_duree_periode is 'Duree d une periode en heure';
comment on column cycle.ccl_duree_nombre_cycles is 'La periode du cycle correspond a n autres cycles';
comment on column cycle.ccl_duree_ccl_id is 'Identifiant du cycle etalon';
comment on column cycle.ccl_duree_nombre_cycles_ecoules is 'Nombre de cycles ecoules';
comment on column cycle.ccl_pourcentage_alea is 'Alea eventuel: 0% pour perfection; 100% pour un alea complet';

comment on table cycle_monde is 'Rattachement de cycles aux mondes';
comment on column cycle_monde.cmn_id is 'Identifiant';
comment on column cycle_monde.cmn_mnd_id is 'Identifiant du monde associ�';
comment on column cycle_monde.cmn_ccl_id is 'Identifiant du cycle associ�';

comment on table cycle_secteur is 'Rattachement de cycles aux secteurs';
comment on column cycle_secteur.csc_id is 'Identifiant';
comment on column cycle_secteur.csc_sct_id is 'Identifiant du secteur associ�';
comment on column cycle_secteur.csc_ccl_id is 'Identifiant du cycle associ�';

comment on table periode is 'Detail d une periode d un cycle';
comment on column periode.prd_id is 'Identifiant';
comment on column periode.prd_nom is 'Nom';
comment on column periode.prd_ccl_id is 'Cycle correspondant';
comment on column periode.prd_min is 'Minimum de periode';
comment on column periode.prd_max is 'Maximum de periode';
comment on column periode.prd_complement_description is 'Complement de description de la periode';

comment on table lieu_cycle is 'Rattachement de lieux aux cycles';
comment on column lieu_cycle.lcd_id is 'Identifiant';
comment on column lieu_cycle.lcd_ccl_id is 'Identifiant du cycle associ�';
comment on column lieu_cycle.lcd_lie_id is 'Identifiant du lieu associ�';

comment on table lieu_cyclique is 'Aspect compl�mentaire d un lieu � un cycle donn�';
comment on column lieu_cyclique.lcc_id is 'Identifiant';
comment on column lieu_cyclique.lcc_prd_id is 'Identifiant de la p�riode associ�e';
comment on column lieu_cyclique.lcc_lie_id is 'Identifiant du lieu associ�';
comment on column lieu_cyclique.lcc_description is 'Description compl�mentaire � cette p�riode donn�e';

comment on table calque is 'Calque g�ographique repr�sentant selon : le relief, le climat, les voieries, etc.';
comment on column calque.clq_id is 'Identifiant';
comment on column calque.clq_nom is 'Nom';
comment on column calque.clq_description is 'Description';
comment on column calque.clq_indice_population is 'Indice de densit� de population';
comment on column calque.clq_facteur_effacement_trace is 'Facteur influant sur l''effacement des traces';
comment on column calque.clq_facteur_nb_trace is 'Facteur influant sur le nombre de traces';
comment on column calque.clq_cout_pn is 'Cout journalier en pn';
comment on column calque.clq_cout_acces_pn is 'Cout d''acc�s en pn';
comment on column calque.clq_cout_pv is 'Cout journalier en point de vie';
comment on column calque.clq_cout_acces_pa is 'Cout d''acc�s en point d''action';
comment on column calque.clq_cout_echec_pv is 'Cout en pv en cas d''�chec';
comment on column calque.clq_couleur is 'Code couleur associ�';
comment on column calque.clq_tcq_id is 'Type de calque';
comment on column calque.clq_id_externe is 'Identifiant externe � l''application utilis� lors des imports';
comment on column calque.clq_difficulte is 'Formule de difficult� de d�placement dans ces zones et de fait facilit� � se perdre';
comment on column calque.clq_crc_id is 'Comp�tence associ�e';
comment on column calque.clq_crc2_crc_id is '�ventuelle deuxi�me caract�ristique associ�e';
comment on column calque.clq_echec_bloquant is 'Si � vrai, �chouer sur son d�placement ne fait pas progresser';

comment on table type_calque is 'Type de calque';
comment on column type_calque.tcq_id is 'Identifiant';
comment on column type_calque.tcq_nom is 'Nom du type de calque';
comment on column type_calque.tcq_principal is 'Un type de calque est d�sign� comme le type de calque principal. Le label de base sera prit sur le calque correspondant au type de calque principal.';

comment on table calque_lieu_carto is 'Rattachement de calques aux lieux carto';
comment on column calque_lieu_carto.clc_id is 'Identifiant';
comment on column calque_lieu_carto.clc_clq_id is 'Identifiant du calque associ�';
comment on column calque_lieu_carto.clc_lic_id is 'Identifiant du lieu carto associ�';

comment on table type_vehicule is 'Type de vehicule';
comment on column type_vehicule.tvh_id is 'Identifiant';
comment on column type_vehicule.tvh_nom is 'Nom du type de v�hicule';
comment on column type_vehicule.tvh_carburant_egn_id is 'Type de carburant employ�';
comment on column type_vehicule.tvh_puissance is 'Puissance developp�e lors d''un d�placement';
comment on column type_vehicule.tvh_consommation is 'Nombre d''unit� de carburant utilis� par tour';
comment on column type_vehicule.tvh_usure_max is 'Maximum de point d''usure pour un vehicule r�par� ou neuf';
comment on column type_vehicule.tvh_usure is 'Point d''usure gagn� � chaque tour lorsque en marche';

comment on table vent_lieu_carto is 'Rattachement de vents aux lieux carto';
comment on column vent_lieu_carto.cvt_id is 'Identifiant';
comment on column vent_lieu_carto.cvt_tvt_id is 'Identifiant du type de vent associ�';
comment on column vent_lieu_carto.cvt_lic_id is 'Identifiant du lieu carto associ�';
comment on column vent_lieu_carto.cvt_force is 'Force du vent';

comment on table restriction_calque is 'Restriction d un type de vehicule � un calque en particulier';
comment on column restriction_calque.rtc_id is 'Identifiant';
comment on column restriction_calque.rtc_tvh_id is 'Identifiant du type de v�hicule';
comment on column restriction_calque.rtc_clq_id is 'Identifiant du calque de restriction';
comment on column restriction_calque.rtc_rendement is 'Rendement associ� � ce type de terrain';

comment on table influence_vent is 'Vent influen�ant les d�placements d un type de vehicule';
comment on column influence_vent.inv_id is 'Identifiant';
comment on column influence_vent.inv_tvh_id is 'Identifiant du type de v�hicule';
comment on column influence_vent.inv_tvt_id is 'Identifiant du type de vent';
comment on column influence_vent.inv_puissance is 'Puissance associ�e � ce moyen de propulsion';

comment on table vehicule is 'V�hicule';
comment on column vehicule.vhc_id is 'Identifiant';
comment on column vehicule.vhc_tvh_id is 'Identifiant du type de v�hicule';
comment on column vehicule.vhc_reservoir is 'Maximum de ressource pouvant �tre stock�';
comment on column vehicule.vhc_reservoir_actuel is 'Niveau de ressource actuel dans le r�servoir';
comment on column vehicule.vhc_usure_actuelle is 'Niveau d''usure actuel';
comment on column vehicule.vhc_amarre is '� vrai si le v�hicule est amarr�';
comment on column vehicule.vhc_moteur_fonctionnel is '� vrai si le moteur est en route';
comment on column vehicule.vhc_masse is 'Masse � d�placer';
comment on column vehicule.vhc_bride_moteur is 'Pourcentage de puissance laiss�e pour le moteur';
comment on column vehicule.vhc_bride_vent is 'Pourcentage de puissance laiss�e pour la propulsion passive';
comment on column vehicule.vhc_direction is 'Direction souhait�e';
comment on column vehicule.vhc_date_remise is 'Date de remise';
comment on column vehicule.vhc_reserve_nourriture is 'R�serve de nourriture max dans le v�hicule';
comment on column vehicule.vhc_reserve_nourriture_actuel is 'Niveau actuel de la r�serve de nourriture';

comment on table type_vent is 'Type de vent';
comment on column type_vent.tvt_id is 'Identifiant';
comment on column type_vent.tvt_nom is 'Nom du type de vent';

comment on table type_poste is 'Type de poste';
comment on column type_poste.tps_id is 'Identifiant';
comment on column type_poste.tps_nom is 'Nom du type de poste';
comment on column type_poste.tps_rendement_min is 'Rendement minimum d''un poste';
comment on column type_poste.tps_propulsion_active is 'Poste pour une propulsion active ?';
comment on column type_poste.tps_formule_difficulte is 'Formule de difficult� de tenue du poste';

comment on table poste is 'Poste en place sur un v�hicule';
comment on column poste.pst_id is 'Identifiant';
comment on column poste.pst_tps_id is 'Identifiant du type de poste';
comment on column poste.pst_prs_id is 'Identifiant de l''�ventuelle personne assurant le poste';
comment on column poste.pst_vhc_id is 'Identifiant du v�hicule associ�';

comment on table type_vehicule_poste is 'Type poste en place sur un type de v�hicule';
comment on column type_vehicule_poste.ttp_id is 'Identifiant';
comment on column type_vehicule_poste.ttp_tvh_id is 'Identifiant du type de v�hicule';
comment on column type_vehicule_poste.ttp_tps_id is 'Identifiant du type de poste';

comment on table monture is 'Monture (cheval, �l�phant, moto...)';
comment on column monture.mon_id is 'Identifiant de la monture';
comment on column monture.mon_prs_id is 'Personnage associ� � la monture';
comment on column monture.mon_cavalier_prs_id is '�ventuel identifiant du cavalier';
comment on column monture.mon_clef_elm_id is '�ventuelle cl� n�cessaire pour faire fonctionner la monture';
comment on column monture.mon_vivante is 'Indique si une cr�ature est vivante ou non (et notamment si elle consomme des PNs � l''arr�t)';
comment on column monture.mon_egn_id is '�ventuel type de carburant � associer � la monture';
comment on column monture.mon_consomation is 'Consomation par case franchie';
comment on column monture.mon_usure is 'Dans le cas de monture inanim�e, usure (en � PV �) par d�placement';
comment on column monture.mon_surplus_cout_pn is '�ventuel surplus de cout de PN en charge';
comment on column monture.mon_formule_difficulte is 'Formule de maitrise de la monture';

comment on table particularite_chemin is 'Particularit� associ�e � un chemin (champs textuel du style � par la fen�tre �,  � par la porte �)';
comment on column particularite_chemin.pch_id is 'Identifiant';
comment on column particularite_chemin.pch_nom is 'Nom de la particularit�';

comment on table description_lieu is 'Description plus pr�cises de certains lieux';
comment on column description_lieu.dsl_id is 'Identifiant';
comment on column description_lieu.dsl_lie_id is '	';
comment on column description_lieu.dsl_difficulte is 'Difficult� associ�e � la connaissance de cette description';
comment on column description_lieu.dsl_description is 'Compl�ment de description';

comment on table classeniveau_action is 'Limitation des actions pour certains niveaux de certaines classes';
comment on column classeniveau_action.cna_id is 'Identifiant';
comment on column classeniveau_action.cna_act_id is 'Identifiant de l''action sp�cialement consid�r�e pour cette classe';
comment on column classeniveau_action.cna_cln_id is 'Identifiant du niveau de classe autoris�';
comment on column classeniveau_action.cna_autorise is 'Action autoris�e (True) ou interdite (False) sp�cifiquement pour cette classe';

comment on table engagement is 'Engagement entre personnage';
comment on column engagement.eng_id is 'Identifiant';
comment on column engagement.eng_prs1_prs_id is 'Identifiant du premier personnage impliqu� dans l''engagement';
comment on column engagement.eng_prs2_prs_id is 'Identifiant du deuxi�me personnage impliqu� dans l''engagement';
comment on column engagement.eng_init_1 is '� vrai si le premier personnage a l''initiative sinon l''initiative appartient au second personnage';
comment on column engagement.eng_lie_id is 'Lieu o� se passe la confrontation';
comment on column engagement.eng_distant is '� vrai si engagement distant sinon engagement de corps � corps';

comment on table coffre is 'Conteneur d''objet';
comment on column coffre.cff_id is 'Identifiant';
comment on column coffre.cff_elm_id is 'Identifiant de l''�l�ment associ�';
comment on column coffre.cff_clef_egn_id is 'Clef g�n�rique du coffre';
comment on column coffre.cff_taille is 'Indice de la contenance de ce coffre';
comment on column coffre.cff_ouvert is '�tat ouvert/ferm�';

comment on column coffre_element.cel_id is 'Identifiant';
comment on column coffre_element.cel_cff_id is 'Coffre associ�';
comment on column coffre_element.cel_elm_id is '�l�ment associ�';
comment on column coffre_element.cel_prix is '�ventuel prix associ� si un magasin est associ� au coffre';
comment on column coffre_element.cel_mnn_id is 'Monnaie associ�e';

comment on table element_visible is '�l�ments visible pour un personnage';
comment on column element_visible.elv_id is 'Identifiant';
comment on column element_visible.elv_prs_id is 'Personnage associ�';
comment on column element_visible.elv_elm_id is '�l�ment associ�';

comment on table image is 'Image';
comment on column image.img_id is 'Identifiant';
comment on column image.img_nom is 'Nom associ�';
comment on column image.img_adresse is 'Adresse relative de l''image';

comment on table boutique is 'Boutique';
comment on column boutique.btq_id is 'Identifiant';
comment on column boutique.btq_cff_id is 'Identifiant du coffre associ�';
comment on column boutique.btq_nom is 'Nom de la boutique';
comment on column boutique.btq_description is '�ventuelle description associ�e � la boutique';
comment on column boutique.btq_prs_id is '�ventuel g�rant de la boutique';
comment on column boutique.btq_atm_id is '�ventuel automate associ�';
comment on column boutique.btq_ouverte is '� vrai si la boutique est ouverte';

comment on table automate is 'Automate similant un personnage. Des caract�ristiques lui sont attribu�es.';
comment on column automate.atm_id is 'Identifiant';
comment on column automate.atm_nom is '�ventuel nom associ�';

comment on table element_gen_connu is '�l�ments g�n�riques identifi�s par un personnage';
comment on column element_gen_connu.egc_id is 'Identifiant';
comment on column element_gen_connu.egc_egn_id is '�l�ment g�n�rique associ�';
comment on column element_gen_connu.egc_prs_id is 'Personnage associ�';

comment on table automate_carac is 'Caract�ristiques associ�es � un automate';
comment on column automate_carac.acr_id is 'Identifiant abstrait';
comment on column automate_carac.acr_atm_id is 'Automate associ�';
comment on column automate_carac.acr_crc_id is 'Caracteristique associ�e';
comment on column automate_carac.acr_score is 'Score dans la caract�ristique.';

comment on table achat_element_gen is '�l�ments g�n�riques achetables par une boutique donn�e';
comment on column achat_element_gen.aeg_id is 'Identifiant';
comment on column achat_element_gen.aeg_egn_id is '�l�ment g�n�rique associ�';
comment on column achat_element_gen.aeg_btq_id is 'Boutique associ�e';
comment on column achat_element_gen.aeg_prix is 'Prix de base';
comment on column achat_element_gen.aeg_mnn_id is 'Monnaie associ�e au prix';
comment on column achat_element_gen.aeg_nombre_max is 'Nombre maximum d''�l�ments achetables';

comment on table type_environnement is 'Environnement d''un secteur';
comment on column type_environnement.tev_id is 'Identifiant';
comment on column type_environnement.tev_nom is 'Nom de l''environnement';
comment on column type_environnement.tev_crc_id is 'Caract�ristique associ�e';

comment on table profil_annonce is 'Profil mis � disposition';
comment on column profil_annonce.pro_id is 'Identifiant';
comment on column profil_annonce.pro_nom is 'Nom';
comment on column profil_annonce.pro_statut is 'Entier pointant vers des statuts';
comment on column profil_annonce.pro_ctp_id is 'Cat�gorie de profil';
comment on column profil_annonce.pro_mji_id is 'MJ associ�';
comment on column profil_annonce.pro_desc is 'Description';
comment on column profil_annonce.pro_debat is 'Discussion entre MJs';
comment on column profil_annonce.pro_nbpersos is 'Nombre de personnages � pourvoir';
comment on column profil_annonce.pro_historique is 'Historique';
comment on column profil_annonce.pro_demandeaction is 'Demande d''action sur un profil';

comment on table news is 'Nouvelles';
comment on column news.new_id is 'Identifiant';
comment on column news.new_titre is 'Titre de la nouvelle';
comment on column news.new_txt is 'Texte de la nouvelle';
comment on column news.new_date is 'Date de publication';
comment on column news.new_dateedit is 'Date de derni�re modification';
comment on column news.new_mji_id is 'MJ associ�';

comment on table categorie_profil is 'Cat�gories des profils';
comment on column categorie_profil.ctp_id is 'Identifiant';
comment on column categorie_profil.ctp_nom is 'Nom';

comment on table candidature is 'Candidature';
comment on column candidature.can_id is 'Identifiant';
comment on column candidature.can_jou_id is 'Joueur associ�';
comment on column candidature.can_pro_id is 'Profil associ�';
comment on column candidature.can_statut is 'Statut de la candidature';
comment on column candidature.can_nomperso is 'Nom du personnage';
comment on column candidature.can_rac_id is 'Race associ�e';
comment on column candidature.can_cls_id is 'Classe associ�e';
comment on column candidature.can_sex_id is 'Sexe associ�';
comment on column candidature.can_descriptionperso is 'Description du personnage';
comment on column candidature.can_backgroundperso is 'Background du personnage';
comment on column candidature.can_butsperso is 'Buts du personnage';
comment on column candidature.can_debatjoumj is 'D�bat entre le recruteur et le joueur';
comment on column candidature.can_debatmj is 'D�bat entre le recruteur et le MJ qui a propos� le profil';
comment on column candidature.can_historique is 'Historique de la candidature';
comment on column candidature.can_commentaire is 'Pense-b�te pour le recruteur';
comment on column candidature.can_datemodification is 'Date de derni�re modification';

comment on table question is 'Question du quizz';
comment on column question.que_id is 'Identifiant';
comment on column question.que_intitule is 'Intitul� de la question';

comment on table reponse is 'Proposition de r�ponse � une question du quizz';
comment on column reponse.rep_id is 'Identifiant';
comment on column reponse.rep_que_id is 'Question associ�e';
comment on column reponse.rep_intitule is 'Intitul� de la r�ponse';
comment on column reponse.rep_bonne is '� vrai si la r�ponse est bonne';

comment on column profil_classe.pcl_id is 'Identifiant abstrait';
comment on column profil_classe.pcl_pro_id is 'Profil associ�';
comment on column profil_classe.pcl_cls_id is 'Classe associ�e';

comment on column profil_race.pra_id is 'Identifiant abstrait';
comment on column profil_race.pra_pro_id is 'Profil associ�';
comment on column profil_race.pra_rac_id is 'Race associ�e';

comment on column profil_sexe.psx_id is 'Identifiant abstrait';
comment on column profil_sexe.psx_pro_id is 'Profil associ�';
comment on column profil_sexe.psx_sex_id is 'Sexe associ�';


