-- Dégâts
INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (1, 'tranchant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (2, 'perforant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (3, 'contondant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (4, 'explosif');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (5, 'ballistique');


-- Sabre GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (1, 4, 1, null, 1, 1, 3, false, null, 1, NULL, 11, NULL);

-- Sabre
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (1, 1, null, 6);


-- Epee ancestrale GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (2, 5, 1, null, null, 2, 4, true, null, 1, NULL, 11, NULL);

-- Epee ancestrale
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (2, 2, null, 7);


-- Mitraillette lance-grenade (partie mitraillette) GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (3, 6, 5, null, 20, 2, 4, true, 1, 2, NULL, 15, NULL);

-- Mitraillette lance-grenade (partie lance-grenade) GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (4, 6, 4, null, 20, 5, 10, true, 2, 2, NULL, 15, NULL); -- Maladie ?


-- Mitraillette lance-grenade (partie mitraillette)
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (3, 3, 1, 8);

-- Mitraillette lance-grenade (partie lance-grenade)
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (4, 4, 2, 8);


-- Arc court GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (5, 9, 2, null, 10, 2, 3, true, 3, 2, NULL, 15, NULL);

-- Arc court
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (5, 5, null, 11);

-- Mains nues GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (6, 12, 3, null, 1, 1, 1, true, null, 2, NULL, 9, NULL);

-- Mimines pour tous
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (6, 6, null, 14);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (7, 6, null, 15);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (8, 6, null, 16);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (9, 6, null, 17);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (10, 6, null, 18);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (11, 6, null, 19);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (12, 6, null, 20);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (13, 6, null, 21);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (14, 6, null, 22);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (15, 6, null, 23);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (16, 6, null, 24);
