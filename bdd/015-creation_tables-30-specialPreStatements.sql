
-- Special statements for postgres:pre databases
  DROP SEQUENCE personnage_prs_id_seq CASCADE;
CREATE SEQUENCE personnage_prs_id_seq START 100;
  DROP SEQUENCE itineraire_itn_id_seq CASCADE;
CREATE SEQUENCE itineraire_itn_id_seq START 100;
  DROP SEQUENCE type_trace_ttr_id_seq CASCADE;
CREATE SEQUENCE type_trace_ttr_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE forme_frm_id_seq CASCADE;
CREATE SEQUENCE forme_frm_id_seq START 100;
  DROP SEQUENCE race_rac_id_seq CASCADE;
CREATE SEQUENCE race_rac_id_seq START 100;
  DROP SEQUENCE sexe_sex_id_seq CASCADE;
CREATE SEQUENCE sexe_sex_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE classe_cls_id_seq CASCADE;
CREATE SEQUENCE classe_cls_id_seq START 100;
  DROP SEQUENCE classe_niveau_cln_id_seq CASCADE;
CREATE SEQUENCE classe_niveau_cln_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE chemin_chm_id_seq CASCADE;
CREATE SEQUENCE chemin_chm_id_seq START 100;
  DROP SEQUENCE saison_ssn_id_seq CASCADE;
CREATE SEQUENCE saison_ssn_id_seq START 100;
  DROP SEQUENCE lieu_lie_id_seq CASCADE;
CREATE SEQUENCE lieu_lie_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE secteur_sct_id_seq CASCADE;
CREATE SEQUENCE secteur_sct_id_seq START 100;
  DROP SEQUENCE type_environnement_tev_id_seq CASCADE;
CREATE SEQUENCE type_environnement_tev_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE monde_mnd_id_seq CASCADE;
CREATE SEQUENCE monde_mnd_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE element_elm_id_seq CASCADE;
CREATE SEQUENCE element_elm_id_seq START 100;
  DROP SEQUENCE technologie_tcn_id_seq CASCADE;
CREATE SEQUENCE technologie_tcn_id_seq START 100;
  DROP SEQUENCE matiere_mtr_id_seq CASCADE;
CREATE SEQUENCE matiere_mtr_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE caracteristique_crc_id_seq CASCADE;
CREATE SEQUENCE caracteristique_crc_id_seq START 100;
  DROP SEQUENCE categorie_caracteristique_ccr_id_seq CASCADE;
CREATE SEQUENCE categorie_caracteristique_ccr_id_seq START 100;
  DROP SEQUENCE langue_lng_id_seq CASCADE;
CREATE SEQUENCE langue_lng_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE maladie_mld_id_seq CASCADE;
CREATE SEQUENCE maladie_mld_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE monnaie_mnn_id_seq CASCADE;
CREATE SEQUENCE monnaie_mnn_id_seq START 100;
  DROP SEQUENCE compagnie_cmp_id_seq CASCADE;
CREATE SEQUENCE compagnie_cmp_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE joueur_jou_id_seq CASCADE;
CREATE SEQUENCE joueur_jou_id_seq START 100;
  DROP SEQUENCE mj_mji_id_seq CASCADE;
CREATE SEQUENCE mj_mji_id_seq START 100;
  DROP SEQUENCE css_css_id_seq CASCADE;
CREATE SEQUENCE css_css_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE action_act_id_seq CASCADE;
CREATE SEQUENCE action_act_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE element_gen_egn_id_seq CASCADE;
CREATE SEQUENCE element_gen_egn_id_seq START 100;
  DROP SEQUENCE arme_gen_agn_id_seq CASCADE;
CREATE SEQUENCE arme_gen_agn_id_seq START 100;
  DROP SEQUENCE armure_gen_ugn_id_seq CASCADE;
CREATE SEQUENCE armure_gen_ugn_id_seq START 100;
  DROP SEQUENCE vaisseau_gen_vgn_id_seq CASCADE;
CREATE SEQUENCE vaisseau_gen_vgn_id_seq START 100;
  DROP SEQUENCE automate_atm_id_seq CASCADE;
CREATE SEQUENCE automate_atm_id_seq START 100;
  DROP SEQUENCE boutique_btq_id_seq CASCADE;
CREATE SEQUENCE boutique_btq_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_munition_tmt_id_seq CASCADE;
CREATE SEQUENCE type_munition_tmt_id_seq START 100;
  DROP SEQUENCE munition_mnt_id_seq CASCADE;
CREATE SEQUENCE munition_mnt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_element_tel_id_seq CASCADE;
CREATE SEQUENCE type_element_tel_id_seq START 100;
  DROP SEQUENCE type_vaisseau_tvs_id_seq CASCADE;
CREATE SEQUENCE type_vaisseau_tvs_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE vaisseau_gen_vsg_id_seq CASCADE;
CREATE SEQUENCE vaisseau_gen_vsg_id_seq START 100;
  DROP SEQUENCE type_vaisseau_tvs_id_seq CASCADE;
CREATE SEQUENCE type_vaisseau_tvs_id_seq START 100;
  DROP SEQUENCE type_poste_tpt_id_seq CASCADE;
CREATE SEQUENCE type_poste_tpt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE zone_zon_id_seq CASCADE;
CREATE SEQUENCE zone_zon_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_degat_tdg_id_seq CASCADE;
CREATE SEQUENCE type_degat_tdg_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE filature_flt_id_seq CASCADE;
CREATE SEQUENCE filature_flt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE mime_mim_id_seq CASCADE;
CREATE SEQUENCE mime_mim_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE rumeur_rmr_id_seq CASCADE;
CREATE SEQUENCE rumeur_rmr_id_seq START 100;
  DROP SEQUENCE rumeur_ecoute_rmc_id_seq CASCADE;
CREATE SEQUENCE rumeur_ecoute_rmc_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE type_lieu_tpl_id_seq CASCADE;
CREATE SEQUENCE type_lieu_tpl_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE debit_element_dbt_id_seq CASCADE;
CREATE SEQUENCE debit_element_dbt_id_seq START 100;
  DROP SEQUENCE vente_element_vnt_id_seq CASCADE;
CREATE SEQUENCE vente_element_vnt_id_seq START 100;
  DROP SEQUENCE depot_element_dpt_id_seq CASCADE;
CREATE SEQUENCE depot_element_dpt_id_seq START 100;
  DROP SEQUENCE banque_bnq_id_seq CASCADE;
CREATE SEQUENCE banque_bnq_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE arme_arm_id_seq CASCADE;
CREATE SEQUENCE arme_arm_id_seq START 100;
  DROP SEQUENCE armure_aru_id_seq CASCADE;
CREATE SEQUENCE armure_aru_id_seq START 100;
  DROP SEQUENCE recharge_rch_id_seq CASCADE;
CREATE SEQUENCE recharge_rch_id_seq START 100;
  DROP SEQUENCE vaisseau_vss_id_seq CASCADE;
CREATE SEQUENCE vaisseau_vss_id_seq START 100;
  DROP SEQUENCE coffre_cff_id_seq CASCADE;
CREATE SEQUENCE coffre_cff_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE calque_clq_id_seq CASCADE;
CREATE SEQUENCE calque_clq_id_seq START 100;
  DROP SEQUENCE type_calque_tcq_id_seq CASCADE;
CREATE SEQUENCE type_calque_tcq_id_seq START 100;
  DROP SEQUENCE type_vent_tvt_id_seq CASCADE;
CREATE SEQUENCE type_vent_tvt_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE lieu_lie_id_seq CASCADE;
CREATE SEQUENCE lieu_lie_id_seq START 1000;
  DROP SEQUENCE image_img_id_seq CASCADE;
CREATE SEQUENCE image_img_id_seq START 100;

-- Special statements for postgres:pre databases
  DROP SEQUENCE vehicule_vhc_id_seq CASCADE;
CREATE SEQUENCE vehicule_vhc_id_seq START 100;
  DROP SEQUENCE type_vehicule_tvh_id_seq CASCADE;
CREATE SEQUENCE type_vehicule_tvh_id_seq START 1000;
  DROP SEQUENCE type_poste_tps_id_seq CASCADE;
CREATE SEQUENCE type_poste_tps_id_seq START 100;

-- Special statements for postgres:pre databases
DROP INDEX idx_lic_localisation;

