-- Types de munition
INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (1, 'Balle');

INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (2, 'Grenade');

INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (3, 'Fleche');



-- Balles classique
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (1, 'Balle classique', 'Efficace, pas trop chère (généralement), ce genre de munition est prisé.', 1, null);

-- Balles explosives -> maladie
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (2, 'Balle explosive', 'A l''impact, cette balle se décompose en une explosion, provoquant des dégâts conséquents.', 1, null);

-- Grenades explosives
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (3, 'Grenade explosive', 'Une bonne grenade classique, qui fait tres tres mal.', 2, null);

-- Fleches
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (4, 'Fleche', 'Pfffttt. Rapide et précise.', 3, null);



-- Chargeur 20 balles classiques
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (1, 7, 1, 20);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (1, 9, 18);


-- Chargeur 2 grenades explosives
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (2, 8, 3, 2);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (2, 10, 2);


-- Carquois de 30 fleches
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (3, 10, 4, 30);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (3, 12, 27);


