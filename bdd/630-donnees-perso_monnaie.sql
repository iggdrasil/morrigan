
-- Siltaom a 100 po et 30 pa
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (1, 1, 100);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (1, 2, 30);

-- Nimnae a 10 po et 50 pc
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (2, 1, 10);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (2, 3, 50);

-- Arkenlond a 26 po et 35 pa
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (3, 1, 26);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (3, 2, 35);

-- Syl n'est guère riche
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (5, 1, 3);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (5, 2, 10);

-- Sulfure ne l'est pas plus !
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (6, 1, 1);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (6, 2, 3);

