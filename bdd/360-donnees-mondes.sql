-- Saisons

INSERT INTO saison (ssn_id, ssn_ordre, ssn_nom, ssn_default)
	VALUES (1, 1, 'Jour', True);
	
INSERT INTO saison (ssn_id, ssn_ordre, ssn_nom, ssn_default)
	VALUES (2, 2, 'Nuit', False);

-- La grande A'Tuin !
INSERT INTO monde (mnd_id, mnd_nom, mnd_tmn_id, mnd_description, mnd_pere_mnd_id, mnd_niveau, mnd_mld_id, mnd_image)
	VALUES (1, 'Le disque monde', 2, 'Le Disque-Monde est un monde rond et plat qui repose sur le dos de quatre éléphants géants, eux-mâmes placés sur le dos d''une tortue interstellaire, A''tuin, qui est sur le dos de... pas grand chose. A''tuin parcourt l''univers à la recherche de... elle seule le sait. Car on ignore tout d''elle. Nul ne sait si cette tortue est mâle ou femelle. Les savants du Disque-Monde ont établi une hypothèse qui veut que cette tortue soit en route pour se reproduire; si A''tuin est mâle, pas de problème... si elle est femelle, on est mal ! Cette théorie porte le nom de ""Big Bang"" ou de ""Grande Secousse"".', NULL, NULL, NULL, 'maps/demo_map.jpg');

-- Vents
INSERT INTO type_vent (tvt_id, tvt_nom) values (1, 'Vent');
INSERT INTO type_vent (tvt_id, tvt_nom) values (2, 'Courant marin');

-- Environnements
INSERT INTO type_calque (tcq_id, tcq_nom, tcq_principal) values (1, 'Environnement', TRUE);

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (1, 0, 'Estran', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (2, 1, 'Glacier', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (3, 2, 'Roche', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (4, 3, 'Désert', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (5, 4, 'Toundra', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (6, 5, 'Steppe', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (7, 6, 'Marais', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (8, 7, 'Prairie', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (9, 8, 'Forêt', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (10, 9, 'Champs', '', 1, 1, 0, 0, 0, 0, '', 1);


-- Climats
INSERT INTO type_calque (tcq_id, tcq_nom) values (2, 'Climat');

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (11, 1, 'Arctique', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (12, 2, 'Sibérien', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (13, 3, 'Tempéré', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (14, 4, 'Méditerranéen', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (15, 5, 'Désertique chaud', '', 1, 1, 0, 0, 0, 0, '', 2);

-- Relief
INSERT INTO type_calque (tcq_id, tcq_nom) values (3, 'Relief');

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (16, 1, 'Mer', '', 1, 1, 0, 1000, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (17, 2, 'Plaine', '', 1, 1, 0, 4, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (18, 3, 'Colline', '', 1, 1, 0, 5, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (19, 4, 'Moyenne montagne', '', 1, 1, 0, 8, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (20, 5, 'Montagne', '', 1, 1, 0, 10, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (21, 6, 'Haute montagne', '', 1, 1, 0, 15, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (22, 7, 'Très haute montagne', '', 1, 1, 0, 20, 0, 0, '', 3, 1);

-- Viaires
INSERT INTO type_calque (tcq_id, tcq_nom, tcq_principal) values (4, 'Voirie', True);

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (23, 1, 'Route', '', 1, 1, 0, -3, 0, 0, '', 4);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (24, 2, 'Rivière', '', 1, 1, 0, 1000, 0, 0, '', 4);

-- Types de lieu

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (1, 'Commun');
	
INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (2, 'Taverne');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (3, 'Banque');
	
INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (4, 'Forge');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (5, 'Temple');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (6, 'Armurerie');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (7, 'Échoppe');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (8, 'Poste de pilotage');

-- Type d'environnement

INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (1, 'Urbain', 36);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (2, 'Chaud', 37);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (3, 'Froid', 38);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (4, 'Tempéré', 39);
