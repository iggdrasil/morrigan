
-- Generated SQL Insert statements
-- --------------------------------------------------------------------


-- inserts for categorie
insert into categorie values ( 1, 'Univers', false ) ;
insert into categorie values ( 2, 'Joueur', false ) ;
insert into categorie values ( 3, 'Combat', false ) ;

-- inserts for parametre
insert into parametre values ( 1, 1, 'Saison', '1', 'Id de la saison en cours' ) ;
insert into parametre values ( 2, 2, 'Nb classes/perso', '2', 'Nombre de classes par personnage' ) ;
insert into parametre values ( 3, 2, 'Nb aptitudes/perso', '5', 'Nombre d''aptitudes par personnage' ) ;
insert into parametre values ( 4, 2, 'Remise de PA', 'int(%(c5)d/2)+5', 'Nombre de points d''action a la remise' ) ;
insert into parametre values ( 5, 2, 'Remise de PV', 'int(%(c3)d/4)+1', 'Nombre de points de vie a la remise' ) ;
insert into parametre values ( 6, 2, 'Remise de PN', '%(remise_pn)d', 'Nombre de points de nourriture a la remise' ) ;
insert into parametre values ( 7, 2, 'Remise de PP', 'int((%(c1)d+%(c2)d)/4)', 'Nombre de points de paralysie a la remise' ) ;

-- inserts for type_personnage
insert into type_personnage values ( 1, 'PJ' ) ;
insert into type_personnage values ( 2, 'PNJ' ) ;
insert into type_personnage values ( 3, 'Familier' ) ;
insert into type_personnage values ( 4, 'Gob' ) ;
insert into type_personnage values ( 5, 'Monture' ) ;

-- inserts for type_caracteristique
insert into type_caracteristique values ( 1, 'Attribut' ) ;
insert into type_caracteristique values ( 2, 'Competence' ) ;
insert into type_caracteristique values ( 3, 'Langue' ) ;

-- inserts for type_maladie
insert into type_maladie values ( 1, 'Classe' ) ;
insert into type_maladie values ( 2, 'Race' ) ;
insert into type_maladie values ( 3, 'Definitif' ) ;
insert into type_maladie values ( 4, 'Temporaire' ) ;
insert into type_maladie values ( 5, 'Instantane' ) ;
insert into type_maladie values ( 6, 'Lieu' ) ;

-- inserts for type_chemin
insert into type_chemin values ( 1, 'Classique' ) ;
insert into type_chemin values ( 2, 'Acces trappe' ) ;
insert into type_chemin values ( 3, 'Acces sas' ) ;
insert into type_chemin values ( 4, 'Acces pont' ) ;

-- inserts for type_secteur
insert into type_secteur values ( 1, 'Immobile' ) ;
insert into type_secteur values ( 2, 'Mobile (planete)' ) ;
insert into type_secteur values ( 3, 'Mobile (galaxie)' ) ;
insert into type_secteur values ( 4, 'Vaisseau (terre)' ) ;
insert into type_secteur values ( 5, 'Vaisseau (tout-terrain)' ) ;
insert into type_secteur values ( 6, 'Vaisseau spatial' ) ;

-- inserts for type_monde
insert into type_monde values ( 1, 'Galaxie' ) ;
insert into type_monde values ( 2, 'Planete' ) ;

-- inserts for type_vaisseau
insert into type_vaisseau values ( 1, 'Terrestre' ) ;
insert into type_vaisseau values ( 2, 'Aeroglisseur' ) ;
insert into type_vaisseau values ( 3, 'Sous-marin' ) ;
insert into type_vaisseau values ( 4, 'Marin' ) ;
insert into type_vaisseau values ( 5, 'Spatial' ) ;

-- inserts for type_element
insert into type_element values ( 1, 'Objet' ) ;
insert into type_element values ( 2, 'Arme' ) ;
insert into type_element values ( 3, 'Armure' ) ;
insert into type_element values ( 4, 'Sort' ) ;
insert into type_element values ( 5, 'Composite' ) ;

-- inserts for type_classe
insert into type_classe values ( 1, 'Classe' ) ;
insert into type_classe values ( 2, 'Aptitude' ) ;

-- inserts for taille
insert into taille values ( 1, 'Microscopique', 'Indiscernable � l''oeil nu.', 0 ) ;
insert into taille values ( 2, 'Minuscule', 'Tout piti-piti.', 1 ) ;
insert into taille values ( 3, 'Tr�s petite', 'On commence � peine � le remarquer.', 2 ) ;
insert into taille values ( 4, 'Petite', 'On le remarque', 4 ) ;
insert into taille values ( 5, 'Plut�t petite', 'Taille d''un chat', 8 ) ;
insert into taille values ( 6, 'Plut�t moyenne', 'Taille d''un grand chien', 16 ) ;
insert into taille values ( 7, 'Moyenne', 'Taille d''un humano�de', 32 ) ;
insert into taille values ( 8, 'Plut�t grande', 'Taille d''un g�ant.', 64 ) ;
insert into taille values ( 9, 'Grande', 'Taille d''une maison', 128 ) ;
insert into taille values ( 10, 'Imposante', 'Aussi imposant qu''une cath�drale, un vaisseau', 256 ) ;
insert into taille values ( 11, 'Tr�s grande', 'Taille d''un quartier', 512 ) ;
insert into taille values ( 12, 'Immense', 'Taille d''une petite ville', 1024 ) ;
insert into taille values ( 13, 'Colossale', 'Taille d''une grande ville', 2048 ) ;
insert into taille values ( 14, 'Infinie', 'Inconcevable.', 10000 ) ;

-- inserts for type_filature
insert into type_filature values ( 1, 'Filature' ) ;
insert into type_filature values ( 2, 'Pistage' ) ;
insert into type_filature values ( 3, 'Suite' ) ;

-- inserts for type_mime
insert into type_mime values ( 1, 'Simple' ) ;
insert into type_mime values ( 2, 'Personnage' ) ;

