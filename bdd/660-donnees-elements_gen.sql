
-- Clé de la remise
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (1, 'Clé de la remise', 'La clé de la remise de la taverne de New Seattle. Une pauvre clé toute pourrie.', 1, 5, false, null, null, null, 4, 4);

-- Chevalière de Syl
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (2, 'Chevaliere en bronze', 'Une jolie chevaliere en bronze dont la forme n''est pas sans rappeler l''empereur de New Seattle.', 1, 100, true, null, null, null, 2, 2);

-- Sac
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (3, 'Sacoche en peau de bete', 'Capable de contenir une quantité impressionnante de nourriture (un repas frugal, tout au plus), ce sac est parfaitement multifonction.', 1, 10, false, null, null, null, 3, 8);


-- Sabre (joli, hein, quand même)
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (4, 'Sabre', 'Finenement ouvragé, ce sabre est autant un sabre de parade que de combat. N''empêche qu''il vous tranchera malgré tout en deux si vous n''êtes pas initié à ce genre d''arme. Argh.', 2, 120, true, null, null, null, 4, 18);

-- Epée ancestrale
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (5, 'Epée ancestrale', 'Une belle, une magnifique épée lourdement forgée dans un lieu tenu secret. Irrésistible.', 2, 1000, true, null, null, null, 4, 20);

-- Mitraillette lance-grenade
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (6, 'Mitraillette lance-grenade', 'Attention au grabuge. Voici le dernier cri des armes de guerre personnelles.', 5, 200, true, null, null, 1, 4, 20);


-- Chargeur 20 balles
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (7, 'Chargeur 20 balles classiques', 'Un simple chargeur de 20 balles.', 1, 3, false, null, null, null, 4, 2);

-- Chargeur 2 grenades explosives
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (8, 'Chargeur 2 grenades explosives', 'Un chargeur de 2 grenades a utiliser dans un lance-grenade.', 1, 10, false, null, null, null, 4, 3);


-- Arc court
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (9, 'Arc court', 'Un bon petit arc en bois.', 2, 30, true, null, null, null, null, 16);

-- Carquois
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (10, 'Carquois de 30 flèches', 'Un carquois de 30 flèches.', 1, 15, false, null, null, null, 5, 5);

-- Armure de cuir
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (11, 'Armure de cuir', 'L''armure de cuir est une compromis entre solidité et légèreté.', 3, 120, true, null, null, null, 6, 24);

-- Ses petites menottes
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (12, 'Mains nues', 'Arme qui ne vous ferra jamais défaut. À moins que...', 2, 0, false, null, null, null, null, null);

-- Un coffre
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille) 
    VALUES (13, 'Coffre', 'Un chouette truc où l''on peut ranger des choses dedans', 1, 100, false, null, null, null, null, 28);

-- Une armoire
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille) 
    VALUES (14, 'Armoire', 'Un chouette truc où l''on peut ranger encore plus de choses que dans un coffre mais encore moins transportable', 1, 300, false, null, null, null, null, 66);

