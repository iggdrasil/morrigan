--Style classique
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (1, 'Classique', true, 'http://www.peacefrogs.net/morrigan_extra/css/style.css');

-- Style verdâtre Odyssée
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (2, 'Odyssée', false, 'http://www.peacefrogs.net/morrigan_extra/css/odyssee.css');

-- Style épuré
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (3, 'Clean', false, 'http://www.peacefrogs.net/morrigan_extra/css/clean.css');

-- Autre style assez simple
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (4, 'Clean-2 - Die IE, die!', false, 'http://www.peacefrogs.net/morrigan_extra/css/clean-2.css');
