
-- Une clé de la remise appartient à Syl ...
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (1, 1, null, null, null, null, null, null, null, 5, null);

-- Une chevaliere appartient également à Syl ...
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (2, 2, null, null, null, true, null, null, null, 5, null);

-- et un sac aussi !
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (3, 3, null, null, null, null, null, null, null, 5, null);

-- Arkenlond a aussi une chevaliere.
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (4, 2, null, 'Autour de ce bijou en cuivre, c''est un cerclage en or que l''on discerne là ?', 150, false, null, null, null, 4, null);

-- Une clé est sur le sol de la taverne !
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (5, 1, null, null, null, null, null, null, null, null, 2);


-- Sabre ELEMENT
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES(6, 4, 'Sabre de Yachus', null, 100, true, null, null, false, 6, null);

-- Epée ancestrale ELEMENT (ds la remise)
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (7, 5, 'Epee ancestrale des O''Coolin', null, null, null, null, null, null, null, 3);

-- Mitraillette lance-grenade ELEMENT
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (8, 6, 'Mitraillette lance-grenade', null, null, null, null, null, null, 3, null);


-- Chargeur de 20 balles (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (9, 'Chargeur 20 balles', 7, null, null, null, null, null, null, null, 3);

-- Chargeur de 2 grenades (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (10, 'Chargeur 2 grenades', 8, null, null, null, null, null, null, null, 3);

-- Arc ELEMENT (ds la remise)
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES(11, 9, 'Arc court en bois', null, null, true, null, null, false, null, 3);

-- Carquois de 30 flèches (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (12, 'Carquois de 30 flèches', 10, null, null, null, null, null, null, null, 3);


-- Armure de cuir
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (13, 'Armure de cuir', 11, null, null, 'true', null, null, null, null, 3); 

-- Mimines pour tous !
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (14, 12, 'Main nues', null, 0, false, true, null, true, 1, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (15, 12, 'Main nues', null, 0, false, true, null, true, 2, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (16, 12, 'Main nues', null, 0, false, true, null, true, 3, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (17, 12, 'Main nues', null, 0, false, true, null, true, 4, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (18, 12, 'Main nues', null, 0, false, true, null, true, 5, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (19, 12, 'Main nues', null, 0, false, true, null, true, 6, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (20, 12, 'Main nues', null, 0, false, true, null, true, 7, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (21, 12, 'Main nues', null, 0, false, true, null, true, 8, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (22, 12, 'Main nues', null, 0, false, true, null, true, 9, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (23, 12, 'Main nues', null, 0, false, true, null, true, 10, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (24, 12, 'Main nues', null, 0, false, true, null, true, 20, null);

-- on meuble un peu la remise
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
    VALUES (25, 13, 'Coffre de la remise', null, null, null, null, null, null, null, 3);
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
    VALUES (26, 14, 'Armoire de la remise', null, null, null, null, null, null, null, 3);
