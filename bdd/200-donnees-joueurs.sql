
-- Siltaom (mdp: 'siltaom')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (1, 'Siltaom', '71fa146a8f9f1531186dd2e5a9d431c2', 'armel@peacefrogs.net', 2, +2);

-- Nimnae (mdp: 'nimnae')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (2, 'Nimnae', '7c030583a5cbe1766b38ca5374cbe07c', 'etienne@peacefrogs.net', 2, +2);

-- Arkenlond (mdp: 'arkenlond')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (3, 'Arkenlond', '721649b8ae5c2f712989ab20cbc96230', 'giddzit@yahoo.fr', 2, +2);

-- Syl (mdp: 'syl')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (4, 'Syl', '0c7e0e3afed9b89ff0fd10b7a5bff0ce', 'syllelfesylvain@hotmail.com', 2, +2);

-- Florent (mdp: 'florent')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (5, 'Florent', 'b2ff8e48c14343ca3f51fce08f4d0d12', 'lioret.florent@wanadoo.fr', null, +2);

-- Stéphanie (mdp: 'stephanie')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (6, 'Stephanie', '59f29878f0ca6fffa2485e3c5e3b5443', 'stephy@schtroumpf.com', null, +2);

-- Nawa (mdp: 'nawa')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (7, 'Nawa', '1bf1fb5624fbf5ce51ebc776e7982040', 'bellefeegore@gmail.com', null, +2);

-- Zarkam (mdp: 'beep')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (8, 'Zarkam', '1284e53a168a5ad955485a7c83b10de0', 'yann.lejeune@gmail.com', null, +2);

-- Jipli (mdp: 'jipli')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (9, 'Jipli', 'b116275e9d57e77ec86f115ad003a867', 'jp.lapica@free.fr', null, +2);

-- Nath (mdp: 'nath')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (10, 'Nath', '320c8d45fe4ee818e1d185954c2c251d', 'erinelle@free.fr', null, +2);

-- Dummy (mdp : 'nopass')
insert into joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    values (20, 'Dummy', '0945fc9611f55fd0e183fb8b044f1afe', 'etienne@peacefrogs.net', 1, +2);

