
-- Actions pour 1 personne

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (1, 'Salut', '%(subject)s salue l''assemblée.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (2, 'Rire', '%(subject)s se met à rire.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (3, 'Au revoir', '%(subject)s fait au revoir.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (4, 'Applaudir', '%(subject)s applaudit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (5, 'Surprise', '%(subject)s semble surpris.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (6, 'Confus', '%(subject)s semble confus.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (7, 'Tousser', '%(subject)s tousse.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (8, 'Peur', '%(subject)s semble avoir peur.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (9, 'Pleurer', '%(subject)s pleure.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (10, 'Clin d''oeil', '%(subject)s fait un clin d''oeil.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (11, 'Rougir', '%(subject)s rougit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (12, 'Mécontentement', '%(subject)s marque son mécontentement.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (13, 'Joie', '%(subject)s semble heureux.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (14, 'Révérence', '%(subject)s fait une révérence.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (15, 'Trépigner', '%(subject)s trépigne.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (16, 'Sourciller', '%(subject)s sourcille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (17, 'Grimacer', '%(subject)s grimace.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (18, 'Acquiescement', '%(subject)s acquiesce.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (19, 'Desapprobation', '%(subject)s marque sa desapprobation.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (20, 'Se prosterner', '%(subject)s se prosterne.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (21, 'Rôter', '%(subject)s rôte.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (22, 'Pêter', '%(subject)s pête.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (23, 'S''agenouiller', '%(subject)s s''agenouille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (24, 'Timide', '%(subject)s semble timide.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (25, 'Soupirer', '%(subject)s soupire.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (26, 'Sourire', '%(subject)s sourit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (27, 'Cracher', '%(subject)s crache.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (28, 'Siffloter', '%(subject)s sifflote.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (29, 'Bailler', '%(subject)s baille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (30, 'Faim/Soif', '%(subject)s montre qu''il a faim et soif.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (31, 'Se reculer', '%(subject)s se recule.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (32, 'S''avance', '%(subject)s s''avance.', 1);


-- Actions pour 2

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (33, 'Accolade', '%(subject)s tente de faire une accolade à %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (34, 'Insulter', '%(subject)s insulte %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (35, 'Embrasser', '%(subject)s tente d''embrasser %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (36, 'Se moquer', '%(subject)s se moque de %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (37, 'Désigner', '%(subject)s désigne %(dest)s.', 2);
