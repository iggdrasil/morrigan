
-- Ne rien faire rend des PAs et des PNs / à ne surtout pas laisser dans un jeu :)
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (1, 'Do nothing', 'doing_nothing', 'Ne rien faire, c''est tellement bon...', '0', '0', '-40', '-40', '-40', '0,0', '0,0', true, 'Action simple destinée à la beta. Permet de récupérer des PNs et des PAs. Ne restera pas dans la version finale du jeu.', True);

-- Parler
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (2, 'Talk', 'talk', 'Papoter est bon pour votre santé mais peut-etre pas celle des autres.', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'L''action principale du jeu. Il est possible de s''adresser à une ou plusieurs personnes voire à l''ensemble des personnes présentes dans le lieu.', True);

-- Changer de forme (exemple du druide, du vampire)
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (3, 'Change shape', 'change_shape', 'Passer dans des endroits improbables, devenir plus fort, ou juste s''amuser à changer d''apparence.', '%(d10)d + %(c1)d + %(c12)d - %(d20)d', '0', '5', '10', '0', '2,12', '1,1;2,12', true, 'Action permettant de changer de forme. Cette action est bien sûr destinée simplement aux personnages métamorphes. Attention le changement ne peut se faire discretement.', False);

-- Changer de personnage pour gérer un second
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (4, 'Change character', 'change_character', 'Gérer un autre personnage, familier, etc.', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'Action permettant de se mettre dans la peau d''un autre personnage que l''on a à gérer (par exemple sa monture).', True);

-- Se déplacer
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (5, 'Move', 'move', 'Se déplacer: à pied, avec une monture, en véhicule, etc.', '%(d10)d + %(e0)d - %(d20)d - %(e1)d', '0', '%(e2)d', '%(e3)d', '0', '1,%(e4)d', '2,%(e4)d', true, 'Action de déplacement dans un autre lieu. Sur la carte ou dans un dans une même zone, les coûts sont variables', False);

-- Remise
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (6, 'Deliveries', 'remise', 'Remises journalieres', '0', '0', '-5', '-5', '-5', '0,0', '0,0', true, 'Tu es un gros-bill. Tu as le pouvoir des remises ! Est-ce normal ?', True);

-- Donner un objet
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (7, 'Give object', 'give_object', 'Donner un objet.', '0', '0', '0', '1', '0', '0,0', '0,0', true, 'Le don d''objet ne s''applique qu''aux objets non équipés et potentiellement donnable.', False);

-- Fouiller
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (8, 'Dig', 'dig', 'Fouiller le lieu.', '%(d10)d + 2*%(c4)d - %(d15)d', '0', '1', '0', '0', '0,0', '0,0', true, 'Rechercher un objet dans un lieu.', False);

-- Jeter un objet
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (9, 'Drop object', 'drop_object', 'Jeter un objet.', '0', '0', '0', '1', '0', '0,0', '0,0', true, 'Jeter un objet. Cette action ne s''applique qu''aux objets non équippés et potentiellement jetables.', False);

-- Equiper
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (10, 'Equip', 'equip', 'Equiper un objet', '0', '0', '0', '1', '4', '0,0', '0,0', true, 'Équiper et déséquiper les armes et armures que l''on possède.', True);

-- Mimer
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (11, 'Emote', 'emote', 'Mimer une action', 0, 0, '0', '0', '0', '0,0', '0,0', true, 'Mimer une action, seul ou en liaison avec un autre personnage.', True);

-- Lancer une rumeur
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (13, 'Buzz', 'buzz', 'Lancer une rumeur', '%(d10)d + %(c6)d + %(c22)d - 10 - %(d5)d - %(e1)d', '0', '10', '10', '0', '2,22', '1,6;2,22', true, 'Lancer une rumeur voire pourquoi pas dire des choses vraies.', False);

-- Ecouter une rumeur
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (14, 'Get the buzz', 'getBuzz', 'Aller aux nouvelles', '%(d10)d + %(c6)d + %(c22)d - 10 - %(d5)d', '0', '4', '0', '0', '2,22', '1,6;2,22', true, 'Aller aux nouvelles, se mettre au courant des éventuelles rumeurs disponibles.', False);

-- Suivre quelqu'un
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_cout_pp, act_form_cout_pv, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (15, 'Follow someone', 'follow', 'Suivre une personne', '%(d10)d + %(c26)d - 5 - %(d5)d - %(e1)d', '0', 2, 0, '0', null, null, '1,26', '2,26', true, 'Suivre une personne qui vient de partir', False);

-- Rechercher des traces de pas	
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_cout_pp, act_form_cout_pv,"act_form_gain_min","act_form_gain_max", "act_courante", "act_aide", act_combat) 
    values (16, 'Search steps', 'search_steps', 'Rechercher des traces de pas', '%(d10)d + %(c26)d + %(c5)d - 10 - %(d5)d - %(e1)d', '0', '10', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Montrer la route
insert into action (act_id, "act_nom", "act_nom_form", "act_desc", "act_form_attaque", "act_form_defense","act_form_cout_pa","act_form_cout_pn", act_form_cout_pc, "act_form_cout_pp","act_form_cout_pv","act_form_gain_min","act_form_gain_max", "act_courante", "act_aide", act_combat)
    values (17, 'Show the way', 'show_way', 'Montrer la route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, False);

-- Créer un lieu topologique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (18, 'Create a new location', 'create_location', 'Créer un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (18, 16, TRUE);

-- Créer un secteur
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (19, 'Create a new sector', 'create_sector', 'Créer un secteur', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer un secteur', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (19, 16, TRUE);

-- Créer une nouvelle route
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (20, 'Create a new road', 'create_road', 'Créer une route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer une route', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (20, 16, TRUE);

-- Créer une nouvelle description associée à un lieu
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (21, 'Create a new location description', 'create_loc_desc', 'Créer une description pour un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer une description pour un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (21, 16, TRUE);

-- Rechercher précisement dans un lieu
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (22, 'Look precisely', 'look_exactly', 'Regarder précisément un lieu', '%(d10)d + %(c26)d', '0', '2', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Modifier un lieu topologique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (23, 'Modify current location', 'modify_location', 'Modifier un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (23, 16, TRUE);

-- Modifier un secteur
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (24, 'Modify current sector', 'modify_sector', 'Modifier un secteur', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier un secteur', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (24, 16, TRUE);

-- Modifier une route
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (25, 'Modify currents roads', 'modify_road', 'Modifier une route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier une route', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (25, 16, TRUE);

-- Modifier une description précise
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (26, 'Modify description', 'modify_loc_desc', 'Modifier une description', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier une description', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (26, 16, TRUE);

-- Se téléporter (en tant qu'admin)
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (27, 'Teleport', 'admin_teleport', 'Se téléporter', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Se téléporter pour faciliter l''administration', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (27, 16, TRUE);

-- Chercher un passage
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (28, 'Search for an hidden path', 'search_path', 'Chercher un passage', '%(d10)d + %(c26)d + %(c5)d - 10 - %(d5)d', '0', '4', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Recharger une arme
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (29, 'Reload a weapon', 'reload', 'Recharger une arme', '0', '0', '0', '4', '4', '0', '0', '0,0', '0,0', True, NULL, True);

-- Décharger une arme
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (30, 'Unload a weapon', 'unload', 'Décharger une arme', '0', '0', '0', '4', '4', '0', '0', '0,0', '0,0', True, NULL, True);

-- S'engager dans un combat
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pp, act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (31, 'Take part in a fight', 'take_part', 'S''engager dans un combat', '%(d10)d+(not %(e1)d)*%(c2)d+(not not %(e1)d)*%(c1)d', '%(d10)d+%(c2)d', '4', '4', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Quitter un combat
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (32, 'Leave a fight', 'leave_fight', 'Se désengager d''un combat', '%(d6)d+(not %(e1)d)*%(c2)d+(not not %(e1)d)*%(c1)d', '(not %(e2)d)*%(c2)d+(not not %(e2)d)*(%(c1)d+%(d6)d)', '0', '4', '0', '0', '2', '0,0', '0,0', True, NULL, True);

-- Gagner l'initiative
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (33, 'Take the initiative', 'take_init', 'Prendre l''initiative', '10+%(e1)d*3+%(e2)d', '10+%(e3)d*3+%(e4)d', '0', '4', '0', '0', '4', '0,0', '0,0', True, NULL, True);

-- Attaquer quelqu'un
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (34, 'Attack someone', 'attack', 'Attaquer', '%(e1)d+%(e2)d+%(c17)d+%(e3)d', '(%(e4)d+%(e5)d+%(c18)d)+%(e6)d-%(e7)d', '0', '4', '0', '0', '4', '0,0', '0,0', True, NULL, True);

-- Ramasser un objet
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (35, 'Take an object', 'takeObject', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Mettre dans un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (36, 'Put into', 'putInto', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Prendre dans un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (37, 'Take into', 'takeInto', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Ouvrir/fermer un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (38, 'Open/close a chest', 'openCloseChest', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Ouvrir/fermer une boutique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (39, 'Open/close a shop', 'openCloseShop', 'Ouvrir/fermer un commerce', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Ouvrir/fermer une boutique', False);
-- Cette action n'est disponible que pour les commercants
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (39, 17, TRUE);
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (39, 18, TRUE);

-- Gérer une boutique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (40, 'Manage a shop', 'manageShop', 'Gérer une boutique', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, '', False);
-- Cette action n'est disponible que pour les commercants
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (40, 17, TRUE);
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (40, 18, TRUE);

-- Acheter
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (41, 'Buy', 'buy', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, '', False);

-- Prendre des notes personnelles dans son fichier d'action
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (42, 'Taking notes', 'taking_notes', 'Prendre des notes dans son fichier d''action', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'Action de prise de note dans le fichier d''action. Ces notes ne sont destinées qu''au joueur lui même et ne peuvent pas être partagées.', True);
