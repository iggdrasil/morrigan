
-- Le champ pcl_id est SERIAL et inutilisé sans crainte

-- Siltaom est un mage niv 2
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (1, 2,  null);

-- Nimnae est une pretresse niv 1 (avec un titre personnalisé !)
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (2, 4, 'Prêtresse de Bisounours');

-- Arkenlond est un guerrier niv 1
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (3, 1, null);

-- Syl est un acrobate niv 1
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (5, 5, null);

-- Sulfure est un guerrier niv 2
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (6, 1, null);

-- Des MJs lieu
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (1, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (2, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (3, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (8, 16, null);

-- Nimnae est une colporteuse niv 1 (avec un titre personnalisé !)
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (2, 17, 'Colporteuse à deux sous');

