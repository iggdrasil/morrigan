INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (1, 'Or', 'Metal précieux parmi les précieux, l''or est une ressource rare fort appréciée pour sa solidité et sa simple beauté', 8);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (2, 'Bronze', 'Alliage de cuivre et d''étain, le cuivre est reconnu pour sa résistance à l''usure et l''érosion', 6);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (3, 'Peau de bete', 'Mieux vaut faire parti des traqueurs que des traqués. Certains en font encore les frais, pour leur solide peau.', 3);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (4, 'Fer', 'Costaud mais pas trop, le fer est très répandu: il a fait ses preuves !', 5);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (5, 'Bois', 'Issu de ressources naturelles, le bois est souvent utilisé car à la fois costaud et flexible.', 4);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (6, 'Cuir', 'Le cuir est de la peau d''animal tannée.', 3);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (7, 'Organique', 'De la chair, du sang...', 2);
