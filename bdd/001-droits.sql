
-- Afin de gérer les droits d'accès aux données, des groupes et utilisateurs sont créés. Se logger sur la base de données morrigan, en tant qu'utilisateur morrigan (avec le bon mot de passe).

CREATE USER connexion WITH ENCRYPTED PASSWORD 'connexion';
CREATE USER supermj WITH ENCRYPTED PASSWORD 'supermj';
CREATE USER mjobjet WITH ENCRYPTED PASSWORD 'mjobjet';
CREATE USER mjlieu WITH ENCRYPTED PASSWORD 'mjlieu';
CREATE USER peuji WITH ENCRYPTED PASSWORD 'peuji';

CREATE GROUP general WITH USER peuji;
CREATE GROUP meuji WITH USER supermj, mjobjet, mjlieu;
