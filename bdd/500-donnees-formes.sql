-- Sexes
INSERT INTO sexe (sex_id, sex_nom)
	VALUES (1, 'Male');

INSERT INTO sexe (sex_id, sex_nom)
	VALUES (2, 'Féminin');

INSERT INTO sexe (sex_id, sex_nom)
	VALUES (3, 'Asexué');
	
-- Types de trace

insert into type_trace (ttr_id, ttr_nom) values (1, 'Pied nu');
insert into type_trace (ttr_id, ttr_nom) values (2, 'Botte');
insert into type_trace (ttr_id, ttr_nom) values (3, 'Chaussure à talon');
insert into type_trace (ttr_id, ttr_nom) values (4, 'Patte d''oiseau');
insert into type_trace (ttr_id, ttr_nom) values (5, 'Patte féline');

-- Siltaom est un elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (1, 1, 1, 1, false, 'Elfe fragile aux pommettes rosies par le soleil, ...', 'Siltaom fait actuellement de la flûte sur un dromadaire.', 'http://siltaom.free.fr/elfe_morrigan.jpg', 1, 3, 4, 36);

-- Siltaom est aussi un vampire !!
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (2, 1, 4, 1, true, 'Le vampire face à vous se tient bien droit. Le teint livide, il vous observe, guette chacun de vos mouvements.', 'Attablé, Siltaom déguste un cadavre fort attrayant', '', 1, 4, 5, 30);


-- Nimnae est une elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (3, 2, 1, 2, true, 'Face a vous se tient, une jeune humaine au teint lilial, de taille moyenne. Pour qui a deja rencontre un de ces etres albinos, il n''est guere surprenant de decouvrir les yeux ecarlates qui emergent de ce facies a la blancheur si parfaite. De longs et tenus cheveux aux pales nuances cuivrees soigneusement deposes sur les epaules encadrent ce visage a la peau depigmentee. Les traits de cette figure pourraient etre qualifies de fins et delicats s''il n''y avait ce nez a l''arete relativement saillante qui semble decouper en deux parties ce facies. Cette impression se confirme lorsque s''attardant dans le regard de cette humaine, on remarque que l''oeil gauche est plus sombre que l''oeil droit. Ce visage et de menues mains de nacre sont les seuls membres qui fendent le noir profond des vetements que cette jeune humaine porte. Elle est notamment vetue d''une grande cape dont la capuche n''est que tres rarement rabattue pour cacher ce visage ivoire.En effet ce ne sera que pour se proteger du soleil ou s''isoler que la jeune humaine disparaitra dans la penombre de sa tenue. Le seul luxe apparent sur la jeune dame est un pendentif retenue par une chainette en etain passee autour de son cou. Le pendentif a en son centre une petite aquamarine de couleur bleu foncé en forme de larme. Ce bijou n''a probablement pasgrande valeur marchande mais Nimnae semble y preter beaucoup d''importance.', 'A ses oreilles vous vous direz surement que cette humaine ...', 'http://www.peacefrogs.net/~etienne/bg/morr_nim.png', 2, 3, 3, 38);


-- Arkenlond est un orc
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (4, 3, 2, 1, false, 'L''orc avec les lunettes délicatement posées sur son gros pif...', 'Arkenlond fait du body painting.', 'http://www.peacefrogs.net/~etienne/bg/ark.jpg', 5, 6, 6, 30);

-- ... ou un elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (5, 3, 1, 1, false, 'Elfe filiforme plein de charme.', 'Arkenlond tapote un doigt songeur sur son menton.', '', 1, 4, 4, 36);


-- Aegon est un dragon
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (6, 4, 5, 1, true, 'Grand dragon noir, l''ampleur de ses ailes est telle qu''il lui suffit de survoler une ville pour la plonger dans l''ombre.', 'Balerion crache des flammes aussi noires que ses écailles.', 'http://www.peacefrogs.net/~etienne/bg/aegon.jpg', 4, 6, 6, 50);


-- Que peut bien être Syl ...
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (7, 5, 1, 1, true, 'Elfe aux oreilles coupées.', 'Syl pleure.', '', 1, 4, 4, 36);


-- Sulfure est un humain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (8, 6, 3, 1, true, 'Humain à la dégaine originale, Sulfure est un être de chair et de muscle et de muscle (vous re-regardez de nouveau le bonhomme est vous vous dites vraiment qu''il est fait de muscle au-dessus de muscle). Son sabre en bandoulière, il semble peu amical mais attire les regards sur lui. Sans doute que cela est dû à la présence d''aiguilles à tricoter sur son flanc.', 'Sulfure joue au bilboquet, son péché mignon.', '', 2, 4, 4, 29);

-- Ambre est une humaine
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (9, 7, 3, 2, true, 'Jeune femme rousse aux yeux verts.', 'Souriante.', '', 1, 3, 3, 26);

-- Cerencov est un humain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (10, 8, 3, 1, true, 'Viel homme.', '', '', 1, 4, 4, 30);

-- Cohen est un nain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (11, 9, 6, 1, true, 'Ce nain bourru (digne ambassadeur de sa race) compense sa taille par un tempérament fort', 'Cohen est un tantinet éméché', NULL, 2, 5, 3, 22);

-- Miss Sable est une humaine
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (12, 10, 3, 2, true, 'Femme petite et mince.', 'Souriante, elle se promène d''un pas léger.', '', 2, 4, 4, 26);

-- Même si il a l'air d'un fantôme Dummy est un humain

INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
 VALUES (20, 20, 3, 1, true, 'Vous voyez un etre indistinct et sans formes... Un fantome peut-etre',
   'Regard hagard et pas hesitant', '', 1, 4, 4, 30);

