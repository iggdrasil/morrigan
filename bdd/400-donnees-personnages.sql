-- Siltaom
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES ( 1, 1, true, null, 1, 'Siltaom le ridicule', 'Siltaom le gros-bill', 'Il était une fois un orc....', 'Arrivée: Baie; escalade du Mont-Granite sans les mains, ...', now(), now(), 30, 35, 8, 20, 9, 10, 50, 50, 15, 15, 1, 1, now());

-- Nimnae
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (2, 2, true, null, 1, 'Nimnae', 'Nimnae sombre', 'Dans une grotte naquit la petite...', 'Arrivée: pouf; Missionnaire dans les îles...', now(), now(), 26, 32, 10, 20, 8, 10, 45, 45, 20, 20, 1, 1, now());

-- Arkenlond
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (3, 3, true, null, 1, 'Arkenlond le belliqueux', 'Arkenlond le vétéran', 'Un jour, un âtre vert en armure tendit une hache...', 'Débarquement sur la plage d''Amoho; Fou de bibliothèque; Fou;...', now(), now(), 23, 25, 5, 20, 10, 10, 80, 80, 25, 25, 1, 1, now());

-- Aegon Balerion
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (4, 2, false, 2, 1, 'Aegon Balerion', 'Aegon Balerion', 'Elevé par Nimnae qui avait recu ces oeufs de sa famille ...', 'Naissance sous un pont.', now(), now(), 111, 250, 200, 200, 500, 500, 225, 300, 30, 30, 3, 1, now());

-- Syl
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (5, 4, true, null, 1, 'Mr S','Syl', 'Syl l''elfe est apparu spontanément ...', 'Arrivée: En cours.', now(), now(), 20, 20, 20, 20, 10, 10, 50, 50, 10, 10, 1, 1, now());

-- Sulfure
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (6, 5, true, null, 1, 'Sulfure','Sulfure', 'D''un volcan est surgi un homme...', 'Aucune idée. Il a fait pouf.', now(), now(), 20, 25, 15, 18, 10, 10, 80, 80, 15, 15, 1, 1, now());

-- Ambre
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (7, 7, True, null, 1, 'Ambre', 'Ambre', 'Fille d''une larme de mer et de poussière de terre.', 'Elle quitta bien vite ses parents pour un ailleurs.', now(), now(), 22, 28, 20, 20, 8, 10, 40, 40, 10, 10, 1, 1, NOW());

-- Cerencov
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (8, 8, True, null, 1, 'Cerencov', 'Cerencov', 'Enfant prodige aux milles talents. Depuis il se fait bien plus discret.', 'Il tue ses parents avec ses dents avant de s''en aller en courant.', now(), now(), 22, 28, 20, 20, 8, 10, 40, 40, 10, 10, 1, 1, NOW());

-- Cohen
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (9, 9, True, null, 1, 'Cohen', 'Cohen', 'Dit le barbare, élévé par des orcs, il s''imposa bien vite comme le plus fort d''entre eux...', 'Trop long a conter...', now(), now(), 30, 30, 25, 25, 2, 2, 100, 100, 20, 20, 1, 1, NOW());

-- Miss Sable
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (10, 10, True, null, 1, 'Miss Sable', 'Miss Sable', 'Il y a bien longtemps dans une lointaine galaxie...', 'Erre de par le monde pour trouver l''illumination.', now(), now(), 22, 28, 18, 18, 8, 10, 40, 40, 8, 8, 1, 1, NOW());

-- Dummy
insert into personnage
(prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
values (20, 20, True, NULL, 1, 'Dummy','Dummy', '', '', NOW(), NOW(), 25, 25, 20, 20, 9, 10, 40, 40, 10, 10, 1, 1, NOW());

