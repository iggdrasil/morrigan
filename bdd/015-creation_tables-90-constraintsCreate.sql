
-- Generated SQL Constraints
-- --------------------------------------------------------------------

create unique index idxu_prs_nom_originel on personnage  (prs_nom_originel) ;
create unique index idxu_prs_nom_actuel on personnage  (prs_nom_actuel) ;
create unique index idxu_jou_pseudo on joueur  (jou_pseudo) ;
create unique index idxu_frm_prs_rac on forme  (frm_prs_id,frm_rac_id) ;
create unique index idxu_cln_titre on classe_niveau  (cln_titre) ;
create unique index idx_cln_clsniv on classe_niveau  (cln_cls_id,cln_niveau) ;
create unique index idxu_cls_nom on classe  (cls_nom) ;
create unique index idxu_pml_prsmld on perso_maladie  (pml_prs_id,pml_mld_id) ;
create unique index idxu_mld_nom on maladie  (mld_nom) ;
create unique index idxu_crc_nom on caracteristique  (crc_nom) ;
create unique index idxu_pcr_prscrc on perso_carac  (pcr_prs_id,pcr_crc_id) ;
create unique index idxu_ctg_libelle on categorie  (ctg_libelle) ;
create unique index idxu_pci_prscls on perso_classe_int  (pci_prs_id,pci_cls_id) ;
create unique index idxu_pqc_clncls on prerequis_classe  (pqc_cln_id,pqc_cls_id) ;
create unique index idxu_mnd_nom on monde  (mnd_nom) ;
create unique index idxu_lie_nom on lieu  (lie_nom) ;
create unique index idxu_sct_nom on secteur  (sct_nom) ;
create unique index idxu_ssn_nom on saison  (ssn_nom) ;
create unique index idxu_mnn_nom on monnaie  (mnn_nom) ;
create unique index idxu_pmn_prsmnn on perso_monnaie  (pmn_prs_id,pmn_mnn_id) ;
create unique index idx_cmp_nom on compagnie  (cmp_nom) ;
create unique index idxu_plcprs on place_compagnie  (plc_prs_id) ;
create unique index idxu_plc_cmpprs on place_compagnie  (plc_cmp_id,plc_prs_id) ;
create unique index idxu_nrc_nom on nature_caracteristique  (ncr_nom) ;
create unique index idxu_act_nom on action  (act_nom) ;
create unique index idxu_egn_nom on element_gen  (egn_nom) ;
create unique index idxu_arm_agnelm on arme  (arm_agn_id,arm_elm_id) ;
create unique index idxu_cgn_egn on recharge_gen  (cgn_egn_id) ;
create unique index idxu_vgn_egn on vaisseau_gen  (vgn_egn_id) ;
create unique index idxu_agd_ugntdg on armure_gen_degat  (agd_ugn_id,agd_tdg_id) ;
create unique index idxu_agz_ugnzon on armure_gen_zone  (agz_ugn_id,agz_zon_id) ;
create unique index idxu_vgp_vgntpt on vaisseau_gen_poste  (vgp_vgn_id,vgp_tpt_id) ;
create unique index idxu_aru_elm on armure  (aru_elm_id) ;
create unique index idxu_rch_elm on recharge  (rch_elm_id) ;
create unique index idxu_vss_sct on vaisseau  (vss_sct_id) ;
create unique index idxu_vss_elm on vaisseau  (vss_elm_id) ;
create unique index idxu_inc_clsint on interdit_classe  (inc_cls_id,inc_interdit_cls_id) ;
create unique index idxu_pqa_cln on prerequis_carac  (pqa_cln_id) ;
create unique index idxu_rac_nom on race  (rac_nom) ;
create unique index idxu_acc_chmprs on acces_connu  (acc_chm_id,acc_prs_id) ;
create unique index idxu_mnc_mndprs on monde_connu  (mnc_mnd_id,mnc_prs_id) ;
create unique index idxu_lng_nom on langue  (lng_nom) ;
create index idx_lic_lie_id on lieu_carto  (lic_lie_id) ;
create index idx_clc_clq_id on calque_lieu_carto  (clc_clq_id) ;
create index idx_clc_lic_id on calque_lieu_carto  (clc_lic_id) ;
create unique index idxu_cff_elm on coffre  (cff_elm_id) ;
create unique index idxu_cel_cffelm on coffre_element  (cel_cff_id,cel_elm_id) ;
create unique index idxu_elv_prselm on element_visible  (elv_prs_id,elv_elm_id) ;
create unique index idxu_btq_cff on boutique  (btq_cff_id) ;
create unique index idxu_egc_egnprs on element_gen_connu  (egc_egn_id,egc_prs_id) ;
create unique index idxu_acr_atmcrc on automate_carac  (acr_atm_id,acr_crc_id) ;
create unique index idxu_aeg_egnbtq on achat_element_gen  (aeg_egn_id,aeg_btq_id) ;
create unique index idxu_pcl_procls on profil_classe  (pcl_pro_id,pcl_cls_id) ;
create unique index idxu_pra_prorac on profil_race  (pra_pro_id,pra_rac_id) ;
create unique index idxu_psx_prosex on profil_sexe  (psx_pro_id,psx_sex_id) ;
alter table personnage add constraint fk_pj_joueur
  foreign key (prs_jou_id)
  references joueur (jou_id)  ;
alter table mj add constraint fk_mj_joueur
  foreign key (mji_jou_id)
  references joueur (jou_id)  ;
alter table perso_classe add constraint fk_ppc_perso
  foreign key (pce_prs_id)
  references personnage (prs_id)  ;
alter table forme add constraint fk_rf_race
  foreign key (frm_rac_id)
  references race (rac_id)  ;
alter table classe_niveau add constraint fk_ccn_classe
  foreign key (cln_cls_id)
  references classe (cls_id)  ;
alter table perso_maladie add constraint fk_ppm_perso
  foreign key (pml_prs_id)
  references personnage (prs_id)  ;
alter table classe_niveau add constraint fk_mcn_maladie
  foreign key (cln_mld_id)
  references maladie (mld_id)  ;
alter table race add constraint fk_mr_maladie
  foreign key (rac_mld_id)
  references maladie (mld_id)  ;
alter table perso_maladie add constraint fk_mpm_maladie
  foreign key (pml_mld_id)
  references maladie (mld_id)  ;
alter table perso_carac add constraint fk_cpc_carac
  foreign key (pcr_crc_id)
  references caracteristique (crc_id) on update cascade on delete cascade ;
alter table perso_carac add constraint fk_ppc_carac
  foreign key (pcr_prs_id)
  references personnage (prs_id) on update cascade on delete cascade ;
alter table parametre add constraint fk_cp_categorie
  foreign key (prm_ctg_id)
  references categorie (ctg_id)  ;
alter table personnage add constraint fk_prs_possesseur
  foreign key (prs_possesseur_prs_id)
  references personnage (prs_id)  ;
alter table perso_classe_int add constraint fk_pci_perso
  foreign key (pci_prs_id)
  references personnage (prs_id)  ;
alter table perso_classe_int add constraint fk_pci_cls
  foreign key (pci_cls_id)
  references classe (cls_id)  ;
alter table classe add constraint fk_tcl_typcls
  foreign key (cls_tcl_id)
  references type_classe (tcl_id)  ;
alter table monde add constraint fk_mnd_pere
  foreign key (mnd_pere_mnd_id)
  references monde (mnd_id)  ;
alter table lieu add constraint fk_scl_secteur
  foreign key (lie_sct_id)
  references secteur (sct_id)  ;
alter table chemin add constraint fk_lch_depart
  foreign key (chm_arrivee_lie_id)
  references lieu (lie_id)  ;
alter table chemin add constraint fk_lch_arrivee
  foreign key (chm_depart_lie_id)
  references lieu (lie_id)  ;
alter table chemin add constraint fk_ec_cle
  foreign key (chm_egn_id)
  references element_gen (egn_id)  ;
alter table personnage add constraint fk_pl_depart
  foreign key (prs_lie_id)
  references lieu (lie_id)  ;
alter table perso_classe add constraint fk_pcn_clniv
  foreign key (pce_cln_id)
  references classe_niveau (cln_id)  ;
alter table forme add constraint fk_pf_perso
  foreign key (frm_prs_id)
  references personnage (prs_id)  ;
alter table perso_monnaie add constraint fk_pm_perso
  foreign key (pmn_prs_id)
  references personnage (prs_id)  ;
alter table perso_monnaie add constraint fk_pm_monnaie
  foreign key (pmn_mnn_id)
  references monnaie (mnn_id)  ;
alter table place_compagnie add constraint fk_pcp_perso
  foreign key (plc_prs_id)
  references personnage (prs_id)  ;
alter table place_compagnie add constraint fk_pcp_cmp
  foreign key (plc_cmp_id)
  references compagnie (cmp_id)  ;
alter table maladie add constraint fk_mtm_typmal
  foreign key (mld_tml_id)
  references type_maladie (tml_id)  ;
alter table secteur add constraint fk_sts_typsct
  foreign key (sct_tsc_id)
  references type_secteur (tsc_id)  ;
alter table chemin add constraint fk_ctc_typchm
  foreign key (chm_tch_id)
  references type_chemin (tch_id)  ;
alter table personnage add constraint fk_ptp_typprs
  foreign key (prs_tpr_id)
  references type_personnage (tpr_id)  ;
alter table caracteristique add constraint fk_ctc_typcrc
  foreign key (crc_tcr_id)
  references type_caracteristique (tcr_id)  ;
alter table lieu_action add constraint fk_lac_action
  foreign key (lac_act_id)
  references action (act_id)  ;
alter table typelieu_action add constraint fk_tla_action
  foreign key (tla_act_id)
  references action (act_id)  ;
alter table lieu_action add constraint fk_lac_lieu
  foreign key (lac_lie_id)
  references lieu (lie_id)  ;
alter table lieu add constraint fk_tli_typelieu
  foreign key (lie_tli_id)
  references type_lieu (tli_id)  ;
alter table typelieu_action add constraint fk_tla_typelieu
  foreign key (tla_tli_id)
  references type_lieu (tli_id)  ;
alter table debit_element add constraint fk_dli_dbtlie
  foreign key (dbt_lie_id)
  references lieu (lie_id)  ;
alter table vente_element add constraint fk_vli_vntlie
  foreign key (vnt_lie_id)
  references lieu (lie_id)  ;
alter table depot_element add constraint fk_dlie_dptlie
  foreign key (dpt_lie_id)
  references lieu (lie_id)  ;
alter table banque add constraint fk_bli_bnqlie
  foreign key (bnq_lie_id)
  references lieu (lie_id)  ;
alter table caracteristique add constraint fk_ccc_catcrc
  foreign key (crc_ncr_id)
  references nature_caracteristique (ncr_id)  ;
alter table element add constraint fk_ege_generique
  foreign key (elm_egn_id)
  references element_gen (egn_id)  ;
alter table arme add constraint fk_ar_recharge
  foreign key (arm_rch_id)
  references recharge (rch_id)  ;
alter table arme add constraint fk_aga_generique
  foreign key (arm_agn_id)
  references arme_gen (agn_id)  ;
alter table arme_gen add constraint fk_egag_arme
  foreign key (agn_egn_id)
  references element_gen (egn_id)  ;
alter table element_gen add constraint fk_egm_matiere
  foreign key (egn_mtr_id)
  references matiere (mtr_id)  ;
alter table element_gen add constraint fk_egt_techno
  foreign key (egn_tcn_id)
  references technologie (tcn_id)  ;
alter table armure_gen add constraint fk_egug_armure
  foreign key (ugn_egn_id)
  references element_gen (egn_id)  ;
alter table recharge_gen add constraint fk_egcg_recharge
  foreign key (cgn_egn_id)
  references element_gen (egn_id)  ;
alter table vaisseau_gen add constraint fk_egvg_vaisseau
  foreign key (vgn_egn_id)
  references element_gen (egn_id)  ;
alter table vaisseau_gen add constraint fk_tvg_typevaisseau
  foreign key (vgn_tvs_id)
  references type_vaisseau (tvs_id)  ;
alter table arme_gen add constraint fk_agm_maladie
  foreign key (agn_mld_id)
  references maladie (mld_id)  ;
alter table arme_gen add constraint fk_agtmt_type_munition
  foreign key (agn_tmt_id)
  references type_munition (tmt_id)  ;
alter table recharge_gen add constraint fk_cgmn_munition
  foreign key (cgn_mnt_id)
  references munition (mnt_id)  ;
alter table munition add constraint fk_mnm_maladie
  foreign key (mnt_mld_id)
  references maladie (mld_id)  ;
alter table arme_gen add constraint fk_agtd_degat
  foreign key (agn_tdg_id)
  references type_degat (tdg_id)  ;
alter table armure_gen_zone add constraint fk_agz_zone
  foreign key (agz_zon_id)
  references zone_armure (zon_id)  ;
alter table armure_gen_zone add constraint fk_agz_zones
  foreign key (agz_ugn_id)
  references armure_gen (ugn_id)  ;
alter table armure_gen_degat add constraint fk_agd_degat
  foreign key (agd_ugn_id)
  references armure_gen (ugn_id)  ;
alter table armure_gen_degat add constraint fk_agd_degats
  foreign key (agd_tdg_id)
  references type_degat (tdg_id)  ;
alter table vaisseau_gen_poste add constraint fk_vgtp_poste
  foreign key (vgp_vgn_id)
  references vaisseau_gen (vgn_id)  ;
alter table vaisseau_gen_poste add constraint fk_tvp_typeposte
  foreign key (vgp_tpt_id)
  references type_poste_tmp (tpt_id)  ;
alter table element_gen add constraint fk_egt_type
  foreign key (egn_tel_id)
  references type_element (tel_id)  ;
alter table armure add constraint fk_aue_armure
  foreign key (aru_elm_id)
  references element (elm_id)  ;
alter table recharge add constraint fk_re_recharge
  foreign key (rch_elm_id)
  references element (elm_id)  ;
alter table vaisseau add constraint fk_ve_vaisseau
  foreign key (vss_elm_id)
  references element (elm_id)  ;
alter table vaisseau add constraint fk_vs_vaisseau
  foreign key (vss_sct_id)
  references secteur (sct_id)  ;
alter table element add constraint fk_ep_prs
  foreign key (elm_prs_id)
  references personnage (prs_id)  ;
alter table forme add constraint fk_sf_sexe
  foreign key (frm_sex_id)
  references sexe (sex_id)  ;
alter table arme add constraint fk_ae_arme
  foreign key (arm_elm_id)
  references element (elm_id)  ;
alter table interdit_classe add constraint fk_cli_cls
  foreign key (inc_cls_id)
  references classe (cls_id)  ;
alter table interdit_classe add constraint fk_cli_clsint
  foreign key (inc_interdit_cls_id)
  references classe (cls_id)  ;
alter table prerequis_classe add constraint fk_pqc_cls
  foreign key (pqc_cls_id)
  references classe (cls_id)  ;
alter table prerequis_carac add constraint fk_pqa_clsniv
  foreign key (pqa_cln_id)
  references classe_niveau (cln_id)  ;
alter table prerequis_classe add constraint fk_pqc_cln
  foreign key (pqc_cln_id)
  references classe_niveau (cln_id)  ;
alter table element add constraint fk_el_lieu
  foreign key (elm_lie_id)
  references lieu (lie_id)  ;
alter table acces_connu add constraint fk_acp_prs
  foreign key (acc_prs_id)
  references personnage (prs_id)  ;
alter table acces_connu add constraint fk_ach_sct
  foreign key (acc_chm_id)
  references chemin (chm_id)  ;
alter table monde_connu add constraint fk_mcp_prs
  foreign key (mnc_prs_id)
  references personnage (prs_id)  ;
alter table monde_connu add constraint fk_mcm_mnd
  foreign key (mnc_mnd_id)
  references monde (mnd_id)  ;
alter table munition add constraint fk_cgtmt_type_munition
  foreign key (mnt_tmt_id)
  references type_munition (tmt_id)  ;
alter table joueur add constraint fk_jc_joueurcss
  foreign key (jou_css_id)
  references css (css_id)  ;
alter table itineraire add constraint fk_ich_itineraire
  foreign key (itn_chm_id)
  references chemin (chm_id)  ;
alter table itineraire add constraint fk_frmit_itineraire
  foreign key (itn_frm_id)
  references forme (frm_id)  ;
alter table filature add constraint fk_ftf_typflt
  foreign key (flt_tfl_id)
  references type_filature (tfl_id)  ;
alter table filature add constraint fk_ftprs_prssuivant
  foreign key (flt_suivant_prs_id)
  references personnage (prs_id)  ;
alter table filature add constraint fk_ftfrm_prssuivi
  foreign key (flt_suivi_frm_id)
  references forme (frm_id)  ;
alter table mime add constraint fk_mtm_typmim
  foreign key (mim_tmm_id)
  references type_mime (tmm_id)  ;
alter table rumeur add constraint fk_rmrprs_auteur
  foreign key (rmr_prs_id)
  references personnage (prs_id)  ;
alter table rumeur_ecoute add constraint fk_rmc_ecouteur
  foreign key (rmc_prs_id)
  references personnage (prs_id)  ;
alter table rumeur_ecoute add constraint fk_rmc_rumeur
  foreign key (rmc_rmr_id)
  references rumeur (rmr_id)  ;
alter table langue add constraint fk_ctl_conlangue
  foreign key (lng_id)
  references caracteristique (crc_id)  ;
alter table itineraire add constraint fk_itt_type_trace
  foreign key (itn_ttr_id)
  references type_trace (ttr_id)  ;
alter table trace_connue add constraint fk_tctt_type_trace
  foreign key (trc_ttr_id)
  references type_trace (ttr_id)  ;
alter table trace_connue add constraint fk_ptc_itineraire
  foreign key (trc_prs_id)
  references personnage (prs_id)  ;
alter table trace_connue add constraint fk_ptct_itineraire
  foreign key (trc_frm_trace_id)
  references forme (frm_id)  ;
alter table forme add constraint fk_fot_type_trace
  foreign key (frm_ttr_id)
  references type_trace (ttr_id)  ;
alter table rumeur add constraint fk_rmrlng_langue
  foreign key (rmr_lng_id)
  references langue (lng_id)  ;
alter table calque_lieu_carto add constraint fk_lcclc_lieucalque
  foreign key (clc_lic_id)
  references lieu_carto (lic_id)  ;
alter table lieu_carto add constraint fk_liel_lieucarto
  foreign key (lic_lie_id)
  references lieu (lie_id)  ;
alter table lieu_carto add constraint fk_lcm_lieucmonde
  foreign key (lic_mnd_id)
  references monde (mnd_id)  ;
alter table lieu_carto add constraint fk_lcs_lieucsaison
  foreign key (lic_ssn_id)
  references saison (ssn_id)  ;
alter table monde add constraint fk_mtm_typmnd
  foreign key (mnd_tmn_id)
  references type_monde (tmn_id)  ;
alter table cycle add constraint fk_ccl_cycle
  foreign key (ccl_duree_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_monde add constraint fk_cclm_cycle_monde
  foreign key (cmn_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_monde add constraint fk_cmn_monde
  foreign key (cmn_mnd_id)
  references monde (mnd_id)  ;
alter table cycle_secteur add constraint fk_cclsc_cycle_secteur
  foreign key (csc_ccl_id)
  references cycle (ccl_id)  ;
alter table cycle_secteur add constraint fk_csc_secteur
  foreign key (csc_sct_id)
  references secteur (sct_id)  ;
alter table lieu_cycle add constraint fk_ccll_lieu_cycle
  foreign key (lcd_ccl_id)
  references cycle (ccl_id)  ;
alter table lieu_cycle add constraint fk_lcdl_lieu
  foreign key (lcd_lie_id)
  references lieu (lie_id)  ;
alter table periode add constraint fk_ccl_cperiode
  foreign key (prd_ccl_id)
  references cycle (ccl_id)  ;
alter table lieu_cyclique add constraint fk_lccl_lieu
  foreign key (lcc_lie_id)
  references lieu (lie_id)  ;
alter table lieu_cyclique add constraint fk_lcdp_lcycle
  foreign key (lcc_prd_id)
  references periode (prd_id)  ;
alter table calque add constraint fk_tcq_calque_tcalque
  foreign key (clq_tcq_id)
  references type_calque (tcq_id)  ;
alter table calque_lieu_carto add constraint fk_clqclc_cartocalque
  foreign key (clc_clq_id)
  references calque (clq_id)  ;
alter table vent_lieu_carto add constraint fk_lctvt_lieuvent
  foreign key (cvt_lic_id)
  references lieu_carto (lic_id)  ;
alter table vent_lieu_carto add constraint fk_vlctv_cartovent
  foreign key (cvt_tvt_id)
  references type_vent (tvt_id)  ;
alter table calque add constraint fk_clcrc_calquecarac
  foreign key (clq_crc_id)
  references caracteristique (crc_id)  ;
alter table rumeur add constraint fk_rmrsct_secteur
  foreign key (rmr_sct_id)
  references secteur (sct_id)  ;
alter table restriction_calque add constraint fk_rtc_calque
  foreign key (rtc_clq_id)
  references calque (clq_id)  ;
alter table influence_vent add constraint fk_tvi_typevent
  foreign key (inv_tvt_id)
  references type_vent (tvt_id)  ;
alter table influence_vent add constraint fk_tvi_typevehicule
  foreign key (inv_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table restriction_calque add constraint fk_tvr_typevehicule
  foreign key (rtc_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table vehicule add constraint fk_vht_typevehiculeposte
  foreign key (vhc_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table secteur add constraint fk_svh_vehicule
  foreign key (sct_vhc_id)
  references vehicule (vhc_id)  ;
alter table type_vehicule_poste add constraint fk_ttp_typevehicule
  foreign key (ttp_tvh_id)
  references type_vehicule (tvh_id)  ;
alter table type_vehicule_poste add constraint fk_ttp_typeposte
  foreign key (ttp_tps_id)
  references type_poste (tps_id)  ;
alter table poste add constraint fk_pst_typeposte
  foreign key (pst_tps_id)
  references type_poste (tps_id)  ;
alter table poste add constraint fk_pst_vehicule
  foreign key (pst_vhc_id)
  references vehicule (vhc_id)  ;
alter table monture add constraint fk_monture_cavperso
  foreign key (mon_cavalier_prs_id)
  references personnage (prs_id)  ;
alter table monture add constraint fk_mon_perso
  foreign key (mon_prs_id)
  references personnage (prs_id)  ;
alter table monture add constraint fk_monture_element
  foreign key (mon_clef_elm_id)
  references element (elm_id)  ;
alter table monture add constraint fk_monture_elementgen
  foreign key (mon_egn_id)
  references element_gen (egn_id)  ;
alter table chemin add constraint fk_chcrc_carac
  foreign key (chm_crc_id)
  references caracteristique (crc_id)  ;
alter table chemin add constraint fk_ctc_parchm
  foreign key (chm_pch_id)
  references particularite_chemin (pch_id)  ;
alter table description_lieu add constraint fk_desclieu
  foreign key (dsl_lie_id)
  references lieu (lie_id)  ;
alter table classeniveau_action add constraint fk_cna_action
  foreign key (cna_act_id)
  references action (act_id)  ;
alter table classeniveau_action add constraint fk_cnacln_action
  foreign key (cna_cln_id)
  references classe_niveau (cln_id)  ;
alter table chemin add constraint fk_chcrc_carac_2
  foreign key (chm_crc_2_id)
  references caracteristique (crc_id)  ;
alter table calque add constraint fk_clcrc_calquecarac2
  foreign key (clq_crc2_crc_id)
  references caracteristique (crc_id)  ;
alter table poste add constraint fk_pst_perso
  foreign key (pst_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_perso_un
  foreign key (eng_prs1_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_perso_deux
  foreign key (eng_prs2_prs_id)
  references personnage (prs_id)  ;
alter table engagement add constraint fk_eng_lieu
  foreign key (eng_lie_id)
  references lieu (lie_id)  ;
alter table arme_gen add constraint fk_agcrc_1
  foreign key (agn_carac1_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_2
  foreign key (agn_carac2_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_3
  foreign key (agn_compet1_crc_id)
  references caracteristique (crc_id)  ;
alter table arme_gen add constraint fk_agcrc_4
  foreign key (agn_compet2_crc_id)
  references caracteristique (crc_id)  ;
alter table element_gen add constraint fk_egn_maladie_possession
  foreign key (egn_possession_mld_id)
  references maladie (mld_id)  ;
alter table element_gen add constraint fk_egn_maladie_equipe
  foreign key (egn_equipe_mld_id)
  references maladie (mld_id)  ;
alter table type_vehicule add constraint fk_tveg_carburant
  foreign key (tvh_carburant_egn_id)
  references element_gen (egn_id)  ;
alter table rumeur add constraint fk_rmrlie_lieu
  foreign key (rmr_lie_id)
  references lieu (lie_id)  ;
alter table itineraire add constraint fk_depiti_lieu
  foreign key (itn_depart_lie_id)
  references lieu (lie_id)  ;
alter table itineraire add constraint fk_arriti_lieu
  foreign key (itn_arrivee_lie_id)
  references lieu (lie_id)  ;
alter table monde add constraint fk_mtm_maladie
  foreign key (mnd_mld_id)
  references maladie (mld_id)  ;
alter table depot_element add constraint fk_dpt_element
  foreign key (dpt_elm_id)
  references element (elm_id)  ;
alter table vente_element add constraint fk_vnt_element
  foreign key (vnt_elm_id)
  references element (elm_id)  ;
alter table banque add constraint fk_bnq_possesseur
  foreign key (bnq_prs_id)
  references personnage (prs_id)  ;
alter table banque add constraint fk_bnq_monnaie
  foreign key (bnq_mnn_id)
  references monnaie (mnn_id)  ;
alter table depot_element add constraint fk_dpt_possesseur
  foreign key (dpt_prs_id)
  references personnage (prs_id)  ;
alter table debit_element add constraint fk_dbt_element
  foreign key (dbt_egn_id)
  references element_gen (egn_id)  ;
alter table coffre add constraint fk_ce_coffre
  foreign key (cff_elm_id)
  references element (elm_id)  ;
alter table coffre_element add constraint fk_cee_coffre
  foreign key (cel_elm_id)
  references element (elm_id)  ;
alter table coffre_element add constraint fk_cce_coffre
  foreign key (cel_cff_id)
  references coffre (cff_id)  ;
alter table coffre add constraint fk_ceg_cle_coffre
  foreign key (cff_clef_egn_id)
  references element_gen (egn_id)  ;
alter table element_visible add constraint fk_elv_elm
  foreign key (elv_elm_id)
  references element (elm_id)  ;
alter table element_visible add constraint fk_elv_prs
  foreign key (elv_prs_id)
  references personnage (prs_id)  ;
alter table lieu add constraint fk_iml_image
  foreign key (lie_img_id)
  references image (img_id)  ;
alter table element_gen_connu add constraint fk_egc_element_gen_connu
  foreign key (egc_egn_id)
  references element_gen (egn_id)  ;
alter table element_gen_connu add constraint fk_egc_personnage
  foreign key (egc_prs_id)
  references personnage (prs_id)  ;
alter table boutique add constraint fk_btq_personnage
  foreign key (btq_prs_id)
  references personnage (prs_id)  ;
alter table boutique add constraint fk_btq_automate
  foreign key (btq_atm_id)
  references automate (atm_id)  ;
alter table boutique add constraint fk_btq_coffre
  foreign key (btq_cff_id)
  references coffre (cff_id)  ;
alter table automate_carac add constraint fk_acr_automate
  foreign key (acr_atm_id)
  references automate (atm_id)  ;
alter table automate_carac add constraint fk_acr_carac
  foreign key (acr_crc_id)
  references caracteristique (crc_id)  ;
alter table achat_element_gen add constraint fk_aeg_boutique
  foreign key (aeg_btq_id)
  references boutique (btq_id)  ;
alter table achat_element_gen add constraint fk_aeg_element_gen
  foreign key (aeg_egn_id)
  references element_gen (egn_id)  ;
alter table coffre_element add constraint fk_mce_monnaie
  foreign key (cel_mnn_id)
  references monnaie (mnn_id)  ;
alter table achat_element_gen add constraint fk_mae_monnaie
  foreign key (aeg_mnn_id)
  references monnaie (mnn_id)  ;
alter table secteur add constraint fk_ste_typevn
  foreign key (sct_tev_id)
  references type_environnement (tev_id)  ;
alter table type_environnement add constraint fk_tec_carac
  foreign key (tev_crc_id)
  references caracteristique (crc_id)  ;
alter table reponse add constraint fk_rq_question
  foreign key (rep_que_id)
  references question (que_id)  ;
alter table news add constraint fk_mn_mj
  foreign key (new_mji_id)
  references mj (mji_id)  ;
alter table candidature add constraint fk_jc_joueur
  foreign key (can_jou_id)
  references joueur (jou_id)  ;
alter table profil_annonce add constraint fk_map_mj
  foreign key (pro_mji_id)
  references mj (mji_id)  ;
alter table candidature add constraint fk_pac_profil
  foreign key (can_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_annonce add constraint fk_cpp_catpro
  foreign key (pro_ctp_id)
  references categorie_profil (ctp_id)  ;
alter table candidature add constraint fk_cxc_sexe
  foreign key (can_sex_id)
  references sexe (sex_id)  ;
alter table profil_classe add constraint fk_pac_proann
  foreign key (pcl_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_race add constraint fk_par_proann
  foreign key (pra_pro_id)
  references profil_annonce (pro_id)  ;
alter table profil_sexe add constraint fk_pas_proann
  foreign key (psx_pro_id)
  references profil_annonce (pro_id)  ;
alter table candidature add constraint fk_cr_race
  foreign key (can_rac_id)
  references race (rac_id)  ;
alter table candidature add constraint fk_cc_classe
  foreign key (can_cls_id)
  references classe (cls_id)  ;
alter table profil_classe add constraint fk_cpc_classe
  foreign key (pcl_cls_id)
  references classe (cls_id)  ;
alter table profil_race add constraint fk_pr_race
  foreign key (pra_rac_id)
  references race (rac_id)  ;
alter table profil_sexe add constraint fk_ps_sexe
  foreign key (psx_sex_id)
  references sexe (sex_id)  ;

