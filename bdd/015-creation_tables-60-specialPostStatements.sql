
-- Special statements for postgres:post databases
-- Un perso a UNE et UNE seule forme
CREATE TRIGGER trg_frm_actuelle 
 AFTER INSERT OR UPDATE ON forme 
 FOR EACH ROW EXECUTE PROCEDURE trg_frm_actuelle();
CREATE TRIGGER trg_frm_actuelle_del
 BEFORE DELETE ON forme
 FOR EACH ROW EXECUTE PROCEDURE trg_frm_actuelle_del();

-- Special statements for postgres:post databases
-- Un joueur a UN personnage principal
CREATE TRIGGER trg_prs_principal 
 AFTER INSERT OR UPDATE ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_prs_principal();
CREATE TRIGGER trg_prs_principal_del
 BEFORE DELETE ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_prs_principal_del();

-- Special statements for postgres:post databases
-- Un et un seul lieu saisonnier par lieu
--CREATE TRIGGER trg_lis_defaut
-- AFTER INSERT OR UPDATE ON lieu_saisonnier
-- FOR EACH ROW EXECUTE PROCEDURE trg_lis_defaut();
--CREATE TRIGGER trg_lis_defaut_del
-- BEFORE DELETE ON lieu_saisonnier
-- FOR EACH ROW EXECUTE PROCEDURE trg_lis_defaut_del();

-- Special statements for postgres:post databases
-- Un perso a au plus un niveau d'une classe
CREATE TRIGGER trg_pcl_classe 
 BEFORE INSERT OR UPDATE ON perso_classe
 FOR EACH ROW EXECUTE PROCEDURE trg_pcl_classe();

-- Special statements for postgres:post databases
-- Mise a jour des scores calcules a chaque modif
CREATE TRIGGER trg_pcr_update_score 
 BEFORE UPDATE ON perso_carac
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_update_score();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_pcr_insert_carac 
 AFTER INSERT ON caracteristique
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_insert_carac();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_pcr_insert_perso
 AFTER INSERT ON personnage
 FOR EACH ROW EXECUTE PROCEDURE trg_pcr_insert_perso();

-- Special statements for postgres:post databases
-- Alignement des perso_carac par personnage sur les caracs
CREATE TRIGGER trg_jou_css_defaut
 BEFORE INSERT ON joueur
 FOR EACH ROW EXECUTE PROCEDURE trg_jou_css_defaut();

-- Special statements for postgres:post databases
SELECT AddGeometryColumn('lieu_carto', 'lic_localisation',
                         -1, 'POINT', 2);
CREATE INDEX idx_lic_localisation ON lieu_carto
   USING gist(lic_localisation gist_geometry_ops);

