BEGIN {
	i=0
}

END {
	for (i in errors) print i".\t"errors[i];
	exit i
}


#
# Check if the object is already present in the array
#
function check ( tableName, objName, objArray, objType ) {
	#print "check", tableName, objName, objType
	table=tableName;
	previous_table=objArray[objName];
	if ( previous_table"X" == "X" ) {
		#print previous_table"\ttableName="tableName"\tobjName="objName
		objArray[objName]=tableName
	} else {
		error(objType" '"objName"' used by tables:\n\t\t'"previous_table"'\n\t\t'"table"'");
	}
}


#
# Log errors
#
function error ( message ) {
	i++;
	errors[i]=message
}


/add constraint/ {
	check($3, $6, ctr, "Constraint")
}


/create unique index/ {
	check($6, $4, idx, "index")
}

# Helper : keep current table name
/create table/ {
	tableNameCreation=$3
}

# Look for xxx_id
$1 ~ /^[a-z][a-z][a-z]_id$/ {
	check(tableNameCreation, $1, ids, "ID")
}
