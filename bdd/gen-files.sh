#!/bin/bash

# Variables fixes
d=`date +%F.%H-%M-%S`
ok="ok"
pok="ERREUR !"
SERVEUR="www.peacefrogs.net"

# Paramètres
PARSE_DIA_BIN="/usr/local/bin/parsediasql"
options="gnirpzlsa";
SAVE_ACT="";
TEDIA_ACT="";
PARSEDIA_ACT="";
SQLINST_ACT="";
SQLREMPL_ACT="";
PNG_ACT="";
TARGZ_ACT="";
LIENHTML_ACT="";

SAVE_MSG="* Sauvegarde sur le serveur distant";
TEDIA_MSG="* Génération de SQL (installation depuis le DIA)";
PARSEDIA_MSG="* Utilisation de parsedia à la place de tedia2sql";
SQLINST_MSG="* Génération de SQL (installation complémentaire à DIA)";
SQLREMPL_MSG="* Génération de SQL (remplissage)";
PNG_MSG="* Génération du PNG";
TARGZ_MSG="* Génération du tar.gz (avec toutes les sources disponibles regénérées !)";
LIENHTML_MSG="* Mise à jour du lien du tar.gz sur la page HTML";
TOUT_MSG="* Tout à faire";


function syntaxe()
{
	echo "Syntaxe : gen-files.sh <options>"
	echo "	Les options sont :"
	echo "	-g : Génère le fichier SQL d'installation à partir du fichier DIA (via tedia2sql)"
	echo "	-n : Forcer l'utilisation de parsediasql à la place de tedia2sql (necessaire pour le nouveau format dia à partir de 0.97)"
	echo "	-i : Génère les fichiers SQL d'installation (création de table, séquenceurs, triggers, etc.)"
	echo "	-r : Génère les fichiers SQL de remplissage"
	echo "	-p : Génère le fichier PNG du schéma DIA"
	echo "	-z : Génère le fichier tar.gz des fichiers du répertoire"
	echo "	-l : Met à jour le lien du fichier tar.gz depuis la page HTML"
	echo "	-s : Sauvegarde sur un serveur distant"
	echo "	-a : Fait tout (A PRIVILEGIER)"
	echo 
	echo " Ex: ./gen-files.sh -gir [permet de tester si des modifs sur le schéma garde une structure correcte]"
}

function lecture_param()
{
	while getopts $options option
	do
		case $option in
		g)
			echo "$TEDIA_MSG";
			TEDIA_ACT=1;
			;;
		n)
			echo "$PARSEDIA_MSG";
			PARSEDIA_ACT=1;
			;;
		i)
			echo "$SQLINST_MSG";
			SQLINST_ACT=1;
			;;
		r)
			echo "$SQLREMPL_MSG";
			SQLREMPL_ACT=1;
			;;
		p)
			echo "$PNG_MSG";
			PNG_ACT=1;
			;;
		z)
			echo "$TARGZ_MSG";
			TARGZ_ACT=1;
			;;
		l)
			echo "$LIENHTML_MSG";
			LIENHTML_ACT=1;
			;;
		s)
			echo "$SAVE_MSG";
			SAVE_ACT=1;
			;;
		a)
			echo "$TOUT_MSG";
			TEDIA_ACT=1;
			SQLINST_ACT=1;
			SQLREMPL_ACT=1;
			PNG_ACT=1;
			TARGZ_ACT=1;
			LIENHTML_ACT=1;
			SAVE_ACT=1;
			;;
		esac
	done
}

function proposer_menu()
{
	echo "Choisissez ce que vous souhaitez faire:"
	select choix in "$TEDIA_MSG" "$SQLINST_MSG" "$SQLREMPL_MSG" "$PNG_MSG" "$TARGZ_MSG" "$LIENHTML_MSG" "$SAVE_MSG" "$TOUT_MSG"
	do
		if [ -z "$choix" ]
		then
			echo "Choix inconnu."
			exit
		fi
		echo "Vous avez choisi : $choix";
	done
}

if [ "$1 " == "--help " ]
then
	syntaxe;
	exit;
fi;

if [ -n "$1" ]
then
	lecture_param $*;
else
	#proposer_menu;
	syntaxe;
fi;

function generer_SQL_parsediasql()
{
	echo "Conversion du fichier SQL pour qu'il soit compatible avec parsediasql... "
	./dia-remove-placeholder.py schema.dia
	echo "Generation de fichier SQL (parsediasql)... "
    perl $PARSE_DIA_BIN --file schema-corrected.dia --db postgres --ignore_type_mismatch > 015-creation_tables.sql
    sed -i '/^\[WARN/d' 015-creation_tables.sql
	echo " (et correction au passage de mauvaises protections de caractères dans les commentaires...)"
	sed -i s/\\\\'/\'\'/g 015-creation_tables.sql && echo $ok\)

	echo "Verification du modele : trigrammes "
	awk -f checker.awk 015-creation_tables.sql && echo $ok
	retour=$?
	echo "Verification du modele : contraintes"
	awk -f checker.awk 015-creation_tables.sql && echo $ok
	retour=$(($retour+$?))
	if [ $retour != 0 ]
	then
		echo "Le modele n'est pas correct ! $retour erreurs !"
		exit 1
	fi;

	echo "-- Application maladroite des droits de lecture des sequenceurs aux mj" > 016-droits-seq.sql
	grep "CREATE SEQUENCE" installation-tables.sql \
		| awk '{print "GRANT SELECT, UPDATE ON "$3" TO supermj;"}' >> 016-droits-seq.sql
}

function generer_SQL_tedia2sql()
{
	echo "Generation de fichier SQL (tedia2sql)... "
	# -i (input) -o (output) -d (drop) -t (base) -s (separation des sources) -c (moins de verbose)
    tedia2sql -i schema.dia -o 015-creation_tables.sql -d -t postgres -s -c && echo $ok
	#../tedia2sql-129/tedia2sql -i schema.dia -o 015-creation_tables.sql -d -t postgres -s -c && echo $ok
	echo " (et correction au passage de mauvaises protections de caractères dans les commentaires...)"
	sed -i s/\\\\'/\'\'/g 015-creation_tables-50-schemaCreate.sql && echo $ok\)

	echo "Verification du modele : trigrammes "
	awk -f checker.awk 015-creation_tables-50-schemaCreate.sql && echo $ok
	retour=$?
	echo "Verification du modele : contraintes"
	awk -f checker.awk 015-creation_tables-90-constraintsCreate.sql && echo $ok
	retour=$(($retour+$?))
	if [ $retour != 0 ]
	then
		echo "Le modele n'est pas correct ! $retour erreurs !"
		exit 1
	fi;

	echo "-- Application maladroite des droits de lecture des sequenceurs aux mj" > 016-droits-seq.sql
	grep "CREATE SEQUENCE" installation-tables.sql \
		| awk '{print "GRANT SELECT, UPDATE ON "$3" TO supermj;"}' >> 016-droits-seq.sql
}

function generer_SQL_installation()
{
	echo -n "Generation du fichier SQL d'installation des tables... "
	cat 0[1-9][0-9]-*.sql > installation-tables.sql && iconv -f iso8859-15 -t utf-8 installation-tables.sql > installation-tables-utf8.sql && mv installation-tables-utf8.sql installation-tables.sql && echo $ok
}

function generer_SQL_remplissage()
{
	echo -n "Generation du fichier SQL de remplissage des tables... "
	cat [1-9][0-9][0-9]-*.sql > remplissage-tables.sql && echo $ok
}

function generer_PNG_ACT()
{
	echo -n "Generation du schema en PNG... "
	dia schema.dia --export=schema.png && echo " "$ok
}

function generer_TARGZ_ACT()
{
	echo -n "Creation du tar.gz... "
	tar -cvzf "scripts-bdd-morrigan.$d.tar.gz" [0-9]*.??? schema.dia schema.png remplissage-tables.sql installation-tables.sql && echo $ok
}

function generer_lien_HTML()
{
	echo -n "Mise a jour du lien vers public_html... "
	rm scripts-bdd-morrigan.tar.gz && ln -s "scripts-bdd-morrigan.$d.tar.gz" scripts-bdd-morrigan.tar.gz && echo $ok
}

function sauver_serveur_distant()
{
	echo -n "Envoi du fichier sur $SERVEUR... "
	scp "scripts-bdd-morrigan.$d.tar.gz" armel@$SERVEUR:~/morrigan && echo $ok
}

test -n "$TEDIA_ACT" && test "$PARSEDIA_ACT" != 1 && generer_SQL_tedia2sql;
test -n "$TEDIA_ACT" && test -n "$PARSEDIA_ACT" && generer_SQL_parsediasql;
test -n "$SQLINST_ACT" && generer_SQL_installation;
test -n "$SQLREMPL_ACT" && generer_SQL_remplissage;
test -n "$PNG_ACT" && generer_PNG_ACT;
test -n "$TARGZ_ACT" && generer_TARGZ_ACT;
test -n "$LIENHTML_ACT" && generer_lien_HTML;
test -n "$SAVE_ACT" && sauver_serveur_distant;

echo "FIN."
