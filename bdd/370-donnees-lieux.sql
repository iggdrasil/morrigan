-- La ville de New Seattle

INSERT INTO secteur (sct_id, sct_nom, sct_description, sct_tsc_id, sct_charge_max, sct_tev_id)
	VALUES (1, 'New-Seattle', 'New-Seattle est une ville chaleureuse et accueillante. Les voyageurs y sont fréquents, ce qui apportent à cet endroit hors du commun richesse et grandeur.', 1, null, 1);


-- Sa place, son marché, sa taverne

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (1, 1, 1, 'Place du marché', False, 'Dans la fraicheur du matin, les marchands s''activent sur la place du village. Des étals divers vous offrent moult denrées et toutes les espèces de breloques.');
	
INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (2, 2, 1, 'Taverne', False, 'Tout autour de vous, des gens attablés discutent politique... Comment ? On ne vous l''a pas dit ? Ce lieu est le nerf de la guerre. Tout y est remis en cause, des solutions sont présentées. Malheureusement personne n''écoutent ces ivrognes. Une bière bien fraiche vous permettrait sûrement d''oublier le soleil de plomb qui règne derrière ses stores tirés.');

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (3, 1, 1, 'Remise', False, 'Dans cette remise, vous trouvez de nombreux futs, des quantités astronomiques de chopines, toutes prêtes à remplacer leurs fidèles camarades mortes au front... sur le sol de la taverne.');

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (4, 1, 1, 'Les enfers', False, 'Ceci est votre dernière demeure. Vous êtes ici pour l''éternité et l''éternité c''est long, surtout sur la fin.');


