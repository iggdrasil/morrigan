--Style classique
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (1, 'Classique', true, 'http://www.peacefrogs.net/morrigan_extra/css/style.css');

-- Style verdâtre Odyssée
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (2, 'Odyssée', false, 'http://www.peacefrogs.net/morrigan_extra/css/odyssee.css');

-- Style épuré
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (3, 'Clean', false, 'http://www.peacefrogs.net/morrigan_extra/css/clean.css');

-- Autre style assez simple
INSERT INTO css (css_id, css_nom, css_defaut, css_chemin)
	VALUES (4, 'Clean-2 - Die IE, die!', false, 'http://www.peacefrogs.net/morrigan_extra/css/clean-2.css');

-- Siltaom (mdp: 'siltaom')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (1, 'Siltaom', '71fa146a8f9f1531186dd2e5a9d431c2', 'armel@peacefrogs.net', 2, +2);

-- Nimnae (mdp: 'nimnae')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (2, 'Nimnae', '7c030583a5cbe1766b38ca5374cbe07c', 'etienne@peacefrogs.net', 2, +2);

-- Arkenlond (mdp: 'arkenlond')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (3, 'Arkenlond', '721649b8ae5c2f712989ab20cbc96230', 'giddzit@yahoo.fr', 2, +2);

-- Syl (mdp: 'syl')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (4, 'Syl', '0c7e0e3afed9b89ff0fd10b7a5bff0ce', 'syllelfesylvain@hotmail.com', 2, +2);

-- Florent (mdp: 'florent')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (5, 'Florent', 'b2ff8e48c14343ca3f51fce08f4d0d12', 'lioret.florent@wanadoo.fr', null, +2);

-- Stéphanie (mdp: 'stephanie')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
	VALUES (6, 'Stephanie', '59f29878f0ca6fffa2485e3c5e3b5443', 'stephy@schtroumpf.com', null, +2);

-- Nawa (mdp: 'nawa')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (7, 'Nawa', '1bf1fb5624fbf5ce51ebc776e7982040', 'bellefeegore@gmail.com', null, +2);

-- Zarkam (mdp: 'beep')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (8, 'Zarkam', '1284e53a168a5ad955485a7c83b10de0', 'yann.lejeune@gmail.com', null, +2);

-- Jipli (mdp: 'jipli')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (9, 'Jipli', 'b116275e9d57e77ec86f115ad003a867', 'jp.lapica@free.fr', null, +2);

-- Nath (mdp: 'nath')
INSERT INTO joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    VALUES (10, 'Nath', '320c8d45fe4ee818e1d185954c2c251d', 'erinelle@free.fr', null, +2);

-- Dummy (mdp : 'nopass')
insert into joueur (jou_id, jou_pseudo, jou_mdp, jou_email, jou_css_id, jou_fuseau)
    values (20, 'Dummy', '0945fc9611f55fd0e183fb8b044f1afe', 'etienne@peacefrogs.net', 1, +2);

-- Natures de carac

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (1, 'Physique');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (2, 'Intellectuel');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (3, 'Combat');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (4, 'Magie');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (5, 'Déplacement');

INSERT INTO nature_caracteristique (ncr_id, ncr_nom)
	VALUES (6, 'Technique');

-- Caractéristiques (attributs)

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (1, 'Force', 'Fort comme un troll, dit-on. Devinez pourquoi.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (2, 'Dextérité', 'Adroit comme un elfe. La dextérité est l''habileté â manier une arme, un instrument.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (3, 'Constitution', 'Solide comme un ent. La constitution est la résistance, l''endurance.', 1,
        1, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (4, 'Intelligence', 'Intelligent comme un dragon. L''intelligence est la capacité à savoir réfléchir.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (5, 'Sagesse', 'Sage comme un poisson. Vous avez déjà vu un poisson sortir tout seul de l''eau ? Non, et bien voilà.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (6, 'Charisme', 'Beau comme moi. Cela veut tout dire.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (7, 'Perception', 'Beau comme moi. Cela veut tout dire.', 1,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (8, 'Karma', 'Good ou bad...', 1,
        2, 0, False);

-- Caractéristiques (compétences)

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (9, 'Mains nues/arme improvisée', 'Vraiment ? Tu penses avoir un don pour foutre des pains ?', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (10, 'Arme légère', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (11, 'Arme classique', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (12, 'Arme à deux mains', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (13, 'Combat à deux armes', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (14, 'Combat monté', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (15, 'Arme de tir', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (16, 'Arme de jet', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (17, 'Technique offensive', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (18, 'Technique defensive', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (19, 'Nécromancie', '', 2,
        4, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (20, 'Shamanisme', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (21, 'Magie élémentaire', '', 2,
        3, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (22, 'Commerce', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (23, 'Vol à la tire', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (24, 'Crochetage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (25, 'Détection des pièges', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (26, 'Discrétion', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (27, 'Pistage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (28, 'Alchimie', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (29, 'Artisanat', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (30, 'Soins', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (31, 'Dépeçage', '', 2,
        6, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (32, 'Connaissance de la faune', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (33, 'Connaissance de la flore', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (34, 'Connaissance des roches', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (35, 'Connaissance des métaux', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (36, 'Survie en cité', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (37, 'Survie extérieure : climat chaud', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (38, 'Survie extérieure : climat froid', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (39, 'Survie extérieure : climat tempéré', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (40, 'Pédagogie', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (41, 'Lecture/écriture', '', 2,
        2, 0, True);
INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (42, 'Astronomie/cartographie', '', 2,
        2, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (43, 'Escalade', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (44, 'Navigation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (45, 'Équitation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (46, 'Natation', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (47, 'Marche/course', '', 2,
        5, 0, True);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (48, 'Millianais', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (49, 'Septimien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (50, 'Septimien ancien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (51, 'Légion ancienne', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (52, 'Karhnais', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (53, 'Avresolien', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (54, 'Elfique', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (55, 'Nain', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (56, 'Orc', '', 3,
        2, 0, False);

INSERT INTO caracteristique (crc_id, crc_nom, crc_description, crc_tcr_id,
                             crc_ncr_id, crc_delta_min_max, crc_visible)
VALUES (57, 'Draconique', '', 3,
        2, 0, False);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (1, 'Elfe', null, 'L''elfe peut s''apparenter a l''humain. De taille superieur a ce dernier, il se differencie plus particulierement de lui par ses oreilles pointues.', false, 35);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (2, 'Orc', null, 'L''orc est une creature de physionomie musclee. Sa sale tete et son manque de savoir-vivre le font regulierement remarquer.', false, 32);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (3, 'Humain', null, 'L''humain est de taille moyenne. Plus trapu que l''elfe, moins costaud que l''orc, il presente des facultes transversales comme la couture, les bains de boue ou la maroquinerie.', false, 30);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (4, 'Vampire', null, 'Le vampire est a part. Par son affinite avec la nuit comme son gout pour le sang, il n''est guere ouvertement apprecie. Sauf parmi ses congeneres...', false, 30);

INSERT INTO race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille)
	VALUES (5, 'Dragon', null, 'Le dragon est un animal aujourd''hui domestique. Dote de quatre pattes (dont deux ridicules), il peut tres bien courir et voler partout dans l''appartement. Mefiance...', true, 50);
	
insert into race (rac_id, rac_nom, rac_mld_id, rac_desc, rac_monture, rac_indice_taille) 
    values (6, 'Nain', NULL, 'Trapu voire même trapu, franc et têtu, gagner l''amitié d''un nain est précieux, s''en faire un ennemi est mortel.', FALSE, 22);

INSERT INTO compagnie (cmp_id, cmp_nom)
	VALUES (1, 'Les moucherons');

INSERT INTO compagnie (cmp_id, cmp_nom)
	VALUES (2, 'Les combattants de l''impossible');
-- Saisons

INSERT INTO saison (ssn_id, ssn_ordre, ssn_nom, ssn_default)
	VALUES (1, 1, 'Jour', True);
	
INSERT INTO saison (ssn_id, ssn_ordre, ssn_nom, ssn_default)
	VALUES (2, 2, 'Nuit', False);

-- La grande A'Tuin !
INSERT INTO monde (mnd_id, mnd_nom, mnd_tmn_id, mnd_description, mnd_pere_mnd_id, mnd_niveau, mnd_mld_id, mnd_image)
	VALUES (1, 'Le disque monde', 2, 'Le Disque-Monde est un monde rond et plat qui repose sur le dos de quatre éléphants géants, eux-mâmes placés sur le dos d''une tortue interstellaire, A''tuin, qui est sur le dos de... pas grand chose. A''tuin parcourt l''univers à la recherche de... elle seule le sait. Car on ignore tout d''elle. Nul ne sait si cette tortue est mâle ou femelle. Les savants du Disque-Monde ont établi une hypothèse qui veut que cette tortue soit en route pour se reproduire; si A''tuin est mâle, pas de problème... si elle est femelle, on est mal ! Cette théorie porte le nom de ""Big Bang"" ou de ""Grande Secousse"".', NULL, NULL, NULL, 'maps/demo_map.jpg');

-- Vents
INSERT INTO type_vent (tvt_id, tvt_nom) values (1, 'Vent');
INSERT INTO type_vent (tvt_id, tvt_nom) values (2, 'Courant marin');

-- Environnements
INSERT INTO type_calque (tcq_id, tcq_nom, tcq_principal) values (1, 'Environnement', TRUE);

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (1, 0, 'Estran', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (2, 1, 'Glacier', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (3, 2, 'Roche', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (4, 3, 'Désert', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (5, 4, 'Toundra', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (6, 5, 'Steppe', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (7, 6, 'Marais', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (8, 7, 'Prairie', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (9, 8, 'Forêt', '', 1, 1, 0, 0, 0, 0, '', 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (10, 9, 'Champs', '', 1, 1, 0, 0, 0, 0, '', 1);


-- Climats
INSERT INTO type_calque (tcq_id, tcq_nom) values (2, 'Climat');

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (11, 1, 'Arctique', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (12, 2, 'Sibérien', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (13, 3, 'Tempéré', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (14, 4, 'Méditerranéen', '', 1, 1, 0, 0, 0, 0, '', 2);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (15, 5, 'Désertique chaud', '', 1, 1, 0, 0, 0, 0, '', 2);

-- Relief
INSERT INTO type_calque (tcq_id, tcq_nom) values (3, 'Relief');

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (16, 1, 'Mer', '', 1, 1, 0, 1000, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (17, 2, 'Plaine', '', 1, 1, 0, 4, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (18, 3, 'Colline', '', 1, 1, 0, 5, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (19, 4, 'Moyenne montagne', '', 1, 1, 0, 8, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (20, 5, 'Montagne', '', 1, 1, 0, 10, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (21, 6, 'Haute montagne', '', 1, 1, 0, 15, 0, 0, '', 3, 1);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id, clq_crc_id)
    VALUES (22, 7, 'Très haute montagne', '', 1, 1, 0, 20, 0, 0, '', 3, 1);

-- Viaires
INSERT INTO type_calque (tcq_id, tcq_nom, tcq_principal) values (4, 'Voirie', True);

INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (23, 1, 'Route', '', 1, 1, 0, -3, 0, 0, '', 4);
INSERT INTO calque (clq_id, clq_id_externe, clq_nom, clq_description, clq_facteur_effacement_trace, clq_facteur_nb_trace, clq_cout_pn, clq_cout_acces_pn, clq_cout_pv, clq_cout_acces_pa, clq_couleur, clq_tcq_id)
    VALUES (24, 2, 'Rivière', '', 1, 1, 0, 1000, 0, 0, '', 4);

-- Types de lieu

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (1, 'Commun');
	
INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (2, 'Taverne');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (3, 'Banque');
	
INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (4, 'Forge');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (5, 'Temple');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (6, 'Armurerie');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (7, 'Échoppe');

INSERT INTO type_lieu (tli_id, tli_nom)
	VALUES (8, 'Poste de pilotage');

-- Type d'environnement

INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (1, 'Urbain', 36);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (2, 'Chaud', 37);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (3, 'Froid', 38);
INSERT INTO type_environnement (tev_id, tev_nom, tev_crc_id)
    VALUES (4, 'Tempéré', 39);
-- La ville de New Seattle

INSERT INTO secteur (sct_id, sct_nom, sct_description, sct_tsc_id, sct_charge_max, sct_tev_id)
	VALUES (1, 'New-Seattle', 'New-Seattle est une ville chaleureuse et accueillante. Les voyageurs y sont fréquents, ce qui apportent à cet endroit hors du commun richesse et grandeur.', 1, null, 1);


-- Sa place, son marché, sa taverne

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (1, 1, 1, 'Place du marché', False, 'Dans la fraicheur du matin, les marchands s''activent sur la place du village. Des étals divers vous offrent moult denrées et toutes les espèces de breloques.');
	
INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (2, 2, 1, 'Taverne', False, 'Tout autour de vous, des gens attablés discutent politique... Comment ? On ne vous l''a pas dit ? Ce lieu est le nerf de la guerre. Tout y est remis en cause, des solutions sont présentées. Malheureusement personne n''écoutent ces ivrognes. Une bière bien fraiche vous permettrait sûrement d''oublier le soleil de plomb qui règne derrière ses stores tirés.');

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (3, 1, 1, 'Remise', False, 'Dans cette remise, vous trouvez de nombreux futs, des quantités astronomiques de chopines, toutes prêtes à remplacer leurs fidèles camarades mortes au front... sur le sol de la taverne.');

INSERT INTO lieu (lie_id, lie_tli_id, lie_sct_id, lie_nom, lie_carto, lie_desc)
	VALUES (4, 1, 1, 'Les enfers', False, 'Ceci est votre dernière demeure. Vous êtes ici pour l''éternité et l''éternité c''est long, surtout sur la fin.');


-- Siltaom
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES ( 1, 1, true, null, 1, 'Siltaom le ridicule', 'Siltaom le gros-bill', 'Il était une fois un orc....', 'Arrivée: Baie; escalade du Mont-Granite sans les mains, ...', now(), now(), 30, 35, 8, 20, 9, 10, 50, 50, 15, 15, 1, 1, now());

-- Nimnae
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (2, 2, true, null, 1, 'Nimnae', 'Nimnae sombre', 'Dans une grotte naquit la petite...', 'Arrivée: pouf; Missionnaire dans les îles...', now(), now(), 26, 32, 10, 20, 8, 10, 45, 45, 20, 20, 1, 1, now());

-- Arkenlond
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (3, 3, true, null, 1, 'Arkenlond le belliqueux', 'Arkenlond le vétéran', 'Un jour, un âtre vert en armure tendit une hache...', 'Débarquement sur la plage d''Amoho; Fou de bibliothèque; Fou;...', now(), now(), 23, 25, 5, 20, 10, 10, 80, 80, 25, 25, 1, 1, now());

-- Aegon Balerion
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (4, 2, false, 2, 1, 'Aegon Balerion', 'Aegon Balerion', 'Elevé par Nimnae qui avait recu ces oeufs de sa famille ...', 'Naissance sous un pont.', now(), now(), 111, 250, 200, 200, 500, 500, 225, 300, 30, 30, 3, 1, now());

-- Syl
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (5, 4, true, null, 1, 'Mr S','Syl', 'Syl l''elfe est apparu spontanément ...', 'Arrivée: En cours.', now(), now(), 20, 20, 20, 20, 10, 10, 50, 50, 10, 10, 1, 1, now());

-- Sulfure
INSERT INTO personnage ( prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
    VALUES (6, 5, true, null, 1, 'Sulfure','Sulfure', 'D''un volcan est surgi un homme...', 'Aucune idée. Il a fait pouf.', now(), now(), 20, 25, 15, 18, 10, 10, 80, 80, 15, 15, 1, 1, now());

-- Ambre
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (7, 7, True, null, 1, 'Ambre', 'Ambre', 'Fille d''une larme de mer et de poussière de terre.', 'Elle quitta bien vite ses parents pour un ailleurs.', now(), now(), 22, 28, 20, 20, 8, 10, 40, 40, 10, 10, 1, 1, NOW());

-- Cerencov
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (8, 8, True, null, 1, 'Cerencov', 'Cerencov', 'Enfant prodige aux milles talents. Depuis il se fait bien plus discret.', 'Il tue ses parents avec ses dents avant de s''en aller en courant.', now(), now(), 22, 28, 20, 20, 8, 10, 40, 40, 10, 10, 1, 1, NOW());

-- Cohen
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (9, 9, True, null, 1, 'Cohen', 'Cohen', 'Dit le barbare, élévé par des orcs, il s''imposa bien vite comme le plus fort d''entre eux...', 'Trop long a conter...', now(), now(), 30, 30, 25, 25, 2, 2, 100, 100, 20, 20, 1, 1, NOW());

-- Miss Sable
INSERT INTO personnage (prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
VALUES (10, 10, True, null, 1, 'Miss Sable', 'Miss Sable', 'Il y a bien longtemps dans une lointaine galaxie...', 'Erre de par le monde pour trouver l''illumination.', now(), now(), 22, 28, 18, 18, 8, 10, 40, 40, 8, 8, 1, 1, NOW());

-- Dummy
insert into personnage
(prs_id, prs_jou_id, prs_principal, prs_possesseur_prs_id, prs_lie_id, prs_nom_originel, prs_nom_actuel, prs_background, prs_biographie, prs_inscription, prs_derniere_connexion, prs_pa, prs_pa_max, prs_pn, prs_pn_max, prs_pp, prs_pp_max, prs_pv, prs_pv_max, prs_pc, prs_pc_max, prs_tpr_id, prs_calc_facteur_nb_trace, prs_date_arrivee)
values (20, 20, True, NULL, 1, 'Dummy','Dummy', '', '', NOW(), NOW(), 25, 25, 20, 20, 9, 10, 40, 40, 10, 10, 1, 1, NOW());


-- Siltaom et Nimnae font partie des Moucherons
INSERT INTO place_compagnie (plc_cmp_id, plc_place, plc_prs_id)
	VALUES (1, 1, 1);

INSERT INTO place_compagnie (plc_cmp_id, plc_place, plc_prs_id)
	VALUES (1, 2, 2);

-- Arkenlond font partie des Combattants
INSERT INTO place_compagnie (plc_cmp_id, plc_place, plc_prs_id)
	VALUES (2, 1, 3);

INSERT INTO place_compagnie (plc_cmp_id, plc_place, plc_prs_id)
    VALUES (2, 2, 5);

--- des scores moyens pour tous

UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 1; -- Force
UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 2; -- Dex
UPDATE perso_carac SET pcr_score_xp = 1280, pcr_score_xp_max = 1280 WHERE pcr_crc_id = 3; -- Const


-- Siltaom est un mago de l'eau

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 1 AND pcr_crc_id = 4; -- Int

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 1 AND pcr_crc_id = 5; -- Sag

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 1 AND pcr_crc_id = 6; -- Charisme (mieux en vampire)

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 1 AND pcr_crc_id = 13; -- Eau

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 1 AND pcr_crc_id = 7; -- Occultisme

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 1 AND pcr_crc_id = 22; -- Étiquette

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 1 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 1 AND pcr_crc_id = 24; -- Langue elfique


-- Nimnae est une prêtresse
UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 2 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 512, pcr_score_xp_max = 512 WHERE pcr_prs_id = 2 AND pcr_crc_id = 2;

UPDATE perso_carac SET pcr_score_xp = 768, pcr_score_xp_max = 768 WHERE pcr_prs_id = 2 AND pcr_crc_id = 3; -- Const

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 2 AND pcr_crc_id = 4; -- Pas très fute-fute

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 2 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 1024 WHERE pcr_prs_id = 2 AND pcr_crc_id = 6; -- Mais elle impose

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 9; -- Epée

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 12; -- Ténèbres

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 14; -- Herboriste

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 2 AND pcr_crc_id = 7; -- Occultisme

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 20; -- Un petit pêché ...

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 2 AND pcr_crc_id = 22; -- Étiquette

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 2 AND pcr_crc_id = 24; -- Langue elfique

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 2 AND pcr_crc_id = 25; -- Draconique


-- Arkenlond est un bourrin

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 2; -- Dext

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 3 AND pcr_crc_id = 3; -- Const

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 3 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 8; -- Mains nues

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 10; -- Et les deux en plus !

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 3 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 3 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 0 WHERE pcr_prs_id = 3 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 0 WHERE pcr_prs_id = 3 AND pcr_crc_id = 12;


-- Aegon est un petit mago chétif (mais attention il déchire grâce à sa maladie de dragon)

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 4 AND pcr_crc_id = 1; -- Force
UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 4 AND pcr_crc_id = 3; -- Constitution

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 4 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 4 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 4 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 4 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 4 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 128 WHERE pcr_prs_id = 4 AND pcr_crc_id = 24; -- Langue elfique

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 4 AND pcr_crc_id = 25; -- Draconique


-- Syl est... heu... un gringalet qui sait tirer à l'arc (le bigre, c'est bien le seul)
UPDATE perso_carac SET pcr_score_xp = 640, pcr_score_xp_max = 640 WHERE pcr_prs_id = 5 AND pcr_crc_id = 1; -- Force

UPDATE perso_carac SET pcr_score_xp = 1920, pcr_score_xp_max = 1920 WHERE pcr_prs_id = 5 AND pcr_crc_id = 2; -- Dext

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 5 AND pcr_crc_id = 5;

UPDATE perso_carac SET pcr_score_xp = 512 WHERE pcr_prs_id = 5 AND pcr_crc_id = 19;

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 5 AND pcr_crc_id = 8;

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 5 AND pcr_crc_id = 21;

UPDATE perso_carac SET pcr_score_xp = 512 WHERE pcr_prs_id = 5 AND pcr_crc_id = 22;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 5 AND pcr_crc_id = 23; -- Langue humaine

UPDATE perso_carac SET pcr_score_xp = 1280 WHERE pcr_prs_id = 5 AND pcr_crc_id = 24; -- Langue elfique


-- Sulfure est une brute (ne pas négliger les menues réparations de vêtements)
-- Epee, const et force sont ses points forts.

UPDATE perso_carac SET pcr_score_xp = 1580, pcr_score_xp_max = 1580 WHERE pcr_prs_id = 6 AND pcr_crc_id = 1;

UPDATE perso_carac SET pcr_score_xp = 640, pcr_score_xp_max = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 2;

UPDATE perso_carac SET pcr_score_xp = 1152, pcr_score_xp_max = 1152 WHERE pcr_prs_id = 6 AND pcr_crc_id = 3;

UPDATE perso_carac SET pcr_score_xp = 256 WHERE pcr_prs_id = 6 AND pcr_crc_id = 4;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 6;

UPDATE perso_carac SET pcr_score_xp = 768 WHERE pcr_prs_id = 6 AND pcr_crc_id = 8;

UPDATE perso_carac SET pcr_score_xp = 1408 WHERE pcr_prs_id = 6 AND pcr_crc_id = 9;

UPDATE perso_carac SET pcr_score_xp = 384 WHERE pcr_prs_id = 6 AND pcr_crc_id = 10;

UPDATE perso_carac SET pcr_score_xp = 896 WHERE pcr_prs_id = 6 AND pcr_crc_id = 20;

UPDATE perso_carac SET pcr_score_xp = 640 WHERE pcr_prs_id = 6 AND pcr_crc_id = 23; -- Langue humaine


-- Tout le monde a un minimum en Millianais histoire de causer un peu
UPDATE perso_carac set pcr_score_xp=1024, pcr_score_xp_max=1024 where pcr_crc_id=48;
-- Sexes
INSERT INTO sexe (sex_id, sex_nom)
	VALUES (1, 'Male');

INSERT INTO sexe (sex_id, sex_nom)
	VALUES (2, 'Féminin');

INSERT INTO sexe (sex_id, sex_nom)
	VALUES (3, 'Asexué');
	
-- Types de trace

insert into type_trace (ttr_id, ttr_nom) values (1, 'Pied nu');
insert into type_trace (ttr_id, ttr_nom) values (2, 'Botte');
insert into type_trace (ttr_id, ttr_nom) values (3, 'Chaussure à talon');
insert into type_trace (ttr_id, ttr_nom) values (4, 'Patte d''oiseau');
insert into type_trace (ttr_id, ttr_nom) values (5, 'Patte féline');

-- Siltaom est un elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (1, 1, 1, 1, false, 'Elfe fragile aux pommettes rosies par le soleil, ...', 'Siltaom fait actuellement de la flûte sur un dromadaire.', 'http://siltaom.free.fr/elfe_morrigan.jpg', 1, 3, 4, 36);

-- Siltaom est aussi un vampire !!
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (2, 1, 4, 1, true, 'Le vampire face à vous se tient bien droit. Le teint livide, il vous observe, guette chacun de vos mouvements.', 'Attablé, Siltaom déguste un cadavre fort attrayant', '', 1, 4, 5, 30);


-- Nimnae est une elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (3, 2, 1, 2, true, 'Face a vous se tient, une jeune humaine au teint lilial, de taille moyenne. Pour qui a deja rencontre un de ces etres albinos, il n''est guere surprenant de decouvrir les yeux ecarlates qui emergent de ce facies a la blancheur si parfaite. De longs et tenus cheveux aux pales nuances cuivrees soigneusement deposes sur les epaules encadrent ce visage a la peau depigmentee. Les traits de cette figure pourraient etre qualifies de fins et delicats s''il n''y avait ce nez a l''arete relativement saillante qui semble decouper en deux parties ce facies. Cette impression se confirme lorsque s''attardant dans le regard de cette humaine, on remarque que l''oeil gauche est plus sombre que l''oeil droit. Ce visage et de menues mains de nacre sont les seuls membres qui fendent le noir profond des vetements que cette jeune humaine porte. Elle est notamment vetue d''une grande cape dont la capuche n''est que tres rarement rabattue pour cacher ce visage ivoire.En effet ce ne sera que pour se proteger du soleil ou s''isoler que la jeune humaine disparaitra dans la penombre de sa tenue. Le seul luxe apparent sur la jeune dame est un pendentif retenue par une chainette en etain passee autour de son cou. Le pendentif a en son centre une petite aquamarine de couleur bleu foncé en forme de larme. Ce bijou n''a probablement pasgrande valeur marchande mais Nimnae semble y preter beaucoup d''importance.', 'A ses oreilles vous vous direz surement que cette humaine ...', 'http://www.peacefrogs.net/~etienne/bg/morr_nim.png', 2, 3, 3, 38);


-- Arkenlond est un orc
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (4, 3, 2, 1, false, 'L''orc avec les lunettes délicatement posées sur son gros pif...', 'Arkenlond fait du body painting.', 'http://www.peacefrogs.net/~etienne/bg/ark.jpg', 5, 6, 6, 30);

-- ... ou un elfe
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (5, 3, 1, 1, false, 'Elfe filiforme plein de charme.', 'Arkenlond tapote un doigt songeur sur son menton.', '', 1, 4, 4, 36);


-- Aegon est un dragon
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
	VALUES (6, 4, 5, 1, true, 'Grand dragon noir, l''ampleur de ses ailes est telle qu''il lui suffit de survoler une ville pour la plonger dans l''ombre.', 'Balerion crache des flammes aussi noires que ses écailles.', 'http://www.peacefrogs.net/~etienne/bg/aegon.jpg', 4, 6, 6, 50);


-- Que peut bien être Syl ...
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (7, 5, 1, 1, true, 'Elfe aux oreilles coupées.', 'Syl pleure.', '', 1, 4, 4, 36);


-- Sulfure est un humain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
    VALUES (8, 6, 3, 1, true, 'Humain à la dégaine originale, Sulfure est un être de chair et de muscle et de muscle (vous re-regardez de nouveau le bonhomme est vous vous dites vraiment qu''il est fait de muscle au-dessus de muscle). Son sabre en bandoulière, il semble peu amical mais attire les regards sur lui. Sans doute que cela est dû à la présence d''aiguilles à tricoter sur son flanc.', 'Sulfure joue au bilboquet, son péché mignon.', '', 2, 4, 4, 29);

-- Ambre est une humaine
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (9, 7, 3, 2, true, 'Jeune femme rousse aux yeux verts.', 'Souriante.', '', 1, 3, 3, 26);

-- Cerencov est un humain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (10, 8, 3, 1, true, 'Viel homme.', '', '', 1, 4, 4, 30);

-- Cohen est un nain
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (11, 9, 6, 1, true, 'Ce nain bourru (digne ambassadeur de sa race) compense sa taille par un tempérament fort', 'Cohen est un tantinet éméché', NULL, 2, 5, 3, 22);

-- Miss Sable est une humaine
INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
VALUES (12, 10, 3, 2, true, 'Femme petite et mince.', 'Souriante, elle se promène d''un pas léger.', '', 2, 4, 4, 26);

-- Même si il a l'air d'un fantôme Dummy est un humain

INSERT INTO forme (frm_id, frm_prs_id, frm_rac_id, frm_sex_id, frm_actuelle, frm_description, frm_desc_actuelle, frm_url_image, frm_ttr_id, frm_profond_trace, frm_taille_trace, frm_indice_taille)
 VALUES (20, 20, 3, 1, true, 'Vous voyez un etre indistinct et sans formes... Un fantome peut-etre',
   'Regard hagard et pas hesitant', '', 1, 4, 4, 30);

-- Mage (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (1, 'Mage', 'Le mage est un lanceur de sort', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (1, 1, 1, 'Mage débutant', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (2, 1, 2, 'Mage initié', null, false);

-- Guerrier (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (2, 'Guerrier', 'Le guerrier peut etre une brute ou un tactitien', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (3, 2, 1, 'Tapouilleur', null, false);

-- Prêtre (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (3, 'Pretre', 'Un prêtre se doit de prier son dieu.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (4, 3, 1, 'Fidèle', null, true);

-- Acrobate (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
	VALUES (4, 'Acrobate', 'L''acrobate est doué pour sauter dans tous les sens.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (5, 4, 1, 'Gesticuleur', null, true);

-- Lumineux (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (5, 'Lumineux', 'Avoir un comportement louable tend au coté lumineux.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (6, 5, 1, 'Bon', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (7, 5, 2, 'Blanc', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (8, 5, 3, 'Lumineux', null, true);

-- Obscur (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (6, 'Obscur', 'Egoiste, attiré par la puissance, le pouvoir: tendre vers l''obscurité.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (9, 6, 1, 'Mauvais', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (10, 6, 2, 'Noir', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (11, 6, 3, 'Obscur', null, true);

-- Tank (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (7, 'Tank', 'Encaisser les coups n''est pas donné à tout le monde.', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (12, 7, 1, 'Résistant', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (13, 7, 2, 'Tank', null, false);

-- Meneur (aptitude)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (8, 'Meneur', 'LE meneur d''hommes, le vrai, pret à envoyer se battre !', 2);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (14, 8, 1, 'Causeur public', null, false);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
	VALUES (15, 8, 2, 'Meneur d''hommes', null, false);

-- MJ lieu (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
    VALUES (9, 'MJ Lieu', 'Le MJ lieu peut créer des chemins, des lieux.', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
    VALUES (16, 9, 1, 'MJ Lieu', null, false);

-- Marchand (classe)
INSERT INTO classe (cls_id, cls_nom, cls_desc_mj, cls_tcl_id)
        VALUES (10, 'Marchand', 'Le marchand achète/vends, gère des boutiques', 1);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
        VALUES (17, 10, 1, 'Colporteur en herbe', null, true);

INSERT INTO classe_niveau (cln_id, cln_cls_id, cln_niveau, cln_titre, cln_mld_id, cln_titre_visible)
        VALUES (18, 10, 2, 'Marchand', null, true);


-- Le champ pcl_id est SERIAL et inutilisé sans crainte

-- Siltaom est un mage niv 2
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (1, 2,  null);

-- Nimnae est une pretresse niv 1 (avec un titre personnalisé !)
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (2, 4, 'Prêtresse de Bisounours');

-- Arkenlond est un guerrier niv 1
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (3, 1, null);

-- Syl est un acrobate niv 1
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (5, 5, null);

-- Sulfure est un guerrier niv 2
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (6, 1, null);

-- Des MJs lieu
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (1, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (2, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (3, 16, null);
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
    VALUES (8, 16, null);

-- Nimnae est une colporteuse niv 1 (avec un titre personnalisé !)
INSERT INTO perso_classe (pcl_prs_id, pcl_cln_id, pcl_titre)
	VALUES (2, 17, 'Colporteuse à deux sous');


-- Or
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (1, 'Or', 100);

-- Argent
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (2, 'Argent', 10);

-- Bronze
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (3, 'Bronze', 1);


-- Siltaom a 100 po et 30 pa
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (1, 1, 100);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (1, 2, 30);

-- Nimnae a 10 po et 50 pc
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (2, 1, 10);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (2, 3, 50);

-- Arkenlond a 26 po et 35 pa
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (3, 1, 26);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (3, 2, 35);

-- Syl n'est guère riche
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (5, 1, 3);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (5, 2, 10);

-- Sulfure ne l'est pas plus !
INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (6, 1, 1);

INSERT INTO perso_monnaie (pmn_prs_id, pmn_mnn_id, pmn_montant)
	VALUES (6, 2, 3);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (1, 'Or', 'Metal précieux parmi les précieux, l''or est une ressource rare fort appréciée pour sa solidité et sa simple beauté', 8);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (2, 'Bronze', 'Alliage de cuivre et d''étain, le cuivre est reconnu pour sa résistance à l''usure et l''érosion', 6);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (3, 'Peau de bete', 'Mieux vaut faire parti des traqueurs que des traqués. Certains en font encore les frais, pour leur solide peau.', 3);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (4, 'Fer', 'Costaud mais pas trop, le fer est très répandu: il a fait ses preuves !', 5);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (5, 'Bois', 'Issu de ressources naturelles, le bois est souvent utilisé car à la fois costaud et flexible.', 4);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (6, 'Cuir', 'Le cuir est de la peau d''animal tannée.', 3);

INSERT INTO matiere (mtr_id, mtr_nom, mtr_desc, mtr_resistance)
	VALUES (7, 'Organique', 'De la chair, du sang...', 2);
-- Mécanique

INSERT INTO technologie (tcn_id, tcn_nom, tcn_desc, tcn_taux_enrayage)
	VALUES (1, 'Mecanique', 'Arme a feu, ...', 2);

-- Clé de la remise
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (1, 'Clé de la remise', 'La clé de la remise de la taverne de New Seattle. Une pauvre clé toute pourrie.', 1, 5, false, null, null, null, 4, 4);

-- Chevalière de Syl
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (2, 'Chevaliere en bronze', 'Une jolie chevaliere en bronze dont la forme n''est pas sans rappeler l''empereur de New Seattle.', 1, 100, true, null, null, null, 2, 2);

-- Sac
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (3, 'Sacoche en peau de bete', 'Capable de contenir une quantité impressionnante de nourriture (un repas frugal, tout au plus), ce sac est parfaitement multifonction.', 1, 10, false, null, null, null, 3, 8);


-- Sabre (joli, hein, quand même)
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (4, 'Sabre', 'Finenement ouvragé, ce sabre est autant un sabre de parade que de combat. N''empêche qu''il vous tranchera malgré tout en deux si vous n''êtes pas initié à ce genre d''arme. Argh.', 2, 120, true, null, null, null, 4, 18);

-- Epée ancestrale
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (5, 'Epée ancestrale', 'Une belle, une magnifique épée lourdement forgée dans un lieu tenu secret. Irrésistible.', 2, 1000, true, null, null, null, 4, 20);

-- Mitraillette lance-grenade
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (6, 'Mitraillette lance-grenade', 'Attention au grabuge. Voici le dernier cri des armes de guerre personnelles.', 5, 200, true, null, null, 1, 4, 20);


-- Chargeur 20 balles
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (7, 'Chargeur 20 balles classiques', 'Un simple chargeur de 20 balles.', 1, 3, false, null, null, null, 4, 2);

-- Chargeur 2 grenades explosives
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (8, 'Chargeur 2 grenades explosives', 'Un chargeur de 2 grenades a utiliser dans un lance-grenade.', 1, 10, false, null, null, null, 4, 3);


-- Arc court
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (9, 'Arc court', 'Un bon petit arc en bois.', 2, 30, true, null, null, null, null, 16);

-- Carquois
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (10, 'Carquois de 30 flèches', 'Un carquois de 30 flèches.', 1, 15, false, null, null, null, 5, 5);

-- Armure de cuir
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (11, 'Armure de cuir', 'L''armure de cuir est une compromis entre solidité et légèreté.', 3, 120, true, null, null, null, 6, 24);

-- Ses petites menottes
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille)
	VALUES (12, 'Mains nues', 'Arme qui ne vous ferra jamais défaut. À moins que...', 2, 0, false, null, null, null, null, null);

-- Un coffre
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille) 
    VALUES (13, 'Coffre', 'Un chouette truc où l''on peut ranger des choses dedans', 1, 100, false, null, null, null, null, 28);

-- Une armoire
INSERT INTO element_gen (egn_id, egn_nom, egn_desc, egn_tel_id, egn_prix, egn_equipable, egn_possession_mld_id, egn_equipe_mld_id, egn_tcn_id, egn_mtr_id, egn_taille) 
    VALUES (14, 'Armoire', 'Un chouette truc où l''on peut ranger encore plus de choses que dans un coffre mais encore moins transportable', 1, 300, false, null, null, null, null, 66);


-- Une clé de la remise appartient à Syl ...
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (1, 1, null, null, null, null, null, null, null, 5, null);

-- Une chevaliere appartient également à Syl ...
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (2, 2, null, null, null, true, null, null, null, 5, null);

-- et un sac aussi !
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (3, 3, null, null, null, null, null, null, null, 5, null);

-- Arkenlond a aussi une chevaliere.
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (4, 2, null, 'Autour de ce bijou en cuivre, c''est un cerclage en or que l''on discerne là ?', 150, false, null, null, null, 4, null);

-- Une clé est sur le sol de la taverne !
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (5, 1, null, null, null, null, null, null, null, null, 2);


-- Sabre ELEMENT
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES(6, 4, 'Sabre de Yachus', null, 100, true, null, null, false, 6, null);

-- Epée ancestrale ELEMENT (ds la remise)
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (7, 5, 'Epee ancestrale des O''Coolin', null, null, null, null, null, null, null, 3);

-- Mitraillette lance-grenade ELEMENT
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (8, 6, 'Mitraillette lance-grenade', null, null, null, null, null, null, 3, null);


-- Chargeur de 20 balles (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (9, 'Chargeur 20 balles', 7, null, null, null, null, null, null, null, 3);

-- Chargeur de 2 grenades (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (10, 'Chargeur 2 grenades', 8, null, null, null, null, null, null, null, 3);

-- Arc ELEMENT (ds la remise)
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES(11, 9, 'Arc court en bois', null, null, true, null, null, false, null, 3);

-- Carquois de 30 flèches (ds la remise)
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (12, 'Carquois de 30 flèches', 10, null, null, null, null, null, null, null, 3);


-- Armure de cuir
INSERT INTO element (elm_id, elm_nom, elm_egn_id, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (13, 'Armure de cuir', 11, null, null, 'true', null, null, null, null, 3); 

-- Mimines pour tous !
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (14, 12, 'Main nues', null, 0, false, true, null, true, 1, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (15, 12, 'Main nues', null, 0, false, true, null, true, 2, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (16, 12, 'Main nues', null, 0, false, true, null, true, 3, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (17, 12, 'Main nues', null, 0, false, true, null, true, 4, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (18, 12, 'Main nues', null, 0, false, true, null, true, 5, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (19, 12, 'Main nues', null, 0, false, true, null, true, 6, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (20, 12, 'Main nues', null, 0, false, true, null, true, 7, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (21, 12, 'Main nues', null, 0, false, true, null, true, 8, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (22, 12, 'Main nues', null, 0, false, true, null, true, 9, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (23, 12, 'Main nues', null, 0, false, true, null, true, 10, null);
INSERT INTO element(elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
	VALUES (24, 12, 'Main nues', null, 0, false, true, null, true, 20, null);

-- on meuble un peu la remise
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
    VALUES (25, 13, 'Coffre de la remise', null, null, null, null, null, null, null, 3);
INSERT INTO element (elm_id, elm_egn_id, elm_nom, elm_desc, elm_prix, elm_equipable, elm_equipe, elm_enraye, elm_naturel, elm_prs_id, elm_lie_id)
    VALUES (26, 14, 'Armoire de la remise', null, null, null, null, null, null, null, 3);

-- sac
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
	VALUES (1, 3, NULL, 10, True);

-- coffre
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
    VALUES (2, 25, 5, 16, False);

-- armoire
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
    VALUES (3, 26, NULL, 40, True);
 
-- Types de munition
INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (1, 'Balle');

INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (2, 'Grenade');

INSERT INTO type_munition (tmt_id, tmt_nom)
	VALUES (3, 'Fleche');



-- Balles classique
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (1, 'Balle classique', 'Efficace, pas trop chère (généralement), ce genre de munition est prisé.', 1, null);

-- Balles explosives -> maladie
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (2, 'Balle explosive', 'A l''impact, cette balle se décompose en une explosion, provoquant des dégâts conséquents.', 1, null);

-- Grenades explosives
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (3, 'Grenade explosive', 'Une bonne grenade classique, qui fait tres tres mal.', 2, null);

-- Fleches
INSERT INTO munition (mnt_id, mnt_nom, mnt_desc, mnt_tmt_id, mnt_mld_id)
	VALUES (4, 'Fleche', 'Pfffttt. Rapide et précise.', 3, null);



-- Chargeur 20 balles classiques
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (1, 7, 1, 20);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (1, 9, 18);


-- Chargeur 2 grenades explosives
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (2, 8, 3, 2);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (2, 10, 2);


-- Carquois de 30 fleches
INSERT INTO recharge_gen (cgn_id, cgn_egn_id, cgn_mnt_id, cgn_nb_max_mnt)
	VALUES (3, 10, 4, 30);

INSERT INTO recharge (rch_id, rch_elm_id, rch_nb_mnt)
	VALUES (3, 12, 27);


-- Dégâts
INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (1, 'tranchant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (2, 'perforant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (3, 'contondant');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (4, 'explosif');

INSERT INTO type_degat (tdg_id, tdg_nom)
	VALUES (5, 'ballistique');


-- Sabre GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (1, 4, 1, null, 1, 1, 3, false, null, 1, NULL, 11, NULL);

-- Sabre
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (1, 1, null, 6);


-- Epee ancestrale GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (2, 5, 1, null, null, 2, 4, true, null, 1, NULL, 11, NULL);

-- Epee ancestrale
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (2, 2, null, 7);


-- Mitraillette lance-grenade (partie mitraillette) GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (3, 6, 5, null, 20, 2, 4, true, 1, 2, NULL, 15, NULL);

-- Mitraillette lance-grenade (partie lance-grenade) GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (4, 6, 4, null, 20, 5, 10, true, 2, 2, NULL, 15, NULL); -- Maladie ?


-- Mitraillette lance-grenade (partie mitraillette)
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (3, 3, 1, 8);

-- Mitraillette lance-grenade (partie lance-grenade)
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (4, 4, 2, 8);


-- Arc court GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (5, 9, 2, null, 10, 2, 3, true, 3, 2, NULL, 15, NULL);

-- Arc court
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (5, 5, null, 11);

-- Mains nues GENERIQUE
INSERT INTO arme_gen (agn_id, agn_egn_id, agn_tdg_id, agn_mld_id, agn_portee, agn_degat_min, agn_degat_max, agn_deux_mains, agn_tmt_id, agn_carac1_crc_id, agn_carac2_crc_id, agn_compet1_crc_id, agn_compet2_crc_id)
	VALUES (6, 12, 3, null, 1, 1, 1, true, null, 2, NULL, 9, NULL);

-- Mimines pour tous
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (6, 6, null, 14);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (7, 6, null, 15);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (8, 6, null, 16);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (9, 6, null, 17);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (10, 6, null, 18);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (11, 6, null, 19);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (12, 6, null, 20);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (13, 6, null, 21);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (14, 6, null, 22);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (15, 6, null, 23);
INSERT INTO arme (arm_id, arm_agn_id, arm_rch_id, arm_elm_id)
	VALUES (16, 6, null, 24);
-- Zones corporelles
INSERT INTO zone_armure (zon_id, zon_nom)
	VALUES (1, 'Buste');

INSERT INTO zone_armure (zon_id, zon_nom)
	VALUES (2, 'Tête');



-- Armure de cuir
INSERT INTO armure_gen (ugn_id, ugn_egn_id, ugn_bonus_defense)
	VALUES (1, 11, 0);

-- Degats encaisses par l'armure de cuir
-- Tranchant
INSERT INTO armure_gen_degat (agd_id, agd_ugn_id, agd_tdg_id, agd_encaissement_min, agd_encaissement_max)
	VALUES (1, 1, 1, 0, 1);

-- Perforant
INSERT INTO armure_gen_degat (agd_id, agd_ugn_id, agd_tdg_id, agd_encaissement_min, agd_encaissement_max)
	VALUES (2, 1, 2, 0, 2);

-- Zones protégées par l'armure de cuir
INSERT INTO armure_gen_zone (agz_id, agz_ugn_id, agz_zon_id)
	VALUES (1, 1, 1);


INSERT INTO armure (aru_id, aru_elm_id)
	VALUES (1, 13);
-- sac
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
    VALUES (1, 3, NULL, 10, True);

-- coffre
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
    VALUES (2, 25, 5, 16, False);

-- armoire
INSERT INTO coffre (cff_id, cff_elm_id, cff_clef_egn_id, cff_taille, cff_ouvert)
    VALUES (3, 26, NULL, 40, True);
 
-- Chemin PLACE <-> TAVERNE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (1, 1, 2, 'Vous poussez la porte de la taverne. La joie de l''alcool s''offre à vous.', 'Vous quittez les bruits et conversations pour gagner la place.', true, true, true, null, 0, 0, 1, 1, 22);


-- Chemin TAVERNE <-> REMISE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (2, 2, 3, 'Vous utilisez la clé de la remise pour y pénétrer.', 'Vous utilisez la clé de la remise pour la quitter.', true, true, true, 1, 0, 0, 1, 1, 22);

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (3, 2, 3, 'Vous quittez la taverne pour la remise.', 'Vous quittez la remise pour la taverne.', true, true, true, null, 0, 0, 3, 1, 22);


-- Chemin REMISE -> PLACE

INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
	VALUES (4, 3, 1, 'Vous vous glissez par la lucarne et retombez en contre-bas sur une ruelle adjacente à la place.', null, true, false, true, null, 30, 10, 4, 1, 22);


-- Forêt <-> New-Seattle

--INSERT INTO chemin (chm_id, chm_depart_lie_id, chm_arrivee_lie_id, chm_desc_aller, chm_desc_retour, chm_aller, chm_retour, chm_ouvert, chm_egn_id, chm_discretion, chm_difficulte, chm_cout_pa, chm_tch_id, chm_crc_id)
--	VALUES (5, 1, 1395, 'Sortant de la cité en poussant le petit portillon de bois vermoulu, vous gagnez une jolie forêt...', 'Après avoir erré  dans la forêt, passant ses portes magnifiques vous entrez de la cité...', TRUE, TRUE, TRUE, null, 0, 0, 1, 1, 22);




-- Ne rien faire rend des PAs et des PNs / à ne surtout pas laisser dans un jeu :)
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (1, 'Do nothing', 'doing_nothing', 'Ne rien faire, c''est tellement bon...', '0', '0', '-40', '-40', '-40', '0,0', '0,0', true, 'Action simple destinée à la beta. Permet de récupérer des PNs et des PAs. Ne restera pas dans la version finale du jeu.', True);

-- Parler
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (2, 'Talk', 'talk', 'Papoter est bon pour votre santé mais peut-etre pas celle des autres.', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'L''action principale du jeu. Il est possible de s''adresser à une ou plusieurs personnes voire à l''ensemble des personnes présentes dans le lieu.', True);

-- Changer de forme (exemple du druide, du vampire)
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (3, 'Change shape', 'change_shape', 'Passer dans des endroits improbables, devenir plus fort, ou juste s''amuser à changer d''apparence.', '%(d10)d + %(c1)d + %(c12)d - %(d20)d', '0', '5', '10', '0', '2,12', '1,1;2,12', true, 'Action permettant de changer de forme. Cette action est bien sûr destinée simplement aux personnages métamorphes. Attention le changement ne peut se faire discretement.', False);

-- Changer de personnage pour gérer un second
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (4, 'Change character', 'change_character', 'Gérer un autre personnage, familier, etc.', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'Action permettant de se mettre dans la peau d''un autre personnage que l''on a à gérer (par exemple sa monture).', True);

-- Se déplacer
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (5, 'Move', 'move', 'Se déplacer: à pied, avec une monture, en véhicule, etc.', '%(d10)d + %(e0)d - %(d20)d - %(e1)d', '0', '%(e2)d', '%(e3)d', '0', '1,%(e4)d', '2,%(e4)d', true, 'Action de déplacement dans un autre lieu. Sur la carte ou dans un dans une même zone, les coûts sont variables', False);

-- Remise
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (6, 'Deliveries', 'remise', 'Remises journalieres', '0', '0', '-5', '-5', '-5', '0,0', '0,0', true, 'Tu es un gros-bill. Tu as le pouvoir des remises ! Est-ce normal ?', True);

-- Donner un objet
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (7, 'Give object', 'give_object', 'Donner un objet.', '0', '0', '0', '1', '0', '0,0', '0,0', true, 'Le don d''objet ne s''applique qu''aux objets non équipés et potentiellement donnable.', False);

-- Fouiller
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (8, 'Dig', 'dig', 'Fouiller le lieu.', '%(d10)d + 2*%(c4)d - %(d15)d', '0', '1', '0', '0', '0,0', '0,0', true, 'Rechercher un objet dans un lieu.', False);

-- Jeter un objet
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (9, 'Drop object', 'drop_object', 'Jeter un objet.', '0', '0', '0', '1', '0', '0,0', '0,0', true, 'Jeter un objet. Cette action ne s''applique qu''aux objets non équippés et potentiellement jetables.', False);

-- Equiper
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (10, 'Equip', 'equip', 'Equiper un objet', '0', '0', '0', '1', '4', '0,0', '0,0', true, 'Équiper et déséquiper les armes et armures que l''on possède.', True);

-- Mimer
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (11, 'Emote', 'emote', 'Mimer une action', 0, 0, '0', '0', '0', '0,0', '0,0', true, 'Mimer une action, seul ou en liaison avec un autre personnage.', True);

-- Lancer une rumeur
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (13, 'Buzz', 'buzz', 'Lancer une rumeur', '%(d10)d + %(c6)d + %(c22)d - 10 - %(d5)d - %(e1)d', '0', '10', '10', '0', '2,22', '1,6;2,22', true, 'Lancer une rumeur voire pourquoi pas dire des choses vraies.', False);

-- Ecouter une rumeur
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (14, 'Get the buzz', 'getBuzz', 'Aller aux nouvelles', '%(d10)d + %(c6)d + %(c22)d - 10 - %(d5)d', '0', '4', '0', '0', '2,22', '1,6;2,22', true, 'Aller aux nouvelles, se mettre au courant des éventuelles rumeurs disponibles.', False);

-- Suivre quelqu'un
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_cout_pp, act_form_cout_pv, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
	VALUES (15, 'Follow someone', 'follow', 'Suivre une personne', '%(d10)d + %(c26)d - 5 - %(d5)d - %(e1)d', '0', 2, 0, '0', null, null, '1,26', '2,26', true, 'Suivre une personne qui vient de partir', False);

-- Rechercher des traces de pas	
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_cout_pp, act_form_cout_pv,"act_form_gain_min","act_form_gain_max", "act_courante", "act_aide", act_combat) 
    values (16, 'Search steps', 'search_steps', 'Rechercher des traces de pas', '%(d10)d + %(c26)d + %(c5)d - 10 - %(d5)d - %(e1)d', '0', '10', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Montrer la route
insert into action (act_id, "act_nom", "act_nom_form", "act_desc", "act_form_attaque", "act_form_defense","act_form_cout_pa","act_form_cout_pn", act_form_cout_pc, "act_form_cout_pp","act_form_cout_pv","act_form_gain_min","act_form_gain_max", "act_courante", "act_aide", act_combat)
    values (17, 'Show the way', 'show_way', 'Montrer la route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, False);

-- Créer un lieu topologique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (18, 'Create a new location', 'create_location', 'Créer un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (18, 16, TRUE);

-- Créer un secteur
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (19, 'Create a new sector', 'create_sector', 'Créer un secteur', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer un secteur', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (19, 16, TRUE);

-- Créer une nouvelle route
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (20, 'Create a new road', 'create_road', 'Créer une route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer une route', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (20, 16, TRUE);

-- Créer une nouvelle description associée à un lieu
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (21, 'Create a new location description', 'create_loc_desc', 'Créer une description pour un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour créer une description pour un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (21, 16, TRUE);

-- Rechercher précisement dans un lieu
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (22, 'Look precisely', 'look_exactly', 'Regarder précisément un lieu', '%(d10)d + %(c26)d', '0', '2', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Modifier un lieu topologique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (23, 'Modify current location', 'modify_location', 'Modifier un lieu', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier un lieu', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (23, 16, TRUE);

-- Modifier un secteur
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (24, 'Modify current sector', 'modify_sector', 'Modifier un secteur', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier un secteur', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (24, 16, TRUE);

-- Modifier une route
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (25, 'Modify currents roads', 'modify_road', 'Modifier une route', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier une route', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (25, 16, TRUE);

-- Modifier une description précise
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (26, 'Modify description', 'modify_loc_desc', 'Modifier une description', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Action pour modifier une description', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (26, 16, TRUE);

-- Se téléporter (en tant qu'admin)
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (27, 'Teleport', 'admin_teleport', 'Se téléporter', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Se téléporter pour faciliter l''administration', False);
-- Cette action n'est disponible que pour les MJs lieu
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (27, 16, TRUE);

-- Chercher un passage
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (28, 'Search for an hidden path', 'search_path', 'Chercher un passage', '%(d10)d + %(c26)d + %(c5)d - 10 - %(d5)d', '0', '4', '0', '0', '0', '0', '2,26', '1,5;2,26', True, NULL, False);

-- Recharger une arme
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (29, 'Reload a weapon', 'reload', 'Recharger une arme', '0', '0', '0', '4', '4', '0', '0', '0,0', '0,0', True, NULL, True);

-- Décharger une arme
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (30, 'Unload a weapon', 'unload', 'Décharger une arme', '0', '0', '0', '4', '4', '0', '0', '0,0', '0,0', True, NULL, True);

-- S'engager dans un combat
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pp, act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (31, 'Take part in a fight', 'take_part', 'S''engager dans un combat', '%(d10)d+(not %(e1)d)*%(c2)d+(not not %(e1)d)*%(c1)d', '%(d10)d+%(c2)d', '4', '4', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Quitter un combat
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (32, 'Leave a fight', 'leave_fight', 'Se désengager d''un combat', '%(d6)d+(not %(e1)d)*%(c2)d+(not not %(e1)d)*%(c1)d', '(not %(e2)d)*%(c2)d+(not not %(e2)d)*(%(c1)d+%(d6)d)', '0', '4', '0', '0', '2', '0,0', '0,0', True, NULL, True);

-- Gagner l'initiative
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (33, 'Take the initiative', 'take_init', 'Prendre l''initiative', '10+%(e1)d*3+%(e2)d', '10+%(e3)d*3+%(e4)d', '0', '4', '0', '0', '4', '0,0', '0,0', True, NULL, True);

-- Attaquer quelqu'un
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn,act_form_cout_pp,act_form_cout_pv, act_form_cout_pc, act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (34, 'Attack someone', 'attack', 'Attaquer', '%(e1)d+%(e2)d+%(c17)d+%(e3)d', '(%(e4)d+%(e5)d+%(c18)d)+%(e6)d-%(e7)d', '0', '4', '0', '0', '4', '0,0', '0,0', True, NULL, True);

-- Ramasser un objet
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (35, 'Take an object', 'takeObject', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Mettre dans un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (36, 'Put into', 'putInto', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Prendre dans un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (37, 'Take into', 'takeInto', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Ouvrir/fermer un « coffre »
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (38, 'Open/close a chest', 'openCloseChest', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, NULL, True);

-- Ouvrir/fermer une boutique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (39, 'Open/close a shop', 'openCloseShop', 'Ouvrir/fermer un commerce', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, 'Ouvrir/fermer une boutique', False);
-- Cette action n'est disponible que pour les commercants
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (39, 17, TRUE);
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (39, 18, TRUE);

-- Gérer une boutique
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (40, 'Manage a shop', 'manageShop', 'Gérer une boutique', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', False, '', False);
-- Cette action n'est disponible que pour les commercants
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (40, 17, TRUE);
insert into classeniveau_action (cna_act_id, cna_cln_id, cna_autorise)
    values (40, 18, TRUE);

-- Acheter
insert into action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense,act_form_cout_pa,act_form_cout_pn, act_form_cout_pc,act_form_cout_pp,act_form_cout_pv,act_form_gain_min,act_form_gain_max, act_courante, act_aide, act_combat)
    values (41, 'Buy', 'buy', '', '0', '0', '0', '0', '0', '0', '0', '0,0', '0,0', True, '', False);

-- Prendre des notes personnelles dans son fichier d'action
INSERT INTO action (act_id, act_nom, act_nom_form, act_desc, act_form_attaque, act_form_defense, act_form_cout_pa, act_form_cout_pn, act_form_cout_pc, act_form_gain_min, act_form_gain_max, act_courante, act_aide, act_combat)
        VALUES (42, 'Taking notes', 'taking_notes', 'Prendre des notes dans son fichier d''action', '0', '0', '0', '0', '0', '0,0', '0,0', true, 'Action de prise de note dans le fichier d''action. Ces notes ne sont destinées qu''au joueur lui même et ne peuvent pas être partagées.', True);

-- Actions pour 1 personne

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (1, 'Salut', '%(subject)s salue l''assemblée.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (2, 'Rire', '%(subject)s se met à rire.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (3, 'Au revoir', '%(subject)s fait au revoir.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (4, 'Applaudir', '%(subject)s applaudit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (5, 'Surprise', '%(subject)s semble surpris.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (6, 'Confus', '%(subject)s semble confus.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (7, 'Tousser', '%(subject)s tousse.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (8, 'Peur', '%(subject)s semble avoir peur.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (9, 'Pleurer', '%(subject)s pleure.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (10, 'Clin d''oeil', '%(subject)s fait un clin d''oeil.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (11, 'Rougir', '%(subject)s rougit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (12, 'Mécontentement', '%(subject)s marque son mécontentement.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (13, 'Joie', '%(subject)s semble heureux.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (14, 'Révérence', '%(subject)s fait une révérence.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (15, 'Trépigner', '%(subject)s trépigne.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (16, 'Sourciller', '%(subject)s sourcille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (17, 'Grimacer', '%(subject)s grimace.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (18, 'Acquiescement', '%(subject)s acquiesce.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (19, 'Desapprobation', '%(subject)s marque sa desapprobation.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (20, 'Se prosterner', '%(subject)s se prosterne.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (21, 'Rôter', '%(subject)s rôte.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (22, 'Pêter', '%(subject)s pête.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (23, 'S''agenouiller', '%(subject)s s''agenouille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (24, 'Timide', '%(subject)s semble timide.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (25, 'Soupirer', '%(subject)s soupire.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (26, 'Sourire', '%(subject)s sourit.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (27, 'Cracher', '%(subject)s crache.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (28, 'Siffloter', '%(subject)s sifflote.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (29, 'Bailler', '%(subject)s baille.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (30, 'Faim/Soif', '%(subject)s montre qu''il a faim et soif.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (31, 'Se reculer', '%(subject)s se recule.', 1);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (32, 'S''avance', '%(subject)s s''avance.', 1);


-- Actions pour 2

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (33, 'Accolade', '%(subject)s tente de faire une accolade à %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (34, 'Insulter', '%(subject)s insulte %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (35, 'Embrasser', '%(subject)s tente d''embrasser %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (36, 'Se moquer', '%(subject)s se moque de %(dest)s.', 2);

INSERT INTO mime (mim_id, mim_nom, mim_label, mim_tmm_id)
	VALUES (37, 'Désigner', '%(subject)s désigne %(dest)s.', 2);

-- Monde connu

INSERT INTO monde_connu (mnc_mnd_id, mnc_prs_id)
	VALUES (1, 1);

INSERT INTO monde_connu (mnc_mnd_id, mnc_prs_id)
	VALUES (1, 2);

INSERT INTO monde_connu (mnc_mnd_id, mnc_prs_id)
	VALUES (1, 3);

INSERT INTO monde_connu (mnc_mnd_id, mnc_prs_id)
	VALUES (1, 4);

INSERT INTO monde_connu (mnc_mnd_id, mnc_prs_id)
	VALUES (1, 5);


-- Accès connu vers un secteur

--INSERT INTO acces_connu (acc_chm_id, acc_prs_id)
--	VALUES (5, 1);

--INSERT INTO acces_connu (acc_chm_id, acc_prs_id)
--	VALUES (5, 2);

--INSERT INTO acces_connu (acc_chm_id, acc_prs_id)
--	VALUES (5, 3);

--INSERT INTO acces_connu (acc_chm_id, acc_prs_id)
--	VALUES (5, 4);

--INSERT INTO acces_connu (acc_chm_id, acc_prs_id)
--	VALUES (5, 5);

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (48, 'Millianais', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (49, 'Septimien', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (50, 'Septimien ancien', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (51, 'Légion ancienne', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (52, 'Karhnais', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (53, 'Avresolien', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (54, 'Elfique', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (55, 'Nain', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (56, 'Orc', '');

INSERT INTO langue (lng_id, lng_nom, lng_description)
VALUES (57, 'Draconique', '');
