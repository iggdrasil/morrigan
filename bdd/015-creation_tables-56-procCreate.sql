
-- Procédure
-- Retour    : le score après calcul (intervention des maladies...)
-- Parametre : score avant prise en compte des maladies
-- Retour	 : score après traitement
CREATE OR REPLACE FUNCTION calcul_score(integer) RETURNS integer AS '
BEGIN
    RETURN $1/128;
END;
' LANGUAGE plpgsql;

