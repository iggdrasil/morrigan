
-- Or
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (1, 'Or', 100);

-- Argent
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (2, 'Argent', 10);

-- Bronze
INSERT INTO monnaie (mnn_id, mnn_nom, mnn_indice)
	VALUES (3, 'Bronze', 1);

