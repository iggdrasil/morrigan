-- Zones corporelles
INSERT INTO zone_armure (zon_id, zon_nom)
	VALUES (1, 'Buste');

INSERT INTO zone_armure (zon_id, zon_nom)
	VALUES (2, 'Tête');



-- Armure de cuir
INSERT INTO armure_gen (ugn_id, ugn_egn_id, ugn_bonus_defense)
	VALUES (1, 11, 0);

-- Degats encaisses par l'armure de cuir
-- Tranchant
INSERT INTO armure_gen_degat (agd_id, agd_ugn_id, agd_tdg_id, agd_encaissement_min, agd_encaissement_max)
	VALUES (1, 1, 1, 0, 1);

-- Perforant
INSERT INTO armure_gen_degat (agd_id, agd_ugn_id, agd_tdg_id, agd_encaissement_min, agd_encaissement_max)
	VALUES (2, 1, 2, 0, 2);

-- Zones protégées par l'armure de cuir
INSERT INTO armure_gen_zone (agz_id, agz_ugn_id, agz_zon_id)
	VALUES (1, 1, 1);


INSERT INTO armure (aru_id, aru_elm_id)
	VALUES (1, 13);
