
1. Comment installer son environnement de travail
-------------------------------------------------

1.1 Récupérer les sources
	On retrouve les 2 parties de l'outil d'administration sous forme de 2 répertoires, 'appli' et 'lib-ui'.
	Ces 2 aspects ont la même organisation:
		- src/	sources java
		- ressources/	fichiers de ressource (configuration, images, ...)
		- build.xml	fichier de tâches ant (cf plus loin)

1.2 Sous l'IDE Eclipse, importer les 2 projets associés.

1.3 Sélectionner le formatter (admin/eclipse-formatter.xml) pour les 2 projets.


2. Comment créer les *.jar et les déployer
------------------------------------------

2.1 En premier lieu, lib-ui
	Se placer dans son dossier :
		#ant create_jar
	La génération du jar de la librairie graphique a lieu par défaut dans un sous-répertoire créé pour l'occasion : generated/

2.2 Puis, dans l'admin
	2.2.1 Renseigner le fichier ressources/misc/deployment.properties sur son modèle ressources/misc/deployment.properties.template
		Il contient les quelques informations indispensables propres au système sur lequel l'admin sera installé (répertoire cible, url associée); essentiellement nécessaire pour générer le fichier jnlp de lancement.
	2.2.2 Se placer dans ce dossier :
		#ant deploy
		Cette tâche intègre 
			- le rappatriement du jar de la lib-ui, 
			- la compilation de l'admin,
			- la création du jar de l'admin,
			- la génération des fichiers jnlp (un pour le lancement via le système de fichier, un pour le lancement via le web) dans un répertoire spécificié
			- la signature des *.jar

