/*
 * MLogLevel.java
 * 3 juil. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

/**
 * @author Armel
 */
public interface MLogLevel {

  /**
   * Pas de log.
   */
  public static final int NONE = 0;

  /**
   * Logs informatifs (minimum).
   */
  public static final int INFO = 1;

  /**
   * Logs d'erreur.
   */
  public static final int ERROR = 1 << 1;

  /**
   * Logs de debug.
   */
  public static final int DEBUG = 1 << 2;

  /**
   * Tous les logs.
   */
  public static final int ALL = INFO | ERROR | DEBUG;

}
