/*
 * MHeavyLogger.java
 * 3 juil. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

/**
 * @author Armel
 */
public class MHeavyLogger extends MAbstractLogger {

  /** Index in the stack trace of the caller (if no error) */
  private static int INDEX_CALLER_STD = 1;

  /**
   * Index in the stack trace of the caller (if error and real stack trace,
   * etc.)
   */
  private static int INDEX_CALLER_ERROR = 0;

  private static boolean xml = false;

  /**
   * 
   */
  public MHeavyLogger(String logFileName, int logLevel, boolean console) {
    super(logFileName, logLevel, console);
  }

  /**
   * @see morrigan.log.MAbstractLogger#debug(java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  @Override
  public void debug(String className, String methodName, String text) {
    final CallerInfo callerInfo = createCallerInfo(new Exception(),
        INDEX_CALLER_STD);
    log(LEVEL.DEBUG, callerInfo, text, null);
  }

  /**
   * @see morrigan.log.MAbstractLogger#error(java.lang.String, java.lang.String,
   *      java.lang.String, java.lang.Exception)
   */
  @Override
  public void error(String className, String methodName, String text,
      Exception e) {
    log(LEVEL.ERROR, createCallerInfo(e, INDEX_CALLER_ERROR), text, e);
  }

  /**
   * @see morrigan.log.MAbstractLogger#error(java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  @Override
  public void error(String className, String methodName, String text) {
    log(LEVEL.ERROR, createCallerInfo(new Exception(), INDEX_CALLER_STD), text,
        null);
  }

  /**
   * @see morrigan.log.MAbstractLogger#infos(java.lang.String)
   */
  @Override
  public void infos(String text) {
    final CallerInfo callerInfo = createCallerInfo(new Exception(),
        INDEX_CALLER_STD);
    log(LEVEL.INFO, callerInfo, text, null);
  }

  private enum LEVEL {
    INFO,
    DEBUG,
    ERROR;
  }

  private void log(LEVEL level, CallerInfo callerInfo, String message,
      Throwable throwable) {
    log(level, callerInfo.getClazz(), callerInfo.getMethod(), callerInfo
        .getLine(), message, throwable);
  }

  private synchronized void log(LEVEL level, String clazz, String method,
      int line, String message, Throwable throwable) {

    if (xml) {} else {
      StringBuffer buffer = new StringBuffer("[" + level.toString() + "]");
      buffer.append('\t');
      buffer.append("[" + clazz + " - " + method + " - (" + line + ")]");

      switch (level) {
        case INFO:
        case DEBUG:
          buffer.append('[').append(message).append(']');
          System.out.println(buffer.toString());
          break;
        case ERROR:
          buffer.append('[').append(message).append("]\n");
          if (throwable != null) {
            // buffer.append('[').append(throwable.getMessage()).append("]\n");
            buffer.append("[Stacktrace : \n");
            for (StackTraceElement element : throwable.getStackTrace()) {
              buffer.append('\t').append(element).append('\n');
            }
            buffer.setLength(buffer.length() - 1);
            buffer.append(" ]");
          }
          System.out.println(buffer.toString());
          break;
        default:

          break;
      }
    }
  }

  private synchronized static CallerInfo createCallerInfo(Throwable throwable,
      int callerIndex) {
    StackTraceElement stackTraceElement = throwable.getStackTrace()[callerIndex];
    String clazz = stackTraceElement.getClassName();
    String method = stackTraceElement.getMethodName();
    int line = stackTraceElement.getLineNumber();
    return new CallerInfo(clazz, method, line);
  }

  static private class CallerInfo {

    private final String clazz;

    private final String method;

    private final int line;

    public CallerInfo(String clazz, String method, int line) {
      this.clazz = clazz;
      this.method = method;
      this.line = line;
    }

    public String getClazz() {
      return this.clazz;
    }

    public int getLine() {
      return this.line;
    }

    public String getMethod() {
      return this.method;
    }
  }
}
