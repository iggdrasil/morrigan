/*
 * MLazyLogger.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

/**
 * @author Armel
 */
public class MLazyLogger implements MLogger {

  /**
   * Default constructor.
   */
  public MLazyLogger() {
    super();
  }

  /** {@inheritDoc} */
  public void debug(String className, String methodName, String text) {
    if ((getLogLevel() & MLogLevel.DEBUG) == MLogLevel.DEBUG) {
      write("DEBUG\n  . " + "[" + className + " - " + methodName + "] " + text);
    }
  }

  /** {@inheritDoc} */
  public void error(String className, String methodName, String text) {
    error("[" + className + " - " + methodName + "] " + text);
  }

  /** {@inheritDoc} */
  public void error(String className, String methodName, String text,
      Exception e) {
    error(className, methodName, text + " : " + e.toString());
    e.printStackTrace();
  }

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param text erreur a ecrire
   */
  private void error(String text) {
    if ((getLogLevel() & MLogLevel.ERROR) == MLogLevel.ERROR) {
      System.err.println("ERROR\n  . " + text);
    }
  }

  /**
   * @see morrigan.log.MLogger#infos(java.lang.String)
   */
  public void infos(String text) {
    if ((getLogLevel() & MLogLevel.INFO) == MLogLevel.INFO) {
      write("INFOS\n  . " + text);
    }
  }

  /** {@inheritDoc} */
  public void clore() {}

  /** {@inheritDoc} */
  public int getLogLevel() {
    return MLogLevel.ALL;
  }

  /** {@inheritDoc} */
  public void setLogLevel(int logLevel) {}

  /** {@inheritDoc} */
  public void setLogPanel(MLogPanel logPanel) {}

  /**
   * Ecrire le texte dans le fichier.
   * 
   * @param text texte a logger
   */
  private void write(String text) {
    System.out.println(text);
  }
}
