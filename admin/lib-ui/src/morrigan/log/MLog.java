/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MLog.java - CREATION : 2004/04/03
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.log;

import org.apache.commons.lang.Validate;

/**
 * Cette classe permet de gerer les logs. L'unique instance ouvre un fichier et
 * ecrit les informations fournies. Elle peut aussi afficher sur la sortie
 * standard. Les logs sont de 3 types: informations (INFOS), erreur (ERROR),
 * debug (DEBUG). Ceci permet de differencier et de n'extraire que les
 * informations utiles. De plus, les fichiers sont regulierement notes par une
 * heure.
 * 
 * @author armel
 */
public final class MLog {

  private static MLogger logger = new MLazyLogger();

  /**
   * Constructeur d'un MLog.
   */
  private MLog() {}

  /**
   * Lancer le systeme de log pour un ou des mode(s) specifiques(s), en
   * precisant si l'affichage doit aussi se faire sur la console.
   * 
   * @param sNomLog
   * @param iNiveauLog
   * @param console
   */
  public static void lancer(String sNomLog, int iNiveauLog, boolean console) {
    logger = createLogger(sNomLog, iNiveauLog, console);
  }

  /**
   * Lancer le systeme de log en precisant si l'affichage doit aussi se faire
   * sur la console.
   * 
   * @param sNomLog
   * @param console
   */
  public static void lancer(String sNomLog, boolean console) {
    logger = createLogger(sNomLog, MLogLevel.ALL, console);
  }

  /**
   * Lancer le systeme de log pour un ou des mode(s) specifiques(s). L'affichage
   * se fera aussi sur la console.
   * 
   * @param sNomLog
   * @param iNiveauLog
   */
  public static void lancer(String sNomLog, int iNiveauLog) {
    logger = createLogger(sNomLog, iNiveauLog, true);
  }

  /**
   * Lancer le systeme de log pour tous les modes (ALL). L'affichage se fera
   * aussi sur la console.
   * 
   * @param sNomLog
   */
  public static void lancer(String sNomLog) {
    logger = createLogger(sNomLog, MLogLevel.ALL, true);
  }

  private static MLogger createLogger(String sNomLog, int iNiveauLog,
      boolean bConsole) {
    Validate.notEmpty(sNomLog, "No log file name specified !");
    return new MSimpleLogger(sNomLog, iNiveauLog, bConsole);
  }

  public static void setPanelLog(MLogPanel logPanel) {
    logger.setLogPanel(logPanel);
  }

  /**
   * Ecrire des informations dans le fichier de log.
   * 
   * @param text texte a ecrire
   */
  public static void infos(String text) {
    logger.infos(text);
  }

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param sClassName nom de la classe ou s'est produite l'erreur
   * @param methodName nom de la methode ou s'est produite l'erreur
   * @param text texte de l'erreur
   */
  public static void error(String sClassName, String methodName, String text) {
    logger.error(sClassName, methodName, text);
  }

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param sClassName nom de la classe ou s'est produite l'erreur
   * @param methodName nom de la methode ou s'est produite l'erreur
   * @param text texte expliquant l'erreur
   * @param e erreur
   */
  public static void error(String sClassName, String methodName, String text,
      Exception e) {
    logger.error(sClassName, methodName, text, e);
  }

  /**
   * Ecrire une information de debuggage dans le fichier de log.
   * 
   * @param sClassName nom de la classe a debugger
   * @param methodName nom de la methode a debugger
   * @param text texte expliquant le debuggage
   */
  public static void debug(String sClassName, String methodName, String text) {
    logger.debug(sClassName, methodName, text);
  }

  /**
   * Fermer le fichier de log.
   */
  public static void clore() {
    logger.clore();
  }

  /**
   * Definir le niveau de log.
   * 
   * @param iLogLevel
   */
  public static void setLogLevel(int iLogLevel) {
    logger.setLogLevel(iLogLevel);
  }

  public static int getLogLevel() {
    return logger.getLogLevel();
  }

}