/*
 * MLogger.java
 * 3 juil. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

/**
 * @author Armel
 */
public interface MLogger {

  public void setLogPanel(MLogPanel logPanel);

  /**
   * Ecrire des informations dans le fichier de log.
   * 
   * @param text texte a ecrire
   */
  public void infos(String text);

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param sClassName nom de la classe ou s'est produite l'erreur
   * @param methodName nom de la methode ou s'est produite l'erreur
   * @param text texte de l'erreur
   */
  public void error(String sClassName, String methodName, String text);

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param sClassName nom de la classe ou s'est produite l'erreur
   * @param methodName nom de la methode ou s'est produite l'erreur
   * @param text texte expliquant l'erreur
   * @param e erreur
   */
  public void error(String sClassName, String methodName, String text,
      Exception e);

  /**
   * Ecrire une information de debuggage dans le fichier de log.
   * 
   * @param sClassName nom de la classe a debugger
   * @param methodName nom de la methode a debugger
   * @param text texte expliquant le debuggage
   */
  public void debug(String sClassName, String methodName, String text);

  /**
   * Fermer le fichier de log.
   */
  public void clore();

  /**
   * Definir le niveau de log.
   * 
   * @param iLogLevel
   */
  public void setLogLevel(int iLogLevel);

  /**
   * @return the log level
   */
  public int getLogLevel();
}
