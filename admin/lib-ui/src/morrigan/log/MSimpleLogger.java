/*
 * MSimpleLogger.java
 * 3 juil. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Calendar;

/**
 * @author Armel
 */
public class MSimpleLogger extends MAbstractLogger {

  private FileOutputStream fosMini;

  private int lastLogLevel = MLogLevel.NONE;

  /**
   * Delai entre 2 affichages d'heures (en millisecondes).
   */
  private final long lDelai = 60000;

  private long lastModifDate = Calendar.getInstance().getTimeInMillis()
      - lDelai;

  protected MSimpleLogger(String logFileName, int logLevel, boolean console) {
    super(logFileName, logLevel, console);
    write("\n\n********************\n");
  }

  /** {@inheritDoc} */
  @Override
  public void debug(String className, String methodName, String text) {
    if ((getLogLevel() & MLogLevel.DEBUG) == MLogLevel.DEBUG) {
      write((lastLogLevel == MLogLevel.DEBUG ? "  . " : "DEBUG\n  . ") + "["
          + className + " - " + methodName + "] " + text);
      lastLogLevel = MLogLevel.DEBUG;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void error(String className, String methodName, String text) {
    error("[" + className + " - " + methodName + "] " + text);
  }

  /** {@inheritDoc} */
  @Override
  public void error(String className, String methodName, String text,
      Exception e) {
    error(className, methodName, text + " : " + e.toString());
    if (getStream() != null) {
      e.printStackTrace(new PrintStream(getStream()));
    } else {
      e.printStackTrace();
    }
    
    if (getLogPanel() != null) {
      StringBuffer sb = new StringBuffer();
      StackTraceElement[] stackTrace = e.getStackTrace();
      for (StackTraceElement stackElement : stackTrace) {
        sb.append(stackElement.toString()).append('\n');
      }
      getLogPanel().append(sb.toString());
    }

  }

  /**
   * Ecrire une erreur dans le fichier de log.
   * 
   * @param text erreur a ecrire
   */
  private void error(String text) {
    if ((getLogLevel() & MLogLevel.ERROR) == MLogLevel.ERROR) {
      write((lastLogLevel == MLogLevel.ERROR ? "  . " : "ERROR\n  . ") + text);
      lastLogLevel = MLogLevel.ERROR;
    }
  }

  /**
   * @see morrigan.log.MLogger#infos(java.lang.String)
   */
  @Override
  public void infos(String text) {
    if ((getLogLevel() & MLogLevel.INFO) == MLogLevel.INFO) {
      write((lastLogLevel == MLogLevel.INFO ? "  . " : "INFOS\n  . ") + text);
    }
    lastLogLevel = MLogLevel.INFO;

  }

  /**
   * Retourner le flux ou il faut ecrire.
   * 
   * @return flux d'ecriture
   */
  private FileOutputStream getStream() {
    return fosMini;
  }

  /**
   * Ecrire le texte dans le fichier.
   * 
   * @param text texte a logger
   */
  private void write(String text) {
    try {
      String sTexteAEcrire = text;

      if (sTexteAEcrire == null) {
        sTexteAEcrire = "";
      }

      long lNow = Calendar.getInstance().getTimeInMillis();

      if (lastModifDate < lNow - lDelai) {
        lastModifDate = lNow;
        sTexteAEcrire = getLogHour() + "\n" + sTexteAEcrire;
      }

      if (getLogPanel() != null) {
        getLogPanel().append(sTexteAEcrire + "\n");
      }

      if (isConsole()) {
        System.out.println(sTexteAEcrire);
      }

      if (fosMini != null) {
        fosMini.write((sTexteAEcrire + "\n").getBytes());
      }

    } catch (Exception e) {
      System.err.println("[" + this.getClass().getName()
          + "] [write] Erreur : " + e);
    }
  }
}
