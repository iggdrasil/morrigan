/*
 * MAbstractLogger.java
 * 3 juil. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

/**
 * @author Armel
 */
public abstract class MAbstractLogger implements MLogger {

  private String logFileName;

  private int logLevel; // = MLogLevel.ALL

  private boolean console;

  private MLogPanel mLogPanel = null;

  private FileOutputStream fosMini = null;

  private MLogPanel logPanel = null;

  /**
   * Constructor.
   */
  protected MAbstractLogger(String logFileName, int logLevel, boolean console) {
    this.logFileName = logFileName;
    this.logLevel = logLevel;
    this.console = console;

    try {
      // TODO: parser et creer les repertoires.
      File fDir = new File("log");
      if (!fDir.exists()) {
        fDir.mkdir();
      }
      fosMini = new FileOutputStream("log" + File.separator + logFileName + "-"
          + getLogFileDate(), true);
      if (fosMini == null) {
        throw new Exception("Impossible d'ouvrir le fichier de log:"
            + logFileName);
      }
    } catch (Exception e) {
      System.err.println("[" + MLog.class + "] [lancer] Erreur : " + e);
      e.printStackTrace();
    }

    // write("\n\n********************\n");
  }

  /**
   * @see morrigan.log.MLogger#clore()
   */
  public void clore() {
    try {
      if (fosMini != null) {
        fosMini.close();
      }
    } catch (Exception e) {
      System.err.println("[" + MLog.class + "] [close] Erreur : " + e);
    }
  }

  /**
   * @see morrigan.log.MLogger#getLogLevel()
   */
  public int getLogLevel() {
    return logLevel;
  }

  /**
   * @see morrigan.log.MLogger#setLogLevel(int)
   */
  public void setLogLevel(int logLevel) {
    this.logLevel = logLevel;
  }

  /**
   * @see morrigan.log.MLogger#setLogPanel(morrigan.log.MLogPanel)
   */
  public void setLogPanel(MLogPanel logPanel) {
    this.logPanel = logPanel;
  }

  /**
   * @return the logPanel
   */
  public MLogPanel getLogPanel() {
    return this.logPanel;
  }

  /**
   * @return the console
   */
  public boolean isConsole() {
    return this.console;
  }

  /**
   * @param console the console to set
   */
  public void setConsole(boolean console) {
    this.console = console;
  }

  /**
   * @see morrigan.log.MLogger#infos(java.lang.String)
   */
  public abstract void infos(String text);

  /**
   * @see morrigan.log.MLogger#debug(java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  public abstract void debug(String className, String methodName, String text);

  /**
   * @see morrigan.log.MLogger#error(java.lang.String, java.lang.String,
   *      java.lang.String, java.lang.Exception)
   */
  public abstract void error(String className, String methodName, String text,
      Exception e);

  /**
   * @see morrigan.log.MLogger#error(java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  public abstract void error(String className, String methodName, String text);

  /**
   * Retourner la date dans le format des logs.
   * 
   * @return chaine de caracteres representant la date
   */
  protected String getLogFileDate() {
    Calendar cal = Calendar.getInstance();
    int mois = cal.get(Calendar.MONTH) + 1;
    int jour = cal.get(Calendar.DAY_OF_MONTH);
    return cal.get(Calendar.YEAR) + "-" + (mois < 10 ? "0" + mois : "" + mois)
        + "-" + (jour < 10 ? "0" + jour : "" + jour);
  }

  /**
   * Retourner l'heure actuelle dans le format des logs.
   * 
   * @return chaine de caracteres representant l'heure.
   */
  protected String getLogHour() {
    Calendar cal = Calendar.getInstance();
    int heure = cal.get(Calendar.HOUR_OF_DAY);
    int min = cal.get(Calendar.MINUTE);
    int sec = cal.get(Calendar.SECOND);
    return "[" + (heure < 10 ? "0" + heure : "" + heure) + ":"
        + (min < 10 ? "0" + min : "" + min) + ":"
        + (sec < 10 ? "0" + sec : "" + sec) + "]";
  }

}
