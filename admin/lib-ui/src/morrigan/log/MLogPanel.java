/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MLogPanel.java - CREATION : 2004/08/01
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.log;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.field.util.MTextArea;
import morrigan.graphique.menu.MJCheckBoxMenuItem;
import morrigan.graphique.menu.MJMenuItem;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;

/**
 * Ce panel permet d'afficher les logs et de gerer le systeme de log. On peut
 * ainsi specifier le type de message que l'on souhaite voir apparaitre.
 * 
 * @author armel
 */
public class MLogPanel extends MJPanelQuad implements MouseListener,
    ActionListener {
  private MTextArea mjtaLog = null;

  private MJPopupMenu popup = null;

  private MJMenuItem miClear = null;

  private MJCheckBoxMenuItem mcbAfficherInfos = null;

  private MJCheckBoxMenuItem mcbAfficherErreurs = null;

  private MJCheckBoxMenuItem mcbAfficherDebug = null;

  public MLogPanel() {
    super(1, 1);
    preparer();
    MLog.setPanelLog(this);
  }

  private void preparer() {
    mjtaLog = new MTextArea();
    mjtaLog.setEditable(false);
    MJScrollPane mjScrollPane = new MJScrollPane(mjtaLog);
    creerPopup();

    mjtaLog.addMouseListener(this);

    add(mjScrollPane, 0, 0);
  }

  public void append(String sChaine) {
    mjtaLog.append(sChaine);
    // Repositionnement a la fin du texte.
    mjtaLog.setCaretPosition(mjtaLog.getText().length());
  }

  private void creerPopup() {
    popup = new MJPopupMenu();
    miClear = new MJMenuItem("Nettoyer", KeyEvent.VK_N);
    mcbAfficherInfos = new MJCheckBoxMenuItem("Afficher Infos", KeyEvent.VK_I,
        true);
    mcbAfficherErreurs = new MJCheckBoxMenuItem("Afficher Erreurs",
        KeyEvent.VK_E, true);
    mcbAfficherDebug = new MJCheckBoxMenuItem("Afficher Debug", KeyEvent.VK_D,
        true);

    miClear.addActionListener(this);
    mcbAfficherInfos.addActionListener(this);
    mcbAfficherErreurs.addActionListener(this);
    mcbAfficherDebug.addActionListener(this);

    popup.add(miClear);
    popup.addSeparator();
    popup.add(mcbAfficherInfos);
    popup.add(mcbAfficherErreurs);
    popup.add(mcbAfficherDebug);
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == mcbAfficherInfos | e.getSource() == mcbAfficherErreurs
        | e.getSource() == mcbAfficherDebug) {
      int iLog = mcbAfficherInfos.isSelected() ? MLogLevel.INFO : 0;
      iLog |= (mcbAfficherErreurs.isSelected() ? MLogLevel.ERROR : 0);
      iLog |= (mcbAfficherDebug.isSelected() ? MLogLevel.DEBUG : 0);

      MLog.setLogLevel(iLog);
    } else if (e.getSource() == miClear) {
      mjtaLog.setText("");
    }
  }

  @SuppressWarnings("unused")
  public void mouseClicked(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  public void mousePressed(MouseEvent e) {
    if (e.isPopupTrigger()
        || ((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) == InputEvent.BUTTON3_DOWN_MASK)) {
      popup.show(e.getComponent(), e.getX(), e.getY());
    }
  }

  @SuppressWarnings("unused")
  public void mouseReleased(MouseEvent e) {}
}