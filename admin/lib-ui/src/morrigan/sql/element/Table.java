package morrigan.sql.element;

import org.apache.commons.lang.Validate;

public class Table {

  private final String name;

  private String alias;

  public Table(String name) {
    Validate.notNull(name, "The table must have a name");
    this.name = name;
    // By default, alias <= name
    this.alias = name;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getAlias() {
    return alias;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (this == obj) {
      return true;
    }

    if (!this.getClass().equals(obj.getClass())) {
      return false;
    }

    Table other = (Table) obj;
    return this.getName().equals(other.getName());
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

}
