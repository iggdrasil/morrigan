package morrigan.sql.element;

public class Field {

  private final String name;

  private final Table table;

  private String alias;

  public Field(String name, Table table) {
    this.name = name;
    // By default, alias <= name
    this.alias = name;
    this.table = table;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @return the table
   */
  public Table getTable() {
    return table;
  }
}
