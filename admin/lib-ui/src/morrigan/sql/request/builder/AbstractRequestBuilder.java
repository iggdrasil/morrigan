package morrigan.sql.request.builder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;
import morrigan.sql.request.TableSelection;

public abstract class AbstractRequestBuilder implements RequestBuilder {

  private char aliasTableIndex = 'a';

  private int aliasFieldIndex = 1;

  public AbstractRequestBuilder() {}

  abstract String buildSqlRequest(Request request);

  public final String build(Request request) {
    prepareAlias(request.getTableSelections());
    return buildSqlRequest(request);
  }

  /**
   * Prepare the fields and tables alias.
   * 
   * @param tableSelections
   */
  private void prepareAlias(List<TableSelection> tableSelections) {
    aliasTableIndex = 'a';
    Set<String> tablesToAlias = new HashSet<String>();

    // If a table is used several times, it must be alias'd
    Set<String> inspectedTables = new HashSet<String>();
    for (TableSelection tableSelection : tableSelections) {
      Table table = tableSelection.getTable();
      String name = table.getName();
      if (inspectedTables.contains(name)) {
        tablesToAlias.add(name);
      }
      inspectedTables.add(name);
    }
    inspectedTables = null;

    for (TableSelection tableSelection : tableSelections) {
      Table table = tableSelection.getTable();
      // Alias tables
      if (tablesToAlias.contains(table.getName())) {
        char alias = aliasTableIndex++;
        table.setAlias("".intern() + alias);
        // Alias their fields
        for (Field field : tableSelection.getFields()) {
          field.setAlias(field.getName() + (aliasFieldIndex++));
        }
      }
    }
  }

  protected String buildFieldWithAlias(Field field) {
    StringBuffer select = new StringBuffer();
    select.append(field.getTable().getAlias());
    select.append('.');
    select.append(field.getName());

    // Alias is used only if different from the name
    if (!field.getName().equals(field.getAlias())) {
      // PostgreSQL, not Oracle : AS
      if (false) {
        select.append(' ');
      } else {
        select.append(_AS_);
      }
      select.append(field.getAlias());
    }

    return select.toString();
  }

  protected String buildTableWithAlias(Table table) {
    StringBuffer from = new StringBuffer();
    from.append(table.getName());
    // PostgreSQL, not Oracle : AS
    if (true) {
      from.append(' ');
    } else {
      from.append(_AS_);
    }
    from.append(table.getAlias());
    return from.toString();
  }
}
