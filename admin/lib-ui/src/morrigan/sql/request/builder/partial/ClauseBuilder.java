package morrigan.sql.request.builder.partial;

import morrigan.sql.request.clause.Clause;

public interface ClauseBuilder {

  final String _LIKE_ = " LIKE ";

  final String _DIFFERENT_ = " <> ";

  final String _SUPERIOR_ = " > ";

  final String _INFERIOR_ = " < ";

  final String _EQUAL_ = " = ";

  public StringBuffer toSql(Clause clause);

}