package morrigan.sql.request.builder.partial;

import morrigan.sql.request.clause.Clause;

public class DefaultClauseBuilder implements ClauseBuilder {

  public DefaultClauseBuilder() {}

  /** {@inheritDoc} */
  public StringBuffer toSql(Clause clause) {
    StringBuffer clauseBuffer = new StringBuffer();
    clauseBuffer.append(clause.getTable().getAlias());
    clauseBuffer.append('.');
    clauseBuffer.append(clause.getTableField().getName());
    switch (clause.getOperator()) {
      case EQUAL:
        clauseBuffer.append(_EQUAL_);
        break;
      case INFERIOR:
        clauseBuffer.append(_INFERIOR_);
        break;
      case SUPERIOR:
        clauseBuffer.append(_SUPERIOR_);
        break;
      case DIFFERENT:
        clauseBuffer.append(_DIFFERENT_);
        break;
      case LIKE:
        clauseBuffer.append(_LIKE_);
        break;
    }
    clauseBuffer.append('\'').append(clause.getComparator()).append('\'');

    return clauseBuffer;
  }
}
