package morrigan.sql.request.builder;

/**
 * A request builder that explicit the Request Object into an SQL request.
 * 
 * @author armel
 */
public interface RequestBuilder {

  final String SELECT_ = "SELECT ";

  final String _FROM_ = " FROM ";

  final String _AS_ = " AS ";

  final String _ON_ = " ON ";

  final String _INNER_JOIN_ = " INNER JOIN ";

  final String _LEFT_OUTER_JOIN_ = " LEFT OUTER JOIN ";

  final String _WHERE_ = " WHERE ";

  // Clauses
  final String _AND_ = " AND ";

  final String _OR_ = " OR ";

  /**
   * Build the SQL request according to a Request.
   * 
   * @param request the original request
   * @return an SQL request
   */
  public String build(Request request);

}