package morrigan.sql.request.builder;

import java.util.List;

import morrigan.sql.element.Field;
import morrigan.sql.request.TableSelection;
import morrigan.sql.request.builder.partial.ClauseBuilder;
import morrigan.sql.request.builder.partial.DefaultClauseBuilder;
import morrigan.sql.request.clause.Clause;
import morrigan.sql.request.join.Join;

public class DefaultRequestBuilder extends AbstractRequestBuilder {

  public DefaultRequestBuilder() {}

  @Override
  public String buildSqlRequest(Request request) {
    List<TableSelection> tableSelections = request.getTableSelections();
    List<Join> joins = request.getJoins();
    List<Clause> clauses = request.getClauses();

    StringBuffer select = new StringBuffer();
    StringBuffer from = new StringBuffer();
    StringBuffer whereJointure = new StringBuffer();
    StringBuffer whereClause = new StringBuffer();

    for (TableSelection tableSelection : tableSelections) {
      for (Field field : tableSelection.getFields()) {
        select.append(buildFieldWithAlias(field));
        select.append(", ".intern());
      }

      from.append(buildTableWithAlias(tableSelection.getTable()));
      from.append(", ".intern());
    }
    if (!tableSelections.isEmpty()) {
      select.setLength(select.length() - 2);
      from.setLength(from.length() - 2);
    }

    for (Join join : joins) {
      whereJointure.append(join.getFirstTable().getAlias());
      whereJointure.append('.');
      whereJointure.append(join.getFirstTableField().getName());
      whereJointure.append('=');
      whereJointure.append(join.getSecondTable().getAlias());
      whereJointure.append('.');
      whereJointure.append(join.getSecondTableField().getName());

      whereJointure.append(_AND_);
    }
    if (!joins.isEmpty()) {
      whereJointure.setLength(whereJointure.length() - 5);
    }

    ClauseBuilder defaultClauseBuilder = new DefaultClauseBuilder();

    for (Clause clause : clauses) {
      whereClause.append(defaultClauseBuilder.toSql(clause));
      whereClause.append(_AND_);
    }
    if (!clauses.isEmpty()) {
      whereClause.setLength(whereClause.length() - _AND_.length());
    }

    StringBuffer buffer = new StringBuffer();
    buffer.append(SELECT_);
    buffer.append(select.toString());
    buffer.append(_FROM_);
    buffer.append(from.toString());
    if (!joins.isEmpty() || !clauses.isEmpty()) {
      buffer.append(_WHERE_);
    }
    if (!joins.isEmpty()) {
      buffer.append(whereJointure.toString());
    }
    if (!clauses.isEmpty()) {
      buffer.append(whereClause.toString());
    }

    return buffer.toString();
  }
}
