package morrigan.sql.request.builder;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.Validate;

import morrigan.sql.element.Table;
import morrigan.sql.request.TableSelection;
import morrigan.sql.request.clause.Clause;
import morrigan.sql.request.join.InnerJoin;
import morrigan.sql.request.join.Join;
import morrigan.sql.request.join.OuterJoin;

public class ExplicitJoinRequestBuilder extends PartedRequestBuilder {

  @Override
  public String buildSqlRequest(Request request) {
    List<TableSelection> tableSelections = request.getTableSelections();
    List<Join> joins = request.getJoins();
    List<Clause> clauses = request.getClauses();

    StringBuffer select = buidSelect(tableSelections);
    StringBuffer from = joins.isEmpty() ? buildFrom(tableSelections)
        : buildFrom(joins);
    StringBuffer clauseBuffer = buidClause(clauses);

    StringBuffer buffer = new StringBuffer(SELECT_).append(select.toString())
        .append(_FROM_).append(from.toString());

    if (!clauses.isEmpty()) {
      buffer.append(_WHERE_);
    }

    if (clauseBuffer.length() > 0) {
      buffer.append(clauseBuffer.toString());
    }

    return buffer.toString();
  }

  /**
   * Build an SQL join :
   * 
   * <pre>
   * personnage A LEFT JOIN joueur B ON A.prs_jou_id = B.jou_id
   * </pre>
   * 
   * @param joins
   * @return
   */
  protected StringBuffer buildFrom(List<Join> joins) {

    JoinComparator joinComparator = new JoinComparator();
    Collections.sort(joins, joinComparator);

    StringBuffer from = new StringBuffer();

    boolean firstTable = true;
    Set<Table> joinedTables = new HashSet<Table>();
    for (Join join : joins) {
      // ie: personnage A LEFT JOIN joueur B
      if (firstTable) {
        from.append(buildTableWithAlias(join.getFirstTable()));
        firstTable = false;
        joinedTables.add(join.getFirstTable());
      }
      // If the first table is not already mentionned in the FROM part, it means
      // the first table is a new one : the join must be inverted.
      if (!joinedTables.contains(join.getFirstTable())) {
        // And if the second is also a new one : error
        Validate.isTrue(joinedTables.contains(join.getSecondTable()),
            "The joins are not in the good order");
        join = invert(join);
      }
      joinedTables.add(join.getSecondTable());

      from.append(join.isOuterJoin() ? _LEFT_OUTER_JOIN_ : _INNER_JOIN_);
      from.append(buildTableWithAlias(join.getSecondTable()));

      // ie: ON A.prs_jou_id = B.jou_id
      from.append(_ON_);
      from.append(join.getFirstTable().getAlias());
      from.append('.');
      from.append(join.getFirstTableField().getName());
      from.append(" = ".intern());
      from.append(join.getSecondTable().getAlias());
      from.append('.');
      from.append(join.getSecondTableField().getName());
    }

    return from;
  }

  /**
   * Invert join.
   * 
   * @param join
   * @return
   */
  private Join invert(Join join) {
    if (join instanceof OuterJoin) {
      return new OuterJoin(join.getSecondTableField(), join
          .getFirstTableField());
    }
    return new InnerJoin(join.getSecondTableField(), join.getFirstTableField());
  }

  @Override
  protected StringBuffer buildJoins(Collection<Join> joins) {
    return new StringBuffer();
  }

  /**
   * A join comparator. It looks at the alias of the first table of the join.
   * 
   * @author alagadic
   */
  private static class JoinComparator implements Comparator<Join> {
    public JoinComparator() {}

    public int compare(Join join1, Join join2) {
      if (join1 == join2) {
        return 0;
      }
      if (join1 == null) {
        return -1;
      }

      return join1.getFirstTable().getAlias().compareTo(
          join2.getFirstTable().getAlias());
    }
  }

}
