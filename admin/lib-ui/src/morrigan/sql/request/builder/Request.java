package morrigan.sql.request.builder;

import java.util.ArrayList;
import java.util.List;

import morrigan.sql.element.Table;
import morrigan.sql.request.TableSelection;
import morrigan.sql.request.clause.Clause;
import morrigan.sql.request.join.Join;

/**
 * SQL information model
 * 
 * @author alagadic
 */
public class Request {

  private List<TableSelection> tableSelections = new ArrayList<TableSelection>();

  private List<Table> tables = new ArrayList<Table>();

  private List<Join> joins = new ArrayList<Join>();

  private List<Clause> clauses = new ArrayList<Clause>();

  public Request() {}

  public void addSelection(TableSelection selection) {
    tableSelections.add(selection);
    tables.add(selection.getTable());
  }

  public void addClause(Clause clause) {
    clauses.add(clause);
  }

  public void addJointure(Join join) {
    joins.add(join);
  }

  public List<TableSelection> getTableSelections() {
    return tableSelections;
  }

  public List<Table> getTables() {
    return tables;
  }

  public List<Join> getJoins() {
    return joins;
  }

  public List<Clause> getClauses() {
    return clauses;
  }
}
