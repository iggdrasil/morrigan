package morrigan.sql.request.builder;

import java.util.Collection;
import java.util.List;

import morrigan.sql.element.Field;
import morrigan.sql.request.TableSelection;
import morrigan.sql.request.builder.partial.ClauseBuilder;
import morrigan.sql.request.builder.partial.DefaultClauseBuilder;
import morrigan.sql.request.clause.Clause;
import morrigan.sql.request.join.Join;

public class PartedRequestBuilder extends AbstractRequestBuilder {

  private ClauseBuilder defaultClauseBuilder = new DefaultClauseBuilder();

  public PartedRequestBuilder() {}

  @Override
  public String buildSqlRequest(Request request) {
    List<TableSelection> tableSelections = request.getTableSelections();
    List<Join> joins = request.getJoins();
    List<Clause> clauses = request.getClauses();

    StringBuffer select = buidSelect(tableSelections);
    StringBuffer from = buildFrom(tableSelections);
    StringBuffer joinBuffer = buildJoins(joins);
    StringBuffer clauseBuffer = buidClause(clauses);

    StringBuffer buffer = new StringBuffer(SELECT_).append(select.toString())
        .append(_FROM_).append(from.toString());

    if (!joins.isEmpty() || !clauses.isEmpty()) {
      buffer.append(_WHERE_);
    }

    if (!joins.isEmpty()) {
      buffer.append(joinBuffer.toString());
    }
    if (!clauses.isEmpty()) {
      buffer.append(clauseBuffer.toString());
    }

    return buffer.toString();
  }

  /**
   * Build the SELECT section without the 'SELECT' word.
   * 
   * @param tableSelections
   * @return a SELECT buffer
   */
  protected StringBuffer buidSelect(Collection<TableSelection> tableSelections) {
    StringBuffer select = new StringBuffer();
    for (TableSelection tableSelection : tableSelections) {
      for (Field field : tableSelection.getFields()) {
        select.append(buildFieldWithAlias(field));
        select.append(", ".intern());
      }
    }
    if (!tableSelections.isEmpty()) {
      select.setLength(select.length() - ", ".intern().length());
    }
    return select;
  }

  /**
   * Build the FROM section without the 'FROM' word.
   * 
   * @param tableSelections
   * @return a FROM buffer
   */
  protected StringBuffer buildFrom(Collection<TableSelection> tableSelections) {
    StringBuffer from = new StringBuffer();
    for (TableSelection tableSelection : tableSelections) {
      from.append(buildTableWithAlias(tableSelection.getTable()));
      from.append(", ".intern());
    }
    if (!tableSelections.isEmpty()) {
      from.setLength(from.length() - ", ".intern().length());
    }
    return from;
  }

  /**
   * Build the joins section without the 'FROM' or 'WHERE' words.
   * 
   * @param joins
   * @return a joins buffer
   */
  protected StringBuffer buildJoins(Collection<Join> joins) {

    StringBuffer whereJoin = new StringBuffer();
    for (Join join : joins) {
      whereJoin.append(join.getFirstTable().getAlias());
      whereJoin.append('.');
      whereJoin.append(join.getFirstTableField().getName());
      whereJoin.append('=');
      whereJoin.append(join.getSecondTable().getAlias());
      whereJoin.append('.');
      whereJoin.append(join.getSecondTableField().getName());

      whereJoin.append(_AND_);
    }
    if (!joins.isEmpty()) {
      whereJoin.setLength(whereJoin.length() - 5);
    }

    return whereJoin;
  }

  /**
   * Build the clauses section without the 'WHERE' word.
   * 
   * @param clauses
   * @return a clauses buffer
   */
  protected StringBuffer buidClause(Collection<Clause> clauses) {
    StringBuffer whereClause = new StringBuffer();
    for (Clause clause : clauses) {
      StringBuffer sqlClause = defaultClauseBuilder.toSql(clause);
      whereClause.append(sqlClause);
      whereClause.append(_AND_);
    }

    if (!clauses.isEmpty()) {
      whereClause.setLength(whereClause.length() - _AND_.length());
    }

    return whereClause;
  }
}
