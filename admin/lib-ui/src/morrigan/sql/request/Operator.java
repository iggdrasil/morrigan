/**
 * 
 */
package morrigan.sql.request;

public enum Operator {
  SUPERIOR,
  INFERIOR,
  EQUAL,
  DIFFERENT,
  LIKE;
}