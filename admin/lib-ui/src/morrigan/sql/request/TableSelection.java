package morrigan.sql.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;

public class TableSelection {

  private final Table table;

  private List<Field> fields = new ArrayList<Field>();

  public TableSelection(Table table) {
    this.table = table;
  }

  public TableSelection(Table table, List<Field> fields) {
    this(table);
    this.fields = fields;
  }

  public void addSelection(Field fieldToAdd) {
    Validate.notNull(fieldToAdd, "The new selection field must not be null");
    Validate.isTrue(fieldToAdd.getTable().equals(table), "The new field ["
        + fieldToAdd + "] does not belong to the selection definition table ["
        + table.getName() + "]");

    for (Field currentField : fields) {
      Table tableToAdd = fieldToAdd.getTable();
      Validate.isTrue(currentField.getTable().equals(tableToAdd),
          "Each field of this selection must refer to the same table "
              + "and the table " + tableToAdd.getName()
              + " is not current selection table ");
    }
    fields.add(fieldToAdd);
  }

  public Table getTable() {
    return table;
  }

  public List<Field> getFields() {
    return fields;
  }
}
