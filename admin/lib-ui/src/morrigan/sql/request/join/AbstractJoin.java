package morrigan.sql.request.join;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;

/**
 * Abstract JOIN. Can be an INNER or an OUTER JOIN.
 * 
 * @author armel
 */
public abstract class AbstractJoin implements Join {

  private final Field firstField;

  private final Field secondField;

  /**
   * Constructor with each side of the join.
   * 
   * @param firstField
   * @param secondField
   */
  public AbstractJoin(Field firstField, Field secondField) {
    this.firstField = firstField;
    this.secondField = secondField;
  }

  /** {@inheritDoc} */
  public Table getFirstTable() {
    return firstField.getTable();
  }

  /** {@inheritDoc} */
  public Field getFirstTableField() {
    return firstField;
  }

  /** {@inheritDoc} */
  public Table getSecondTable() {
    return secondField.getTable();
  }

  /** {@inheritDoc} */
  public Field getSecondTableField() {
    return secondField;
  }

}
