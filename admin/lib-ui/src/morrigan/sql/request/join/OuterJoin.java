package morrigan.sql.request.join;

import morrigan.sql.element.Field;

/**
 * The OUTER JOIN is a join between two tables; each record of the first table
 * can match a record in the second. If not, the first table record still stays.
 * 
 * <pre>
 * SELECT *
 * FROM   employee 
 *        OUTER JOIN department 
 *         ON employee.DepartmentID = department.DepartmentID
 * </pre>
 * 
 * is similar to
 * 
 * <pre>
 * SELECT *  
 * FROM   employee, department 
 * WHERE  employee.DepartmentID = department.DepartmentID
 * </pre>
 * 
 * @author alagadic
 */
public class OuterJoin extends AbstractJoin {

  /**
   * Constructor with each side of the join.
   * 
   * @param firstField
   * @param secondField
   */
  public OuterJoin(Field firstField, Field secondField) {
    super(firstField, secondField);
  }

  /** {@inheritDoc} */
  public boolean isOuterJoin() {
    return false;
  }
}
