package morrigan.sql.request.join;

import morrigan.sql.element.Field;

/**
 * The INNER JOIN is a join between two tables; each record of the first table
 * must match a record in the second. The INNER JOIN is the default join between
 * tables when no explicit join is written.
 * 
 * <pre>
 * SELECT *
 * FROM   employee 
 *        INNER JOIN department 
 *         ON employee.DepartmentID = department.DepartmentID
 * </pre>
 * 
 * is similar to
 * 
 * <pre>
 * SELECT *  
 * FROM   employee, department 
 * WHERE  employee.DepartmentID = department.DepartmentID
 * </pre>
 * 
 * @author armel
 */
public class InnerJoin extends AbstractJoin {

  /**
   * Constructor with each side of the join.
   * 
   * @param firstField
   * @param secondField
   */
  public InnerJoin(Field firstField, Field secondField) {
    super(firstField, secondField);
  }

  /** {@inheritDoc} */
  public boolean isOuterJoin() {
    return true;
  }

}
