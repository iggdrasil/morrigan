package morrigan.sql.request.join;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;

/**
 * Join between two tables. A field for each table is specified.
 * 
 * @author armel
 */
public interface Join {

  /**
   * @return the first table
   */
  Table getFirstTable();

  /**
   * @return the first table field
   */
  Field getFirstTableField();

  /**
   * @return the second table
   */
  Table getSecondTable();

  /**
   * @return the second table field
   */
  Field getSecondTableField();

  /**
   * @return <code>true</code> if the join is OUTER;<code>false</code> is
   *         INNER.
   */
  boolean isOuterJoin();
}
