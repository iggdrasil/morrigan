package morrigan.sql.request.clause;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;
import morrigan.sql.request.Operator;

public abstract class AbstractClause implements Clause {

  private final Field field;

  private final Operator operator;

  private final Object comparator;

  public AbstractClause(Field field, Operator operator, Object comparator) {
    this.field = field;
    this.operator = operator;
    this.comparator = comparator;
  }

  public Table getTable() {
    return field.getTable();
  }

  public Field getTableField() {
    return field;
  }

  /**
   * @return the field
   */
  public Field getField() {
    return field;
  }

  /**
   * @return the operator
   */
  public Operator getOperator() {
    return operator;
  }

  /**
   * @return the comparator
   */
  public Object getComparator() {
    return comparator;
  }

}
