package morrigan.sql.request.clause;

import morrigan.sql.element.Field;
import morrigan.sql.request.Operator;

public class StringClause extends AbstractClause {

  public StringClause(Field field, Operator operator, String string) {
    super(field, operator, string);
  }

  public String getStringComparator() {
    return (String) getComparator();
  }

}
