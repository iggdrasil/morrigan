package morrigan.sql.request.clause;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;
import morrigan.sql.request.Operator;

public interface Clause {

  Table getTable();

  Field getTableField();

  Operator getOperator();

  Object getComparator();

}
