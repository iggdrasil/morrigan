package morrigan.sql.request.clause;

import morrigan.sql.element.Field;
import morrigan.sql.request.Operator;

public class NumericClause extends AbstractClause {

  public NumericClause(Field field, Operator operator, Number number) {
    super(field, operator, number);
  }

  public Number getNumericComparator() {
    return (Number) getComparator();
  }

}
