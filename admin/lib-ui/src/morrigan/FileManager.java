package morrigan;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;

import morrigan.log.MLog;

public class FileManager {

  public FileManager() {}

  private InputStream loadFileInJar(String fileName)
      throws MalformedURLException, FileNotFoundException {
    return openFileToStream(fileName);
  }

  private Image loadImageInJar(String fileName) throws MalformedURLException,
      FileNotFoundException {
    return loadImage(fileName);
  }

  private ImageIcon loadIconInJar(String fileName)
      throws MalformedURLException, FileNotFoundException {
    return loadIcon(fileName);
  }

  public ImageIcon loadIcon(String relativePathImage) {
    URL urlImage = null;
    ImageIcon image = null;

    // Cas de l'image dans le jar
    if ((urlImage = this.getClass().getClassLoader().getResource(
        relativePathImage)) != null) {
      MLog.infos("JAR (image : " + relativePathImage + "; url : " + urlImage
          + ")");
      image = new ImageIcon(urlImage);
    } else {
      // Cas de l'image dans le repertoire
      MLog.infos("FICHIER (image: " + relativePathImage + ")");
      image = new ImageIcon(java.awt.Toolkit.getDefaultToolkit().getImage(
          relativePathImage));
    }

    return image;
  }

  public Image loadImage(String relativePathImage) {
    URL urlImage = null;
    Image image = null;

    // Cas de l'image dans le jar
    if ((urlImage = this.getClass().getClassLoader().getResource(
        relativePathImage)) != null) {
      MLog.infos("JAR (image: " + relativePathImage + ")");
      image = new ImageIcon(urlImage).getImage();
    } else {
      // Cas de l'image dans le repertoire
      MLog.infos("FICHIER (image: " + relativePathImage + ")");
      image = java.awt.Toolkit.getDefaultToolkit().getImage(relativePathImage);
    }

    return image;

  }

  private URL openFileToUrl(String filePath) throws MalformedURLException {
    MLog.infos("Tentative d'ouverture du FICHIER (file: " + filePath
        + ") > url");
    URL url = null;
    if ((url = this.getClass().getResource(filePath)) != null) {
      MLog.infos("JAR (file url : " + url + ")");
    } else {
      MLog.infos("FICHIER (file: " + filePath + ")");
      url = new File(filePath).toURL();
    }

    return url;
  }

  public InputStream openFileToStream(String relativePathFile)
      throws FileNotFoundException {
    MLog.infos("Tentative d'ouverture du FICHIER (file: " + relativePathFile
        + ") > stream");

    InputStream isFichier = null;
    if ((isFichier = this.getClass()
        .getResourceAsStream('/' + relativePathFile)) != null) {
      MLog.infos("Loading file in JAR");
    } else {
      isFichier = new FileInputStream(relativePathFile);
      MLog.infos("Loading file directly in FILE");
    }
    return isFichier;
  }
}
