/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MDictionnaire.java - CREATION : 2004/04/24
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.dictionnaire;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

import morrigan.FileManager;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MDictionnaire {
  private static MDictionnaire mdInstance = new MDictionnaire();

  private Properties hmDico = null;

  private MDictionnaire() {}

  /**
   * Lancer la gestion du dictionnaire: un fichier de type Properties est ouvert
   * et parcouru pour en extraire les definitions. Celui-ci ne doit pas avoir de
   * suffixe de locale ni d'extension (automtiquement .properties): un fichier
   * correspondant a la locale courante est tout d'abord recherche puis sans
   * locale precisee.
   * 
   * @param sNomFichier Nom du fichier sans le suffixe de locale ni extension.
   */
  public static void charger(String sNomFichier) throws Exception {
    InputStream isDico = null;
    try {
      isDico = new FileManager().openFileToStream(sNomFichier);
      // isDico = getFichierSelonLocale(Locale.getDefault(), sNomFichier);

      if (isDico == null) {
        MLog.error(mdInstance.getClass().toString(), "charger",
            "No dictionnary found");
        throw new Exception("Pas de dictionnaire");
      }

      mdInstance.hmDico = new Properties();
      mdInstance.hmDico.load(isDico);
      isDico.close();
    } catch (IOException e) {
      MLog.error(mdInstance.getClass().toString(), "lancer",
          "Error while loading dictionnary", e);
      throw e;
    }
  }

  private static InputStream getFichierSelonLocale(Locale locale,
      String sFichier) throws FileNotFoundException {
    final String localeSuffix = locale.getLanguage();
    String sFichierLocal = sFichier;

    if (localeSuffix.length() > 0)
      sFichierLocal += "_" + localeSuffix + ".properties";

    InputStream isFichier = ouvrirFichier(sFichierLocal);
    if (isFichier == null) {
      MLog.infos("No local dictionnary. Trying without local.");
      isFichier = ouvrirFichier(sFichier + ".properties");
    } else
      MLog.infos("Local dictionnary found.");

    return isFichier;
  }

  private static InputStream ouvrirFichier(String sNomFichier)
      throws FileNotFoundException {
    InputStream is = new FileManager().openFileToStream(sNomFichier);
    return is;
  }

  public static String getLabel(Integer idLabel) {
    return getLabel(idLabel == null ? "" : idLabel.toString());
  }

  public static String getLabel(String idLabel) {
    if (mdInstance == null || mdInstance.hmDico == null) {
      return "?";
    }

    String sLabel = (String) mdInstance.hmDico.get(idLabel);

    return sLabel != null ? sLabel : '!' + idLabel + '!';
  }

}