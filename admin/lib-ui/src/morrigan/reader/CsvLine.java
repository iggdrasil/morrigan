package morrigan.reader;

import java.util.HashMap;
import java.util.Map;

/**
 * CSV line.
 * 
 * @author Armel
 */
public class CsvLine {

  Map<String, String> map = new HashMap<String, String>();

  /**
   * Default constructor.
   */
  public CsvLine() {
    super();
  }

  /**
   * @return the map
   */
  Map<String, String> getMap() {
    return this.map;
  }

  /**
   * Retrieve the value linked to a column.
   * 
   * @param column column name
   * @return the linked value
   */
  public String getString(String column) {
    return map.get(column);
  }

  @Override
  public String toString() {
    return "[CsvLine : " + map + "]";
  }

}