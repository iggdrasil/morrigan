package morrigan.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Armel
 */
public class CsvLineParser {

  /** Column names */
  private List<CsvColumn> columns;

  /**
   * 
   */
  public CsvLineParser(String headerLine) {
    StringTokenizer stringTokenizer = getTokens(headerLine);
    int columnQty = stringTokenizer.countTokens();
    String[] lineElements = parseLine(headerLine, columnQty);
    columns = new ArrayList<CsvColumn>();
    for (int order = 0; order < lineElements.length; order++) {
      columns.add(new CsvColumn(order, lineElements[order]));
    }
  }

  private StringTokenizer getTokens(String line) {
    return new StringTokenizer(line, ";", true);
  }

  public String[] parseLine(String line) {
    return parseLine(line, columns.size());
  }

  private String[] parseLine(String line, int columnQty) {
    String[] lineElements = new String[columnQty];
    StringTokenizer stringTokenizer = new StringTokenizer(line, ";", true);

    int order = 0;
    boolean tokenEnCours = false;
    while (stringTokenizer.hasMoreElements()) {
      String token = stringTokenizer.nextToken();
      if ("".equals(token)) {
        continue;
      } else if (";".equals(token)) {
        if (tokenEnCours) {
          lineElements[order++] = null;
        }
        tokenEnCours = !tokenEnCours;

      } else {
        lineElements[order++] = token;
        tokenEnCours = false;
      }
    }
    return lineElements;
  }

  /**
   * Create a CSV line using
   * 
   * @param columns
   * @param line
   * @return
   */
  public CsvLine createCsvLine(String line) {
    final CsvLine csvLine = new CsvLine();
    String[] lineElements = parseLine(line);
    for (CsvColumn column : columns) {
      csvLine.getMap().put(column.getName(), lineElements[column.getOrder()]);
    }

    return csvLine;
  }

}
