/*
 * CsvReader.java
 * 24 avr. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * CSV file reader. The file must start with a header line specifying the column
 * names.
 * 
 * @author Armel
 */
public class CsvReader {

  /** Reader */
  private BufferedReader internalReader;

  private final CsvLineParser csvLineParser;

  public CsvReader(File csvFile, boolean header) throws IOException {
    super();
    FileReader fileReader = new FileReader(csvFile);
    internalReader = new BufferedReader(fileReader);

    // Read header and construct line parser (if any).
    String headerLine = header ? readLine() : null;
    csvLineParser = new CsvLineParser(headerLine);
  }

  /**
   * @throws IOException
   */
  public CsvReader(File csvFile) throws IOException {
    this(csvFile, true);
  }

  public CsvLine next() throws IOException {
    CsvLine entry = null;
    String line = readLine();
    if (line == null) {
      internalReader.close();
      return null;
    } else {
      line = line.trim();
      entry = csvLineParser.createCsvLine(line);
    }
    return entry;
  }

  private String readLine() throws IOException {
    String line = internalReader.readLine();
    if (line != null) {
      line = line.trim();
      // If this line is a comment, try the next one.
      if (line.length() == 0 || line.charAt(0) == '#') {
        line = readLine();
      }
    } else {
      return null;
    }

    return line;
  }
}