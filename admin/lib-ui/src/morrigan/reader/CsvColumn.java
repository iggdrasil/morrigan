package morrigan.reader;

public class CsvColumn {

  private final int order;

  private final String name;

  public CsvColumn(int order, String name) {
    this.order = order;
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public int getOrder() {
    return this.order;
  }
}