/*
 * ####################################################################### 
 * # PROJECT : Morrigan - FILE : MPreferences.java - CREATION : 2008/05/26 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com> 
 * # 
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING. 
 * #######################################################################
 */
package morrigan.preference;

import morrigan.graphique.field.MDateField.DATEFORMAT;

/**
 * Preferences singleton.
 * 
 * @author Siltaom
 */
public class MPreferences implements MDefaultPreferences {
  private String dateHourMask = DEFAULT_DATEHOUR_MASK;
  private String dateMask = DEFAULT_DATE_MASK;
  private String hourMask = DEFAULT_HOUR_MASK;

  private String dateHourFormat = DEFAULT_DATEHOUR_FORMAT;
  private String dateFormat = DEFAULT_DATE_FORMAT;
  private String hourFormat = DEFAULT_HOUR_FORMAT;

  static private MPreferences instance = new MPreferences();

  /**
   * @return the unique instance
   */
  public static MPreferences getInstance() {
    return instance;
  }

  /**
   * Hidden constructor.
   */
  private MPreferences() {
  }

  public String getFormat(DATEFORMAT date_format) {
    switch (date_format) {
    case DATE:
      return this.dateFormat;
    case DATEHOUR:
      return this.dateHourFormat;
    case HOUR:
      return this.hourFormat;
    }
    return null;
  }

  public String getMask(DATEFORMAT date_format) {
    switch (date_format) {
    case DATE:
      return this.dateMask;
    case DATEHOUR:
      return this.dateHourMask;
    case HOUR:
      return this.hourMask;
    }
    return null;
  }
  
  /**
   * @return the dateHourMask
   */
  public String getDateHourMask() {
    return dateHourMask;
  }

  /**
   * @param dateHourMask the dateHourMask to set
   */
  public void setDateHourMask(String dateHourMask) {
    this.dateHourMask = dateHourMask;
  }

  /**
   * @return the dateMask
   */
  public String getDateMask() {
    return dateMask;
  }

  /**
   * @param dateMask the dateMask to set
   */
  public void setDateMask(String dateMask) {
    this.dateMask = dateMask;
  }

  /**
   * @return the hourMask
   */
  public String getHourMask() {
    return hourMask;
  }

  /**
   * @param hourMask the hourMask to set
   */
  public void setHourMask(String hourMask) {
    this.hourMask = hourMask;
  }

  /**
   * @return the dateHourFormat
   */
  public String getDateHourFormat() {
    return dateHourFormat;
  }

  /**
   * @param dateHourFormat the dateHourFormat to set
   */
  public void setDateHourFormat(String dateHourFormat) {
    this.dateHourFormat = dateHourFormat;
  }

  /**
   * @return the dateFormat
   */
  public String getDateFormat() {
    return dateFormat;
  }

  /**
   * @param dateFormat the dateFormat to set
   */
  public void setDateFormat(String dateFormat) {
    this.dateFormat = dateFormat;
  }

  /**
   * @return the hourFormat
   */
  public String getHourFormat() {
    return hourFormat;
  }

  /**
   * @param hourFormat the hourFormat to set
   */
  public void setHourFormat(String hourFormat) {
    this.hourFormat = hourFormat;
  }
}
