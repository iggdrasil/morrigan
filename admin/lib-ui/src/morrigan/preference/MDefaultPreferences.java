/*
 * ####################################################################### 
 * # PROJECT : Morrigan - FILE : MDefaultPreferences.java - CREATION : 2009/01/22 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com> 
 * # 
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING. 
 * #######################################################################
 */
package morrigan.preference;

/**
 * @author Armel
 */
public interface MDefaultPreferences {
  
  // Date and/or hour : masks
  
  String DEFAULT_DATEHOUR_MASK = "##/##/## ##:##";
  
  String DEFAULT_DATE_MASK = "##/##/##";

  String DEFAULT_HOUR_MASK = "##:##";

  // Date and/or hour : formats
  
  String DEFAULT_DATEHOUR_FORMAT = "dd/MM/yy hh:mm";

  String DEFAULT_DATE_FORMAT = "dd/MM/yy";

  String DEFAULT_HOUR_FORMAT = "hh:mm";

}
