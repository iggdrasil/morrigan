/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJPopupMenu.java - CREATION : 2004/01/06
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

import javax.swing.JPopupMenu;

/**
 * Un <code>MJPopupMenu</code> est un menu sous forme de Popup dont l'utilite
 * est la gestion des actions. Il sert de relai entre le MJPanelActions et les
 * boutons des actions (pas les boutons des themes d'actions, a ne pas
 * confondre). A chaque fois que le popup disparait, les boutons du
 * MJPanelActions sont remis a zero. Question d'esthetique :)
 * 
 * @author armel
 */
public class MJPopupMenu extends JPopupMenu {

  /**
   * Constructeur d'un MJPopupMenu.
   */
  public MJPopupMenu() {
    super();
  }
}