/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJMenu.java - CREATION : 2004/07/27
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.menu;

import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import javax.swing.JMenu;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJMenu extends JMenu {

  public MJMenu(String sTitreMenu, int iMenmonic) {
    this(sTitreMenu);
    this.setMnemonic(iMenmonic);
  }

  public MJMenu(String sTitreMenu) {
    super(sTitreMenu);
  }

  public void add(MJMenuItem menuItem) {
    if (menuItem != null) {
      super.add(menuItem);
    }
  }

  public void add(MJMenuItem menuItem, ActionListener alElement) {
    if (alElement != null) {
      menuItem.addActionListener(alElement);
    }

    super.add(menuItem);
  }

  public void add(MJMenuItem menuItem, ItemListener ilElement) {
    if (ilElement != null) {
      menuItem.addItemListener(ilElement);
    }

    super.add(menuItem);
  }
}