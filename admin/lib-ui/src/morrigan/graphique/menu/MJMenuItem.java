/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJMenuItem.java - CREATION : 2004/01/06
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import morrigan.log.MLog;

/**
 * @author armel
 */
public class MJMenuItem extends JMenuItem implements ActionListener,
    ItemListener {

  public MJMenuItem(String sNomItem) {
    this(sNomItem, -1, null);
  }

  public MJMenuItem(String sNomItem, int iMnemonic) {
    this(sNomItem, iMnemonic, null);
  }

  public MJMenuItem(String sNomItem, ImageIcon icone) {
    this(sNomItem, -1, icone);
  }

  public MJMenuItem(String sNomItem, int iMnemonic, ImageIcon icone) {
    super(sNomItem, icone);
    if (iMnemonic >= 0) {
      setMnemonic(iMnemonic);
    }
  }

  public void actionPerformed(ActionEvent e) {
    MLog.debug("MJMenuItem", "actionPerformed", "ITEM->Action de l'item " + getText());
  }

  public void itemStateChanged(ItemEvent e) {
    MLog.debug("MJMenuItem", "itemStateChanged", "ITEM->Changement de l'item " + getText());
  }

}