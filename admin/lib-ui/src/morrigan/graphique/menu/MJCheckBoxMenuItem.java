/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJCheckBoxMenuItem.java - CREATION : 2004/08/02
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.menu;

import javax.swing.JCheckBoxMenuItem;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJCheckBoxMenuItem extends JCheckBoxMenuItem {

  public MJCheckBoxMenuItem(String sNomMenu) {
    this(sNomMenu, -1, false);
  }

  public MJCheckBoxMenuItem(String sNomMenu, boolean bSelected) {
    this(sNomMenu, -1, bSelected);
  }

  public MJCheckBoxMenuItem(String sNomMenu, int iMnemonic) {
    this(sNomMenu, iMnemonic, false);
  }

  public MJCheckBoxMenuItem(String sNomMenu, int iMnemonic, boolean bSelected) {
    super(sNomMenu, bSelected);
    setMnemonic(iMnemonic);
  }
}
