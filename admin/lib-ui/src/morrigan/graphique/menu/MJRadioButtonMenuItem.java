/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJRadioButtonMenuItem.java - CREATION : 2004/07/27
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.menu;

import javax.swing.JRadioButtonMenuItem;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJRadioButtonMenuItem extends JRadioButtonMenuItem {

  public MJRadioButtonMenuItem(String sNomMenu) {
    this(sNomMenu, -1, false);
  }

  public MJRadioButtonMenuItem(String sNomMenu, boolean bSelected) {
    this(sNomMenu, -1, bSelected);
  }

  public MJRadioButtonMenuItem(String sNomMenu, int iMnemonic) {
    this(sNomMenu, iMnemonic, false);
  }

  public MJRadioButtonMenuItem(String sNomMenu, int iMnemonic, boolean bSelected) {
    super(sNomMenu, bSelected);
    setMnemonic(iMnemonic);
  }
}