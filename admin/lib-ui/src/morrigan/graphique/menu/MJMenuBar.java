/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJMenuBar.java - CREATION : 2004/07/27
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.menu;

import javax.swing.JMenuBar;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJMenuBar extends JMenuBar {

  public MJMenuBar() {

  }
}
