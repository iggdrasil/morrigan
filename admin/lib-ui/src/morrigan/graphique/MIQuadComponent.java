/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIQuadComponent.java - CREATION : 2003/12/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

/**
 * L'interface <code>MJComponent</code> sert a manipuler des composants
 * graphiques dans un panel quadrille (<code>MJPanelQuad</code>) imposant
 * des dimensions en cellules.
 * 
 * @see morrigan.graphique.panel.MJPanelQuad
 * @author armel
 */
public interface MIQuadComponent {
  /**
   * Largeur et hauteur d'une cellule, en pixels.
   */
  public final int iTailleCell = 16;

  /**
   * Largeur d'une cellule, en pixels.
   */
  public final int iLargeurCell = 16;

  /**
   * Hauteur d'une cellule, en pixels.
   */
  public final int iHauteurCell = 27;

  /**
   * Retourne la largeur du composant en cellules.
   * 
   * @return largeur du composant en cellules
   */
  public int getLargeurCell();

  /**
   * Retourne la hauteur du composant en cellules.
   * 
   * @return hauteur du composant en cellules
   */
  public int getHauteurCell();

}
