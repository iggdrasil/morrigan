/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MModele.java - CREATION : 2004/07/15
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;

import morrigan.FileManager;
import morrigan.log.MLog;

/**
 * @author armel
 */
public class MModele {

  enum ELEMENT {
    COULEUR("couleur", "couleur.defaut", ColorUIResource.class) {

      @Override
      Object toObject(String sObject) {
        return Color.decode(sObject);
      }
    },
    POLICE("police", "police.defaut", FontUIResource.class) {

      @Override
      Object toObject(String sObject) {
        return Color.decode(sObject);
      }
    },
    NOMBRE("nombre", "nombre.defaut", BigDecimal.class) {

      @Override
      Object toObject(String sObject) {
        return new BigDecimal(sObject);
      }
    },
    TEXTE("texte", "texte.defaut", String.class) {

      @Override
      Object toObject(String sObject) {
        return sObject;
      }
    },
    URL("url", "url.defaut", URL.class) {

      @Override
      Object toObject(String sObject) throws Exception {
        return new URL(sObject);
      }
    },
    BOOLEEN("booleen", "booleen.defaut", Boolean.class) {

      @Override
      Object toObject(String sObject) {
        return new Boolean(sObject);
      }
    };

    private String sNom;

    private String sNomDefaut;

    private Class<?> cClasse;

    ELEMENT(String sNom, String sNomDefaut, Class<?> cClasse) {
      this.sNom = sNom;
      this.sNomDefaut = sNomDefaut;
      this.cClasse = cClasse;
    }

    String getNom() {
      return sNom;
    }

    String getNomDefaut() {
      return sNomDefaut;
    }

    Class<?> getClasse() {
      return cClasse;
    }

    abstract Object toObject(String sObject) throws Exception;
  }

  private static MModele mmInstance = null;

  HashMap<String, Object> hmElement = new HashMap<String, Object>();

  private static String formatDate;

  private static String formatDateHour;

  private static String formatHour;

  private MModele() {}

  static public MModele getInstance() {
    return mmInstance;
  }

  static public void chargerConfiguration(Properties prop) {
    mmInstance = new MModele();
    mmInstance.chargerProperties(prop);
  }

  static public void chargerConfiguration(String sNomFichier) {
    mmInstance = new MModele();
    mmInstance.chargerFichierConf(sNomFichier);
  }

  /**
   * Retourner la couleur associee a l'element graphique specifie. Si l'element
   * n'est pas referencee dans la liste, la couleur par defaut est alors
   * retournee. Si celle-ci n'a pas ete precisee, la methode retourne null.
   * 
   * @param sNomElement
   * @return la couleur de l'element
   */
  static public Color getCouleur(String sNomElement) {
    if (mmInstance == null)
      return null;

    if (sNomElement != null) {
      Object oCouleur = mmInstance.hmElement.get(sNomElement);
      if (oCouleur instanceof Color) {
        return (Color) mmInstance.hmElement.get(sNomElement);
      }
    }
    return (Color) mmInstance.hmElement.get("couleur.defaut");
  }

  static public Font getPolice(String sNomElement) {
    if (mmInstance == null)
      return null;

    if (sNomElement != null) {
      Object oFont = mmInstance.hmElement.get(sNomElement);
      if (oFont instanceof Font)
        return (Font) mmInstance.hmElement.get(sNomElement);
    }
    return (Font) mmInstance.hmElement.get("police.defaut");
  }

  static public Boolean getBooleen(String sNomElement) {
    if (mmInstance == null)
      return null;

    if (sNomElement != null) {
      Object oFont = mmInstance.hmElement.get(sNomElement);
      if (oFont instanceof Font)
        return (Boolean) mmInstance.hmElement.get(sNomElement);
    }
    return null;
  }

  static public BigDecimal getNombre(String sNomElement) {
    if (mmInstance == null)
      return null;

    if (sNomElement != null) {
      Object oNombre = mmInstance.hmElement.get(sNomElement);
      if (oNombre instanceof BigDecimal)
        return (BigDecimal) mmInstance.hmElement.get(sNomElement);
    }
    return null;
  }

  static public void setFormatDate(String formatDate) {
    MModele.formatDate = formatDate;
  }

  static public String getFormatDate() {
    return formatDate;
  }

  static public void setFormatDateHour(String formatDateHour) {
    MModele.formatDateHour = formatDateHour;
  }

  static public String getFormatDateHour() {
    formatDateHour = getFormatDate() + ' ' + getFormatHour();
    return formatDateHour;
  }

  static public void setFormatHour(String formatHour) {
    MModele.formatHour = formatHour;
  }

  static public String getFormatHour() {
    return formatHour;
  }

  /**
   * Charger les proprietes depuis un fichier de configuration. Celui-ci est
   * tout d'abord recherche dans un JAR puis sur le disque.
   * 
   * @param sNomFichier chemin du fichier
   */
  private void chargerFichierConf(String sNomFichier) {
    Properties prop = null;
    InputStream isConf = null;

    try {
      MLog.infos("Loading configuration file [" + sNomFichier + "]");
      isConf = new FileManager().openFileToStream(sNomFichier);
      MLog.infos("Loading configuration file - DONE");
      prop = new Properties();
      prop.load(isConf);
      isConf.close();
    } catch (Exception e1) {
      e1.printStackTrace();
      return;
    }

    chargerProperties(prop);

    try {
      isConf.close();
    } catch (IOException e) {
      MLog.error(MModele.class.toString(), "loadConfiguration ",
          "Error while closing configuration file", e);
    }
  }

  /**
   * Charger les proprietes depuis une table Properties.
   * 
   * @param prop
   */
  private void chargerProperties(Properties prop) {
    if (prop.isEmpty())
      return;

    Enumeration<?> eKeys = prop.keys();
    String sKey = null;
    do {
      sKey = (String) eKeys.nextElement();
      hmElement.put(sKey, chargerElement(prop, sKey));
      // System.out.println(sKey + " \t=> " + hmElement.get(sKey));
    } while (eKeys.hasMoreElements());
    
    appliquerModeleGraphique();
  }

  /**
   * Charger une couleur.
   * 
   * @param prop
   * @param propertyKey
   * @return l'objet associe a la cle
   * @throws NumberFormatException
   */
  private Object chargerElement(Properties prop, String propertyKey)
      throws NumberFormatException {
    ELEMENT element = null;
    if (propertyKey.indexOf("couleur") == 0)
      element = ELEMENT.COULEUR;
    else if (propertyKey.indexOf("police") == 0)
      element = ELEMENT.POLICE;
    else if (propertyKey.indexOf("texte") == 0)
      element = ELEMENT.TEXTE;
    else if (propertyKey.indexOf("nombre") == 0)
      element = ELEMENT.NOMBRE;
    else if (propertyKey.indexOf("url") == 0)
      element = ELEMENT.URL;
    else if (propertyKey.indexOf("bool") == 0)
      element = ELEMENT.BOOLEEN;

    if (element == null)
      return null;

    String sObject = prop.getProperty(propertyKey);

    if (sObject != null && !sObject.equals("")) {
      // Si un element a le nom de la valeur, on renvoie sa valeur
      // (attention, faut suivre).
      String sElement = prop.getProperty(sObject);
      if (sElement != null)
        sObject = sElement;

      try {
        return element.toObject(sObject);
      } catch (Exception e) {
        MLog.error("MModele", "chargerElement", "Error while loading element "
            + element.getNom(), e);
      }
    }

    if (element == ELEMENT.NOMBRE || element == ELEMENT.BOOLEEN) {
      return null;
    }

    return hmElement.get(element.getNomDefaut());
  }

  private void appliquerModeleGraphique() {
    appliquerModeleGraphiquePanel();
    appliquerModeleGraphiqueBouton();
    appliquerModeleGraphiqueMenu();
    appliquerModeleGraphiqueLabel();
    appliquerModeleGraphiqueText();
    appliquerModeleGraphiqueTextArea();
    appliquerModeleGraphiqueOnglet();
    appliquerModeleGraphiqueBarreDefil();
    appliquerModeleGraphiqueTable();
  }

  private void appliquerModeleGraphiquePanel() {
    UIManager.put("Panel.background", getCouleur("couleur.fond"));
  }

  private void appliquerModeleGraphiqueBouton() {
    // Parametrage des boutons
    UIManager.put("Button.font", getPolice("police.defaut"));
    UIManager.put("Button.background", getCouleur("couleur.bouton.fond"));
    UIManager.put("Button.foreground", getCouleur("couleur.bouton.inactif"));
    // Parametrage des toggle boutons (tres nul, d'ailleurs...)
    UIManager.put("ToggleButton.foreground",
        getCouleur("couleur.bouton.inactif"));
    UIManager.put("ToggleButton.background", getCouleur("couleur.bouton.fond"));

    // Empty margins
    Insets insets = new Insets(0, 0, 0, 0);
    UIManager.put("Button.margin", insets);
    UIManager.put("ToggleButton.margin", insets);
  }

  private void appliquerModeleGraphiqueMenu() {
    // Parametrage des menu items (tres nul, lui aussi...)
    UIManager.put("MenuItem.foreground", getCouleur("couleur.menu.inactif"));
    UIManager.put("MenuItem.background",
        getCouleur("couleur.menu.inactif.fond"));
    UIManager.put("MenuItem.borderPainted", getBooleen("booleen.menu.bordure"));
    // Boolean(false));
    UIManager.put("MenuItem.selectionForeground",
        getCouleur("couleur.menu.survole"));
    UIManager.put("MenuItem.selectionBackground",
        getCouleur("couleur.menu.survole.fond"));
    UIManager.put("MenuItem.disabledForeground",
        getCouleur("couleur.menu.inactif")); // MFenetre.COULEUR_MENU_INACTIF.darker());
    // UIManager.put("MenuItem.acceleratorForeground",Color.RED); ???
    // UIManager.put("MenuItem.acceleratorSelectionForeground",Color.RED); ???
  }

  private void appliquerModeleGraphiqueLabel() {
    // Parametrage des labels
    UIManager.put("Label.font", getPolice("police.defaut"));
    UIManager.put("Label.foreground", getCouleur("couleur.police"));
  }

  private void appliquerModeleGraphiqueText() {
    String[] textFieldNames = new String[] { "FormattedTextField", "PasswordField" };
    for (String f : textFieldNames) {
      UIManager.put(f + ".background",
          getCouleur("couleur.texte.fond"));
      UIManager.put(f + ".disabledBackground",
          getCouleur("couleur.menu.inactif"));
      UIManager.put(f + ".inactiveBackground",
          getCouleur("couleur.menu.inactif"));
      
      UIManager.put("TextField.background", getCouleur("couleur.texte.fond"));

      UIManager.put(f + ".font", getCouleur("police.defaut"));
      UIManager.put(f + ".foreground",
          getCouleur("couleur.texte.police"));
      UIManager.put(f + ".inactiveForeground",
          getCouleur("couleur.texte.insensible"));
    }
  }

  private void appliquerModeleGraphiqueTextArea() {
    // Parametrage des text areas
    UIManager.put("TextArea.font", getPolice("police.defaut"));
    UIManager.put("TextArea.foreground", getCouleur("couleur.texte.police"));
    UIManager.put("TextArea.background", getCouleur("couleur.texte.fond"));
    UIManager.put("TextArea.selectionForeground",
        getCouleur("couleur.texte.selection.police"));
    UIManager.put("TextArea.selectionBackground",
        getCouleur("couleur.texte.selection.fond"));
    UIManager.put("TextArea.inactiveForeground",
        getCouleur("couleur.texte.insensible"));
  }

  private void appliquerModeleGraphiqueOnglet() {
    // Parametrage des onglets
    UIManager.put("TabbedPane.font", getPolice("police.defaut"));
    UIManager.put("TabbedPane.foreground", getCouleur("couleur.onglet.police"));
    UIManager.put("TabbedPane.background", getCouleur("couleur.onglet.fond"));
    UIManager.put("TabbedPane.selected", getCouleur("couleur.onglet.actif"));
    UIManager.put("TabbedPane.focus", getCouleur("couleur.onglet.actif"));
    UIManager.put("TabbedPane.tabInsets", new Insets(0, 20, 0, 20));
    // Filet autour interieur de chaque tab
    UIManager.put("TabbedPane.light", getCouleur("couleur.onglet.actif"));
    // Filet exterieur autour de chaque tab
    UIManager.put("TabbedPane.darkShadow", getCouleur("couleur.onglet.police"));
  }

  private void appliquerModeleGraphiqueBarreDefil() {
    UIManager.put("ScrollBar.background", getCouleur("couleur.police"));
    UIManager.put("ScrollBar.thumb", getCouleur("couleur.barre"));
    UIManager.put("ScrollBar.thumbDarkShadow",
        getCouleur("couleur.barre.ombre"));
    UIManager.put("ScrollBar.thumbHighlight",
        getCouleur("couleur.barre.relief"));

    // Ombre autour de la zone dans laquelle on bouge la barre
    // UIManager.put("ScrollBar.shadow",Color.RED);
    // Relief sur la barre (haut, gauche, bas) (sombre)
    // UIManager.put("ScrollBar.thumbShadow",Color.YELLOW);
    // Relief sur la barre (haut, gauche) (clair)
    // UIManager.put("ScrollBar.thumbHighlight",Color.RED);

    // Ombre autour de tous les elements de la scrollbar (+ zone barre) (gauche,
    // bas)
    // UIManager.put("ScrollBar.darkShadow", Color.RED);

    // Ombre haut-gauche autour des fleches
    // UIManager.put("ScrollBar.highlight", Color.RED);
  }

  private void appliquerModeleGraphiqueTable() {
    UIManager.put("TableHeader.background", getCouleur("couleur.fond"));
    UIManager.put("TableHeader.foreground", getCouleur("couleur.police"));
    UIManager.put("TableHeader.font", getCouleur("police.defaut"));
  }
}