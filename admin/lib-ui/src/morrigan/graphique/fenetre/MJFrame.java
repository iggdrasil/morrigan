/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJFrame.java - CREATION : 2004/07/15
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.fenetre;

import javax.swing.JFrame;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public abstract class MJFrame extends JFrame {
  public MJFrame(String sTitre) {
    super(sTitre);
    chargerModele();
  }

  public MJFrame(String sTitre, int iLargeurPx, int iHauteurPx) {
    super(sTitre);
    imposerTaille(iLargeurPx, iHauteurPx);
    chargerModele();
  }

  public void lancer() {
    setVisible(true);
  }

  public void imposerTaille(int iLargeurPx, int iHauteurPx) {
    setSize(iLargeurPx, iHauteurPx);
  }

  public void chargerModele() {
  // TODO: ajouter des caracteristiques d'elements graphiques (bordure de
  // fenetre, etc.)
  }
}
