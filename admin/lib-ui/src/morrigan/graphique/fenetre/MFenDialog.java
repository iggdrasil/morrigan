/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MFenDialog.java - CREATION : 2004/03/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.fenetre;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;

import morrigan.dictionnaire.MDictionnaire;

/**
 * Cette classe permet de disposer d'une fenetre dediee au dialogue. Par defaut,
 * elle est modale.
 * 
 * @author armel
 */
public abstract class MFenDialog extends JDialog implements ActionListener {

  public MFenDialog(Frame fPere) {
    super(fPere);
    setModal(true);

    remplir();
    pack();
    applySize();

    // Positionnement
    if (fPere != null) {
      Dimension dimFen = fPere.getSize();
      Dimension dimDialogue = this.getSize();
      setLocation((dimFen.width - dimDialogue.width) / 2,
          (dimFen.height - dimDialogue.height) / 2);
    } else {
      final Rectangle screenRect = new JFrame().getGraphicsConfiguration()
          .getBounds();
      setLocation(screenRect.x + screenRect.width / 2 - getSize().width / 2,
          screenRect.y + screenRect.height / 2 - getSize().height / 2);
    }
    // setVisible(true);
  }

  /**
   * Constructeur d'une fenetre de dialogue. Elle est placee au milieu de la
   * fenetre mere.
   */
  public MFenDialog(Frame fPere, String titre) {
    this(fPere);
    setTitle(titre != null ? MDictionnaire.getLabel(1003) : titre);
  }

  /**
   * Remplir la fenetre de composants graphiques.
   */
  abstract protected void remplir();

  /**
   * Lancer l'interface avec des proprietes speciales.
   */
  abstract public void launch();

  /**
   * Appliquer une taille.
   */
  protected void applySize() {}
}