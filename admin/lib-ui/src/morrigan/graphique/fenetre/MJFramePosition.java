/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJFramePosition.java - CREATION : 2004/08/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.fenetre;

import java.awt.Point;
import java.math.BigDecimal;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.MModele;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public abstract class MJFramePosition extends MJFrame implements
    PositionnableFrame, CellStructure {

  private Point pPosition = null;

  public MJFramePosition(String sTitre) {
    super(sTitre);
  }

  public MJFramePosition(String sTitre, int iLargeurCell, int iHauteurCell) {
    this(sTitre);
    BigDecimal oTaille = MModele.getNombre("nombre.cellule.largeur");
    int iLargeurPxCell = (oTaille != null ? oTaille.intValue()
        : MIQuadComponent.iLargeurCell);

    oTaille = MModele.getNombre("nombre.cellule.hauteur");
    int iHauteurPxCell = (oTaille != null ? oTaille.intValue()
        : MIQuadComponent.iHauteurCell);

    // TODO: regler les tailles pour l'affichage... Bidouille :'(
    // int iMarge = iHauteurCell < 10 ? iHauteurPxCell / 2 : 0;
    int iMarge = 0;
    this.imposerTaille(iLargeurCell * iLargeurPxCell, (iHauteurCell + 0)
        * iHauteurPxCell + iMarge);
  }

  protected void conserverPosition() {
    if (!isVisible())
      return;
    pPosition = getLocationOnScreen();
  }

  @Override
  public void setVisible(boolean bVisible) {
    if (!bVisible)
      conserverPosition();

    super.setVisible(bVisible);
  }

  @Override
  public Point getLocationOnScreen() {
    if (isVisible())
      return super.getLocationOnScreen();
    return pPosition;
  }

  @Override
  public void setLocation(int x, int y) {
    pPosition = new Point(x, y);
    super.setLocation(x, y);
  }
}
