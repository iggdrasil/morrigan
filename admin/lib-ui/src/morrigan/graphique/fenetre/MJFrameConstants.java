package morrigan.graphique.fenetre;

/**
 * @author Armel
 */
public interface MJFrameConstants {

  public static final int numberOfPixelsPerWidthCell = 16;

  public static final int numberOfPixelsPerHeightCell = 22;
}
