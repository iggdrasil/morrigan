package morrigan.graphique;

import java.awt.Color;

public class Colors {

  public static Color getMandatoryColor() {
    // MModele.getCouleur("couleur.champ.obligatoire");
    return Color.RED;
  }

  public static Color getLineBorderColor() {
    // MModele.getCouleur("couleur.bouton.bordure");
    return Color.LIGHT_GRAY;
  }

  public static Color getActiveButtonColor() {
    // MModele.getCouleur("couleur.bouton.actif");
    return Color.GRAY;
  }

  public static Color getInactiveButtonColor() {
    // MModele.getCouleur("couleur.bouton.inactif");
    return Color.LIGHT_GRAY;
  }
  
  public static Color getOverButtonColor() {
    // MModele.getCouleur("couleur.bouton.survole");
    return Color.DARK_GRAY;
  }

  public static Color getBackgroundButtonColor() {
    // MModele.getCouleur("couleur.bouton.fond");
    return Color.BLUE;
  }

  public static Color getBackgroundColor() {
//    MModele.getCouleur("couleur.fond")
    return Color.BLUE;
  }

  public static Color getTreeTextColor() {
    return Color.DARK_GRAY;
  }

  public static Color getErrorBgColor() {
    return Color.red;
  }
}
