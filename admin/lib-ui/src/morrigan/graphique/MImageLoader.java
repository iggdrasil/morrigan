/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MImageLoader.java - CREATION : 2006/03/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

import morrigan.log.MLog;

/**
 * Chargeur de fichier pour en faire des <code>Image</code> ou
 * <code>ImageIcon</code>.
 * 
 * @author armel
 */
public class MImageLoader {
  public static ImageIcon loadIcon(String pathImage) {
    URL urlImage = null;
    ImageIcon image = null;

    // Cas de l'image dans le jar
    if ((urlImage = MImageLoader.class.getClassLoader().getResource(pathImage)) != null) {
      MLog.infos("JAR (image : " + pathImage + "; url : " + urlImage + ")");
      image = new ImageIcon(urlImage);
    } else {
      // Cas de l'image dans le repertoire
      MLog.infos("FICHIER (image: " + pathImage + ")");
      image = new ImageIcon(java.awt.Toolkit.getDefaultToolkit().getImage(
          pathImage));
    }

    return image;
  }

  public static Image load(String pathImage) {
    URL urlImage = null;
    Image image = null;

    // Cas de l'image dans le jar
    if ((urlImage = new MImageLoader().getClass().getClassLoader().getResource(
        pathImage)) != null) {
      MLog.infos("JAR (image: " + pathImage + ")");
      image = new ImageIcon(urlImage).getImage();
    } else {
      // Cas de l'image dans le repertoire
      MLog.infos("FICHIER (image: " + pathImage + ")");
      image = java.awt.Toolkit.getDefaultToolkit().getImage(pathImage);
    }

    return image;

  }

}
