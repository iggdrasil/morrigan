/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJTabbedPane.java - CREATION : 2004/01/04
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTabbedPane;

import morrigan.dictionnaire.MDictionnaire;
import morrigan.graphique.Colors;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.MStylable;

/**
 * Cette classe <code>MJTabbedPane</code> etend de simplement de JTabbedPane
 * et lui associe une couleur. De plus la taille des titres est homogene.
 * 
 * @author armel
 */
public class MJTabbedPane extends JTabbedPane implements MStylable,
    MIQuadComponent {

  private final int hauteurCell;

  private final int largeurCell;

  /**
   * Constructeur d'un MJTabbedPane dont les onglets seront ecrits en gras.
   */
  public MJTabbedPane(int iHauteurCell, int iLargeurCell) {
    this(STYLE.NORMAL, iHauteurCell, iLargeurCell);
  }

  /**
   * Constructeur d'un MJTabbedPane dans un certain style.
   * 
   * @param style Style parmi ceux proposes
   */
  public MJTabbedPane(STYLE style, int iHauteurCell, int iLargeurCell) {
    this.hauteurCell = iHauteurCell;
    this.largeurCell = iLargeurCell;
    imposerStyle(style);
//    chargerModele();
  }

  /**
   * Donner un style aux onglets.
   * 
   * @param style Style parmi ceux proposes
   */
  public void imposerStyle(STYLE style) {
    String sFontName = getFont().getFontName();
    int iFontSize = getFont().getSize();
    int iFontStyle = Font.PLAIN;

    switch (style) {
      case NORMAL:
        iFontStyle = Font.PLAIN;
        break;

      case TITRE:
        iFontStyle = Font.BOLD;
        break;

      case ITALIQUE:
        iFontStyle = Font.ITALIC;
        break;

      default:
        break;
    }

    this.setFont(new Font(sFontName, iFontStyle, iFontSize));
  }

  /** {@inheritDoc} */
  public int getHauteurCell() {
    return hauteurCell;
  }

  /** {@inheritDoc} */
  public int getLargeurCell() {
    return largeurCell;
  }

  /**
   * Ajouter un onglet avec un titre, un objet graphique et un tooltip au
   * survol.
   * 
   * @param idTitle
   * @param component
   * @param idTip
   */
  public void addTab(int idTitle, Component component, int idTip) {
    super.addTab(MDictionnaire.getLabel(idTitle), null, component,
        MDictionnaire.getLabel(idTip));
  }

  private void chargerModele() {
    setBackground(Colors.getBackgroundColor());
  }

}
