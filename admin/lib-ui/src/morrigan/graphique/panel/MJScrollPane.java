/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJScrollPane.java - CREATION : 2004/01/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.graphique.panel;

import java.awt.Component;

import javax.swing.JScrollPane;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.field.util.MTextArea;
import morrigan.graphique.table.MDonnees;
import morrigan.graphique.table.MJTable;

/**
 * @author armel
 */
public class MJScrollPane extends JScrollPane implements MIQuadComponent,
    MIField {

  protected MJScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
    super(view, vsbPolicy, hsbPolicy);
  }

  public MJScrollPane(MJTree mtree) {
    this(mtree, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }
  
  public MJScrollPane(MTextArea mta) {
    this(mta, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  public MJScrollPane(MJPanel mjp) {
    this(mjp, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_NEVER);
  }

  public MJScrollPane(MJPanel mjp, int vsbPolicy, int hsbPolicy) {
    this((Component) mjp, vsbPolicy, hsbPolicy);
  }

  public MJScrollPane(MJTable mjt) {
    this(mjt, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }
  
  /** {@inheritDoc} */
  public int getHauteurCell() {
    if (getViewport().getComponent(0) instanceof MIQuadComponent) {
      // Le 0 est l'element voulu
      return ((MIQuadComponent) getViewport().getComponent(0)).getHauteurCell();
    }
    return 1;
  }

  /** {@inheritDoc} */
  public int getLargeurCell() {
    if (getViewport().getComponent(0) instanceof MIQuadComponent) {
      // Le 0 est l'element voulu
      return ((MIQuadComponent) getViewport().getComponent(0)).getLargeurCell(); 
    }
    return 1;
  }

  /** {@inheritDoc} */
  public void associate(MDonnees md, String sColonne) {}

  /** {@inheritDoc} */
  public boolean isMandatory() {
    return false;
  }

  /** {@inheritDoc} */
  public void setMandatory(boolean bMandatory) {}

  /** {@inheritDoc} */
  public void setValue(Object value) {}

  /** {@inheritDoc} */
  public Object getValue() {
    return null;
  }

  /** {@inheritDoc} */
  public String getColonne() {
    return null;
  }
  
  /** {@inheritDoc} */
  public MDonnees getDonnees() {
    return null;
  }
}