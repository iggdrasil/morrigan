/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJPanel.java - CREATION : 2003/12/21
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import morrigan.graphique.MModele;

/**
 * Panel personnalise, autorisant une bordure generique.
 * 
 * @author armel
 */
public class MJPanel extends JPanel {

  public enum BORDURE {
    /** Sans bordure. */
    SANS,
    /** Avec une bordure rehaussee. */
    RAISED,
    /** Avec un fin trait comme bordure. */
    ETCHED,
    /** Avec une bordure de vide. */
    VIDE,
    /** Bordure des menus (vide, rehaussage, vide). */
    MENU
  }

  private int largeurPx;

  private int hauteurPx;

  /**
   * Constructeur d'un MJPanel vide, sans bordure.
   */
  public MJPanel() {
    this(BORDURE.SANS);
  }

  /**
   * Constructeur d'un MJPanel, avec un type de bordure.
   * 
   * @param typeBordure type de la bordure
   */
  public MJPanel(BORDURE typeBordure) {
//    chargerModele();
    this.ajouterBordureInterne(typeBordure);
  }

  /**
   * Constructeur d'un MJPanel avec des dimensions imposees, sans bordure.
   * 
   * @param largeurPx largeur du panel en pixels
   * @param hauteurPx hauteur du panel en pixels
   */
  public MJPanel(int largeurPx, int hauteurPx) {
    this(BORDURE.SANS, largeurPx, hauteurPx);
  }

  /**
   * Constructeur d'un MJPanel avec ou sans bordure, avec des dimensions
   * imposees.
   * 
   * @param typeBordure type de bordure
   * @param largeurPx largeur du panel en pixels
   * @param hauteurPx hauteur du panel en pixels
   */
  public MJPanel(BORDURE typeBordure, int largeurPx, int hauteurPx) {
    this(typeBordure);
    this.imposerTaille(largeurPx, hauteurPx);
  }

  /**
   * Constructeur d'un MJPanel avec une bordure vide non uniforme, et autour une
   * potentielle bordure visible.
   * 
   * @param typeBordure type de la bordure
   * @param haut taille de la bordure en haut
   * @param gauche taille de la bordure a gauche
   * @param bas taille de la bordure en bas
   * @param droite taille de la bordure a droite
   */
  public MJPanel(BORDURE typeBordure, int haut, int gauche, int bas, int droite) {
    this();
    this.ajouterBordureInterne(typeBordure, haut, gauche, bas, droite);
  }

  /**
   * Retourne la largeur du MJPanel.
   * 
   * @return largeur
   */
  public int getLargeur() {
    return this.largeurPx;
  }

  /**
   * Retourne la hauteur du MJPanel.
   * 
   * @return hauteur
   */
  public int getHauteur() {
    return this.hauteurPx;
  }

  /**
   * Imposer la taille du MJPanel.
   * 
   * @param largeurPanelPx largeur du panel
   * @param hauteurPanelPx hauteur du panel
   */
  public void imposerTaille(int largeurPanelPx, int hauteurPanelPx) {
    this.largeurPx = largeurPanelPx;
    this.hauteurPx = hauteurPanelPx;
    Dimension dim = new Dimension(largeurPanelPx, hauteurPanelPx);
    this.setMaximumSize(dim);
    this.setMinimumSize(dim);
    this.setPreferredSize(dim);
  }

  /**
   * Ajouter une bordure d'un certain type.
   * 
   * @param typeBordure type de la bordure
   */
  private void ajouterBordureInterne(BORDURE typeBordure) {
    this.ajouterBordureInterne(typeBordure, 0, 0, 0, 0);
  }

  /**
   * Ajouter une bordure d'un certain type.
   * 
   * @param typeBordure type de la bordure
   * @param haut taille de la bordure du haut
   * @param gauche taille de la bordure de gauche
   * @param bas taille de la bordure du bas
   * @param droite taille de la bordure de droite
   */
  public void ajouterBordureInterne(BORDURE typeBordure, int haut, int gauche,
      int bas, int droite) {
    switch (typeBordure) {
      case SANS:
        break;

      case VIDE:
        this.ajouterBordureInterne(BorderFactory.createEmptyBorder(haut,
            gauche, bas, droite));
        break;

      case RAISED:
        this.ajouterBordureInterne(BorderFactory.createRaisedBevelBorder());
        this.ajouterBordureInterne(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        break;

      case ETCHED:
        this.ajouterBordureInterne(BorderFactory.createEtchedBorder());
        this.ajouterBordureInterne(BorderFactory.createEmptyBorder(2, 5, 2, 5));
        break;

      case MENU:
        this.ajouterBordureInterne(BorderFactory.createRaisedBevelBorder());
        this.ajouterBordureInterne(BorderFactory.createEmptyBorder(5, 7, 5, 5));
        break;
      default:
        break;
    }
  }

  /**
   * Ajouter une bordure a l'interieur de la precedente.
   * 
   * @param border bordure a inserer
   */
  private void ajouterBordureInterne(Border border) {
    this.setBorder(BorderFactory.createCompoundBorder(getBorder(), border));
  }

  private void chargerModele() {
//    setBackground(MModele.getCouleur("couleur.fond"));
//    setForeground(MModele.getCouleur("couleur.front"));
  }
}