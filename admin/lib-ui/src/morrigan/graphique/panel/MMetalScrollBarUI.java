/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MMetalScrollBarUI.java - CREATION : 2004/07/31
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import javax.swing.JButton;
import javax.swing.plaf.metal.MetalScrollBarUI;

import morrigan.graphique.MModele;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MMetalScrollBarUI extends MetalScrollBarUI {

  public MMetalScrollBarUI() {}

  @Override
  protected void configureScrollBarColors() {
    super.configureScrollBarColors();
  }

  @Override
  protected JButton createDecreaseButton(int orientation) {
    decreaseButton = new MMetalScrollButton(orientation, scrollBarWidth,
        isFreeStanding);
//    decreaseButton.setBackground(MModele.getCouleur("couleur.police"));
//    decreaseButton.setForeground(MModele.getCouleur("couleur.fond"));
    return decreaseButton;
  }

  @Override
  protected JButton createIncreaseButton(int orientation) {
    increaseButton = new MMetalScrollButton(orientation, scrollBarWidth,
        isFreeStanding);
//    increaseButton.setBackground(MModele.getCouleur("couleur.police"));
//    increaseButton.setForeground(MModele.getCouleur("couleur.fond"));
    return increaseButton;
  }
}