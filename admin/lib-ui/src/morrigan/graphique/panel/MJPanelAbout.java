/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJPanelAbout.java - CREATION : 2004/08/14
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import javax.swing.ImageIcon;

import morrigan.graphique.MJLabel;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJPanelAbout extends MJPanelQuad {

  public MJPanelAbout(String sNomApplication, String sVersion, ImageIcon icon) {
    super(BORDURE.ETCHED, 1, 2);
    MJLabel mjlNom = new MJLabel(sNomApplication, 1, 1);
    MJLabel mjlVersion = new MJLabel(sVersion, 1, 1);

    this.add(mjlNom, 0, 0);
    this.add(mjlVersion, 0, 1);
  }
}