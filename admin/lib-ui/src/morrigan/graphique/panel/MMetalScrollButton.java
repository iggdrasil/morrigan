/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MMetalScrollButton.java - CREATION : 2004/07/31
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import javax.swing.plaf.metal.MetalScrollButton;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MMetalScrollButton extends MetalScrollButton {

  public MMetalScrollButton(int direction, int width, boolean freeStanding) {
    super(direction, width, freeStanding);
  }
}