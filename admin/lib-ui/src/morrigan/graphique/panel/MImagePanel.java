/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MImagePanel.java - CREATION : 2003/12/07
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import morrigan.graphique.MImageLoader;
import morrigan.log.MLog;

/**
 * Le panel <code>MImagePanel</code> permet d'afficher facilement une image
 * dans un panel. Celui-ci peut avoir une marge (uniforme ou non).
 * 
 * @author armel
 */
public class MImagePanel extends MJPanel {

  private static final int MARGE_DEFAUT = 10;

  private String pathImage = null;

  private int hauteurMarge = 10;

  private int largeurMarge = 10;

  private int hauteurPanel = 0;

  private int largeurPanel = 0;

  Image image = null;

  /**
   * Constructeur d'un MImagePanel a l'aide de l'url d'une image.
   * 
   * @param urlImage url de l'image
   */
  public MImagePanel(String urlImage) {
    this(urlImage, -1, -1, MARGE_DEFAUT, MARGE_DEFAUT);
  }

  /**
   * Constructeur d'un MImagePanel a l'aide de l'url d'une image.
   */
  public MImagePanel() {
    this(-1, -1, MARGE_DEFAUT, MARGE_DEFAUT);
  }

  /**
   * Constructeur d'un MImagePanel sans image, celle-ci etant specifier
   * ulterieurement a l'aide de la methode chargerImage().
   * 
   * @param pathImage chemin de l'image
   * @param largeurPanel largeur du panel contenant l'image en pixels
   * @param hauteurPanel hauteur du panel contenant l'image en pixels
   * @param margeHorizontale marge a droite et gauche de l'image en pixels
   * @param margeVerticale marge en bas et en haut de l'image en pixels
   */
  public MImagePanel(String pathImage, int largeurPanel, int hauteurPanel,
      int margeHorizontale, int margeVerticale) {
    this(largeurPanel, hauteurPanel, margeHorizontale, margeVerticale);
    this.pathImage = pathImage;
    if (pathImage != null)
      this.chargerImage(pathImage);
  }

  /**
   * Constructeur d'un MImagePanel sans image, celle-ci etant specifier
   * ulterieurement a l'aide de la methode chargerImage().
   * 
   * @param largeurPanel largeur du panel contenant l'image en pixels
   * @param hauteurPanel hauteur du panel contenant l'image en pixels
   * @param margeHorizontale marge a droite et gauche de l'image en pixels
   * @param margeVerticale marge en bas et en haut de l'image en pixels
   */
  public MImagePanel(int largeurPanel, int hauteurPanel, int margeHorizontale,
      int margeVerticale) {
    super();
    this.largeurMarge = margeHorizontale;
    this.hauteurMarge = margeVerticale;
    this.largeurPanel = largeurPanel;
    this.hauteurPanel = hauteurPanel;
  }

  protected void justifySizeForImage() {
    MLog.debug(this.getClass().toString(), "justifySizeForImage",
        "Changement de taille !");
    // S'il faut redimensionner...
    if (largeurPanel > 0 && hauteurPanel > 0) {
      int iLargeurImage = image.getWidth(this);
      int iHauteurImage = image.getHeight(this);

      // TODO: retrouver les dimensions de l'image plus proprement
      while (iLargeurImage == -1)
        iLargeurImage = image.getWidth(this);
      while (iHauteurImage == -1)
        iHauteurImage = image.getWidth(this);

      // Test Horizontal
      // Si l'image est plus large que l'espace reserve, on redimensionne
      if (iLargeurImage > largeurPanel - 2 * largeurMarge) {
        iHauteurImage = (int) (iHauteurImage * ((float) (largeurPanel - 2 * this.largeurMarge) / (float) iLargeurImage));
        iLargeurImage = largeurPanel - 2 * this.largeurMarge;
        this.image = this.image.getScaledInstance(iLargeurImage, iHauteurImage,
            Image.SCALE_SMOOTH);
      }

      // Test Vertical
      // Si l'image est plus haute que l'espace reserve, on redimensionne
      if (iHauteurImage > hauteurPanel - 2 * hauteurMarge) {
        iLargeurImage = (int) (iLargeurImage * ((float) (hauteurPanel - 2 * this.hauteurMarge) / (float) iHauteurImage));
        iHauteurImage = hauteurPanel - 2 * this.hauteurMarge;
        this.image = this.image.getScaledInstance(iLargeurImage, iHauteurImage,
            Image.SCALE_SMOOTH);
      }

      this.largeurMarge = (largeurPanel - iLargeurImage) / 2;
      this.hauteurMarge = (hauteurPanel - iHauteurImage) / 2;
      this.imposerTaille(largeurPanel, hauteurPanel);

    } else {
      this.imposerTaille(image.getWidth(this) + largeurMarge * 2, image
          .getHeight(this)
          + hauteurMarge * 2);
    }

    this.update(this.getGraphics());
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (image != null)
      g.drawImage(image, largeurMarge, hauteurMarge, Color.RED, this);
  }

  /**
   * ATTENTION ! L'IMAGE EST EN LOCAL !!!
   * 
   * @param pathImage
   */
  public void chargerImage(String cheminImage) {
    // pathImage = "ressources/images/nimnae.jpg";

    if (pathImage == null)
      return;
    this.pathImage = cheminImage;

    if (true) {
      this.image = MImageLoader.load(pathImage);

      if (image == null) {
        // TODO: mettre un timeout et lancer un thread

        // Thread threadImage = new Thread()
        // {
        // public void run()
        // {
        try {
          URL urlImage = new URL(MImagePanel.this.getPathImage());
          MLog.infos("NET (image: " + urlImage + ")");
          MImagePanel.this.image = java.awt.Toolkit.getDefaultToolkit()
              .getImage(urlImage);
          MLog.infos("image obtenue !");
          MImagePanel.this.justifySizeForImage();
        } catch (Exception e) {
          MLog.error(MImagePanel.class.toString(), "run",
              "Image non dispo en ligne: " + e);
        }
        // }
        // };
        // threadImage.start();
      }

      if (image != null)
        MImagePanel.this.justifySizeForImage();

    }
  }

  protected String getPathImage() {
    return pathImage;
  }

}
