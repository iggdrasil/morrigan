/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJPanelQuad.java - CREATION : 2003/12/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.panel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.Line2D;

import javax.swing.JComponent;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.field.MTextAreaField;
import morrigan.log.MLog;

/**
 * Ce panel est quadrille. Il est represente par des cellules, toutes etant de
 * meme taille. Les dimensions (largeur et hauteur) sont en cellules.
 * 
 * @author armel
 */
public class MJPanelQuad extends MJPanel implements MIQuadComponent {

  /**
   * Constructeur d'un MJPanelQuad, avec une taille specifique en cellules et
   * une bordure rehaussee.
   * 
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   */
  public MJPanelQuad(int largeurCell, int hauteurCell) {
    this(BORDURE.RAISED, largeurCell, hauteurCell);
  }

  /**
   * Constructeur d'un MJPanelQuad, avec une taille specifique en cellules, avec
   * ou sans bordure.
   * 
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   * @param typeBordure type de la bordure
   */
  public MJPanelQuad(BORDURE typeBordure, int largeurCell, int hauteurCell) {
    super(typeBordure);
    this.setLayout(new CellLayout(new Dimension(largeurCell, hauteurCell)));
  }

  /**
   * Ajouter un composant (<code>MIQuadComponent</code>) pouvant entrer dans
   * le systeme de quadrillage. La place qu'il occupera dependra de sa largeur
   * et de sa hauteur (en cellules). <br>
   * <b>Attention ! </b> Lors d'ajout de classes graphiques, celle-ci doit etre
   * ajoutee dans cette methode. C'est pourri mais pas mieux pour le moment.
   * <br>
   * <b>Attention ! (2) </b> Pb peut-etre regle...
   * 
   * @param mjc composant gerant le quadrillage
   * @param x abscisse du composant
   * @param y ordonnee du composant
   */
  public void add(MIQuadComponent mjc, int x, int y) {
    try {
      JComponent jc = (JComponent) mjc;

      /* On rajoute un scrollpane a un textarea */
      if (mjc instanceof MTextAreaField
          && ((MTextAreaField) mjc).isScrollBarVisible()) {
        jc = new MJScrollPane((MTextAreaField) mjc);
      }

      this.add(jc, new Rectangle(x, y, mjc.getLargeurCell(), mjc
          .getHauteurCell()));
    } catch (Exception e) {
      MLog.error("MJPanelQuad", "add", "Erreur d'insertion dans le panel ", e);
    }
  }

  /**
   * Imposer la taille en pixels du panel. Cette etape est appelee a chaque
   * redimensionnement de la grille de quadrillage.
   * 
   * @param largeur largeur en pixels
   * @param hauteur hauteur en pixels
   */
  @Override
  public void imposerTaille(int largeur, int hauteur) {
    this.imposerTaille(new Dimension(largeur, hauteur));
  }

  /**
   * Imposer la taille en pixels du panel. Cette etape est appelee a chaque
   * redimensionnement de la grille de quadrillage.
   * 
   * @param dim dimension du panel
   */
  public void imposerTaille(Dimension dim) {
    this.setMinimumSize(dim);
    this.setMaximumSize(dim);
    this.setPreferredSize(dim);
  }

  /*
   * (non-Javadoc)
   * 
   * @see client.util.MJComponent#getHauteurCell()
   */
  public int getHauteurCell() {
    return (int) ((CellLayout) getLayout()).getGridSize().getHeight();
  }

  /*
   * (non-Javadoc)
   * 
   * @see client.util.MJComponent#getLargeurCell()
   */
  public int getLargeurCell() {
    return (int) ((CellLayout) getLayout()).getGridSize().getWidth();
  }

  /** {@inheritDoc} */
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    if (false) {
      final Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);

      final double echelle = MIQuadComponent.iTailleCell;
      final double margin = 3;
      final Stroke cellStroke = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE,
          BasicStroke.JOIN_MITER);

      final Color foregroundColor = Color.LIGHT_GRAY;
      // Grid drawing
      g2.setColor(foregroundColor);
      g2.setStroke(cellStroke);

      for (int column = 0; column <= getLargeurCell(); column++) {
        Line2D.Double verticalLine = new Line2D.Double(margin + column
            * echelle, margin, margin + column * echelle, margin
            + getHauteurCell() * echelle);
        g2.draw(verticalLine);
      }

      for (int iLigne = 0; iLigne <= getHauteurCell(); iLigne++) {
        Line2D.Double horizontalLine = new Line2D.Double(margin, margin
            + iLigne * echelle, margin + getLargeurCell() * echelle, margin
            + iLigne * echelle);
        g2.draw(horizontalLine);
      }
    }
  }
  
}