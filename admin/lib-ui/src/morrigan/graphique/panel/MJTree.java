package morrigan.graphique.panel;

import javax.swing.JTree;

import morrigan.graphique.MIQuadComponent;

public class MJTree extends JTree implements MIQuadComponent {

  private final int hauteurCell;
  private final int largeurCell;

  public MJTree(int largeurCell, int hauteurCell) {
    this.largeurCell = largeurCell;
    this.hauteurCell = hauteurCell;
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

}
