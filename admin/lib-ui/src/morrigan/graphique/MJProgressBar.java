/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJProgressBar.java - CREATION : 4 déc. 2005
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
/*
 * Created on 4 déc. 2005
 * 
 * Ce fichier appartient au projet Morrigan.
 */
package morrigan.graphique;

import javax.swing.JProgressBar;

public class MJProgressBar extends JProgressBar {

}
