/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MStylable.java - CREATION : 2004/01/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

/**
 * Cette interface s'applique e une zone ou apparaissent des chaines de
 * caracteres.
 * 
 * @author armel
 */
public interface MStylable {
  enum STYLE {
    /**
     * Style de taille normale, classique (fin, droit).
     */
    NORMAL,
    /**
     * Style caracteristique d'un titre.
     */
    TITRE,
    /**
     * Style italique.
     */
    ITALIQUE
  }

  /**
   * Imposer un style a une zone de texte.
   * 
   * @param style style a appliquer
   */
  public void imposerStyle(STYLE style);
}
