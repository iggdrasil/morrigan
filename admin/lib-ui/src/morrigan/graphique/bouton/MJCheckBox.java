/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJCheckBox.java - CREATION : 2003/12/22
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.bouton;

import java.awt.Dimension;

import javax.swing.JCheckBox;

/**
 * Ce <code>MJCheckBox</code> est un bouton valide coche/decoche personnalise.
 * 
 * @author armel
 */
public class MJCheckBox extends JCheckBox {

  /**
   * Constructeur d'un MJCheckBox.
   * 
   * @param largeurPx largeur du bouton en pixels
   * @param hauteurPx hauteur du bouton en pixels
   */
  public MJCheckBox(int largeurPx, int hauteurPx) {
    super();
    this.imposerTaille(largeurPx, hauteurPx);
    this.setOpaque(false);
  }

  /**
   * Imposer la taille du bouton.
   * 
   * @param largeur largeur du bouton en pixels
   * @param hauteur hauteur du bouton en pixels
   */
  private void imposerTaille(int largeur, int hauteur) {
    this.imposerTaille(new Dimension(largeur, hauteur));
  }

  /**
   * Imposer la taille du bouton.
   * 
   * @param dim dimensions du bouton en pixels
   */
  private void imposerTaille(Dimension dim) {
    this.setMinimumSize(dim);
    this.setMaximumSize(dim);
    this.setPreferredSize(dim);
  }
}
