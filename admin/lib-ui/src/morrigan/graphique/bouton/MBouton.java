package morrigan.graphique.bouton;

public interface MBouton {
  /**
   * Specifier si le bouton est accessible dans le cadre de son ecran.
   * <b>ATTENTION</b>: Il ne s'agit pas de dire s'il est "cliquable" ou non
   * (via setEnabled(...)) puisque ce dernier aspect rentre dans le contexte
   * immediat.
   * 
   * @param bAutorise <code>true</code> si autorise dans l'ecran,
   *          <code>false</code> sinon
   */
  public void setAutorise(boolean bAutorise);

  public boolean isAutorise();

  /**
   * Specifier si un bouton est accessible immediatement. Rentre aussi en compte
   * l'autorisation donnee par l'ecran sur le bouton.
   */
  public void setEnabled(boolean bEnabled);
}
