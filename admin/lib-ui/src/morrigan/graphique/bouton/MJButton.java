/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJButton.java - CREATION : 2003/12/07
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.bouton;

import java.awt.Font;

import javax.swing.JButton;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.MStylable;

/**
 * @author armel
 */
public class MJButton extends JButton implements MStylable,
    MIQuadComponent, MBouton {

  private boolean bAutorise = true;

  private int iLargeurCell = 1;

  private int iHauteurCell = 1;

  /**
   * Constructeur d'un MJButton, avec un label et un style gras.
   * 
   * @param labelBouton label du bouton
   */
  public MJButton(String labelBouton) {
    this(labelBouton, STYLE.TITRE);
  }

  /**
   * Constructeur d'un MJButton, avec un label.
   * 
   * @param labelBouton label du bouton
   * @param style de police a appliquer
   */
  public MJButton(String labelBouton, STYLE style) {
    super(labelBouton);

    // La zone de clic n'est pas affichee
    this.setContentAreaFilled(false);
    // Le focus n'est pas dessine
    this.setFocusPainted(false);
    this.setOpaque(true);
    this.imposerStyle(style);
  }

  /**
   * Constructeur d'un MJButton, en specifiant un label et imposant une taille
   * en cellules.
   * 
   * @param sLabelBouton label du bouton
   * @param iLargeurCell largeur du bouton en cellules
   * @param iHauteurCell hauteur du bouton en cellules
   */
  public MJButton(String sLabelBouton, int iLargeurCell, int iHauteurCell) {
    this(sLabelBouton);
    this.iLargeurCell = iLargeurCell;
    this.iHauteurCell = iHauteurCell;
  }

  public void imposerStyle(STYLE style) {
    String sFontName = getFont().getFontName();
    int iFontSize = getFont().getSize();
    int iFontStyle = Font.PLAIN;

    switch (style) {
      case NORMAL:
        iFontStyle = Font.PLAIN;
        break;

      case TITRE:
        iFontStyle = Font.BOLD;
        break;

      case ITALIQUE:
        iFontStyle = Font.ITALIC;
        break;

      default:
        break;
    }

    this.setFont(new Font(sFontName, iFontStyle, iFontSize));
  }

  public int getHauteurCell() {
    return iHauteurCell;
  }

  public int getLargeurCell() {
    return iLargeurCell;
  }

  public void setAutorise(boolean bAutorise) {
    this.bAutorise = bAutorise;
    if (!bAutorise) {
      setEnabled(false);
    }
  }

  public boolean isAutorise() {
    return bAutorise;
  }

  @Override
  public void setEnabled(boolean bEnabled) {
    super.setEnabled(bEnabled && bAutorise);
  }
}
