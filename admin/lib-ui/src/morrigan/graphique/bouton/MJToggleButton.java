/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJToggleButton.java - CREATION : 2003/10/30
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.bouton;

import java.awt.Font;

import javax.swing.JToggleButton;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.MStylable;

/**
 * Un MJToggleButton est un <code>JToggleButon</code> auquel ont ete apportees
 * des modifications, comme par exemple les couleurs actives et passives, les
 * enjolivures, etc. Parce qu'il implemente <code>MJComponent</code>, il peut
 * etre utilise dans un panel de type quadrillage ( <code>MJPanelQuad</code>).
 * 
 * @author armel
 */
public class MJToggleButton extends JToggleButton implements MIQuadComponent,
    MStylable, MBouton {

  private int largeurCell = 0;

  private int hauteurCell = 0;

  private boolean bAutorise = true;

  /**
   * Constructeur d'un MJToggleButton, avec un label, des dimensions en cellules
   * et un style.
   * 
   * @param label chaine de caracteres a afficher
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   * @param style style a appliquer
   */
  public MJToggleButton(String label, int largeurCell, int hauteurCell,
      STYLE style) {
    this(label, style);
    this.largeurCell = largeurCell;
    this.hauteurCell = hauteurCell;
    this.imposerStyle(style);
  }

  /**
   * Constructeur d'un MJToggleButton, avec un label, des dimensions en cellules
   * et le style par defaut (gras).
   * 
   * @param label chaine de caracteres a afficher
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   */
  public MJToggleButton(String label, int largeurCell, int hauteurCell) {
    this(label, largeurCell, hauteurCell, STYLE.TITRE);
  }

  /**
   * Constructeur d'un MJToggleButton avec un label et un style gras.
   * 
   * @param label chaine de caracteres a afficher
   */
  public MJToggleButton(String label) {
    this(label, STYLE.TITRE);
  }

  public MJToggleButton(String label, STYLE style) {
    super(label);
    this.imposerStyle(style);
  }

  public void imposerStyle(STYLE style) {
    if (getFont() == null)
      return;

    String sFontName = getFont().getFontName();
    int iFontSize = getFont().getSize();
    int iFontStyle = Font.PLAIN;

    switch (style) {
      case NORMAL:
        iFontStyle = Font.PLAIN;
        // iFontSize -= 2;
        break;

      case TITRE:
        iFontStyle = Font.BOLD;
        break;

      case ITALIQUE:
        iFontStyle = Font.ITALIC;
        break;

      default:
        break;
    }

    this.setFont(new Font(sFontName, iFontStyle, iFontSize));
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

  public void setAutorise(boolean bAutorise) {
    this.bAutorise = bAutorise;
    if (!bAutorise) {
      setEnabled(false);
    }
  }

  public boolean isAutorise() {
    return bAutorise;
  }

  @Override
  public void setEnabled(boolean bEnabled) {
    super.setEnabled(bEnabled && bAutorise);
  }
}