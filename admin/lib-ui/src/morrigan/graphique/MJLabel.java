/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJLabel.java - CREATION : 2003/11/30
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;

/**
 * Ce type de label est un <code>JLabel</code> personnalise, apportant une
 * normalisation des labels. Il permet en outre de rentrer dans une zone
 * quadrillee.
 * 
 * @see morrigan.graphique.panel.MJPanelQuad
 * @author armel
 */
public class MJLabel extends JLabel implements MIQuadComponent, MStylable {

  private int largeurCell = 0;

  private int hauteurCell = 0;

  /**
   * Constructeur d'un MJLabel, avec un texte et un style.
   * 
   * @param texte chaine de caracteres a afficher
   * @param style style de texte
   */
  public MJLabel(String texte, STYLE style) {
    super(texte);
    // this.setToolTipText(texte);
    this.imposerStyle(style);
  }

  /**
   * Constructeur d'un MJLabel, en specifiant une chaine de caracteres qui sera
   * presente avec un style NORMAL.
   * 
   * @param texte chaine de caracteres a afficher
   */
  public MJLabel(String texte) {
    this(texte, STYLE.NORMAL);
  }

  /**
   * Constructeur d'un MJLabel sans texte, avec un style NORMAL.
   */
  public MJLabel() {
    this("");
  }

  /**
   * Constructeur de MJLabel, autorisant une chaine de caracteres, ainsi que des
   * dimensions en cellules. Ce constructeur n'est utile que dans le cas d'un
   * systeme quadrille.
   * 
   * @param texte chaine de caracteres a afficher
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   */
  public MJLabel(String texte, int largeurCell, int hauteurCell) {
    this(texte);
    this.largeurCell = largeurCell;
    this.hauteurCell = hauteurCell;
  }

  /**
   * Constructeur de MJLabel, autorisant une chaine de caracteres, ainsi que des
   * dimensions en cellules. Ce constructeur n'est utile que dans le cas d'un
   * systeme quadrille.
   * 
   * @param texte chaine de caracteres a afficher
   * @param largeurCell largeur en cellules
   * @param hauteurCell hauteur en cellules
   * @param style style de texte
   */
  public MJLabel(String texte, int largeurCell, int hauteurCell, STYLE style) {
    this(texte, style);
    this.largeurCell = largeurCell;
    this.hauteurCell = hauteurCell;
  }

  /**
   * Imposer la taille du label en pixels.
   * 
   * @param largeurPx largeur du label en pixels
   * @param hauteurPx hauteur du label en pixels
   */
  public void imposerTaille(int largeurPx, int hauteurPx) {
    Dimension dim = new Dimension(largeurPx, hauteurPx);
    this.setMaximumSize(dim);
    this.setMinimumSize(dim);
    this.setPreferredSize(dim);
  }

  /*
   * (non-Javadoc)
   * 
   * @see client.util.MJComponent#getHauteurCell()
   */
  public int getHauteurCell() {
    return this.hauteurCell;
  }

  /*
   * (non-Javadoc)
   * 
   * @see client.util.MJComponent#getLargeurCell()
   */
  public int getLargeurCell() {
    return this.largeurCell;
  }

  /*
   * (non-Javadoc)
   * 
   * @see client.util.MFontable#imposerPolice(int)
   */
  public void imposerStyle(STYLE style) {
    String sFontName = getFont().getFontName();
    int iFontSize = getFont().getSize();
    int iFontStyle = Font.PLAIN;

    switch (style) {
      case NORMAL:
        iFontStyle = Font.PLAIN;
        break;

      case TITRE:
        iFontStyle = Font.BOLD;
        iFontSize += 2;
        break;

      case ITALIQUE:
        iFontStyle = Font.ITALIC;
        break;

      default:
        break;
    }

    this.setFont(new Font(sFontName, iFontStyle, iFontSize));
  }

  /**
   * Inscrit un entier s'il existe. Si l'argument est null, aucun changement
   * n'est fait.
   * 
   * @param integer entier a inscrire
   */
  public void setText(Integer integer) {
    if (integer != null)
      super.setText(integer.toString());
  }

  /**
   * Inscrit une chaine de caracteres si elle existe. Si l'argument est null,
   * aucun changement n'est fait.
   * 
   * @param string chaine a inscrire
   */
  @Override
  public void setText(String string) {
    if (string != null)
      super.setText(string);
  }
}
