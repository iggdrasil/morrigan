package morrigan.graphique.table.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

public class StringRenderer extends JTextField implements TableCellRenderer {

  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {

    // this.setBackground(isSelected ? Color.YELLOW : Color.GREEN);
    // this.setForeground(isSelected ? Color.GREEN : Color.YELLOW);
    this.setText((String) value);
    return this;
  }
}