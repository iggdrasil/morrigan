package morrigan.graphique.table.renderer;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * Inspire de JTable.BooleanRenderer
 * 
 * @author alagadic
 */
public class CheckBoxRenderer extends JCheckBox implements TableCellRenderer {

  private static final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

  public CheckBoxRenderer() {
    super();
    setHorizontalAlignment(JLabel.CENTER);
    setBorderPainted(true);
  }

  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {

    if (isSelected) {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    } else {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
    }

    /* Sans valeur, on ne met AUCUN renderer specifique */
    if (value == null) {
      DefaultTableCellRenderer defaultTableCellRenderer = new DefaultTableCellRenderer();
      defaultTableCellRenderer = (DefaultTableCellRenderer) defaultTableCellRenderer
          .getTableCellRendererComponent(table, null, isSelected, hasFocus,
              row, column);
      return defaultTableCellRenderer;
    }

    setSelected(value != null && ((Boolean) value).booleanValue());

    if (hasFocus) {
      setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
    } else {
      setBorder(noFocusBorder);
    }
    return this;
  }

}