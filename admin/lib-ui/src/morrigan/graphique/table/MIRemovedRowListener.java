/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIRemovedRowListener.java - CREATION : 2004/08/08
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public interface MIRemovedRowListener {
  void removedRow(int iIndex);
}