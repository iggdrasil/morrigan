/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MInfoColonne.java - CREATION : 2005/12/26
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

public class MInfoColonne {
  private String sColumnName = new String();

  private Class<?> cColumnClasse = String.class;

  MInfoColonne() {}

  public Class<?> getColumnClasse() {
    return cColumnClasse;
  }

  public void setColumnClasse(Class<?> columnClasse) {
    cColumnClasse = columnClasse;
  }

  public String getColumnName() {
    return sColumnName;
  }

  public void setColumnName(String columnName) {
    sColumnName = columnName;
  }

}
