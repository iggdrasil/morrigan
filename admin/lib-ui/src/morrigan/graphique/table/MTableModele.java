/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MTableModele.java - CREATION : 2004/07/26
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MTableModele extends AbstractTableModel {
  public static final int INFOS_COLONNE_NOM = 0;

  public static final int INFOS_COLONNE_CLASSE = 1;

  /**
   * En cas de surcharge de vColumnInfos, il faut gerer toutes modifs possibles
   * (ajout, suppression, modif).
   */
  protected List<MInfoColonne> columnInfos = new ArrayList<MInfoColonne>();

  private List<String> columnNames = new ArrayList<String>();

  // private Vector<Class> vColumnClasses = new Vector<Class>();
  // private Vector vColumnToolTip = new Vector();
  protected List<List<Object>> vData = new ArrayList<List<Object>>();

  private List<MIRemovedRowListener> vRemovedRowListener = new ArrayList<MIRemovedRowListener>();

  private Object oOldValue = null;

  public MTableModele() {}

  public MTableModele(String[] sNomsColonnes) {
    if (sNomsColonnes == null)
      return;
    for (int i = 0; i < sNomsColonnes.length; i++)
      addColumn(sNomsColonnes[i]);
  }

  protected void addColumn(MInfoColonne mInfo) {
    columnInfos.add(mInfo);
    columnNames.add(mInfo.getColumnName());
  }

  protected MInfoColonne createInfoCol() {
    return new MInfoColonne();
  }

  protected MInfoColonne addColumn(String sColonneNom) {
    final MInfoColonne mInfo = createInfoCol();
    mInfo.setColumnName(sColonneNom);

    columnInfos.add(mInfo);
    columnNames.add(sColonneNom);

    for (int i = 0; i < vData.size(); i++) {
      // vData.get(i).setSize(getColumnCount());
      setSize(vData.get(i), getColumnCount());
    }

    fireTableStructureChanged();
    return mInfo;
  }

  private void setSize(List<Object> list, int newSize) {
    // Si la taille demandée est supérieure, on augmente la taille
    if (newSize > list.size()) {
      Object[] oSrcData = list.toArray(new Object[list.size()]);
      Object[] oDestData = list.toArray(new Object[list.size()]);
      System.arraycopy(oSrcData, 0, oDestData, 0, list.size());
      list.clear();
      for (Object o : oDestData) {
        list.add(o);
      }
    } else {
      // Sinon, on enlève les fins de lignes
      for (int i = newSize; i < list.size(); i++) {
        list.remove(newSize);
      }
    }
  }

  @Override
  public String getColumnName(int iColumn) {
    return iColumn < columnInfos.size() ? columnInfos.get(iColumn)
        .getColumnName() : null;
  }

  public String[] getColumnNames() {
    Vector<String> vNoms = new Vector<String>();
    for (int iCol = 0; iCol < columnInfos.size(); iCol++)
      vNoms.add(columnInfos.get(iCol).getColumnName());

    String[] sColumnNames = vNoms.toArray(new String[columnInfos.size()]);
    return sColumnNames;
  }

  @Override
  public Class<?> getColumnClass(int iColumn) {
    return columnInfos.get(iColumn).getColumnClasse();
  }

  public int getColumnCount() {
    return columnInfos.size();
  }

  public void addRow() {
    addRow(new ArrayList<Object>(getColumnCount()));
  }

  public void addRow(List<Object> vRow) {
    insertRow(getRowCount(), vRow);
  }

  public void insertRow(int iIndex, List<Object> vRow) {
    List<Object> vRowToInsert = vRow;

    // Si la ligne a inserer est nulle, elle est consideree comme vierge.
    if (vRowToInsert == null)
      vRowToInsert = new ArrayList<Object>(columnInfos.size());

    ajusterTailleLigne(vRowToInsert);
    vData.add(iIndex, vRowToInsert);
    fireTableRowsInserted(iIndex, iIndex);
  }

  public void removeRow(int iIndexRow) {
    // TODO: supprimer si c'est possible ! Attention aux cles !!!!

    for (MIRemovedRowListener mrrl : vRemovedRowListener) {
      mrrl.removedRow(iIndexRow);
    }

    vData.remove(iIndexRow);
    fireTableRowsDeleted(iIndexRow, iIndexRow);
  }

  protected void ajusterTailleLigne(List<Object> vLigne) {
    for (int i = vLigne.size(); i < columnInfos.size(); i++)
      vLigne.add(null);
  }

  public int getRowCount() {
    return vData.size();
  }

  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }

  public Object getValueAt(int rowIndex, String sColonne) {
    // le +1 fait exploser dans le cas ou la colonne n'existe pas
    for (int iColonne = 0; iColonne < columnInfos.size() + 1; iColonne++)
      if (columnInfos.get(iColonne).getColumnName().equals(sColonne))
        return getValueAt(rowIndex, iColonne);
    return null;
  }

  public Object getValueAt(int rowIndex, int columnIndex) {
    if (rowIndex < 0 || columnIndex < 0)
      return null;
    return vData.get(rowIndex).get(columnIndex);
  }

  public void setValueAt(Object value, int row, String sColonne) {
    for (int iColonne = 0; iColonne < columnInfos.size(); iColonne++)
      if (columnInfos.get(iColonne).getColumnName().equals(sColonne)) {
        setValueAt(value, row, iColonne);
        break;
      }
  }

  @Override
  public void setValueAt(Object value, int row, int col) {
    oOldValue = getValueAt(row, col);

    // Si pas de changement, aucune modif a deplorer
    if (oOldValue == null && (value == null || value.equals("")))
      return;
    if (oOldValue != null && oOldValue.equals(value))
      return;

    vData.get(row).set(col, value);
    fireTableCellUpdated(row, col);
  }

  public void clear() {
    columnInfos = new Vector<MInfoColonne>();
    vData = new ArrayList<List<Object>>();
    fireTableDataChanged();
    fireTableStructureChanged();
  }

  public void clearData() {
    vData = new ArrayList<List<Object>>();
    fireTableRowsDeleted(0, getRowCount());
  }

  public void addRemovedRowListener(MIRemovedRowListener rrListener) {
    if (rrListener != null && !vRemovedRowListener.contains(rrListener))
      vRemovedRowListener.add(rrListener);
  }

  public void removeRemovedRowListener(MIRemovedRowListener rrListener) {
    if (rrListener != null && vRemovedRowListener.contains(rrListener))
      vRemovedRowListener.remove(rrListener);
  }

  protected boolean containsColumn(String sColumnName) {
    // En cas de surcharge de addColumn()
    construitVColumnName();

    return columnNames.contains(sColumnName);
  }

  private void construitVColumnName() {
    if (columnNames == null)
      for (int iCol = 0; iCol < columnInfos.size(); iCol++)
        columnNames.add(columnInfos.get(iCol).getColumnName());
  }

  protected int getIndexOfColumn(String sColumnName) {
    // En cas de surcharge de addColumn()
    construitVColumnName();

    return columnNames.indexOf(sColumnName);
  }

  public MInfoColonne getInfoCol(int iCol) {
    return columnInfos.get(iCol);
  }

  public MInfoColonne getInfoCol(String sCol) {
    return getInfoCol(getIndexOfColumn(sCol));
  }

  public void setInfoCol(String sCol, MInfoColonne mInfo) {
    columnInfos.set(getIndexOfColumn(sCol), mInfo);
  }
}