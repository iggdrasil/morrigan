/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJTable.java - CREATION : 2004/07/24
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.JTableHeader;

import morrigan.graphique.MIQuadComponent;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MJTable extends JTable implements MIQuadComponent {

  private int iLargeurCell = 0;

  private int iHauteurCell = 0;

  public MJTable(MTableModele tm) {
    super(tm);
  }

  public MTableModele getMTableModele() {
    return (MTableModele) getModel();
  }

  public MJTable(MTableModele tm, int iLargeurCell, int iHauteurCell) {
    super(tm);
    this.iLargeurCell = iLargeurCell;
    this.iHauteurCell = iHauteurCell;
  }

  @Override
  protected JTableHeader createDefaultTableHeader() {
    return new JTableHeader(columnModel) {
      @Override
      public String getToolTipText(MouseEvent e) {
        java.awt.Point p = e.getPoint();
        int index = columnModel.getColumnIndexAtX(p.x);
        return getToolTip(columnModel.getColumn(index).getModelIndex());
      }
    };
  }

  protected String getToolTip(int iColonne) {
    return getColumnName(iColonne);
  }

  public int getHauteurCell() {
    return iHauteurCell;
  }

  public int getLargeurCell() {
    return iLargeurCell;
  }

  public void addColumn(MInfoColonne mInfo) {
    getMTableModele().addColumn(mInfo);
  }

  public void addRow(Vector<Object> vRow) {
    insertRow(getRowCount(), vRow);
  }

  public void appendRow(Vector<Object> vRow) {
    insertRow(getSelectedRow() > 0 ? getSelectedRow() : getRowCount(), vRow);
  }

  public void insertRow(int iIndex, Vector<Object> vRow) {
    getMTableModele().insertRow(iIndex, vRow);
  }

  public void removeRow(int iIndexRow) {
    getMTableModele().removeRow(iIndexRow);
  }

  public void removeSelectedRows() {
    // Decalage d'index du a la suppression [l'index diminue de 1 a chaque
    // suppression d'une
    // ligne situee apres. Il faut donc compenser...
    // NB: l'ordre des lignes selectionnees est croissant
    int iDecalage = 0;
    int[] iIndexRows = getSelectedRows();

    for (int i = 0; i < iIndexRows.length; i++)
      removeRow(iIndexRows[i] - (iDecalage++));
  }

  public void selectRow(int iIndexRow) {
    ListSelectionModel lsm = getSelectionModel();
    lsm.clearSelection();
    lsm.addSelectionInterval(iIndexRow, iIndexRow);
  }
}