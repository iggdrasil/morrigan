package morrigan.graphique.table.tree.renderer;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;

public class TreeTableCellRenderer extends JTree implements TableCellRenderer {

  private final JTable table;

  protected int visibleRow;

  public TreeTableCellRenderer(JTable table) {
    this.table = table;
  }

  public void setBounds(int x, int y, int w, int h) {
    super.setBounds(x, 0, w, table.getHeight());
  }

  public void paint(Graphics g) {
    g.translate(0, -visibleRow * getRowHeight());
    super.paint(g);
  }

  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {
    visibleRow = row;
    return this;
  }
}