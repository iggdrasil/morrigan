/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MInfoColonnePlus.java - CREATION : 2005/12/26
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

import java.util.Vector;

import morrigan.graphique.field.util.MIField;

public class MInfoColonnePlus extends MInfoColonne {
  private final Vector<MIField> vColumnsFields = new Vector<MIField>();

  private boolean bMandatory = false;

  public void addField(MIField mif) {
    vColumnsFields.add(mif);
  }

  public Vector<MIField> getFields() {
    return vColumnsFields;
  }

  public boolean isMandatory() {
    return bMandatory;
  }

  public void setMandatory(boolean mandatory) {
    bMandatory = mandatory;
  }

}
