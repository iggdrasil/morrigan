/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MDonnees.java - CREATION : 2004/10/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.table;

import java.util.List;

import morrigan.graphique.field.util.MIField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MDonnees extends MTableModele {
  protected int iLigneActive = 0;

  /**
   * Ce booleen permet de specifier si une valeur envoyee par un champ par un
   * setValueAt doit etre enregistree (=>refresh des autres champs associes a la
   * colonne) ou non (rien a faire)
   */
  private boolean bRefreshEnCours = false;

  public MDonnees(String[] sNomsColonnes) {
    super(sNomsColonnes);
  }

  @Override
  protected MInfoColonne createInfoCol() {
    return new MInfoColonnePlus();
  }

  public void setValueAt(Object oValue, String sNomColonne) {
    if (!bRefreshEnCours) {
      // MLog.debug("MDonnees", "setValueAt", "Nouvelle valeur ? sauver:" +
      // oValue);
      applyNewValue(oValue, sNomColonne);
    }
    // else
    // MLog.debug("MDonnees", "setValueAt", "Pas une nouvelle valeur :" +
    // oValue);
  }

  protected void applyNewValue(Object oValue, String sNomColonne) {
    setValueAt(oValue, iLigneActive, sNomColonne);
  }

  public void associate(MIField mfChamp, String sNomColonne) {
    final int iCol = getIndexOfColumn(sNomColonne);
    final List<MIField> vFields = ((MInfoColonnePlus) columnInfos.get(iCol))
        .getFields();

    // Si on trouve le champ parmi les champs deja associes a la colonne, on ne
    // fait rien.
    if (vFields.contains(mfChamp))
      return;

    // Sinon, on rajoute le champ
    vFields.add(mfChamp);
    mfChamp.associate(this, sNomColonne);
  }

  @Override
  public void fireTableCellUpdated(int iLigne, int iColonne) {
    List<MIField> vFields = ((MInfoColonnePlus) columnInfos.get(iColonne))
        .getFields();

    // Si la ligne mise a jour est la ligne active, les champs associes sont
    // aussi mis a jour
    if (iLigne == iLigneActive && vFields != null)
      refreshField(iColonne);
    super.fireTableCellUpdated(iLigne, iColonne);
  }

  public int getLigneActive() {
    return iLigneActive;
  }

  public void setLigneActive(int iLigneActive) {
    this.iLigneActive = iLigneActive;
    refreshFields();
  }

  public boolean isLigneVide(int iLigne) {
    for (Object o : vData.get(iLigne)) {
      if (o != null && !o.equals("")) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void removeRow(int iIndexRow) {
    super.removeRow(iIndexRow);
    setLigneActive(iIndexRow > 0 ? iIndexRow - 1 : 0);
  }

  /**
   * Raffraichir les champs avec les valeurs de la ligne active
   * 
   * @param iColonne
   */
  private void refreshField(int iColonne) {
    bRefreshEnCours = true;
    // MLog.debug("MDonnees", "refreshField", "Raffraichissement du champ " +
    // iColonne);

    List<MIField> vFields = ((MInfoColonnePlus) columnInfos.get(iColonne))
        .getFields();
    if (vFields == null)
      return;

    // Chaque champ ayant ete associe a une colonne est mis a jour
    for (MIField field : vFields)
      field.setValue(getValueAt(iLigneActive, iColonne));
    bRefreshEnCours = false;
  }

  public void refreshField(String sColonne) {
    refreshField(getIndexOfColumn(sColonne));
  }

  /**
   * Raffraichir les champs avec les valeurs de la ligne active
   */
  private void refreshFields() {
    bRefreshEnCours = true;
    // MLog.debug("MDonnees", "refreshFields", "Raffraichissement des champs");
    // Pour chaque colonne, ...
    for (int iColonne = 0; iColonne < columnInfos.size(); iColonne++) {
      // MLog.debug("MDonnees", "refreshFields", "Colonne traitee:" +
      // getColumnName(iColonne));
      // ... on retrouve les champs associes
      List<MIField> vFields = ((MInfoColonnePlus) columnInfos.get(iColonne))
          .getFields();
      if (vFields == null)
        continue;
      // ... et on les met a jour
      for (MIField field : vFields) {
        // MLog.debug("MDonnees", "refreshFields", "Champ mis a jour:" +
        // vFields.get(iField));
        field.setValue(getValueAt(iLigneActive, iColonne));
      }
    }
    bRefreshEnCours = false;
    // MLog.debug("MDonnees", "refreshFields", "Raffraichissement des champs |
    // FIN");
  }

  @Override
  public String toString() {
    return new StringBuffer("MDonnees").append(vData).toString();
  }

  public List<MIField> getFields(String sNomChamp) {
    return ((MInfoColonnePlus) columnInfos.get(getIndexOfColumn(sNomChamp)))
        .getFields();
  }

  public void setData(List<List<Object>> vData) {
    this.vData = vData;
  }

}