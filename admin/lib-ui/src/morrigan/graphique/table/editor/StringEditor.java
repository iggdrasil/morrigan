package morrigan.graphique.table.editor;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;

public class StringEditor extends DefaultCellEditor {

  public StringEditor() {
    super(new JTextField());
  }

}