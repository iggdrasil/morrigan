package morrigan.graphique.table.editor;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;

public class CheckBoxEditor extends DefaultCellEditor {

  public CheckBoxEditor() {
    super(new JCheckBox());
    setClickCountToStart(2);
  }
}
