/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MDateField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import morrigan.graphique.Colors;
import morrigan.graphique.field.util.MFormattedField;
import morrigan.log.MLog;
import morrigan.preference.MPreferences;

import org.apache.commons.lang.StringUtils;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MDateField extends MFormattedField {

  public enum DATEFORMAT {
    DATE, HOUR, DATEHOUR;

    public String getFormat() {
      return MPreferences.getInstance().getFormat(this);
    }

    public String getMask() {
      return MPreferences.getInstance().getMask(this);
    }
  }

  private DATEFORMAT dateFormat;
  private Color initialBgColor;

  private DefaultFormatterFactory defaultFormatterFactory;

  private MaskFormatter maskFormatter;

  private char placeholder = '_';

  public MDateField(int hauteurCell, int largeurCell, DATEFORMAT df) {
    super(hauteurCell, largeurCell);

    this.initialBgColor = getBackground();
    this.dateFormat = df;
    
    setFormatterFactory(createFormatterFactory());
    setInputVerifier(new MDateVerifier());
  }
  
  public MDateField(int iHauteurCell, int iLargeurCell, DATEFORMAT df,
      boolean bMandatory) {
    this(iHauteurCell, iLargeurCell, df);
    setMandatory(bMandatory);
  }
  
  @Override
  protected AbstractFormatterFactory createFormatterFactory() {
    if (this.dateFormat == null) {
      return null;
    }
    
    try {
      String mask = this.dateFormat.getMask();
      maskFormatter = new MyMaskFormatter(mask);
      maskFormatter.setPlaceholderCharacter(placeholder);
      maskFormatter.setValidCharacters("0123456789");
      
      defaultFormatterFactory = new DefaultFormatterFactory();
      defaultFormatterFactory.setDefaultFormatter(maskFormatter);
      defaultFormatterFactory.setDisplayFormatter(maskFormatter);
      defaultFormatterFactory.setEditFormatter(maskFormatter);
      defaultFormatterFactory.setNullFormatter(maskFormatter);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return defaultFormatterFactory;
  }

  /**
   * Use the date(/hour) format with the mask placeholder.
   * @return
   */
  private String extractFormattedMask() {
    return dateFormat.getMask().replace('#',
        maskFormatter.getPlaceholderCharacter());
  }

  /**
   * Date verifier.
   * @author Armel
   */
  private class MDateVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent input) {
      boolean valid = false;
      if (input instanceof MDateField) {
        MDateField dateField = (MDateField) input;
        valid = checkValidity(dateField);
        adaptGraphic(dateField, valid);
      }
      return valid;
    }

    private void adaptGraphic(MDateField dateField, boolean valid) {
      if (valid) {
        dateField.setToolTipText(StringUtils.EMPTY);
        dateField.setBackground(initialBgColor);
      } else {
        dateField.selectAll();
        dateField
            .setToolTipText("<HTML><BODY BGCOLOR=\"RED\">Ce n'est pas une date !</BODY></HTML>");
        dateField.setBackground(Colors.getErrorBgColor());
      }
    }

    /**
     * Check validity of value.
     * @param dateField
     * @return
     */
    boolean checkValidity(MDateField dateField) {
      try {
        String text = dateField.getText();
        String dateMask = extractFormattedMask();
        if (StringUtils.isNotBlank(text) && !text.equals(dateMask)) {
          String formatSys = dateFormat.getFormat();
          Date date = new SimpleDateFormat(formatSys).parse(text);
          MLog.debug("MDateVerifier", "checkValidity", "parsing of '" + text
              + "' => '" + date + "'");
        }
        return true;
      } catch (ParseException e) {
        MLog.debug("MDateField", "processFocusEvent", "Impossible de parser la date : '"
            + getText());
      }
      return false;
    }
  }

  /**
   * Mask formatter adapted to accept the <code>null</code> value and to fix the inserted date.
   * @author Armel
   */
  private class MyMaskFormatter extends MaskFormatter {

    public MyMaskFormatter(String mask) throws ParseException {
      super(mask);
    }

    @Override
    public String valueToString(Object value) throws ParseException {
      String toString = super.valueToString(value);
      // MLog.debug("MyMaskFormatter", "valueToString", toString);
      return toString;
    }

    @Override
    public Object stringToValue(String value) throws ParseException {
      String formattedMask = extractFormattedMask();
      if (value == null || formattedMask.equals(value)) {
        return null;
      }
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat
          .getFormat());
      Date date = simpleDateFormat.parse(value);
      String sDate = simpleDateFormat.format(date);
      Object toValue = super.stringToValue(sDate);
      // MLog.debug("MyMaskFormatter", "stringToValue",
      // String.valueOf(toValue));
      return toValue;
    }

  }

  public String getFormatSys() {
    return dateFormat.getFormat();
  }

}