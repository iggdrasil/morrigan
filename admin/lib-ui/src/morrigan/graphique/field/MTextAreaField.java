/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MTextAreaField.java - CREATION : 2004/09/81
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;

import morrigan.graphique.Colors;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.bouton.MJButton;
import morrigan.graphique.field.util.MDialogText;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.field.util.MTextArea;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.table.MDonnees;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MTextAreaField extends MJPanel implements MIQuadComponent,
    MIField, ActionListener {
  protected int hauteurCell = 1;

  protected int largeurCell = 1;

  private boolean bMandatory = false;

  private String linkedColumn = null;

  public JTextComponent textComponent = null;

  private MTextArea mta = null;

  private MTextField mtf = null;

  private MJButton mjbDetail = null;

  private final boolean boutonDetail;

  public MTextAreaField(String sTexte, int iHauteurCell, int iLargeurCell) {
    this(true, sTexte, iHauteurCell, iLargeurCell);
  }

  public MTextAreaField(boolean boutonDetail, String sTexte, int iHauteurCell,
      int iLargeurCell) {
    this.boutonDetail = boutonDetail;
    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    if (boutonDetail) {
      textComponent = mtf = createLightTextField();
      mtf.setEnabled(true);
      mtf.setEditable(true);
      mjbDetail = new MJButton("...");
      mjbDetail.addActionListener(this);
      add(mtf);
      add(mjbDetail);
    } else {
      setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
      textComponent = mta = new MTextArea(sTexte);
      mta.setEnabled(true);
      mta.setEditable(true);
      add(mta);
    }

    this.hauteurCell = iHauteurCell;
    this.largeurCell = iLargeurCell;
  }

  protected MTextField createLightTextField() {
    return new MTextField(1, 1);
  }

  public MTextAreaField(String sTexte, int iHauteurCell, int iLargeurCell,
      boolean bMandatory) {
    this(sTexte, iHauteurCell, iLargeurCell);
    this.setMandatory(bMandatory);
  }

  public MTextAreaField(int iHauteurCell, int iLargeurCell) {
    this("", iHauteurCell, iLargeurCell);
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

  public void setMandatory(boolean bMandatory) {
    this.bMandatory = bMandatory;
    if (bMandatory) {
      Color mandatoryColor = Colors.getMandatoryColor();
      setBorder(new LineBorder(mandatoryColor));
    }
  }

  public boolean isMandatory() {
    return bMandatory;
  }

  public void associate(MDonnees mdAss, String aLinkedColumn) {
    ((MIField) textComponent).associate(mdAss, aLinkedColumn);

    this.linkedColumn = aLinkedColumn;
  }

  public void setValue(Object value) {
    if (textComponent instanceof MIField) {
      // Ce qui doit TOUJOURS etre le cas
      ((MIField) textComponent).setValue(value);
    }
  }

  /**
   * Retourne la valeur du champ (= le texte contenu dans ce champ)
   */
  public Object getValue() {
    return getText();
  }

  public String getText() {
    final String sTexte = textComponent.getText();
    return sTexte == null ? "" : sTexte;
  }

  /**
   * Retourner le nom de la colonne en base.
   * 
   * @return Nom colonne
   */
  public String getColonne() {
    return linkedColumn;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == mjbDetail) {
      MDialogText jdDetail = new MDialogText((MIField) textComponent);
      jdDetail.setVisible(true);
    }
  }

  public boolean isScrollBarVisible() {
    return mta != null;
  }

  @Override
  public void setBackground(Color bg) {
    if (textComponent != null) {
      textComponent.setBackground(bg);
    }
  }

  public MIField getField() {
    return boutonDetail ? mtf : mta;
  }

  /** {@inheritDoc} */
  public MDonnees getDonnees() {
    return (boutonDetail ? mtf : mta).getDonnees();
  }

}