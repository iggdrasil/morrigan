/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MPasswordField.java - CREATION : 2009/01/31
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JPasswordField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import morrigan.graphique.Colors;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.table.MDonnees;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MPasswordField extends JPasswordField implements MIQuadComponent,
    MIField {

  private String columnName = null;

  private Object oldValue = null;

  private final int hauteurCell;

  private final int largeurCell;

  private MDonnees md;

  private boolean mandatory;

  public MPasswordField(int hauteurCell, int largeurCell) {
    super();
    this.hauteurCell = hauteurCell;
    this.largeurCell = largeurCell;

    this.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        applyNewValue();
      }
    });
    this.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        applyNewValue();
      }
    }); 
  }

  public MPasswordField(int hauteurCell, int largeurCell, boolean mandatory) {
    this(hauteurCell, largeurCell);
    this.setMandatory(mandatory);
  }

  /** {@inheritDoc} */
  public void associate(MDonnees mdAsso, String sColAsso) {
    mdAsso.associate(this, sColAsso);
    this.md = mdAsso;
    this.columnName = sColAsso;
  }

  public String getColonne() {
    return columnName;
  }

  public MDonnees getDonnees() {
    return md;
  }

  public Object getValue() {
    return String.valueOf(getPassword());
  }

  public boolean isMandatory() {
    return mandatory;
  }

  public void setMandatory(boolean mandatory) {
    this.mandatory = mandatory;
    Border bb = BorderFactory.createLoweredBevelBorder();
    if (mandatory) {
      Color mandatoryColor = Colors.getMandatoryColor();
      bb = BorderFactory.createCompoundBorder(new LineBorder(mandatoryColor),
          bb);
    }

    this.setBorder(bb);
  }
  
  private void applyNewValue() {
    // MLog.debug("MFormattedField", "applyNewValue", "oOldValue=" + oldValue
    // + " |     getValue()=" + getValue());
    if ((oldValue == null && getValue() != null)
        || (oldValue != null && !oldValue.equals(getValue()))) {
      if (md != null && columnName != null) {
        md.setValueAt(getValue(), columnName);
      }
    }
    oldValue = getValue();
  }

  public void setValue(Object value) {
    this.oldValue = value;
    setText(value == null ? "" : value.toString());
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

}