package morrigan.graphique.field.format;

import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

public class MEditNumberFormatter extends NumberFormatter {

  public MEditNumberFormatter(DecimalFormat editionDecimalFormat) {
    super(editionDecimalFormat);
//    setFormat(null);
  }

  @Override
  public Object stringToValue(String text) throws ParseException {
    // Empty field
    if (text != null && text.length() == 0) {
      return null;
    }

    Object stringToValue = super.stringToValue(text);
    return stringToValue;
  }

  @Override
  public String valueToString(Object value) throws ParseException {
    String valueToString = super.valueToString(value);
    return valueToString;
  }
}
