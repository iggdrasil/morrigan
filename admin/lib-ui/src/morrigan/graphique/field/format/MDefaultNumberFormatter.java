package morrigan.graphique.field.format;

import java.text.ParseException;

import javax.swing.text.NumberFormatter;

public class MDefaultNumberFormatter extends NumberFormatter {
  
  @Override
  public Object stringToValue(String text) throws ParseException {
    Object stringToValue = super.stringToValue(text);
    return stringToValue;
  }

  @Override
  public String valueToString(Object value) throws ParseException {
    String valueToString = super.valueToString(value);
    return valueToString;
  }
}
