/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIntegerField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import morrigan.graphique.field.util.MNumberField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MIntegerField extends MNumberField {
  public MIntegerField(int iHauteurCell, int iLargeurCell) {
    super(iHauteurCell, iLargeurCell, 0);
  }

  public MIntegerField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    super(iHauteurCell, iLargeurCell, bMandatory);
  }

  @Override
  protected Class<?> getClazz() {
    return Long.class;
  }
}