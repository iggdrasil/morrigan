/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEModeSaisie.java - CREATION : 2005/11/08
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field.util;

public enum MEModeSaisie {
  SAISIE,
  RECHERCHE
}