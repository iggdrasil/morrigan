/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MSimpleField.java - CREATION : 2005/11/01
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field.util;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import morrigan.graphique.Colors;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.table.MDonnees;

/**
 * Champ gere manuellement avec un DocumentListener pour intervenir a chaque
 * saisie d'un caractere.
 * 
 * @author armel
 */
public class MSimpleField extends JTextField implements MIQuadComponent,
    MIField, ActionListener {
  protected enum CHAMP {
    TEXTE,
    ENTIER,
    DECIMAL
  }

  protected int hauteurCell = 1;

  protected int largeurCell = 1;

  private boolean bMandatory = false;

  private MDonnees md = null;

  private String columnName = null;

  private Object oValue = null;

  private Object oOldValue = null;

  public MSimpleField(int iHauteur, int iLargeur) {
    this.hauteurCell = iHauteur;
    this.largeurCell = iLargeur;
    setMandatory(bMandatory);
    addActionListener(this);
  }

  public void associate(MDonnees mdAsso, String sColAsso) {
    mdAsso.associate(this, sColAsso);
    this.md = mdAsso;
    this.columnName = sColAsso;
  }

  public Object getValue() {
    return getText();
  }

  public boolean isMandatory() {
    return bMandatory;
  }

  public void setMandatory(boolean bMandatory) {
    this.bMandatory = bMandatory;
    Border bb = BorderFactory.createLoweredBevelBorder();
    if (bMandatory) {
      Color mandatoryColor = Colors.getMandatoryColor();
      bb = BorderFactory.createCompoundBorder(new LineBorder(mandatoryColor),
          bb);
    }

    this.setBorder(bb);
  }

  public void setValue(Object value) {
    this.oOldValue = value;
    this.oValue = value;
    setText(oValue == null ? "" : oValue.toString());
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

  @Override
  protected void processFocusEvent(FocusEvent e) {
    super.processFocusEvent(e);
    if (e.getID() == FocusEvent.FOCUS_GAINED) {
      // Gain de focus => selection complete (si texte)
      if (getText().length() != 0) {
        this.setSelectionStart(0);
        this.setSelectionEnd(getText().length());
      }
    } else if (e.getID() == FocusEvent.FOCUS_LOST) {
      // MLog.debug("MField", "processFocusEvent", "FOCUS_LOST => Nlle valeur:"
      // +getValue());
      applyNewValue();
    }
  }

  private void applyNewValue() {
    if ((oOldValue == null && !getText().equals(""))
        || (oOldValue != null && !oOldValue.toString().equals(getText()))) {
      // MLog.debug("MSimpleField", "applyNewValue", "oOldValue=" + oOldValue +
      // " |
      // getText()=" + getText());
      setValue(getText());
      if (md != null && columnName != null) {
        md.setValueAt(getValue(), columnName);
      }
    }
    oOldValue = getValue();
  }

  @Override
  public String getText() {
    final String sTexte = super.getText();
    return sTexte == null ? "" : sTexte;
  }

  /** {@inheritDoc} */
  public void actionPerformed(ActionEvent e) {
    applyNewValue();
  }

  /** {@inheritDoc} */
  public String getColonne() {
    return columnName;
  }
  
  /** {@inheritDoc} */
  public MDonnees getDonnees() {
    return md;
  }
}
