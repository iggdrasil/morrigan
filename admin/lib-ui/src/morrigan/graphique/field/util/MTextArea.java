/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MTextAreaField.java - CREATION : 2003/12/06
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.graphique.field.util;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.FocusEvent;

import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.table.MDonnees;

/**
 * Cette zone de texte permet d'ecrire des textes sur plusieurs lignes.
 * 
 * @author armel
 */
public class MTextArea extends JTextArea implements MIQuadComponent, MIField {
  private boolean bMandatory = false;

  private MDonnees md = null;

  private String columnName = null;

  private Object oOldValue = null;

  /**
   * Constructeur sans texte, avec une simple et fine bordure.
   */
  public MTextArea() {
    super();
    this.setLineWrap(true);
    this.setWrapStyleWord(true);
    this.setTabSize(2);

    this.creerBordureVide(2);
  }

  /**
   * Constructeur avec un texte.
   * 
   * @param sTexte texte d'origine
   */
  public MTextArea(String sTexte) {
    this();
    setText(sTexte);
    setEnabled(true);
    setEditable(true);
  }

  /**
   * Creer une bordure vide uniforme tout autour de la zone de texte.
   * 
   * @param bordure largeur de la bordure
   */
  private void creerBordureVide(int bordure) {
    this.setMargin(new Insets(bordure, bordure, bordure, bordure));
  }

  public int getHauteurCell() {
    return 1;
  }

  public int getLargeurCell() {
    return 1;
  }

  public void setMandatory(boolean bMandatory) {
    this.bMandatory = bMandatory;
    setBorder(bMandatory ? new LineBorder(Color.RED) : null);
  }

  public boolean isMandatory() {
    return bMandatory;
  }

  public void associate(MDonnees mdAss, String sColAss) {
    this.md = mdAss;
    this.columnName = sColAss;
    md.associate(this, columnName);
  }

  public void setValue(Object value) {
    oOldValue = value;
    if (md != null && columnName != null) {
      md.setValueAt(value, columnName);
    }
    setText(value == null ? "" : value.toString());
  }

  /**
   * Retourne la valeur du champ (= le texte contenu dans ce champ)
   */
  public Object getValue() {
    return getText();
  }

  @Override
  protected void processFocusEvent(FocusEvent e) {
    super.processFocusEvent(e);
    if (e.getID() == FocusEvent.FOCUS_GAINED) {
      // Gain de focus => selection complete (si texte)
      if (getText().length() != 0) {
        this.setSelectionStart(0);
        this.setSelectionEnd(getText().length());
      }
    } else if (e.getID() == FocusEvent.FOCUS_LOST) {
      applyNewValue();
    }
  }

  private void applyNewValue() {
    if ((oOldValue == null && !getText().equals(""))
        || (oOldValue != null && !oOldValue.toString().equals(getText()))) {
      // MLog.debug("MTextArea", "applyNewValue", "oOldValue=" + oOldValue + " |
      // getValue()="
      // + getValue());
      setValue(getText());
      if (md != null && columnName != null) {
        md.setValueAt(getValue(), columnName);
      }
    }
    oOldValue = getValue();
  }

  /** {@inheritDoc} */
  public String getColonne() {
    return columnName;
  }
  
  /** {@inheritDoc} */
  public MDonnees getDonnees() {
    return md;
  }

}