/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIField.java - CREATION : 2004/09/18
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field.util;

import morrigan.graphique.table.MDonnees;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public interface MIField {
  void setMandatory(boolean bMandatory);

  /**
   * @param enabled <code>true</code> if the field is enabled; <code>false</code> if not.
   */
  void setEnabled(boolean enabled);

  /**
   * @return <code>true</code> if the field is mandatory; <code>false</code> if not.
   */
  boolean isMandatory();

  /**
   * Link to a MDonnees column.
   * @param md
   * @param columnName
   */
  void associate(MDonnees md, String columnName);

  /**
   * @return the linked column in the MDonnees
   */
  String getColonne();
  
  /**
   * @return the data set.
   */
  MDonnees getDonnees();

  /**
   * @param value a new value
   */
  void setValue(Object value);

  /**
   * @return the current value
   */
  Object getValue();
}