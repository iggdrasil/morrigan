/*
 * Cree le 13 mars 2006
 * Ce fichier appartient au projet Morrigan.
 */
package morrigan.graphique.field.util;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;

import morrigan.graphique.bouton.MJButton;
import morrigan.graphique.field.MTextAreaField;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.log.MLog;

public class MDialogText extends JDialog implements ActionListener {
  private MJButton mjbValider = new MJButton(" Valider ");

  private MJButton mjbAnnuler = new MJButton(" Annuler ");

  private MIField jtcSource = null;

  private MTextAreaField mtafEdition = null;

  public MDialogText(MIField jtcSource) {
    super((Frame) null, "Edition", true);
    setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));

    this.jtcSource = jtcSource;
    Object valueAt = jtcSource.getDonnees().getValueAt(jtcSource.getDonnees().getLigneActive(), jtcSource.getColonne());
    MLog.debug("MDialogText", "MDialogText", "initial value = " + valueAt);
    final Object oTexteOrigine = jtcSource.getValue();
    MLog.debug("MDialogText", "MDialogText", "oTexteOrigine= " + oTexteOrigine);
    mtafEdition = new MTextAreaField(false, oTexteOrigine == null ? ""
        : oTexteOrigine.toString(), 1, 1);

    MJPanel mjpBoutons = new MJPanel(MJPanel.BORDURE.MENU);
    mjpBoutons.setLayout(new BoxLayout(mjpBoutons, BoxLayout.LINE_AXIS));
    mjpBoutons.add(Box.createHorizontalGlue());
    mjpBoutons.add(mjbValider);
    mjpBoutons.add(Box.createRigidArea(new Dimension(10, 0)));
    mjpBoutons.add(mjbAnnuler);
    mjpBoutons.add(Box.createHorizontalGlue());

    mjbValider.addActionListener(this);
    mjbAnnuler.addActionListener(this);

    add(new MJScrollPane(mtafEdition));
    add(mjpBoutons);
    setMinimumSize(new Dimension(400, 200));
    setPreferredSize(new Dimension(400, 200));
    pack();

    // Position de la fenetre
    final Rectangle screenRect = new JFrame().getGraphicsConfiguration()
        .getBounds();
    setLocation(screenRect.x + screenRect.width / 2 - getSize().width / 2,
        screenRect.y + screenRect.height / 2 - getSize().height / 2);
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == mjbValider) {
      jtcSource.setValue(mtafEdition.getValue());
    }

    this.setVisible(false);
    this.dispose();
  }
}
