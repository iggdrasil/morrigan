/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MFormattedField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field.util;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import morrigan.graphique.Colors;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.table.MDonnees;

/**
 * Champ gerant automatiquement le formattage de la valeur.
 * 
 * @author armel
 */
public abstract class MFormattedField extends JFormattedTextField implements
    MIQuadComponent, MIField {

  private int hauteurCell = 1;

  private int largeurCell = 1;

  private boolean mandatory = false;

  private MDonnees md = null;

  private String columnName = null;

  private Object oldValue = null;

  public MFormattedField(int hauteurCell, int largeurCell) {
    this();
    this.hauteurCell = hauteurCell;
    this.largeurCell = largeurCell;
  }

  public MFormattedField() {
    super();
    setMandatory(mandatory);
    this.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        applyNewValue();
      }
    });

    AbstractFormatterFactory defaultFormatterFactory = createFormatterFactory();
    setFormatterFactory(defaultFormatterFactory);
  }

  abstract protected AbstractFormatterFactory createFormatterFactory();

  @Override
  public void commitEdit() throws ParseException {
    super.commitEdit();
    // Method not called if no formatter set !!!
    applyNewValue();
  }

  public int getHauteurCell() {
    return hauteurCell;
  }

  public int getLargeurCell() {
    return largeurCell;
  }

  public void setMandatory(boolean bMandatory) {
    this.mandatory = bMandatory;
    Border bb = BorderFactory.createLoweredBevelBorder();
    if (bMandatory) {
      Color mandatoryColor = Colors.getMandatoryColor();
      bb = BorderFactory.createCompoundBorder(new LineBorder(mandatoryColor),
          bb);
    }

    this.setBorder(bb);
  }

  /** {@inheritDoc} */
  public boolean isMandatory() {
    return mandatory;
  }

  /** {@inheritDoc} */
  public void associate(MDonnees mdAsso, String sColAsso) {
    mdAsso.associate(this, sColAsso);
    this.md = mdAsso;
    this.columnName = sColAsso;
  }
  
  @Override
  public void setValue(Object value) {
    super.setValue(value);
    applyNewValue();
  }

  private void applyNewValue() {
    // MLog.debug("MFormattedField", "applyNewValue", "oOldValue=" + oldValue
    // + " |     getValue()=" + getValue());
    if ((oldValue == null && getValue() != null)
        || (oldValue != null && !oldValue.equals(getValue()))) {
      if (md != null && columnName != null) {
        md.setValueAt(getValue(), columnName);
      }
    }
    oldValue = getValue();
  }

  /** {@inheritDoc} */
  public String getColonne() {
    return columnName;
  }

  /** {@inheritDoc} */
  public MDonnees getDonnees() {
    return md;
  }
}