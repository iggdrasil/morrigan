package morrigan.graphique.field.util;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import morrigan.graphique.field.format.MEditNumberFormatter;
import morrigan.log.MLog;

public abstract class MNumberField extends MFormattedField {

  private double minValue = -Double.MAX_VALUE;

  private double maxValue = Double.MAX_VALUE;

  private NumberFormatter defaultFormatter;

  private NumberFormatter editFormatter;

  private DecimalFormat editionDecimalFormat;

  private DecimalFormat displayDecimalFormat;

  private Popup popup;

  private int precision;

  public MNumberField(int hauteurCell, int largeurCell) {
    super(hauteurCell, largeurCell);

    setInputVerifier(new MyInputVerifier());
    addKeyListener(new MyKeyListener());
  }

  public MNumberField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    this(iHauteurCell, iLargeurCell);
    setMandatory(bMandatory);
  }

  public MNumberField(int iHauteurCell, int iLargeurCell, int precision,
      boolean bMandatory) {
    this(iHauteurCell, iLargeurCell, precision);
    setMandatory(bMandatory);
  }

  public MNumberField(int iHauteurCell, int iLargeurCell, int precision) {
    this(iHauteurCell, iLargeurCell);
    this.precision = precision;
    AbstractFormatterFactory defaultFormatterFactory = createFormatterFactory();
    setFormatterFactory(defaultFormatterFactory);
  }

  @Override
  protected AbstractFormatterFactory createFormatterFactory() {
    // char decimalSeparator =
    // displayDecimalFormat.getDecimalFormatSymbols().getDecimalSeparator();
    // char groupingSeparator =
    // displayDecimalFormat.getDecimalFormatSymbols().getGroupingSeparator();
    String pattern = createPattern();

    displayDecimalFormat = new DecimalFormat(pattern);
    // displayDecimalFormat.setGroupingUsed(true);
    // displayDecimalFormat.applyPattern(pattern);

    // char decimalSeparator = new
    // DecimalFormatSymbols(Locale.US).getDecimalSeparator();
    editionDecimalFormat = new DecimalFormat(pattern.replaceAll(",", ""));
    // editionDecimalFormat.setGroupingUsed(false);
    // editionDecimalFormat.applyPattern(pattern);
    // editionDecimalFormat.setParseIntegerOnly(false);

    // defaultFormatter = new NumberFormatter(new
    // DecimalFormat("#,##0.000000"));
    // editFormatter = new NumberFormatter(new DecimalFormat("#,##0.000000"));
    defaultFormatter = new NumberFormatter(displayDecimalFormat);
    editFormatter = new MEditNumberFormatter(editionDecimalFormat);

    defaultFormatter.setValueClass(getClazz());
    editFormatter.setValueClass(getClazz());

    editFormatter.setOverwriteMode(false);

    DefaultFormatterFactory defaultFormatterFactory = new DefaultFormatterFactory(
        defaultFormatter);
    defaultFormatterFactory.setEditFormatter(editFormatter);
    return defaultFormatterFactory;
  }

  private String createPattern() {
    StringBuffer szBuffer = new StringBuffer(precision + 1);

    if (precision == 0) {
      return "#,##0";
    }
    for (int i = precision; i > 0; i--) {
      szBuffer.append('0');
    }

    return "#,##0." + szBuffer;
  }

  abstract protected Class<?> getClazz();

  public void setMinValue(double minValue) {
    this.minValue = minValue;
    fireChange();
  }

  public void setMaxValue(double max) {
    this.maxValue = max;
    fireChange();
  }

  private void fireChange() {
  // defaultFormatter.setMinimum(minValue);
  // defaultFormatter.setMaximum(maxValue);
  // editFormatter.setMinimum(minValue);
  // editFormatter.setMaximum(maxValue);
  }

  public void setPopup(Popup popup) {
    this.popup = popup;
  }

  public Popup getPopup() {
    return popup;
  }

  private JLabel popupContents = new JLabel();

  private boolean signAllowed = true;

  public boolean isSignAllowed() {
    return signAllowed;
  }

  public void setSignAllowed(boolean signAllowed) {
    this.signAllowed = signAllowed;
  }

  private class MyInputVerifier extends InputVerifier {

    public MyInputVerifier() {}

    private boolean verify(String text) {
      double dText;
      try {
        Format format = editionDecimalFormat;
        if (format != null) {
          Number parseObject = (Number) format.parseObject(text);
          dText = Double.parseDouble(parseObject.toString());
        } else {
          dText = Double.parseDouble(text);
        }
      } catch (Exception e) {
        MLog.debug(this.getClass().getSimpleName(), "verify", "cannot parse '"
            + text + "'");
        return false;
      }
      if (dText < minValue || dText > maxValue) {
        return false;
      }

      return true;
    }

    private boolean verify(MNumberField input) {
      String text = input.getText();
      if (text == null || text.length() == 0) {
        return true;
      }

      return verify(text);
    }

    @Override
    public boolean verify(JComponent input) {

      boolean verified = false;
      if (MNumberField.class.isAssignableFrom(input.getClass())) {
        verified = verify((MNumberField) input);
      }
      if (!verified) {
        String formattedMinValue = editionDecimalFormat.format(minValue);
        String formattedMaxValue = editionDecimalFormat.format(maxValue);
        String text = "<html><body bgcolor=\"red\">Le champ doit contenir un nombre compris entre "
            + formattedMinValue + " et " + formattedMaxValue + "</body></html>";
        popupContents.setText(text);

        if (getPopup() != null) {
          popup.hide();
        }
        PopupFactory popupFactory = PopupFactory.getSharedInstance();
        Point locationOnScreen = input.getLocationOnScreen();
        popup = popupFactory.getPopup(input, popupContents,
            (int) locationOnScreen.getX() + 1, (int) locationOnScreen.getY()
                + input.getHeight() + 1);
        setPopup(popup);
        popup.show();
      } else {
        if (getPopup() != null) {
          popup.hide();
          setPopup(null);
        }
      }
      return verified;
    }
  }

  /**
   * 
   */
  private class MyKeyListener extends KeyAdapter {

    @Override
    public void keyTyped(KeyEvent e) {
      char c = e.getKeyChar();

      if (Character.isDigit(c)
          || c == KeyEvent.VK_BACK_SPACE
          || (isSignAllowed() && minValue < 0 && c == KeyEvent.VK_MINUS)
          || (precision > 0 && (c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))) {
        return;
      }
      e.consume();
    }

    @Override
    public void keyPressed(KeyEvent e) {
      int keyCode = e.getKeyCode();
      switch (keyCode) {
        case KeyEvent.VK_UP:
          try {
            addAndApply(1);
          } catch (Exception ex) {
            // ex.printStackTrace();
          }
          break;
        case KeyEvent.VK_DOWN:
          try {
            addAndApply(-1);
          } catch (Exception ex) {
            // ex.printStackTrace();
          }
          break;

        default:
          break;
      }
    }

    private void addAndApply(int i) throws ParseException {
      Number n = editionDecimalFormat.parse(getText());
      double d = n.doubleValue() + i;
      setText(editionDecimalFormat.format(d));
    }
  }
}
