/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MTextField.java - CREATION : 2004/08/22
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

import morrigan.graphique.field.util.MFormattedField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MTextField extends MFormattedField {
  public MTextField(int hauteurCell, int largeurCell) {
    super(hauteurCell, largeurCell);
    setHorizontalAlignment(SwingConstants.LEFT);
  }

  public MTextField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    this(iHauteurCell, iLargeurCell);
    this.setMandatory(bMandatory);
  }

  @Override
  protected AbstractFormatterFactory createFormatterFactory() {
    DefaultFormatter defaultFormat = new DefaultFormatter();
    defaultFormat.setOverwriteMode(false);
    DefaultFormatterFactory defaultFormatterFactory = new DefaultFormatterFactory(
        defaultFormat);
    return defaultFormatterFactory;
  }
}