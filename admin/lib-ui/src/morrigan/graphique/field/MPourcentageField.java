/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MPourcentageField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import morrigan.graphique.field.util.MFormattedField;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MPourcentageField extends MFormattedField {

  public MPourcentageField(int hauteurCell, int largeurCell) {
    super(hauteurCell, largeurCell);
    setHorizontalAlignment(SwingConstants.RIGHT);
  }

  public MPourcentageField(int hauteurCell, int largeurCell, boolean mandatory) {
    this(hauteurCell, largeurCell);
    setMandatory(mandatory);
  }

  @Override
  protected AbstractFormatterFactory createFormatterFactory() {
    try {
      // Affichage (2 decimales)
      NumberFormat percentDisplayFormat = NumberFormat.getPercentInstance();
      percentDisplayFormat.setMinimumFractionDigits(2);
      percentDisplayFormat.setMaximumFractionDigits(2);

      // Edition
      NumberFormat percentEditFormat = NumberFormat.getNumberInstance();
      percentEditFormat.setGroupingUsed(false);
      percentEditFormat.setMinimumFractionDigits(2);
      percentEditFormat.setMaximumFractionDigits(2);

      NumberFormatter percentEditFormatter = new MyNumberFormatter(
          percentEditFormat);
      NumberFormatter nb = new NumberFormatter(percentDisplayFormat);

      DefaultFormatterFactory fmtFactory = new DefaultFormatterFactory(nb, nb,
          percentEditFormatter);
      return fmtFactory;
    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "MPourcentageField",
          "Formattage de pourcentage impossible", e);
    }

    return new DefaultFormatterFactory(new DefaultFormatter());
  }

  private final class MyNumberFormatter extends NumberFormatter {
    private MyNumberFormatter(NumberFormat format) {
      super(format);
    }

    @Override
    public String valueToString(Object oValue) throws ParseException {
      Number number = (Number) oValue;
      if (number != null) {
        double d = number.doubleValue() * 100.0;
        number = new Double(d);
      }
      return super.valueToString(number);
    }

    @Override
    public Object stringToValue(String sValueDisplayed) throws ParseException {
      Number number = (Number) super.stringToValue(sValueDisplayed);
      if (number != null) {
        double d = number.doubleValue() / 100.0;
        number = new Double(d);
      }
      return number;
    }
  }

}