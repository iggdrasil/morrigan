/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MDecimalField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.graphique.field;

import morrigan.graphique.field.util.MNumberField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MDecimalField extends MNumberField {
  
  private static final int DEFAULT_PRECISION = 1;

  public MDecimalField(int iHauteurCell, int iLargeurCell, int precision) {
    super(iHauteurCell, iLargeurCell, precision);
  }

  public MDecimalField(int iHauteurCell, int iLargeurCell) {
    this(iHauteurCell, iLargeurCell, DEFAULT_PRECISION);
  }

  public MDecimalField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    super(iHauteurCell, iLargeurCell, bMandatory);
  }

  @Override
  protected Class<?> getClazz() {
    return Double.class;
  }
}