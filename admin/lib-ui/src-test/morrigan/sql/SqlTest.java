package morrigan.sql;

import java.util.LinkedList;
import java.util.List;

import morrigan.sql.element.Field;
import morrigan.sql.element.Table;
import morrigan.sql.request.Operator;
import morrigan.sql.request.TableSelection;
import morrigan.sql.request.builder.DefaultRequestBuilder;
import morrigan.sql.request.builder.ExplicitJoinRequestBuilder;
import morrigan.sql.request.builder.PartedRequestBuilder;
import morrigan.sql.request.builder.Request;
import morrigan.sql.request.builder.RequestBuilder;
import morrigan.sql.request.clause.Clause;
import morrigan.sql.request.clause.NumericClause;
import morrigan.sql.request.clause.StringClause;
import morrigan.sql.request.join.InnerJoin;
import morrigan.sql.request.join.Join;
import morrigan.sql.request.join.OuterJoin;


public class SqlTest {
	
	public static void main(String[] args) {
    List<Request> requests = new LinkedList<Request>();
    requests.add(createRequest1());
    requests.add(createRequest2());
    requests.add(createRequest3());
    requests.add(createRequest4());
    requests.add(createRequest5());
		
		RequestBuilder defaultBuilder = new DefaultRequestBuilder();
		RequestBuilder partedBuilder = new PartedRequestBuilder();
		RequestBuilder withJoinBuilder = new ExplicitJoinRequestBuilder();
		
    for(Request req : requests) {
      buildWithBuilder(defaultBuilder, req);
      buildWithBuilder(partedBuilder, req);
      buildWithBuilder(withJoinBuilder, req);
    }
	}

	private static void buildWithBuilder(RequestBuilder requestBuilder,
			Request request1) {
		String sql = requestBuilder.build(request1);
		System.out.println(sql + ';');
	}
	
	/**
	 * Request for :
	 * <pre>SELECT prs_nom, prs_id FROM personnage</pre>
	 * @return the request
	 */
	private static Request createRequest1() {
		System.out.println("Requete sur une table");
		Request request = new Request();
		
		// Tables
		Table tablePerso = new Table("personnage");
		
		// Selection
		TableSelection tableSelection = new TableSelection(tablePerso);
		tableSelection.addSelection(new Field("prs_nom_originel", tablePerso));
		Field field_prs_id = new Field("prs_id", tablePerso);
		tableSelection.addSelection(field_prs_id);
		request.addSelection(tableSelection);
		
		// No jointure
		// No clause

		return request;
	}
	
	/**
	 * Request for :
	 * <pre>
	 * SELECT prs_nom, 
	 *        prs_id 
	 * FROM personnage
	 * WHERE prs_id > 1
	 *   AND prs_nom LIKE '%Sil%'
	 * </pre>
	 * @return the request
	 */
	private static Request createRequest2() {
		System.out.println("Requete sur une table + 2 clauses");
		Request request = new Request();
		
		// Tables
		Table tablePerso = new Table("personnage");
		
		// Selection
		TableSelection tableSelection = new TableSelection(tablePerso);
		Field field_prs_nom = new Field("prs_nom_originel", tablePerso);
		tableSelection.addSelection(field_prs_nom);
		Field field_prs_id = new Field("prs_id", tablePerso);
		tableSelection.addSelection(field_prs_id);
		request.addSelection(tableSelection);
		
		// No jointure
		
		// Clauses
		Clause clause = new NumericClause(field_prs_id, Operator.SUPERIOR, 1);
		request.addClause(clause);
		clause = new StringClause(field_prs_nom, Operator.LIKE, "%Sil%");
		request.addClause(clause);

		return request;
	}
  
  /**
   * Request for :
   * <pre>
   * SELECT a.prs_nom prs_nom1, 
   *             a.prs_id prs_id2, 
   *             c.prs_nom prs_nom4
   *   FROM personnage AS a, 
   *        personnage AS b 
   *   WHERE a.prs_possesseur_prs_id = c.prs_id
   * </pre>
   * @return
   */
  private static Request createRequest3() {
    System.out.println("Requete entre 2 tables identiques");
    
    Request request = new Request();
    // Tables
    Table tablePerso = new Table("personnage");
    Table tablePersoPossesseur = new Table("personnage");
    
    // Selections
    TableSelection tableSelection = new TableSelection(tablePerso);
    tableSelection.addSelection(new Field("prs_nom_originel", tablePerso));
    tableSelection.addSelection(new Field("prs_id", tablePerso));
    request.addSelection(tableSelection);
    
    tableSelection = new TableSelection(tablePersoPossesseur);
    tableSelection.addSelection(new Field("prs_nom_originel", tablePersoPossesseur));
    request.addSelection(tableSelection);
    
    // Jointures
    Field prs_possesseur_prs_id = new Field("prs_possesseur_prs_id", tablePerso);
    Field prs_id = new Field("prs_id", tablePersoPossesseur);
    
    Join join = new InnerJoin(prs_possesseur_prs_id, prs_id);
    request.addJointure(join);
    return request;
  }
	
	/**
	 * Request for INNER JOIN:
	 * <pre>
	 * SELECT a.prs_nom prs_nom1, 
	 *             a.prs_id prs_id2, 
	 *             b.jou_nom jou_nom, 
	 *             c.prs_nom prs_nom4
	 *   FROM personnage AS a, 
	 *        joueur AS b, 
	 *        personnage AS c 
	 *   WHERE b.jou_id = personnage0.prs_jou_id 
	 *     AND a.prs_possesseur_prs_id = c.prs_id
	 * </pre>
	 * @return
	 */
	private static Request createRequest4() {
		System.out.println("Requete entre 2 tables identiques + 1 troisieme");
		
		Request request = new Request();
		// Tables
		Table tablePerso = new Table("personnage");
		Table tablePerso2 = new Table("personnage");
		Table tableJoueur = new Table("joueur");
		
		// Selections
		TableSelection tableSelection = new TableSelection(tablePerso);
		tableSelection.addSelection(new Field("prs_nom_originel", tablePerso));
		tableSelection.addSelection(new Field("prs_id", tablePerso));
		request.addSelection(tableSelection);
		
		tableSelection = new TableSelection(tableJoueur);
		tableSelection.addSelection(new Field("jou_nom", tableJoueur));
		request.addSelection(tableSelection);
		
		tableSelection = new TableSelection(tablePerso2);
		tableSelection.addSelection(new Field("prs_nom_originel", tablePerso2));
		request.addSelection(tableSelection);
		
		// Jointures
		Field jou_id = new Field("jou_id", tableJoueur);
		Field prs_jou_id = new Field("prs_jou_id", tablePerso);
		
		Join join = new InnerJoin(jou_id, prs_jou_id);
		request.addJointure(join);
		
		Field prs_possesseur_prs_id = new Field("prs_possesseur_prs_id", tablePerso);
		Field prs_id = new Field("prs_id", tablePerso2);
		
		join = new InnerJoin(prs_possesseur_prs_id, prs_id);
		request.addJointure(join);
		return request;
	}
  
  /**
   * Request for OUTER JOIN:
   * <pre>
   * SELECT a.prs_nom prs_nom1, 
   *             a.prs_id prs_id2, 
   *             b.jou_nom jou_nom, 
   *             c.prs_nom prs_nom4
   *   FROM personnage AS a, 
   *        joueur AS b, 
   *        personnage AS c 
   *   WHERE b.jou_id = personnage0.prs_jou_id 
   *     AND a.prs_possesseur_prs_id (+) = c.prs_id
   * </pre>
   * @return
   */
  private static Request createRequest5() {
    System.out.println("Requete entre 2 tables identiques + 1 troisieme, avec INNER JOIN");
    
    Request request = new Request();
    // Tables
    Table tablePerso = new Table("personnage");
    Table tablePerso2 = new Table("personnage");
    Table tableJoueur = new Table("joueur");
    
    // Selections
    TableSelection tableSelection = new TableSelection(tablePerso);
    tableSelection.addSelection(new Field("prs_nom_originel", tablePerso));
    tableSelection.addSelection(new Field("prs_id", tablePerso));
    request.addSelection(tableSelection);
    
    tableSelection = new TableSelection(tableJoueur);
    tableSelection.addSelection(new Field("jou_nom", tableJoueur));
    request.addSelection(tableSelection);
    
    tableSelection = new TableSelection(tablePerso2);
    tableSelection.addSelection(new Field("prs_nom_originel", tablePerso2));
    request.addSelection(tableSelection);
    
    // Jointures
    Field jou_id = new Field("jou_id", tableJoueur);
    Field prs_jou_id = new Field("prs_jou_id", tablePerso);
    
    Join join = new OuterJoin(prs_jou_id, jou_id);
    request.addJointure(join);
    
    Field prs_possesseur_prs_id = new Field("prs_possesseur_prs_id", tablePerso);
    Field prs_id = new Field("prs_id", tablePerso2);
    
    join = new OuterJoin(prs_possesseur_prs_id, prs_id);
    request.addJointure(join);
    return request;
  }
}
