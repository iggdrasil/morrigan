package morrigan.field;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import morrigan.graphique.field.MDecimalField;
import morrigan.graphique.field.MIntegerField;
import morrigan.graphique.field.MTextField;

public class FieldMain extends JFrame {

  private static final long serialVersionUID = 2284886295541634609L;

  public FieldMain() {
    JPanel j = new JPanel();
    BoxLayout boxLayout = new BoxLayout(j, BoxLayout.Y_AXIS);
    j.setLayout(boxLayout);
    setContentPane(j);

    MIntegerField ni = new MIntegerField(1, 1);
    j.add(ni);
    MDecimalField nd = new MDecimalField(1, 1, 1);
    j.add(nd);
    MTextField t = new MTextField(1, 1);
    j.add(t);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  public static void main(String[] args) {
    FieldMain fieldMain = new FieldMain();
    fieldMain.pack();
    fieldMain.setVisible(true);
  }

}
