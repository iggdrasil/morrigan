package morrigan.test;

import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MADateField;
import morrigan.admin.ui.field.MATextField;
import morrigan.graphique.MJLabel;
import morrigan.graphique.field.MDateField;
import morrigan.graphique.field.MDecimalField;
import morrigan.graphique.field.MIntegerField;
import morrigan.graphique.field.MTextAreaField;

public class MAFenTest extends MAFenetre {
  public MAFenTest() {
    super("Test", 14, 18);
    init();
    setPanelBoutonsEnabled(false);
  }

  private void init() {
    // Declarations
    int iLargeurLabel = 6;
    int iLargeurChamps = 10;

    // Declarations
    MJLabel mlTexteC = new MJLabel("Texte court", iLargeurLabel, 1);
    MJLabel mlEntier = new MJLabel("Entier", iLargeurLabel, 1);
    MJLabel mlDeci = new MJLabel("Decimal", iLargeurLabel, 1);
    // MJLabel mlPourc = new MJLabel("Pourcentage", iLargeurLabel, 1);
    MJLabel mlDate = new MJLabel("Date", iLargeurLabel, 1);
    MJLabel mlDateHeure = new MJLabel("Date+Heure", iLargeurLabel, 1);
    MJLabel mlHeure = new MJLabel("Heure", iLargeurLabel, 1);
    MJLabel mlTexteL = new MJLabel("Texte long", iLargeurLabel, 1);
    MJLabel mlTexteL2 = new MJLabel("Texte long 2", iLargeurLabel, 1);

    MATextField mjt = new MATextField(1, iLargeurChamps);
    mjt.setValue("Siltaom");
    MIntegerField mif = new MIntegerField(1, iLargeurChamps);
    mif.setValue(new Integer(123456));
    MDecimalField mdfDec = new MDecimalField(1, iLargeurChamps);
    mdfDec.setValue(new Double(12345.6));
    // MPourcentageField mpf = new MPourcentageField(iLargeurChamps, 1);
    // mpf.setValue(new Double(0.245));
    MADateField mdfDate = new MADateField(1, iLargeurChamps,
        MDateField.DATEFORMAT.DATE);
    // mdfDate.setValue(new Long("1093352515046"));
    MADateField mdfDateHeure = new MADateField(1, iLargeurChamps,
        MDateField.DATEFORMAT.DATEHOUR);
//    // mdfDateHeure.setValue(new Long("1093352515046"));
    MADateField mdfHeure = new MADateField(1, iLargeurChamps,
        MDateField.DATEFORMAT.HOUR);
    // mdfHeure.setValue(new Long("1093352515046"));
    MTextAreaField mtaf = new MTextAreaField(false, "", 3, iLargeurChamps);
    mtaf.setValue("Il etait une fois dans une sombre batisse, ...");
    MTextAreaField mtaf2 = new MTextAreaField(1, iLargeurChamps);
    mtaf2
        .setValue("A la lumiere de sa lampe a petrole, Luigi prenait de l'avance");

    mlTexteC.setLabelFor(mjt);
    mlEntier.setLabelFor(mif);
    mlDeci.setLabelFor(mdfDec);
    // mlPourc.setLabelFor(mpf);
    mlDate.setLabelFor(mdfDate);
    mlDateHeure.setLabelFor(mdfDateHeure);
    mlHeure.setLabelFor(mdfHeure);
    mlTexteL.setLabelFor(mtaf);
    mlTexteL2.setLabelFor(mtaf2);

    // Listeners

    // Placement
    int xLabel = 1;
    int yLabel = 2;
    int xChamps = xLabel + iLargeurLabel + 1;

    mpqPrinc.add(mlTexteC, xLabel, yLabel);
    mpqPrinc.add(mjt, xChamps, yLabel++);
    mpqPrinc.add(mlEntier, xLabel, yLabel);
    mpqPrinc.add(mif, xChamps, yLabel++);
    mpqPrinc.add(mlDeci, xLabel, yLabel);
    mpqPrinc.add(mdfDec, xChamps, yLabel++);
    // mpqPrinc.add(mlPourc, xLabel, yLabel);
    // mpqPrinc.add(mpf, xChamps, yLabel++);
    mpqPrinc.add(mlDate, xLabel, yLabel);
    mpqPrinc.add(mdfDate, xChamps, yLabel++);
    mpqPrinc.add(mlDateHeure, xLabel, yLabel);
    mpqPrinc.add(mdfDateHeure, xChamps, yLabel++);
    mpqPrinc.add(mlHeure, xLabel, yLabel);
    mpqPrinc.add(mdfHeure, xChamps, yLabel++);
    mpqPrinc.add(mlTexteL, xLabel, yLabel);
    mpqPrinc.add(mtaf, xChamps, yLabel);
    yLabel += 3;
    mpqPrinc.add(mlTexteL2, xLabel, yLabel);
    mpqPrinc.add(mtaf2, xChamps, yLabel);
  }

}
