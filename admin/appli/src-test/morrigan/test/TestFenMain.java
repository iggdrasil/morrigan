package morrigan.test;

import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import morrigan.admin.MAWindowManager;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.exception.OpenWindowException;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.launching.LauncherFactory;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.application.MinLauncherInitializer;
import morrigan.common.launching.launcher.Launcher;

public class TestFenMain {

  public static void main(String[] args) {
    System.setProperty("launching.console", "false");
    Initializer minLauncherInitializer = MinLauncherInitializer
        .getMinLauncherInitializer();
    Launcher launcher = LauncherFactory.createLauncher();
    launcher.launchInitializer(minLauncherInitializer);
    MASysWindow sysWindow = new MASysWindow("test", MAFenTest.class);
    try {
      MAWindowManager.createWindow(sysWindow);
      Window window = MAWindowManager.openWindow(sysWindow);
      ((JFrame) window).setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    } catch (CreateWindowException e) {
      e.printStackTrace();
    } catch (OpenWindowException e) {
      e.printStackTrace();
    }
  }
}
