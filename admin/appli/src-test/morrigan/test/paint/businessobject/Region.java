package morrigan.test.paint.businessobject;

import java.awt.Color;

import morrigan.test.table.element.Polygon;

public class Region {

  private String name = null;

  private Polygon polygon = null;

  private RegionType regionType = null;

  public Region(String name, RegionType regionType) {
    super();
    this.name = name;
    this.regionType = regionType;
  }

  public Region(String name, RegionType regionType, Polygon polygon) {
    this(name, regionType);
    this.polygon = polygon;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RegionType getRegionType() {
    return regionType;
  }

  public void setRegionType(RegionType regionType) {
    this.regionType = regionType;
  }

  public Polygon getPolygon() {
    return polygon;
  }

  public void setPolygon(Polygon polygon) {
    this.polygon = polygon;
  }

  @Override
  public String toString() {
    return "Region: " + name + ", " + polygon;
  }

  /**
   * cf table sql : type_region
   * 
   * @author armel
   */
  static public class RegionType {

    static public final RegionType desert = new RegionType("desert", Color
        .decode("0xfff945"));

    static public final RegionType foret = new RegionType("foret", Color
        .decode("0x007c00"));

    static public final RegionType riviere = new RegionType("riviere", Color
        .decode("0x3f5af0"));

    static public final RegionType plaine = new RegionType("plaine", Color
        .decode("0x35c02e"));

    static public final RegionType marais = new RegionType("marais", Color
        .decode("0x6b8666"));

    static public final RegionType montagne = new RegionType("montagne", Color
        .decode("0x9f7252"));

    static public final RegionType vide = new RegionType("vide", Color
        .decode("0x000000"));

    private final String name;

    private final Color color;

    public RegionType(String name, Color color) {
      this.name = name;
      this.color = color;
    }

    public String getName() {
      return name;
    }

    public Color getColor() {
      return color;
    }
  }
}
