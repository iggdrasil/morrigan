package morrigan.test.paint;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import morrigan.test.paint.businessobject.Region;
import morrigan.test.paint.ui.AwtPolygon;
import morrigan.test.table.element.SqlPoint;

class MonPanel extends JPanel {

  private static final long serialVersionUID = 6063513914576430607L;

  static final int DEFAULT_SCALE = 7;

  private List<Cell> selectedCells = new LinkedList<Cell>();

  /** n pixels pour 1 cote de case sur la carte */
  private int scale = DEFAULT_SCALE;

  /** Margin on top, bottom and sides */
  private int margin = 3;

  private Cell currentCell = null;

  private Dimension dimension = null;

  private int displaiedThings = 0;

  private List<Region> regions = MockService.getRegions();

  private MoveInfo moveInfo = new MoveInfo();

  public MonPanel() {
    super();

    setVisible(ELEMENT_CARTE.QUADRILLAGE, true);
    setVisible(ELEMENT_CARTE.REGION, true);
    setVisible(ELEMENT_CARTE.SURVOL, true);
    setVisible(ELEMENT_CARTE.LINE, false);
    setVisible(ELEMENT_CARTE.POLYGON_SIDE, true);

    this.addMouseListener(new PanelMouseListener());

    this.addMouseMotionListener(new PanelMouseMotion());

  }

  /**
   * Set the current cell with the cursor point information
   * 
   * @param cursorPoint
   */
  protected void checkAndSetCurrentCell(Point cursorPoint) {
    if (checkValidCell(cursorPoint)) {
      setCurrentCell(cursorPoint);
    } else {
      currentCell = null;
    }
  }

  private void setCurrentCell(Point cursorPoint) {
    currentCell = Util.convertClickedPointToCell(cursorPoint);
  }

  /**
   * Check if the cursor is a valid cell
   * 
   * @param cursorPoint the cursor point
   * @return <code>true</code> if the point can be converted into cell;<code>false</code>
   *         otherwise
   */
  private boolean checkValidCell(Point cursorPoint) {
    if (cursorPoint.getX() <= margin
        || cursorPoint.getX() >= getDimension().getWidth() * scale + margin
        || cursorPoint.getY() <= margin
        || cursorPoint.getY() >= getDimension().getHeight() * scale + margin) {
      return false;
    }
    return true;
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    if (isVisible(ELEMENT_CARTE.REGION)) {
      drawRegions(g2);
    }
    if (isVisible(ELEMENT_CARTE.SURVOL)) {
      drawFliedOverRegion(g2);
    }
    if (isVisible(ELEMENT_CARTE.QUADRILLAGE)) {
      drawQuad(g2);
    }
    if (isVisible(ELEMENT_CARTE.LINE)) {
      drawCurrentLine(g2);
    }
    if (isVisible(ELEMENT_CARTE.POLYGON_SIDE)) {
      drawCurrentSide(g2);
    }

    drawChangedPolygon(g2);
    drawSelectedPoints(g2);
    drawCurrentCell(g2);
  }

  /**
   * @param g2
   */
  private void drawChangedPolygon(Graphics2D g2) {
    if (moveInfo.getOriginDraggedCell() != null && currentCell != null) {
      if (moveInfo.moveInAction()) {
        // FAKE
        Point toPoint = Util.convertCellToClickedPoint(currentCell);
        Line2D lineToDraw = Util.convertLineToLineToDraw(new Line2D.Double(
            moveInfo.getOriginDraggedCell(), currentCell));

        Polygon polygonToDraw = new Polygon();
        polygonToDraw.addPoint((int) lineToDraw.getX1(), (int) lineToDraw
            .getY1());
        polygonToDraw.addPoint((int) lineToDraw.getX2(), (int) lineToDraw
            .getY2());
        polygonToDraw.addPoint((int) toPoint.getX(), (int) toPoint.getY());
        Rectangle rectangleToDraw = polygonToDraw.getBounds();
        g2.setColor(Color.gray);
        g2.draw(rectangleToDraw);
      }
    }
  }

  /**
   * @param g2
   */
  private void drawCurrentSide(Graphics2D g2) {
    if (currentCell != null) {
      Stroke stroke = g2.getStroke();

      if (moveInfo.getOriginDraggedCell() != null) {
        Line2D line = Util.convertLineToLineToDraw(new Line2D.Double(moveInfo
            .getCornerPathBeforeCorner(), moveInfo.getOriginDraggedCell()));

        g2.setStroke(new BasicStroke(5));
        g2.setColor(moveInfo.getDraggedRegion().getRegionType().getColor()
            .darker());
        g2.draw(Util.convertLineToLineToDraw(line));
        g2.setStroke(stroke);
      }
    }
  }

  /**
   * @param g2
   */
  private void drawFliedOverRegion(Graphics2D g2) {

    if (currentCell != null) {
      Point currentPoint = Util.convertCellToClickedPoint(currentCell);
      Stroke stroke = g2.getStroke();

      // Paint region flied over
      for (Region region : regions) {
        int[][] points = Util.convertXYCoordonatesToXYToDraw(region
            .getPolygon().retrieveXYPoints());
        Polygon awtPolygon = new AwtPolygon(points[0], points[1],
            points[0].length);
        if (awtPolygon.contains(currentPoint)) {
          g2.setStroke(new BasicStroke(scale));
          g2.setColor(region.getRegionType().getColor().brighter());
          g2.setStroke(stroke);
          g2.fillPolygon(awtPolygon);
        }
      }
      g2.setStroke(stroke);
    }
  }

  private void drawRegions(Graphics2D g2) {
    Stroke initialStroke = g2.getStroke();
    Stroke stroke = new BasicStroke(scale);
    g2.setStroke(stroke);
    for (Region region : regions) {
      int[][] points = Util.convertXYCoordonatesToXYToDraw(region.getPolygon()
          .retrieveXYPoints());
      g2.setColor(region.getRegionType().getColor());
      g2.setStroke(stroke);
      g2.fillPolygon(points[0], points[1], points[0].length);
      g2.drawPolygon(points[0], points[1], points[0].length);
    }
    g2.setStroke(initialStroke);
  }

  /**
   * Draw a line between last selected cell and current cell.
   * 
   * @param g2
   */
  private void drawCurrentLine(Graphics2D g2) {
    if (!selectedCells.isEmpty() && currentCell != null) {
      Point currentPoint = Util.convertCellToClickedPoint(currentCell);
      Point lastPoint = Util.convertCellToClickedPoint(getLastCell());
      g2.drawLine((int) currentPoint.getX(), (int) currentPoint.getY(),
          (int) lastPoint.getX(), (int) lastPoint.getY());
    }
  }

  /**
   * Draw the current cell.
   * 
   * @param g2
   */
  private void drawCurrentCell(Graphics2D g2) {
    // The cursor is over a cell
    if (currentCell != null) {
      Rectangle rectangle = Util.convertCellToRectangleToDraw(currentCell);
      g2.setColor(Color.MAGENTA);
      g2.drawRect((int) rectangle.getX(), (int) rectangle.getY(),
          (int) rectangle.getWidth(), (int) rectangle.getHeight());

    }
  }

  private void drawSelectedPoints(Graphics2D g2) {
    g2.setColor(Color.RED);
    for (Object element : selectedCells) {
      Cell point = (Cell) element;
      Rectangle rectangle = Util.convertCellToRectangleToDraw(point);
      g2.drawRect((int) rectangle.getX(), (int) rectangle.getY(),
          (int) rectangle.getWidth(), (int) rectangle.getHeight());
    }
  }

  private void drawQuad(Graphics2D g2) {
    g2.setColor(Color.LIGHT_GRAY);
    for (int iCol = 0; iCol <= dimension.getWidth(); iCol++) {
      g2.draw(new Line2D.Double(margin + iCol * scale, margin, margin + iCol
          * scale, margin + dimension.getHeight() * scale));
    }

    for (int iLigne = 0; iLigne <= dimension.getHeight(); iLigne++) {
      g2.draw(new Line2D.Double(margin, margin + iLigne * scale, margin
          + dimension.getWidth() * scale, margin + iLigne * scale));
    }
  }

  void setVisible(ELEMENT_CARTE element, boolean value) {
    if (value) {
      displaiedThings |= (1 << element.getLevel());
    } else {
      displaiedThings &= ~(1 << element.getLevel());
    }
  }

  boolean isVisible(ELEMENT_CARTE element) {
    return (0x1 & (displaiedThings >> element.getLevel())) == 1;
  }

  public Dimension getDimension() {
    return dimension;
  }

  public void setDimension(Dimension dimension) {
    this.dimension = dimension;
  }

  public void setScale(int scale) {
    this.scale = scale;
    Util.scale = scale;
  }

  public void setMargin(int margin) {
    this.margin = margin;
    Util.margin = margin;
  }

  public List<Cell> getSelectedCells() {
    return selectedCells;
  }

  public Cell getLastCell() {
    // Optim
    if (selectedCells instanceof LinkedList) {
      return (Cell) ((LinkedList) selectedCells).getLast();
    }
    return selectedCells.get(selectedCells.size() - 1);
  }

  private final class PanelMouseListener extends MouseAdapter {
    @Override
    public void mouseClicked(MouseEvent e) {
      super.mouseClicked(e);
      // Invalidate rectangle around the click.
      Rectangle rectToRepaint = Util
          .convertClickedPointToInvalidatedRectangle(e.getPoint());
      repaint(rectToRepaint);

      if (!getSelectedCells().isEmpty() && currentCell != null) {
        // And possibly the line between this click and the previous one
        repaint(Util.convertLineToInvalidatedRectangle(Util
            .convertCellToClickedPoint(getLastCell()), Util
            .convertCellToClickedPoint(currentCell)));
      }

      getSelectedCells().add(Util.convertClickedPointToCell(e.getPoint()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {
      super.mouseReleased(e);

      // Check that the new region side will not override another region
      if (checkValidCell(e.getPoint())) {
        // If the future point is correct, apply the new current cell
        if (moveInfo.getOriginDraggedCell() == null
            || checkFutureOverlap(Util.convertClickedPointToCell(e.getPoint()))) {
          setCurrentCell(e.getPoint());
        }
      }

      // Drag in application
      if (moveInfo.getOriginDraggedCell() != null && currentCell != null) {
        // Translate thes sides
        Point toPoint = Util.convertCellToClickedPoint(currentCell);
        Line2D lineToDraw = Util.convertLineToLineToDraw(new Line2D.Double(
            moveInfo.getOriginDraggedCell(), currentCell));

        // int delta = (int) moveInfo.getOriginDraggedSide().ptLineDist(
        // currentCell);
        // int sign;
        // double y = currentCell.getY();
        // double x = currentCell.getX();
        //
        // CURSOR_MOVE_DIRECTION moveDirection = Util.retrieveMoveDirection(
        // moveInfo.getOriginDraggedSide(), currentCell);
        //
        // SqlPoint pointA = new SqlPoint((int) moveInfo.getOriginDraggedSide()
        // .getX1(), (int) moveInfo.getOriginDraggedSide().getY1());
        // SqlPoint pointB = new SqlPoint((int) moveInfo.getOriginDraggedSide()
        // .getX2(), (int) moveInfo.getOriginDraggedSide().getY2());
        //
        // if (moveDirection == CURSOR_MOVE_DIRECTION.HORIZONTAL)
        // {
        // sign = moveInfo.getOriginDraggedSide().intersectsLine(x, y, -x, y) ?
        // 1
        // : -1;
        // moveInfo.getDraggedRegion().getPolygon().translateSide(pointA,
        // pointB, sign * delta, 0);
        // }
        // if (moveDirection == CURSOR_MOVE_DIRECTION.VERTICAL)
        // {
        // sign = moveInfo.getOriginDraggedSide().intersectsLine(x, y, x, -y) ?
        // 1
        // : -1;
        // moveInfo.getDraggedRegion().getPolygon().translateSide(pointA,
        // pointB, 0, sign * delta);
        // }
        repaint();
      }
      moveInfo.setOriginDraggedCell(null);
    }
  }

  private class MoveInfo {

    private Cell originDraggedCell = null;

    private Region draggedRegion = null;

    private Cell cornerPathBeforeCorner = null;

    private Cell cornerPathAfterCorner = null;

    public MoveInfo() {}

    public boolean moveInAction() {
      return currentCell != originDraggedCell;
    }

    public Cell getOriginDraggedCell() {
      return originDraggedCell;
    }

    public void setOriginDraggedCell(Cell fromDragCell) {
      this.originDraggedCell = fromDragCell;
      List<SqlPoint> path = draggedRegion.getPolygon().getPath();

      int cellIndex = path.indexOf(fromDragCell);
      System.out.println(cellIndex);
      cornerPathBeforeCorner = new Cell(path.get((path.size() + cellIndex - 1)
          % path.size()));
      cornerPathAfterCorner = new Cell(path.get((path.size() + cellIndex + 1)
          % path.size()));
    }

    public Region getDraggedRegion() {
      return draggedRegion;
    }

    public void setDraggedRegion(Region draggedRegion) {
      this.draggedRegion = draggedRegion;
    }

    public Cell getCornerPathAfterCorner() {
      return cornerPathAfterCorner;
    }

    public Cell getCornerPathBeforeCorner() {
      return cornerPathBeforeCorner;
    }

  }

  private final class PanelMouseMotion extends MouseMotionAdapter {
    private Cell cell;

    @Override
    public void mouseDragged(MouseEvent e) {
      super.mouseDragged(e);

      // Check that the new region side will not override another region
      if (checkValidCell(e.getPoint())) {
        cell = Util.convertClickedPointToCell(e.getPoint());
        // If the future point is correct, apply the new current cell
        if (moveInfo.getOriginDraggedCell() != null || checkFutureOverlap(cell)) {
          ;
        }
        setCurrentCell(e.getPoint());
      }

      // If drag move begins
      if (moveInfo.getOriginDraggedCell() == null && currentCell != null) {
        Region draggedRegion = Util.retrieveRegionContainingCell(regions, cell);
        System.out.println(draggedRegion);
        if (draggedRegion != null) {
          // List<SqlPoint> path = draggedRegion.getPolygon().getPath();
          // for (Iterator<SqlPoint> iter = path.iterator(); iter.hasNext();)
          // {
          // SqlPoint sqlPoint = iter.next();
          // if (cell.getX() == sqlPoint.getX() && cell.getY() ==
          // sqlPoint.getY() )
          // ;
          //            
          // }
          // {}
          System.out.println();
          moveInfo = new MoveInfo();
          moveInfo.setDraggedRegion(draggedRegion);
          moveInfo.setOriginDraggedCell(currentCell);
        }
      }
      repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
      Point cursorPoint = e.getPoint();

      // Refresh older areas
      refreshAroundPreviousCell(cursorPoint);

      // Check location
      checkAndSetCurrentCell(cursorPoint);

      if (currentCell == null) {
        return;
      }

      // Refresh current areas
      refreshArroundCurrentCell();
    }

    /**
     * Refresh around the previous cell.
     */
    private void refreshAroundPreviousCell(Point cursorPoint) {
      if (currentCell != null) {
        // Ex current cell
        repaint(Util.convertCellToInvalidatedRectangle(currentCell));

        // Refresh older flied over region
        if (isVisible(ELEMENT_CARTE.SURVOL)) {
          Polygon awtPolygon = Util.retrieveRectangleContainingCell(regions,
              currentCell);
          if (awtPolygon != null) {
            Rectangle bounds = awtPolygon.getBounds();
            repaint(new Rectangle((int) bounds.getX() - scale, (int) bounds
                .getY()
                - scale, (int) bounds.getWidth() + scale * 2, (int) bounds
                .getHeight()
                + scale * 2));
          }
        }
      }

      // Refresh last line (and set lastPoint)
      if (isVisible(ELEMENT_CARTE.LINE)) {
        if (!getSelectedCells().isEmpty()) {
          Point lastPoint = Util.convertCellToClickedPoint(getLastCell());
          repaint(Util
              .convertLineToInvalidatedRectangle(lastPoint, cursorPoint));
        }
      }
    }

    /**
     * Refresh around the current cell.
     */
    private void refreshArroundCurrentCell() {
      Point currentPoint = Util.convertCellToClickedPoint(currentCell);

      // Paint rectangle around the current cell
      repaint(Util.convertCellToInvalidatedRectangle(currentCell));

      // Paint "rectangle" with diagonale "last cell" vs "current point"
      if (isVisible(ELEMENT_CARTE.LINE)) {
        if (!getSelectedCells().isEmpty()) {
          Point lastPoint = Util.convertCellToClickedPoint(getLastCell());
          repaint(Util.convertLineToInvalidatedRectangle(lastPoint,
              currentPoint));
        }
      }

      // Paint region flied over
      if (isVisible(ELEMENT_CARTE.SURVOL)) {
        Polygon awtPolygon = Util.retrieveRectangleContainingCell(regions,
            currentCell);
        if (awtPolygon != null) {
          repaint(awtPolygon.getBounds());
        }
      }
    }
  }

  public enum CURSOR_MOVE_DIRECTION {
    VERTICAL,
    HORIZONTAL,
    VERTICAL_AND_HORIZONTAL;
  }

  public enum ELEMENT_CARTE {
    /** Quadrillage du monde */
    QUADRILLAGE,
    /** Regions */
    REGION,
    /** Routes */
    ROUTE,
    /** Survol de region */
    SURVOL,
    /** POLYGON_SIDE */
    POLYGON_SIDE,
    /** TEST LINE */
    LINE;

    private int level;

    static {
      int i = 0;
      QUADRILLAGE.setLevel(1 << i++);
      REGION.setLevel(1 << i++);
      ROUTE.setLevel(1 << i++);
      SURVOL.setLevel(1 << i++);
      LINE.setLevel(1 << i++);
    }

    private void setLevel(int level) {
      this.level = level;
    }

    public int getLevel() {
      return level;
    }
  }

  static private class Util {

    static private int scale = 7; // n pixels pour 1 cote de case sur la carte

    static private int margin = 3;

    public static Rectangle convertCellToRectangleToDraw(Cell cell) {
      return new Rectangle((int) cell.getX() * scale + margin + 1, (int) cell
          .getY()
          * scale + margin + 1, scale - 2, scale - 2);
    }

    public static CURSOR_MOVE_DIRECTION retrieveMoveDirection(
        Line2D fromDragLine2D, Cell currentCell) {
      double minX = Math.min(fromDragLine2D.getX1(), fromDragLine2D.getX2());
      double maxX = Math.max(fromDragLine2D.getX1(), fromDragLine2D.getX2());
      double minY = Math.min(fromDragLine2D.getY1(), fromDragLine2D.getY2());
      double maxY = Math.max(fromDragLine2D.getY1(), fromDragLine2D.getY2());

      boolean horizontalMove = (currentCell.getY() >= minY && currentCell
          .getY() <= maxY);
      boolean verticalMove = (currentCell.getX() > minX && currentCell.getX() < maxX);

      if (horizontalMove && verticalMove) {
        return CURSOR_MOVE_DIRECTION.VERTICAL_AND_HORIZONTAL;
      }

      return horizontalMove ? CURSOR_MOVE_DIRECTION.HORIZONTAL
          : (verticalMove ? CURSOR_MOVE_DIRECTION.VERTICAL : null);

    }

    public static Line2D convertLineToLineToDraw(Line2D line) {
      double x1 = line.getX1() * scale + scale / 2 + margin;
      double y1 = line.getY1() * scale + scale / 2 + margin;
      double x2 = line.getX2() * scale + scale / 2 + margin;
      double y2 = line.getY2() * scale + scale / 2 + margin;
      return new Line2D.Double(x1, y1, x2, y2);
    }

    public static Rectangle convertCellToInvalidatedRectangle(Cell cell) {
      return convertClickedPointToInvalidatedRectangle(convertCellToClickedPoint(cell));
    }

    public static Rectangle convertClickedPointToRectangleToDrawn(Point point) {
      return new Rectangle((int) point.getX() - 3, (int) point.getY() - 3, 5, 5);
    }

    public static Rectangle convertClickedPointToInvalidatedRectangle(
        Point point) {
      return new Rectangle((int) point.getX() - scale, (int) point.getY()
          - scale, scale * 2, scale * 2);
    }

    public static Cell convertClickedPointToCell(Point point) {
      if (point == null) {
        return null;
      }

      double x = point.getX();
      double y = point.getY();
      x = (x - margin) / scale + 0.5;
      y = (y - margin) / scale + 0.5;
      // System.out.println(point + ">x=" + x + ">" + (int) x);
      return new Cell((int) x, (int) y);
    }

    public static Point convertCellToClickedPoint(Cell cell) {
      double x = cell.getX();
      double y = cell.getY();
      x = (x * scale) + margin + scale / 2;
      y = (y * scale) + margin + scale / 2;
      return new Point((int) x, (int) y);
    }

    public static Rectangle convertLineToInvalidatedRectangle(Point aPoint,
        Point anotherPoint) {
      // Top left corner
      Point point = new Point((int) Math
          .min(aPoint.getX(), anotherPoint.getX()) - 1, (int) Math.min(aPoint
          .getY(), anotherPoint.getY()) - 1);
      // Dimension
      Dimension dimension = new Dimension((int) Math.abs(aPoint.getX()
          - anotherPoint.getX()) + 2, (int) Math.abs(aPoint.getY()
          - anotherPoint.getY()) + 2);
      return new Rectangle(point, dimension);
    }

    public static int[][] convertXYCoordonatesToXYToDraw(int[][] xyCells) {
      int[][] xyPoints = new int[2][xyCells[0].length];
      for (int i = 0; i < xyCells[0].length; i++) {
        xyPoints[0][i] = xyCells[0][i] * scale + margin + scale / 2; // x
        xyPoints[1][i] = xyCells[1][i] * scale + margin + scale / 2; // y
      }
      return xyPoints;
    }

    public static Polygon retrieveRectangleContainingCell(List<Region> regions,
        Cell cell) {
      int[][] points;
      for (Region region : regions) {
        points = region.getPolygon().retrieveXYPoints();
        Polygon awtPolygon = new AwtPolygon(points[0], points[1],
            points[0].length);

        if (awtPolygon.contains(cell)) {
          int[][] pointsToDraw = Util.convertXYCoordonatesToXYToDraw(region
              .getPolygon().retrieveXYPoints());
          return new Polygon(pointsToDraw[0], pointsToDraw[1],
              pointsToDraw[0].length);
        }
      }
      return null;
    }

    public static Region retrieveRegionContainingCell(List<Region> regions,
        Cell cell) {
      int[][] points;
      for (Region region : regions) {
        points = region.getPolygon().retrieveXYPoints();
        Polygon awtPolygon = new AwtPolygon(points[0], points[1],
            points[0].length);

        if (awtPolygon.contains(cell)) {
          return region;
        }
      }
      return null;
    }
  }

  /**
   * @param futureCell
   * @return
   */
  public boolean checkFutureOverlap(Cell futureCell) {
    if (true)
      return true;
    // If no movement, ok. Otherwise, tests...
    // if (Util.retrieveMoveDirection(moveInfo.getOriginDraggedSide(),
    // futureCell) != null)
    {
      Polygon simplePolygon = new Polygon();
      // simplePolygon.addPoint((int) moveInfo.getOriginDraggedSide().getX1(),
      // (int) moveInfo.getOriginDraggedSide().getY1());
      // simplePolygon.addPoint((int) moveInfo.getOriginDraggedSide().getX2(),
      // (int) moveInfo.getOriginDraggedSide().getY2());
      simplePolygon.addPoint((int) futureCell.getX(), (int) futureCell.getY());
      Rectangle newRectangleArea = simplePolygon.getBounds();
      newRectangleArea.grow(1, 1);

      for (Region region : regions) {
        int[][] cells = region.getPolygon().retrieveXYPoints();
        AwtPolygon awtPolygon = new AwtPolygon(cells[0], cells[1],
            cells[0].length);
        // If the polygon does not contain the original cell dragged
        // and intersect the polygon extension, wrong !
        // if (awtPolygon.getSideThatContains(moveInfo.getOriginDraggedCell())
        // == null
        // && awtPolygon.intersects(newRectangleArea))
        // {
        // System.out.println(awtPolygon.intersects(newRectangleArea) + " > "
        // + futureCell + " overlaped by " + region);
        // return false;
        // }
      }
    }
    return true;
  }

  public boolean checkPointAsACorner(Cell cell) {
    for (Region region : regions) {
      if (region.getPolygon().getPath().contains(cell)) {
        return true;
      }
    }
    return false;
  }

}