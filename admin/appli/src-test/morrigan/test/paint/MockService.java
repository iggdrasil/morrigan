package morrigan.test.paint;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import morrigan.test.paint.businessobject.Region;
import morrigan.test.paint.businessobject.Region.RegionType;
import morrigan.test.table.element.Polygon;

public class MockService {

  static public Dimension getMondeDim() {
    return new Dimension(15, 20);
  }

  static public List<Region> getRegions() {
    List<Region> list = new ArrayList<Region>();

    list.add(new Region("Desert d'Ank Morpok", RegionType.desert, Polygon
        .createPolygon("((0,0),(9,0),(9,5),(0,5))")));
    // list.add(new Region("Riviere de l'oubli", RegionType.riviere, Polygon
    // .createPolygon("((0,6),(9,6))")));
    list
        .add(new Region(
            "Foret de la debauche",
            RegionType.foret,
            Polygon
                .createPolygon("((0,7),(5,7),(5,10),(10,10),(10,14),(5,14),(5,17),(0,17))")));
    list.add(new Region("Le marais de la salamandre", RegionType.marais,
        Polygon.createPolygon("((6,7),(6,9),(10,9),(10,7))")));
    return list;
  }
}
