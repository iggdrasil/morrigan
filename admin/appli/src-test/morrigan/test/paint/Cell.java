/**
 * 
 */
package morrigan.test.paint;

import java.awt.Point;

import morrigan.test.table.element.SqlPoint;

public class Cell extends Point {

  public Cell(int i, int j) {
    super(i, j);
  }

  public Cell() {
    super();
  }

  public Cell(SqlPoint point2D) {
    this(point2D.getX(), point2D.getY());
  }

}