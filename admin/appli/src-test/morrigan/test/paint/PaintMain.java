package morrigan.test.paint;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class PaintMain {

  /**
   * @param args
   */
  public static void main(String[] args) {

    Dimension dim = MockService.getMondeDim();
    final int nbColumns = (int) dim.getWidth();
    final int nbRows = (int) dim.getHeight();

    final int echelle = 10;
    final int margin = 3;

    SwingUtilities.invokeLater(new Runnable() {

      public void run() {
        JFrame frame = new JFrame("Test paint");

        MonPanel panel = new MonPanel();
        panel.setDimension(new Dimension(nbColumns, nbRows));
        panel.setScale(echelle);
        panel.setMargin(margin);
        panel.setPreferredSize(new Dimension(nbColumns * echelle + margin * 2,
            nbRows * echelle + margin * 2));

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
      }
    });
  }
}
