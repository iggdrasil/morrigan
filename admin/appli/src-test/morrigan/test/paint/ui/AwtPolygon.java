/*
 * AwtPolygon.java
 * 2 avr. 07
 */
package morrigan.test.paint.ui;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;

public class AwtPolygon extends Polygon {

  public AwtPolygon() {
    super();
  }

  /**
   * Default constructor.
   */
  public AwtPolygon(int xpoints[], int ypoints[], int npoints) {
    super(xpoints, ypoints, npoints);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.awt.Polygon#contains(java.awt.Point)
   */
  @Override
  public boolean contains(Point point) {
    if (point == null) {
      return false;
    }
    // If inside, ok
    if (super.contains(point)) {
      return true;
    }
    // If not, try the boundaries
    return getSideThatContains(point) != null;
  }

  private Line2D getSideThatContains(Point point) {

    PathIterator pathIterator = getPathIterator(null);
    Line2D line = new Line2D.Float();
    float[] previousPoint = null;
    float[] coords = null;
    float[] firstCoords = null;

    while (!pathIterator.isDone()) {
      coords = new float[2];
      int pathSegType = pathIterator.currentSegment(coords);
      if (pathSegType == PathIterator.SEG_MOVETO) {
        firstCoords = coords;
      } else if (pathSegType == PathIterator.SEG_LINETO
          && previousPoint != null) {
        line.setLine(previousPoint[0], previousPoint[1], coords[0], coords[1]);
        // System.out.println(line);
        if (line.ptSegDist(point) == 0) {
          return line;
        }
      } else if (pathSegType == PathIterator.SEG_CLOSE && previousPoint != null
          && firstCoords != null) {
        line.setLine(previousPoint[0], previousPoint[1], firstCoords[0],
            firstCoords[1]);
        if (line.ptSegDist(point) == 0) {
          return line;
        }
        break;
      }
      previousPoint = coords;
      pathIterator.next();
    }

    return null;
  }
}