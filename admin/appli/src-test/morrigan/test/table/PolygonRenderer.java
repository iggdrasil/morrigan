package morrigan.test.table;

import java.awt.Component;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import morrigan.test.table.element.Polygon;
import morrigan.test.table.element.SqlPoint;

public class PolygonRenderer extends DefaultTableCellRenderer implements
    TableCellRenderer {

  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {

    Polygon polygon = (Polygon) value;
    List<SqlPoint> path = polygon.getPath();
    StringBuffer stringBuffer = new StringBuffer("(");
    for (Iterator iter = path.iterator(); iter.hasNext();) {
      SqlPoint point = (SqlPoint) iter.next();
      stringBuffer.append("(" + point.getX() + ',' + point.getY() + ')');
    }
    stringBuffer.append(")");

    DefaultTableCellRenderer cellRenderer = (DefaultTableCellRenderer) super
        .getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
            column);
    cellRenderer.setText(stringBuffer.toString());

    return cellRenderer;
  }

  @Override
  public void paint(Graphics g) {
    super.paint(g);
  }

}
