package morrigan.test.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * A table model using user objects (extending the same class) called RECORD as
 * rows.
 * 
 * @author alagadic
 */
public class MJTableBusinessObjectModel<BO extends BusinessObject> extends
    AbstractTableModel {

  /** Business objects lines. */
  private List<BusinessObject> rows = new ArrayList<BusinessObject>();

  private List<Class> columnClasses = new ArrayList<Class>();

  private List<String> columnNames = new ArrayList<String>();

  public MJTableBusinessObjectModel() {
    super();
  }

  public Class<?> getColumnClass(int columnIndex) {
    return columnClasses.get(columnIndex);
  }

  public int getColumnCount() {
    return columnNames.size();
  }

  public String getColumnName(int columnIndex) {
    return columnNames.get(columnIndex);
  }

  public int getRowCount() {
    return rows.size();
  }

  public Object getValueAt(int rowIndex, int columnIndex) {
    return rows.get(rowIndex).getValue(columnIndex);
  }

  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return rows.get(rowIndex).isEditable();
  }

  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    rows.get(rowIndex).setValue(columnIndex, value);
    fireTableCellUpdated(rowIndex, columnIndex);
  }

  public void addRow(BO boTable) {
    rows.add(boTable);
    checkRowsHomogeneity(boTable);
    fireTableRowsInserted(rows.size() - 1, rows.size() - 1);
    fireTableStructureChanged();
  }

  public void checkRowsHomogeneity(BO newBOTable) {
    // Add new column classes.
    if (columnClasses.size() < newBOTable.getColumnCount()) {
      for (int columnIndex = columnClasses.size(); columnIndex < newBOTable
          .getColumnCount(); columnIndex++) {
        columnClasses.add(columnIndex, newBOTable.getClazz(columnIndex));
        columnNames.add(newBOTable.getColumnName(columnIndex));
      }
    }
  }
}
