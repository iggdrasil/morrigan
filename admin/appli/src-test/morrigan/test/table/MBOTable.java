/**
 * 
 */
package morrigan.test.table;

import java.util.HashMap;
import java.util.Map;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;

/**
 * Structure & initial values of an sql table (as a business object).
 * 
 * @author alagadic
 */
class MBOTable implements BusinessObject {

  private final Table table;

  private final Map<Attribute, Object> values = new HashMap<Attribute, Object>();

  private final Map<Attribute, Object> initialValues;

  /**
   * Constructor.
   * 
   * @param table structure
   * @param initialValues values
   */
  public MBOTable(Table table, Map<Attribute, Object> initialValues) {
    this.table = table;
    this.initialValues = initialValues;
    values.putAll(initialValues);
  }

  public Object getValue(int columnIndex) {
    if (columnIndex > table.getAttributesNb() - 1) {
      return null;
    }

    return getValue(table.getAttribute(columnIndex));
  }

  public Object getValue(Attribute key) {
    return values.get(key);
  }

  public void setValue(int columnIndex, Object newValue) {
    if (columnIndex <= table.getAttributesNb() - 1) {
      setValue(table.getAttribute(columnIndex), newValue);
    }
  }

  public void setValue(Attribute attribute, Object newValue) {
    values.put(attribute, newValue);
  }

  /** see {@link MBOTable#getValue(Attribute)} */
  public Object getValue(Object key) {
    // if (!(key instanceof Attribute)) {
    throw new IllegalArgumentException("The only key allowed is an Attribute");
    // }
    // return null;
  }

  /** see {@link MBOTable#setValue(Attribute, Object)} */
  public void setValue(Object key, Object newValue) {
    throw new IllegalArgumentException("The only key allowed is an Attribute");
  }

  public boolean isEditable() {
    return false;
  }

  public Class getClazz(int columnIndex) {
    if (columnIndex > table.getAttributesNb() - 1) {
      return Object.class;
    }
    return TypeClassHelper.getClassFromTypeChamp(table
        .getAttribute(columnIndex).getType());
  }

  public String getColumnName(int columnIndex) {
    if (columnIndex > table.getAttributesNb() - 1) {
      return null;
    }
    return table.getAttribute(columnIndex).getLabel();
  }

  public boolean hasBeenChanged() {
    return !values.equals(initialValues);
  }

  public int getColumnCount() {
    return table.getAttributesNb();
  }
}