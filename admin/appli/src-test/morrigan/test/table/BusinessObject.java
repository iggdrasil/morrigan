package morrigan.test.table;

public interface BusinessObject {

  public int getColumnCount();

  public Class getClazz(int columnIndex);

  public boolean isEditable();

  public Object getValue(int columnIndex);

  public Object getValue(Object key);

  public void setValue(int columnIndex, Object value);

  public void setValue(Object key, Object value);

  public String getColumnName(int columnIndex);

  public boolean hasBeenChanged();
}
