package morrigan.test.table;

public interface Row {

  public Object getValue(int index);

  public Object getValue(Object key);

  public void setValue(Object key, Object newValue);

}
