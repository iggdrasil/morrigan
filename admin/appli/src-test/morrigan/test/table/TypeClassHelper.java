package morrigan.test.table;

import java.util.Date;

import morrigan.admin.util.METypeChamp;
import morrigan.test.table.element.Polygon;

public class TypeClassHelper {

  public static Class getClassFromTypeChamp(METypeChamp typeChamp) {
    switch (typeChamp) {
      case BOOLEEN:
        return Boolean.class;
      case DATE:
        return Date.class;
      case DATEHEURE:
        return Date.class;
      case DECIMAL:
        return Double.class;
      case ENTIER:
        return Integer.class;
      case HEURE:
        return Date.class;
      case TEXTE:
        return String.class;
      case ZONETEXTE:
        return String.class;
      case POLYGON:
        return Polygon.class;
      default:
        throw new RuntimeException("Type de champ inconnu !");
    }
  }
}