package morrigan.test.table.element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlPoint {

  private int x;

  private int y;

  public SqlPoint(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  /** @deprecated */
  public void translate(int xDelta, int yDelta) {
    this.x += xDelta;
    this.y += yDelta;
  }

  static private final Pattern point2dPattern = Pattern
      .compile("\\(([0-9]+),([0-9]+)\\)");

  @SuppressWarnings("unused")
  static private final Pattern point2dPatternWithSpaces = Pattern
      .compile("\\s*\\(\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*\\)\\s*");

  public static SqlPoint createPoint2D(String xy) {
    Matcher m = point2dPattern.matcher(xy);
    m.matches();
    return new SqlPoint(Integer.parseInt(m.group(1)), Integer.parseInt(m
        .group(2)));
  }

  public static class Float {

    private double x;

    private double y;

    public Float(double x, double y) {
      this.x = x;
      this.y = y;
    }

    static private final Pattern floatPoint2dPattern = Pattern
        .compile("\\(([0-9.]+),([0-9.]+)\\)");

    @SuppressWarnings("unused")
    static private final Pattern floatPoint2dPatternWithSpaces = Pattern
        .compile("\\s*\\(\\s*([0-9.]+)\\s*,\\s*([0-9.]+)\\s*\\)\\s*");

    public static SqlPoint.Float createPoint2D(String xy) {
      Matcher m = floatPoint2dPattern.matcher(xy);
      m.matches();
      return new SqlPoint.Float(Double.parseDouble(m.group(1)), Double
          .parseDouble(m.group(2)));
    }

    public double getX() {
      return x;
    }

    public void setX(double x) {
      this.x = x;
    }

    public double getY() {
      return y;
    }

    public void setY(double y) {
      this.y = y;
    }

  }

  @Override
  public String toString() {
    return "SqlPoint [x=" + x + ";y=" + y + "]";
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }

    if (!(other instanceof SqlPoint)) {
      return false;
    }

    if (this == other) {
      return true;
    }

    SqlPoint otherPoint = (SqlPoint) other;
    return otherPoint.getX() == this.getX() && otherPoint.getY() == this.getY();
  }
}
