package morrigan.test.table.element;

import java.util.LinkedList;
import java.util.List;

public class Path {

  private List<SqlPoint> list = new LinkedList<SqlPoint>();

  public Path(List<SqlPoint> list) {
    this.list = list;
  }

  public List<SqlPoint> getList() {
    return list;
  }

  public void setList(List<SqlPoint> list) {
    this.list = list;
  }

  public void addPoint(SqlPoint point) {
    list.add(point);
  }

}
