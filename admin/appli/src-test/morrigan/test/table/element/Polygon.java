package morrigan.test.table.element;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polygon {

  private List<SqlPoint> path = new LinkedList<SqlPoint>();

  public Polygon(List<SqlPoint> path) {
    this.path = path;
  }

  public Polygon(SqlPoint[] path) {
    Collections.addAll(this.path, path);
  }

  public List<SqlPoint> getPath() {
    return path;
  }

  /**
   * Returns a 2D table containing all the x and all the y coordonates of the
   * points.
   * 
   * @return a 2D table
   */
  public int[][] retrieveXYPoints() {
    int[] x = new int[path.size()];
    int[] y = new int[path.size()];
    int i = 0;
    for (SqlPoint point : path) {
      x[i] = point.getX();
      y[i] = point.getY();
      i++;
    }
    return new int[][] { x, y };
  }

  public void setPath(List<SqlPoint> closedPath) {
    this.path = closedPath;
  }

  // static private final String lightPoint = "\\(\\d+,\\d+\\)";
  static private final String pointPattern = "\\s*\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\)\\s*";

  static private final Pattern pointsPattern = Pattern.compile("^\\((?:("
      + pointPattern + "),)+(" + pointPattern + ")+\\)$");

  // static private final Pattern pointsSplitter = Pattern.compile("\\D+");
  static private final Pattern pointsSplitterPattern = Pattern
      .compile("\\),\\(");

  @SuppressWarnings("unused")
  static private final Pattern pointsWithSpaces = Pattern
      .compile("^\\s*\\(\\s*(?:(\\s*" + pointPattern
          + "\\s*)\\s*,\\s*)+\\s*(\\s*" + pointPattern + "\\s*)+\\s*\\)\\s*$");

  /**
   * Creates a polygon with a string such as "((1,23),(45,0),(6,789))".
   * 
   * @param xys the path in a string format
   * @return a polygon with the desired path
   */
  static public Polygon createPolygon(String xys) {
    Matcher m = pointsPattern.matcher(xys);
    List<SqlPoint> list = new ArrayList<SqlPoint>();

    try {
      m.matches();
      String[] splitResult = pointsSplitterPattern.split(xys, 0);
      // System.out.println(Arrays.asList(splitResult));
      for (int i = 0; i < splitResult.length; i++) {
        // splitResult[i] looks like '123,34' but '(123,34)' is attended by the
        // Point creator
        String xy;
        if (i == 0) {
          xy = splitResult[i].substring(2);
        } else if (i == splitResult.length - 1) {
          xy = splitResult[i].substring(0, splitResult[i].length() - 2);
        } else {
          xy = splitResult[i];
        }
        list.add(SqlPoint.createPoint2D('(' + xy + ')'));
      }

    } catch (java.lang.IllegalStateException e) {
      System.err.println("Impossible de parser le champ >" + xys + "< !!");
      e.printStackTrace();
    }

    // System.out.println(list);

    return new Polygon(list);
  }

  @Override
  public String toString() {
    return "Polygon [path=" + path + "]";
  }

  /**
   * Translate a side (ie 2 points) on the right, left, up or down.
   * 
   * @param line
   * @param xDelta
   * @param yDelta
   * @deprecated
   */
  public void translateSide(SqlPoint pointA, SqlPoint pointB, int xDelta,
      int yDelta) {
    SqlPoint firstPoint = null;

    for (int pointIndex = 0; pointIndex < path.size() + 1; pointIndex++) {
      // We use the % size to make a complete turn around the path
      // (first > ... > last > first)
      SqlPoint point = path.get(pointIndex % path.size());

      // If a point is found
      if ((point.equals(pointA) || point.equals(pointB))) {
        // If it is the first point
        if (firstPoint == null) {
          firstPoint = point;

        } else if (point.equals(firstPoint)) {
          // Or if it is already the first point (because of point over another
          // one)
          firstPoint = point;
        } else {
          // This is the second point !

          // First and second points are translated
          firstPoint.translate(xDelta, yDelta);
          point.translate(xDelta, yDelta);
          break;
        }
      } else {
        // If a first point has been set but the next point is not the second
        // one, the first point is false !
        firstPoint = null;
      }
    }
  }

  static boolean areEquals(SqlPoint point, java.awt.geom.Point2D otherPoint) {
    return (point.getX() == otherPoint.getX() && point.getY() == otherPoint
        .getY());
  }
}
