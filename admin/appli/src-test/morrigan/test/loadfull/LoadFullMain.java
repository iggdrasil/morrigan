package morrigan.test.loadfull;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAWindowContext;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.launching.LauncherFactory;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.application.MinLauncherInitializer;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.launcher.Launcher;
import morrigan.common.launching.step.configuration.DiaFileLoading;

public class LoadFullMain extends JFrame {

  private static final String KO = "KO";

  private static final String EMPTY_STRING = "";

  private static final String OK = "ok";

  private DefaultTableModel model;

  public LoadFullMain() {
    model = new DefaultTableModel(new String[] { "Table name", "Status",
        "Error message", "Stack" }, 0);
    JTable table = new JTable(model);
    JScrollPane scrollPane = new JScrollPane(table);

    this.getContentPane().add(scrollPane);
    Dimension minimumSize = new Dimension(200, 400);
    this.setPreferredSize(minimumSize);
    this.setMinimumSize(minimumSize);

    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setVisible(true);
  }

  private void addRow(String tableName, String status, String errorMessage,
      String stack) {
    model.addRow(new String[] { tableName, status, errorMessage, stack });
  }

  public static void main(String[] args) {
    LoadFullMain loadFullMain = new LoadFullMain();
    loadFullMain.pack();

    Initializer initializer = MinLauncherInitializer
        .getMinLauncherInitializer();
    System.setProperty("launching.console", "true");
    Launcher launcher = LauncherFactory.createLauncher();
    launcher.launchInitializer(initializer);
    try {
      new DiaFileLoading(true).consume();
    } catch (InitException e1) {
      e1.printStackTrace();
    }

    List<String> notToLoad = new ArrayList<String>();
    for (MEASpecificWindow w : MEASpecificWindow.values()) {
      notToLoad.add(w.getSysName());
      loadFullMain.addRow(w.getSysName(), "-", EMPTY_STRING, EMPTY_STRING);
    }

    Set<MASysWindow> sysWindows = MAWindowContext.getInstance()
        .getFenetresWindow();
    for (MASysWindow sysWindow : sysWindows) {
      try {
        if (!notToLoad.contains(sysWindow.getSysName())) {
          MAWindowManager.createWindow(sysWindow);
          loadFullMain.addRow(sysWindow.getSysName(), OK, EMPTY_STRING,
              EMPTY_STRING);
        }
      } catch (Exception e) {
        e.printStackTrace();
        loadFullMain.addRow(sysWindow.getSysName(), KO, e.getMessage(),
            toString(e));
      }

    }
  }

  static public String toString(Throwable e) {
    StringBuffer sb = new StringBuffer();
    StackTraceElement[] stackTrace = e.getStackTrace();
    for (StackTraceElement stackElement : stackTrace) {
      sb.append(stackElement.toString()).append('\n');
    }
    return sb.toString();
  }
}
