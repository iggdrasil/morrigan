package morrigan.labeleditor;

import javax.swing.SwingUtilities;

import morrigan.admin.context.MAContextManager;
import morrigan.common.SynchronizationChecker;
import morrigan.common.launching.LauncherFactory;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.application.LabelEditorInitializer;
import morrigan.common.launching.launcher.Launcher;

/**
 * @author Armel
 */
public final class SimpleLabelEditorMain {

  /**
   * Hidden constructor.
   */
  private SimpleLabelEditorMain() {}

  /**
   * Launch.
   * 
   * @param args Arguments
   */
  public static void main(String[] args) {
    try {
      // MAContextManager.initPreLaunchingContext();
      Initializer initializer = LabelEditorInitializer
          .getLabelEditorInitializer();
      Launcher launcher = LauncherFactory.createLauncher();
      launcher.launchInitializer(initializer);
      // STEP.LOG.execute();
      // MAContextManager.initContext(CONTEXT_TYPE.SIMPLE);
      // STEP.CHARGEMENT_FICHIER_DIA.execute();
      checkSynchroInThread();
      // showWindowInThread();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Build the editor in a thread launched by the SwingUtilities class.
   */
  private static void showWindowInThread() {
    SwingUtilities.invokeLater(new Runnable() {

      public void run() {
        SimpleLabelEditor editor = new SimpleLabelEditor();
        editor.setVisible(true);
      }
    });
  }

  /**
   * Check the synchronization between the dia file and menu file in a specific
   * thread.
   */
  private static void checkSynchroInThread() {
    SynchronizationChecker synchronizationChecker = new SynchronizationChecker(
        MAContextManager.getInstance().getGraphicContext().getMenuFile(),
        MAContextManager.getInstance().getDbContext().getTables());

    synchronizationChecker.start();
  }
}
