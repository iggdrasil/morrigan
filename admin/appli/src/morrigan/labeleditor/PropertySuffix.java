package morrigan.labeleditor;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;

/**
 * Contains the suffixes used in the property file listing the overriding
 * settings of {@link Table} and {@link Attribute}.
 * 
 * @author alagadic
 */
public interface PropertySuffix {

  public final static String DOT_LABEL = ".label";

  public final static String DOT_COMMENT = ".comment";

  public final static String DOT_ENABLED = ".enabled";

  public final static String DOT_MANDATORY = ".mandatory";
}
