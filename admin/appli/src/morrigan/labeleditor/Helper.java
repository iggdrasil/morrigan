package morrigan.labeleditor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.common.launching.ActionLauncher;
import morrigan.labeleditor.util.SqlAttributeTreeWrapper;
import morrigan.labeleditor.util.SqlTableWrapper;

import org.jdom.JDOMException;

public class Helper {

  public static synchronized DefaultMutableTreeNode loadDiaFile()
      throws FileNotFoundException, IOException, JDOMException {
    ActionLauncher.chargerFichierDia(false);

    Map<String, Table> tables = MAContextManager.getInstance().getDbContext()
        .getTables();
    DefaultMutableTreeNode root = new DefaultMutableTreeNode("Tables");

    // Extract and create info in the good order
    SortedSet<DefaultMutableTreeNode> sortedSet = new TreeSet<DefaultMutableTreeNode>(
        new TreeNodeComparator());
    for (Iterator<Table> iterTables = tables.values().iterator(); iterTables
        .hasNext();) {
      Table table = iterTables.next();
      DefaultMutableTreeNode tableNode = new DefaultMutableTreeNode(
          new SqlTableWrapper(table));
      for (Iterator<Attribute> iter = table.getAttributesIterator(); iter
          .hasNext();) {
        Attribute attribute = iter.next();
        tableNode.add(new DefaultMutableTreeNode(new SqlAttributeTreeWrapper(
            attribute)));
      }
      sortedSet.add(tableNode);
    }

    // Put it in the tree
    for (Iterator<DefaultMutableTreeNode> iter = sortedSet.iterator(); iter
        .hasNext();) {
      MutableTreeNode element = iter.next();
      root.add(element);
    }

    return root;
  }

  public static synchronized Properties loadPropertiesFile()
      throws FileNotFoundException, IOException {
    FileInputStream inputStream = null;
    try {
      Properties properties = new Properties();
      inputStream = new FileInputStream(MAContextManager.getInstance()
          .getSchemaContext().getTableDescriptionFile());
      properties.load(inputStream);

      return properties;
    } finally {
      if (inputStream != null) {
        try {
          inputStream.close();
        } catch (IOException e) {
          // Do nothing
        }
      }
    }
  }

  static private class TreeNodeComparator implements
      Comparator<DefaultMutableTreeNode> {

    /**
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(DefaultMutableTreeNode o1, DefaultMutableTreeNode o2) {
      String string = o1.getUserObject().toString();
      String otherString = o2.getUserObject().toString();
      return string.compareTo(otherString);
    }
  }
}
