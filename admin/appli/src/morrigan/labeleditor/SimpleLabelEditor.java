package morrigan.labeleditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.util.EqualsHelper;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.labeleditor.util.SqlJTreeTable;
import morrigan.labeleditor.util.SqlTableTreeModel;
import morrigan.log.MLog;

public class SimpleLabelEditor extends MJFramePosition implements
    ActionListener {

  private SqlTableTreeModel sqlTableTreeModel;

  private Properties propertiesLoaded;

  private SqlJTreeTable treeTable;

  public enum ACTION {
    SAUVER,
    QUITTER,
    ABOUT;
  }

  public SimpleLabelEditor() {
    super("Label editor");
    this.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent we) {
        if (propertiesLoaded == null) {
          return;
        }

        if (!propertiesLoaded.equals(sqlTableTreeModel.getProperties())) {
          int retour = JOptionPane.showConfirmDialog(SimpleLabelEditor.this,
              "Sauver ?", "Sauver", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
          if (retour == JOptionPane.YES_OPTION) {
            saveProperties(sqlTableTreeModel.getProperties());
          }
          super.windowClosing(we);
        }

      }
    });

    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.setResizable(true);
    buildMenu();
    buildTreeTable();
    add(new JScrollPane(treeTable));
    setSize(1000, 500);
  }

  private void buildMenu() {
    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);

    JMenu menuFichier = new JMenu("Fichier");
    menuFichier.setMnemonic(KeyEvent.VK_F);
    JMenuItem menuItemSauver = new JMenuItem("Sauver", KeyEvent.VK_S);
    menuItemSauver.setAccelerator(KeyStroke.getKeyStroke('S',
        InputEvent.ALT_MASK));
    menuItemSauver.addActionListener(this);
    menuItemSauver.setActionCommand(ACTION.SAUVER.name());
    menuFichier.add(menuItemSauver);

    JMenuItem menuItemQuitter = new JMenuItem("Quitter", KeyEvent.VK_Q);
    menuItemQuitter.setAccelerator(KeyStroke.getKeyStroke('Q',
        InputEvent.ALT_MASK));
    menuItemQuitter.addActionListener(this);
    menuItemQuitter.setActionCommand(ACTION.QUITTER.name());
    menuFichier.add(menuItemQuitter);

    menuBar.add(menuFichier);
  }

  private void buildTreeTable() {
    DefaultMutableTreeNode root = null; // ??
    Properties properties = null; // ??
    sqlTableTreeModel = new SqlTableTreeModel(root, properties);

    try {
      root = Helper.loadDiaFile();
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      sqlTableTreeModel.setRoot(root);
      propertiesLoaded = Helper.loadPropertiesFile();
      sqlTableTreeModel.setProperties(propertiesLoaded);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    treeTable = new SqlJTreeTable(sqlTableTreeModel);

    int widths[] = new int[] { 200, 280, 500, 30, 30, 100, 40, 20, 20, 100 };

    for (int i = 0; i < treeTable.getColumnCount() - 1; i++) {
      treeTable.getColumnModel().getColumn(i).setPreferredWidth(widths[i]);
    }
    treeTable.getColumnModel().getColumn(SqlTableTreeModel.COLONNE.ID.getNum())
        .setMaxWidth(widths[SqlTableTreeModel.COLONNE.ID.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.FOREIGN_ID.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.FOREIGN_ID.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.ENABLED.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.ENABLED.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.MANDATORY.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.MANDATORY.getNum()]);
  }

  public void actionPerformed(ActionEvent e) {
    ACTION action = ACTION.valueOf(e.getActionCommand());
    switch (action) {
      case SAUVER:
        checkAndSaveProperties(sqlTableTreeModel.getProperties());
        break;
      case ABOUT:
        break;
      case QUITTER:
        if (propertiesLoaded == null) {
          return;
        }
        if (!propertiesLoaded.equals(sqlTableTreeModel.getProperties())) {
          int retour = JOptionPane.showConfirmDialog(SimpleLabelEditor.this,
              "Sauver ?", "Sauver", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
          if (retour == JOptionPane.YES_OPTION) {
            saveProperties(sqlTableTreeModel.getProperties());
          }
          this.dispose();
          System.exit(0);
        }

        break;
      default:
        break;
    }
  }

  private void checkAndSaveProperties(Properties properties) {
    if (!EqualsHelper.equals(propertiesLoaded, properties)) {
      saveProperties(properties);
    } else {
      MLog.infos("No change on labels.");
    }
  }

  private void saveProperties(Properties properties) {
    MLog.infos("Saving labels...");

    FileOutputStream out;
    try {
      out = new FileOutputStream(MAContextManager.getInstance()
          .getSchemaContext().getTableDescriptionFile());
      properties.store(out, "morrigan complement tables");
      out.close();
      propertiesLoaded.putAll(properties);
      MLog.infos("Labels saved !");
    } catch (FileNotFoundException fnfe) {
      fnfe.printStackTrace();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
