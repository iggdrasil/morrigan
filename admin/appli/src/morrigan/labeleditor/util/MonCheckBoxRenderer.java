/**
 * 
 */
package morrigan.labeleditor.util;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import morrigan.graphique.table.renderer.CheckBoxRenderer;
import morrigan.labeleditor.util.SqlTableTreeModel.COLONNE;

class MonCheckBoxRenderer extends CheckBoxRenderer {

  private final JTree tree;

  public MonCheckBoxRenderer(JTree tree) {
    super();
    this.tree = tree;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {
    Component renderer = super.getTableCellRendererComponent(table, value,
        isSelected, hasFocus, row, column);

    if (!(renderer instanceof MonCheckBoxRenderer)) {
      return renderer;
    }

    renderer.setEnabled(table.isCellEditable(row, column));

    // Business object
    TreePath treePath = tree.getPathForRow(row);
    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath
        .getLastPathComponent();
    Object userObject = treeNode.getUserObject();

    // Set different background color if value different from dia schema
    if (userObject instanceof ElementTreeWrapper) {

      ElementTreeWrapper wrapper = (ElementTreeWrapper) userObject;
      COLONNE colonne = SqlTableTreeModel.COLONNE.getColonneByNum(column);
      Object displaiedValue = table.getValueAt(row, column);
      if (!wrapper.getValue(colonne).equals(displaiedValue)) {
        // Background color
        Color backgroundColor = null;
        if (isSelected) {
          // 16-23 are red, 8-15 are green, 0-7 are blue
          backgroundColor = new Color(
              table.getSelectionBackground().getRGB() + 0x171717);
        } else {
          backgroundColor = new Color(table.getBackground().getRGB() - 0x171717);
        }
        renderer.setBackground(backgroundColor);
      }
    }

    return renderer;
  }
}