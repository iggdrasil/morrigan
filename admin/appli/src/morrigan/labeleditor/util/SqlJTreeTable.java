package morrigan.labeleditor.util;

import java.util.Properties;

import javax.swing.tree.TreeNode;

import morrigan.graphique.table.tree.JTreeTable;

public class SqlJTreeTable extends JTreeTable {

  // private SqlTableTreeModel treeTableModel;

  public SqlJTreeTable(SqlTableTreeModel treeTableModel) {
    super(treeTableModel);
    // this.treeTableModel = treeTableModel;
    setDefaultRenderer(Boolean.class, new MonCheckBoxRenderer(getTree()));
    // setDefaultRenderer(String.class, new StringRenderer());
    // setDefaultEditor(String.class, new StringEditor());

    tree.setShowsRootHandles(false);
  }

  // @Override
  // public TableCellRenderer getCellRenderer(int row, int column) {
  // return super.getCellRenderer(row, column);
  // }

  @Deprecated
  public void setProperties(Properties properties) {
    ((SqlTableTreeModel) getModel()).setProperties(properties);
    repaint();
  }

  @Deprecated
  public void setRoot(TreeNode rootNode) {
    // treeTableModel.setRoot(node);
    ((SqlTableTreeModel) getModel()).setRoot(rootNode);

    repaint();
  }
}
