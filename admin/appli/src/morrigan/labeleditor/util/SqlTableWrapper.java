package morrigan.labeleditor.util;

import java.util.Iterator;
import java.util.List;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Operation;
import morrigan.admin.database.tabledesc.Table;
import morrigan.labeleditor.util.SqlTableTreeModel.COLONNE;

public class SqlTableWrapper implements Table, ElementTreeWrapper {

  private final Table table;

  public SqlTableWrapper(Table table) {
    super();
    this.table = table;
  }

  public String getName() {
    return table.getName();
  }

  public String getLabel() {
    return table.getLabel();
  }

  public boolean isEnabled() {
    return table.isEnabled();
  }

  public int getAttributesNb() {
    return table.getAttributesNb();
  }

  public Attribute getAttribute(String nomAttribut) {
    return table.getAttribute(nomAttribut);
  }

  public Iterator<Attribute> getAttributesIterator() {
    return table.getAttributesIterator();
  }

  public Attribute getAttribute(int index) {
    return table.getAttribute(index);
  }

  public Iterator<Operation> getOperationsIterator() {
    return table.getOperationsIterator();
  }

  public Object getValue(COLONNE colonne) {
    switch (colonne) {
      case ENABLED:
        return table.isEnabled();
      case LABEL:
        return table.getLabel();
      case NOM:
        return table.getName();
      case COMMENT:
      case FOREIGN_ID:
      case ID:
      case MANDATORY:
      case TABLE:
      case TAILLE:
      case TYPE:
      default:
        return null;
    }
  }

  /**
   * Used for the tree view.
   */
  @Override
  public String toString() {
    return table.getName();
  }

  /**
   * @see morrigan.admin.database.tabledesc.Table#getUniqueIndexAttributes()
   */
  public List<Attribute> getUniqueIndexAttributes() {
    return this.table.getUniqueIndexAttributes();
  }

}
