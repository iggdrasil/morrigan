package morrigan.labeleditor.util;

import java.util.Properties;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.graphique.table.tree.model.AbstractTreeTableModel;
import morrigan.graphique.table.tree.model.TreeTableModel;
import morrigan.labeleditor.PropertySuffix;

public class SqlTableTreeModel extends AbstractTreeTableModel implements
    TreeTableModel {

  public enum COLONNE {
    NOM("Table - Colonne", true /* obligatoire */, TreeTableModel.class),
    LABEL("Label", true, String.class),
    COMMENT("Comentaire", true, String.class),
    MANDATORY("Oblg", true, Boolean.class),
    ENABLED("Modf", true, Boolean.class),
    TYPE("Type", false, String.class),
    TAILLE("Taille", false, Integer.class),
    ID("PK", false, Boolean.class),
    FOREIGN_ID("FK", false, Boolean.class),
    TABLE("Table", false, String.class);

    private final String nom;

    private int num;

    private final boolean isEditable;

    private final Class clazz;

    private COLONNE(String nom, boolean isEditable, Class clazz) {
      this.nom = nom;
      this.isEditable = isEditable;
      this.clazz = clazz;
    }

    public String getNom() {
      return nom;
    }

    public void setNum(int num) {
      this.num = num;
    }

    public int getNum() {
      return num;
    }

    public boolean isEditable() {
      return isEditable;
    }

    public Class getClazz() {
      return clazz;
    }

    static public COLONNE getColonneByNum(int num) {
      for (COLONNE colonne : values()) {
        if (colonne.getNum() == num) {
          return colonne;
        }
      }
      throw new RuntimeException("The column '" + num + "' is unknown");
    }
  }

  static COLONNE[] colonnes = new COLONNE[] { COLONNE.NOM, COLONNE.LABEL,
      COLONNE.COMMENT, COLONNE.MANDATORY, COLONNE.ENABLED, COLONNE.TYPE,
      COLONNE.TAILLE, COLONNE.ID, COLONNE.FOREIGN_ID, COLONNE.TABLE };

  static {
    // Set the column number
    for (int num = 0; num < colonnes.length; num++) {
      colonnes[num].setNum(num);
    }
  }

  private Properties properties = new Properties();

  public SqlTableTreeModel(TreeNode root, Properties properties) {
    super(root);
    setProperties(properties);
  }

  public int getColumnCount() {
    return colonnes.length;
  }

  public String getColumnName(int column) {
    return colonnes[column].getNom();
  }

  @Override
  public Class getColumnClass(int column) {
    return colonnes[column].getClazz();
  }

  public Object getChild(Object parent, int index) {
    return ((DefaultMutableTreeNode) parent).getChildAt(index);
  }

  public int getChildCount(Object parent) {
    return ((MutableTreeNode) parent).getChildCount();
  }

  @Override
  public boolean isCellEditable(Object node, int column) {

    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
    Object userObject = treeNode.getUserObject();

    if (getRoot() == node) {
      return false;
    }

    COLONNE colonne = COLONNE.getColonneByNum(column);
    if (userObject instanceof Table) {
      return (colonne == COLONNE.NOM || colonne == COLONNE.COMMENT
          || colonne == COLONNE.LABEL || colonne == COLONNE.ENABLED);
    } else if (userObject instanceof Attribute) {
      Attribute attribute = (Attribute) userObject;
      if (COLONNE.MANDATORY == colonne) {
        // On peut dire qu'un champ est obligatoire s'il ne l'est pas en base.
        // Mais pas l'inverse !
        return colonne.isEditable() && !attribute.isMandatory();
      } else if (COLONNE.ENABLED == colonne && attribute.isID()) {
        // Un ID (cl� primaire) n'est pas modifiable
        return false;
      }
    }

    return colonne.isEditable();
  }

  @Override
  public void setValueAt(Object value, Object node, int column) {
    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
    Object userObject = treeNode.getUserObject();

    COLONNE colonne = COLONNE.getColonneByNum(column);
    String text = null;
    if (value instanceof String) {
      text = (String) value;
      if (text.trim().length() == 0) {
        text = null;
      }
    } else if (value instanceof Boolean) {
      text = ((Boolean) value).toString();
    }

    if (userObject instanceof Table) {
      Table table = (Table) userObject;
      switch (colonne) {
        case LABEL:
          putProperty(table.getName() + PropertySuffix.DOT_LABEL, text);
          break;
        case COMMENT:
          putProperty(table.getName() + PropertySuffix.DOT_COMMENT, text);
          break;
        case ENABLED:
          if (((Boolean) value) == table.isEnabled()) {
            text = null;
          }
          putProperty(table.getName() + PropertySuffix.DOT_ENABLED, text);
          break;
        default:
          throw new RuntimeException("The column '" + colonne
              + "' was not attended here !");
      }

    } else if (userObject instanceof Attribute) {
      Attribute attribute = (Attribute) userObject;
      switch (colonne) {
        case LABEL:
          putProperty(attribute.getName() + PropertySuffix.DOT_LABEL, text);
          break;
        case COMMENT:
          putProperty(attribute.getName() + PropertySuffix.DOT_COMMENT, text);
          break;
        case ENABLED:
          if (((Boolean) value) == attribute.isEnabled()) {
            text = null;
          }
          putProperty(attribute.getName() + PropertySuffix.DOT_ENABLED, text);
          break;
        case MANDATORY:
          if (((Boolean) value) == attribute.isMandatory()) {
            text = null;
          }
          putProperty(attribute.getName() + PropertySuffix.DOT_MANDATORY, text);
          break;
        default:
          throw new RuntimeException("The column '" + colonne
              + "' was not attended here !");
      }
    }
  }

  public Object getValueAt(Object node, int column) {
    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
    Object userObject = treeNode.getUserObject();
    if (userObject instanceof Table) {
      Table table = (Table) userObject;
      switch (COLONNE.getColonneByNum(column)) {
        case NOM:
          return table.getName();
        case LABEL:
          return properties.getProperty(table.getName()
              + PropertySuffix.DOT_LABEL, table.getLabel());
        case COMMENT:
          return properties.getProperty(table.getName()
              + PropertySuffix.DOT_COMMENT, "");
        case ENABLED:
          return Boolean.valueOf(properties
              .getProperty(table.getName() + PropertySuffix.DOT_ENABLED,
                  Boolean.toString(table.isEnabled())));
        case MANDATORY:
        case TYPE:
        case TAILLE:
        case ID:
        case FOREIGN_ID:
        case TABLE:
        default:
          return null;
      }
    } else if (userObject instanceof Attribute) {
      Attribute attribute = (Attribute) userObject;
      switch (COLONNE.getColonneByNum(column)) {
        case NOM:
          return attribute.getName();
        case LABEL:
          return properties.getProperty(attribute.getName()
              + PropertySuffix.DOT_LABEL, attribute.getLabel());
        case COMMENT:
          return properties.getProperty(attribute.getName()
              + PropertySuffix.DOT_COMMENT, attribute.getComment());
        case MANDATORY:
          return Boolean.valueOf(properties.getProperty(attribute.getName()
              + PropertySuffix.DOT_MANDATORY, Boolean.toString(attribute
              .isMandatory())));
        case ENABLED:
          return Boolean.valueOf(properties.getProperty(attribute.getName()
              + PropertySuffix.DOT_ENABLED, Boolean.toString(attribute
              .isEnabled())));
        case TYPE:
          return attribute.getType().toString();
        case TAILLE:
          return attribute.getSize();
        case ID:
          return attribute.isID();
        case FOREIGN_ID:
          return attribute.isForeignID();
        case TABLE:
          return null;
        default:
          break;
      }
    } else if (getRoot() == node) {
      return column == COLONNE.NOM.getNum() ? "/" : null;
    }

    return null;
  }

  public Properties getProperties() {
    return properties;
  }

  private void putProperty(String key, String text) {
    if (text == null) {
      properties.remove(key);
    } else {
      properties.put(key, text);
    }
  }

  public void setProperties(Properties properties) {
    this.properties.clear();
    if (properties != null) {
      this.properties.putAll(properties);
      // Parent needs to be repainted
    }
  }

}
