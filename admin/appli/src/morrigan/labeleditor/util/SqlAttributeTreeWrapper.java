package morrigan.labeleditor.util;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.util.METypeChamp;
import morrigan.labeleditor.util.SqlTableTreeModel.COLONNE;

public class SqlAttributeTreeWrapper implements Attribute, ElementTreeWrapper {

  private final Attribute attribute;

  public SqlAttributeTreeWrapper(Attribute attribute) {
    super();
    this.attribute = attribute;
  }

  public String getName() {
    return attribute.getName();
  }

  public String getLabel() {
    return attribute.getLabel();
  }

  public String getComment() {
    return attribute.getComment();
  }

  public boolean isMandatory() {
    return attribute.isMandatory();
  }

  public void setComment(String comment) {
    attribute.setComment(comment);
  }

  public void setLabel(String label) {
    attribute.setLabel(label);
  }

  public void setMandatory(boolean isMandatory) {
    attribute.setMandatory(isMandatory);
  }

  public Integer getSize() {
    return attribute.getSize();
  }

  public METypeChamp getType() {
    return attribute.getType();
  }

  public boolean isEnabled() {
    return attribute.isEnabled();
  }

  public Table getTable() {
    return attribute.getTable();
  }

  public boolean isID() {
    return attribute.isID();
  }

  public boolean isForeignID() {
    return attribute.isForeignID();
  }

  public Object getValue(COLONNE colonne) {
    switch (colonne) {
      case COMMENT:
        return attribute.getComment();
      case ENABLED:
        return attribute.isEnabled();
      case FOREIGN_ID:
        return attribute.isForeignID();
      case ID:
        return attribute.isID();
      case LABEL:
        return attribute.getLabel();
      case MANDATORY:
        return attribute.isMandatory();
      case NOM:
        return attribute.getName();
      case TABLE:
        return attribute.getTable();
      case TAILLE:
        return attribute.getSize();
      case TYPE:
        return attribute.getType();
      default:
        return null;
    }
  }

  /**
   * Used for the tree view.
   */
  @Override
  public String toString() {
    return attribute.getName();
  }
}
