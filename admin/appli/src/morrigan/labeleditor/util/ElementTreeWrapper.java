package morrigan.labeleditor.util;

import morrigan.labeleditor.util.SqlTableTreeModel.COLONNE;

public interface ElementTreeWrapper {

  public Object getValue(COLONNE colonne);

}
