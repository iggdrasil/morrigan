package morrigan.labeleditor.complete;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.util.EqualsHelper;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.graphique.table.tree.model.TreeTableModelAdapter;
import morrigan.labeleditor.Helper;
import morrigan.labeleditor.util.SqlJTreeTable;
import morrigan.labeleditor.util.SqlTableTreeModel;
import morrigan.log.MLog;

import org.jdom.JDOMException;

public class CompleteLabelEditor extends MJFramePosition implements
    ActionListener {

  private SqlTableTreeModel sqlTableTreeModel;

  private Properties propertiesLoaded;

  private SqlJTreeTable treeTable;

  public enum ACTION {
    SAUVER,
    QUITTER,
    OUVRIR_DIA,
    OUVRIR_PROP,
    ABOUT;
  }

  public CompleteLabelEditor() {
    super("Label editor");
    this.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent we) {
        if (propertiesLoaded == null) {
          return;
        }

        if (!propertiesLoaded.equals(sqlTableTreeModel.getProperties())) {
          int retour = JOptionPane.showConfirmDialog(CompleteLabelEditor.this,
              "Sauver ?", "Sauver", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
          if (retour == JOptionPane.YES_OPTION) {
            saveProperties(sqlTableTreeModel.getProperties());
          }
          super.windowClosing(we);
        }

      }
    });

    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.setResizable(true);
    buildMenu();
    buildTreeTable();
    add(new JScrollPane(treeTable));
    setSize(1000, 500);
  }

  private void buildMenu() {
    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);

    JMenu menuFichier = new JMenu("Fichier");
    menuFichier.setMnemonic(KeyEvent.VK_F);
    JMenuItem menuItemSauver = new JMenuItem("Sauver", KeyEvent.VK_S);
    menuItemSauver.setAccelerator(KeyStroke.getKeyStroke('S',
        InputEvent.ALT_MASK));
    menuItemSauver.addActionListener(this);
    menuItemSauver.setActionCommand(ACTION.SAUVER.name());
    menuFichier.add(menuItemSauver);

    // JMenuItem menuItemOuvrir = new JMenuItem("Ouvrir properties",
    // KeyEvent.VK_O);
    // menuItemOuvrir.setAccelerator(KeyStroke.getKeyStroke('O'));
    // menuItemOuvrir.addActionListener(this);
    // menuItemOuvrir.setActionCommand(ACTION.OUVRIR_PROP.name());
    // menuFichier.add(menuItemOuvrir);
    //
    // JMenuItem menuItemOuvrirDia = new JMenuItem("Ouvrir dia", KeyEvent.VK_U);
    // menuItemOuvrirDia.setAccelerator(KeyStroke.getKeyStroke('U'));
    // menuItemOuvrirDia.addActionListener(this);
    // menuItemOuvrirDia.setActionCommand(ACTION.OUVRIR_DIA.name());
    // menuFichier.add(menuItemOuvrirDia);

    JMenuItem menuItemQuitter = new JMenuItem("Quitter", KeyEvent.VK_Q);
    menuItemQuitter.setAccelerator(KeyStroke.getKeyStroke('Q',
        InputEvent.ALT_MASK));
    menuItemQuitter.addActionListener(this);
    menuItemQuitter.setActionCommand(ACTION.QUITTER.name());
    menuFichier.add(menuItemQuitter);

    menuBar.add(menuFichier);
  }

  private void buildTreeTable() {
    // if (sqlTableTreeModel != null) {
    // throw new IllegalStateException("The Tree Table has already been created
    // !");
    // }

    DefaultMutableTreeNode root = null; // ??
    Properties properties = null; // ??
    sqlTableTreeModel = new SqlTableTreeModel(root, properties);

    try {
      root = Helper.loadDiaFile();
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      // sqlTableTreeModel.setRoot(root);
      propertiesLoaded = Helper.loadPropertiesFile();
      sqlTableTreeModel.setProperties(propertiesLoaded);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    // Marche pas : new PUIS setRoot
    // treeTable = new SqlJTreeTable(sqlTableTreeModel);
    // ((TreeTableModelAdapter) treeTable.getModel()).setRoot(root);
    // sqlTableTreeModel.setRoot(root);

    // Marche : setRoot PUIS new
    sqlTableTreeModel.setRoot(root);
    treeTable = new SqlJTreeTable(sqlTableTreeModel);

    int widths[] = new int[] { 200, 280, 500, 30, 30, 100, 40, 20, 20, 100 };

    for (int i = 0; i < treeTable.getColumnCount() - 1; i++) {
      treeTable.getColumnModel().getColumn(i).setPreferredWidth(widths[i]);
    }
    treeTable.getColumnModel().getColumn(SqlTableTreeModel.COLONNE.ID.getNum())
        .setMaxWidth(widths[SqlTableTreeModel.COLONNE.ID.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.FOREIGN_ID.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.FOREIGN_ID.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.ENABLED.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.ENABLED.getNum()]);
    treeTable.getColumnModel().getColumn(
        SqlTableTreeModel.COLONNE.MANDATORY.getNum()).setMaxWidth(
        widths[SqlTableTreeModel.COLONNE.MANDATORY.getNum()]);
  }

  public void actionPerformed(ActionEvent e) {
    ACTION action = ACTION.valueOf(e.getActionCommand());
    switch (action) {
      case SAUVER:
        checkAndSaveProperties(sqlTableTreeModel.getProperties());
        break;
      case OUVRIR_DIA:
        setAndLoadDiaFile(MAContextManager.getInstance().getSchemaContext()
            .getDiaFile());

        // File selectedFile = showFileChooser("All dia files", "dia");
        // if (selectedFile != null) {
        // clearTreeTable();
        // setAndLoadDiaFile(selectedFile);
        // }
        break;
      case OUVRIR_PROP:
        setAndLoadPropertiesFile(MAContextManager.getInstance()
            .getSchemaContext().getTableDescriptionFile());

        // selectedFile = showFileChooser("All properties files", "properties");
        // if (selectedFile != null) {
        // clearTreeTable();
        // setAndLoadPropertiesFile(selectedFile);
        // }
        break;
      case ABOUT:
        break;
      case QUITTER:
        if (propertiesLoaded == null) {
          return;
        }
        if (!propertiesLoaded.equals(sqlTableTreeModel.getProperties())) {
          int retour = JOptionPane.showConfirmDialog(CompleteLabelEditor.this,
              "Sauver ?", "Sauver", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
          if (retour == JOptionPane.YES_OPTION) {
            saveProperties(sqlTableTreeModel.getProperties());
          }
          this.dispose();
          System.exit(0);
        }

        break;
      default:
        break;
    }
  }

  @Deprecated
  private File showFileChooser(String description, String extention) {
    JFileChooser fileChooser = new JFileChooser();
    ExtensionFileFilter filter = new ExtensionFileFilter();
    filter.addExtension(extention);
    filter.setDescription(description);
    fileChooser.setFileFilter(filter);
    int returnVal = fileChooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File selectedFile = fileChooser.getSelectedFile();
      System.out.println("You chose to open this file: "
          + selectedFile.getName());
      return selectedFile;
    }
    return null;
  }

  public void setAndLoadDiaFile(File diaFile) {
    MAContextManager.getInstance().getSchemaContext().setDiaFile(diaFile);
    try {
      ((TreeTableModelAdapter) treeTable.getModel()).setRoot(Helper
          .loadDiaFile());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (JDOMException e) {
      e.printStackTrace();
    }
  }

  @Deprecated
  private void setAndLoadPropertiesFile(File propertiesFile) {
    try {
      MAContextManager.getInstance().getSchemaContext()
          .setTableDescriptionFile(propertiesFile);
      treeTable.setProperties(propertiesLoaded = Helper.loadPropertiesFile());
    } catch (FileNotFoundException fnfe) {
      // Pas grave !!
      // fnfe.printStackTrace();
      System.out.println("Nouveau fichier de desc.");
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  private void checkAndSaveProperties(Properties properties) {
    if (!EqualsHelper.equals(propertiesLoaded, properties)) {
      saveProperties(properties);
    } else {
      MLog.infos("No change on labels.");
    }
  }

  private void saveProperties(Properties properties) {
    MLog.infos("Save labels...");

    FileOutputStream out;
    try {
      out = new FileOutputStream(MAContextManager.getInstance()
          .getSchemaContext().getTableDescriptionFile());
      properties.store(out, "morrigan complement tables");
      out.close();
      propertiesLoaded.putAll(properties);
    } catch (FileNotFoundException fnfe) {
      fnfe.printStackTrace();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  private class ExtensionFileFilter extends FileFilter {

    private List<String> extensions = new ArrayList<String>();

    private String description;

    @Override
    public boolean accept(File f) {
      boolean extensionOk = false;
      for (Iterator<String> iter = extensions.iterator(); iter.hasNext()
          || extensionOk;) {
        extensionOk = f.getName().endsWith(iter.next());
      }
      return (f.canRead() && f.canWrite() && extensionOk);
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public void addExtension(String string) {
      extensions.add(string);
    }

    @Override
    public String getDescription() {
      return description;
    }

  }
}
