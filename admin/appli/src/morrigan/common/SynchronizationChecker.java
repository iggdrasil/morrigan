package morrigan.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import morrigan.FileManager;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.util.MADialogue;
import morrigan.admin.util.MADialogue.MESSAGE;
import morrigan.common.launching.ActionLauncher;
import morrigan.log.MLog;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * @author Armel
 */
public class SynchronizationChecker extends Thread {

  /** Menu file */
  private String menuFile;

  /** Map of {@link Table} */
  private Map<String, Table> tableMap;

  /**
   * Constructor.
   * 
   * @param menuFile the menu file
   * @param tableMap a map of [table name, {@link Table}]
   */
  public SynchronizationChecker(String menuFile, Map<String, Table> tableMap) {
    this.menuFile = menuFile;
    this.tableMap = tableMap;
  }

  /**
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    super.run();
    try {
      checkSynchro();
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Check if the menu file is synchronized with the Dia file.
   * 
   * @throws Exception
   */
  private void checkSynchro() throws Exception {
    InputStream isTables = null;
    Document doc = null;

    try {
      MLog.infos("Ouverture du fichier de menu " + menuFile);
      isTables = new FileManager().openFileToStream(menuFile);

      if (isTables != null) {
        MLog.infos("Loading menu from JAR");
      } else {
        MLog.infos("Loading menu from FILE");
        isTables = new FileInputStream(menuFile);
      }

      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(isTables);
      isTables.close();
      final Element eMenu = doc.getRootElement().getChild("menu");

      Map<String, MenuItem> menu = new HashMap<String, MenuItem>();

      // Liste des entrees-menu et sous-menus
      for (Object oElm : eMenu.getChildren()) {
        feedItem((Element) oElm, menu);
      }

      ActionLauncher.chargerFichierDia(false);

      Set<String> tables = new HashSet<String>();
      for (String key : tableMap.keySet()) {
        tables.add(tableMap.get(key).getName());
      }

      // Custom windows
      for (MEASpecificWindow specificWindow : MEASpecificWindow.values()) {
        // Some windows must not be listed
        if (!specificWindow.equals(MEASpecificWindow.MENU)
            && !specificWindow.equals(MEASpecificWindow.LOGIN)
            && !specificWindow.equals(MEASpecificWindow.CREATION)
            && !specificWindow.equals(MEASpecificWindow.LOG)) {
          tables.add(specificWindow.getSysName().toLowerCase());
        }
      }

      boolean everythingIsAllwright = true;

      for (String key : menu.keySet()) {
        MenuItem entreeItem = menu.get(key);
        if (!tables.remove(entreeItem.getSysName())) {
          MLog.error(SynchronizationChecker.class.getName(), "checkSynchro",
              entreeItem + " is unknown in the dia file !");
          everythingIsAllwright = false;
        }
      }

      if (tables.size() > 0) {
        MLog.error(SynchronizationChecker.class.getName(), "checkSynchro",
            tables.size() + " are not in the menu yet.");
        MLog.error(SynchronizationChecker.class.getName(), "checkSynchro",
            tables.toString());
        everythingIsAllwright = false;
      }

      if (everythingIsAllwright) {
        MLog.infos("Synchronization check : OK");
      } else {
        MADialogue.showMessage(null, MESSAGE.ATTENTION,
            "The menu file and dia file are not synchronized !");
        MLog.error("SynchronizationChecker", "checkSynchro",
            "The menu file and the dia file must be synchronized !\n"
                + "Please check the log and fix them.");
      }

    } catch (FileNotFoundException fnfe) {
      MLog.debug("SynchronizationChecker", "checkSynchro ",
          "Pas de fichier de donnees tables.");
      throw fnfe;
    } catch (IOException ioe) {
      MLog.error("SynchronizationChecker", "checkSynchro ",
          "Erreur de lecture du fichier des donnees tables", ioe);
      throw ioe;
    } catch (Exception e) {
      MLog.error("SynchronizationChecker", "checkSynchro ",
          "Erreur d'utilisation du fichier des donnees tables", e);
      throw e;
    } finally {
      if (isTables != null) {
        isTables.close();
      }
    }
  }

  private void feedItem(Element eElmMenu, Map<String, MenuItem> item) {

    final String sTypeElm = eElmMenu.getName();
    if (sTypeElm.equals("sous-menu")) {
      feedSubItem(eElmMenu, item);
    } else if (sTypeElm.equals("entree-menu")) {
      feedMenuItem(eElmMenu, item);
    }
  }

  private void feedSubItem(Element eSsMenu, Map<String, MenuItem> item) {
    for (Object oElm : eSsMenu.getChildren()) {
      feedItem((Element) oElm, item);
    }
  }

  private void feedMenuItem(Element eEntreeMenu, Map<String, MenuItem> items) {
    final String lib = eEntreeMenu.getAttributeValue("libelle");
    final String nomSys = eEntreeMenu.getAttributeValue("nomsys");
    final String action = eEntreeMenu.getAttributeValue("action");
    if (action == null && lib != null && nomSys != null) {
      items.put(nomSys, new MenuItem(lib, nomSys));
    }
  }

  /**
   * Local class
   * 
   * @author Armel
   */
  private static class MenuItem {

    /** Label */
    private final String label;

    /** System name */
    private final String sysName;

    /**
     * @param label
     * @param sysName
     */
    public MenuItem(String label, String sysName) {
      this.label = label;
      this.sysName = sysName;
    }

    /**
     * @return the label
     */
    public String getLabel() {
      return this.label;
    }

    /**
     * @return the sysName
     */
    public String getSysName() {
      return this.sysName;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return "[MenuItem : table '" + sysName + "' with label '" + label + "']";
    }
  }

}
