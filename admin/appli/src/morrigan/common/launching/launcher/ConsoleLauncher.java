package morrigan.common.launching.launcher;

import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.initializer.InitException;
import morrigan.log.MLog;

/**
 * @author Armel
 */
public class ConsoleLauncher extends AbstractLauncher {

  private Initializer initializer;

  private String currentStepName;

  private int stepNumber;

  /**
   * Default constructor.
   */
  public ConsoleLauncher() {}

  /** {@inheritDoc} */
  @Override
  protected void setInitializer(Initializer initializer) {
    this.initializer = initializer;
    MLog.infos("Initialization of the launcher");
  }

  /** {@inheritDoc} */
  @Override
  protected void start() {
    MLog.infos("Starting - " + initializer.getRemainingSteps() + " steps");
  }

  /** {@inheritDoc} */
  @Override
  protected void preStep() {
    currentStepName = initializer.getNextStepName();
    stepNumber = initializer.getRemainingSteps();
    MLog.infos("Launching step (" + stepNumber + ") '" + currentStepName
        + "'...");
  }

  /** {@inheritDoc} */
  @Override
  protected void postStep() {
    MLog.infos("Launching step (" + stepNumber + ") '" + currentStepName
        + "' - OK");
  }

  /** {@inheritDoc} */
  @Override
  protected void finish() throws InitException {
    MLog.infos("Launching steps - OK");
  }

  /** {@inheritDoc} */
  @Override
  protected void error(Exception exception) {
    MLog.error("ConsoleLauncher", "error",
        "An error occured while launching step", exception);
  }
}
