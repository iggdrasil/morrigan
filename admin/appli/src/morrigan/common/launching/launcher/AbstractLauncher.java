package morrigan.common.launching.launcher;

import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.initializer.InitException;

abstract class AbstractLauncher implements Launcher {

  /**
   * Launch application initializer.
   */
  public void launchInitializer(Initializer initializer) {
    try {
      setInitializer(initializer);
      initializer.executePreparationSteps();

      start();
      while (initializer.hasMoreSteps()) {
        preStep();
        initializer.executeAndConsumeStep();
        postStep();
      }

      finish();
    } catch (Exception e) {
      error(e);
    }
  }

  abstract void setInitializer(Initializer initializer);

  abstract void start();

  abstract void preStep();

  abstract void postStep();

  abstract void finish() throws InitException;

  abstract void error(Exception exception);
}
