package morrigan.common.launching.launcher;

import morrigan.common.launching.application.Initializer;

/**
 * @author Armel
 */
public interface Launcher {

  public void launchInitializer(Initializer initializer);

}
