package morrigan.common.launching.launcher;

import javax.swing.SwingUtilities;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.specif.MSplashScreen;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.initializer.InitException;

/**
 * @author Armel
 */
public class GraphicalLauncher extends AbstractLauncher {

  /** Splash screen */
  private static MSplashScreen splashScreen = null;

  private Initializer initializer;

  /**
   * Default constructor.
   */
  public GraphicalLauncher() {}

  /** {@inheritDoc} */
  @Override
  protected void setInitializer(Initializer initializer) {
    this.initializer = initializer;
  }

  /** {@inheritDoc} */
  @Override
  protected void start() {
    splashScreen = new MSplashScreen(MAContextManager.getInstance()
        .getGraphicContext().getSplashScreenImage(), initializer
        .getRemainingSteps());
    showSplashScreen();
  }

  /** {@inheritDoc} */
  @Override
  protected void preStep() {
    String nextStepName = initializer.getNextStepName();
    if (splashScreen != null) {
      splashScreen.notifyStep(initializer.getLaunchedSteps(), nextStepName);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void postStep() {}

  /** {@inheritDoc} */
  @Override
  protected void finish() throws InitException {
    // On apprecie le travail :)
    try {
      Thread.sleep(1500);
    } catch (InterruptedException e) {
      throw new InitException("Error while admiring splash screen", e);
    }
    hideSplash();
  }

  /** {@inheritDoc} */
  @Override
  protected void error(Exception exception) {
    exception.printStackTrace();
    if(splashScreen != null) {
      splashScreen.notifierErreurEtape(exception.getMessage());
    }
  }

  /**
   * Show the splash screen.
   */
  private void showSplashScreen() {
    SwingUtilities.invokeLater(new Runnable() {

      public void run() {
        splashScreen.setVisible(true);
      }
    });
  }

  /**
   * Hides the spash screen.
   */
  private void hideSplash() {
    SwingUtilities.invokeLater(new Runnable() {

      public void run() {
        splashScreen.setVisible(false);
        splashScreen.dispose();
        splashScreen = null;
      }
    });
  }
}
