/*
 * ActionLauncher.java
 * 28 juin 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching;

import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.xmlreader.MAGzipUtils;
import morrigan.admin.xmlreader.XmlDiaReader;
import morrigan.admin.xmlreader.handler.AssociationHandler;
import morrigan.admin.xmlreader.handler.LightTableHandler;
import morrigan.admin.xmlreader.handler.TableHandler;
import morrigan.common.SynchronizationChecker;
import morrigan.log.MLog;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * @author Armel
 */
public final class ActionLauncher {

  /**
   * Hidden constructor
   */
  private ActionLauncher() {}

  public static void chargerFichierDescriptionTable() {
    Properties propertiesDescTable = new Properties();
    try {
      FileInputStream in = new FileInputStream(MAContextManager.getInstance()
          .getSchemaContext().getTableDescriptionFile());
      propertiesDescTable.load(in);
      in.close();
    } catch (FileNotFoundException fnfe) {
      // fnfe.printStackTrace();
      // Pas grave !!
      MLog.infos("Pas de fichier de description de table.");
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

    MAContextManager.getInstance().getDbContext().setTableDescProperties(
        propertiesDescTable);
  }

  /**
   * Load the dia file.
   * 
   * @param usePropertiesLabelFile <code>true</code> if the labels in the
   *          properties file overrides the dia labels; <code>false</code>
   *          otherwise.
   * @throws FileNotFoundException if the file is not found
   * @throws IOException if the file reading fails
   * @throws JDOMException if the parsing fails
   */
  public static synchronized void chargerFichierDia(
      boolean usePropertiesLabelFile) throws FileNotFoundException,
      IOException, JDOMException {

    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    if (dbContext.getTables() != null) {
      MLog.infos("Dia file already loaded.");
      return;
    }

    MAGzipUtils.extractTo(MAContextManager.getInstance().getSchemaContext()
        .getDiaFile(), MAContextManager.getInstance().getSchemaContext()
        .getXmlDiaFile());

    XmlDiaReader xmlDiaReader = new XmlDiaReader(usePropertiesLabelFile);
    xmlDiaReader.parse();

    LightTableHandler lightTableHandler = xmlDiaReader.getLightTableHandler();
    dbContext.setLightTables(lightTableHandler.getTables());

    TableHandler tableHandler = xmlDiaReader.getTableHandler();
    dbContext.setTables(tableHandler.getTables());
    
    AssociationHandler associationHandler = xmlDiaReader.getAssociationHandler();
    dbContext.setForeignToPrimaryKeyLinks(associationHandler.getForeignToPrimaryKeysLinks());
    dbContext.setPrimaryToForeignKeysLinks(associationHandler.getPrimaryToForeignKeysLinks());
  }

  /**
   * Prepare the window configuration file.
   */
  public static void prepareWindowPrefFile() {
    try {
      String windowPrefFile = System.getProperty("user.home") + File.separator;
      windowPrefFile += System.getProperty("os.name").equals("Linux") ? ".morrigan"
          : "Morrigan";

      File fDir = new File(windowPrefFile);
      if (!fDir.exists()) {
        fDir.mkdir();
      }

      windowPrefFile += File.separator
          + MAContextManager.getInstance().getPreferencesFileName();

      MAContextManager.getInstance().setPreferencesFile(windowPrefFile);
    } catch (Exception e) {
      MLog.error("MAdminAppli", "preparerSauvegarde",
          "Erreur d'ouverture du fichier de conf ", e);
    }
  }

  /**
   * Load windows parameters.
   * 
   * @return <code>true</code> if ok; <code>false</code> otherwise
   */
  public static boolean chargerParamFenetres() {
    FileInputStream fio = null;
    Document doc = null;
    try {
      String windowPrefFile = MAContextManager.getInstance()
          .getPreferencesFile();

      MLog.infos("Ouverture du fichier de conf " + windowPrefFile);
      fio = new FileInputStream(windowPrefFile);
      final SAXBuilder builder = new SAXBuilder();
      doc = builder.build(fio);
      fio.close();
      Element eRoot = doc.getRootElement();

      // final int iMargeMini = 10;
      // final int iCoteMini = 100;
      // Rectangle rEcran = mfa.getGraphicsConfiguration().getBounds();

      final List<?> lFenetres = eRoot.getChild("fenetres").getChildren(
          "fenetre");

      for (int i = lFenetres.size() - 1; i >= 0; i--) {
        Element eFenetre = (Element) lFenetres.get(i);

        // Coordonnees et dimensions
        Element ePosition = eFenetre.getChild("position");
        // Element eDimension = eFenetre.getChild("dimension");
        int x = Integer.parseInt(ePosition.getChildText("x")); // 0
        int y = Integer.parseInt(ePosition.getChildText("y")); // 0
        // int iHauteur =
        // Integer.parseInt(eDimension.getChildText("hauteur")); // 100
        // int iLargeur =
        // Integer.parseInt(eDimension.getChildText("largeur")); // 100

        // Verification des dimensions pour que la fenetre reste visible
        // if (iHauteur < iMargeMini)
        // iHauteur = iCoteMini;
        // if (iLargeur < iMargeMini)
        // iLargeur = iCoteMini;
        // if (x + iLargeur < iMargeMini || x > rEcran.width -
        // iMargeMini)
        // x = 0;
        // if (y + iLongueur < iMargeMini || y > rEcran.height -
        // iMargeMini)
        // y = 0;

        // Creation
        MASysWindow fenetre = MAContextManager.getInstance().getWindowContext()
            .getSysWindowByWindowName(eFenetre.getAttributeValue("nom"));
        fenetre.savePosition(new Point(x, y));

        // Visibilite
        if (eFenetre.getChildText("ouvert").equals("true")
            && !fenetre.getSysName().equals(MEASpecificWindow.MENU.toString())) {
          MAWindowManager.createWindow(fenetre); // , x, y, iLargeur,
          // iHauteur);
          MAWindowManager.openWindow(fenetre);
        }
      }

    } catch (FileNotFoundException fnfe) {
      MLog.debug("MAdminAppli", "chargerParamFenetres ",
          "Pas de fichier de conf.");
      return false;
    } catch (IOException e) {
      MLog.error("MAdminAppli", "chargerParamFenetres ",
          "Erreur de lecture du fichier de conf", e);
      return false;
    } catch (Exception e) {
      MLog.error("MAdminAppli", "chargerParamFenetres ",
          "Erreur d'utilisation de la conf", e);
      return false;
    }

    return true;
  }

  public static void chargerPreferences() {
    FileInputStream fio = null;
    Document doc = null;
    try {
      String windowPrefFile = MAContextManager.getInstance()
          .getPreferencesFile();

      MLog.infos("Ouverture du fichier de preferences " + windowPrefFile);
      fio = new FileInputStream(windowPrefFile);
      final SAXBuilder builder = new SAXBuilder();
      doc = builder.build(fio);
      fio.close();
      Element eRoot = doc.getRootElement();

      final Element preferences = eRoot.getChild("preferences");
      String dateFormat = preferences.getChildText("dateFormat");
      MAContextManager.getInstance().getPreferenceContext().setDateFormat(
          dateFormat);
    } catch (FileNotFoundException fnfe) {
      MLog.debug("MAdminAppli", "chargerPreferences ",
          "Pas de fichier de preferences.");
    } catch (IOException e) {
      MLog.error("MAdminAppli", "chargerPreferences ",
          "Erreur de lecture du fichier de preferences", e);
    } catch (Exception e) {
      MLog.error("MAdminAppli", "chargerPreferences ",
          "Erreur d'utilisation des preferences", e);
    }
  }

  public static void checkSynchroFiles() {
    SynchronizationChecker synchronizationChecker = new SynchronizationChecker(
        MAContextManager.getInstance().getGraphicContext().getMenuFile(),
        MAContextManager.getInstance().getDbContext().getTables());
    synchronizationChecker.run();
  }

  /** Action */
  abstract static class Action {

    /**
     * Execute an action
     * 
     * @throws Exception if the execution fails
     */
    public abstract void execute() throws Exception;
  }

}
