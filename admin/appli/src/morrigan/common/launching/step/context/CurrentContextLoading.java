package morrigan.common.launching.step.context;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.loader.model.ContextVisitor;
import morrigan.admin.context.loader.model.GlobalContext;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * Need the identification step to be done.
 * 
 * @author Armel Lagadic
 */
public class CurrentContextLoading implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    MAContextManager instance = MAContextManager.getInstance();
    String currentContextName = instance.getCurrentContextName();
    GlobalContext globalContext = instance.getGlobalContext();

    globalContext.accept(new ContextVisitor(currentContextName));
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Context loading";
  }

}
