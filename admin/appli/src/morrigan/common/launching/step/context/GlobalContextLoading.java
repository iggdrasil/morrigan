package morrigan.common.launching.step.context;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import morrigan.admin.context.loader.ContextLoader;
import morrigan.admin.context.loader.ContextLoaderFactory;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel Lagadic
 */
public class GlobalContextLoading implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    boolean jnlp = Boolean.getBoolean("jnlp");
    jnlp = true;
    ContextLoader contextLoader = new ContextLoaderFactory()
        .createContextLoader(jnlp);
    try {
      contextLoader.loadContext();
    } catch (FileNotFoundException e) {
      throw new InitException("Context file not found", e);
    } catch (ParserConfigurationException e) {
      throw new InitException("Context file could not be parsed", e);
    } catch (SAXException e) {
      throw new InitException("Context file could not be parsed", e);
    } catch (IOException e) {
      throw new InitException("Context file could not be read", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Context loading";
  }

}
