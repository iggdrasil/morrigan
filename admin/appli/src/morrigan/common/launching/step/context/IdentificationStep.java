package morrigan.common.launching.step.context;

import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.context.loader.model.DatabaseType;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.exception.OpenWindowException;
import morrigan.admin.specif.login.LoginFen;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

public class IdentificationStep implements LaunchingStep {

  public String getStepName() {
    return "Identification";
  }

  public void consume() throws InitException {
    final MASysWindow fenetre = MAContextManager.getInstance()
        .getWindowContext().getSysWindowByWindowName(
            MEASpecificWindow.LOGIN.getSysName());
    try {
      // Semaphore usage : block the next step by setting a semaphore which is
      // free when identification is OK.
      Semaphore semaphore = new Semaphore(1);
      // Acquire
      semaphore.acquire();
      LoginFen loginFen = (LoginFen) MAWindowManager.createWindow(fenetre);
      loginFen.init();
      loginFen.setLock(semaphore);
      MAWindowManager.openWindow(fenetre);
      // The window has locked the semaphore.
      // Now the current thread must wait for the window to free it.
      semaphore.acquire();
      Properties dbProperties = loginFen.getDbProperties();
      String choosenContextName = loginFen.getChoosenContextName();
      MAContextManager.getInstance().setCurrentContextName(choosenContextName);
      launchWindowKiller(fenetre, 2000);

      MADbContext dbContext = MAContextManager.getInstance().getDbContext();
      dbContext.setUserName(dbProperties.getProperty("user"));
      dbContext.setPassword(dbProperties.getProperty("password"));
      dbContext.setIp(dbProperties.getProperty("host"));
      dbContext.setPort(dbProperties.getProperty("port"));
      dbContext.setDatabaseName(dbProperties.getProperty("db"));
      DatabaseType databaseType = MAContextManager.getInstance()
          .getGlobalContext().getDatabaseTypes().get(
              dbProperties.getProperty("databasetype"));
      dbContext.setType(databaseType.getDatabase());
      dbContext.setDriverClassName(databaseType.getDriverClassName());
      

    } catch (CreateWindowException e) {
      throw new InitException("The identification window could not be created",
          e);
    } catch (OpenWindowException e) {
      throw new InitException("The identification window could not be opened",
          e);
    } catch (InterruptedException e) {
      throw new InitException("The lock while identification could not wait", e);
    }
  }

  private void launchWindowKiller(final MASysWindow fenetre, int delay) {
    TimerTask timerTask = new TimerTask() {
      @Override
      public void run() {
        MAWindowManager.closeWindow(fenetre);
      }
    };
    new Timer().schedule(timerTask, delay);
  }

}
