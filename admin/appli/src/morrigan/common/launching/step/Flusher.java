/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Flusher.java - CREATION : 2008/11/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.common.launching.step;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.launching.initializer.InitException;

public class Flusher implements LaunchingStep {

  public void consume() throws InitException {
    MASysWindow menu = MAContextManager.getInstance().getWindowContext().getSysWindowByWindowName(
            MEASpecificWindow.MENU.getSysName());
    if (menu.getWindow() != null) {
      menu.getWindow().setVisible(false);
    }
    menu.setWindow(null);
    MAWindowManager.closeAllWindows();
    MAWindowManager.closeAllWindows();
  }

  public String getStepName() {
    return "Flusher";
  }

}
