package morrigan.common.launching.step.database;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.DBManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.log.MLog;

/**
 * @author Armel
 */
public class DataBaseConnectionStep implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    String dbUrl = dbContext.getDatabaseUrl();
    String dbUser = dbContext.getUserName();
    String dbPassword = dbContext.getPassword();
    String driverClazz = dbContext.getDriverClassName();

    MLog.debug("MAdminAppli", "lancerEtape", "Demande de connexion a la base "
        + dbUrl + " | user=" + dbUser + " | pwd=" + dbPassword.replaceAll(".", "*"));

    try {
      DBManager.openConnection(dbUrl, dbUser, dbPassword, driverClazz);
    } catch (Exception e) {
      throw new InitException("Connection with database fails", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Database connection";
  }

}
