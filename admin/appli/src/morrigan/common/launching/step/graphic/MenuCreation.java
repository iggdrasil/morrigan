/*
 * MenuCreation.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.graphic;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.util.MAMenuGenerator;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class MenuCreation implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      MAMenuGenerator.loadMenuFromFile(MAContextManager.getInstance()
          .getGraphicContext().getMenuFile());
    } catch (Exception e) {
      new InitException("Menu creation fails", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Menu loading";
  }

}
