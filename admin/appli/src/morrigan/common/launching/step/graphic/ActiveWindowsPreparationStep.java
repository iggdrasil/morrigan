/*
 * WindowPreparation.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.graphic;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.exception.WindowException;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.log.MLog;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * @author Armel Lagadic
 */
public class ActiveWindowsPreparationStep implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      // On CREE la fenetre de menu
      // Le positionnement peut arriver ensuite, donc pas
      // d'affichage premature
      MAWindowManager.createWindow(MAContextManager.getInstance()
          .getWindowContext().getSysWindowByWindowName(
              MEASpecificWindow.MENU.getSysName()));

      if (!chargerParamFenetres()) {
        // Fenetres a ouvrir en cas d'absence de fichier de
        // conf (en plus du MENU)
        MAWindowManager.openWindow(MAContextManager.getInstance()
            .getWindowContext().getSysWindowByWindowName(
                MEASpecificWindow.LOG.getSysName()));
      }
    } catch (WindowException e) {
      throw new InitException("Windows initiliazation fails", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Windows initialization";
  }

  /**
   * Load windows parameters.
   * 
   * @return <code>true</code> if ok; <code>false</code> otherwise
   */
  private boolean chargerParamFenetres() {
    FileInputStream fio = null;
    Document doc = null;
    try {
      String windowPrefFile = MAContextManager.getInstance()
          .getPreferencesFile();

      MLog.infos("Ouverture du fichier de conf " + windowPrefFile);
      fio = new FileInputStream(windowPrefFile);
      final SAXBuilder builder = new SAXBuilder();
      doc = builder.build(fio);
      fio.close();
      Element eRoot = doc.getRootElement();

      final List<?> lFenetres = eRoot.getChild("fenetres").getChildren(
          "fenetre");

      for (int i = lFenetres.size() - 1; i >= 0; i--) {
        Element eFenetre = (Element) lFenetres.get(i);

        // Coordonnees et dimensions
        Element ePosition = eFenetre.getChild("position");
        // Element eDimension = eFenetre.getChild("dimension");
        int x = Integer.parseInt(ePosition.getChildText("x")); // 0
        int y = Integer.parseInt(ePosition.getChildText("y")); // 0

        // Creation
        MASysWindow fenetre = MAContextManager.getInstance().getWindowContext()
            .getSysWindowByWindowName(eFenetre.getAttributeValue("nom"));
        fenetre.savePosition(new Point(x, y));

        // Visibilite
        if (eFenetre.getChildText("ouvert").equals("true")
            && !fenetre.getSysName().equals(MEASpecificWindow.MENU.toString())) {
          MAWindowManager.createWindow(fenetre);
          MAWindowManager.openWindow(fenetre);
        }
      }

    } catch (FileNotFoundException fnfe) {
      MLog.debug("MAdminAppli", "chargerParamFenetres ",
          "Pas de fichier de conf.");
      return false;
    } catch (IOException e) {
      MLog.error("MAdminAppli", "chargerParamFenetres ",
          "Erreur de lecture du fichier de conf", e);
      return false;
    } catch (Exception e) {
      MLog.error("MAdminAppli", "chargerParamFenetres ",
          "Erreur d'utilisation de la conf", e);
      return false;
    }

    return true;
  }
}
