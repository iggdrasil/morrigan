package morrigan.common.launching.step.graphic;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.synth.SynthLookAndFeel;

import org.apache.commons.lang.Validate;

import morrigan.FileManager;
import morrigan.admin.context.MAContextManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.graphique.MModele;

/**
 * @author Armel
 */
public class GraphicalConfLaunching implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    String confFile = MAContextManager.getInstance().getGraphicContext()
        .getConfFile();
    try {
      Validate.notNull(confFile, "The UI file cannot be null");
      if(confFile.endsWith("xml")) {
        initLookAndFeel(confFile);
      } else {
        MModele.chargerConfiguration(confFile);
      }
    } catch (Exception e) {
      throw new InitException("This look and feel cannot be set", e);
    }
  }

  private void initLookAndFeel(String relativePathFile) throws FileNotFoundException, ParseException,
      UnsupportedLookAndFeelException {
    SynthLookAndFeel laf = new SynthLookAndFeel();
    InputStream inputStream = new FileManager()
        .openFileToStream(relativePathFile);
    laf.load(inputStream, GraphicalConfLaunching.class);
    UIManager.setLookAndFeel(laf);
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Graphical configuration loading";
  }
}
