package morrigan.common.launching.step.graphic.painter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.plaf.synth.SynthConstants;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthPainter;

import morrigan.admin.ui.field.MIAField;
import morrigan.admin.ui.field.MIAField.AFFICHAGE;
import morrigan.log.MLog;

import org.apache.commons.lang.Validate;

public class PlainPainter extends SynthPainter {

  private static final String BUTTON_INACTIVE_BORDER = "Button.inactiveBorder";
  private static final String BUTTON_BORDER = "Button.border";
  private static final String TEXT_FIELD_DISABLED_BACKGROUND = "TextField.disabledBackground";
  private static final String TEXT_FIELD_READ_BACKGROUND = "TextField.readBackground";
  private static final String TEXT_FIELD_SEARCH_BACKGROUND = "TextField.searchBackground";
  private static final String PANEL_END_BACKGROUND = "Panel.endBackground";
  private static final String MENU_OVER_BACKGROUND = "Menu.overBackground";
  private static final String MENU_BACKGROUND = "Menu.background";

  private Color buttonBorder;
  private Color inactiveButtonBorder;
  private Color toolTipBorder;
  private Color panelBg;
  private Color searchColor;
  private Color readColor;
  private Color disabledColor;
  private Color menuBg;
  private Color menuOverBg;

  /*
   * Backgrounds
   */

  @Override
  public void paintPanelBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (panelBg == null) {
      panelBg = UIManager.getColor(PANEL_END_BACKGROUND);
    }
    paintBackground(context, g, x, y, w, h, panelBg);
  }

  @Override
  public void paintButtonBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    this.paintPanelBackground(context, g, x, y - 1, w - 1, h);
  }

  @Override
  public void paintMenuItemBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    paintMenuBackground(context, g, x, y, w, h);
  }

  @Override
  public void paintMenuBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (menuBg == null) {
      menuBg = UIManager.getColor(MENU_BACKGROUND);
      menuOverBg = UIManager.getColor(MENU_OVER_BACKGROUND);
    }
    Color c;
    switch (context.getComponentState()) {
    case SynthConstants.MOUSE_OVER:
      c = menuOverBg;
      break;
    default:
      c = menuBg;
      break;
    }
    JMenuItem menu = (JMenuItem) context.getComponent();
    MLog.debug("PlainPainter", "paintMenuBackground", menu.getText());
    paintBackground(context, g, x, y, w, h, c);
  }

  @Override
  public void paintSeparatorBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (menuBg == null) {
      menuBg = UIManager.getColor(MENU_BACKGROUND);
    }
    JSeparator c = (JSeparator) context.getComponent();
    Dimension s = c.getSize();
    if (c.getOrientation() == JSeparator.VERTICAL) {
      g.setColor(menuBg);
      g.drawLine(1, 0, 1, s.height);
    } else // HORIZONTAL
    {
      g.setColor(menuBg);
      g.drawLine(0, 1, s.width, 1);
    }
  }

  @Override
  public void paintSeparatorForeground(SynthContext context, Graphics g, int x,
      int y, int w, int h, int orientation) {
    JSeparator c = (JSeparator) context.getComponent();
    Dimension s = c.getSize();

    if (c.getOrientation() == JSeparator.VERTICAL) {
      g.setColor(c.getForeground());
      g.drawLine(0, 0, 0, s.height);
    } else {
      // HORIZONTAL
      g.setColor(c.getForeground());
      g.drawLine(0, 0, s.width, 0);
    }
  }

  @Override
  public void paintTextFieldBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (searchColor == null) {
      searchColor = UIManager.getColor(TEXT_FIELD_SEARCH_BACKGROUND);
      readColor = UIManager.getColor(TEXT_FIELD_READ_BACKGROUND);
      disabledColor = UIManager.getColor(TEXT_FIELD_DISABLED_BACKGROUND);
    }

    MIAField field = castIntoField(context.getComponent());
    Color c;
    
    if (field.isBddEnabled()) {
      c = (field.getMode() == AFFICHAGE.RECHERCHE) ? searchColor : readColor;
    } else {
      c = disabledColor;
    }

    paintBackground(context, g, x, y, w, h, c);
  }

  @Override
  public void paintFormattedTextFieldBackground(SynthContext context,
      Graphics g, int x, int y, int w, int h) {
    this.paintTextFieldBackground(context, g, x, y, w, h);
  }

  @Override
  public void paintPasswordFieldBackground(SynthContext context, Graphics g,
      int x, int y, int w, int h) {
    this.paintTextFieldBackground(context, g, x, y, w, h);
  }

  @Override
  public void paintTextAreaBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    this.paintBackground(context, g, x, y, w, h, Color.RED);
  }

  @Override
  public void paintTextPaneBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    this.paintTextFieldBackground(context, g, x, y, w, h);
  }

  /*
   * Borders
   */

  @Override
  public void paintToolTipBorder(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (toolTipBorder == null) {
      toolTipBorder = UIManager.getColor(TEXT_FIELD_SEARCH_BACKGROUND);
    }
    paintBorder(context, g, x, y, w - 1, h - 1, toolTipBorder);
  }

  @Override
  public void paintToggleButtonBorder(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    if (buttonBorder == null) {
      buttonBorder = UIManager.getColor(BUTTON_BORDER);
      inactiveButtonBorder = UIManager.getColor(BUTTON_INACTIVE_BORDER);
    }

    Color border = (context.getComponent().isEnabled()) ? buttonBorder
        : inactiveButtonBorder;

    paintBorder(context, g, x, y, w - 1, h - 1, border);
  }

  @Override
  public void paintButtonBorder(SynthContext context, Graphics g, int x, int y,
      int w, int h) {
    if (buttonBorder == null) {
      buttonBorder = UIManager.getColor(BUTTON_BORDER);
      inactiveButtonBorder = UIManager.getColor(BUTTON_INACTIVE_BORDER);
    }

    Color border = (context.getComponent().isEnabled()) ? buttonBorder
        : inactiveButtonBorder;

    paintBorder(context, g, x, y, w - 1, h - 1, border);
  }

  @Override
  public void paintMenuItemBorder(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    System.out.println(context.getComponent());
    paintBorder(context, g, x, y, w, h, Color.RED);
  }

  @Override
  public void paintMenuBorder(SynthContext context, Graphics g, int x, int y,
      int w, int h) {
    System.out.println(context.getComponent());
    paintBorder(context, g, x, y, w, h, Color.RED);
  }

  /*
   * 
   * Helpers
   */

  /**
   * 
   * @param component
   * @return
   */
  private MIAField castIntoField(JComponent component) {
    Validate.isTrue(MIAField.class.isInstance(component),
        "The JComponent to paint must be a MIAField (not a '"
            + component.getClass().getName());
    return (MIAField) component;
  }

  /**
   * Paints the background of an component.
   * 
   * @param context SynthContext identifying the <code>JComponent</code> and
   *        <code>Region</code> to paint to
   * @param g <code>Graphics</code> to paint to
   * @param x X coordinate of the area to paint to
   * @param y Y coordinate of the area to paint to
   * @param w Width of the area to paint to
   * @param h Height of the area to paint to
   * @param color the color to use
   */
  private void paintBackground(SynthContext context, Graphics g, int x, int y,
      int w, int h, Color color) {
    Graphics2D g2 = (Graphics2D) g;
    g2.setPaint(color);
    g2.fillRect(x, y, w, h);
    g2.setPaint(null);
  }

  /**
   * Paints the border of an component.
   * 
   * @param context SynthContext identifying the <code>JComponent</code> and
   *        <code>Region</code> to paint to
   * @param g <code>Graphics</code> to paint to
   * @param x X coordinate of the area to paint to
   * @param y Y coordinate of the area to paint to
   * @param w Width of the area to paint to
   * @param h Height of the area to paint to
   * @param color the color to use
   */
  private void paintBorder(SynthContext context, Graphics g, int x, int y,
      int w, int h, Color color) {
    Graphics2D g2 = (Graphics2D) g;
    g2.setPaint(color);
    g2.drawRect(x, y, w, h);
    g2.setPaint(null);
  }
}