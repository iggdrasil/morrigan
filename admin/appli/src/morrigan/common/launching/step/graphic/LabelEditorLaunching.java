/*
 * ApplicationLaunching.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.graphic;

import morrigan.common.launching.WindowActionLauncher;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.labeleditor.SimpleLabelEditor;

/**
 * @author Armel
 */
public class LabelEditorLaunching implements LaunchingStep,
    WindowActionLauncher {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    SimpleLabelEditor editor = new SimpleLabelEditor();
    editor.setVisible(true);
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Application launching";
  }

}
