/*
 * ApplicationLaunching.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.graphic;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.exception.WindowException;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.common.launching.WindowActionLauncher;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class AdministrationLaunching implements LaunchingStep,
    WindowActionLauncher {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      MAWindowManager.openWindow(MAContextManager.getInstance()
          .getWindowContext().getSysWindowByWindowName(
              MEASpecificWindow.MENU.getSysName()));
    } catch (WindowException e) {
      throw new InitException("Application launching fails", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Application launching";
  }

}
