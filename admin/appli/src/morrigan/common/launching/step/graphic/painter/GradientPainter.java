package morrigan.common.launching.step.graphic.painter;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.CubicCurve2D;

import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthPainter;

import morrigan.admin.ui.field.MIAField;
import morrigan.admin.ui.field.MIAField.AFFICHAGE;

import org.apache.commons.lang.Validate;

public class GradientPainter extends SynthPainter {

  public GradientPainter() {}
  
  public void paintPanelBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    Color start = UIManager.getColor("Panel.startBackground");
    Color end = UIManager.getColor("Panel.endBackground");
    Graphics2D g2 = (Graphics2D) g;
    GradientPaint grPaint = new GradientPaint((float) x, (float) y, start,
        (float) w, (float) h, end);
    g2.setPaint(grPaint);
    g2.fillRect(x, y, w, h);
    g2.setPaint(null);
    g2.setColor(new Color(255, 255, 255, 80));
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    CubicCurve2D.Double arc2d = new CubicCurve2D.Double(0, h / 4, w / 3,
        h / 10, 0.66 * w, 1.5 * h, w, h / 8);
    g2.draw(arc2d);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_OFF);
  }
  
  @Override
  public void paintTextPaneBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    super.paintTextPaneBackground(context, g, x, y, w, h);
  }

  public void paintTextFieldBackground(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    JComponent component = context.getComponent();
    Validate.isTrue(MIAField.class.isInstance(component),
        "The JComponent to paint must be a MIAField (not a '"
            + component.getClass().getName());
    MIAField field = (MIAField) component;
    
    Color startSearch = UIManager.getColor("TextField.startSearchBackground");
    Color endSearch = UIManager.getColor("TextField.endSearchBackground");
    Color startRead = UIManager.getColor("TextField.startReadBackground");
    Color endRead = UIManager.getColor("TextField.endReadBackground");
    
    Color first, second;
    if (field.getMode() == AFFICHAGE.RECHERCHE) {
      first = startSearch;
      second = endSearch;
    } else {
      first = startRead;
      second = endRead;
    }

    // For simplicity this always recreates the GradientPaint. In a
    // real app you should cache this to avoid garbage.
    Graphics2D g2 = (Graphics2D) g;
    GradientPaint paint = new GradientPaint((float) x, (float) y, first,
        (float) (x + w), (float) (y + h), second);
    g2.setPaint(paint);
    g2.fillRect(x, y, w, h);
    g2.setPaint(null);
  }

  @Override
  public void paintFormattedTextFieldBackground(SynthContext context,
      Graphics g, int x, int y, int w, int h) {
    this.paintTextFieldBackground(context, g, x, y, w, h);
  }
  
  @Override
  public void paintToolTipBorder(SynthContext context, Graphics g, int x,
      int y, int w, int h) {
    super.paintToolTipBorder(context, g, x, y, w, h);
  }
}
