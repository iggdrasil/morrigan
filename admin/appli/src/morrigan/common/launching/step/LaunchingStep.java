package morrigan.common.launching.step;

import morrigan.common.launching.initializer.InitException;

/**
 * @author Armel
 */
public interface LaunchingStep {

  /**
   * Execute de step.
   * 
   * @throws InitException TODO
   */
  public void consume() throws InitException;

  /**
   * @return step name
   */
  public String getStepName();
}
