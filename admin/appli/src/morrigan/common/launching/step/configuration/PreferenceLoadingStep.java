/*
 * WindowPreparation.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import morrigan.admin.context.MAContextManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.log.MLog;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * @author Armel
 */
public class PreferenceLoadingStep implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    FileInputStream fio = null;
    Document doc = null;
    try {
      String windowPrefFile = MAContextManager.getInstance()
          .getPreferencesFile();

      MLog.infos("Ouverture du fichier de preferences " + windowPrefFile);
      fio = new FileInputStream(windowPrefFile);
      final SAXBuilder builder = new SAXBuilder();
      doc = builder.build(fio);
      fio.close();
      Element eRoot = doc.getRootElement();

      final Element preferences = eRoot.getChild("preferences");
      String dateFormat = preferences.getChildText("dateFormat");
      MAContextManager.getInstance().getPreferenceContext().setDateFormat(
          dateFormat);
    } catch (FileNotFoundException fnfe) {
      // No soucy. This must be the first launch.
      //    throw new InitException("No preferences file found", fnfe);
    } catch (IOException e) {
      throw new InitException("Error while reading preferences file", e);
    } catch (Exception e) {
      throw new InitException("Error while applying preferences from file", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Windows initialization";
  }

}
