package morrigan.common.launching.step.configuration;

import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.log.MLog;
import morrigan.log.MLogLevel;

/**
 * @author Armel
 */
public class LogStep implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    MLog.lancer("adm-Morrigan", MLogLevel.ALL);
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Logging initialization";
  }
}
