/*
 * FilesSynchronization.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import morrigan.admin.context.MAContextManager;
import morrigan.common.SynchronizationChecker;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class FilesSynchronizationChecking implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    SynchronizationChecker synchronizationChecker = new SynchronizationChecker(
        MAContextManager.getInstance().getGraphicContext().getMenuFile(),
        MAContextManager.getInstance().getDbContext().getTables());
    synchronizationChecker.run();

  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Files synchronization checking";
  }

}
