/*
 * DiaFileLoading.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import morrigan.common.launching.ActionLauncher;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class DiaFileLoading implements LaunchingStep {

  private final boolean useComplementFile;

  public DiaFileLoading(boolean useComplementFile) {
    this.useComplementFile = useComplementFile;
  }

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      ActionLauncher.chargerFichierDia(useComplementFile);
    } catch (Exception e) {
      throw new InitException("Error while loading DIA file", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Dia file loading";
  }

}
