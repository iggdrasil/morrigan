/*
 * DescriptionsFileLoading.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import morrigan.admin.context.MAContextManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.log.MLog;

/**
 * @author Armel
 */
public class DescriptionsFileLoading implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    Properties propertiesDescTable = new Properties();
    try {
      FileInputStream in = new FileInputStream(MAContextManager.getInstance()
          .getSchemaContext().getTableDescriptionFile());
      propertiesDescTable.load(in);
      in.close();
    } catch (FileNotFoundException fnfe) {
      // fnfe.printStackTrace();
      // Pas grave !!
      MLog.infos("Pas de fichier de description de table.");
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

    MAContextManager.getInstance().getDbContext().setTableDescProperties(
        propertiesDescTable);
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Descriptions file loading";
  }

}
