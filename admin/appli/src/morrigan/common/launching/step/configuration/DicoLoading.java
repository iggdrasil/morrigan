/*
 * DicoLaunching.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import morrigan.admin.context.MAContextManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.dictionnaire.MDictionnaire;

/**
 * @author Armel
 */
public class DicoLoading implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      MDictionnaire.charger(MAContextManager.getInstance()
          .getDictionaryContext().getDicoFile());
    } catch (Exception e) {
      throw new InitException("Error while loading dictionnary", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Dictionary loading";
  }
}
