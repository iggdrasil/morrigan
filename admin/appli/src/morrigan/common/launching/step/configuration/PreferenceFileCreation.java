/*
 * PreferenceFileCreation.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.step.configuration;

import java.io.File;

import morrigan.admin.context.MAContextManager;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class PreferenceFileCreation implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    try {
      String windowPrefFile = System.getProperty("user.home") + File.separator;
      windowPrefFile += System.getProperty("os.name").equals("Linux") ? ".morrigan"
          : "Morrigan";

      File fDir = new File(windowPrefFile);
      if (!fDir.exists()) {
        fDir.mkdir();
      }

      windowPrefFile += File.separator
          + MAContextManager.getInstance().getPreferencesFileName();

      MAContextManager.getInstance().setPreferencesFile(windowPrefFile);
    } catch (Exception e) {
      throw new InitException("Erreur d'ouverture du fichier de conf", e);
    }
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Preferences file creation";
  }

}
