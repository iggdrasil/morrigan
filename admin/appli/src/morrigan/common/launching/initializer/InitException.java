/*
 * InitException.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.initializer;

/**
 * Initialisation exception.
 * 
 * @author Armel
 */
public class InitException extends Exception {

  /**
   * Default constructor.
   */
  public InitException(String message) {
    super(message);
  }

  /**
   * Constructor with message and
   * 
   * @param message
   * @param nestedException
   */
  public InitException(String message, Exception nestedException) {
    super(message, nestedException);
  }
}
