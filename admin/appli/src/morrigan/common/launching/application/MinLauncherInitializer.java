package morrigan.common.launching.application;

import java.util.List;

import morrigan.common.launching.step.LaunchingStep;
import morrigan.common.launching.step.configuration.DescriptionsFileLoading;
import morrigan.common.launching.step.configuration.PreferenceFileCreation;
import morrigan.common.launching.step.configuration.PreferenceLoadingStep;
import morrigan.common.launching.step.graphic.GraphicalConfLaunching;

public class MinLauncherInitializer extends AbstractInitializer {

  /** Unique instance. */
  private static final MinLauncherInitializer instance = new MinLauncherInitializer();

  /**
   * Constructor used by factory.
   */
  protected MinLauncherInitializer() {
    super();
  }

  protected void feedStepList(List<LaunchingStep> stepList) {
    stepList.add(new GraphicalConfLaunching());
    stepList.add(new DescriptionsFileLoading());
    stepList.add(new PreferenceFileCreation());
    stepList.add(new PreferenceLoadingStep());
  }

  /**
   * @return the administration application initializer.
   */
  static public Initializer getMinLauncherInitializer() {
    return instance;
  }

}
