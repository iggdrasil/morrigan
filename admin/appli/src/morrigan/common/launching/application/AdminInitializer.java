package morrigan.common.launching.application;

import java.util.List;

import morrigan.common.launching.step.LaunchingStep;
import morrigan.common.launching.step.configuration.DescriptionsFileLoading;
import morrigan.common.launching.step.configuration.DiaFileLoading;
import morrigan.common.launching.step.configuration.DicoLoading;
import morrigan.common.launching.step.configuration.FilesSynchronizationChecking;
import morrigan.common.launching.step.configuration.PreferenceFileCreation;
import morrigan.common.launching.step.configuration.PreferenceLoadingStep;
import morrigan.common.launching.step.context.CurrentContextLoading;
import morrigan.common.launching.step.context.IdentificationStep;
import morrigan.common.launching.step.database.DataBaseConnectionStep;
import morrigan.common.launching.step.graphic.ActiveWindowsPreparationStep;
import morrigan.common.launching.step.graphic.AdministrationLaunching;
import morrigan.common.launching.step.graphic.GraphicalConfLaunching;
import morrigan.common.launching.step.graphic.MenuCreation;

/**
 * Administrator application initializer.
 * 
 * @author Armel
 */
public class AdminInitializer extends AbstractInitializer {

  /**
   * Constructor used by factory.
   */
  protected AdminInitializer() {
    super();
  }

  @Override
  protected void feedStepList(List<LaunchingStep> stepList) {
    stepList.add(new DicoLoading());
    stepList.add(new GraphicalConfLaunching());
    stepList.add(new IdentificationStep());
    stepList.add(new CurrentContextLoading());

    stepList.add(new DescriptionsFileLoading());
    stepList.add(new DiaFileLoading(true));
    stepList.add(new FilesSynchronizationChecking());
    stepList.add(new MenuCreation());
    stepList.add(new DataBaseConnectionStep());
    stepList.add(new PreferenceFileCreation());
    stepList.add(new PreferenceLoadingStep());
    stepList.add(new ActiveWindowsPreparationStep());
    stepList.add(new AdministrationLaunching());
  }

  /**
   * @return the administration application initializer.
   */
  static public Initializer createAdminInitializer() {
    return new AdminInitializer();
  }

}
