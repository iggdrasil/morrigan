package morrigan.common.launching.application;

import java.util.List;

import morrigan.common.launching.step.LaunchingStep;
import morrigan.common.launching.step.configuration.DescriptionsFileLoading;
import morrigan.common.launching.step.configuration.DiaFileLoading;
import morrigan.common.launching.step.configuration.DicoLoading;
import morrigan.common.launching.step.configuration.FilesSynchronizationChecking;
import morrigan.common.launching.step.configuration.PreferenceFileCreation;
import morrigan.common.launching.step.graphic.GraphicalConfLaunching;
import morrigan.common.launching.step.graphic.LabelEditorLaunching;

/**
 * @author Armel
 */
public class LabelEditorInitializer extends AbstractInitializer {

  public static LabelEditorInitializer instance = new LabelEditorInitializer();

  /**
   * Constructor used by factory.
   */
  protected LabelEditorInitializer() {
    super();
  }

  @Override
  protected void feedStepList(List<LaunchingStep> stepList) {
    stepList.add(new DicoLoading());
    stepList.add(new GraphicalConfLaunching());
    stepList.add(new DiaFileLoading(false));
    stepList.add(new DescriptionsFileLoading());
    stepList.add(new FilesSynchronizationChecking());
    stepList.add(new PreferenceFileCreation());
    stepList.add(new LabelEditorLaunching());
  }

  /**
   * @return the label editor application initializer (unique instance).
   */
  static public LabelEditorInitializer getLabelEditorInitializer() {
    return instance;
  }

}
