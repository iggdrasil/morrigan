/*
 * Initializer.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.application;

import morrigan.common.launching.initializer.InitException;

/**
 * Application initializer. It provides steps that are run successively.
 * 
 * @author Armel
 */
public interface Initializer {

  /**
   * @return the next step name
   */
  String getNextStepName();

  /**
   * @return <code>true</code> if it has more steps; <code>false</code>
   *         otherwise
   */
  boolean hasMoreSteps();

  /**
   * @return the number of remaining steps.
   */
  int getRemainingSteps();

  /**
   * @return the number of already launched steps.
   */
  int getLaunchedSteps();

  /**
   * Execute preparation steps.
   * 
   * @throws InitException
   */
  void executePreparationSteps() throws InitException;

  /**
   * Execute steps.
   * 
   * @throws InitException
   */
  void executeAndConsumeStep() throws InitException;
}
