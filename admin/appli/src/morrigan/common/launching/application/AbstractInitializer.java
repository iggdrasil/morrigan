/*
 * AbstractInitializer.java
 * 17 d�c. 07
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.common.launching.application;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import morrigan.common.launching.WindowActionLauncher;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.Flusher;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.common.launching.step.configuration.LogStep;
import morrigan.common.launching.step.context.GlobalContextLoading;

/**
 * @author Armel
 */
public abstract class AbstractInitializer implements Initializer {

  private final LinkedList<LaunchingStep> preparationStepList = new LinkedList<LaunchingStep>();

  private final LinkedList<LaunchingStep> stepList = new LinkedList<LaunchingStep>();

  private final int initStepQuantity;

  /**
   * Constructor used by factory.
   */
  protected AbstractInitializer() {
    super();

    preparationStepList.add(new GlobalContextLoading());
    preparationStepList.add(new Flusher());
    stepList.add(new LogStep());

    feedStepList(stepList);
    initStepQuantity = stepList.size();
  }

  /** {@inheritDoc} */
  public void executePreparationSteps() throws InitException {
    for (LaunchingStep step : preparationStepList) {
      executeStep(step);
    }
  }

  /**
   * @param stepList
   */
  abstract protected void feedStepList(List<LaunchingStep> list);

  /** {@inheritDoc} */
  public String getNextStepName() {
    return stepList.getFirst().getStepName();
  }

  /** {@inheritDoc} */
  public void executeAndConsumeStep() throws InitException {
    final LaunchingStep step = stepList.removeFirst();
    executeStep(step);
  }

  /**
   * Execute step in the good thread.
   * 
   * @param step a step
   * @throws InitException
   */
  private void executeStep(final LaunchingStep step) throws InitException {
    if (step instanceof WindowActionLauncher) {
      SwingUtilities.invokeLater(new Runnable() {

        public void run() {
          try {
            step.consume();
          } catch (Exception e) {
            throw new RuntimeException("Error while initializing step "
                + step.getStepName(), e);
          }
        }
      });

    } else {
      step.consume();
    }
  }

  /** {@inheritDoc} */
  public boolean hasMoreSteps() {
    return !stepList.isEmpty();
  }

  /** {@inheritDoc} */
  public int getRemainingSteps() {
    return stepList.size();
  }

  /** {@inheritDoc} */
  public int getLaunchedSteps() {
    return initStepQuantity - getRemainingSteps();
  }
}
