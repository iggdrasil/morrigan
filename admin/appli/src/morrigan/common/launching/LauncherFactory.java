package morrigan.common.launching;

import morrigan.common.launching.launcher.ConsoleLauncher;
import morrigan.common.launching.launcher.GraphicalLauncher;
import morrigan.common.launching.launcher.Launcher;

/**
 * @author Armel
 */
public class LauncherFactory {

  public static Launcher createLauncher() {
    Launcher launcher = null;
    boolean consoleLaunching = Boolean.getBoolean("launching.console");
    if (!consoleLaunching) {
      launcher = new GraphicalLauncher();
    } else {
      launcher = new ConsoleLauncher();
    }
    return launcher;
  }

}
