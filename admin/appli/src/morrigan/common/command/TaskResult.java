/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : TaskResult.java - CREATION : 2008/10/13
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.common.command;

import java.util.List;

public class TaskResult {

  private final int taskNumber;

  private boolean ok = false;

  private final int result;
  
  private Throwable throwable = null;

  public TaskResult(Task<TaskResult> task, int taskNumber, int result, Exception e) {
    this(task, taskNumber,result);
    this.throwable = e;
  }

  public TaskResult(Task<TaskResult> task, int taskNumber, int result) {
//    this.task = task;
    this.taskNumber = taskNumber;
    this.result = result;
  }
  
  /**
   * @return the taskNumber
   */
  public int getTaskNumber() {
    return taskNumber;
  }

  /**
   * @return the ok
   */
  public boolean isOk() {
    return ok;
  }

  /**
   * @param ok the ok to set
   */
  public void setOk(boolean ok) {
    this.ok = ok;
  }

  public int getResult() {
    return result;
  }

//  public List<TaskListener> getTaskListeners() {
//    return task.getTaskListeners();
//  }
  
  /**
   * @return the throwable
   */
  public Throwable getThrowable() {
    return throwable;
  }
   /**
   * @param throwable the throwable to set
   */
  public void setThrowable(Throwable throwable) {
    this.throwable = throwable;
  }

  @Override
  public String toString() {
    return "TaskResult : taskNum=" + taskNumber + ", result=" + result;
  }
}
