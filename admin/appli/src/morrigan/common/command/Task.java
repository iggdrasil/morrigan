/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : TaskExtension.java - CREATION : 2008/10/13
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.common.command;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public abstract class Task<V> implements Callable<V> {
  
  private int num;
  
  private List<TaskListener> taskListeners = new ArrayList<TaskListener>();

  public Task(int num) {
    this.num = num;
  }

  public int getNum() {
    return num;
  }

  public void addListener(TaskListener listener) {
    taskListeners.add(listener);
  }

  public void removeListener(TaskListener listener) {
    taskListeners.remove(listener);
  }

  public List<TaskListener> getTaskListeners() {
    return taskListeners;
  }

  public void setNum(int num) {
    this.num = num;
  }
  
}