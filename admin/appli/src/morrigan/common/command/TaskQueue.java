/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : TaskQueue.java - CREATION : 2008/10/13
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.common.command;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * A beautiful task queue. Just submit a new task ({@link #submit(Task)}) and
 * this one will be executed when enough time is available.
 * 
 * @author armel
 */
public class TaskQueue {

  private static TaskQueue instance = new TaskQueue();

  private final ThreadPoolExecutor threadPoolExecutor;

  private int taskNumber = 1;

  private TaskQueue() {
    BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();
    threadPoolExecutor = new ThreadPoolExecutorExtension(2, 5, 3,
        TimeUnit.SECONDS, workQueue);
  }

  /**
   * @return the queue instance
   */
  public static TaskQueue getInstance() {
    return instance;
  }

  /**
   * Submit a new Task.
   * 
   * @param task
   * @return the linked future
   */
  public Future<TaskResult> submit(Task<TaskResult> task) {
    task.setNum(taskNumber++);
    Future<TaskResult> future = threadPoolExecutor.submit(task);
    return future;
  }

  /**
   * Personalised thread pool executor.
   * 
   * @author armel
   */
  private final class ThreadPoolExecutorExtension extends ThreadPoolExecutor {
    private ThreadPoolExecutorExtension(int corePoolSize, int maximumPoolSize,
        long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
      super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
      super.beforeExecute(t, r);
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
      TaskResult o = null;
      if (FutureTask.class.isInstance(r)) {
        try {
          FutureTask<?> futureTask = (FutureTask<?>) r;
          o = (TaskResult) futureTask.get();
          if (o.getThrowable() != null) {
            throw new RuntimeException(o.getThrowable());
          }

//          for (TaskListener l : o.getTaskListeners()) {
//            l.notifyTaskEnd(o);
//          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
      }
       super.afterExecute(r, t);
    }
  }
}
