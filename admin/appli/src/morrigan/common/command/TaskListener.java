/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : TaskListener.java - CREATION : 2008/10/13
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.common.command;

/**
 * An task listener.
 * 
 * @author armel
 */
public interface TaskListener {

  void notifyTaskStart(Task task);

  void notifyTaskProgression(Task task, int percentage);

  void notifyTaskEnd(TaskResult myResult);
}
