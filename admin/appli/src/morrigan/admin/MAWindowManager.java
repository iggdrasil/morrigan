package morrigan.admin;

import java.awt.Window;
import java.lang.reflect.Constructor;

import javax.swing.SwingUtilities;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.exception.OpenWindowException;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.util.MADialogue;
import morrigan.admin.util.MAGenerateurFenetre;
import morrigan.admin.util.MAMenuGenerator;
import morrigan.admin.util.MAUtil;
import morrigan.admin.util.MADialogue.MESSAGE;
import morrigan.log.MLog;

/**
 * Gere l'ouverture et la fermeture des fenetres.
 * 
 * @author armel
 */
public class MAWindowManager {

  /** Main window */
  private static MAFenAdmin menuWindow = null;

  /**
   * Open a window or put it in first sight if hidden, or create it if it does
   * not exist yet.
   * 
   * @param sysWindow the system name of window to open (or create or show)
   * @throws OpenWindowException if the window opening fails
   * @throws CreateWindowException if the window creation fails
   */
  public static Window openWindow(final MASysWindow sysWindow)
      throws OpenWindowException, CreateWindowException {

    if (SwingUtilities.isEventDispatchThread()) {
      return openWindowInAThread(sysWindow);
    }

    final Window[] createdWindow = new Window[1];
    try {
      SwingUtilities.invokeAndWait(new Runnable() {
        public void run() {
          try {
            createdWindow[0] = openWindowInAThread(sysWindow);
          } catch (OpenWindowException e) {
            e.printStackTrace();
          } catch (CreateWindowException e) {
            e.printStackTrace();
          }
        }
      });

    } catch (Exception e) {
      throw new CreateWindowException("The window cannot be opened", e);
    }
    return createdWindow[0];
  }

  /**
   * Open a window in the current thread.
   * @param sysWindow
   * @return the opened window
   * @throws OpenWindowException
   * @throws CreateWindowException
   */
  private static Window openWindowInAThread(MASysWindow sysWindow)
      throws OpenWindowException, CreateWindowException {
    try {
      MAUtil.notNull(sysWindow,
          "sysWindow must not be null when is to be created");

      // Si la fenetre existe deja, on l'affiche
      // Sinon on la cree avec la position par defaut.
      // MJFrame mjf = hmFenetresActives.get(fenetre);
      final Window mjf;
      if (sysWindow.getWindow() != null) {
        mjf = sysWindow.getWindow();
      } else {
        mjf = createWindow(sysWindow);
        if (mjf == null) {
          MLog.error("MAdminAppli", "ouvrirFenetre", "La fenetre "
              + sysWindow.getSysName() + " n'a pas ete construite !");
          throw new OpenWindowException("La fenetre " + sysWindow.getSysName()
              + " n'a pas ete construite !");
        }

        if (sysWindow.getSavedPosition() != null) {
          mjf.setLocation(sysWindow.getSavedPosition());
        }
      }

      if (!sysWindow.getWindow().isVisible()
          && sysWindow.getSavedPosition() != null) {
        mjf.setLocation(sysWindow.getSavedPosition());
      }

      mjf.setVisible(true);
      sysWindow.getWindow().requestFocus();

      if (menuWindow != null) {
        menuWindow.notifyOpening(sysWindow);
      }
      return mjf;
    } catch (CreateWindowException cwe) {
      MADialogue.showMessage(null, MESSAGE.ERREUR,
          "An error occured while creating the window "
              + (sysWindow != null ? sysWindow.getSysName() : "")
              + "[see log file]");
      throw cwe;
    } catch (Exception e) {
      MADialogue.showMessage(null, MESSAGE.ERREUR,
          "A strange error occured while creating the window "
              + (sysWindow != null ? sysWindow.getSysName() : "")
              + "[see log file]");
      throw new CreateWindowException(
          "An error occured while creating the window", e);
    }
  }

  /**
   * Create a window at a specific (x,y) point.<br>
   * This window is not visible yet ! Use {@link #openWindow(MASysWindow)} to
   * set it visible.
   * 
   * @param sysWindow system name of the window
   * @param x x position
   * @param y y position
   * @return the new window
   * @throws CreateWindowException if the window creation fails
   */
  public static Window createWindow(MASysWindow sysWindow, int x, int y)
      throws CreateWindowException {
    final Window mjf = MAWindowManager.createWindow(sysWindow);
    if (mjf == null) {
      return null;
    }

    mjf.setLocation(x, y);

    return mjf;
  }

  /**
   * Create a window (or do nothing if it already exists).
   * 
   * @param sysWindow the system name of the window to create
   * @return the new window
   * @throws CreateWindowException if the creation failed
   */
  public static Window createWindow(MASysWindow sysWindow)
      throws CreateWindowException {
    MAUtil.notNull(sysWindow, "fenetre");

    Window mjf = sysWindow.getWindow();

    // La fenetre n'existe pas encore, il faut la creer
    if (mjf == null) {
      // Fenetre issue de table ?
      if (!sysWindow.isSpecific()) {
        // Fenetre basee sur du XML ?
        mjf = MAGenerateurFenetre.createWindow(sysWindow);
      } else {
        // ou Fenetre specifique ?
        Class<?> cFenetre = sysWindow.getClazz();
        try {
          Constructor<?> constructor = cFenetre.getConstructor(new Class[] {});
          mjf = (Window) constructor.newInstance(new Object[] {});
        } catch (Exception e) {
          MLog.error("MAdminAppli", "creerFenetre",
              "Erreur lors de la creation de la fenetre "
                  + sysWindow.getSysName(), e);

          throw new CreateWindowException(
              "Erreur lors de la creation de la fenetre "
                  + sysWindow.getSysName(), e);
        }
      }

      if (mjf == null) {
        MLog.error("MAdminAppli", "creerFenetre",
            "Impossible de creer la fenetre " + sysWindow.getSysName());
        throw new CreateWindowException("Impossible de creer la fenetre "
            + sysWindow.getSysName());
      }

      if (sysWindow.getSysName().equals(MEASpecificWindow.MENU.getSysName())) {
        menuWindow = (MAFenAdmin) mjf;
        menuWindow.applyMenu(MAMenuGenerator.getMenuBar());
      }

      mjf.addWindowListener(menuWindow);
      sysWindow.setWindow(mjf);
    }

    return mjf;
  }

  /**
   * Close a window by hiding it.
   * 
   * @param sysWindow window system name
   */
  public static void closeWindow(MASysWindow sysWindow) {
    if (sysWindow == null) {
      throw new IllegalArgumentException("fenetre cannot be null");
    }
    final Window mjf = sysWindow.getWindow();

    // Si la fenetre est deja invisible, on ne fait rien
    if (mjf == null || !mjf.isVisible()) {
      return;
    }

    mjf.setVisible(false);

    if (menuWindow != null) {
      menuWindow.notifyClosing(sysWindow);
    }
  }

  /**
   * Close all windows (except the menu) by hiding them.
   */
  public static void closeAllWindows() {
    for (MASysWindow fenetre : MAContextManager.getInstance()
        .getWindowContext().values()) {
      if (fenetre.getSysName() != MEASpecificWindow.MENU.getSysName()) {
        closeWindow(fenetre);
      }
    }
  }

  /**
   * Notify a window opening
   * 
   * @param sysWindow the window that has been opened
   */
  protected void notifyOpening(MASysWindow sysWindow) {
    menuWindow.notifyOpening(sysWindow);
  }

  /**
   * Notify a window closing
   * 
   * @param sysWindow the window that has been closed
   */
  protected void notifyClosing(MASysWindow sysWindow) {
    menuWindow.notifyClosing(sysWindow);
  }

}
