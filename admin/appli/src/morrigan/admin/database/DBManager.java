/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : DBManager.java - CREATION : 2004/01/01
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import morrigan.log.MLog;

/**
 * Cette classe a pour but de gerer les acces a la base de donnees. Afin que
 * cela fonctionne au mieux, une unique instance de cette classe existe. Toutes
 * les requetes sur la base de donnees doivent passer par elle. Ceci permet une
 * gestion plus fine et moins anarchique. De plus, les acces sont moins
 * concurrentiels.
 * 
 * @author armel
 */
public class DBManager {
  /**
   * The database url, such as :<br>
   * <code>jdbc:postgresql://nikita:5432/morrigan</code>
   */
  private String dbUrl = null;

  private String dbUser = null;

  private String dbPassword = null;

  private Connection connection = null;

  private boolean autoCommit = true;

  static private DBManager instance = null;

  private final String driverClazz;

  static public void openConnection(String dbUrl, String dbUserName,
      String dbPassword, String driverClazz) throws Exception {
    instance = new DBManager(dbUrl, dbUserName, dbPassword, driverClazz);
    instance.openConnection();
  }

  static public boolean fermerConnexion() {
    if (getInstance() != null) {
      if (!getInstance().autoCommit)
        try {
          if (!rollback())
            throw new Exception();
        } catch (Exception e) {
          MLog.error("DBManager", "fermerConnexion", "Rollback impossible", e);
        }
      getInstance().closeConnection();
    }
    return true;
  }

  /**
   * Constructeur d'un gestionnaire d'acces.
   * 
   * @param driverClazz TODO
   */
  private DBManager(String dbUrl, String dbUser, String dbPassword,
      String driverClazz) throws Exception {
    this.dbUrl = dbUrl;
    this.dbUser = dbUser;
    this.dbPassword = dbPassword;
    this.driverClazz = driverClazz;
    this.connection = null;
  }

  /**
   * Ouvre une connexion avec la base de donnees.
   */
  private void openConnection() throws Exception {
    Class.forName(driverClazz);
    DriverManager.setLoginTimeout(1);
    try {
      connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
      // statement = connection.createStatement();
    } catch (SQLException e) {
      MLog.error("DBManager", "openConnection",
          "Error when trying a connection to the database (ulr='" + dbUrl
              + "', user='" + dbUser + "')", e);
    }
  }

  /**
   * Ferme la connexion avec la base de donnees.
   */
  private void closeConnection() {
    try {
      if (connection != null)
        connection.close();
    } catch (SQLException e) {
      System.err.println("[" + this.getClass()
          + "] [closeConnection()]Erreur lors de la deconnexion de la base: "
          + e);
      e.printStackTrace();
    }
  }

  /**
   * Execute la requete SQL (de type SELECT) et renvoie le(s) resultat(s).
   * 
   * @param query requete a executer
   * @return resultat de la requete
   * @throws SQLException
   */
  public ResultSet executeQuery(String query) throws SQLException {
    if (connection == null)
      return null;

    ResultSet resultSet = null;
    Statement statement = connection.createStatement();
    resultSet = statement.executeQuery(query);
    return resultSet;
  } // executeQuery

  /**
   * Execute la requete SQL (de type UPDATE, DELETE) et renvoie le nombre de
   * lignes concerneees.
   * 
   * @param query requete a executer
   * @return le nombre de lignes modifiees
   * @throws SQLException
   * @see DBManager#executeInsert(String)
   */
  public int executeUpdate(String query) throws SQLException {
    if (connection == null)
      return -1;

    int iNbLignes = 0;
    Statement statement = connection.createStatement();
    iNbLignes = statement.executeUpdate(query);
    return iNbLignes;
  } // executeUpdate

  /**
   * Execute la requete SQL de type INSERT et renvoie le nombre de lignes
   * concerneees.
   * 
   * @param query requete a executer
   * @return le nombre de lignes inserees
   * @throws SQLException
   */
  public int executeInsert(String query) throws SQLException {
    if (connection == null)
      return -1;

    int iNbLignes = 0;

    Statement statement = connection.createStatement();
    iNbLignes = statement.executeUpdate(query);
    return iNbLignes;
  } // executeInsert

  public boolean openTransaction() throws SQLException {
    if (connection == null)
      return false;

    Statement statement = connection.createStatement();
    statement.executeUpdate("BEGIN;");
    MLog.infos("Ouverture de transaction.");
    return true;
  } // executeCommit

  public boolean executeCommit() throws SQLException {
    if (connection == null)
      return false;

    Statement statement = connection.createStatement();
    statement.executeUpdate("COMMIT;");

    MLog.infos("Commit. Les donnees sont validees.");

    if (!autoCommit)
      return openTransaction();

    return true;
  } // executeCommit

  public boolean executeRollback() throws SQLException {
    if (connection == null)
      return false;

    Statement statement = connection.createStatement();
    statement.executeUpdate("ROLLBACK;");
    MLog.infos("Rollback. Les donnees sont annulees.");
    if (!autoCommit)
      return openTransaction();

    return true;
  } // executeRollback

  /**
   * Retourne l'instance unique de MDBManager.
   * 
   * @return l'instance unique de MDBManager
   */
  static public DBManager getInstance() {
    return instance;
  }

  /**
   * Insere des donnees et retourne le nombre de lignes inserees.
   * 
   * @param query requete
   * @return nombre de lignes inserees
   * @throws SQLException
   */
  static public int insert(String query) throws SQLException {
    if (getInstance() == null)
      return -1;
    return getInstance().executeInsert(query);
  }

  /**
   * Met a jour des donnees et retourne le nombre de lignes modifiees.
   * 
   * @param query requete
   * @return nombre de lignes modifiees
   * @throws SQLException
   */
  static public int update(String query) throws SQLException {
    if (getInstance() == null)
      return -1;
    return getInstance().executeUpdate(query);
  }

  /**
   * Selectionne des donnees.
   * 
   * @param query requete
   * @return resultat du SELECT
   * @throws SQLException
   */
  static public ResultSet select(String query) throws SQLException {
    if (getInstance() == null)
      return null;
    return getInstance().executeQuery(query);
  }

  static public void setAutoCommit(boolean bAutoCommit) throws SQLException {
    if (getInstance() == null)
      return;

    MLog.infos("Changement de type de connexion. Autocommit "
        + (bAutoCommit ? "actif" : "inactif"));

    // Commit prealable
    getInstance().executeCommit();
    getInstance().autoCommit = bAutoCommit;

    if (!bAutoCommit)
      getInstance().openTransaction();
  }

  static public boolean commit() throws SQLException {
    if (getInstance() == null)
      return false;
    return getInstance().executeCommit();
  }

  static public boolean rollback() throws SQLException {
    if (getInstance() == null)
      return false;
    return getInstance().executeRollback();
  }
}