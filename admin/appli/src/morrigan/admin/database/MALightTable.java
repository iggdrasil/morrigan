/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MALightTable.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database;

/**
 * Light version of a table : a name and its trigram.
 * 
 * @author armel
 */
public class MALightTable {
  private String nom;

  private String trigramme;

  /**
   * Constructor with all parameters.
   * 
   * @param name
   * @param trigram
   */
  public MALightTable(String name, String trigram) {
    this.nom = name;
    this.trigramme = trigram;
  }

  public String getNom() {
    return nom;
  }

  public String getTrig() {
    return trigramme;
  }
}