/**
 * 
 */
package morrigan.admin.database.tabledesc.table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Operation;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.database.tabledesc.operation.UniqueIndexOperation;

public class DefaultTable implements Table {

  private final List<Attribute> attributes = new LinkedList<Attribute>();

  private final List<Operation> operations = new LinkedList<Operation>();

  private final String name;

  private final String label;

  private boolean isEnabled = true;

  public DefaultTable(String name, String label) {
    this.name = name;
    this.label = label;
  }

  public String getName() {
    return name;
  }

  public String getLabel() {
    return label;
  }

  public boolean isEnabled() {
    return isEnabled;
  }

  public void setEnabled(boolean isEnabled) {
    this.isEnabled = isEnabled;
  }

  public void addAttribute(Attribute attribute) {
    attributes.add(attribute);
  }

  public Iterator<Attribute> getAttributesIterator() {
    return attributes.iterator();
  }

  public Attribute getAttribute(int index) {
    return attributes.get(index);
  }

  public Attribute getAttribute(String nomAttribut) {
    for (Iterator<Attribute> iter = attributes.iterator(); iter.hasNext();) {
      Attribute attribut = iter.next();
      if (attribut.getName().equals(nomAttribut)) {
        return attribut;
      }
    }

    throw new RuntimeException("Attribut de name '" + name + "' inconnu !");
  }

  public void addOperation(Operation operation) {
    operations.add(operation);
  }

  public Iterator<Operation> getOperationsIterator() {
    return operations.iterator();
  }

  @Override
  public String toString() {
    return "[Table\t: " + name + ",\n Attributes\t: " + attributes
        + ",\n Operations\t: " + operations + "]\n";
  }

  public int getAttributesNb() {
    return attributes.size();
  }

  public List<Attribute> getUniqueIndexAttributes() {
    List<Attribute> uniqueIndexAttributes = new ArrayList<Attribute>();
    for (Operation operation : operations) {
      if (operation instanceof UniqueIndexOperation) {
        UniqueIndexOperation indexOperation = (UniqueIndexOperation) operation;
        uniqueIndexAttributes.addAll(indexOperation.getFields());
      }
    }

    return uniqueIndexAttributes;
  }
}