/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Grant.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

import java.util.ArrayList;
import java.util.List;

/**
 * Grant: list of rights on table.
 * 
 * @author armel
 */
public class Grant {

  private List<GrantRight> rights = new ArrayList<GrantRight>();

  public Grant() {}

}
