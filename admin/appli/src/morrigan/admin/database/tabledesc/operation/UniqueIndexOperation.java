/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : UniqueIndexOperation.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Attribute;

/**
 * Unique index operation.
 * 
 * @author armel
 */
public class UniqueIndexOperation extends AbstractOperation {

  private List<String> stringFields = new ArrayList<String>();

  private List<Attribute> fields = null;

  public UniqueIndexOperation(String comment) {
    super(comment);
  }

  public List<Attribute> getFields() {
    if (fields == null) {
      fields = new ArrayList<Attribute>();
      for (String attributeName : stringFields) {
        fields.add(MAContextManager.getInstance().getDbContext()
            .getAttributeByName(attributeName));
      }
    }
    return fields;
  }

  @Override
  public String toString() {
    return "[UniqueIndexOperation : " + getComment() + ", " + getFields() + "]";
  }

  public void addField(String param) {
    stringFields.add(param);
  }

}
