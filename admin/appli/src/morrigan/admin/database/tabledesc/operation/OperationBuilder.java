/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : OperationBuilder.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

import java.util.List;

import morrigan.admin.database.tabledesc.Operation;

/**
 * Helper for building Operation, such as SQL information.
 * 
 * @author armel
 */
public class OperationBuilder {

  public static final String GRANT = "grant";

  public static final String UNIQUE_INDEX = "unique index";

  public static final String INDEX = "index";

  private AbstractOperation currentOperation = null;

  /**
   * Default constructor.
   */
  public OperationBuilder() {}

  /**
   * Return the built operation.
   * 
   * @return
   */
  public Operation getBuiltOperation() {
    return currentOperation;
  }

  /**
   * Instanciate a new operation.
   * 
   * @param name name of the operation
   * @param type type of the operation (grant, index, unique index)
   * @param comment a comment
   */
  public void newOperation(String name, String type, String comment) {
    if (GRANT.equalsIgnoreCase(type)) {
      currentOperation = new GrantOperation(comment);
      addRightsFromString(name);
    } else if (UNIQUE_INDEX.equalsIgnoreCase(type)) {
      currentOperation = new UniqueIndexOperation(comment);
    } else if (INDEX.equalsIgnoreCase(type)) {
      // Nothing for the moment
      // TODO : notice the INDEX
    } else {
      throw new RuntimeException("Unknown operation type ! '" + type + "'");
    }
  }

  /**
   * Add rights to the operation. Rights are mentionned in a string.
   * 
   * @param string
   */
  private void addRightsFromString(String string) {
    List<GrantRight> rights = ((GrantOperation) currentOperation).getRights();
    for (GrantRight grantRight : GrantRight.values()) {
      if (string.toLowerCase().contains(grantRight.toString().toLowerCase())) {
        rights.add(grantRight);
      }
    }
  }

  /**
   * Add a param to the operation. It can be a user or a field.
   * 
   * @param param a user or a field expressed as a string
   */
  public void addParam(String param) {
    if (currentOperation instanceof GrantOperation) {
      ((GrantOperation) currentOperation).getUsers().add(param);
    } else if (currentOperation instanceof UniqueIndexOperation) {
      ((UniqueIndexOperation) currentOperation).addField(param);
    }
  }

}
