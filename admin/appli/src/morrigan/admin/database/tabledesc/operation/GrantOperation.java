/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : GrantOperation.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

import java.util.ArrayList;
import java.util.List;

/**
 * Grant operation. Action of giving rights to users.
 * 
 * @author armel
 */
public class GrantOperation extends AbstractOperation {

  private List<String> users = new ArrayList<String>();

  private List<GrantRight> rights = new ArrayList<GrantRight>();

  public GrantOperation(String comment) {
    super(comment);
  }

  public void setUsers(List<String> users) {
    this.users = users;
  }

  public List<String> getUsers() {
    return users;
  }

  @Override
  public String toString() {
    return "[GrantOperation : " + getComment() + ", " + getUsers() + ", "
        + getRights() + "]";
  }

  public void setRights(List<GrantRight> rights) {
    this.rights = rights;
  }

  public List<GrantRight> getRights() {
    return rights;
  }
}
