/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : GrantRight.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

/**
 * Rights on table, such as a select or a modification.
 * 
 * @author armel
 */
public enum GrantRight {
  ALL,
  SELECT,
  INSERT,
  UPDATE,
  DELETE;
}
