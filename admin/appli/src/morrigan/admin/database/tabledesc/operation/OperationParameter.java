package morrigan.admin.database.tabledesc.operation;

public class OperationParameter {

  private final String nom;

  public OperationParameter(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  @Override
  public String toString() {
    return "[Param - " + nom + "]";
  }
}