/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : AbstractOperation.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.operation;

import morrigan.admin.database.tabledesc.Operation;

/**
 * An abstract operation (sql information).
 * 
 * @author armel
 */
public abstract class AbstractOperation implements Operation {

  private final String comment;

  public AbstractOperation(String comment) {
    this.comment = comment;
  }

  public String getComment() {
    return comment;
  }

  @Override
  public String toString() {
    return "[AbstractOperation : " + comment + " ]";
  }
}
