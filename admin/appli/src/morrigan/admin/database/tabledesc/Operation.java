/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MALightTable.java - CREATION : 2008/06/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc;

public interface Operation {

  public String getComment();
}