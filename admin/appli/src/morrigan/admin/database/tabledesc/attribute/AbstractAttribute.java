/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : AbstractAttribute.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.attribute;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.util.MAUtil;
import morrigan.admin.util.METypeChamp;

public abstract class AbstractAttribute implements Attribute {

  private final String name;

  private String label;

  private final METypeChamp type;

  private final String defaultValue;

  private String comment;

  private final Integer size;

  private boolean isMandatory;

  private boolean isEnabled;

  private Table table = null;

  public AbstractAttribute(String name, String label, METypeChamp type,
      String defaultValue, String comment, Integer size, boolean isEnabled) {

    MAUtil.notNullOrEmpty(name, "attribute name");
    MAUtil.notNullOrEmpty(label, "attribute label");
    MAUtil.notNull(type, "attribute type");
    MAUtil.notNullOrEmpty(comment, "attribute comment");

    this.name = name;
    this.label = label;
    this.type = type;
    this.defaultValue = defaultValue;
    this.comment = comment;
    this.size = size;
    this.isEnabled = isEnabled;
    this.isMandatory = ((defaultValue != null) && defaultValue.toLowerCase()
        .contains("not null"))
        || (name.endsWith("_id") && (name.length() == 6));
  }

  public String getComment() {
    return comment;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public String getLabel() {
    return label;
  }

  public String getName() {
    return name;
  }

  public Integer getSize() {
    return size;
  }

  public METypeChamp getType() {
    return type;
  }

  @Override
  public String toString() {
    return "[Attribute : " + name + ", " + label + ", " + type + ", " + comment
        + ", " + defaultValue + ", " + size + "]";
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public void setMandatory(boolean isMandatory) {
    this.isMandatory = isMandatory;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public boolean isEnabled() {
    return isEnabled;
  }

  public boolean isID() {
    return (this instanceof AttributePrimaryKey);
  }

  public boolean isForeignID() {
    return (this instanceof AttributeForeignKey);
  }

  public Table getTable() {
    if (table == null) {
      table = MAContextManager.getInstance().getDbContext()
          .getTableParTrigramme(getName().substring(0, 3));
    }
    return table;
  }
}