/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : AttributeBuilder.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.attribute;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.util.MAUtil;
import morrigan.admin.util.METypeChamp;
import morrigan.log.MLog;

public class AttributeBuilder {

  private static final String ID = "id";

  private AttributeBuilder() {}

  static public Attribute buildAttribute(String nom, String label, String type,
      String defaultValue, String comment, Integer size, boolean isEnabled) {

    Attribute attribute = null;

    MAUtil.notNullOrEmpty(nom, "attribute name");
    MAUtil.notNullOrEmpty(type, "attribute type");
    MAUtil.notNullOrEmpty(comment, "attribute comment");

    METypeChamp typeChamp = METypeChamp.getChampParTypeSql(type);
    
    String[] split = nom.split("_");
    
    if (nom.length() == 6 && ID.equals(split[split.length-1])) {
      // xxx_ID
      attribute = new AttributePrimaryKey(nom, label, typeChamp, defaultValue,
          comment, size);
    } else if (split.length >= 2 && split[split.length - 2].length() == 3
        && ID.equals(split[split.length - 1])) {
      // xxx_yyy_id
      attribute = new AttributeForeignKey(nom, label, typeChamp, defaultValue,
          comment, size, isEnabled);
    } else {
      // xxx_abcdef
      attribute = new DefaultAttribute(nom, label, typeChamp, defaultValue,
          comment, size, isEnabled);
    }

    return attribute;
  }

}
