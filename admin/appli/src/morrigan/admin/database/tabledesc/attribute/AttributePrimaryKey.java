/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : AttributePrimaryKey.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.attribute;

import morrigan.admin.util.METypeChamp;

public class AttributePrimaryKey extends AbstractAttribute {

  public AttributePrimaryKey(String nom, String label, METypeChamp type,
      String defaultValue, String comment, Integer size) {
    super(nom, label, type, defaultValue, comment, size, false);
  }

}
