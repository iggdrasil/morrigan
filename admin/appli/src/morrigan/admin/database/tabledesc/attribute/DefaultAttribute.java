/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : DefaultAttribute.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.attribute;

import morrigan.admin.util.METypeChamp;

public class DefaultAttribute extends AbstractAttribute {

  public DefaultAttribute(String nom, String label, METypeChamp type,
      String defaultValue, String comment, Integer size, boolean enabled) {
    super(nom, label, type, defaultValue, comment, size, enabled);
  }

}
