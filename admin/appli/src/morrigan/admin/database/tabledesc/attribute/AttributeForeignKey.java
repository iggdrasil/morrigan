/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : AttributeForeignKey.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc.attribute;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.util.METypeChamp;

public class AttributeForeignKey extends AbstractAttribute {

  private Table foreignTable = null;

  public AttributeForeignKey(String nom, String label, METypeChamp type,
      String defaultValue, String comment, Integer size, boolean isEnabled) {
    super(nom, label, type, defaultValue, comment, size, isEnabled);
  }

  public Table getForeignTable() {
    if (foreignTable == null) {
      String[] split = getName().split("_");
      foreignTable = MAContextManager.getInstance().getDbContext()
          .getTableParTrigramme(split[split.length-2]);
    }
    return foreignTable;
  }

}
