/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MALightTable.java - CREATION : 2008/06/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc;

import morrigan.admin.util.METypeChamp;

public interface Attribute {

  public String getName();

  public String getLabel();

  public String getComment();

  public boolean isMandatory();

  public void setComment(String comment);

  public void setMandatory(boolean isMandatory);

  public void setLabel(String label);

  public Integer getSize();

  public METypeChamp getType();

  public boolean isEnabled();

  public boolean isID();

  public boolean isForeignID();

  public Table getTable();
}
