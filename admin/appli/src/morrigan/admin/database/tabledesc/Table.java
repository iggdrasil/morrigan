/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Table.java - CREATION : 2008/06/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database.tabledesc;

import java.util.Iterator;
import java.util.List;

public interface Table {

  public String getName();

  public String getLabel();

  public int getAttributesNb();

  public Iterator<Operation> getOperationsIterator();

  public Attribute getAttribute(String nomAttribut);

  public Iterator<Attribute> getAttributesIterator();

  public Attribute getAttribute(int index);

  public boolean isEnabled();

  public List<Attribute> getUniqueIndexAttributes();
}
