package morrigan.admin.database.tabledesc.association;

public abstract class AbstractAssociation implements Association {

  private final String name;

  private String firstEndName;

  private String secondEndName;

  public AbstractAssociation(String name) {
    super();
    this.name = name;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the firstEndName
   */
  public String getFirstEndName() {
    return firstEndName;
  }

  /**
   * @param firstEndName the firstEndName to set
   */
  public void setFirstEndName(String firstEndName) {
    this.firstEndName = firstEndName;
  }

  /**
   * @return the secondEndName
   */
  public String getSecondEndName() {
    return secondEndName;
  }

  /**
   * @param secondEndName the secondEndName to set
   */
  public void setSecondEndName(String secondEndName) {
    this.secondEndName = secondEndName;
  }

  @Override
  public String toString() {
    return "Association (" + firstEndName + " -> " + secondEndName + ")";
  }

}
