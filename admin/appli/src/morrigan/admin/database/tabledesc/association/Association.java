package morrigan.admin.database.tabledesc.association;

public interface Association {

  String getName();

}
