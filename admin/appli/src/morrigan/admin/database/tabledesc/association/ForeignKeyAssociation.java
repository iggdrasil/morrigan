package morrigan.admin.database.tabledesc.association;

public class ForeignKeyAssociation extends AbstractAssociation {

  private String tableName = null;

  public ForeignKeyAssociation(String name) {
    super(name);
  }

  /**
   * @return the tableName
   */
  public String getTableName() {
    return tableName;
  }

  /**
   * @param tableName the tableName to set
   */
  public void setTableName(String tableName) {
    this.tableName = tableName;
  }

  /**
   * @return the field which is the primary key.
   */
  public String getPrimaryKeyName() {
    return getFirstEndName().length() < getSecondEndName().length() ? getFirstEndName()
        : getSecondEndName();
  }

  /**
   * @return the field which is the primary key.
   */
  public String getForeignKeyName() {
    return getFirstEndName().length() > getSecondEndName().length() ? getFirstEndName()
        : getSecondEndName();
  }
}
