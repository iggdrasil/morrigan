/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MADBTable.java - CREATION : 2005/11/20
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.database;

import org.jdom.Element;

public class MADBTable {
  private String sNom;

  private String sTrigramme;

  public MADBTable(Element eXML) {
    this.sNom = eXML.getAttributeValue("nom");
    this.sTrigramme = eXML.getAttributeValue("trigramme");
  }

  public String getNom() {
    return sNom;
  }

  public String getTrig() {
    return sTrigramme;
  }
}