package morrigan.admin.database;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Properties;

import morrigan.log.MLog;

public class DatabaseConnexionRetriever {

  public DatabaseConnexionRetriever() {}

  /**
   * @param adresse
   * @param useProxy
   * @param login
   * @param password
   * @return properties
   * @see http://www.exampledepot.com/egs/java.net/Post.html
   */
  public Properties doPost(String adresse, boolean useProxy, String login,
      String password) {
    MLog.debug("DatabaseConnexionRetriever", "doPost",
        "Try retrieve DB url by using address=" + adresse + " and login="
            + login + " (useproxy=" + useProxy + ')');

    if (useProxy) {
      Properties prop = System.getProperties();
      prop.put("http.proxyHost", "proxy.fr.world.socgen");
      prop.put("http.proxyPort", "8080");

      Authenticator.setDefault(new AuthenticatorExtension());
    }

    OutputStreamWriter writer = null;
    InputStream inputStream = null;
    try {
      // encodage des parametres de la requete
      String donnees = URLEncoder.encode("login", "UTF-8") + "="
          + URLEncoder.encode(login, "UTF-8");
      donnees += "&" + URLEncoder.encode("password", "UTF-8") + "="
          + URLEncoder.encode(password, "UTF-8");

      // creation de la connection
      URL url = new URL(adresse);
      URLConnection conn = url.openConnection();
      conn.setDoOutput(true);

      // envoi de la requete
      writer = new OutputStreamWriter(conn.getOutputStream());
      writer.write(donnees);
      writer.flush();

      // lecture de la reponse
      inputStream = conn.getInputStream();
      Properties properties = new Properties();
      properties.load(inputStream);

      if (properties.size() == 0) {
        MLog.error("", "", "Wrong url, login and/or password");
        return null;
      }

      return properties;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        writer.close();
      } catch (Exception e) {}
      try {
        inputStream.close();
      } catch (Exception e) {}
    }
    return null;
  }

  private final class AuthenticatorExtension extends Authenticator {
    protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication("armel.lagadic", "XXXXXXXX"
          .toCharArray());
    }
  }
}
