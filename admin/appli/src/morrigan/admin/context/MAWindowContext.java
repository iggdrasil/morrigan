/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAWindowContext.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.graphique.fenetre.MJFrame;
import morrigan.log.MLog;

/**
 * The window context.
 * 
 * @author Armel
 */
public final class MAWindowContext {

  /** Singleton */
  private static MAWindowContext instance = new MAWindowContext();

  /** Set of {@link MASysWindow} */
  private final Set<MASysWindow> fenetresWindow = new HashSet<MASysWindow>();

  private MAWindowContext() {
    MLog.debug("MAHSFenetre", "MAHSFenetre",
        "Chargement de la liste des noms de fenetres specifiques");
    // Chargement des fenetres specifiques
    loadSpecificWindows();
  }

  private void loadSpecificWindows() {
    for (MEASpecificWindow specificWindow : MEASpecificWindow.values()) {
      fenetresWindow.add(new MASysWindow(specificWindow.getSysName(),
          specificWindow.getClazz()));
    }
  }

  /**
   * Ajouter une fenetre a la liste.
   * 
   * @param sNomSys nom systeme de la fenetre
   */
  public void addFenetre(String sNomSys) {
    if (sNomSys != null) {
      if (instance.fenetresWindow.contains(sNomSys)) {
        MLog.error("MAHSFenetre", "addFenetre", "La fenetre " + sNomSys
            + "est deja listee !");
      } else {
        instance.fenetresWindow.add(new MASysWindow(sNomSys));
      }
    }
  }

  /**
   * Retourner la liste des noms systeme des fenetres.
   * 
   * @return noms systeme des fenetres
   */
  public MASysWindow[] values() {
    // Construction d'un tableau contenant les fenetres
    final Iterator<MASysWindow> iFenSys = instance.fenetresWindow.iterator();
    final List<MASysWindow> vFenWindow = new ArrayList<MASysWindow>();
    while (iFenSys.hasNext()) {
      vFenWindow.add(iFenSys.next());
    }

    final MASysWindow[] mafFenSys = new MASysWindow[vFenWindow.size()];
    vFenWindow.toArray(mafFenSys);

    return mafFenSys;
  }

  /**
   * Retrouver la fenetre qui correspondant au nom systeme.
   * 
   * @param sNomSys Nom systeme de la fenetre
   * @return fenetre
   */
  public MASysWindow getSysWindowByWindowName(String sNomSys) {
    // Parcours des fenetres jusqu'a trouver celle qui a ce nom SYSTEME.
    Iterator<MASysWindow> iFenSys = instance.fenetresWindow.iterator();
    while (iFenSys.hasNext()) {
      MASysWindow maFenWindow = iFenSys.next();
      if (maFenWindow.getSysName().equals(sNomSys)) {
        return maFenWindow;
      }
    }

    MLog.error("MAHSFenetre", "getFenSysParNomSys",
        "Aucune fenetre trouvee pour \"" + sNomSys + '"');

    return null;
  }

  /**
   * Get system window given a Frame.
   * 
   * @param frame a frame
   * @return the system window if any, <code>null</code> otherwise
   */
  public MASysWindow getSystemWindowByFrame(MJFrame frame) {
    // Parcours des fenetres jusqu'a trouver celle qui a ce nom SYSTEME.
    Iterator<MASysWindow> iFenSys = fenetresWindow.iterator();
    while (iFenSys.hasNext()) {
      MASysWindow maFenWindow = iFenSys.next();
      if (maFenWindow.getWindow() == frame) {
        return maFenWindow;
      }
    }

    MLog.error("MAHSFenetre", "getFenSysParNomSys",
        "Aucune fenetre trouvee pour \"" + frame + '"');

    return null;
  }

  /**
   * @return instance of Window Context
   */
  public static MAWindowContext getInstance() {
    return instance;
  }

  /**
   * @return the Set of system windows
   */
  public Set<MASysWindow> getFenetresWindow() {
    return fenetresWindow;
  }

}
