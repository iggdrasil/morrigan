/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAPreferenceContext.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context;

import morrigan.graphique.MModele;

/**
 * User preferences :
 * <li>date format
 * <li>... These preferences must be changed live.
 * 
 * @author Siltaom
 */
public class MAPreferenceContext {

  private static String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";

  private static String DEFAULT_HOUR_FORMAT = "hh:mm:ss";

  /** Unique instance */
  static private MAPreferenceContext instance = new MAPreferenceContext();

  private String dateFormat = null;

  private String hourFormat = null;

  /** Hidden constructor */
  private MAPreferenceContext() {
    setDateFormat(DEFAULT_DATE_FORMAT);
    setHourFormat(DEFAULT_HOUR_FORMAT);
  }

  /**
   * @return the date format
   */
  public String getDateFormat() {
    return dateFormat;
  }

  /**
   * @param dateFormat the dateFormat to set
   */
  public void setDateFormat(String dateFormat) {
    if (dateFormat == null || dateFormat.length() == 0) {
      this.dateFormat = DEFAULT_DATE_FORMAT;
    } else {
      this.dateFormat = dateFormat;
    }

    MModele.setFormatDate(dateFormat);
    // TODO : spread the change
  }

  /**
   * @return the date format
   */
  public String getHourFormat() {
    return hourFormat;
  }

  /**
   * @param dateFormat the dateFormat to set
   */
  public void setHourFormat(String hourFormat) {
    if (hourFormat == null || hourFormat.length() == 0) {
      this.hourFormat = DEFAULT_HOUR_FORMAT;
    } else {
      this.hourFormat = hourFormat;
    }

    MModele.setFormatHour(hourFormat);
    // TODO : spread the change
  }

  /**
   * @return the only one instance
   */
  public static MAPreferenceContext getInstance() {
    return instance;
  }
}
