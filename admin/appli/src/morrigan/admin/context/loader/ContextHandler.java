/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextHandler.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.loader.model.Authentication;
import morrigan.admin.context.loader.model.Connection;
import morrigan.admin.context.loader.model.Context;
import morrigan.admin.context.loader.model.ContextVisitor;
import morrigan.admin.context.loader.model.DatabaseType;
import morrigan.admin.context.loader.model.Dia;
import morrigan.admin.context.loader.model.Dictionary;
import morrigan.admin.context.loader.model.GlobalContext;
import morrigan.admin.context.loader.model.Graphic;
import morrigan.admin.context.loader.model.Log;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class ContextHandler extends DefaultHandler {

  /** Being built elements */
  private Context context = null;

  private Connection connection = null;

  private DatabaseType databasetype = null;

  private Authentication authentication = null;

  private Dia dia = null;

  private Dictionary dictionary = null;

  private Graphic graphic = null;

  private Log log = null;

  protected String tempVal = null;

  private GlobalContext globalContext = new GlobalContext();

  ContextHandler() {
    super();
  }

  @Override
  public void startElement(String uri, String localName, String qName,
      Attributes attributes) throws SAXException {

    if (qName.equalsIgnoreCase("init-context")) {
      globalContext.setCurrentContextName(attributes.getValue("ref"));
    } else if (qName.equalsIgnoreCase("context")) {
      String name = attributes.getValue("name");
      globalContext.add(context = new Context(name));
    } else if (context == null) {
      // If context is not open.
      if (qName.equalsIgnoreCase("connection")) {
        String name = attributes.getValue("name");
        globalContext.add(connection = new Connection(name));
      } else if (qName.equalsIgnoreCase("auth")) {
        String name = attributes.getValue("name");
        globalContext.add(authentication = new Authentication(name));
      } else if (qName.equalsIgnoreCase("databasetype")) {
        String name = attributes.getValue("name");
        globalContext.add(databasetype = new DatabaseType(name));
      } else if (qName.equalsIgnoreCase("dia")) {
        String name = attributes.getValue("name");
        globalContext.add(dia = new Dia(name));
      } else if (qName.equalsIgnoreCase("dictionary")) {
        String name = attributes.getValue("name");
        globalContext.add(dictionary = new Dictionary(name));
      } else if (qName.equalsIgnoreCase("graphic")) {
        String name = attributes.getValue("name");
        globalContext.add(graphic = new Graphic(name));
      } else if (qName.equalsIgnoreCase("log")) {
        String name = attributes.getValue("name");
        globalContext.add(log = new Log(name));
      }

    } else if (context != null) {
      // If context open, just names
      String name = attributes.getValue("ref");
      if (qName.equalsIgnoreCase("connection")) {
        context.setConnectionName(name);
      } else if (qName.equalsIgnoreCase("auth")) {
        context.setAuthenticationName(name);
      } else if (qName.equalsIgnoreCase("dia")) {
        context.setDiaName(name);
      } else if (qName.equalsIgnoreCase("dictionary")) {
        context.setDictionaryName(name);
      } else if (qName.equalsIgnoreCase("graphic")) {
        context.setGraphicName(name);
      } else if (qName.equalsIgnoreCase("log")) {
        context.setLogName(name);
      }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    tempVal = new String(ch, start, length);
  }

  @Override
  public void endElement(String uri, String localName, String qName)
      throws SAXException {
    if (qName.equalsIgnoreCase("context")) {
      context = null;
    } else if (connection != null) {
      if (qName.equalsIgnoreCase("connection")) {
        connection = null;
      } else if (qName.equalsIgnoreCase("databasetype")) {
        connection.setDatabaseType(tempVal);
      } else if (qName.equalsIgnoreCase("ip")) {
        connection.setIp(tempVal);
      } else if (qName.equalsIgnoreCase("port")) {
        connection.setPort(tempVal);
      } else if (qName.equalsIgnoreCase("database")) {
        connection.setDatabaseName(tempVal);
      } else if (qName.equalsIgnoreCase("user")) {
        connection.setUser(tempVal);
      } else if (qName.equalsIgnoreCase("password")) {
        connection.setPassword(tempVal);
      }
    } else if (authentication != null) {
      if (qName.equalsIgnoreCase("auth")) {
        authentication = null;
      } else if (qName.equalsIgnoreCase("url")) {
        authentication.setAddress(tempVal);
      }
    } else if (databasetype != null) {
      if (qName.equalsIgnoreCase("databasetype")) {
        databasetype = null;
      } else if (qName.equalsIgnoreCase("database")) {
        databasetype.setDatabase(tempVal);
      } else if (qName.equalsIgnoreCase("driver")) {
        databasetype.setDriverClassName(tempVal);
      }
    } else if (dia != null) {
      if (qName.equalsIgnoreCase("dia")) {
        dia = null;
      } else if (qName.equalsIgnoreCase("file")) {
        dia.setFile(tempVal);
      } else if (qName.equalsIgnoreCase("complement")) {
        dia.setComplement(tempVal);
      }
    } else if (dictionary != null) {
      if (qName.equalsIgnoreCase("dictionary")) {
        dictionary = null;
      } else if (qName.equalsIgnoreCase("file")) {
        dictionary.setFile(tempVal);
      } else if (qName.equalsIgnoreCase("language")) {
        dictionary.setLanguage(tempVal);
      }
    } else if (graphic != null) {
      if (qName.equalsIgnoreCase("graphic")) {
        graphic = null;
      } else if (qName.equalsIgnoreCase("ui-file")) {
        graphic.setFile(tempVal);
      } else if (qName.equalsIgnoreCase("splash-screen")) {
        graphic.setSpashScreenFile(tempVal);
      } else if (qName.equalsIgnoreCase("menu-file")) {
        graphic.setMenuFile(tempVal);
      }
    } else if (log != null) {
      if (qName.equalsIgnoreCase("log")) {
        log = null;
      } else if (qName.equalsIgnoreCase("directory")) {
        log.setDirectory(tempVal);
      } else if (qName.equalsIgnoreCase("level")) {
        log.setLevel(tempVal);
      }
    }
  }

  @Override
  public void endDocument() throws SAXException {
    // Stock the different contexts
    MAContextManager.getInstance().setGlobalContext(globalContext);

    // Load the default one.
    ContextVisitor contextVisitor = new ContextVisitor(globalContext
        .getCurrentContextName());
    globalContext.accept(contextVisitor);
  }
}