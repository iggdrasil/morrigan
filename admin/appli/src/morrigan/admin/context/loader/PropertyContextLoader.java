/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : PropertyContextLoader.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import morrigan.FileManager;
import morrigan.admin.context.MAContextManager;

/**
 * A context loader depending on a property file.
 * 
 * @author armel
 */
public class PropertyContextLoader implements ContextLoader {

  public void loadContext() {
    String contextFileName = System.getProperty("context.file",
        "ressources/misc/deployment.properties");
    loadContext(contextFileName);
  }

  public void loadContext(String filePath) {
    Properties properties = new Properties();
    try {
      InputStream stream = new FileManager().openFileToStream(filePath);
      properties.load(stream);
    } catch (IOException e) {
      throw new RuntimeException(
          "Impossible to load the context property file", e);
    }

    String type = properties.getProperty("@DATABASE.TYPE@");
    String ip = properties.getProperty("@DATABASE.IP@");
    String port = properties.getProperty("@DATABASE.PORT@");
    String name = properties.getProperty("@DATABASE.NAME@");
    String login = properties.getProperty("@DATABASE.USER@");
    String password = properties.getProperty("@DATABASE.PASSWORD@");

    MAContextManager.getInstance().getDbContext().setUserName(login);
    MAContextManager.getInstance().getDbContext().setPassword(password);
    MAContextManager.getInstance().getDbContext().setType(type);
    MAContextManager.getInstance().getDbContext().setIp(ip);
    MAContextManager.getInstance().getDbContext().setPort(port);
    MAContextManager.getInstance().getDbContext().setDatabaseName(name);
  }

}
