/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : DatabaseType.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public class DatabaseType extends ContextElement {

  private String driverClassName;

  private String database;

  /**
   * @return the database
   */
  public String getDatabase() {
    return database;
  }

  /**
   * @param database the database to set
   */
  public void setDatabase(String database) {
    this.database = database;
  }

  public DatabaseType(String name) {
    super(name);
  }

  public void setDriverClassName(String driverClassName) {
    this.driverClassName = driverClassName;
  }

  public String getDriverClassName() {
    return driverClassName;
  }

  /**
   * @param visitor
   */
  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }
}
