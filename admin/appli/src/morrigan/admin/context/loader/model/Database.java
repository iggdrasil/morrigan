package morrigan.admin.context.loader.model;

import java.util.Map;

public class Database {

  private Map<String, Connection> connections;

  private Map<String, DatabaseType> databaseTypes;

  public Database() {}

  public void addConnection(Connection connection) {
    connections.put(connection.getName(), connection);
  }

  public Map<String, Connection> getConnections() {
    return connections;
  }

  public void addDatabaseType(DatabaseType databaseType) {
    databaseTypes.put(databaseType.getName(), databaseType);
  }

  public Map<String, DatabaseType> getDatabaseTypes() {
    return databaseTypes;
  }
}
