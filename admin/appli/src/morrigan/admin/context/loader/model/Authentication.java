package morrigan.admin.context.loader.model;

public class Authentication extends ContextElement {

  private String address;

  public Authentication(String name) {
    super(name);
  }

  /**
   * @return the address
   */
  public String getAddress() {
    return address;
  }

  /**
   * @param address the address to set
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * @param visitor
   */
  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }
}
