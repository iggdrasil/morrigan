/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Context.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public class Context extends ContextElement {

  private String diaName;

  private String dictionaryName;

  private String graphicName;

  private String logName;

  private String connectionName;

  private String authenticationName;

  /**
   * Constructor.
   * 
   * @param name
   */
  public Context(String name) {
    super(name);
  }

  /**
   * @return the logName
   */
  public String getLogName() {
    return logName;
  }

  /**
   * @param logName the logName to set
   */
  public void setLogName(String logName) {
    this.logName = logName;
  }

  /**
   * @return the diaName
   */
  public String getDiaName() {
    return diaName;
  }

  /**
   * @param diaName the diaName to set
   */
  public void setDiaName(String diaName) {
    this.diaName = diaName;
  }

  /**
   * @return the dictionaryName
   */
  public String getDictionaryName() {
    return dictionaryName;
  }

  /**
   * @param dictionaryName the dictionaryName to set
   */
  public void setDictionaryName(String dictionaryName) {
    this.dictionaryName = dictionaryName;
  }

  /**
   * @return the graphicName
   */
  public String getGraphicName() {
    return graphicName;
  }

  /**
   * @param graphicName the graphicName to set
   */
  public void setGraphicName(String graphicName) {
    this.graphicName = graphicName;
  }

  /**
   * @return the connectionName
   */
  public String getConnectionName() {
    return connectionName;
  }

  /**
   * @param connectionName the connectionName to set
   */
  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  /**
   * @return the authenticationName
   */
  public String getAuthenticationName() {
    return authenticationName;
  }

  /**
   * @param authenticationName the authenticationName to set
   */
  public void setAuthenticationName(String authenticationName) {
    this.authenticationName = authenticationName;
  }

  /**
   * @return <code>true</code> if an authentication is needed;
   *         <code>false</code> otherwise
   */
  public boolean isAuthenticationNeeded() {
    return authenticationName != null;
  }
}
