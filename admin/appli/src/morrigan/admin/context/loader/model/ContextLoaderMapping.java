/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextLoaderMapping.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

import java.util.HashMap;
import java.util.Map;

public class ContextLoaderMapping {

  private Map<String, Class<?>> mapping;

  public ContextLoaderMapping() {
    mapping = new HashMap<String, Class<?>>();

    mapping.put("context", Context.class);
  }
}
