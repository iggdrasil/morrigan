/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Connection.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public class Connection extends ContextElement {

  private String databaseType;

  private String ip;

  private String port;

  private String databaseName;

  private String user;

  private String password;

  public Connection(String name) {
    super(name);
  }

  /**
   * @return the databaseType
   */
  public String getDatabaseType() {
    return databaseType;
  }

  /**
   * @param databaseType the databaseType to set
   */
  public void setDatabaseType(String typedebase) {
    this.databaseType = typedebase;
  }

  /**
   * @return the ip
   */
  public String getIp() {
    return ip;
  }

  /**
   * @param ip the ip to set
   */
  public void setIp(String ip) {
    this.ip = ip;
  }

  /**
   * @return the port
   */
  public String getPort() {
    return port;
  }

  /**
   * @param port the port to set
   */
  public void setPort(String port) {
    this.port = port;
  }

  /**
   * @return the databaseName
   */
  public String getDatabaseName() {
    return databaseName;
  }

  /**
   * @param databaseName the databaseName to set
   */
  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  /**
   * @return the user
   */
  public String getUser() {
    return user;
  }

  /**
   * @param user the user to set
   */
  public void setUser(String userName) {
    this.user = userName;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @param visitor
   */
  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }

}
