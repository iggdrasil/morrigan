/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Dia.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public class Dia extends ContextElement {

  private String file;

  private String complement;

  public Dia(String name) {
    super(name);
  }

  /**
   * @return the file
   */
  public String getFile() {
    return file;
  }

  /**
   * @param file the file to set
   */
  public void setFile(String file) {
    this.file = file;
  }

  /**
   * @return the complement
   */
  public String getComplement() {
    return complement;
  }

  /**
   * @param complement the complement to set
   */
  public void setComplement(String complement) {
    this.complement = complement;
  }

  /**
   * @param visitor
   */
  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }

}
