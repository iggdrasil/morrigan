/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : GlobalContext.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

import java.util.HashMap;
import java.util.Map;

public class GlobalContext {

  private Map<String, Context> contexts = new HashMap<String, Context>();

  private Map<String, Authentication> authentications = new HashMap<String, Authentication>();

  private Map<String, Log> logs = new HashMap<String, Log>();

  private Map<String, Graphic> graphics = new HashMap<String, Graphic>();

  private Map<String, Dictionary> dictionaries = new HashMap<String, Dictionary>();

  private Map<String, Dia> dias = new HashMap<String, Dia>();

  private Map<String, Connection> connections = new HashMap<String, Connection>();

  private Map<String, DatabaseType> databaseTypes = new HashMap<String, DatabaseType>();

  private String currentContextName = null;

  /**
   * Default constructor.
   */
  public GlobalContext() {}

  public void add(ContextElement contextElement) {
    if (contextElement instanceof Context) {
      contexts.put(contextElement.getName(), (Context) contextElement);
    } else if (contextElement instanceof Authentication) {
      authentications.put(contextElement.getName(),
          (Authentication) contextElement);
    } else if (contextElement instanceof Log) {
      logs.put(contextElement.getName(), (Log) contextElement);
    } else if (contextElement instanceof Graphic) {
      graphics.put(contextElement.getName(), (Graphic) contextElement);
    } else if (contextElement instanceof Dictionary) {
      dictionaries.put(contextElement.getName(), (Dictionary) contextElement);
    } else if (contextElement instanceof Dia) {
      dias.put(contextElement.getName(), (Dia) contextElement);
    } else if (contextElement instanceof Connection) {
      connections.put(contextElement.getName(), (Connection) contextElement);
    } else if (contextElement instanceof DatabaseType) {
      databaseTypes
          .put(contextElement.getName(), (DatabaseType) contextElement);
    }
  }

  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }

  public Map<String, Context> getContexts() {
    return contexts;
  }

  /**
   * @return the authentications
   */
  public Map<String, Authentication> getAuthentications() {
    return authentications;
  }

  /**
   * @return the logs
   */
  public Map<String, Log> getLogs() {
    return logs;
  }

  /**
   * @return the graphics
   */
  public Map<String, Graphic> getGraphics() {
    return graphics;
  }

  /**
   * @return the dictionaries
   */
  public Map<String, Dictionary> getDictionaries() {
    return dictionaries;
  }

  /**
   * @return the dias
   */
  public Map<String, Dia> getDias() {
    return dias;
  }

  /**
   * @return the connections
   */
  public Map<String, Connection> getConnections() {
    return connections;
  }

  /**
   * @return the database types
   */
  public Map<String, DatabaseType> getDatabaseTypes() {
    return databaseTypes;
  }

  /**
   * @return the currentContextName
   */
  public String getCurrentContextName() {
    return currentContextName;
  }

  /**
   * @param currentContextName the currentContextName to set
   */
  public void setCurrentContextName(String currentContextName) {
    this.currentContextName = currentContextName;
  }

}
