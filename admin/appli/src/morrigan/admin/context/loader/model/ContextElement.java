/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextElement.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public abstract class ContextElement {

  private final String name;

  public ContextElement(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return getClass().getName() + " - name=" + name;
  }

}
