/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextVisitor.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

import java.io.File;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.context.MAContextManager.GraphicContext;
import morrigan.admin.context.MAContextManager.SchemaContext;

import org.apache.commons.lang.Validate;

public class ContextVisitor {

  private final String contextName;

  private String databasetypeName = null;

  public ContextVisitor(String contextName) {
    this.contextName = contextName;
  }

  public void visit(GlobalContext globalContext) {
    Context context = globalContext.getContexts().get(contextName);
    check(context, contextName, "context");

    // Dia
    String diaName = context.getDiaName();
    check(diaName, "dia");
    Dia dia = globalContext.getDias().get(diaName);
    check(dia, diaName, "dia");
    dia.accept(this);

    // Authentication OR dataBase direct access
    String authenticationName = context.getAuthenticationName();
    if (authenticationName != null) {
      Authentication authentication = globalContext.getAuthentications().get(
          authenticationName);
      check(authentication, authenticationName, "authentication");
      authentication.accept(this);
    } else {
      String connectionName = context.getConnectionName();
      check(connectionName, "connection");
      Connection connection = globalContext.getConnections()
          .get(connectionName);
      check(connection, connectionName, "connection");
      connection.accept(this);
      DatabaseType databaseType = globalContext.getDatabaseTypes().get(
          databasetypeName);
      check(databaseType, databasetypeName, "databaseType");
      databaseType.accept(this);
    }

    // Dictionary
    String dictionaryName = context.getDictionaryName();
    check(dictionaryName, "dictionary");
    Dictionary dictionary = globalContext.getDictionaries().get(dictionaryName);
    check(dictionary, dictionaryName, "dictionary");
    dictionary.accept(this);

    // Graphic
    String graphicName = context.getGraphicName();
    check(graphicName, "graphic");
    Graphic graphic = globalContext.getGraphics().get(graphicName);
    check(graphic, graphicName, "graphic");
    graphic.accept(this);

    // Log
    String logName = context.getLogName();
    check(logName, "log");
    Log log = globalContext.getLogs().get(logName);
    check(log, logName, "log");
    log.accept(this);
  }

  /**
   * Check the string is not null.
   * 
   * @param stringToTest
   * @param label
   * @throws IllegalArgumentException if the string is null
   */
  private void check(String stringToTest, String label) {
    Validate.notNull(stringToTest, "The " + label
        + " name (in context description) " + "is not specified");
  }

  /**
   * Check the context element is not null.
   * 
   * @param contextElement
   * @param elementName
   * @param label
   * @throws IllegalArgumentException if the context element is null
   */
  private void check(ContextElement contextElement, String elementName,
      String label) throws IllegalArgumentException {
    Validate.notNull(contextElement, "The " + label
        + " name (asked by context description) " + "is not described : "
        + elementName);
  }

  public void visit(Connection connection) {
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    dbContext.setDatabaseName(connection.getDatabaseName());
    dbContext.setPassword(connection.getPassword());
    dbContext.setIp(connection.getIp());
    dbContext.setPort(connection.getPort());
    dbContext.setUserName(connection.getUser());

    databasetypeName = connection.getDatabaseType();
  }

  public void visit(DatabaseType databaseType) {
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    dbContext.setType(databaseType.getDatabase());
    dbContext.setDriverClassName(databaseType.getDriverClassName());
  }

  public void visit(Dia dia) {
    SchemaContext schemaContext = MAContextManager.getInstance()
        .getSchemaContext();
    schemaContext.setDiaFile(new File("resources/conf/morrigan-conf.xml"));
    schemaContext.setTableDescriptionFile(new File(dia.getComplement()));

    String extractedDiaFile = System.getProperty("java.io.tmpdir")
        + File.separatorChar + "schema.dia.xml";
    schemaContext.setXmlDiaFile(new File(extractedDiaFile));
  }

  public void visit(Log log) {
    log.getDirectory();
    log.getLevel();
  }

  public void visit(Graphic graphic) {
    GraphicContext graphicContext = MAContextManager.getInstance()
        .getGraphicContext();
    graphicContext.setMenuFile(graphic.getMenuFile());
    graphicContext.setConfFile(graphic.getFile());
    graphicContext.setSplashScreenImage(graphic.getSpashScreenFile());
  }

  public void visit(Dictionary dictionary) {
    MAContextManager.getInstance().getDictionaryContext().setDicoFile(
        dictionary.getFile());
    // dictionary.getLanguage();
  }

  public void visit(Authentication authentication) {
    MAContextManager.getInstance().getDbContext().setAuthenticationAddress(
        authentication.getAddress());
  }

}
