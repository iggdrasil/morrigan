/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : Graphic.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader.model;

public class Graphic extends ContextElement {

  private String file;

  private String spashScreenFile;

  private String menuFile;

  public Graphic(String name) {
    super(name);
  }

  /**
   * @return the file
   */
  public String getFile() {
    return file;
  }

  /**
   * @param file the file to set
   */
  public void setFile(String file) {
    this.file = file;
  }

  /**
   * @return the spashScreenFile
   */
  public String getSpashScreenFile() {
    return spashScreenFile;
  }

  /**
   * @param spashScreenFile the spashScreenFile to set
   */
  public void setSpashScreenFile(String spashScreenFile) {
    this.spashScreenFile = spashScreenFile;
  }

  /**
   * @return the menuFile
   */
  public String getMenuFile() {
    return menuFile;
  }

  /**
   * @param menuFile the menuFile to set
   */
  public void setMenuFile(String menuFile) {
    this.menuFile = menuFile;
  }

  /**
   * @param visitor
   */
  public void accept(ContextVisitor visitor) {
    visitor.visit(this);
  }

}
