/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextLoader.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * A context loader is a class that look in a file for context information.
 * 
 * @author armel
 */
public interface ContextLoader {

  /**
   * Load configuration in default file.
   * 
   * @throws FileNotFoundException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public abstract void loadContext() throws FileNotFoundException,
      ParserConfigurationException, SAXException, IOException;

  /**
   * Load configuration from a specific file
   * 
   * @param filePath context file path
   */
  public abstract void loadContext(String filePath);

}