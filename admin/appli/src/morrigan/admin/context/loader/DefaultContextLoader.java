/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : DefaultContextLoader.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import morrigan.FileManager;

import org.xml.sax.SAXException;

/**
 * A context loader from an XML file.
 * 
 * @author armel
 */
public class DefaultContextLoader implements ContextLoader {

  /**
   * {@inheritDoc}
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public void loadContext() throws ParserConfigurationException, SAXException,
      IOException {
    InputStream inputStream = new FileManager()
        .openFileToStream("ressources/conf/morrigan-conf.xml");

    SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();

    ContextHandler contextHandler = new ContextHandler();
    saxParser.parse(inputStream, contextHandler);

  }

  /** {@inheritDoc} */
  public void loadContext(String filePath) {}

}
