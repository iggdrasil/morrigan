/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : ContextLoaderFactory.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context.loader;

/**
 * @author armel
 */
public class ContextLoaderFactory {

  public ContextLoaderFactory() {}

  public ContextLoader createContextLoader(boolean jnlp) {
    return jnlp ? new DefaultContextLoader() : new PropertyContextLoader();
  }
}
