/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MADbContext.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import morrigan.admin.database.MALightTable;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;

import org.apache.commons.lang.Validate;

public class MADbContext {

  /** Light table referenced by name */
  private Map<String, MALightTable> lightTables = new HashMap<String, MALightTable>();

  /**
   * Tables (linked to the SQL, with attributes, operations, ...) listed in the
   * application, referenced by name.
   */
  private Map<String, Table> tables = null;

  /** Same as tables but referenced by trigramme. */
  private Map<String, Table> tablesByTrig = new HashMap<String, Table>();

  /** A Map<foreignKeyFieldName, primaryKeyFieldName> : associations */
  private Map<String, String> foreignToPrimaryKeyLinks = new HashMap<String, String>();

  private static final MADbContext instance = new MADbContext();

  private Properties tableDescProperties;

  private String databaseName;

  private String password;

  private String port;

  private String userName;

  private String driverClassName;

  private String ip;

  private String authenticationAddress;

  private String type;

  private Map<String, Set<String>> primaryToForeignKeysLinks;

  private MADbContext() {
  }

  public static MADbContext getInstance() {
    return instance;
  }

  /**
   * Recuperer les infos de la table a partir du trigramme.
   * 
   * @param sTrigReq trigramme a rechercher
   * @return Infos sur la table
   */
  public MALightTable getLightTableParTrigramme(String sTrigReq) {
    final Set<String> sNom = lightTables.keySet();
    final Iterator<String> iNom = sNom.iterator();
    while (iNom.hasNext()) {
      String sNomTable = iNom.next();
      String sTrig = lightTables.get(sNomTable).getTrig();
      if (sTrig.equals(sTrigReq)) {
        return lightTables.get(sNomTable);
      }
    }

    throw new RuntimeException(
        "Aucune 'light table' trouvee pour le trigramme \"" + sTrigReq + '"');
  }

  /**
   * Recuperer la structure de la table a partir du trigramme.
   * 
   * @param trigramme trigramme a rechercher
   * @return Infos sur la table
   */
  public Table getTableParTrigramme(String trigramme) {
    Table table = tablesByTrig.get(trigramme);
    if (table == null) {
      table = tables.get(getLightTableParTrigramme(trigramme).getNom());
      tablesByTrig.put(trigramme, table);
    }

    return tablesByTrig.get(trigramme);
  }

  /**
   * Recuperer les infos de la table a partir de son nom.
   * 
   * @param nomTable Nom de la table
   * @return Infos sur la table
   */
  public MALightTable getLightTableParNom(String nomTable) {
    return lightTables.get(nomTable);
  }

  public Table getTableParNom(String nomTable) {
    return tables.get(nomTable);
  }

  public void setLightTables(Map<String, MALightTable> lightTables) {
    this.lightTables = lightTables;
  }

  /**
   * Return a Map<foreignKeyFieldName, primaryKeyFieldName> : associations
   * @return the foreignPrimaryKeysLinks (unmodifiable)
   */
  public Map<String, String> getForeignToPrimaryKeyLinks() {
    return foreignToPrimaryKeyLinks == null ? null : Collections
        .unmodifiableMap(foreignToPrimaryKeyLinks);
  }

  /**
   * @param foreignPrimaryKeysLinks the foreignPrimaryKeysLinks to set
   */
  public void setForeignToPrimaryKeyLinks(
      Map<String, String> foreignPrimaryKeysLinks) {
    this.foreignToPrimaryKeyLinks = foreignPrimaryKeysLinks;
  }

  public Map<String, Table> getTables() {
    return tables == null ? null : Collections.unmodifiableMap(tables);
  }

  /**
   * @param tablesMap a map of table name -> table
   */
  public void setTables(Map<String, Table> tables) {
    this.tables = tables;
  }

  /**
   * <lu> <li>jdbc:postgresql://localhost:5432/maBase</li> <li>
   * jdbc:hsqldb:file:monFichierDeBase</li> <li>jdbc:hsqldb:mem:maBase</li> <li>
   * jdbc:odbc:maBase;CacheSize=30;ExtensionCase=LOWER</li> <li>
   * jdbc:mysql://localhost/maBase <li>jdbc:oracle:oci8@:maBase</li> <li>
   * jdbc:oracle:thin@://localhost:8000:maBase</li> <li>
   * jdbc:sybase:Tds:localhost:5020/maBase</li> </lu>
   * 
   * @return
   */
  public String getDatabaseUrl() {
    // Building url (such as jdbc:postgresql://192.168.0.51:5432/morrigan)
    // Or jdbc:hsqldb:file:resources/hsqldb/montest
    String urlBdd = "jdbc:" + type + "://" + ip + ':' + port + '/'
        + databaseName;
    return urlBdd;
  }

  public void setTableDescProperties(Properties tableDescProperties) {
    this.tableDescProperties = tableDescProperties;
  }

  public Properties getTableDescProperties() {
    return tableDescProperties;
  }

  public Attribute getAttributeByName(String attributeName) {
    Validate.notNull(attributeName, "The attribute name must not be null");
    Table table = getTableParTrigramme(attributeName.substring(0, 3));
    return table.getAttribute(attributeName);
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setPort(String port) {
    this.port = port;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public void setDriverClassName(String driverClassName) {
    this.driverClassName = driverClassName;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  /**
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @return the driverClassName
   */
  public String getDriverClassName() {
    return driverClassName;
  }

  /**
   * @return the authenticationAddress
   */
  public String getAuthenticationAddress() {
    return authenticationAddress;
  }

  /**
   * @param authenticationAddress the authenticationAddress to set
   */
  public void setAuthenticationAddress(String authenticationAddress) {
    this.authenticationAddress = authenticationAddress;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the primaryToForeignKeysLinks
   */
  public Map<String, Set<String>> getPrimaryToForeignKeysLinks() {
    return Collections.unmodifiableMap(primaryToForeignKeysLinks);
  }

  /**
   * @param primaryToForeignKeysLinks the primaryToForeignKeysLinks to set
   */
  public void setPrimaryToForeignKeysLinks(
      Map<String, Set<String>> primaryToForeignKeysLinks) {
    this.primaryToForeignKeysLinks = primaryToForeignKeysLinks;
  }
}
