/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAContextManager.java - CREATION : 2008/09/12
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.context;

import java.io.File;

import morrigan.admin.context.loader.model.GlobalContext;

/**
 * Context manager.
 * 
 * @author Armel
 */
public final class MAContextManager {

  /** Schema (dia) context */
  private SchemaContext schemaContext = new SchemaContext();

  /** Graphical context */
  private GraphicContext graphicContext = new GraphicContext();

  /** Dictionary context */
  private DictionaryContext dictionaryContext = new DictionaryContext();

  /** The window context */
  private MAWindowContext windowContext = MAWindowContext.getInstance();

  /** The database context */
  private MADbContext dbContext = MADbContext.getInstance();

  /** The preferences context */
  private MAPreferenceContext preferenceContext = MAPreferenceContext
      .getInstance();

  /** Preferences file name */
  private String preferencesFileName = "morrigan-preferences.xml";

  /**
   * Preferences file (generated with the file name and the user home directory
   * according to the OS)
   */
  private String preferencesFile = null;

  /** The unique instance of this class */
  private static MAContextManager instance = new MAContextManager();

  private GlobalContext globalContext;

  private String currentContextName;

  /**
   * Hidden constructor.
   */
  private MAContextManager() {}

  /**
   * Get the unique instance of this class.
   * 
   * @return instance
   */
  public static MAContextManager getInstance() {
    return instance;
  }

  /**
   * @return the dbContext
   */
  public MADbContext getDbContext() {
    return this.dbContext;
  }

  /**
   * @return the preferenceContext
   */
  public MAPreferenceContext getPreferenceContext() {
    return this.preferenceContext;
  }

  /**
   * @return the windowContext
   */
  public MAWindowContext getWindowContext() {
    return this.windowContext;
  }

  /**
   * @return the preferencesFile
   */
  public String getPreferencesFile() {
    return this.preferencesFile;
  }

  /**
   * @param preferencesFile the preferencesFile to set
   */
  public void setPreferencesFile(String preferencesFile) {
    this.preferencesFile = preferencesFile;
  }

  /**
   * @param globalContext the globalContext to set
   */
  public void setGlobalContext(GlobalContext globalContext) {
    this.globalContext = globalContext;
  }

  /**
   * @param currentContextName the currentContextName to set
   */
  public void setCurrentContextName(String currentContextName) {
    this.currentContextName = currentContextName;
  }

  /**
   * @return the choosenContextName
   */
  public String getCurrentContextName() {
    return currentContextName;
  }

  /**
   * @return the preferencesFileName
   */
  public String getPreferencesFileName() {
    return preferencesFileName;
  }

  /**
   * @return the schema context
   */
  public SchemaContext getSchemaContext() {
    return schemaContext;
  }

  /**
   * @return the graphic context
   */
  public GraphicContext getGraphicContext() {
    return graphicContext;
  }

  /**
   * @return the graphic context
   */
  public DictionaryContext getDictionaryContext() {
    return dictionaryContext;
  }

  /**
   * @return the globalContext
   */
  public GlobalContext getGlobalContext() {
    return globalContext;
  }

  /**
   * @author armel
   */
  public class SchemaContext {

    /** The dia file. */
    private File diaFile = null;

    /** The XML file of the dia */
    private File xmlDiaFile = null;

    /** The table description file */
    private File tableDescriptionFile = null;

    /**
     * @return the diaFile
     */
    public File getDiaFile() {
      return this.diaFile;
    }

    /**
     * @param diaFile the diaFile to set
     */
    public void setDiaFile(File diaFile) {
      this.diaFile = diaFile;
    }

    /**
     * @return the xmlDiaFile
     */
    public File getXmlDiaFile() {
      return this.xmlDiaFile;
    }

    /**
     * @param xmlDiaFile the xmlDiaFile to set
     */
    public void setXmlDiaFile(File xmlDiaFile) {
      this.xmlDiaFile = xmlDiaFile;
    }

    /**
     * @return the table description file
     */
    public File getTableDescriptionFile() {
      return this.tableDescriptionFile;
    }

    /**
     * @param tableDescriptionFile the table description file to set
     */
    public void setTableDescriptionFile(File tableDescriptionFile) {
      this.tableDescriptionFile = tableDescriptionFile;
    }
  }

  /**
   * @author armel
   */
  public class GraphicContext {

    /** The splash screen image */
    private String splashScreenImage = null;

    /** The configuration file */
    private String confFile = null;

    /** The menu file */
    private String menuFile = null;

    /**
     * @return the confFile
     */
    public String getConfFile() {
      return this.confFile;
    }

    /**
     * @param confFile the confFile to set
     */
    public void setConfFile(String confFile) {
      this.confFile = confFile;
    }

    /**
     * @return the splashScreenImage
     */
    public String getSplashScreenImage() {
      return this.splashScreenImage;
    }

    /**
     * @param splashScreenImage the splash screen image to set
     */
    public void setSplashScreenImage(String splashScreenImage) {
      this.splashScreenImage = splashScreenImage;
    }

    /**
     * @return the menu file
     */
    public String getMenuFile() {
      return this.menuFile;
    }

    /**
     * @param menuFile the menu file to set
     */
    public void setMenuFile(String menuFile) {
      this.menuFile = menuFile;
    }
  }

  /**
   * @author
   */
  public class DictionaryContext {

    /** The dictionnary file */
    private String dicoFile = null;

    /** Language */
    private String language = null;

    /**
     * @return the dicoFile
     */
    public String getDicoFile() {
      return this.dicoFile;
    }

    /**
     * @param dicoFile the dicoFile to set
     */
    public void setDicoFile(String dicoFile) {
      this.dicoFile = dicoFile;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
      return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
      this.language = language;
    }

  }

}
