package morrigan.admin.exception;

/**
 * Window manipulation exception.
 * 
 * @author Armel
 */
public class WindowException extends Exception {

  public WindowException(String message) {
    super(message);
  }

  public WindowException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
