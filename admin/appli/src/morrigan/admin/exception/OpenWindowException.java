package morrigan.admin.exception;

public class OpenWindowException extends WindowException {

  public OpenWindowException(String message) {
    super(message);
  }

  public OpenWindowException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
