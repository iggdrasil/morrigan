package morrigan.admin.exception;

public class CreateWindowException extends WindowException {

  public CreateWindowException(String message) {
    super(message);
  }

  public CreateWindowException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
