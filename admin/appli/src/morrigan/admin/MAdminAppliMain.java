/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAdminAppli.java - CREATION : 2004/08/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin;

import java.awt.Point;
import java.io.IOException;
import java.util.HashMap;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MAPreferenceContext;
import morrigan.admin.database.DBManager;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.util.MPropertiesWriter;
import morrigan.common.launching.LauncherFactory;
import morrigan.common.launching.application.AdminInitializer;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.launcher.Launcher;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public abstract class MAdminAppliMain extends MJFramePosition {

  /** Application name */
  private static final String APPLICATION_NAME = "Administration de Morrigan";

  /** Version */
  private static final String VERSION = "v0.0.6";

  /**
   * Constructor
   */
  public MAdminAppliMain() {
    super(APPLICATION_NAME + " " + VERSION);
    imposerTaille(300, 48);
  }

  /**
   * Lancer l'application d'administration.
   * 
   * @param args Arguments
   */
  public static void main(String... args) {
    Initializer initializer = AdminInitializer.createAdminInitializer();
    Launcher launcher = LauncherFactory.createLauncher();
    launcher.launchInitializer(initializer);
  }

  /**
   * Get the application name.
   * 
   * @return the application name
   */
  public static String getApplicationName() {
    return APPLICATION_NAME;
  }

  /**
   * Get the application version.
   * 
   * @return the version
   */
  public static String getApplicationVersion() {
    return VERSION;
  }

  /**
   * Quitte l'application. Les parametres d'affichage sont sauvegardes.
   * 
   * @param iStatus statut de sortie
   */
  public static void quitter(int iStatus) {
    DBManager.fermerConnexion();
    savePreferences();
    MLog.infos("Fin (status:" + iStatus + ")");
    MLog.clore();
    System.exit(iStatus);
  }

  /**
   * Saves the preferences and the windows configuration.
   */
  public static void savePreferences() {
    try {
      String windowPrefFile = MAContextManager.getInstance()
          .getPreferencesFile();
      if (windowPrefFile != null) {
        MLog.infos("Sauvegarde de la configuration des fenetres ("
            + windowPrefFile + ")");
        MPropertiesWriter mpw = new MPropertiesWriter(windowPrefFile);

        mpw.openElement("references");
        mpw.writeElement("application", APPLICATION_NAME);
        mpw.writeElement("version", VERSION);
        mpw.closeElement("references");

        saveUserPreferences(mpw);
        saveWindowsPreferences(mpw);

        mpw.close();
      } else {
        MLog
            .error(
                "MAdminAppliMain",
                "savePreferences",
                "Sauvegarde de la configuration des fenetres impossible car pas de fichier specifie.");
      }
    } catch (Exception e) {
      MLog.error("MAdminAppliMain", "sauverParametres",
          "Erreur lors de la sauvegarde des parametres", e);
    }
  }

  /**
   * Sauvegarde dans un fichier externe les parametres d'affichage de toutes les
   * fenetres.
   * 
   * @param mpw flux de sauvegarde
   * @throws IOException if save fails
   */
  private static void saveWindowsPreferences(MPropertiesWriter mpw)
      throws IOException {
    mpw.openElement("fenetres");

    for (MASysWindow fenetre : MAContextManager.getInstance()
        .getWindowContext().values()) {
      saveWindowPreference(mpw, fenetre);
    }

    mpw.closeElement("fenetres");
  }

  /**
   * Enregistre les parametres d'affichage d'une fenetre.
   * 
   * @param mpw flux de sauvegarde
   * @param fenetre identifiant de la fenetre a traiter
   * @throws IOException if save fails
   */
  private static void saveWindowPreference(MPropertiesWriter mpw,
      MASysWindow fenetre) throws IOException {
    final String sNomFenetre = fenetre.getSysName();

    if (fenetre.getSavedPosition() == null && fenetre.getWindow() == null) {
      return;
    }

    // Recherche de la position sur l'ecran
    Point pPosition = null;
    if (fenetre.getWindow() == null) {
      pPosition = fenetre.getSavedPosition();
    } else if (fenetre.getWindow().isVisible()) {
      pPosition = fenetre.getWindow().getLocationOnScreen();
      if (pPosition.x < 0 || pPosition.y < 0) {
        pPosition = fenetre.getSavedPosition();
      }
    }

    if (pPosition == null) {
      return;
    }

    final HashMap<String, String> hmAtt = new HashMap<String, String>();
    hmAtt.put("nom", sNomFenetre);
    mpw.openElement("fenetre", hmAtt);
    mpw.writeElement("ouvert", Boolean.toString(fenetre.getWindow() != null
        && fenetre.getWindow().isVisible()));
    mpw.openElement("position");
    mpw.writeElement("x", (pPosition.x < 0 ? 0 : pPosition.x) + "");
    mpw.writeElement("y", (pPosition.y < 0 ? 0 : pPosition.y) + "");
    mpw.closeElement("position");
    mpw.closeElement("fenetre");
    if (fenetre.getWindow() != null) {
      fenetre.getWindow().setVisible(false);
      fenetre.getWindow().dispose();
    }
  }

  /**
   * Save preferences.
   * 
   * @param mpw writer
   */
  private static void saveUserPreferences(MPropertiesWriter mpw)
      throws IOException {
    MAPreferenceContext preferenceContext = MAContextManager.getInstance()
        .getPreferenceContext();

    mpw.openElement("preferences");
    mpw.writeElement("dateFormat", preferenceContext.getDateFormat());
    // ...
    mpw.closeElement("preferences");
  }
}