package morrigan.admin.workflow.bystep;

import java.util.LinkedList;

import morrigan.admin.MAWindowManager;
import morrigan.admin.exception.WindowException;
import morrigan.admin.ui.fenetre.MAFenetreWorkflow;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.util.MAUtil;

/**
 * @author Armel
 */
public class WorkflowProcess {

  private MAFenetreWorkflow fenetre = null;

  private LinkedList<Step> steps = new LinkedList<Step>();

  public void init() {
    loadDescriptiveFile();
  }

  private void loadDescriptiveFile() {
    // ...
    steps.add(new DefaultStep());
    // ...

    fenetre = new MAFenetreWorkflow("Workflow", 10, 10);
  }

  public void runWorkflow() throws WindowException {
    MAUtil.notNull(fenetre, "Workflow must be initialized first !");
    WorkflowContext context = new WorkflowContext();
    for (Step step : steps) {
      step.setContext(context);
      MASysWindow sysWindow = null;
      int x = 0;
      int y = 0;
      MAWindowManager.createWindow(sysWindow, x, y);
    }
  }
}
