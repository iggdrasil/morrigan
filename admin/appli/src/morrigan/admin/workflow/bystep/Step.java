package morrigan.admin.workflow.bystep;

/**
 * @author Armel
 */
public interface Step {

  /**
   * @param context
   */
  void setContext(WorkflowContext context);

}
