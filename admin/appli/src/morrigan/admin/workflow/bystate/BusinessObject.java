package morrigan.admin.workflow.bystate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Armel
 */
public class BusinessObject {

  private Map<String, Object> components = new HashMap<String, Object>();

  BusinessObject() {}

  void setValue(String componentName, Object value) {
    components.put(componentName, value);
  }

  Object getValue(String componentName) {
    return components.get(componentName);
  }

}
