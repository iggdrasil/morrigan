package morrigan.admin.workflow.bystate.state;

import morrigan.admin.workflow.bystate.WorkflowContext;

/**
 * @author Armel
 */
public class QuestionState extends State {

  /**
   * 
   */
  public QuestionState(WorkflowContext workflowContext) {
    super(workflowContext);
  }

  /** {@inheritDoc} */
  public void next() {
    getWorkflowContext().next();
  }
}
