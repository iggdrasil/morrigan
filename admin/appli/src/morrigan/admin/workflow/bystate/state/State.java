package morrigan.admin.workflow.bystate.state;

import morrigan.admin.workflow.bystate.WorkflowContext;

/**
 * @author Armel
 */
public abstract class State {

  private final WorkflowContext workflowContext;

  public State(WorkflowContext workflowContext) {
    this.workflowContext = workflowContext;
  }

  /**
   * @return the workflowContext
   */
  public WorkflowContext getWorkflowContext() {
    return this.workflowContext;
  }

  public void createObject(Object o) {}

  public void selectObject(int id) {}

  public void next() {}

  public void close() {
    workflowContext.cancel();
  }

  public void validate() {}
}
