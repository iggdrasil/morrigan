package morrigan.admin.workflow.bystate.state;

import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.workflow.bystate.WorkflowContext;

/**
 * @author Armel
 */
public class SelectionState extends State {

  private Table table = null;

  /**
   * @param workflowContext
   */
  public SelectionState(WorkflowContext workflowContext) {
    super(workflowContext);
  }

  /**
   * @param table the table to set
   */
  public void setTable(Table table) {
    this.table = table;
  }

  /** {@inheritDoc} */
  public void selectObject(int id) {
    getWorkflowContext().select(id);
  }
}
