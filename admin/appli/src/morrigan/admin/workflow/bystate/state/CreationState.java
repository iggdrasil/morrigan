package morrigan.admin.workflow.bystate.state;

import morrigan.admin.workflow.bystate.WorkflowContext;
import morrigan.graphique.table.MDonnees;

/**
 * @author Armel
 */
public class CreationState extends State {

  /**
   * @param workflowContext
   */
  public CreationState(WorkflowContext workflowContext) {
    super(workflowContext);
  }

  /** {@inheritDoc} */
  public void createObject(MDonnees model) {
    getWorkflowContext().createObject(model);
  }
}
