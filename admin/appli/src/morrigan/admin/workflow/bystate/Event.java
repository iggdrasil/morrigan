package morrigan.admin.workflow.bystate;

import morrigan.admin.util.MADonnees;

/**
 * @author Armel
 */
public abstract class Event {

  public enum EventType {
    CREATION,
    SELECTION
  }

  public abstract EventType getType();

  static public class CreationEvent extends Event {

    private MADonnees donnees;

    public CreationEvent(MADonnees donnees) {
      this.donnees = donnees;
    }

    /**
     * @return the donnees
     */
    public MADonnees getDonnees() {
      return this.donnees;
    }

    @Override
    public EventType getType() {
      return EventType.CREATION;
    }
  }

  static public class SelectionEvent extends Event {

    private int identifier;

    public SelectionEvent(int identifier) {
      this.identifier = identifier;
    }

    /**
     * @return the identifier
     */
    public int getIdentifier() {
      return this.identifier;
    }

    @Override
    public EventType getType() {
      return EventType.SELECTION;
    }
  }
}
