package morrigan.admin.workflow.bystate.test;

import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.workflow.bystate.WorkflowContext;
import morrigan.common.launching.initializer.InitException;
import morrigan.common.launching.step.LaunchingStep;

/**
 * @author Armel
 */
public class WorkflowLauncher implements LaunchingStep {

  /** {@inheritDoc} */
  public void consume() throws InitException {
    Table table = MADbContext.getInstance().getTableParNom("personnage");
    WorkflowContext workflowContext = new WorkflowContext(table);
    workflowContext.start();
  }

  /** {@inheritDoc} */
  public String getStepName() {
    return "Workflow launching";
  }
}
