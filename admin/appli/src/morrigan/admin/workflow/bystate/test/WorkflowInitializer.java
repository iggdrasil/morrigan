package morrigan.admin.workflow.bystate.test;

import java.util.List;

import morrigan.common.launching.application.AbstractInitializer;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.step.LaunchingStep;
import morrigan.common.launching.step.configuration.DescriptionsFileLoading;
import morrigan.common.launching.step.configuration.DiaFileLoading;
import morrigan.common.launching.step.configuration.FilesSynchronizationChecking;
import morrigan.common.launching.step.database.DataBaseConnectionStep;

/**
 * Administrator application initializer.
 * 
 * @author Armel
 */
public class WorkflowInitializer extends AbstractInitializer {

  /**
   * 
   */
  private static final WorkflowInitializer instance = new WorkflowInitializer();

  /**
   * Constructor used by factory.
   */
  private WorkflowInitializer() {
    super();
  }

  @Override
  protected void feedStepList(List<LaunchingStep> stepList) {
    stepList.add(new DiaFileLoading(true));
    stepList.add(new DescriptionsFileLoading());
    stepList.add(new FilesSynchronizationChecking());
    stepList.add(new DataBaseConnectionStep());
    stepList.add(new WorkflowLauncher());
  }

  /**
   * @return the administration application initializer.
   */
  static public Initializer getWorkflowInitializer() {
    return instance;
  }

}
