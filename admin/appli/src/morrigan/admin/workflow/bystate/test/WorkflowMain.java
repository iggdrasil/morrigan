package morrigan.admin.workflow.bystate.test;

import morrigan.common.launching.LauncherFactory;
import morrigan.common.launching.application.Initializer;
import morrigan.common.launching.launcher.Launcher;

/**
 * @author Armel
 */
public class WorkflowMain {

  public static void main(String[] args) {
    Initializer workflowInitializer = WorkflowInitializer
        .getWorkflowInitializer();
    Launcher launcher = LauncherFactory.createLauncher();
    launcher.launchInitializer(workflowInitializer);
  }
}
