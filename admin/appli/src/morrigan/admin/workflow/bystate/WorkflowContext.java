package morrigan.admin.workflow.bystate;

import java.util.LinkedList;

import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.ui.fenetre.MAFenetreWorkflow;
import morrigan.admin.workflow.bystate.state.CreationState;
import morrigan.admin.workflow.bystate.state.SelectionState;
import morrigan.admin.workflow.bystate.state.State;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.table.MDonnees;

/**
 * @author Armel
 */
public class WorkflowContext {

  private SelectionState selectionState;

  private CreationState creationState;

  private State currentState;

  private LinkedList<State> states = new LinkedList<State>();

  private MAFenetreWorkflow fenetre;

  private WorkflowGuiFactory guiFactory;

  private final Table table;

  /**
   * Constructor.
   */
  public WorkflowContext(Table table) {
    this.table = table;
    selectionState = new SelectionState(this);
    creationState = new CreationState(this);
    currentState = creationState;

    guiFactory = new WorkflowGuiFactory();
  }

  public void start() {
    fenetre = new MAFenetreWorkflow("Workflow", 10, 10);
  }

  public void switchToSelection() {
    currentState = selectionState;
  }

  public void switchToCreation() {
    currentState = creationState;
  }

  public void setState(State state) {
    MJPanel panel = guiFactory.createGui(state);
    fenetre.setCurrentPanel(!states.isEmpty(), false, panel);
  }

  public void previous() {
    this.currentState = states.getLast();
  }

  public void next() {}

  /**
   * 
   */
  public void cancel() {}

  /**
   * @param model
   */
  public void createObject(MDonnees model) {}

  /**
   * @param id
   */
  public void select(int id) {}

}
