/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAMenuGenerator.java - CREATION : 2005/11/03
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util;

import java.awt.Component;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JMenuItem;

import morrigan.FileManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.ui.MAJMenuItem;
import morrigan.admin.ui.enumeration.MEActionMenu;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.graphique.menu.MJMenu;
import morrigan.graphique.menu.MJMenuBar;
import morrigan.log.MLog;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * Menu generator.
 * 
 * @author Armel
 */
public final class MAMenuGenerator {

  /** Menu file name */
  private String fileName = null;

  /** Menu bar */
  private MJMenuBar mjbMenu = null;

  /** Unique instance */
  private static MAMenuGenerator maGenMenu = null;

  /**
   * Hidden constructor.
   * 
   * @param fileName the menu file name
   */
  private MAMenuGenerator(String fileName) {
    this.fileName = fileName;
  }

  /**
   * Load the menu from the specified file.
   * 
   * @param fileName menu file name
   * @throws Exception if the loading fails
   */
  public static void loadMenuFromFile(String fileName) throws Exception {
    maGenMenu = new MAMenuGenerator(fileName);
    maGenMenu.chargerMenu();
  }

  /**
   * @return the menu bar
   */
  public static MJMenuBar getMenuBar() {
    return maGenMenu.mjbMenu;
  }

  private void chargerMenu() throws Exception {
    InputStream isTables = null;
    Document doc = null;

    try {
      MLog.infos("Ouverture du fichier de menu " + fileName);
      isTables = new FileManager().openFileToStream(fileName);

      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(isTables);
      isTables.close();
      final Element eMenu = doc.getRootElement().getChild("menu");

      final MJMenuBar menuBar = new MJMenuBar();

      // Liste des entrees-menu et sous-menus
      for (Object oElm : eMenu.getChildren()) {
        menuBar.add(creerElementMenu((Element) oElm));
      }
      
      JMenuItem menuItem = new JMenuItem(MAContextManager.getInstance().getCurrentContextName());
      menuItem.setText("<html><i>" + menuItem.getText() + "</i></html>");
      menuItem.setEnabled(false);
      menuBar.add(menuItem);

      mjbMenu = menuBar;

    } catch (FileNotFoundException fnfe) {
      MLog.debug("MAMenuGenerator", "chargerMenu ",
          "No table data file.");
      throw fnfe;
    } catch (IOException e) {
      MLog.error("MAMenuGenerator", "chargerMenu ",
          "Error while loading tables data file.", e);
      throw e;
    } catch (Exception e) {
      MLog.error("MAMenuGenerator", "chargerMenu ",
          "Error while reading/using tables data file", e);
      throw e;
    }
  }

  private JMenuItem creerElementMenu(Element eElmMenu) {

    final String sTypeElm = eElmMenu.getName();
    if (sTypeElm.equals("sous-menu")) {
      return creerSousMenu(eElmMenu);
    } else if (sTypeElm.equals("entree-menu")) {
      return creerEntreeMenu(eElmMenu);
    }

    return null;
  }

  private MJMenu creerSousMenu(Element eSsMenu) {
    MJMenu mjm = new MJMenu(eSsMenu.getAttributeValue("libelle"));
    for (Object oElm : eSsMenu.getChildren()) {
      final Component cElmMenu = creerElementMenu((Element) oElm);
      if (cElmMenu != null) {
        mjm.add(cElmMenu);
      } else {
        mjm.addSeparator();
      }
    }
    return mjm;
  }

  private JMenuItem creerEntreeMenu(Element eEntreeMenu) {
    final String sLib = eEntreeMenu.getAttributeValue("libelle");
    final String sNomSys = eEntreeMenu.getAttributeValue("nomsys");
    final String sAction = eEntreeMenu.getAttributeValue("action");

    if (sLib == null && (sNomSys == null || sAction == null)) {
      return null;
    }

    JMenuItem mjmi = null;

    final MASysWindow maFen = (sNomSys != null ? MAContextManager.getInstance()
        .getWindowContext().getSysWindowByWindowName(sNomSys) : null);
    final MEActionMenu mam = (sAction != null ? MEActionMenu
        .getActionParNom(sAction) : MEActionMenu.OUVRIR_FENETRE);

    // Creation du bouton suivant l'action demandee
    switch (mam) {
      case TOGGLE:
        // TODO
        // mjmi = new MACheckBoxMenuItem(sLib, maFen);
        // break;

      case OUVRIR_FENETRE:
        mjmi = new MAJMenuItem(sLib, maFen);
        break;
        
      case CREATION:
        mjmi = new MAJMenuItem(sLib, MEActionMenu.CREATION, maFen);
        break;


      default:
//      case QUITTER:
//      case FERMER_TOUT:
//      case OUVRIR_FENETRES_MENU:
        mjmi = new MAJMenuItem(sLib, mam);
        break;
    }
    return mjmi;
  }

}
