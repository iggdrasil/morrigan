/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : METypeChamp.java - CREATION : 2005/11/08
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util;

import morrigan.admin.ui.field.MADateField;
import morrigan.admin.ui.field.MADecimalField;
import morrigan.admin.ui.field.MAIntegerField;
import morrigan.admin.ui.field.MATextAreaField;
import morrigan.admin.ui.field.MATextField;
import morrigan.log.MLog;

public enum METypeChamp {
  DATE("date", MADateField.class),
  DATEHEURE("datetime", MADateField.class),
  HEURE("time", MADateField.class),
  TEXTE("string", MATextField.class),
  ZONETEXTE("text", MATextAreaField.class),
  ENTIER("integer", MAIntegerField.class),
  DECIMAL("decimal", MADecimalField.class),
  BOOLEEN("boolean", null),
  POLYGON("polygon", null);

  private String sType;

  @SuppressWarnings("unused")
  private Class<?> c;

  METypeChamp(String sType, Class<?> c) {
    this.sType = sType;
    this.c = c;
  }

  private String getType() {
    return sType;
  }

  static public METypeChamp getChampParType(String sType) {
    for (METypeChamp c : METypeChamp.values()) {
      if (c.getType().equals(sType)) {
        return c;
      }
    }

    // MLog.error("METypeChamp", "getChampParType", "Aucun type trouve pour \""
    // + sType + '"');

    return getChampParTypeSql(sType);
  }

  static public METypeChamp getChampParTypeSql(String typeSql) {
    METypeChamp type = null;
    Integer size = null;
    try {
      String stringSize = typeSql.substring(typeSql.indexOf('(') + 1, typeSql
          .length() - 1);
      size = Integer.parseInt(stringSize);
      typeSql = typeSql.substring(0, typeSql.indexOf('(')).toLowerCase();
    } catch (Exception e) {
      // Do nothing
    }

    if (typeSql.equals("boolean")) {
      type = BOOLEEN;
    } else if (typeSql.equals("integer")) {
      type = ENTIER;
    } else if (typeSql.equals("smallint")) {
      type = ENTIER;
    } else if (typeSql.equals("tiny")) {
      type = ENTIER;
    } else if (typeSql.equals("number")) {
      type = ENTIER;
    } else if (typeSql.equals("integer") || typeSql.equals("int")) {
      type = ENTIER;
    } else if (typeSql.equals("serial")) {
      type = ENTIER;
    } else if (typeSql.equals("float")) {
      type = DECIMAL;
    } else if (typeSql.equals("inet")) {
      type = TEXTE;
    } else if (typeSql.equals("point")) {
      type = TEXTE;
    } else if (typeSql.equals("circle")) {
      type = TEXTE;
    } else if (typeSql.equals("path")) {
      type = TEXTE;
    } else if (typeSql.equals("polygon")) {
      type = POLYGON;
    } else if (typeSql.equals("char")) {
      type = TEXTE;
    } else if (typeSql.equals("date")) {
      type = DATE;
    } else if (typeSql.equals("timestamp")
        || typeSql.equals("timestamp with time zone")
        || typeSql.equals("timestamp without time zone")) {
      type = DATEHEURE;
    } else if (typeSql.equals("varchar")
        || typeSql.equals("character varying")
        || typeSql.equals("text")) {
      type = (size != null && size < 100) ? TEXTE : ZONETEXTE;
    } else {
      MLog.error("METypeChamp", "getChampParTypeSql",
          "Aucun type trouve pour le type SQL \"" + typeSql + '"');

      throw new RuntimeException("Type SQL '" + typeSql + "' inconnu !");
    }

    return type;
  }
}