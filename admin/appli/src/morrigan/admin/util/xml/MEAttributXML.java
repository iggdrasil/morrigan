/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEAttributXML.java - CREATION : 2005/11/11
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util.xml;

public enum MEAttributXML {
  LIBELLE("libelle"),
  TABLE("table"),
  PREFIXE("prefixe"),
  NOMBDD("nom"),
  TYPE("type"),
  OBLIGATOIRE("obligatoire"),
  EDITABLE("editable");

  private String sLib;

  MEAttributXML(String sLib) {
    this.sLib = sLib;
  }

  public String getLib() {
    return sLib;
  }
}