/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEElementXML.java - CREATION : 2005/11/11
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util.xml;

public enum MEElementXML {
  RACINE("morrigan-admin-fenetres"),
  FENETRES("fenetres"),
  FENETRE("fenetre"),
  LIGNES("lignes"),
  LIGNE("ligne"),
  CHAMP("champ");

  private String sLib;

  MEElementXML(String sLib) {
    this.sLib = sLib;
  }

  public String getLib() {
    return sLib;
  }
}