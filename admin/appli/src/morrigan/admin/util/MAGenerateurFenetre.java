/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAGenerateurFenetre.java - CREATION : 2005/11/03
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
/*
 * Created on 3 nov. 2005 Ce fichier appartient au projet Morrigan.
 */
package morrigan.admin.util;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.MALightTable;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.ui.MAList;
import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.admin.ui.fenetre.MAFenTable;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.ui.field.MABooleanField;
import morrigan.admin.ui.field.MADateField;
import morrigan.admin.ui.field.MADecimalField;
import morrigan.admin.ui.field.MAIntegerField;
import morrigan.admin.ui.field.MATextAreaField;
import morrigan.admin.ui.field.MATextField;
import morrigan.admin.ui.field.MIAField;
import morrigan.graphique.MJLabel;
import morrigan.log.MLog;
import morrigan.preference.MPreferences;

/**
 * Partant d'un fichier XML detaillant la structure des fenetres, cette classe
 * permet de generer ces fenetres.
 * 
 * @author armel
 */
public final class MAGenerateurFenetre {

  /** Hidden constructor */
  private MAGenerateurFenetre() {}

  /**
   * Create a window according to its system name.
   * 
   * @param fenetre the system name of the window to create
   * @return the new window
   * @throws CreateWindowException if the creation fails
   */
  public static MAFenTable createWindow(MASysWindow fenetre)
      throws CreateWindowException {
    Table table = MAContextManager.getInstance().getDbContext().getTableParNom(
        fenetre.getSysName());

    if (table == null) {
      throw new CreateWindowException("Impossible de creer la fenetre "
          + fenetre.getSysName());
    }

    return new Transcoder().transcodeTableIntoFenTable(table);
  }

  /**
   * Create a window with its properties (title, dimension, contenu).
   * 
   * @param sTitre titre
   * @param iHauteurFen hauteur en cellules
   * @param iLargeurFen largeur en cellules
   * @param mdFen donnees sur la fenetre
   * @param mDBTable donnees sur la table SQL
   * @param lLigChamps contenu sous forme de lignes
   * @return une fenetre fraichement creee
   */
  public static MAFenTable creerFenetre(String sTitre, int iHauteurFen,
      int iLargeurFen, final MADonnees mdFen, final MALightTable mDBTable,
      final Ligne[] lLigChamps) {
    MLog.debug("MAGenerateurFenetre", "creerFenetre", sTitre
        + " : iHauteurFen=" + iHauteurFen + ", iLargeurFen=" + iLargeurFen);
    MAFenTable maft = new MAFenTable(sTitre, iHauteurFen, iLargeurFen) {

      @Override
      protected void build() {
        // Jointures avec les tables et champs lies
        linkToTables(mdFen, mDBTable);

        // Declarations
        final int iLargeurLabel = 12;
        final int iLargeurChamps = 12;
        final int iHauteurLigne = 1;

        // Placement
        final int xLabel = 1;
        // Pourcentage de reduction du champ ID par rapport a sa taille
        // initialement calculee
        final int iDeltaID = 30;
        int yLigne = 2;
        final int xChamps = xLabel + iLargeurLabel + 1;

        for (Ligne element : lLigChamps) {
          final String sLibLigne = element.getLabel();
          if (sLibLigne == null) {
            // Si pas de libelle, ligne vierge
            yLigne++;
            continue;
          }

          // Libelle
          MJLabel mjl = new MJLabel(sLibLigne, iLargeurLabel, 1);

          // Tooltip
          MAInfosColonne infosLigne = element.getChamps()[0];
          StringBuffer tooltip = new StringBuffer("<HTML>");
          tooltip.append("<I>Comment : </I>" + infosLigne.getComment() + "<BR>");
          tooltip.append("<I>Def : </I>" + infosLigne.getColumnName() + "<BR>");
          tooltip.append("<I>Type : </I>" + infosLigne.getColumnType() + "<BR>");
          tooltip.append("<I>Database : </I>" + infosLigne.getNomReelChamp() + "<BR>");
          tooltip.append("</HTML>");
          mjl.setToolTipText(tooltip.toString());

          mpqPrinc.add(mjl, xLabel, yLigne);

          // Creation et placement des champs
          MAInfosColonne[] mInfoChps = element.getChamps();
          int xChp = 0;
          for (int iChp = 0; iChp < mInfoChps.length; iChp++) {
            final MAInfosColonne mInfoChp = mInfoChps[iChp];

            int iLargChp = iLargeurChamps; // / mInfoChps.length;

            // Si champ ID/exterieur, diminution/augmentation de taille :
            // * Si ID, reduction
            // * Si EXT, division de la taille restante par le nb de champs EXT
            // restant
            if (mInfoChp.getColumnNature() == MENatureChampBdd.IDVTABLEEXT) {
              iLargChp = (iLargeurChamps * iDeltaID) / 100;
            } else if (mInfoChp.getColumnNature() == MENatureChampBdd.EXT) {
              iLargChp = (iLargeurChamps - xChp) / (mInfoChps.length - iChp);
            }

            MIAField mf = null;
            switch (mInfoChp.getColumnType()) {
              case TEXTE:
                mf = new MATextField(iHauteurLigne, iLargChp);
                break;
              case ZONETEXTE:
                mf = new MATextAreaField(iHauteurLigne, iLargChp);
                break;
              case ENTIER:
                mf = new MAIntegerField(iHauteurLigne, iLargChp);
                break;
              case DECIMAL:
                mf = new MADecimalField(iHauteurLigne, iLargChp);
                break;
              case DATE:
                mf = new MADateField(iHauteurLigne, iLargChp,
                    MADateField.DATEFORMAT.DATE);
                mInfoChp.setFormat(MPreferences.getInstance().getDateFormat());
                // sChpSQL = creerChpSQLFormat(sChpSQL,
                // MADateField.DATEFORMAT.DATE.getFormat());
                break;
              case DATEHEURE:
                mf = new MADateField(iHauteurLigne, iLargChp,
                    MADateField.DATEFORMAT.DATEHOUR);
                mInfoChp.setFormat(MPreferences.getInstance().getDateHourFormat());
                // sChpSQL = creerChpSQLFormat(sChpSQL,
                // MADateField.DATEFORMAT.DATEHEURE.getFormat());
                break;
              case HEURE:
                mf = new MADateField(iHauteurLigne, iLargChp,
                    MADateField.DATEFORMAT.HOUR);
                mInfoChp.setFormat(MPreferences.getInstance().getHourFormat());
                // sChpSQL = creerChpSQLFormat(sChpSQL,
                // MADateField.DATEFORMAT.HEURE.getFormat());
                break;
              case BOOLEEN:
                mf = new MABooleanField(iHauteurLigne, iLargChp / 5);
                // TODO: dimension d'un BooleanField
                break;
              default:
                MLog.error("MAFenTableXML -> MAFenTable", "init",
                    "Le type de champ " + mInfoChp.getColumnType()
                        + " est inconnu !");
                return;
            }

            mpqPrinc.add(mf, xChp + xChamps, yLigne);

            // On prepare le placement du champ suivant
            xChp += mf.getLargeurCell();

            mf.setMandatory(mInfoChp.isMandatory()
                && mInfoChp.getColumnNature() != MENatureChampBdd.IDTABLE);
            mf.setBddEnabled(mInfoChp.isBddEnabled());
            mf.setNatureChpBdd(mInfoChp.getColumnNature());
            if (mf instanceof JComponent) {
              mjl.setLabelFor((JComponent) mf);
            } else {
              MLog.debug("MAFenTableXML -> MAFenTable", "init",
                  "Le MIField cree n'est pas un JComponent...");
            }

            if (mInfoChp.getColumnNature() == MENatureChampBdd.EXT) {
              // Traitement du lien "ChpID <-> Chpexterieur"
              final MALightTable mtLiee = MAContextManager.getInstance()
                  .getDbContext().getLightTableParNom(mInfoChp.getNomTable());
              final String sNomChpID = mInfoChps[0].getColumnName();
              final String sAliasChpID = mInfoChps[0].getColumnName();
              final String sNomChpIDExt = mtLiee.getTrig() + "_id";
              final String sNomChpExt = mInfoChp.getNomReelChamp();
              final String sAliasChpExt = mInfoChp.getColumnName();

              Table heavyTable = MAContextManager.getInstance().getDbContext()
                  .getTableParNom(mInfoChp.getNomTable());
              heavyTable.getUniqueIndexAttributes();

              final MAList malChampExt = new MAList(mtLiee, new String[] {
                  sNomChpIDExt, sNomChpExt }, mdFen, new String[] {
                  sAliasChpID, sAliasChpExt });

              // Lien entre le 1er champ qui correspond a l'ID et le libelle de
              // la table liee
              malChampExt.linkListWithFields((Component) mdFen.getFields(
                  sNomChpID).get(0), (Component) mf);
            }
            getDonnees().associate(mf, mInfoChp.getColumnName());
          }

          yLigne++;
        }
      }
    };

    return maft;

  }

  /**
   * Conteneur d'elements graphiques d'une ligne de fenetre.
   * 
   * @author armel
   */
  public static class Ligne {

    /** The label */
    private String label;

    /** The columns information */
    private List<MAInfosColonne> mInfoChps = new LinkedList<MAInfosColonne>();

    /**
     * Constructor.
     * 
     * @param label label of the line
     */
    Ligne(String label) {
      this.label = label;
    }

    /**
     * Get the label.
     * 
     * @return the label
     */
    String getLabel() {
      return label;
    }

    /**
     * Add a new field.
     * 
     * @param infosColonnes information of the column
     */
    void addChamp(MAInfosColonne infosColonnes) {
      this.mInfoChps.add(infosColonnes);
    }

    /**
     * Add a {@link List} of fields.
     * 
     * @param infosColonnes information of columns
     */
    void addChamps(List<MAInfosColonne> infosColonnes) {
      this.mInfoChps.addAll(infosColonnes);
    }

    /**
     * Get the line fields.
     * 
     * @return the line fields
     */
    MAInfosColonne[] getChamps() {
      MAInfosColonne[] infosColonnes = new MAInfosColonne[mInfoChps.size()];
      mInfoChps.toArray(infosColonnes);
      return infosColonnes;
    }

  }
}
