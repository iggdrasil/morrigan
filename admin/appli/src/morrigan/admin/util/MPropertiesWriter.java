/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MPropertiesWriter.java - CREATION : 2004/09/01
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Set;

import org.apache.commons.lang.Validate;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MPropertiesWriter {

  /** XML header */
  private static final String HEADER_XML = "<?xml version=\"1.0\"?>";

  /** Morrigan header */
  private static final String HEADER_MORRIGAN = "<morrigan-admin "
      + "xml:base=\"http://peacefrogs.net/~morrigan\">";

  /** Morrigan footer */
  private static final String FIN_MORRIGAN = "</morrigan-admin>";

  /** Default encoding */
  private static final String ENCODING = "8859_1";

  /** Writer */
  private BufferedWriter bufferedWriter = null;

  /**
   * Constructor.
   * 
   * @param file the file where saving data
   * @throws FileNotFoundException if the file does not exist
   * @throws UnsupportedEncodingException if the file encoding is not supported
   * @throws IOException if the writing fails
   */
  public MPropertiesWriter(String file) throws FileNotFoundException,
      UnsupportedEncodingException, IOException {
    Validate.notNull(file,
        "File must be specified to initialize the property writer");
    FileOutputStream fosConf = new FileOutputStream(file);
    bufferedWriter = new BufferedWriter(new OutputStreamWriter(fosConf,
        ENCODING));
    writeHeader();
  }

  private void writeHeader() throws IOException {
    writeln(HEADER_XML);
    writeln(HEADER_MORRIGAN);
  }

  /**
   * Write a comment.
   * 
   * @param comment a comment
   * @throws IOException if the writing fails
   */
  public void writeComment(String comment) throws IOException {
    writeln(new StringBuffer("<!-- ").append(comment).append(" -->"));
  }

  /**
   * Write a section with a title, its elements and values.
   * 
   * @param sectionTitle section title
   * @param elements elements
   * @param values values
   * @throws IOException if writing fails
   * @throws Exception if an error occured
   */
  public void writeSection(String sectionTitle, String[] elements,
      String[] values) throws IOException, Exception {
    writeComment(sectionTitle);

    if (elements.length != values.length) {
      throw new Exception("Les tailles des tableaux de proprietes different.");
    }

    for (int i = 0; i < elements.length; i++) {
      writeElement(elements[i], values[i]);
    }

    writeln();
  }

  /**
   * Write an opening tag.
   * 
   * @param sElement element opening.
   * @throws IOException if writing fails
   */
  public void openElement(String sElement) throws IOException {
    writeln(new StringBuffer("<").append(sElement).append(">"));
  }

  /**
   * Write an opening tag with its attributes.
   * 
   * @param element element
   * @param attributes attributes
   * @throws IOException if writing fails
   */
  public void openElement(String element, HashMap<String, ?> attributes)
      throws IOException {
    write("<" + element);
    Set<String> setKey = attributes.keySet();
    Object[] sKeys = setKey.toArray(new Object[setKey.size()]);
    for (Object attributeName : sKeys) {
      write(new StringBuffer(" ").append(attributeName).append("=\"").append(
          attributes.get(attributeName)).append("\""));
    }
    writeln(">");
  }

  /**
   * Close an element.
   * 
   * @param element element
   * @throws IOException if writing fails
   */
  public void closeElement(String element) throws IOException {
    writeln(new StringBuffer("</").append(element).append(">"));
  }

  /**
   * Write an simple element with its value.
   * 
   * @param element element
   * @param value value
   * @throws IOException if writing fails
   */
  public void writeElement(String element, String value) throws IOException {
    writeln(new StringBuffer("<").append(element).append(">").append(value)
        .append("</").append(element).append(">"));
  }

  /**
   * @param element element
   * @param value value
   * @param hmAttributs
   * @throws IOException if writing fails
   */
  public void writeElement(String element, String value,
      HashMap<String, ?> hmAttributs) throws IOException {
    openElement(element, hmAttributs);
    writeln(new StringBuffer(value).append("</").append(element).append(">"));
  }

  /**
   * Write a buffer text.
   * 
   * @param stringBuffer buffer text
   * @throws IOException if writing fails
   */
  public void write(StringBuffer stringBuffer) throws IOException {
    write(stringBuffer.toString());
  }

  /**
   * Write a text.
   * 
   * @param text text
   * @throws IOException if writing fails
   */
  public void write(String text) throws IOException {
    bufferedWriter.write(text);
  }

  /**
   * Write a new line.
   * 
   * @throws IOException if writing fails
   */
  public void writeln() throws IOException {
    bufferedWriter.newLine();
  }

  /**
   * Write a buffer text and new line.
   * 
   * @param line line
   * @throws IOException if writing fails
   */
  public void writeln(StringBuffer line) throws IOException {
    write(line.toString());
  }

  /**
   * Write a text and new line.
   * 
   * @param line line
   * @throws IOException if writing fails
   */
  public void writeln(String line) throws IOException {
    // TODO: tester la presence de caracteres speciaux comme \n, =
    bufferedWriter.write(line);
    bufferedWriter.newLine();
  }

  /**
   * @throws IOException if writing fails
   */
  public void close() throws IOException {
    writeln(FIN_MORRIGAN);
    bufferedWriter.close();
  }
}