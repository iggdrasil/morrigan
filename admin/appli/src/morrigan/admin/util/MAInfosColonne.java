/*
 * Cree le 26 déc. 2005
 * Ce fichier appartient au projet Morrigan.
 */
package morrigan.admin.util;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.table.MInfoColonnePlus;

public class MAInfosColonne extends MInfoColonnePlus {
  private METypeChamp mColumnType = null;

  private Integer size = null;

  private MENatureChampBdd mColumnNature = null;

  // Info pour le traitement avec la base de donnees
  private String sNomReelChamp = null;

  private String sNomTable = null;

  private String sAliasTable = null;

  private boolean bBddEnabled = true;

  private String sFormat = null;

  private String comment = null;

  public void setColumnNature(MENatureChampBdd columnNature) {
    mColumnNature = columnNature;
  }

  public void setNomReelChamp(String nomReelChamp) {
    sNomReelChamp = nomReelChamp;
  }

  public void setTable(String table) {
    sNomTable = table;
  }

  public void setAliasTable(String aliasTable) {
    sAliasTable = aliasTable;
  }

  public MENatureChampBdd getColumnNature() {
    return mColumnNature;
  }

  public String getNomReelChamp() {
    return sNomReelChamp;
  }

  public String getNomTable() {
    return sNomTable;
  }

  public String getAliasTable() {
    return sAliasTable;
  }

  public boolean isBddEnabled() {
    return bBddEnabled;
  }

  public void setBddEnabled(boolean bddEnabled) {
    bBddEnabled = bddEnabled;
  }

  public METypeChamp getColumnType() {
    return mColumnType;
  }

  public void setColumnType(METypeChamp columnType, Integer size) {
    mColumnType = columnType;
    this.size = size;
  }

  public String getFormat() {
    return sFormat;
  }

  public void setFormat(String format) {
    sFormat = format;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
  
  public String getComment() {
    return comment;
  }

}
