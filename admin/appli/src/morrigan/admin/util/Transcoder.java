package morrigan.admin.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.MALightTable;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Operation;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.database.tabledesc.operation.UniqueIndexOperation;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.admin.ui.fenetre.MAFenTable;
import morrigan.admin.util.MAGenerateurFenetre.Ligne;

public class Transcoder {

  /**
   * Default constructor.
   */
  public Transcoder() {}

  /**
   * largeur = largeur d'un libelle + largeur des champs + bordures droite et
   * gauche. 26 = 2*12+2;
   */
  private static final int iLargeurDef = 26;

  /** Index used to generate aliases. */
  private int index = 1;

  /**
   * Creates a MADonnees with the SQL description.
   * 
   * @param table the SQL table description
   * @return a MADonnees
   * @throws CreateWindowException 
   */
  public MADonnees transcodeTableIntoMADonnees(Table table) throws CreateWindowException {
    MAUtil.notNull(table, "SQL table");
    List<MAInfosColonne> colonnes = new LinkedList<MAInfosColonne>();

    for (Iterator<Attribute> iter = table.getAttributesIterator(); iter
        .hasNext();) {
      Attribute attribute = iter.next();

      MAInfosColonne infosColonne = transcodeAttribute(attribute);

      // La ligne contient en premier temps le champ de la table
      colonnes.add(infosColonne);

      // Si lien avec table exterieure, on cherche les infos des colonnes
      // supplementaires.
      if (infosColonne.getColumnNature() == MENatureChampBdd.IDVTABLEEXT) {
        // UNIQUEMENT les champs SUPPLEMENTAIRES
        List<MAInfosColonne> infosColonnesExt = transcodeAttributeExt(attribute);
        // Et on les rajoute a la ligne courante
        colonnes.addAll(infosColonnesExt);
      }
    }

    String[] nomColonnes = extractColumnsNames(colonnes);
    MAInfosColonne[] mInfosCols = new MAInfosColonne[colonnes.size()];
    colonnes.toArray(mInfosCols);

    final MADonnees mDonnees = new MADonnees(nomColonnes, mInfosCols);

    return mDonnees;
  }

  /**
   * Creates a window thanks the SQL description.
   * 
   * @param table the SQL table description
   * @return a window with the table information
   * @throws CreateWindowException
   */
  public MAFenTable transcodeTableIntoFenTable(Table table)
      throws CreateWindowException {
    MAUtil.notNull(table, "SQL table");
    List<MAInfosColonne> colonnes = new LinkedList<MAInfosColonne>();
    List<Ligne> lignes = new LinkedList<Ligne>();

    for (Iterator<Attribute> iter = table.getAttributesIterator(); iter
        .hasNext();) {
      Attribute attribute = iter.next();
      Ligne ligne = new Ligne(attribute.getLabel());

      MAInfosColonne infosColonne = transcodeAttribute(attribute);

      // La ligne contient en premier temps le champ de la table
      ligne.addChamp(infosColonne);
      colonnes.add(infosColonne);

      // Si lien avec table exterieure, on cherche les infos des colonnes
      // supplementaires.
      if (infosColonne.getColumnNature() == MENatureChampBdd.IDVTABLEEXT) {
        // UNIQUEMENT les champs SUPPLEMENTAIRES
        List<MAInfosColonne> infosColonnesExt = transcodeAttributeExt(attribute);
        // Et on les rajoute a la ligne courante
        ligne.addChamps(infosColonnesExt);
        colonnes.addAll(infosColonnesExt);
      }

      lignes.add(ligne);
    }

    String[] nomColonnes = extractColumnsNames(colonnes);
    MAInfosColonne[] mInfosCols = new MAInfosColonne[colonnes.size()];
    colonnes.toArray(mInfosCols);

    String titre = table.getLabel();
    if (titre == null || titre.length() == 0) {
      titre = table.getName();
    }
    // Nb lignes + 1 au-dessus + 1 au-dessous + 1 de boutons
    int hauteurFen = lignes.size() + 2 + 1;
    int largeurFen = iLargeurDef;
    final MADonnees mdFen = new MADonnees(nomColonnes, mInfosCols);
    final MALightTable mDBTable = MAContextManager.getInstance().getDbContext()
        .getLightTableParNom(table.getName());
    final Ligne[] lignesTab = new Ligne[lignes.size()];
    lignes.toArray(lignesTab);

    return MAGenerateurFenetre.creerFenetre(titre, hauteurFen, largeurFen,
        mdFen, mDBTable, lignesTab);
  }

  private String[] extractColumnsNames(List<MAInfosColonne> infosColonnes) {
    String[] columnsNames = new String[infosColonnes.size()];
    int idx = 0;
    for (MAInfosColonne infoColonne : infosColonnes) {
      columnsNames[idx++] = infoColonne.getColumnName();
    }

    return columnsNames;
  }

  public MAInfosColonne transcodeAttribute(Attribute attribute) {
    final String nom = attribute.getName();

    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    final MALightTable lightTable = dbContext.getLightTableParNom(attribute
        .getTable().getName());

    final MAInfosColonne infosColonneGenerated = new MAInfosColonne();
    infosColonneGenerated.setNomReelChamp(nom);
    infosColonneGenerated.setComment(attribute.getComment());
    infosColonneGenerated.setColumnType(attribute.getType(), attribute
        .getSize());
    infosColonneGenerated.setMandatory(attribute.isMandatory());
    // Clairement defini comme non editable ou ID de la table
    infosColonneGenerated.setBddEnabled(!nom.equals(lightTable.getTrig()
        + "_id")
        && attribute.isEnabled());

    infosColonneGenerated.setTable(lightTable.getNom());
    infosColonneGenerated.setAliasTable(lightTable.getTrig());

    // Champ ID ou id vers exterieur ou ...
    if (attribute.isID()) {
      infosColonneGenerated.setColumnNature(MENatureChampBdd.IDTABLE);
    } else if (attribute.isForeignID()) {
      infosColonneGenerated.setColumnNature(MENatureChampBdd.IDVTABLEEXT);
    } else {
      infosColonneGenerated.setColumnNature(MENatureChampBdd.TABLE);
    }

    infosColonneGenerated.setColumnName(infosColonneGenerated.getAliasTable()
        + "_" + infosColonneGenerated.getNomReelChamp());

    return infosColonneGenerated;
  }

  /**
   * Transcode an attribute of a table into a List of attributes. The first one
   * belongs to the first table; the others belong to the "foreign" table.<br>
   * If the external table has a unique index, the fields contributing to it are
   * choosen.
   * 
   * @param attribute
   * @return a List of column information
   * @throws CreateWindowException
   */
  private List<MAInfosColonne> transcodeAttributeExt(Attribute attribute)
      throws CreateWindowException {
    // Retrieve the primary key that links to this foreign key.
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    String primaryKeyFieldName = dbContext.getForeignToPrimaryKeyLinks().get(
        attribute.getName());
    if (primaryKeyFieldName == null) {
      throw new CreateWindowException("No foreign key found from '"
          + attribute.getName() + "' to another table");
    }
    String trig = primaryKeyFieldName.substring(0, 3);
    final Table tableExt = dbContext.getTableParTrigramme(trig);

    // ALIAS ???
    String aliasTable = tableExt.getName() + (index++);

    List<MAInfosColonne> infosColonnesExt = new LinkedList<MAInfosColonne>();

    // recuperations des champs des cles uniques
    for (Iterator<Operation> iter = tableExt.getOperationsIterator(); iter
        .hasNext();) {
      Operation operation = iter.next();
      if (operation instanceof UniqueIndexOperation) {
        UniqueIndexOperation uniqueIndexOp = (UniqueIndexOperation) operation;
        for (Iterator<Attribute> iterAttributes = uniqueIndexOp.getFields()
            .iterator(); iterAttributes.hasNext();) {
          // Depuis le nom du champ...
          String champExt = iterAttributes.next().getName();
          // On recupere l'attribut
          Attribute attributeExt = tableExt.getAttribute(champExt);
          // On transcode pour l'infos colonne
          infosColonnesExt.add(transcodeAttribute(attributeExt));
        }
      }
    }

    // S'il n'y a pas d'index unique, on se rabat au pif sur le premier qui
    // tombe.
    if (infosColonnesExt.size() == 0) {
      Attribute attributeRandom = null;
      for (Iterator<Attribute> iter = tableExt.getAttributesIterator(); iter
          .hasNext();) {
        attributeRandom = iter.next();
        if (!attributeRandom.getName().endsWith("_id")) {
          break;
        }
      }

      infosColonnesExt.add(transcodeAttribute(attributeRandom));
    }

    // On force quelques infos sur l'ensemble des champs EXT
    for (Iterator<MAInfosColonne> iter = infosColonnesExt.iterator(); iter
        .hasNext();) {
      MAInfosColonne infosColonneExt = iter.next();
      infosColonneExt.setComment(attribute.getComment());
      infosColonneExt.setMandatory(attribute.isMandatory());
      infosColonneExt.setBddEnabled(false);

      infosColonneExt.setAliasTable(aliasTable);

      infosColonneExt.setColumnNature(MENatureChampBdd.EXT);
      infosColonneExt.setColumnName(infosColonneExt.getAliasTable() + "_"
          + infosColonneExt.getNomReelChamp());
    }

    return infosColonnesExt;
  }

}
