/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MADialogue.java - CREATION : 19 déc. 2005
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.admin.util;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import morrigan.admin.ui.enumeration.MEReponseUtil;

public class MADialogue extends JOptionPane {
  public enum MESSAGE {
    INFORMATION("Information", JOptionPane.PLAIN_MESSAGE),
    ATTENTION("Attention", JOptionPane.WARNING_MESSAGE),
    ERREUR("Erreur", JOptionPane.ERROR_MESSAGE),
    ERREUR_FATALE("Erreur fatale", JOptionPane.ERROR_MESSAGE),
    QUESTION_OUI_NON("Question", JOptionPane.QUESTION_MESSAGE,
        JOptionPane.YES_NO_OPTION),
    QUESTION_OUI_NON_ANNULER("Question", JOptionPane.QUESTION_MESSAGE,
        JOptionPane.YES_NO_CANCEL_OPTION);

    private String sTitreFen;

    private int iTypeOptionPane;

    private int iChoixReponses;

    MESSAGE(String sTitreFen, int iTypeMessage) {
      this.sTitreFen = sTitreFen;
      this.iTypeOptionPane = iTypeMessage;
    }

    MESSAGE(String sTitreFen, int iTypeMessage, int iChoixReponse) {
      this(sTitreFen, iTypeMessage);
      this.iChoixReponses = iChoixReponse;
    }

    String getTitreFen() {
      return sTitreFen;
    }

    int getTypeMessage() {
      return iTypeOptionPane;
    }

    int getChoixReponse() {
      return iChoixReponses;
    }

    Icon getIcone() {
      return null;
    }
  }

  static public void ouvrirDialogue(Component cFenetreMere, MESSAGE message,
      String sMessage) {
    showMessageDialog(cFenetreMere, sMessage, message.getTitreFen(), message
        .getTypeMessage());
  }

  static public void showMessage(Component cFenetreMere, MESSAGE message,
      String text) {
    JOptionPane.showMessageDialog(cFenetreMere, text, message.getTitreFen(),
        message.getChoixReponse());
  }

  static public MEReponseUtil demanderConfirmation(Component cFenetreMere,
      MESSAGE message, String sQuestion) {
    int n = JOptionPane.showConfirmDialog(cFenetreMere, sQuestion, message
        .getTitreFen(), message.getChoixReponse());

    MEReponseUtil mReponse;
    switch (n) {
      case -1: // Fermeture fenetre
        mReponse = MEReponseUtil.ANNULER;
        break;
      case 0: // Oui
        mReponse = MEReponseUtil.OUI;
        break;
      case 1: // Non
        mReponse = MEReponseUtil.NON;
        break;

      default:
        mReponse = MEReponseUtil.ANNULER;
    }
    return mReponse;
  }
}
