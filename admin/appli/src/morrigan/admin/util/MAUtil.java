package morrigan.admin.util;

/**
 * Class for util methods.
 * 
 * @author Armel
 */
public final class MAUtil {

  /**
   * Hidden constructor.
   */
  private MAUtil() {}

  /**
   * Check if the object is null. In this case, throw an
   * {@link IllegalArgumentException}.
   * 
   * @param object the object to test
   * @param objectName the error text used if the object is null
   * @throws IllegalArgumentException if the object is null
   */
  public static void notNull(Object object, String objectName)
      throws IllegalArgumentException {
    if (object == null) {
      throw new IllegalArgumentException("The '" + objectName
          + "' cannot be null.");
    }
  }

  /**
   * Check if the string is empty. In this case, throw an
   * {@link IllegalArgumentException}.
   * 
   * @param string the string to test
   * @param objectName the error text used if the object is null
   * @throws IllegalArgumentException if the string is empty
   */
  public static void notEmpty(String string, String objectName)
      throws IllegalArgumentException {
    if (string == null) {
      throw new IllegalArgumentException("The '" + objectName
          + "' cannot be empty.");
    }
  }

  /**
   * Check if the string is null or empty. In this case, throw an
   * {@link IllegalArgumentException}.
   * 
   * @param string the string to test
   * @param text the error text used if the object is null
   * @throws IllegalArgumentException if the string is not or empty
   */
  public static void notNullOrEmpty(String string, String text)
      throws IllegalArgumentException {
    notNull(string, text);
    notEmpty(string, text);
  }

}
