/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MADonnees.java - CREATION : 2004/10/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.util;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JComponent;

import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MIAField;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.table.MDonnees;
import morrigan.graphique.table.MInfoColonne;
import morrigan.graphique.table.MInfoColonnePlus;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MADonnees extends MDonnees {

  private int iLigneActiveRecherche = 0;

  private int iLigneActiveLecture = 0;

  private List<List<Object>> vDataLecture = new ArrayList<List<Object>>();

  private List<List<Object>> vDataRecherche = new ArrayList<List<Object>>();

  private MEModeUtilDonnees iModeUtilisation = MEModeUtilDonnees.LECTURE;

  public MADonnees(String[] sNomsColonnes) {
    super(sNomsColonnes);
    vData = vDataRecherche;
    addRow();
    vData = vDataLecture;
    addRow();
  }

  public MADonnees(String[] sNomsColonnes, MAInfosColonne[] mInfosCols) {
    this(sNomsColonnes);
    for (MAInfosColonne mInfoCol : mInfosCols)
      setInfoCol(mInfoCol.getColumnName(), mInfoCol);
  }

  @Override
  protected MInfoColonne createInfoCol() {
    return new MAInfosColonne();
  }

  public Object getRechercheValueAt(int iColonne) {
    return vDataRecherche.get(iLigneActiveRecherche).get(iColonne);
  }

  public int getDerniereLigne() {
    final int iNbLg = getRowCount();
    if (iModeUtilisation == MEModeUtilDonnees.RECHERCHE)
      return iNbLg - 1;

    // if (iModeUtilisation == Mode.LECTURE)
    return iNbLg > 1 ? iNbLg - 2 : 0;
  }

  public void goToLigneActiveRecherche() {
    setLigneActive(iLigneActiveRecherche);
  }

  @Override
  public String toString() {
    return new StringBuffer("MADonnees").append("\n\tRecherche:").append(
        vDataRecherche).append("\n\tLecture:").append(vDataLecture).append(
        "\n\tEn cours :").append(vData).toString();
  }

  /**
   * Definit le type d'utilisation que l'on veut faire des donnees: - Lecture
   * pour retourner les donnees de la base - Recherche pour retourner les
   * criteres de recherches saisies precedemment
   * 
   * @param iModeUtilisation
   */
  public void setMode(MEModeUtilDonnees iModeUtilisation) {
    // Si l'ancien et le nouvel affichage sont les memes, pas de changement
    if (this.iModeUtilisation == iModeUtilisation)
      return;

    // MLog.debug("MDonnees", "setAffichage", "Affichage demande : "
    // + (iModeUtilisation == Mode.LECTURE ? "lecture" : "recherche"));

    // Sinon, on sauve les donnees en cours d'affichage et on affiche les
    // nouvelles
    switch (this.iModeUtilisation = iModeUtilisation) {
      case RECHERCHE:
        vData = vDataRecherche;
        iLigneActiveLecture = 0;
        iLigneActive = iLigneActiveRecherche;
        break;
      case LECTURE:
        vData = vDataLecture;
        // Sauvegarde de l'ancienne ligne de recherche
        iLigneActiveRecherche = iLigneActive;
        iLigneActive = iLigneActiveLecture;
        break;
      default:
        MLog.error("MDonnees", "setAffichage", "Mode d'affichage inconnu: "
            + iModeUtilisation);
        break;
    }

    // MLog.debug("MDonnees", "setAffichage", "MDonnees vaut: " + toString());
  }

  // public void clearData()
  // {
  // iLigneActive = -1;
  // vData = new Vector<Vector<Object>>();
  // }

  @Override
  public void applyNewValue(Object oValue, String sNomColonne) {
    setValueAt(oValue, iLigneActive, sNomColonne);
    // MLog.debug("MADonnees", "applyNewValue", sNomColonne);

    // Notification a la fenetre d'un changement de valeur
    for (int iColonne = 0; iColonne < columnInfos.size(); iColonne++)
      if (columnInfos.get(iColonne).getColumnName().equals(sNomColonne)) {
        // Vector<MIField> vFields = hmColumnsFields.get(sNomColonne);
        // FIXME : migration vers INFOS COL

        // On recupere les champs affiches pour cette colonne...
        Vector<MIField> vFields = ((MInfoColonnePlus) columnInfos
            .get(getIndexOfColumn(sNomColonne))).getFields();
        if (vFields == null)
          continue;
        
        // ... et on les met a jour avec la nouvelle valeur
        for (MIField field : vFields) {
          // MLog.debug("MDonnees", "applyNewValue", "Notif de fenetre");
          Container cParent = null;
          if (field instanceof JComponent) {
            cParent = ((JComponent) field).getParent();
          }

          while (cParent != null && cParent != cParent.getParent()) {
            // MLog.infos("Parent : " + cParent);
            if (cParent instanceof JComponent)
              cParent = cParent.getParent();
            else
              break;
          }
          // MLog.infos("Parent final : " + cParent);
          if (cParent == null) {
            MLog.error("MADonnees", "applyNewValue",
                "Pas de parent trouve pour le champ " + field + " (colonne \""
                    + sNomColonne + "\"");
            return;
          } else if (!(cParent instanceof MAFenetre)) {
            MLog.error("MADonnees", "applyNewValue",
                "Le parent trouve n'est pas une MAFenetre !");
            return;
          }

          ((MAFenetre) cParent).onValueChange();
        }
        break;
      }
  }

  public void associate(MIAField mfChamp, String sNomColonne) {
    super.associate(mfChamp, sNomColonne);
    // Gestion des natures de champ par colonne
    ((MAInfosColonne) columnInfos.get(getIndexOfColumn(sNomColonne)))
        .setColumnNature(mfChamp.getNatureChpBdd());

    // TODO: gerer ici les editeurs à cloner
  }

}