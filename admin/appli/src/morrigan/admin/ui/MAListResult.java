/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAListResult.java - CREATION : 2005/01/16
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenTable;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAListResult extends MJPanelQuad implements MouseListener,
    KeyListener {
  private static final int HAUTEUR_ELT_DEF = 20;

  private static final int HAUTEUR_PANEL_DEF = 300;

  private static final int LARGEUR_PANEL_DEF = 500;

  private static final int LARGEUR_COL_DEF = 100;

  private static final int NB_COL_MAX = 10;

  private MADonnees mdListe = null;

  private MAFenTable maFenTable = null;

  private Component cComp = null;

  private MAJTable mjtList = null;

  // private MJTable mjtList = null;
  private MJPopupMenu popListe = null;

  public MAListResult(MADonnees mdTable) {
    super(BORDURE.RAISED, 1, 1);
    mdListe = mdTable;

    if (mdTable == null) {
      MLog.error("MAList", "MAList",
          "Aucun ensemble de donnees sources n'a ete specifie !");
      return;
    }
  }

  public MAListResult(MAFenTable maft, MADonnees mdTable) {
    this(mdTable);
    maFenTable = maft;

    if (mdTable == null) {
      MLog.error("MAList", "MAList",
          "Aucun ensemble de donnees sources n'a ete specifie !");
      return;
    }
  }

  private void buildScreen() {
    // JComboBox
    if (mjtList == null)
      mjtList = creerListe();
    else
      mjtList.setModel(mdListe);

    // TODO: calculer proprement la taille de la popup

    int iHauteur = (mjtList.getRowCount() + 1) * HAUTEUR_ELT_DEF;
    if (iHauteur > HAUTEUR_PANEL_DEF)
      iHauteur = HAUTEUR_PANEL_DEF;
    int iLargeur = mjtList.getColumnCount() * LARGEUR_COL_DEF;
    if (iLargeur > LARGEUR_PANEL_DEF)
      iLargeur = LARGEUR_PANEL_DEF;
    imposerTaille(iLargeur, iHauteur);
  }

  private MAJTable creerListe() {
    MAJTable mjt = new MAJTable(mdListe);
    // mjtList = new MJTable(mdListe, 1, 1);
    add(new MJScrollPane(mjt), 0, 0);

    mjt.addKeyListener(this);
    mjt.addMouseListener(this);

    // Cacher les colonnes superflues.
    // Toutes les colonnes cle etrangere ("xxx_yyy_id") sont supprimees
    for (int iCol = 1; iCol < mjt.getColumnCount();) {
      final String sNomCol = mjt.getColumnName(iCol);
      if (sNomCol.length() > 6
          && sNomCol.substring(sNomCol.length() - 3).equals("_id"))
        mjt.removeColumn(mjt.getColumnModel().getColumn(iCol));
      else
        iCol++; // Pour traiter toutes les lignes, il ne faut incrementer que
      // sur les
      // lignes non supprimees
    }

    if (mjt.getColumnCount() > NB_COL_MAX) {
      // Puis les colonnes non obligatoires sont supprimees (ds la limite de
      // NB_COL_MAX)
      for (int iCol = mjt.getColumnCount() - 1; iCol > NB_COL_MAX
          && mjt.getColumnCount() > NB_COL_MAX; iCol--) {
        final String sNomCol = mjt.getColumnName(iCol);
        List<MIField> vmf = mdListe.getFields(sNomCol);
        // Les colonnes au-dela de la 10e sont supprimees si non obligatoires
        if (vmf != null && vmf.size() > 0 && !vmf.get(0).isMandatory())
          mjt.removeColumn(mjt.getColumnModel().getColumn(iCol));
      }

      // Enfin, les colonnes au-dela de la 10e place sont supprimees
      for (int iCol = NB_COL_MAX; iCol < mjt.getColumnCount();) {
        mjt.removeColumn(mjt.getColumnModel().getColumn(iCol));
      }
    }

    return mjt;
  }

  /**
   * Fermer le popup de description.
   */
  public void closePopup() {
    if (popListe != null) {
      popListe.setVisible(false);
    }
  }

  public void keyPressed(KeyEvent e) {
    // Si validation par Entree
    if (e.getKeyCode() == 10) {
      // mdListe.setLigneActive(mjtList.getSelectedRow());
      closePopup();
    }
  }

  @SuppressWarnings("unused")
  public void keyReleased(KeyEvent e) {}

  @SuppressWarnings("unused")
  public void keyTyped(KeyEvent e) {}

  public void mouseClicked(MouseEvent e) {
    if (maFenTable != null && e.getSource() == mjtList) {
      // MLog.debug("MAListResult", "mouseClicked", "Clic => Raffraichissement
      // des boutons");
      maFenTable.refreshEnabledBoutons();
      final MEAffichage affichageFen = maFenTable.getModeAffichage();

      // Mise à jour du mode d'affichage de la fenetre
      if (affichageFen == MEAffichage.LECTURE
          && mdListe.getLigneActive() == mdListe.getRowCount() - 1) {
        maFenTable.setModeAffichage(MEAffichage.CREATION);
      } else if ((affichageFen == MEAffichage.CREATION || affichageFen == MEAffichage.MODIFICATION)
          && mdListe.getLigneActive() != mdListe.getRowCount() - 1) {
        maFenTable.setModeAffichage(MEAffichage.LECTURE);
      }
    }
  }

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  public void mousePressed(MouseEvent e) {
    onActionOnButton(e);
  }

  public void mouseReleased(MouseEvent e) {
    onActionOnButton(e);
  }

  private void onActionOnButton(MouseEvent e) {
    // Clic sur le champ a ecouter
    if (e.getSource() == cComp) {
      // Le trigger du bouton droit ne marche qu'a la pression !!! :'(
      if (e.isPopupTrigger()) {
        buildScreen();
        openPopup();
      }
    } else
    // Double clic sur une ligne de la popup
    if (e.getSource() == mjtList) {
      if (e.getClickCount() >= 2)
        closePopup();
    }
  }

  public void openPopup() {
    if (popListe == null) {
      popListe = new MJPopupMenu();
      popListe.add(this);
    }

    popListe.show(cComp, 0, cComp.getHeight());

    mjtList.clearSelection();
    // La ligne active du MADonnee est
    mjtList.selectRow(mdListe.getLigneActive());
  }

  /**
   * Lier une liste a des elements graphiques.
   * 
   * @param cFocus element a surveiller
   * @param cOthers autres elements a surveiller
   */
  public void linkListWithFields(Component cFocus, Component... cOthers) {
    this.cComp = cFocus;
    cFocus.addMouseListener(this);
    for (Component c : cOthers)
      c.addMouseListener(this);
  }
}
