/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : StringTransferHandler.java - CREATION : 2005/01/11
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public abstract class StringTransferHandler extends TransferHandler {

  protected abstract String exportString(JComponent c);

  protected abstract void importString(JComponent c, String str);

  protected abstract void cleanup(JComponent c, boolean remove);

  @Override
  protected Transferable createTransferable(JComponent c) {
    return new StringSelection(exportString(c));
  }

  @Override
  @SuppressWarnings("unused")
  public int getSourceActions(JComponent c) {
    return COPY_OR_MOVE;
  }

  @Override
  public boolean importData(JComponent c, Transferable t) {
    if (canImport(c, t.getTransferDataFlavors())) {
      try {
        String str = (String) t.getTransferData(DataFlavor.stringFlavor);
        importString(c, str);
        return true;
      } catch (UnsupportedFlavorException ufe) {} catch (IOException ioe) {}
    }

    return false;
  }

  @Override
  @SuppressWarnings("unused")
  protected void exportDone(JComponent c, Transferable data, int action) {
    cleanup(c, action == MOVE);
  }

  @Override
  @SuppressWarnings("unused")
  public boolean canImport(JComponent c, DataFlavor[] flavors) {
    for (int i = 0; i < flavors.length; i++) {
      if (DataFlavor.stringFlavor.equals(flavors[i])) {
        return true;
      }
    }
    return false;
  }

}