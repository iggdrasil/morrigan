/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAList.java - CREATION : 2005/01/14 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com> 
 * # 
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING. 
 * #######################################################################
 */
package morrigan.admin.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListSelectionModel;

import org.apache.commons.lang.Validate;

import morrigan.admin.database.DBManager;
import morrigan.admin.database.MALightTable;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.table.MJTable;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAList extends MJPanelQuad implements MouseListener, KeyListener,
    FocusListener {

  private static final int HAUTEUR_ELT_DEF = 20;

  private static final int HAUTEUR_PANEL_DEF = 300;

  private static final int LARGEUR_COL_DEF = 100;

  private static final int LARGEUR_COL_ID = 40;

  private MALightTable mTable = null;

  private MADonnees mdListe = null;

  private MADonnees mdTableDest = null;

  private Component mif = null;

  @SuppressWarnings("unused")
  private Integer iOldID = null;

  private MJTable mjtList = null;

  private MJPopupMenu popListe = null;

  private String[] sNomsChampsDest = null;

  private String[] sNomsChampsList = null;

  private final StringBuffer sSelectFrom;

  private final StringBuffer sbOrder;

  /**
   * Constructor.
   * 
   * @param foreignTableName foreign table name
   * @param foreignFieldNames foreign fields' names
   * @param mdTableLinked data model of the linked table
   * @param fieldNames fields name receiving the selection
   */
  public MAList(MALightTable foreignTableName, String[] foreignFieldNames,
      MADonnees mdTableLinked, String[] fieldNames) {
    super(BORDURE.RAISED, 1, 1);
    this.mTable = foreignTableName;
    this.sNomsChampsList = foreignFieldNames;
    this.mdTableDest = mdTableLinked;
    this.sNomsChampsDest = fieldNames;

    sSelectFrom = new StringBuffer("SELECT ");
    sbOrder = new StringBuffer(" ORDER BY ").append(mTable.getTrig()).append(
        "_id ");

    Validate.notNull(foreignTableName, "No table in database is specified");
    Validate.notEmpty(foreignFieldNames, "No table field in database is specified");
    Validate.notNull(mdTableLinked, "No target model is specified");
    Validate.notEmpty(fieldNames, "No target field is specified");
    Validate.isTrue(foreignFieldNames.length >= fieldNames.length, "Too many target fields");

    sSelectFrom.append(foreignFieldNames[0]);
    for (int iChamp = 1; iChamp < foreignFieldNames.length; iChamp++) {
      sSelectFrom.append(", ").append(foreignFieldNames[iChamp]);
    }

    sSelectFrom.append(" FROM ").append(foreignTableName.getNom());

    MLog.debug("MAList", "MAList", "Creation de la requete:"
        + sSelectFrom.toString());
  }

  /**
   * Appliquer la saisie au MDonnees lie.
   */
  private void applySelectedRow() {
    for (int iChampDest = 0; iChampDest < sNomsChampsDest.length; iChampDest++) {
      Object o = mdListe.getValueAt(mjtList.getSelectedRow(),
          sNomsChampsList[iChampDest]);
      mdTableDest.applyNewValue(o, sNomsChampsDest[iChampDest]);
    }
    closePopup();
  }

  /**
   * Construire l'ecran.
   */
  private void buildScreen() {
    if (mjtList == null) {
      mjtList = new MJTable(mdListe, 1, 1) {

        @Override
        @SuppressWarnings("unused")
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
      mjtList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      add(new MJScrollPane(mjtList), 0, 0);

      mjtList.addKeyListener(this);
      mjtList.addMouseListener(this);
    } else {
      mjtList.setModel(mdListe);
    }

    // TODO: calculer proprement la taille de la popup
    int iHauteur = (mjtList.getRowCount() + 1) * HAUTEUR_ELT_DEF;
    if (iHauteur > HAUTEUR_PANEL_DEF) {
      iHauteur = HAUTEUR_PANEL_DEF;
    }
    imposerTaille(mjtList.getColumnCount() * LARGEUR_COL_DEF, iHauteur);

    mjtList.getColumnModel().getColumn(0).setPreferredWidth(LARGEUR_COL_ID);
    mjtList.getColumnModel().getColumn(0).setMinWidth(LARGEUR_COL_ID);
    mjtList.getColumnModel().getColumn(0).setMaxWidth(LARGEUR_COL_ID);
  }

  /**
   * Fermer la popup de description.
   */
  public void closePopup() {
    if (popListe != null) {
      popListe.setVisible(false);
    }
  }

  /**
   * Recuperer en base les informations a afficher dans le tableau.
   * 
   * @return <code>true</code> si la recherche s'est bien passe;
   *         <code>false</code> sinon.
   */
  public boolean extractListDataFromBase() {
    mdListe = new MADonnees(sNomsChampsList);
    // Purge prealable des donnees
    mdListe.clearData();
    // On rajoute une ligne vierge au debut.
    mdListe.addRow();

    ResultSet rsDonnees = null;
    try {
      // TODO: les clauses sont-elles possibles ?

      // On recupere toutes les donnees de la table.
      MLog.debug("MAList", "extractDataFromBase", sSelectFrom.toString()
          + sbOrder.toString());
      rsDonnees = DBManager.select(sSelectFrom.toString() + sbOrder.toString());

      if (rsDonnees == null) {
        MLog.error("MAList", "extractDataFromBase",
            "Pas de connexion active...");
        return false;
      }

      List<Object> vLigne = null;
      Object oDonnee = null;

      // Parcours des lignes de resultat et insertion dans le tableau
      while (rsDonnees.next()) {
        vLigne = new ArrayList<Object>();
        for (int i = 0; i < mdListe.getColumnCount(); i++) {
          try {
            oDonnee = rsDonnees.getObject(mdListe.getColumnName(i));
            vLigne.add(oDonnee != null ? oDonnee : "");
          } catch (Exception e) {
            vLigne.add("?");
            MLog.error("MAList", "extractDataFromBase",
                "Erreur lecture des donnees de la base.", e);
          }
        }

        mdListe.addRow(vLigne);
      }
      
      if (vLigne == null) {
        MLog.debug("MAList", "extractDataFromBase", "Table " + mTable.getNom()
            + " : aucune donnee trouvee...");
        return false;
      }
    } catch (Exception e) {
      MLog.error("MAList", "extractDataFromBase",
          "Erreur de recuperation d'infos.", e);
    }
    return true;
  }

  public Integer getID() {
    final Object oValue = ((MIAField) mif).getValue();
    return oValue != null && !oValue.equals("") ? new Integer(oValue.toString())
        : null;
  }

  /**
   * Recuperer en base le libelle associe a l'identifiant fourni.
   * 
   * @param sIDValue
   * @return libelle de l'ID
   */
  public boolean extractLibelleFromBase(String sIDValue) {
    // Si l'ancien ID recherche est le meme, inutile de lancer la requete
    // if (sIDValue.equals(iOldID == null ? "" : iOldID.toString()))
    // return true;

    MADonnees mdReponse = new MADonnees(sNomsChampsList);
    // Purge prealable des donnees
    mdReponse.clearData();
    // On rajoute une ligne vierge au debut.
    mdReponse.addRow();

    ResultSet rsDonnees = null;
    try {
      // TODO: les clauses sont-elles possibles ?

      // On recupere toutes les donnees de la table.
      MLog.debug("MAList", "extractDataFromBase", sSelectFrom.toString()
          + " WHERE " + sNomsChampsList[0] + "=" + sIDValue);
      rsDonnees = DBManager.select(sSelectFrom.toString() + " WHERE "
          + sNomsChampsList[0] + "=" + sIDValue);

      if (rsDonnees == null) {
        MLog.error("MAList", "extractDataFromBase",
            "Pas de connexion active...");
        return false;
      }

      List<Object> vLigne = null;
      Object oDonnee = null;

      // Parcours des lignes de resultat et insertion dans le tableau
      while (rsDonnees.next()) {
        vLigne = new ArrayList<Object>();
        for (int i = 0; i < mdReponse.getColumnCount(); i++) {
          try {
            oDonnee = rsDonnees.getObject(mdReponse.getColumnName(i));
            mdTableDest.setValueAt(oDonnee, sNomsChampsDest[i]);
            // FIXME: il faut retrouver le nom de la colonne cible ! (on a
            // seulement le
            // nom reel)
          } catch (Exception e) {
            vLigne.add("?");
            MLog.error("MAList", "extractDataFromBase",
                "Erreur lecture des donnees de la base.", e);
          }
        }

        mdReponse.addRow(vLigne);
      }
      
      if (vLigne == null) {
        MLog.debug("MAList", "extractDataFromBase", "Table " + mTable.getNom()
            + " : aucune donnee trouvee...");
        return false;
      }
    } catch (Exception e) {
      MLog.error("MAList", "extractDataFromBase",
          "Erreur de recuperation d'infos.", e);
    }
    iOldID = (sIDValue == null ? null : new Integer(sIDValue));
    return true;
  }

  public void keyPressed(KeyEvent e) {
    // Si validation par Entree
    if (e.getKeyCode() == 10) {
      applySelectedRow();
    }
  }

  @SuppressWarnings("unused")
  public void keyReleased(KeyEvent e) {}

  @SuppressWarnings("unused")
  public void keyTyped(KeyEvent e) {}

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseClicked(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mousePressed(MouseEvent e) {
  // onActionOnButton(e);
  }

  public void mouseReleased(MouseEvent e) {
    onActionOnButton(e);
  }

  private void onActionOnButton(MouseEvent e) {
    // Double clic sur une ligne de la popup
    if (e.getSource() == mjtList) {
      if (e.getClickCount() >= 2) {
        applySelectedRow();
      }
    }
    // Ou clic sur le champ a ecouter
    else if (((MIAField) mif).isBddEnabled() || isRechercheFenActivee(mif)) {
      if (e.getSource() == mif && e.getClickCount() >= 2) {
        if (((MIAField) mif).isBddEnabled()) {
          mif.setEnabled(true);
          mif.requestFocus();
        }
      } else if (!e.isPopupTrigger() && e.getButton() == 1)
      // Clic gauche (Pas tres propre mais bon...)
      {
        extractListDataFromBase();
        buildScreen();
        openPopup();
      }
    }
  }

  /**
   * Parcourir les parents pour acceder a la fenetre et determiner si le mode
   * RECHERCHE est actif ou non
   * 
   * @param c Champ ID
   * @return <code>true</code> si le mode est actif; <code>false</code>
   *         sinon.
   */
  private boolean isRechercheFenActivee(Component c) {
    Container cParent = c.getParent();
    if (cParent != null) {
      if (cParent instanceof MAFenetre) {
        return ((MAFenetre) cParent).getModeAffichage() == MEAffichage.RECHERCHE;
      }

      return isRechercheFenActivee(cParent);
    }

    return false;
  }

  /**
   * Ouvrir la popup.
   */
  public void openPopup() {
    if (popListe == null) {
      popListe = new MJPopupMenu();
      popListe.add(this);
    }

    popListe.show(mif, 0, mif.getHeight());

    // On recherche dans le tableau de ce qui est dans la fenetre et on le
    // reselectionne par
    // defaut.
    // ATTENTION: les types des objets retournes par la requete ne sont parfois
    // pas exactement
    // ce qui est dans le tableau... (Ex: Short vs Integer)
    Object oRepere = mdTableDest.getValueAt(mdTableDest.getLigneActive(),
        sNomsChampsDest[0]);
    int iLigneSelectionnee = 0;
    if (oRepere != null) {
      oRepere = oRepere.toString();
      for (int iLigne = 0; iLigne < mjtList.getRowCount(); iLigne++) {
        final Object oListValue = mjtList.getValueAt(iLigne, 0);
        if (oListValue != null && oRepere.equals(oListValue.toString())) {
          iLigneSelectionnee = iLigne;
          break;
        }

      }
    }
    mjtList.changeSelection(iLigneSelectionnee, 0, true, false);
  }

  public void linkListWithFields(Component cFocus, Component... cOthers) {
    this.mif = cFocus;
    cFocus.addMouseListener(this);
    cFocus.addFocusListener(this);
    for (Component c : cOthers) {
      c.addMouseListener(this);
      c.addFocusListener(this);
    }
  }

  @SuppressWarnings("unused")
  public void focusGained(FocusEvent e) {}

  public void focusLost(FocusEvent e) {
    if (e.getSource() == mif) {
      try {
        if (((MIAField) mif).getValue().toString().equals("")) {
          for (String sColonne : sNomsChampsDest) {
            mdTableDest.setValueAt("", sColonne);
          }
          mif.setEnabled(false);
          iOldID = null;
        } else if (extractLibelleFromBase(((MIAField) mif).getValue()
            .toString())) {
          mif.setEnabled(false);
        } else {
          mif.requestFocus();
        }
      } catch (Exception ex) {
        MLog.error("MAList", "focusLost",
            "Perte de focus, recherche du libelle", ex);
      }
    }

  }
}
