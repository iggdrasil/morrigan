/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAJScrollPane.java - CREATION : 2004/01/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
/*
 * Created on 5 janv. 2004
 */
package morrigan.admin.ui.field;

import java.awt.Component;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.field.util.MTextArea;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.table.MJTable;

/**
 * Autour d'une zone de texte, il est souvent utile voire indispensable de
 * placer des barres de défilement. Ce panel permet de positionner une barre
 * verticale.
 * 
 * @author armel
 */
public class MAJScrollPane extends MJScrollPane implements MIAField {
  private MAJScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
    super(view, vsbPolicy, hsbPolicy);
  }

  public MAJScrollPane(MTextArea mta) {
    this(mta, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  public MAJScrollPane(MJPanel mjp) {
    this(mjp, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_NEVER);
  }

  public MAJScrollPane(MJTable mjt) {
    this(mjt, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  public void setBddEnabled(boolean bBddEnabled) {}

  public boolean isBddEnabled() {
    return true;
  }

  public void setMode(AFFICHAGE aMode) {
    if (getViewport().getComponent(0) instanceof MIAField) {
      ((MIAField) getViewport().getComponent(0)).setMode(aMode);
      this.repaint();
    }
  }
  
  public AFFICHAGE getMode() {
    if (getViewport().getComponent(0) instanceof MIAField) {
      return ((MIAField) getViewport().getComponent(0)).getMode();
    }
    return null;
  }


  public boolean isIDLibelle() {
    return false;
  }

  public void setNatureChpBdd(MENatureChampBdd mNatChp) {
    if (getViewport().getComponent(0) instanceof MIAField)
      ((MIAField) getViewport().getComponent(0)).setNatureChpBdd(mNatChp);
  }

  public MENatureChampBdd getNatureChpBdd() {
    return null;
  }
}