/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIAField.java - CREATION : 2005/01/10
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field;

import java.awt.Point;
import java.awt.event.MouseListener;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.MIQuadComponent;
import morrigan.graphique.field.util.MIField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public interface MIAField extends MIField, MIQuadComponent {
  /**
   * Display type : EDIT or SEARCH.
   * 
   * @author Armel
   */
  public enum AFFICHAGE {
    SAISIE,
    RECHERCHE
  }

  /**
   * Definit si l'utilisateur peut saisir des donnees qui seront modifiees en
   * base.
   * 
   * @param bBddEnabled
   */
  void setBddEnabled(boolean bBddEnabled);

  boolean isBddEnabled();

  boolean isIDLibelle();

  void setNatureChpBdd(MENatureChampBdd mNatChp);

  MENatureChampBdd getNatureChpBdd();

  /**
   * Definit le passage en mode RECHERCHE ou SAISIE.
   * 
   * @param mode
   */
  void setMode(AFFICHAGE mode);
  
  AFFICHAGE getMode();

  void addMouseListener(MouseListener ml);

  Point getLocationOnScreen();
}
