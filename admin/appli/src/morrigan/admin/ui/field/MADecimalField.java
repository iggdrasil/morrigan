/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MADecimalField.java - CREATION : 2004/08/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.field.MDecimalField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MADecimalField extends MDecimalField implements MIAField {
  
  private boolean bBddEnabled = true;

  private MENatureChampBdd mNatChp = null;

  private AFFICHAGE mode;

  public MADecimalField(int iHauteurCell, int iLargeurCell, int precision) {
    super(iHauteurCell, iLargeurCell, precision);
  }
  
  public MADecimalField(int iHauteurCell, int iLargeurCell) {
    super(iHauteurCell, iLargeurCell);
  }

  public MADecimalField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    this(iHauteurCell, iLargeurCell);
    setMandatory(bMandatory);
  }

  public boolean isBddEnabled() {
    return bBddEnabled;
  }

  public void setBddEnabled(boolean bBddEnabled) {
    this.bBddEnabled = bBddEnabled;
  }

  public void setMode(AFFICHAGE mode) {
    this.mode = mode;
    this.repaint();
  }

  public AFFICHAGE getMode() {
    return mode;
  }  
  
  public boolean isIDLibelle() {
    return mNatChp == MENatureChampBdd.IDVTABLEEXT;
  }

  public void setNatureChpBdd(MENatureChampBdd mNatChp) {
    this.mNatChp = mNatChp;
  }

  public MENatureChampBdd getNatureChpBdd() {
    return mNatChp;
  }

}