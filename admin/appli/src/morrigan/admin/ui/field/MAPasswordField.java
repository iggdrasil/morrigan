/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAPasswordField.java - CREATION : 2009/01/31
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.field.MPasswordField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAPasswordField extends MPasswordField implements MIAField {
  private boolean bBddEnabled = true;

  private MENatureChampBdd mNatChp = null;

  private AFFICHAGE mode;

  public MAPasswordField(int iHauteurCell, int iLargeurCell, boolean bMandatory) {
    super(iHauteurCell, iLargeurCell, bMandatory);
  }

  public MAPasswordField(int iHauteurCell, int iLargeurCell) {
    this(iHauteurCell, iLargeurCell, false);
  }

  public boolean isBddEnabled() {
    return bBddEnabled;
  }

  public void setBddEnabled(boolean bBddEnabled) {
    this.bBddEnabled = bBddEnabled;
  }

  public void setMode(AFFICHAGE mode) {
    this.mode = mode;
    this.repaint();
  }

  public AFFICHAGE getMode() {
    return mode;
  }

  public boolean isIDLibelle() {
    return mNatChp == MENatureChampBdd.IDVTABLEEXT;
  }

  public void setNatureChpBdd(MENatureChampBdd mNatChp) {
    this.mNatChp = mNatChp;
  }

  public MENatureChampBdd getNatureChpBdd() {
    return mNatChp;
  }

}