/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MDictionnaire.java - CREATION : 2004/09/18
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field;

import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.graphique.field.MTextAreaField;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MATextAreaField extends MTextAreaField implements MIAField {
  
  private boolean bBddEnabled = true;

  private MENatureChampBdd mNatChp = null;

  private AFFICHAGE mode;

  public MATextAreaField(String sTexte, int iHauteurCell, int iLargeurCell) {
    super(sTexte, iHauteurCell, iLargeurCell);
  }
  
  @Override
  protected MATextField createLightTextField(){
    return new MATextField(1, 1);
  }

  public MATextAreaField(String sTexte, int iHauteurCell, int iLargeurCell,
      boolean bMandatory) {
    this(sTexte, iHauteurCell, iLargeurCell);
    this.setMandatory(bMandatory);
  }

  public MATextAreaField(int iHauteurCell, int iLargeurCell) {
    this("", iHauteurCell, iLargeurCell);
  }

  public boolean isBddEnabled() {
    return bBddEnabled;
  }

  public void setBddEnabled(boolean bBddEnabled) {
    this.bBddEnabled = bBddEnabled;
  }

  public void setMode(AFFICHAGE mode) {
    this.mode = mode;
    this.getField().setMode(mode);
    this.repaint();
  }

  public AFFICHAGE getMode() {
    return mode;
  }  

  public boolean isIDLibelle() {
    return mNatChp == MENatureChampBdd.IDVTABLEEXT;
  }

  public void setNatureChpBdd(MENatureChampBdd mNatChp) {
    this.mNatChp = mNatChp;
  }

  public MENatureChampBdd getNatureChpBdd() {
    return mNatChp;
  }
  
  @Override
  public MIAField getField() {
    return (MIAField) super.getField();
  }

}