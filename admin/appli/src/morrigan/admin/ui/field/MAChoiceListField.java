/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAChoiceListField.java - CREATION : 2006/05/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field;

import morrigan.admin.ui.field.util.MAChoiceList;

public class MAChoiceListField extends MATextField {
  public MAChoiceListField(int iHauteurCell, int iLargeurCell,
      String[] sColonnes, String[] sValeurs) {
    super(iHauteurCell, iLargeurCell);

    addMouseListener(new MAChoiceList(this, sColonnes, sValeurs));
    setRequestFocusEnabled(true);
  }
}