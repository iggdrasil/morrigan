/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MASetList.java - CREATION : 2005/12/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListSelectionModel;

import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MABooleanField;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.table.MJTable;

public class MABooleanList extends MJPanelQuad implements MouseListener {
  private static final int HAUTEUR_ELT_DEF = 20;

  private static final int HAUTEUR_PANEL_DEF = 300;

  private static final int LARGEUR_COL_DEF = 100;

  private static final int LARGEUR_COL_ID = 100;

  private MADonnees mdListe = null;

  private MJTable mjtList = null;

  private MJPopupMenu popListe = null;

  private MABooleanField mbf = null;

  private String[] sNomsChampsList = null;

  public MABooleanList(MABooleanField mbf) {
    super(BORDURE.RAISED, 1, 1);
    this.mbf = mbf;
    sNomsChampsList = new String[] { "Valeur" };
  }

  /**
   * Appliquer la saisie au MDonnees lie.
   */
  private void applySelectedRow() {
    Object o = mdListe.getValueAt(mjtList.getSelectedRow(), sNomsChampsList[0]);
    mbf.setValue(o);
    closePopup();
  }

  /**
   * Construire l'ecran.
   */
  private void buildScreen() {
    // JComboBox
    if (mjtList == null) {
      mjtList = new MJTable(mdListe, 1, 1) {
        @Override
        @SuppressWarnings("unused")
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
      mjtList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      add(new MJScrollPane(mjtList), 0, 0);

      mjtList.addMouseListener(this);
    } else
      mjtList.setModel(mdListe);

    // TODO: calculer proprement la taille de la popup
    int iHauteur = (mjtList.getRowCount() + 1) * HAUTEUR_ELT_DEF;
    if (iHauteur > HAUTEUR_PANEL_DEF)
      iHauteur = HAUTEUR_PANEL_DEF;
    imposerTaille(mjtList.getColumnCount() * LARGEUR_COL_DEF, iHauteur);

    mjtList.getColumnModel().getColumn(0).setPreferredWidth(LARGEUR_COL_ID);
    mjtList.getColumnModel().getColumn(0).setMinWidth(LARGEUR_COL_ID);
    mjtList.getColumnModel().getColumn(0).setMaxWidth(LARGEUR_COL_ID);
  }

  /**
   * Fermer la popup de description.
   */
  public void closePopup() {
    if (popListe != null)
      popListe.setVisible(false);
  }

  /**
   * Construit le tableau.
   * 
   * @return <code>true</code> si la construction s'est bien passee;
   *         <code>false</code> sinon.
   */
  public boolean createList() {
    mdListe = new MADonnees(sNomsChampsList);
    // Purge prealable des donnees
    mdListe.clearData();

    List<Object> vLigne = null;
    {
      vLigne = new ArrayList<Object>();
      vLigne.add("");
      mdListe.addRow(vLigne);
    }
    {
      vLigne = new ArrayList<Object>();
      vLigne.add("false");
      mdListe.addRow(vLigne);
    }
    {
      vLigne = new ArrayList<Object>();
      vLigne.add("true");
      mdListe.addRow(vLigne);
    }
    return true;
  }

  @SuppressWarnings("unused")
  public void mouseClicked(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mousePressed(MouseEvent e) {}

  public void mouseReleased(MouseEvent e) {
    // Double clic sur une ligne de la popup
    if (e.getSource() == mjtList) {
      if (e.getClickCount() >= 2)
        applySelectedRow();
    }
    // Ou clic sur le champ a ecouter
    else if (((MIAField) mbf).isBddEnabled() || isRechercheFenActivee(mbf)) {

      createList();
      buildScreen();
      openPopup();
    }
  }

  /**
   * Parcourir les parents pour acceder a la fenetre et determiner si le mode
   * RECHERCHE est actif ou non
   * 
   * @param c Champ ID
   * @return <code>true</code> si le mode est actif; <code>false</code>
   *         sinon.
   */
  private boolean isRechercheFenActivee(Component c) {
    Container cParent = c.getParent();
    if (cParent != null) {
      if (cParent instanceof MAFenetre)
        return ((MAFenetre) cParent).getModeAffichage() == MEAffichage.RECHERCHE;

      return isRechercheFenActivee(cParent);
    }

    return false;
  }

  /**
   * Ouvrir la popup.
   */
  public void openPopup() {
    if (popListe == null) {
      popListe = new MJPopupMenu();
      popListe.add(this);
    }

    popListe.show(mbf, 0, mbf.getHeight());

    // On recherche dans le tableau de ce qui est dans la fenetre et on le
    // reselectionne par
    // defaut.
    // ATTENTION: les types des objets retournes par la requete ne sont parfois
    // pas exactement
    // ce qui est dans le tableau... (Ex: Short vs Integer)
    Object oRepere = mbf.getValue();
    int iLigneSelectionnee = 0;
    if (oRepere != null) {
      oRepere = oRepere.toString();
      for (int iLigne = 0; iLigne < mjtList.getRowCount(); iLigne++) {
        final Object oListValue = mjtList.getValueAt(iLigne, 0);
        if (oListValue != null && oRepere.equals(oListValue.toString())) {
          iLigneSelectionnee = iLigne;
          break;
        }

      }
    }
    mjtList.changeSelection(iLigneSelectionnee, 0, true, false);
  }
}
