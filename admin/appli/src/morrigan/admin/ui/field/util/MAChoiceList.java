/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAChoiceList.java - CREATION : 2006/05/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.field.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListSelectionModel;

import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MAChoiceListField;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.table.MJTable;

public class MAChoiceList extends MJPanelQuad implements MouseListener {
  private static final int HAUTEUR_ELT_DEF = 20;

  private static final int HAUTEUR_PANEL_DEF = 300;

  private static final int LARGEUR_COL_DEF = 100;

  private static final int LARGEUR_COL_ID = 100;

  private MADonnees mdListe = null;

  private MJTable mjtList = null;

  private MJPopupMenu popListe = null;

  private MAChoiceListField mlf = null;

  private String[] sNomsChampsList = null;

  public MAChoiceList(MAChoiceListField mlf, String[] sColonnes,
      Object[] oValeurs) {
    super(BORDURE.RAISED, 1, 1);
    this.mlf = mlf;
    sNomsChampsList = sColonnes;
    createList();
    setValues(oValeurs);
  }

  /**
   * Appliquer la saisie au MDonnees lie.
   */
  private void applySelectedRow() {
    Object o = mdListe.getValueAt(mjtList.getSelectedRow(), sNomsChampsList[0]);
    mlf.setValue(o);
    closePopup();
  }

  /**
   * Construire l'ecran.
   */
  private void buildScreen() {
    // JComboBox
    if (mjtList == null) {
      mjtList = new MJTable(mdListe, 1, 1) {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
      mjtList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      add(new MJScrollPane(mjtList), 0, 0);

      mjtList.addMouseListener(this);
    } else {
      mjtList.setModel(mdListe);
    }

    // TODO: calculer proprement la taille de la popup
    int hauteur = (mjtList.getRowCount() + 1) * HAUTEUR_ELT_DEF;
    if (hauteur > HAUTEUR_PANEL_DEF) {
      hauteur = HAUTEUR_PANEL_DEF;
    }
    int width = Math.max(mjtList.getColumnCount() * LARGEUR_COL_DEF, mlf
        .getWidth());
    imposerTaille(width, hauteur);

    mjtList.getColumnModel().getColumn(0).setPreferredWidth(LARGEUR_COL_ID);
    mjtList.getColumnModel().getColumn(0).setMinWidth(LARGEUR_COL_ID);
  }

  /**
   * Fermer la popup de description.
   */
  public void closePopup() {
    if (popListe != null)
      popListe.setVisible(false);
  }

  /**
   * Construit le tableau.
   */
  private void createList() {
    mdListe = new MADonnees(sNomsChampsList);
    mdListe.clearData();
  }

  public void setValues(Object[] oValeurs) {
    // Purge prealable des donnees
    mdListe.clearData();

    List<Object> vLigne = null;

    for (Object oValeur : oValeurs) {
      vLigne = new ArrayList<Object>();
      vLigne.add(oValeur);
      mdListe.addRow(vLigne);
    }
  }

  @SuppressWarnings("unused")
  public void mouseClicked(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mousePressed(MouseEvent e) {}

  public void mouseReleased(MouseEvent e) {
    // Double clic sur une ligne de la popup
    if (e.getSource() == mjtList) {
      if (e.getClickCount() >= 2)
        applySelectedRow();
    }
    // Ou clic sur le champ a ecouter
    else if ((((MIAField) mlf).isBddEnabled() || isRechercheFenActivee(mlf))
        && mlf.isEnabled()) {
      buildScreen();
      openPopup();
    }
  }

  /**
   * Parcourir les parents pour acceder a la fenetre et determiner si le mode
   * RECHERCHE est actif ou non
   * 
   * @param c Champ ID
   * @return <code>true</code> si le mode est actif; <code>false</code>
   *         sinon.
   */
  private boolean isRechercheFenActivee(Component c) {
    Container cParent = c.getParent();
    if (cParent != null) {
      if (cParent instanceof MAFenetre)
        return ((MAFenetre) cParent).getModeAffichage() == MEAffichage.RECHERCHE;

      return isRechercheFenActivee(cParent);
    }

    return false;
  }

  /**
   * Ouvrir la popup.
   */
  public void openPopup() {
    if (popListe == null) {
      popListe = new MJPopupMenu();
      popListe.add(this);
    }

    popListe.show(mlf, 0, mlf.getHeight());

    // On recherche dans le tableau de ce qui est dans la fenetre et on le
    // reselectionne par
    // defaut.
    // ATTENTION: les types des objets retournes par la requete ne sont parfois
    // pas
    // exactement
    // ce qui est dans le tableau... (Ex: Short vs Integer)
    Object oRepere = mlf.getValue();
    int iLigneSelectionnee = 0;
    if (oRepere != null) {
      oRepere = oRepere.toString();
      for (int iLigne = 0; iLigne < mjtList.getRowCount(); iLigne++) {
        final Object oListValue = mjtList.getValueAt(iLigne, 0);
        if (oListValue != null && oRepere.equals(oListValue.toString())) {
          iLigneSelectionnee = iLigne;
          break;
        }

      }
    }
    mjtList.changeSelection(iLigneSelectionnee, 0, true, false);
  }
}