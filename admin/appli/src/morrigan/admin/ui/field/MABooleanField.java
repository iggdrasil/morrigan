/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MABooleanField.java - CREATION : 2005/12/05
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.admin.ui.field;

import morrigan.admin.ui.field.util.MABooleanList;

public class MABooleanField extends MATextField {
  public MABooleanField(int iHauteurCell, int iLargeurCell) {
    super(iHauteurCell, iLargeurCell);

    addMouseListener(new MABooleanList(this));
    setEditable(false);
  }
}
