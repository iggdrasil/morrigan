/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MIMenuElement - CREATION : 2005/11/28
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import java.awt.event.ActionListener;

import morrigan.admin.ui.enumeration.MEActionMenu;
import morrigan.admin.ui.fenetre.MASysWindow;

public interface MIMenuElement {
  public MEActionMenu getActionMenu();

  public MASysWindow getFenetre();

  public void addActionListener(ActionListener al);

}
