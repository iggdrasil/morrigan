/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEAWindow.java - CREATION : 2005/08/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.enumeration;

import morrigan.admin.MAFenAdmin;
import morrigan.admin.specif.MAFenAbout;
import morrigan.admin.specif.MAFenCarteMonde;
import morrigan.admin.specif.MAFenLog;
import morrigan.admin.specif.MAFenPreferences;
import morrigan.log.MLog;

/**
 * L'ensemble des fenetres qui peuvent etre ouvertes est contenu dans cette
 * classe.<br>
 * On y recense les fenetres generiques et specifiques (liee a une Classe).
 * 
 * @author armel
 */
public enum MEAWindow {
  MENU("menu", MAFenAdmin.class),
  LOG("log", MAFenLog.class),
  APROPOS("apropos", MAFenAbout.class),

  CARTEMONDE("cartemonde", MAFenCarteMonde.class),
  CHEMIN(),
  CLASSE(),
  COMPAGNIE(),
  JOUEUR(),
  LIEU(),
  MJ(),
  MONDE(),
  PERSONNAGE(),
  PREFERENCES("preferences", MAFenPreferences.class),
  REGION(),

  acces_connu(),
  action(),
  arme(),
  arme_gen(),
  armure(),
  armure_gen(),
  armure_gen_degat(),
  armure_gen_zone(),
  banque(),
  caracteristique(),
  categorie(),
  classe_niveau(),
  debit_element(),
  depot_element(),
  element(),
  element_gen(),
  forme(),
  interdit_classe(),
  lieu_action(),
  lieu_saisonnier(),
  maladie(),
  matiere(),
  monde_connu(),
  monnaie(),
  munition(),
  nature_caracteristique(),
  parametre(),
  perso_carac(),
  perso_classe(),
  perso_classe_int(),
  perso_maladie(),
  perso_monnaie(),
  place_compagnie(),
  prerequis_carac(),
  prerequis_classe(),
  race(),
  recharge(),
  recharge_gen(),
  region_connue(),
  route(),
  saison(),
  secteur(),
  sexe(),
  taille(),
  technologie(),
  type_caracteristique(),
  type_chemin(),
  type_classe(),
  type_degat(),
  type_element(),
  type_lieu(),
  type_maladie(),
  type_monde(),
  type_personnage(),
  type_poste(),
  type_region(),
  type_secteur(),
  type_vaisseau(),
  typelieu_action(),
  vaisseau(),
  vaisseau_gen(),
  vaisseau_gen_poste(),
  vente_element(),
  zone();

  /**
   * Nom systeme de la fenetre.
   */
  private String sNomSysteme = null;

  /**
   * Eventuelle classe. Si <code>null</code>, la fenetre est generique.
   */
  private Class cClasse = null;

  /**
   * Constructeur de referenceur de fenetre.
   * 
   * @param sNomSysFenetre Nom systeme de la fenetre
   * @param cClassFenetre Classe de la fenetre (specifique)
   */
  MEAWindow(String sNomSysFenetre, Class cClassFenetre) {
    this.sNomSysteme = sNomSysFenetre;
    this.cClasse = cClassFenetre;
  }

  /**
   * Constructeur de referenceur de fenetre sans nom de fenetre specifique: ce
   * nom de fenetre est celui de la table.
   */
  MEAWindow() {
    sNomSysteme = this.toString().toLowerCase();
  }

  /**
   * Retourner le nom systeme de la fenetre.
   * 
   * @return Nom systeme de la fenetre
   */
  public String getNomSys() {
    return sNomSysteme;
  }

  /**
   * Retourner la classe de la fenetre
   * 
   * @return classe si fenetre specifique; <code>null</code> si fenetre
   *         generique.
   */
  public Class getClasse() {
    return cClasse;
  }

  /**
   * Retourner le referenceur de fenetre a partir d'un nom systeme.<br>
   * 
   * @param sNomFenetre Nom systeme de la fenetre
   * @return Referenceur de la fenetre; <code>null</code> si rien trouve
   */
  static public MEAWindow getWindowParNomSys(String sNomFenetre) {
    for (MEAWindow window : values()) {
      if (window.getNomSys().equals(sNomFenetre))
        return window;
    }

    MLog.error("MEAWindow", "getFenetreParNom",
        "Aucune fenetre trouv�ee pour \"" + sNomFenetre + '"');
    return null;
  }
}