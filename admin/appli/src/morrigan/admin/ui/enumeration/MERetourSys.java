/*
 * Cree le 25 déc. 2005
 * Ce fichier appartient au projet Morrigan.
 */
package morrigan.admin.ui.enumeration;

import morrigan.admin.util.MADialogue;

public enum MERetourSys {
  OK(MADialogue.MESSAGE.INFORMATION, "Ok."),
  OK_ENREG_INSERE(MADialogue.MESSAGE.INFORMATION,
      "Enregistrement correctement insere."),
  OK_ENREG_MAJ(MADialogue.MESSAGE.INFORMATION,
      "Enregistrement correctement mis a jour."),
  OK_AUCUN_RESULTAT(MADialogue.MESSAGE.INFORMATION, "Aucune donnees trouvee."),
  ERREUR_MAJ(MADialogue.MESSAGE.ERREUR, "Erreur durant la sauvegarde."),
  ERREUR_INSERTION(MADialogue.MESSAGE.ERREUR,
      "Erreur durant la sauvegarde de la nouvelle donnee."),
  ERREUR_RIEN_A_FAIRE(MADialogue.MESSAGE.ERREUR, "Rien a faire !?"),
  ERREUR_SEQUENCEUR(MADialogue.MESSAGE.ERREUR, "Erreur de sequenceur."),
  ERREUR_CONNEXION_NON_ACTIVE(MADialogue.MESSAGE.ERREUR,
      "Pas de connexion active..."), ;

  private MADialogue.MESSAGE mMessage = null;

  private String sRetour = null;

  MERetourSys(MADialogue.MESSAGE mMessage, String sRetour) {
    this.mMessage = mMessage;
    this.sRetour = sRetour;
  }

  public MADialogue.MESSAGE getMessage() {
    return mMessage;
  }

  public String getTexteRetour() {
    return sRetour;
  }

}
