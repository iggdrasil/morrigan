/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEASpecificWindow.java - CREATION : 2005/08/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.enumeration;

import morrigan.admin.MAFenAdmin;
import morrigan.admin.specif.MAFenAbout;
import morrigan.admin.specif.MAFenCarteMonde;
import morrigan.admin.specif.MAFenDetailMonde;
import morrigan.admin.specif.MAFenLog;
import morrigan.admin.specif.MAFenPreferences;
import morrigan.admin.specif.creation.CreationFen;
import morrigan.admin.specif.login.LoginFen;
import morrigan.log.MLog;

/**
 * L'ensemble des fenetres <b>specifiques</b> qui peuvent etre ouvertes est
 * contenu dans cette classe.
 * 
 * @author armel
 */
public enum MEASpecificWindow {
  /** Menu */
  MENU("menu", MAFenAdmin.class),
  /** Log */
  LOG("log", MAFenLog.class),
  /** World map */
  CARTEMONDE("cartemonde", MAFenCarteMonde.class),
  /** World map */
  DETAILMONDE("detailmonde", MAFenDetailMonde.class),
  /** Preferences */
  PREFERENCES("preferences", MAFenPreferences.class),
  /** About */
  APROPOS("apropos", MAFenAbout.class),
  /** Login */
  LOGIN("login", LoginFen.class),
  /** Creation assistee */
  CREATION("creation", CreationFen.class);

  /**
   * Nom systeme de la fenetre.
   */
  private String sNomSysteme = null;

  /**
   * Classe de fenetre specifique.
   */
  private Class<?> cClasse = null;

  /**
   * Constructeur de referenceur de fenetre specifique.
   * 
   * @param sNomSysFenetre Nom systeme de la fenetre
   * @param cClassFenetre Classe de la fenetre specifique
   */
  MEASpecificWindow(String sNomSysFenetre, Class<?> cClassFenetre) {
    this.sNomSysteme = sNomSysFenetre;
    this.cClasse = cClassFenetre;
  }

  /**
   * Retourner le nom systeme de la fenetre.
   * 
   * @return Nom systeme de la fenetre
   */
  public String getSysName() {
    return sNomSysteme;
  }

  /**
   * Retourner la classe de la fenetre
   * 
   * @return classe si fenetre specifique; <code>null</code> si fenetre
   *         generique.
   */
  public Class<?> getClazz() {
    return cClasse;
  }

  /**
   * Retourner le referenceur de fenetre a partir d'un nom systeme.<br>
   * 
   * @param sNomFenetre Nom systeme de la fenetre
   * @return Referenceur de la fenetre; <code>null</code> si rien trouve
   */
  public static MEASpecificWindow getSpecificWindowParNomSys(String sNomFenetre) {
    for (MEASpecificWindow fenetre : values()) {
      if (fenetre.getSysName().equals(sNomFenetre)) {
        return fenetre;
      }
    }

    MLog.error("MEASpecificWindow", "getFenetreParNom",
        "Aucune fenetre trouvee pour \"" + sNomFenetre + '"');
    return null;
  }
}