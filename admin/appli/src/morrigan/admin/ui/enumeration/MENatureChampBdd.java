/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MENatureChampBdd.java - CREATION : 2005/11/23
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.enumeration;

public enum MENatureChampBdd {
  IDTABLE,
  IDVTABLEEXT,
  TABLE,
  EXT;
}
