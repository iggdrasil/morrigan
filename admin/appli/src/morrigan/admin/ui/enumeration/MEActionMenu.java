/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEActionMenu.java - CREATION : 2005/11/27
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.enumeration;

import morrigan.log.MLog;

public enum MEActionMenu {
  OUVRIR_FENETRE,
  CREATION,
  TOGGLE,
  QUITTER,
  FERMER_TOUT,
  OUVRIR_FENETRES_MENU,
  SWITCH_USER;

  static public MEActionMenu getActionParNom(String sAction) {
    for (MEActionMenu action : values()) {
      if (action.toString().equals(sAction.toUpperCase()))
        return action;
    }

    MLog.error("MEActionMenu", "getActionParNom",
        "No action found for \"" + sAction + '"');
    return null;
  }
}
