/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MEAffichage.java - CREATION : 2005/11/08
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.enumeration;

/*
 * Created on 8 nov. 2005 Ce fichier appartient au projet Morrigan.
 */

public enum MEAffichage {
  CREATION,
  MODIFICATION,
  RECHERCHE,
  LECTURE
}