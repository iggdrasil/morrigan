/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenetre.java - CREATION : 2004/10/17
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.fenetre;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.DBManager;
import morrigan.admin.database.MALightTable;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.admin.ui.enumeration.MEReponseUtil;
import morrigan.admin.ui.enumeration.MERetourSys;
import morrigan.admin.ui.field.MADateField;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MADialogue;
import morrigan.admin.util.MADonnees;
import morrigan.admin.util.MAInfosColonne;
import morrigan.graphique.bouton.MBouton;
import morrigan.graphique.bouton.MJButton;
import morrigan.graphique.bouton.MJToggleButton;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.graphique.field.util.MIField;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.table.MDonnees;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAFenetre extends MJFramePosition implements ActionListener {

  public enum BOUTON {
    VALIDER,
    AJOUTER,
    SUPPRIMER,
    RECHERCHER,
    PRECEDENT,
    SUIVANT
  }

  /** Display mode */
  protected MEAffichage modeAffichage = MEAffichage.LECTURE;

  /** Validation button */
  protected final MJButton mjbValider;

  /** Addition button */
  protected final MJButton mjbAjouter;

  /** Suppression button */
  protected final MJButton mjbSupprimer;

  /** Research button */
  protected final MJToggleButton mjbRechercher;

  /** Preceeding button */
  protected final MJButton mjbPrecedent;

  /** Following button */
  protected final MJButton mjbSuivant;

  /** Buttons panel */
  protected MJPanelQuad mpqPrinc = null;

  /** SQL FROM as a string */
  private StringBuffer sbSelectFrom = null;

  /**
   * Constructor.
   * 
   * @param sTitre title
   * @param iHauteurCell
   * @param iLargeurCell
   */
  public MAFenetre(String sTitre, int iHauteurCell, int iLargeurCell) {
    super(sTitre, iLargeurCell, iHauteurCell);
    setContentPane(mpqPrinc = new MJPanelQuad(iLargeurCell, iHauteurCell));

    int iLargeurBouton = 2;
    int yBouton = 1;

    // Declarations
    mjbValider = new MJButton("Ok", iLargeurBouton, 1);
    mjbValider.setToolTipText("Valider les modifications");
    mjbValider.setEnabled(false);
    mjbAjouter = new MJButton("+", iLargeurBouton, 1);
    mjbAjouter.setToolTipText("Ajouter un enregistrement");
    mjbSupprimer = new MJButton("-", iLargeurBouton, 1);
    mjbSupprimer.setToolTipText("Supprimer l'enregistrement");
    mjbSupprimer.setEnabled(false);
    mjbRechercher = new MJToggleButton("?", iLargeurBouton, 1);
    mjbRechercher.setToolTipText("Saisir les criteres de recherche");
    mjbPrecedent = new MJButton("<", iLargeurBouton, 1);
    mjbPrecedent.setToolTipText("Enregistrement precedent");
    mjbSuivant = new MJButton(">", iLargeurBouton, 1);
    mjbSuivant.setToolTipText("Enregistrement suivant");

    // ListenersjpGeneral
    mjbValider.addActionListener(this);
    mjbAjouter.addActionListener(this);
    mjbSupprimer.addActionListener(this);
    mjbRechercher.addActionListener(this);
    mjbPrecedent.addActionListener(this);
    mjbSuivant.addActionListener(this);

    // Placements
    mpqPrinc.add(mjbValider, yBouton, 0);
    mpqPrinc.add(mjbAjouter, yBouton += iLargeurBouton + 1, 0);
    mpqPrinc.add(mjbSupprimer, yBouton += iLargeurBouton + 1, 0);
    mpqPrinc.add(mjbRechercher, yBouton += iLargeurBouton + 1, 0);
    mpqPrinc.add(mjbPrecedent, yBouton += iLargeurBouton + 1, 0);
    mpqPrinc.add(mjbSuivant, yBouton += mjbPrecedent.getLargeurCell(), 0);

    refreshEnabledBoutons();
  }

  /** {@inheritDoc} */
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == mjbValider) {
      onValiderPressed();
    } else if (e.getSource() == mjbAjouter) {
      onAjouterPressed();
    } else if (e.getSource() == mjbSupprimer) {
      onSupprimerPressed();
    } else if (e.getSource() == mjbPrecedent) {
      onPrecedentPressed();
    } else if (e.getSource() == mjbSuivant) {
      onSuivantPressed();
    } else if (e.getSource() == mjbRechercher) {
      onRechercherPressed();
    }
  }

  /**
   * @return 0
   */
  protected int onValiderPressed() {
    MLog.debug("MAFenetre", "onValiderPressed", "Validation");
    setModeAffichage(MEAffichage.LECTURE);
    return 0;
  }

  /**
   * @return 0
   */
  protected int onAjouterPressed() {
    MLog.debug("MAFenetre", "onAjouterPressed", "Ajout");
    setModeAffichage(MEAffichage.CREATION);
    return 0;
  }

  /**
   * @return 0
   */
  protected int onSupprimerPressed() {
    MLog.debug("MAFenetre", "onSupprimerPressed", "Suppression");
    return 0;
  }

  /**
   * @return 0
   */
  protected int onRechercherPressed() {
    if (mjbRechercher.isSelected()) {
      // Si on lance une recherche alors qu'on est en mode CREATION, demande de
      // confirmation
      if (getModeAffichage() == MEAffichage.CREATION
          && MADialogue.demanderConfirmation(this,
              MADialogue.MESSAGE.QUESTION_OUI_NON,
              "Etes-vous sur de vouloir annuler la creation en cours ?") != MEReponseUtil.OUI) {
        mjbRechercher.setSelected(false);
        return 0;
      } else if (getModeAffichage() == MEAffichage.MODIFICATION
          && MADialogue.demanderConfirmation(this,
              MADialogue.MESSAGE.QUESTION_OUI_NON,
              "Etes-vous sur de vouloir annuler les modifications en cours ?") != MEReponseUtil.OUI) {
        mjbRechercher.setSelected(false);
        return 0;

      }

      MLog.debug("MAFenetre", "onRechercherPressed", "Recherche activee");
      setModeAffichage(MEAffichage.RECHERCHE);
    } else {
      MLog.debug("MAFenetre", "onRechercherPressed", "Resultat");
      setModeAffichage(MEAffichage.LECTURE);
    }

    return 0;
  }

  /**
   * @return 0
   */
  protected int onPrecedentPressed() {
    MLog.debug("MAFenetre", "onPrecedentPressed", "Precedent");
    return 0;
  }

  /**
   * @return 0
   */
  protected int onSuivantPressed() {
    MLog.debug("MAFenetre", "onSuivantPressed", "Suivant");
    return 0;
  }

  /**
   * Definit le mode d'affichage des donnees: lecture, modification, recherche,
   * creation.
   * 
   * @param affichage
   */
  public void setModeAffichage(MEAffichage affichage) {
    // MLog.debug("MAFenetre", "setModeAffichage", "Changement en " +
    // affichage);

    this.modeAffichage = affichage;

    // Les champs sont presentes selon le mode d'affichage
    for (Component c : mpqPrinc.getComponents()) {
      if (c instanceof MIAField) {
        ((MIAField) c)
            .setMode(affichage == MEAffichage.RECHERCHE ? MIAField.AFFICHAGE.RECHERCHE
                : MIAField.AFFICHAGE.SAISIE);
      }
    }

    // Si passage en mode recherche par un autre moyen que le clic,
    // le bouton est considere comme selectionne
    if (modeAffichage == MEAffichage.RECHERCHE && !mjbRechercher.isSelected()) {
      mjbRechercher.setSelected(true);
    }

    refreshEnabledBoutons();

    if (affichage == MEAffichage.RECHERCHE) {
      mjbRechercher.setToolTipText("Lancer la recherche");
    } else {
      mjbRechercher.setToolTipText("Saisir les critères de recherche");
    }
  }

  /**
   * Rafraichir les boutons en les rendant insensibles/sensibles aux clics selon
   * l'affichage.
   */
  public void refreshEnabledBoutons() {
    final boolean bOk, bAjout, bSupp, bRech, bPrec, bSuiv;
    switch (this.modeAffichage) {
      case CREATION:
        bOk = true;
        bAjout = bSupp = false;
        bRech = true;
        bPrec = bSuiv = false;
        break;
      case MODIFICATION:
        bOk = bAjout = bSupp = false;
        bRech = true;
        bPrec = bSuiv = false;
        break;
      case LECTURE:
        bOk = false;
        bAjout = bSupp = bRech = bPrec = bSuiv = true;
        break;
      case RECHERCHE:
        bOk = bAjout = bSupp = false;
        bRech = bPrec = bSuiv = true;
        if (!mjbRechercher.isSelected()) {
          mjbRechercher.setSelected(true);
        }
        break;
      default:
        bOk = bAjout = bSupp = bRech = bPrec = bSuiv = false;
        MLog.error(this.getClass().toString(), "setMode", "Le mode "
            + modeAffichage + " est indefini.");
        break;
    }

    setPanelBoutonEnabled(BOUTON.VALIDER, bOk);
    setPanelBoutonEnabled(BOUTON.AJOUTER, bAjout);
    setPanelBoutonEnabled(BOUTON.SUPPRIMER, bSupp);
    setPanelBoutonEnabled(BOUTON.RECHERCHER, bRech);
    setPanelBoutonEnabled(BOUTON.PRECEDENT, bPrec);
    setPanelBoutonEnabled(BOUTON.SUIVANT, bSuiv);
  }

  public void onValueChange() {
    if (modeAffichage != MEAffichage.RECHERCHE) {
      setModeAffichage(MEAffichage.MODIFICATION);
      boolean bChampsObligatoiresOk = areMandatoryFieldsFilled();
      // Dans le cas d'une modif, on ne peut plus naviguer
      setPanelBoutonEnabled(BOUTON.PRECEDENT, false);
      setPanelBoutonEnabled(BOUTON.SUIVANT, false);
      setPanelBoutonEnabled(BOUTON.AJOUTER, false);

      // Si tous les champs obligatoires sont remplis, on peut valider.
      setPanelBoutonEnabled(BOUTON.VALIDER, bChampsObligatoiresOk);
    } else {
      setPanelBoutonEnabled(BOUTON.VALIDER, false);
    }
  }

  /**
   * Parcourt tous les elements de la fenetre pour verifier que tous les champs
   * obligatoires sont bien remplis.
   * 
   * @return <code>true</code> if the mandatory fields are filled;
   *         <code>false</code> otherwise.
   */
  public boolean areMandatoryFieldsFilled() {
    // MLog.infos("areMandatoryFieldsFilled");
    for (Component cFils : getComponents()) {
      if (cFils instanceof Container) {
        if (!areMandatoryFieldsFilledInContainer((Container) cFils)) {
          return false;
        }
      }
    }

    return true;
  }

  private boolean areMandatoryFieldsFilledInContainer(Container c) {
    for (Component cFils : c.getComponents()) {
      if (cFils instanceof MIAField) {
        MIAField mf = ((MIAField) cFils);
        if (mf.isMandatory()
            && (mf.getValue() == null || mf.getValue().toString().equals(""))) {
          return false;
        }
      } else if (cFils instanceof Container) {
        if (!areMandatoryFieldsFilledInContainer((Container) cFils)) {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Remplir le MADonnees par des donnees issues de la base.
   * 
   * @param mdTable donnnees
   * @param mDBTable table
   * @return <code>true</code> si la recherche a donne des resultats,
   *         <code>false</code> sinon
   */
  public boolean remplir(MADonnees mdTable, MALightTable mDBTable) {
    if (mdTable == null || mDBTable == null) {
      return false;
    }

    // Purge prealable des donnees
    mdTable.clearData();

    ResultSet rsDonnees = null;
    try {
      final StringBuffer sSelectTmp = new StringBuffer(getSelectFrom(mdTable));
      boolean bWhereEnCours = false;

      for (int iCol = 0; iCol < mdTable.getColumnCount(); iCol++) {
        final MAInfosColonne mInfo = (MAInfosColonne) mdTable.getInfoCol(iCol);
        if (mInfo.getColumnNature() == MENatureChampBdd.EXT) {
          continue;
        }

        final Object oValue = mdTable.getRechercheValueAt(iCol);
        // Si le champ a ete rempli, on le rajoute aux clauses
        if (oValue != null && !oValue.equals("")
            && mdTable.getColumnName(iCol).indexOf(mDBTable.getTrig()) == 0) {
          if (!bWhereEnCours) {
            sSelectTmp.append(" WHERE ");
            bWhereEnCours = true;
          } else {
            sSelectTmp.append(" AND ");
          }
          sSelectTmp.append(parseClause(mInfo, oValue.toString()));
        }
      }

      sSelectTmp.append(" ORDER BY ").append(
          mdTable.getInfoCol(0).getColumnName());

      // On recupere toutes les donnees de la table.
      MLog.debug("MAFenetre", "remplir", sSelectTmp.toString());
      rsDonnees = DBManager.select(sSelectTmp.toString());

      if (rsDonnees == null) {
        MLog.error(this.getClass().toString(), "remplir",
            "Pas de connexion active...");
        return false;
      }

      List<Object> vLigne = null;
      Object oDonnee = null;

      // Parcours des lignes de resultat et insertion dans le tableau
      while (rsDonnees.next()) {
        vLigne = new ArrayList<Object>();
        for (int i = 0; i < mdTable.getColumnCount(); i++) {
          try {
            oDonnee = rsDonnees.getObject(mdTable.getColumnName(i));
            vLigne.add(oDonnee);
          } catch (SQLException psqle) {
            vLigne.add("?");
            MLog.error(this.getClass().toString(), "remplir",
                "Erreur lecture des donnees de la base.", psqle);
          } catch (Exception e) {
            vLigne.add("?");
            MLog.error(this.getClass().toString(), "remplir",
                "Erreur lecture des donnees de la base.", e);
          }
        }

        mdTable.addRow(vLigne);
      } 

      // On rajoute toujours une ligne vierge en fin. (pour la creation)
      mdTable.addRow();

      if (vLigne == null) {
        // Aucune ligne creee...
        return false;
      }

    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "remplir",
          "Erreur de recuperation d'infos.", e);
    }
    return true;
  }

  /**
   * Construit la clause avec le mot LIKE si la valeur comporte un % ou un signe
   * '=' dans les autres cas. De plus, des caracteres d'echappement sont
   * utilises si besoin est.
   * 
   * @param sChamp nom du champ en base
   * @param sValue valeur a rechercher dans ce champ
   * @return la clause proprement ecrite
   */
  private String parseClause(MAInfosColonne mInfo, String sValue) {
    String sChamp = mInfo.getAliasTable() + "." + mInfo.getNomReelChamp();
    // FIXME : format de la clause
    return new StringBuffer(sChamp).append(
        (sValue.lastIndexOf('%') != -1 ? " LIKE '" : " = '")).append(
        protegerChaine(sValue)).append('\'').toString();
  }

  /**
   * Positionner les caracteres d'echappement eventuels.
   * 
   * @param sChaine
   * @return la chaine protegee par des caracteres d'echappement
   */
  private String protegerChaine(String sChaine) {
    StringBuffer sbChaine = new StringBuffer();

    // On parcourt la chaine de caracteres a la recherche de caracteres speciaux
    for (char cChar : sChaine.toCharArray()) {
      switch (cChar) {
        case '\'':
          sbChaine.append('\'');
          break;

        default:
          break;
      }
      sbChaine.append(cChar);
    }

    return sbChaine.toString();
  }

  /**
   * Updates the data base with the new data.
   * 
   * @param mdTable data model
   * @param lightTable ligth table description
   * @return the system return code
   */
  public MERetourSys mettreAJour(MADonnees mdTable, MALightTable lightTable) {
    if (mdTable == null || lightTable == null) {
      return MERetourSys.ERREUR_RIEN_A_FAIRE;
    }

    try {
      // Construction des colonnes pour le select
      StringBuffer sUpdate = new StringBuffer("UPDATE ").append(
          lightTable.getNom()).append(" SET ");

      final int iLigne = mdTable.getLigneActive();
      final int iNbCol = mdTable.getColumnCount();
      for (int iCol = 0; iCol < iNbCol; iCol++) {
        final MAInfosColonne mInfo = (MAInfosColonne) mdTable.getInfoCol(iCol);
        // if ( !sColName.substring(0, 3).equals(meTable.getTrig()))
        final MENatureChampBdd mNat = mInfo.getColumnNature();
        if (mNat != MENatureChampBdd.TABLE
            && mNat != MENatureChampBdd.IDVTABLEEXT) {
          // ID de la table courante jamais a mettre a jour
          continue;
        }
        sUpdate.append(mInfo.getNomReelChamp());
        sUpdate.append(" = ").append(preparerValeurChamp(mdTable, mInfo));
        sUpdate.append(", ");
      }

      // On enleve la derniere ","
      if (sUpdate.lastIndexOf(",") == sUpdate.length() - 2) {
        sUpdate.setLength(sUpdate.length() - 2);
      }

      sUpdate.append(" WHERE " + lightTable.getTrig() + "_id = ").append(
          mdTable.getValueAt(iLigne, mdTable.getInfoCol(0).getColumnName()));

      MLog.debug("MAFenetre", "mettreAJour", sUpdate.toString());

      // On recupere toutes les donnees de la table.
      final int iResultat = DBManager.update(sUpdate.toString());

      if (iResultat == -1) {
        MLog.error(this.getClass().toString(), "mettreAJour",
            "Pas de connexion active...");
        return MERetourSys.ERREUR_CONNEXION_NON_ACTIVE;
      } else if (iResultat == 0) {
        MLog.error("MAFenetre", "mettreAJour",
            "Pas de ligne modifiee. Elle a probablement ete supprimee...");
        return MERetourSys.ERREUR_RIEN_A_FAIRE;
      }

      return MERetourSys.OK_ENREG_MAJ;

    } catch (Exception e) {
      MLog.error("MAFenetre", "mettreAJour", "Erreur de recuperation d'infos.",
          e);
    }
    return MERetourSys.ERREUR_MAJ;
  }

  /**
   * Removes data in the database from the specificied table.
   * 
   * @param mdTable the table model
   * @param mDBTable the light table description
   * @return the number of removed lines.
   */
  public int supprimer(MDonnees mdTable, MALightTable mDBTable) {
    if (mdTable == null || mDBTable == null) {
      return -1;
    }

    try {
      // Construction des colonnes pour le select
      final StringBuffer sDelete = new StringBuffer("DELETE FROM ").append(
          mDBTable.getNom()).append(" WHERE ").append(
          mDBTable.getTrig() + "_id = ").append(
          mdTable.getValueAt(mdTable.getLigneActive(), mdTable.getInfoCol(0)
              .getColumnName()));

      MLog.debug(this.getClass().toString(), "supprimer", sDelete.toString());

      // On recupere toutes les donnees de la table.
      final int iResultat = DBManager.update(sDelete.toString());

      if (iResultat == -1) {
        MLog.error(this.getClass().toString(), "supprimer",
            "Pas de connexion active...");
        return -1;
      } else if (iResultat == 0) {
        MLog.error(this.getClass().toString(), "supprimer",
            "Pas de ligne supprimee. "
                + "Elle a probablement deja ete supprimee...");
        return 0;
      }

      return iResultat;

    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "supprimer",
          "Erreur lors de la suppression de ligne.", e);
    }
    return -1;
  }

  /**
   * Adds new data in the database.
   * 
   * @param mdTable the table model
   * @param mDBTable the light table description
   * @return <code>true</code> if everything was ok
   */
  public boolean ajouter(MDonnees mdTable, MALightTable mDBTable) {
    try {
      // Construction des colonnes pour le select
      final StringBuffer sInsert = new StringBuffer("INSERT INTO ").append(
          mDBTable.getNom()).append(" (");
      final StringBuffer sDonnees = new StringBuffer("VALUES (");

      final int iNbCol = mdTable.getColumnCount();
      for (int iCol = 0; iCol < iNbCol; iCol++) {
        final MAInfosColonne mInfo = (MAInfosColonne) mdTable.getInfoCol(iCol);
        final String sColName = mInfo.getNomReelChamp();
        // Si la colonne n'appartient pas a la table, on la passe
        if (!mInfo.getAliasTable().equals(mDBTable.getTrig())) {
          continue;
        }

        sInsert.append(sColName).append(", ");
        sDonnees.append(preparerValeurChamp(mdTable, mInfo)).append(", ");
      }

      // On remplace la derniere "," par ")"
      if (sInsert.lastIndexOf(",") == sInsert.length() - 2) {
        sInsert.setLength(sInsert.length() - 2);
        sInsert.append(") ");
        sDonnees.setLength(sDonnees.length() - 2);
        sDonnees.append(") ");
      }
      // TODO: controler les caracteres d'echappement
      sInsert.append(sDonnees);

      MLog.debug(this.getClass().toString(), "ajouter", sInsert.toString());

      // On recupere toutes les donnees de la table.
      final int iResultat = DBManager.insert(sInsert.toString());

      if (iResultat == -1) {
        MLog.error(this.getClass().toString(), "ajouter",
            "Pas de connexion active...");
        return false;
      } else if (iResultat == 0) {
        MLog.error(this.getClass().toString(), "ajouter",
            "Pas de ligne inseree...");
        return false;
      }

    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "ajouter",
          "Erreur de recuperation d'infos.", e);
      return false;
    }
    return true;
  }

  private String preparerValeurChamp(MDonnees md, MAInfosColonne mInfo) {
    Object oValue = md.getValueAt(md.getLigneActive(), mInfo.getColumnName());
    if (oValue == null || oValue.equals("")) {
      return "null";
    }

    String sChpAjoute = null;
    if (mInfo.getFormat() == null) {
      sChpAjoute = "'" + protegerChaine(oValue.toString()) + "'";
    }

    // TODO: application du format (to_timestamp, etc.)
    final List<MIField> mf = mInfo.getFields();
    if (mf != null && mf.size() > 0) {
      if (mf.get(0) instanceof MADateField) {
        // to_date ne prend pas les heures, minutes.
        sChpAjoute = "to_timestamp('" + oValue.toString() + "', '"
            + ((MADateField) mf.get(0)).getFormatSys() + "') ";
      }
    }
    return sChpAjoute;
  }

  /**
   * @return the modeAffichage
   */
  public MEAffichage getModeAffichage() {
    return this.modeAffichage;
  }

  /**
   * Creer la partie SELECT ... FROM ... de la requete de selection.
   * 
   * @param mdTable
   * @return la premiere partie de la requete
   */
  private String getSelectFrom(MADonnees mdTable) {
    StringBuffer sbFrom = new StringBuffer();
    final MAInfosColonne mInfo0 = ((MAInfosColonne) mdTable.getInfoCol(0));
    sbFrom.append(" FROM ").append(mInfo0.getNomTable()).append(" AS ").append(
        mInfo0.getAliasTable());

    // Si le SELECT n'existe pas encore, on le cree.
    if (sbSelectFrom == null) {
      sbSelectFrom = new StringBuffer("SELECT ");

      final HashSet<String> hsAliasUtilises = new HashSet<String>();
      hsAliasUtilises.add(mInfo0.getAliasTable()); // Table principale
      // consideree comme
      // traitee
      MAInfosColonne mInfoIdVersExt = null;
      for (int iCol = 0; iCol < mdTable.getColumnCount(); iCol++) {
        // Colonne
        final MAInfosColonne mInfo = ((MAInfosColonne) mdTable.getInfoCol(iCol));
        sbSelectFrom.append(buildSelectPart(mInfo));

        if (iCol < mdTable.getColumnCount() - 1) {
          sbSelectFrom.append(", ");
        }

        if (mInfo.getColumnNature() == MENatureChampBdd.IDVTABLEEXT) {
          mInfoIdVersExt = mInfo;
        }

        // Tables
        // Si l'alias n'a pas deja ete utilise, jointure
        if (!hsAliasUtilises.contains(mInfo.getAliasTable())) {
          sbFrom.append(" LEFT JOIN ");
          sbFrom.append(mInfo.getNomTable()).append(" AS ").append(
              mInfo.getAliasTable());

          if (mInfoIdVersExt == null) {
            MLog.debug("MAFenetre", "getSelectFrom",
                "Pas d'identifiant vers une table externe !");
            return null;
          }

          sbFrom.append(" ON (").append(mInfoIdVersExt.getAliasTable()).append(
              ".").append(mInfoIdVersExt.getNomReelChamp());
          sbFrom.append(" = ").append(mInfo.getAliasTable()).append(".")
              .append(
                  MAContextManager.getInstance().getDbContext()
                      .getLightTableParNom(mInfo.getNomTable()).getTrig())
              .append("_id");
          sbFrom.append(") ");

          // L'alias a ete pris en compte.
          hsAliasUtilises.add(mInfo.getAliasTable());
        }

      }

      sbSelectFrom.append(sbFrom);
    }

    return sbSelectFrom.toString();
  }

  private String buildSelectPart(MAInfosColonne mInfo) {
    final String sFormat = mInfo.getFormat();
    final StringBuffer sbChamp = new StringBuffer();
    final String sNomChamp = mInfo.getAliasTable() + "."
        + mInfo.getNomReelChamp();
    if (sFormat != null) {
      sbChamp.append(" to_char(").append(sNomChamp).append(", '").append(
          sFormat).append("')");
    } else {
      sbChamp.append(sNomChamp);
    }

    sbChamp.append(" AS ").append(mInfo.getColumnName());

    return sbChamp.toString();
  }

  /**
   * Set enabled or not the panel buttons.
   * 
   * @param bPanelEnabled <code>true</code> if the buttons will be enabled;
   *          <code>false</code> otherwise
   */
  public void setPanelBoutonsEnabled(boolean bPanelEnabled) {
    // for (BOUTON b : BOUTON.values())
    // setPanelBoutonEnabled(b, bPanelEnabled);

    MLog.debug(this.getName(), "setPanelBoutonsEnabled", (BOUTON.AJOUTER)
        + " | " + mjbAjouter);

    mjbValider.setEnabled(true);
    mjbAjouter.setEnabled(true);
    mjbPrecedent.setEnabled(true);
    mjbRechercher.setEnabled(true);
    mjbSuivant.setEnabled(true);
    mjbSupprimer.setEnabled(true);
  }

  /**
   * Set enabled or not the panel button.
   * 
   * @param bBouton the button to enable/disable
   * @param bEnabled <code>true</code> if the button will be enabled;
   *          <code>false</code> otherwise
   */
  public void setPanelBoutonEnabled(BOUTON bBouton, boolean bEnabled) {
    // Un bouton est accessible s'il est :
    // - autorise par la logique de l'ecran
    // - autorise par le contexte immediat

    getButton(bBouton).setEnabled(bEnabled);
  }

  /**
   * Set authorised or not the panel buttons.
   * 
   * @param bAutorise <code>true</code> if the buttons are authorised;
   *          <code>false</code> otherwise
   */
  public void setPanelBoutonsAutorises(boolean bAutorise) {
    for (BOUTON b : BOUTON.values()) {
      setPanelBoutonAutorise(b, bAutorise);
    }
  }

  /**
   * Set authorised or not the panel button.
   * 
   * @param bBouton the button to set authorised or not
   * @param bAutorise <code>true</code> if the buttons are authorised;
   *          <code>false</code> otherwise
   */
  public void setPanelBoutonAutorise(BOUTON bBouton, boolean bAutorise) {
    getButton(bBouton).setAutorise(bAutorise);
  }

  /**
   * Get the technical button according to the fonctionnale one.
   * 
   * @param bBouton
   * @return
   */
  private MBouton getButton(BOUTON bBouton) {
    MBouton mjb = null;

    switch (bBouton) {
      case VALIDER:
        mjb = mjbValider;
        break;
      case AJOUTER:
        mjb = mjbAjouter;
        break;
      case PRECEDENT:
        mjb = mjbPrecedent;
        break;
      case SUIVANT:
        mjb = mjbSuivant;
        break;
      case RECHERCHER:
        mjb = mjbRechercher;
        break;
      case SUPPRIMER:
        mjb = mjbSupprimer;
        break;
      default:
        throw new RuntimeException("Unknown button '" + bBouton + "'!!!");
    }

    return mjb;
  }
}
