/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MASysWindow.java - CREATION : 2005/11/28
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.fenetre;

import java.awt.Point;
import java.awt.Window;

/**
 * @author armel
 */
public class MASysWindow {

  /** The window */
  private Window window = null;

  /** The system name */
  private String sysName = null;

  /** The class of the window */
  private Class<?> clazz = null;

  /** The position in the screen */
  private Point position = null;

  /** The visibility state */
  private boolean visible = false;

  /**
   * Constructeur d'une fenetre systeme sans classe. Soit donc une fenetre
   * generique liee a une table.
   * 
   * @param sysName the system name of the window
   */
  public MASysWindow(String sysName) {
    this.sysName = sysName;
  }

  /**
   * Constructeur d'une fenetre systeme specifique.
   * 
   * @param sysName the system name of the window
   * @param clazz the class of the window
   */
  public MASysWindow(String sysName, Class<?> clazz) {
    this.sysName = sysName;
    this.clazz = clazz;
  }

  /**
   * @return the class
   */
  public Class<?> getClazz() {
    return this.clazz;
  }

  /**
   * @param clazz the class to set
   */
  public void setClazz(Class<?> clazz) {
    this.clazz = clazz;
  }

  /**
   * @return the visible
   */
  public boolean isVisible() {
    return this.visible;
  }

  /**
   * @param visible the visible to set
   */
  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  /**
   * @return the window
   */
  public Window getWindow() {
    return this.window;
  }

  /**
   * @param window the window to set
   */
  public void setWindow(Window window) {
    this.window = window;
  }

  /**
   * @return the position
   */
  public Point getSavedPosition() {
    return this.position;
  }

  /**
   * @param newPosition the position to save
   */
  public void savePosition(Point newPosition) {
    this.position = newPosition;
  }

  /**
   * Save the location in the screen into the current position.
   */
  public void saveLocationOnScreen() {
    this.position = window.getLocationOnScreen();
  }

  /**
   * @return the sysName
   */
  public String getSysName() {
    return this.sysName;
  }

  /**
   * @param sysName the sysName to set
   */
  public void setSysName(String sysName) {
    this.sysName = sysName;
  }

  /**
   * @return <code>true</code> if specific, <code>false</code> otherwise
   */
  public boolean isSpecific() {
    return clazz != null;
  }
}