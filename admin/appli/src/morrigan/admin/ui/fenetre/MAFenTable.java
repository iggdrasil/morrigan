/*
 * ####################################################################### 
 * # PROJECT : Morrigan - FILE : MAFenTable.java - CREATION : 2005/01/09 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com> 
 * # 
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING. 
 * #######################################################################
 */
package morrigan.admin.ui.fenetre;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.util.List;

import morrigan.admin.database.DBManager;
import morrigan.admin.database.MALightTable;
import morrigan.admin.ui.MAList;
import morrigan.admin.ui.MAListResult;
import morrigan.admin.ui.MJPopupMenuID;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.admin.ui.enumeration.MEReponseUtil;
import morrigan.admin.ui.enumeration.MERetourSys;
import morrigan.admin.ui.field.MAIntegerField;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MADialogue;
import morrigan.admin.util.MADonnees;
import morrigan.admin.util.MAInfosColonne;
import morrigan.admin.util.MEModeUtilDonnees;
import morrigan.graphique.field.util.MIField;
import morrigan.log.MLog;

/**
 * Les fenetres qui heritent de cette classe disposent des moyens de navigation
 * dans les requetes et resultats SUR UNE TABLE DONNEE.
 * 
 * @author armel
 */
public abstract class MAFenTable extends MAFenetre implements MouseListener {
  private MADonnees mdTable = null; // Detail des champs

  private MALightTable mDBInfoTable = null; // Informations sur la table en base

  public MAFenTable(String sTitre, int iHauteurCell, int iLargeurCell) {
    super(sTitre, iHauteurCell, iLargeurCell);
    build();
    applyListenerOnExtFields();
    setModeAffichage(MEAffichage.RECHERCHE);

    MAListResult mlr = new MAListResult(this, getDonnees());
    mlr.linkListWithFields(mjbRechercher);
  }

  protected abstract void build();

  protected void linkToTables(MADonnees mdTableLiee,
      MALightTable mDBInfoTableLiee) {
    this.mdTable = mdTableLiee;
    this.mDBInfoTable = mDBInfoTableLiee;
  }

  @Override
  protected int onValiderPressed() {
    // Si un *_id est present, on met a jour
    // sinon on insere
    String id = mDBInfoTable.getTrig() + "_" + mDBInfoTable.getTrig() + "_id";
    if (mdTable.getValueAt(mdTable.getLigneActive(), id) != null) {
      mettreAJourLigneActive();
    } else {
      MERetourSys mRetour = ajouterLigneActive();
      MADialogue.ouvrirDialogue(this, mRetour.getMessage(), mRetour
          .getTexteRetour());
    }

    return super.onValiderPressed();
  }

  @Override
  protected int onPrecedentPressed() {
    if (mdTable.getRowCount() > 0 && mdTable.getLigneActive() > 0)
      mdTable.setLigneActive(mdTable.getLigneActive() - 1);

    refreshEnabledBoutons();

    return super.onPrecedentPressed();
  }

  @Override
  protected int onSuivantPressed() {
    if ((modeAffichage != MEAffichage.RECHERCHE && mdTable.getLigneActive() < mdTable
        .getRowCount() - 2)
        || (modeAffichage == MEAffichage.RECHERCHE && mdTable.getLigneActive() < mdTable
            .getRowCount() - 1))
      mdTable.setLigneActive(mdTable.getLigneActive() + 1);

    refreshEnabledBoutons();

    return super.onSuivantPressed();
  }

  @Override
  protected int onSupprimerPressed() {
    if (this.modeAffichage == MEAffichage.RECHERCHE) {
      int iLgSupp = mdTable.getLigneActive();

      if (mdTable.getRowCount() == 1 || iLgSupp == mdTable.getRowCount() - 1)
        mdTable.addRow();

      // Supprimer place la ligne active a la ligne suivante,
      // attention donc a l'ordre des actions.
      mdTable.removeRow(iLgSupp);

      refreshEnabledBoutons();
    } else if (this.modeAffichage != MEAffichage.CREATION) {
      MLog.debug("MAFenTable", "onSupprimerPressed", "Suppression");

      if (MADialogue.demanderConfirmation(this,
          MADialogue.MESSAGE.QUESTION_OUI_NON,
          "Etes-vous sur de vouloir supprimer cet enregistrement ?") != MEReponseUtil.OUI)
        return 0;

      final int iNbLgSupp = supprimer(mdTable, mDBInfoTable);
      if (iNbLgSupp > 0) {
        mdTable.removeRow(mdTable.getLigneActive());

        // S'il ne reste plus de ligne de lecture (mais uniquement la derniere
        // ligne vierge), on passe en mode creation
        if (mdTable.getRowCount() == 1)
          setModeAffichage(MEAffichage.CREATION);

        MADialogue.ouvrirDialogue(this, MADialogue.MESSAGE.INFORMATION,
            "Enregistrement supprime");
      } else
        MADialogue.ouvrirDialogue(this, MADialogue.MESSAGE.ERREUR,
            "Erreur lors de la suppression");
    }

    return 0;
  }

  @Override
  public void setModeAffichage(final MEAffichage affichage) {
    final MEAffichage iAncienMode = this.modeAffichage;
    super.setModeAffichage(affichage);

    if (mdTable == null) {
      MLog.error("MAFenTable", "setModeAffichage",
          "Attention ! Pas de MADonnees pour cette fenetre !");
      return;
    }

    // Si on passe en mode recherche, on vide les champs
    if (affichage == MEAffichage.RECHERCHE) {
      if (!mjbRechercher.isSelected())
        mjbRechercher.setSelected(true);

      mdTable.setMode(MEModeUtilDonnees.RECHERCHE);

      // On se place sur la derniere recherche lancee
      mdTable.goToLigneActiveRecherche();
    }
    // Si on quitte le mode recherche, le SELECT est lance
    else if (affichage == MEAffichage.LECTURE
        && iAncienMode == MEAffichage.RECHERCHE) {
      if (mjbRechercher.isSelected())
        mjbRechercher.setSelected(false);

      // Si la recherche s'est faite sur la derniere ligne du Vecteur (et
      // qu'elle n'est pas
      // vierge), on en rajoute une nouvelle vierge.
      if (mdTable.getLigneActive() == mdTable.getRowCount() - 1
          && !mdTable.isLigneVide(mdTable.getLigneActive()))
        mdTable.addRow();

      // Passage en mode lecture
      mdTable.setMode(MEModeUtilDonnees.LECTURE);
      if (!remplir(mdTable, mDBInfoTable)) {
        MADialogue.ouvrirDialogue(this, MADialogue.MESSAGE.INFORMATION,
            "Aucun enregistrement trouve.\nCreation proposee.");
        // Passage en mode CREATION si rien trouve
        setModeAffichage(MEAffichage.CREATION);
      } else if (mdTable.getRowCount() > 0)
        mdTable.setLigneActive(0);
    } else if (affichage == MEAffichage.CREATION) {
      // En mode creation, on se place sur la derniere ligne
      // if (mdTable.getLigneActive() != mdTable.getRowCount() - 1)
      mdTable.setLigneActive(mdTable.getRowCount() - 1);
    }

    refreshEnabledFields();
    refreshEnabledBoutons();
  }

  private void refreshEnabledFields() {
    // Certains champs ne sont pas modifiables en base ou à la saisie
    // (combobox).

    // Par exemple: Les champs XXX_id sont les cles primaires. Ils sont donc
    // saisissables pour
    // une requete mais non modifiables en base

    // TODO: possibilite de "non saisissabilite" de champ (MAListe par ex)
    for (String sColonne : mdTable.getColumnNames()) {
      // Recuperation de tous les champs
      List<MIField> vFields = mdTable.getFields(sColonne);
      if (vFields != null)
        for (MIField field : vFields) {
          // Puis test si on peut le modifier ou non
          field.setEnabled(isFieldEnabled(field));
        }
    }
  }

  /**
   * Retourner si oui ou non le champ peut etre modifiable en fonction du mode
   * courant, de la possibilite de modifier en base, etc.
   * 
   * @return <code>true</code> si le champ peut etre modifie;
   *         <code>false</code> sinon
   */
  private boolean isFieldEnabled(MIField field) {
    if (field instanceof MIAField) {
      final MENatureChampBdd mNatChp = ((MIAField) field).getNatureChpBdd();
      if (mNatChp == MENatureChampBdd.IDVTABLEEXT
          || mNatChp == MENatureChampBdd.EXT)
        return false;
    }

    if (modeAffichage == MEAffichage.RECHERCHE)
      return true;

    if (field instanceof MAIntegerField && ((MAIntegerField) field).isID())
      return false;

    if (field instanceof MIAField && !((MIAField) field).isBddEnabled())
      return false;

    return true;
  }

  @Override
  public void refreshEnabledBoutons() {
    if (mdTable == null) {
      // MLog.error("MAFenTable", "refreshEnabledBoutons", "Attention ! Pas de
      // MADonnees pour cette fenetre !");
      return;
    }

    final int iLg = mdTable.getLigneActive();
    final int iDerLg = mdTable.getRowCount() - 1;
    final boolean bPremLg = (iLg == 0); /* Premiere ligne */
    /**
     * Av dern ligne: dern ligne de resultat.<br>
     * Inutile pour mode RECHERCHE
     */
    final boolean bAvDerLg = (iDerLg >= 1 && iLg == iDerLg - 1);
    final boolean bDerLg = (iLg == iDerLg); /* Derniere ligne : creation */

    if (modeAffichage == MEAffichage.RECHERCHE) {
      // Bouton Precedent : activation si pas premiere ligne
      mjbPrecedent.setEnabled(!bPremLg);
      // Bouton Suivant : activation si pas derniere ligne
      mjbSuivant.setEnabled(!bDerLg);

      mjbAjouter.setEnabled(false);
      mjbSupprimer.setEnabled(true);
    } else {
      // Bouton Precedent : activation si pas premiere ligne ni derniere
      mjbPrecedent.setEnabled(!bPremLg && !bDerLg);
      // Bouton Suivant : activation si pas derniere ligne
      mjbSuivant.setEnabled(!bDerLg && !bAvDerLg);

      mjbAjouter.setEnabled(!bDerLg);
      mjbSupprimer.setEnabled(!bDerLg);
    }

    // AU changement de mode, pas de validation possible parce que pas de
    // modification en cours
    mjbValider.setEnabled(false);
  }

  public MERetourSys mettreAJourLigneActive() {
    MERetourSys mRetour = super.mettreAJour(mdTable, mDBInfoTable);
    MADialogue.ouvrirDialogue(this, mRetour.getMessage(), mRetour
        .getTexteRetour());

    return mRetour;
  }

  public MERetourSys ajouterLigneActive() {
    if (mdTable == null || mDBInfoTable == null)
      return MERetourSys.ERREUR_RIEN_A_FAIRE;

    final String sSeq = mDBInfoTable.getNom() + '_' + mDBInfoTable.getTrig()
        + "_id_seq";

    // Tentative de recherche d'un identifiant
    try {
      StringBuffer sSelect = new StringBuffer("SELECT ").append("nextval('")
          .append(sSeq).append("'::text)");

      // On recupere toutes les donnees de la table.
      MLog.debug("MAFenetre", "ajouter",
          "Recherche d'un ID à partir d'un sequenceur : " + sSelect.toString());
      ResultSet rsDonnees = DBManager.select(sSelect.toString());

      if (rsDonnees == null) {
        MLog.error(this.getClass().toString(), "remplir",
            "Pas de connexion active...");
        return MERetourSys.ERREUR_CONNEXION_NON_ACTIVE;
      }

      // Si on ne peut pas pointer sur la premiere ligne, c'est qu'il n'y en a
      // pas :)
      if (!rsDonnees.next()) {
        MLog.debug(this.getClass().toString(), "remplir", "Table "
            + mDBInfoTable.getNom() + " : aucune donnee trouvee...");
        return MERetourSys.OK_AUCUN_RESULTAT;
      }

      // MLog.debug(this.getClass().toString(), "remplir", "Warnings eventuels
      // de la requete:
      // "
      // + rsDonnees.getWarnings());

      Integer iSeq = Integer.valueOf(rsDonnees.getString(1));
      MLog.debug("MAFenetre", "ajouter", "Sequenceur trouve !" + iSeq);
      mdTable.setValueAt(iSeq, mDBInfoTable.getTrig() + "_"
          + mDBInfoTable.getTrig() + "_id");

    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "ajouter", "Pas de sequenceur "
          + sSeq + " pour la table " + mDBInfoTable.getNom(), e);
      return MERetourSys.ERREUR_SEQUENCEUR;
    }

    // Puis on insère la ligne complète
    final boolean bAjoutOk = super.ajouter(mdTable, mDBInfoTable);

    // Ajout d'une ligne vierge
    if (bAjoutOk) {
      mdTable.addRow();
      refreshEnabledBoutons();
    } else {
      return MERetourSys.ERREUR_INSERTION;
    }

    return MERetourSys.OK_ENREG_INSERE;
  }

  protected MADonnees getDonnees() {
    return mdTable;
  }

  protected MALightTable getTable() {
    return mDBInfoTable;
  }

  /**
   * Lancer une recherche en specifiant l'ID de la table.
   * 
   * @param iID identifiant
   */
  public void lancerRechercheAvecID(Integer iID) {
    // Passage en mode recherche
    setModeAffichage(MEAffichage.RECHERCHE);
    mdTable.setLigneActive(mdTable.getRowCount() - 1);
    mdTable.setValueAt(iID, mDBInfoTable.getTrig() + "_"
        + mDBInfoTable.getTrig() + "_id");
    setModeAffichage(MEAffichage.LECTURE);
  }

  private boolean applyListenerOnExtFields() {
    for (Component cFils : getComponents()) {
      if (cFils instanceof Container)
        applyListenerOnContainer((Container) cFils);
    }

    return true;
  }

  private void applyListenerOnContainer(Container c) {
    for (Component cFils : c.getComponents()) {
      if (cFils instanceof MIAField) {
        MIAField mf = ((MIAField) cFils);
        MENatureChampBdd mNat = mf.getNatureChpBdd();
        if (mNat == MENatureChampBdd.IDVTABLEEXT
            || mNat == MENatureChampBdd.EXT)
          mf.addMouseListener(this);
      } else if (cFils instanceof Container) {
        applyListenerOnContainer((Container) cFils);
      }
    }
  }

  @SuppressWarnings("unused")
  public void mouseClicked(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseEntered(MouseEvent e) {}

  @SuppressWarnings("unused")
  public void mouseExited(MouseEvent e) {}

  public void mousePressed(MouseEvent e) {
    onClick(e);
  }

  public void mouseReleased(MouseEvent e) {
    onClick(e);
  }

  private void onClick(MouseEvent e) {
    if (e.isPopupTrigger() || e.getButton() >= 2) // Pas tres propre mais bon...
    {
      final MIAField mf = (MIAField) e.getSource();
      final MENatureChampBdd mNat = mf.getNatureChpBdd();

      // Extraction du trigramme de la table
      Integer iID = null;
      if (mNat == MENatureChampBdd.IDVTABLEEXT) // Ex: prs_tpr_id
      {
        Object oID = mdTable.getValueAt(mdTable.getLigneActive(), mf
            .getColonne());
        iID = (oID == null || oID.toString().equals("") ? null : new Integer(
            oID.toString()));
      } else if (mNat == MENatureChampBdd.EXT) // Ex: tpr_nom
      {
        // On cherche le champ ID associe en passant par le MAList
        MouseListener[] ml = ((Component) mf).getMouseListeners();
        for (MouseListener m : ml) {
          if (m instanceof MAList) {
            iID = ((MAList) m).getID();
            break;
          }
        }
      }
      
      MIAField source = (MIAField) e.getSource();
      MAInfosColonne infoCol = (MAInfosColonne) this.mdTable.getInfoCol(source.getColonne());

      // Clic droit => Menu
      MJPopupMenuID popupMenuID = new MJPopupMenuID(source, iID, infoCol);
      final Component c = (Component) source;
      popupMenuID.show(c, 0, c.getHeight());

    }
  }

}
