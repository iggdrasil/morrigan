/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenetreSys.java - CREATION : 2005/11/28
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui.fenetre;

import java.awt.Point;
import java.awt.Window;

/**
 * @author armel
 */
public class MAFenetreSys {
  private Window jfFenetre = null;

  private String sNomSys = null;

  private Class cClasse = null;

  private Point pPosition = null;

  private boolean bVisible = false;

  public MAFenetreSys(String sNomSys) {
    this.sNomSys = sNomSys;
  }

  public MAFenetreSys(String sNomSys, Class cClasse) {
    this.sNomSys = sNomSys;
    this.cClasse = cClasse;
  }

  public String getNomSys() {
    return sNomSys;
  }

  public Class getClasse() {
    return cClasse;
  }

  public void setWindow(Window jf) {
    this.jfFenetre = jf;
  }

  public Window getWindow() {
    return jfFenetre;
  }

  public boolean isSpecif() {
    return cClasse != null;
  }

  public void savePosition(Point pPos) {
    this.pPosition = pPos;
  }

  public void saveLocationOnScreen() {
    this.pPosition = jfFenetre.getLocationOnScreen();
  }

  public Point getSavedPosition() {
    return pPosition;
  }

  public void setVisible(boolean bVisible) {
    this.bVisible = bVisible;
  }

  public boolean isVisible() {
    return bVisible;
  }

}