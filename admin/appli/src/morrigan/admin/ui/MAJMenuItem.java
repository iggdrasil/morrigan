/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAJMenuItem.java - CREATION : 2005/11/13
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import org.apache.commons.lang.Validate;

import morrigan.admin.ui.enumeration.MEActionMenu;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.graphique.menu.MJMenuItem;

public class MAJMenuItem extends MJMenuItem implements MIMenuElement {

  private MASysWindow fenWindow = null;

  private MEActionMenu mam = null;

  public MAJMenuItem(String nom, MASysWindow fenWindow) {
    super(nom);
    Validate.notNull(nom, "nom cannot be null");

    this.mam = MEActionMenu.OUVRIR_FENETRE;
    this.fenWindow = fenWindow;
  }

  public MAJMenuItem(String sNom, MEActionMenu mam) {
    super(sNom);
    this.mam = mam;
  }

  public MAJMenuItem(String sNom, MEActionMenu mam, MASysWindow fenWindow) {
    this(sNom, fenWindow);
    this.mam = mam;
  }
  
  public MEActionMenu getActionMenu() {
    return mam;
  }

  public MASysWindow getFenetre() {
    return fenWindow;
  }
  
  public void setFenetre(MASysWindow fenWindow) {
    this.fenWindow = fenWindow;
  }
}
