/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MJPopupMenuField.java - CREATION : 2005/12/11
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */

package morrigan.admin.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.exception.OpenWindowException;
import morrigan.admin.ui.enumeration.MENatureChampBdd;
import morrigan.admin.ui.fenetre.MAFenTable;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.admin.ui.field.MIAField;
import morrigan.admin.util.MAInfosColonne;
import morrigan.graphique.MJPopupMenu;
import morrigan.graphique.menu.MJMenuItem;
import morrigan.log.MLog;

/**
 * Ce menu permet de proposer des actions sur l'identifiant (ID ou champ
 * Externe) qui a reçu le clic.
 * 
 * @author armel
 */
public class MJPopupMenuID extends MJPopupMenu implements ActionListener {

  private MIAField miaf = null;

  private MJMenuItem mjmi = null;

  private MJMenuItem mjmiID = null;

  private Integer iID = null;

  private final MAInfosColonne infoCol;

  public MJPopupMenuID(MIAField field, Integer id, MAInfosColonne infoCol) {
    this.miaf = field;
    this.iID = id;
    this.infoCol = infoCol;
    
    final Object oValueID = field.getValue();
    if (oValueID != null && !oValueID.toString().equals("")) {
      mjmiID = new MJMenuItem("Voir le detail");
      mjmiID.addActionListener(this);
      add(mjmiID);
    }

    mjmi = new MJMenuItem("Ouvrir la fenetre '" + infoCol.getNomTable() + "'");
    mjmi.addActionListener(this);
    add(mjmi);
  }

  public void actionPerformed(ActionEvent ae) {
    final MENatureChampBdd fieldNature = miaf.getNatureChpBdd();
    if (fieldNature == MENatureChampBdd.IDVTABLEEXT || fieldNature == MENatureChampBdd.EXT) {

      String extTableName;
      if (fieldNature == MENatureChampBdd.IDVTABLEEXT) {
        // Ex: prs_tpr_id => Extract 'tpr' => Find 'type_personnage'
        String extColName = infoCol.getNomReelChamp();
        String extTrig = extColName.substring(extColName.length() - 6, extColName.length() - 3);
        Table extTable = MAContextManager.getInstance().getDbContext().getTableParTrigramme(extTrig);
        extTableName = extTable.getName();
      } else {
        // Ex: tpr_nom => Find 'type_personnage'
        extTableName = infoCol.getNomTable();
      }
      final MASysWindow mFenWindow = MAContextManager.getInstance()
          .getWindowContext().getSysWindowByWindowName(extTableName);

      final MAFenTable mFenTable;
      try {
        mFenTable = (MAFenTable) MAWindowManager.createWindow(mFenWindow);
        if (ae.getSource() == mjmiID) {
          mFenTable.lancerRechercheAvecID(iID);
        }
      } catch (CreateWindowException cwe) {
        cwe.printStackTrace();
      }

      try {
        MAWindowManager.openWindow(mFenWindow);
      } catch (CreateWindowException cwe) {
        cwe.printStackTrace();
      } catch (OpenWindowException owe) {
        owe.printStackTrace();
      }

    } else {
      MLog.error("MJPopupMenuField", "actionPerformed",
          "Le champ n'est pas un lien vers une autre table !");
    }
  }
}
