/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MACheckBoxMenuItem - CREATION : 2005/11/28
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import morrigan.admin.ui.enumeration.MEActionMenu;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.graphique.menu.MJCheckBoxMenuItem;

public class MACheckBoxMenuItem extends MJCheckBoxMenuItem implements
    MIMenuElement {

  private MASysWindow fenWindow = null;

  private MEActionMenu mam = null;

  public MACheckBoxMenuItem(String nom, MASysWindow fenWindow) {
    super(nom);
    if (nom == null) {
      throw new IllegalArgumentException("nom cannot be null");
    }
    if (fenWindow == null) {
      throw new IllegalArgumentException("fenWindow cannot be null");
    }

    this.mam = MEActionMenu.TOGGLE;
    this.fenWindow = fenWindow;
  }

  public MEActionMenu getActionMenu() {
    return mam;
  }

  public MASysWindow getFenetre() {
    return fenWindow;
  }
}
