/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAJTable.java - CREATION : 2005/01/16
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.ui;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;

import morrigan.admin.util.MADonnees;
import morrigan.admin.util.MAInfosColonne;
import morrigan.graphique.table.MJTable;
import morrigan.log.MLog;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAJTable extends MJTable {

  public MAJTable(MADonnees md) {
    super(md, 1, 1);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  public MAJTable(MADonnees md, int iLargeurCell, int iHauteurCell) {
    super(md, iLargeurCell, iHauteurCell);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  @Override
  public void setModel(TableModel dataModel) {
    if (dataModel instanceof MADonnees) {
      setModel((MADonnees) dataModel);
    } else {
      MLog.error("MAJTable", "setModel",
          "Seul un MADonnees peut etre place comme modele ! (Plantage en vue)");
    }
  }

  public void setModel(MADonnees md) {
    super.setModel(md);
  }

  @Override
  public boolean isCellEditable(int row, int column) {
    return false;
  }

  @Override
  public void selectRow(int iIndexRow) {
    getSelectionModel().setLeadSelectionIndex(iIndexRow);
  }

  @Override
  public void clearSelection() {
    getSelectionModel().clearSelection();
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    super.valueChanged(e);
    // Si plusieurs passages, on ne prend que le dernier (on evite les
    // surcharges...)
    if (e.getValueIsAdjusting()) {
      return;
    }

    // TODO: Attention: code pas propre.
    // (Recherche, Selection d'un resultat au-dela du 2e, Recherche, Recherche
    // => BUG)
    if (getDonneesModel().getRowCount() > getSelectedRow()
        && getSelectedRow() >= 0)
      getDonneesModel().setLigneActive(getSelectedRow());
  }

  public MADonnees getDonneesModel() {
    return (MADonnees) super.getModel();
  }
  
  @Override
  protected String getToolTip(int iColonne) {
    MAInfosColonne infosLigne = (MAInfosColonne) getDonneesModel().getInfoCol(iColonne);
    
    StringBuffer tooltip = new StringBuffer("<HTML>");
    tooltip.append("<I>Comment : </I>" + infosLigne.getComment() + "<BR>");
    tooltip.append("<I>Def : </I>" + infosLigne.getColumnName() + "<BR>");
    tooltip.append("<I>Type : </I>" + infosLigne.getColumnType() + "<BR>");
    tooltip.append("<I>Database : </I>" + infosLigne.getNomReelChamp() + "<BR>");
    tooltip.append("</HTML>");
    return tooltip.toString();
  }

}
