/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenPreferences.java - CREATION : 2004/08/25
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MAPreferenceContext;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MAChoiceListField;
import morrigan.admin.ui.field.MATextField;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJLabel;
import morrigan.log.MLog;

/**
 * La classe des preferences contient tout ce que l'utilisateur peut modifier
 * pour rendre l'application la plus pratique possible, selon ses goûts.
 * 
 * @author armel
 */
public class MAFenPreferences extends MAFenetre implements
    PropertyChangeListener {
  private final String[] sElementsDate = new String[] { "dd", "MM", "yy",
      "yyyy" };

  private MAChoiceListField mclfElementDate1 = null;

  private MAChoiceListField mclfElementDate2 = null;

  private MAChoiceListField mclfElementDate3 = null;

  private MAChoiceListField mclfSeparateurDate = null;

  private MATextField mtfExempleDate = null;

  private MADonnees mdPref = null;

  /**
   * Default constructor.
   */
  public MAFenPreferences() {
    super("Preferences", 14, 18);
    init();
    loadPreferences();
    setModeAffichage(MEAffichage.LECTURE);

    // Seul le bouton de validation est possible, ici.
    setPanelBoutonsAutorises(false);
    // Attention: Autoriser ne signifie pas rendre le bouton "clicable". Il faut
    // aussi que les
    // champs obligatoires soient tous remplis.
    setPanelBoutonAutorise(BOUTON.VALIDER, true);

    refreshEnabledBoutons();
  }

  /**
   * Init screen
   */
  private void init() {
    // Declarations
    final int iLargeurLabel = 6;
    final int iLargeurChamps = 10;

    // Declarations
    MJLabel mlFormatDate = new MJLabel("Format date", iLargeurLabel, 1);
    MJLabel mlSeparateurDate = new MJLabel("Separateur", iLargeurLabel, 1);
    mclfElementDate1 = new MAChoiceListField(1, iLargeurChamps / 3,
        new String[] { "Valeur" }, sElementsDate);
    mclfElementDate1.setMandatory(true);
    mclfElementDate2 = new MAChoiceListField(1, iLargeurChamps / 3,
        new String[] { "Valeur" }, sElementsDate);
    mclfElementDate2.setMandatory(true);
    mclfElementDate3 = new MAChoiceListField(1, iLargeurChamps / 3,
        new String[] { "Valeur" }, sElementsDate);
    mclfElementDate3.setMandatory(true);
    mclfSeparateurDate = new MAChoiceListField(1, iLargeurChamps / 3,
        new String[] { "Valeur" }, new String[] { "/", "-" });
    mclfSeparateurDate.setMandatory(true);

    mtfExempleDate = new MATextField(1, iLargeurChamps * 2 / 3);
//    mtfExempleDate.setEnabled(false);
    mtfExempleDate.setEditable(false);

    mlFormatDate.setLabelFor(mclfElementDate1);
    mlSeparateurDate.setLabelFor(mclfSeparateurDate);

    // Listeners

    mclfElementDate1.addPropertyChangeListener(this);
    mclfElementDate2.addPropertyChangeListener(this);
    mclfElementDate3.addPropertyChangeListener(this);
    mclfSeparateurDate.addPropertyChangeListener(this);

    // Placement
    int xLabel = 1;
    int yLabel = 2;
    int xChamps = xLabel + iLargeurLabel + 1;

    mpqPrinc.add(mlFormatDate, xLabel, yLabel);
    mpqPrinc.add(mclfElementDate1, xChamps, yLabel);
    mpqPrinc.add(mclfElementDate2, xChamps + iLargeurChamps / 3, yLabel);
    mpqPrinc.add(mclfElementDate3, xChamps + iLargeurChamps * 2 / 3, yLabel++);
    mpqPrinc.add(mlSeparateurDate, xLabel, yLabel);
    mpqPrinc.add(mclfSeparateurDate, xChamps, yLabel);
    mpqPrinc.add(mtfExempleDate, xChamps + iLargeurChamps / 3, yLabel++);

    mdPref = new MADonnees(new String[] { "elementDate1", "elementDate2",
        "elementDate3", "separateurDate" });
    mdPref.associate(mclfElementDate1, "elementDate1");
    mdPref.associate(mclfElementDate2, "elementDate2");
    mdPref.associate(mclfElementDate3, "elementDate3");
    mdPref.associate(mclfSeparateurDate, "separateurDate");
  }

  /**
   * Load from the preference context and set user values into the fields.
   */
  private void loadPreferences() {
    MAPreferenceContext preferenceContext = MAContextManager.getInstance()
        .getPreferenceContext();
    String dateFormat = preferenceContext.getDateFormat();
    // Look for the separator character
    final String emptyString = "";
    for (String elementDate : sElementsDate) {
      dateFormat = dateFormat.replaceAll(elementDate, emptyString);
    }
    // When all possible user values are removed, 2 characters remains : the
    // separator x 2
    String sep = dateFormat.substring(0, 1);
    // Split the date format
    dateFormat = preferenceContext.getDateFormat();
    String[] dateElements = dateFormat.split(sep);

    mclfElementDate1.setValue(dateElements[0]);
    mclfElementDate2.setValue(dateElements[1]);
    mclfElementDate3.setValue(dateElements[2]);
    mclfSeparateurDate.setValue(sep);
  }

  /** {@inheritDoc} */
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("value")) {
      // FIXME: faire une interface pour le changement de valeur d'un champ
      String sep = mclfSeparateurDate.getText();
      String d1 = mclfElementDate1.getText();
      String d2 = mclfElementDate2.getText();
      String d3 = mclfElementDate3.getText();
      final String format = d1 + sep + d2 + sep + d3;

      MLog.debug("MAFenPreferences", "propertyChange", "Format=" + format);
      try {
        String sDateAffichee = new SimpleDateFormat(format).format(Calendar
            .getInstance().getTime());
        mtfExempleDate.setText(sDateAffichee);
      } catch (Exception e) {
        mtfExempleDate.setText("Erreur !");
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  protected int onValiderPressed() {
    String sep = mclfSeparateurDate.getText();
    String d1 = mclfElementDate1.getText();
    String d2 = mclfElementDate2.getText();
    String d3 = mclfElementDate3.getText();
    final String dateFormat = d1 + sep + d2 + sep + d3;

    MAContextManager.getInstance().getPreferenceContext().setDateFormat(
        dateFormat);
    // MAdminAppliMain.savePreferences();

    return super.onValiderPressed();
  }
}