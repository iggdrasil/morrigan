/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MSplashScreen.java - CREATION : 2005/12/03
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingConstants;

import morrigan.admin.MAdminAppliMain;
import morrigan.graphique.MImageLoader;
import morrigan.log.MLog;

public class MSplashScreen extends JWindow implements ActionListener {
  protected JLabel jlSplashLabelImage = null;

  private JProgressBar progressBar = null;

  /**
   * @param imageFileName
   * @param totalStepNumber
   */
  public MSplashScreen(String imageFileName, int totalStepNumber) {
    // Creation des composants
    final JPanel jpGeneral = new JPanel(new BorderLayout());
    jpGeneral.setBorder(BorderFactory.createRaisedBevelBorder());
    jlSplashLabelImage = new JLabel(createImageIcon(imageFileName));

    final JPanel jpImage = new JPanel();
    final JPanel jpPanelProgress = new JPanel();

    progressBar = new JProgressBar(SwingConstants.HORIZONTAL, 0,
        totalStepNumber - 1) {
      @Override
      public Dimension getPreferredSize() {
        // return new Dimension(300, super.getPreferredSize().height);
        return new Dimension(jlSplashLabelImage.getWidth(), 15);
      }
    };
    progressBar.setString("");
    progressBar.setStringPainted(true);
    // progressBar.setIndeterminate(true);

    jpPanelProgress.add(progressBar);

    setContentPane(jpGeneral);
    jpImage.add(jlSplashLabelImage);
    jpGeneral.add(jpImage, BorderLayout.NORTH);
    jpGeneral.add(jpPanelProgress, BorderLayout.CENTER);
    pack();

    // Position du Splash
    final Rectangle screenRect = new JFrame().getGraphicsConfiguration()
        .getBounds();
    setLocation(screenRect.x + screenRect.width / 2 - getSize().width / 2,
        screenRect.y + screenRect.height / 2 - getSize().height / 2);
    // setAlwaysOnTop(true);
  }

  public ImageIcon createImageIcon(String filename) {
    final String path = filename;
    // return new FileManager().loadIcon(path);
    return MImageLoader.loadIcon(path);
  }

  public void notifyStep(int stepNumber, String currentStep) {
    progressBar.setValue(stepNumber);
    progressBar.setString(currentStep);
    MLog.infos(currentStep);
  }

  public void notifierErreurEtape(String sErreur) {
    final String sMessage = progressBar.getString() + " ERREUR : " + sErreur;
    progressBar.setString(sMessage);
    System.out.println(sMessage);

    // Creation d'un bouton pour quitter
    // TODO : mail
    JPanel jpBouton = new JPanel();
    JButton jbQuitter = new JButton("Quitter");
    jbQuitter.addActionListener(this);
    jpBouton.add(jbQuitter);

    // Couleur de la progression faite
    progressBar.setForeground(Color.decode("#C32B2B"));
    // Couleur de la progression a faire
    progressBar.setBackground(Color.decode("#E0BFBF"));

    getContentPane().add(jpBouton, BorderLayout.SOUTH);
    pack();
  }

  public void actionPerformed(ActionEvent e) {
    MAdminAppliMain.quitter(-1);
  }

  @Override
  public void setVisible(boolean bVisible) {
    super.setVisible(bVisible);

    // Le spashscreen est fait pour etre appele UNE fois. Le cacher le
    // detruit... (memoire
    // oblige)
    if (!bVisible)
      jlSplashLabelImage = null;
  }

}
