package morrigan.admin.specif.login;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.loader.model.Authentication;
import morrigan.admin.context.loader.model.Connection;
import morrigan.admin.context.loader.model.Context;
import morrigan.admin.context.loader.model.GlobalContext;
import morrigan.admin.database.DatabaseConnexionRetriever;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MAChoiceListField;
import morrigan.admin.ui.field.MAPasswordField;
import morrigan.admin.ui.field.MATextField;
import morrigan.admin.util.MADialogue;
import morrigan.admin.util.MADonnees;
import morrigan.admin.util.MADialogue.MESSAGE;
import morrigan.graphique.MJLabel;
import morrigan.graphique.MStylable.STYLE;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

public class LoginFen extends MAFenetre {

  private MADonnees mdLogin;

  private MATextField matLogin;

  private MAPasswordField matPassword;

  private MJLabel mlProfile;

  private MAChoiceListField maclProfile;

  private MAChoiceListField maclContext;

  private MJLabel mlStatus;

  private Semaphore semaphore;

  private Properties dbProperties = new Properties();

  private String choosenContextName = null;

  private MJLabel mlLogin;

  private MJLabel mlPassword;

  /**
   * Default constructor.
   */
  public LoginFen() {
    this("Login", 9, 18);
  }

  public LoginFen(String titre, int hauteurCell, int largeurCell) {
    super(titre, hauteurCell, largeurCell);
    setModeAffichage(MEAffichage.LECTURE);
    // Seul le bouton de validation est possible, ici.
    setPanelBoutonsAutorises(false);
    // Attention: Autoriser ne signifie pas rendre le bouton "clicable". Il faut
    // aussi que les champs obligatoires soient tous remplis.
    setPanelBoutonAutorise(BOUTON.VALIDER, true);

    build();

    final Rectangle screenRect = new JFrame().getGraphicsConfiguration()
        .getBounds();
    int x = screenRect.x + screenRect.width / 2 - getSize().width / 2;
    int y = screenRect.y + screenRect.height / 2 - getSize().height / 2 + 180;
    setLocation(x, y);

    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  /**
   * Init screen
   */
  private void build() {
    // Declarations
    final int iLargeurLabel = 6;
    final int iLargeurChamps = 10;

    MJLabel mlContext = new MJLabel("Environment", iLargeurLabel, 1);
    mlLogin = new MJLabel("Login", iLargeurLabel, 1);
    mlPassword = new MJLabel("Password", iLargeurLabel, 1);
    mlProfile = new MJLabel("Profile", iLargeurLabel, 1);
    mlStatus = new MJLabel("", iLargeurLabel + iLargeurChamps + 1, 2,
        STYLE.ITALIQUE);

    String[] contextNames = retrieveContextNames();
    maclContext = new MAChoiceListField(1, iLargeurChamps,
        new String[] { "environment" }, contextNames);
    maclContext.setMandatory(true);
    matLogin = new MATextField(1, iLargeurChamps);
    matPassword = new MAPasswordField(1, iLargeurChamps);
    maclProfile = new MAChoiceListField(1, iLargeurChamps,
        new String[] { "profile" }, new String[] { "supermj", "mjlieu",
            "mjobjet" });

    // Labels <> fields
    mlLogin.setLabelFor(matLogin);
    mlPassword.setLabelFor(matPassword);
    mlContext.setLabelFor(maclContext);
    mlProfile.setLabelFor(maclProfile);

    // Placement
    int xLabel = 1;
    int yLabel = 2;
    int xChamps = xLabel + iLargeurLabel + 1;

    mpqPrinc.add(mlContext, xLabel, yLabel);
    mpqPrinc.add(maclContext, xChamps, yLabel++);
    yLabel++;
    mpqPrinc.add(mlLogin, xLabel, yLabel);
    mpqPrinc.add(matLogin, xChamps, yLabel++);
    mpqPrinc.add(mlPassword, xLabel, yLabel);
    mpqPrinc.add(matPassword, xChamps, yLabel++);
    mpqPrinc.add(mlProfile, xLabel, yLabel);
    mpqPrinc.add(maclProfile, xChamps, yLabel++);
    mpqPrinc.add(mlStatus, xLabel, yLabel++);

    mdLogin = new MADonnees(new String[] { "login", "password", "profile",
        "env" });
    mdLogin.associate(matLogin, "login");
    mdLogin.associate(matPassword, "password");
    mdLogin.associate(maclProfile, "profile");
    mdLogin.associate(maclContext, "env");

    String defaultContextName = retrieveDefaultContextName();
    mdLogin.setValueAt(defaultContextName, "env");

    // Listeners
    TableModelListener tableModelListener = new TableModelListener() {
      public void tableChanged(TableModelEvent e) {
        if ("env".equals(mdLogin.getColumnName(e.getColumn()))) {
          updateFieldsAccess();
        } else {
          checkFields();
        }
      }
    };
    mdLogin.addTableModelListener(tableModelListener);

    init();
  }

  public void init() {
    updateFieldsAccess();
    freezeFields(true);
    mlStatus.setText(StringUtils.EMPTY);
  }

  /**
   * Update the fields access according to the selected environment.
   */
  private void updateFieldsAccess() {
    boolean isAccessible = false;
    choosenContextName = String.valueOf(maclContext.getValue());
    if (!StringUtils.isEmpty(maclContext.getText())) {
      Context context = MAContextManager.getInstance().getGlobalContext()
          .getContexts().get(choosenContextName);
      isAccessible = context.isAuthenticationNeeded();
    }

    mlLogin.setEnabled(isAccessible);
    mlPassword.setEnabled(isAccessible);
    mlProfile.setEnabled(isAccessible);

    matLogin.setEnabled(isAccessible);
    matPassword.setEnabled(isAccessible);
    maclProfile.setEnabled(isAccessible);

    matLogin.setMandatory(isAccessible);
    matPassword.setMandatory(isAccessible);
    maclProfile.setMandatory(isAccessible);
  }

  private String retrieveDefaultContextName() {
    return MAContextManager.getInstance().getGlobalContext()
        .getCurrentContextName();
  }

  /**
   * @return the context names extracted from configuration file (from the
   *         Context Manager)
   */
  private String[] retrieveContextNames() {
    GlobalContext globalContext = MAContextManager.getInstance()
        .getGlobalContext();
    Set<String> keySet = globalContext.getContexts().keySet();
    String[] contextNames = keySet.toArray(new String[keySet.size()]);
    return contextNames;
  }

  public void checkFields() {
    boolean ok = isValidLoginText(matLogin.getText())
        && isValidLoginText(new String(matPassword.getPassword()));
    setPanelBoutonEnabled(BOUTON.VALIDER, ok);
  }

  /**
   * Test if the characters are valid. ie : belong to a-z, A-Z, 0-9, - or _.
   * 
   * @param s the string to test
   * @return true if the characters are valid; false otherwise
   */
  private boolean isValidLoginText(String s) {
    boolean valid = false;
    if (s == null || StringUtils.isBlank(s)) {
      return false;
    }

    Pattern pattern = Pattern.compile("[a-zA-Z-_ 0-9]+");
    Matcher m = pattern.matcher(s);
    valid = m.matches();
    return valid;
  }

  @Override
  protected int onValiderPressed() {

    // Screen fields freezed
    showMessage(Color.yellow, "Being authenticated...");
    freezeFields(false);

    GlobalContext globalContext = MAContextManager.getInstance()
        .getGlobalContext();
    choosenContextName = maclContext.getValue().toString();
    Context potentialContext = globalContext.getContexts().get(
        choosenContextName);

    // If an authentication name is specified, a connection to the
    // authentication server is needed.
    if (potentialContext.isAuthenticationNeeded()) {
      final String login = matLogin.getText();
      final String password = String.copyValueOf(matPassword.getPassword());
      String authenticationName = potentialContext.getAuthenticationName();
      Authentication authentication = globalContext.getAuthentications().get(
          authenticationName);
      DbConnRetrieverThread retrieverThread = new DbConnRetrieverThread(
          authentication.getAddress(), login, password);
      retrieverThread.start();
    } else {
      // Otherwise, use the db properties.
      String connectionName = potentialContext.getConnectionName();
      Connection connection = globalContext.getConnections()
          .get(connectionName);
      Validate.notNull(connection, "No connection could be found for '"
          + connectionName + "' name");

      dbProperties = new Properties();
      dbProperties.setProperty("db", connection.getDatabaseName());
      dbProperties.setProperty("user", connection.getUser());
      dbProperties.setProperty("password", connection.getPassword());
      dbProperties.setProperty("host", connection.getIp());
      dbProperties.setProperty("port", connection.getPort());

      showMessage(Color.yellow, "By-pass authentication.");
      semaphore.release();
    }

    return super.onValiderPressed();
  }

  /**
   * Show a message in a specific color, using the Swing Thread.
   * 
   * @param fgColor
   * @param string
   */
  private void showMessage(final Color fgColor, final String string) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        mlStatus.setForeground(fgColor);
        mlStatus.setText(string);
      }
    });
  }

  /**
   * Freeze the fields so that they cannot/can be changed
   * 
   * @param enabled
   */
  private void freezeFields(final boolean enabled) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        setPanelBoutonEnabled(BOUTON.VALIDER, enabled);
        matLogin.setEnabled(enabled);
        matPassword.setEnabled(enabled);
        maclProfile.setEnabled(enabled);
        maclContext.setEnabled(enabled);
      }
    });
  }

  public void setLock(Semaphore semaphore) {
    this.semaphore = semaphore;
  }

  /**
   * @return the dbProperties
   */
  public Properties getDbProperties() {
    return dbProperties;
  }

  /**
   * @return the choosenContextName
   */
  public String getChoosenContextName() {
    return choosenContextName;
  }

  /**
   * @author alagadic
   */
  private final class DbConnRetrieverThread extends Thread {
    private final String login;

    private final String password;

    private final String address;

    private DbConnRetrieverThread(String address, String login, String password) {
      this.address = address;
      this.login = login;
      this.password = password;
    }

    @Override
    public void run() {
      DatabaseConnexionRetriever dbRetriever = new DatabaseConnexionRetriever();
      dbProperties = dbRetriever.doPost(address, false, login, password);
      choosenContextName = maclContext.getText();

      if (dbProperties != null) {
        showMessage(Color.yellow, "User authenticated.");
        semaphore.release();
      } else {
        showMessage(Color.red, "Wrong login/password");
        MADialogue.ouvrirDialogue(LoginFen.this, MESSAGE.ERREUR,
            "Identification : Wrong login/password");
        freezeFields(true);
      }
    }
  }
}
