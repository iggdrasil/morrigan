/*
 * Cree le 14 mars 2006
 * Ce fichier appartient au projet Morrigan.
 */
package morrigan.admin.specif;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;

import morrigan.admin.MAWindowManager;
import morrigan.admin.MAdminAppliMain;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.ui.enumeration.MEAWindow;
import morrigan.graphique.MImageLoader;
import morrigan.graphique.MStylable;
import morrigan.graphique.bouton.MJButton;
import morrigan.graphique.fenetre.MFenDialog;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJPanelAbout;
import morrigan.graphique.panel.MJPanel.BORDURE;

/**
 * About window
 * 
 * @author Armel
 */
public class MAFenAbout extends MFenDialog {

  public MAFenAbout() {
    super(null, "A propos");
  }

  @Override
  protected void remplir() {
    setContentPane(new MJPanel(BORDURE.VIDE, 5, 5, 10, 5));
    setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
    MJPanel mjpBouton = new MJPanel(BORDURE.VIDE);
    mjpBouton.setLayout(new BoxLayout(mjpBouton, BoxLayout.LINE_AXIS));

    final MJButton mjbOk = new MJButton(" Ok ", MStylable.STYLE.NORMAL);
    mjbOk.addActionListener(this);
    mjpBouton.add(Box.createGlue());
    mjpBouton.add(mjbOk);
    mjpBouton.add(Box.createGlue());

    String image = MAContextManager.getInstance().getGraphicContext().getSplashScreenImage();
    add(new MJPanelAbout(MAdminAppliMain.getApplicationName(), MAdminAppliMain
        .getApplicationVersion(), MImageLoader.loadIcon(image)));
    add(Box.createRigidArea(new Dimension(10, 10)));
    add(mjpBouton);
  }

  @Override
  public void launch() {}

  public void actionPerformed(ActionEvent e) {
    MAWindowManager.closeWindow(MAContextManager.getInstance()
        .getWindowContext().getSysWindowByWindowName(
            MEAWindow.APROPOS.getNomSys()));
  }

}
