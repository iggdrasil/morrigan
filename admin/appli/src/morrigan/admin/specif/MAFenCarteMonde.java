/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenCarteMonde.java - CREATION : 2005/05/14
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.ScrollPaneConstants;

import morrigan.admin.database.DBManager;
import morrigan.admin.ui.MAJTable;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MATextField;
import morrigan.admin.util.MADonnees;
import morrigan.graphique.MJLabel;
import morrigan.graphique.field.MIntegerField;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.table.MDonnees;
import morrigan.log.MLog;

public class MAFenCarteMonde extends MAFenetre {

  // private MADonnees mdMonde = null;
  // private METable eNomTable = null; // METable.CARTEMONDE;
  // private final String sTrigTable = "mnd";

  final int iEchelle = 7; // n pixels pour 1 cote de case sur la carte

  public MAFenCarteMonde() {
    super("Carte du monde", 30, 24);
    init();
  }

  protected void init() {
    // Declarations
    final int iLargeurLabel = 8;
    final int iHauteurDessin = 17;
    final int iHauteurTableauRegion = 5;

    // Libelles
    MJLabel mlId = new MJLabel("ID", iLargeurLabel, 1);
    MJLabel mlNom = new MJLabel("Nom", iLargeurLabel, 1);

    MJLabel mlRegions = new MJLabel("Regions", iLargeurLabel, 1);

    // Champs
    MIntegerField mifId = new MIntegerField(1, iLargeurLabel);
    mifId.setEnabled(false);
    MATextField mtfNom = new MATextField(1, iLargeurLabel, true);
    mtfNom.setValue("Ank");
    mtfNom.setMandatory(true);

    MADonnees mdRegion = new MADonnees(new String[] { "rgn_id", "rgn_nom",
        "rgn_surface", "rgn_trg_id" });
    mdRegion.setValueAt("COucou", "rgn_id");
    MJPanelQuad contentPane = (MJPanelQuad) getContentPane();
    MAJTable mjtRegion = new MAJTable(mdRegion,
        contentPane.getLargeurCell() - 2, iHauteurTableauRegion);
    MJScrollPane mjsRegion = new MJScrollPane(mjtRegion);

    // Placement
    int yLabel = 2;
    final int xLabel = 1;
    final int xChamps = 8;
    contentPane.add(mlId, xLabel, yLabel);
    contentPane.add(mifId, xChamps, yLabel);
    contentPane.add(mlNom, xLabel, ++yLabel);
    contentPane.add(mtfNom, xChamps, yLabel);

    // Dessin
    yLabel += 2;

    MJPanelDessin mjDessinMonde = new MJPanelDessin(contentPane
        .getLargeurCell() - 2, iHauteurDessin);
    MJScrollPane mjsp = new MJScrollPane(mjDessinMonde,
        ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    // Positionnement du pas
    mjsp.getHorizontalScrollBar().setBlockIncrement(4 * iEchelle);
    mjsp.getVerticalScrollBar().setBlockIncrement(4 * iEchelle);
    mjsp.getHorizontalScrollBar().setUnitIncrement(iEchelle);
    mjsp.getVerticalScrollBar().setUnitIncrement(iEchelle);
    contentPane.add(mjsp, xLabel, yLabel);

    yLabel += iHauteurDessin;

    mjDessinMonde.setDimensionsMonde(new Dimension(50, 50)); // TODO:
    // dimensions en
    // dur !!

    contentPane.add(mlRegions, xLabel, yLabel);
    contentPane.add(mjsRegion, xLabel, ++yLabel);

  }

  @Override
  protected int onRechercherPressed() {
    if (mjbRechercher.isSelected()) {
      MLog.debug("MAFenetre", "onSearchPressed", "Recherche activee");
      setModeAffichage(MEAffichage.RECHERCHE);
    } else {
      MLog.debug("MAFenetre", "onSearchPressed", "Resultat");
      setModeAffichage(MEAffichage.LECTURE);
    }

    return 0;
  }

  private MRegion[] getSurfacesRegions() {
    MDonnees mdRegion = new MDonnees(new String[] { "rgn_id", "rgn_mnd_id",
        "rgn_nom", "rgn_surface", "rgn_trg_id", "rgn_description",
        "rgn_acces_libre", "trg_id", "trg_nom", "trg_couleur" });

    final String sNomTable = "region";
    final String sTrigTable = "rgn";

    if (mdRegion == null || sNomTable == null || sTrigTable == null) {
      return null;
    }

    // Purge prealable des donnees
    mdRegion.clearData();

    ResultSet rsDonnees = null;
    try {
      // Construction des colonnes pour le select
      StringBuffer sSelect = new StringBuffer("SELECT ");
      for (int iCol = 1; iCol + 1 < mdRegion.getColumnCount(); iCol++) {
        sSelect.append(mdRegion.getColumnName(iCol)).append(", ");
      }

      sSelect.append(mdRegion.getColumnName(mdRegion.getColumnCount() - 1));

      // On recupere toutes les donnees de la table.
      rsDonnees = DBManager.select(sSelect.append(" FROM ").append(sNomTable)
          .append(", type_region").append(" WHERE rgn_trg_id = trg_id ")
          .append(" ORDER BY ").append(sTrigTable).append("_id ").toString());

      MLog.debug("MAFenetre", "remplir", sSelect.toString());

      if (rsDonnees == null) {
        MLog.error(this.getClass().toString(), "remplir",
            "Pas de connexion active...");
        throw new Exception("Pas de connexion active...");
      }

      MLog.debug(this.getClass().toString(), "remplir",
          "Warnings eventuels de la requete: " + rsDonnees.getWarnings());

      List<Object> vLigne = null;
      Object oDonnee = null;

      // Parcours des lignes de resultat et insertion dans le tableau
      while (rsDonnees.next()) {
        vLigne = new ArrayList<Object>();
        for (int i = 0; i < mdRegion.getColumnCount(); i++) {
          try {
            oDonnee = rsDonnees.getObject(mdRegion.getColumnName(i));
            vLigne.add(oDonnee);
          } catch (Exception e) {
            vLigne.add("?");
            MLog.error(this.getClass().toString(), "remplir",
                "Erreur lecture des donnees de la base.", e);
          }
        }

        mdRegion.addRow(vLigne);
      }

      if (vLigne == null) {
        MLog.debug(this.getClass().toString(), "remplir", "Table " + sNomTable
            + " : aucune donnee trouvee...");
        throw new Exception("Table " + sNomTable
            + " : aucune donnee trouvee...");
      }
      
      // Constructions des polygones (= regions)
      MRegion[] mRegions = new MRegion[mdRegion.getRowCount()];
      for (int iRegion = 0; iRegion < mdRegion.getRowCount(); iRegion++) {
        String sCouleur = mdRegion.getValueAt(iRegion, "trg_couleur")
            .toString().trim();
        // sCouleur.replaceFirst("#", "0x");
        Color cCouleur;
        try {
          sCouleur = "0x" + sCouleur;
          cCouleur = Color.decode(sCouleur.trim());
        } catch (Exception e) {
          cCouleur = Color.GRAY;
          MLog
              .error(
                  this.getClass().toString(),
                  "remplir",
                  "Erreur lecture de couleur de region. Application de la couleur par defaut.",
                  e);
        }

        Polygon pRegion = new Polygon();
        String sPolygon = mdRegion.getValueAt(iRegion, "rgn_surface")
            .toString();
        sPolygon = sPolygon.substring(2, sPolygon.length() - 2);
        StringTokenizer st = new StringTokenizer(sPolygon, "),(");

        // Parcours des coordonnees
        while (st.hasMoreTokens()) {
          // Les coordonnees vont par paires
          String sX = st.nextToken();
          String sY = st.nextToken();
          pRegion
              .addPoint(new Double(sX).intValue(), new Double(sY).intValue());
        }
        String sNom = mdRegion.getValueAt(iRegion, "rgn_nom").toString();
        mRegions[iRegion] = new MRegion(pRegion, sNom, cCouleur);
      }

      return mRegions;

    } catch (Exception e) {
      MLog.error(this.getClass().toString(), "remplir",
          "Erreur de recuperation d'infos.", e);
    }

    return null;
  }

  class MJPanelDessin extends MJPanelQuad implements MouseListener {

    final Color cBg = Color.white;

    final Color cFg = Color.black;

    final Color cCase = Color.gray;

    final Color fg3D = Color.lightGray;

    Dimension dTailleTotale;

    MRegion[] mRegions;

    Dimension dMonde;

    final int iMarge = 3;

    public MJPanelDessin(int largeurCell, int hauteurCell) {
      super(MJPanel.BORDURE.RAISED, largeurCell, hauteurCell);
      this.addMouseListener(this);
    }

    public void setDimensionsMonde(Dimension dMonde) {
      this.dMonde = dMonde;
      this.imposerTaille(dMonde.width * iEchelle + 2 * iMarge, dMonde.height
          * iEchelle + 2 * iMarge);
    }

    public void setRegions(MRegion[] mRegions) {
      this.mRegions = mRegions;
    }

    public void mouseClicked(MouseEvent e) {
      Point pClic = e.getPoint();

      MLog.infos("clic dans cell | X="
          + (int) ((pClic.getX() - iMarge) / iEchelle));
      MLog.infos("clic dans cell | Y="
          + (int) ((pClic.getY() - iMarge) / iEchelle));
    }

    public void mouseEntered(MouseEvent e) {}

    public void mouseExited(MouseEvent e) {}

    public void mousePressed(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

    public void paint(Graphics g) {
      final Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);

      final double dCoeff = iEchelle;

      final Stroke sPinceau = new BasicStroke(10f, BasicStroke.CAP_SQUARE,
          BasicStroke.JOIN_MITER);
      final Stroke sPinceauCase = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE,
          BasicStroke.JOIN_MITER);
      g2.setStroke(sPinceau);

      // Ecart entre le le bord et le centre d'une cellule
      final double dDelta = dCoeff / 2;

      if (mRegions != null)
        for (MRegion region : mRegions) {
          final Polygon pRegion = region.getSurface();

          final int xPoints[] = new int[pRegion.npoints];
          final int yPoints[] = new int[pRegion.npoints];

          // Mise a l'echelle du panel/monde
          for (int iPoint = 0; iPoint < pRegion.npoints; iPoint++) {
            xPoints[iPoint] = (int) (iMarge + dDelta + dCoeff
                * pRegion.xpoints[iPoint]);
            yPoints[iPoint] = (int) (iMarge + dDelta + dCoeff
                * pRegion.ypoints[iPoint]);

            // MLog.infos("SqlPoint (" + xPoints[iPoint] + "," + yPoints[iPoint]
            // + ")");
          }

          final GeneralPath filledPolygon = new GeneralPath(
              GeneralPath.WIND_EVEN_ODD, pRegion.npoints);
          filledPolygon.moveTo(xPoints[0], yPoints[0]);
          for (int index = 1; index < xPoints.length; index++) {
            filledPolygon.lineTo(xPoints[index], yPoints[index]);
          }
          filledPolygon.closePath();
          g2.setPaint(region.getCouleur());
          g2.fill(filledPolygon);
          // g2.setPaint(cFg);
          g2.draw(filledPolygon);
        }

      g2.setPaint(cCase);
      g2.setStroke(sPinceauCase);

      for (int iCol = 0; iCol <= dMonde.width; iCol++) {
        g2.draw(new Line2D.Double(iMarge + iCol * dCoeff, iMarge, iMarge + iCol
            * dCoeff, iMarge + dMonde.height * dCoeff));
      }

      for (int iLigne = 0; iLigne <= dMonde.height; iLigne++) {
        g2.draw(new Line2D.Double(iMarge, iMarge + iLigne * dCoeff, iMarge
            + dMonde.width * dCoeff, iMarge + iLigne * dCoeff));
      }
    }
  }

  private class MRegion {

    Polygon pSurface;

    String sNom;

    Color cSurface;

    MRegion(Polygon pSurface, String sNom, Color cSurface) {
      this.pSurface = pSurface;
      this.sNom = sNom;
      this.cSurface = cSurface;
    }

    public Polygon getSurface() {
      return pSurface;
    }

    public Color getCouleur() {
      return cSurface;
    }

    public String getNom() {
      return sNom;
    }
  }
}
