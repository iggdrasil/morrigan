/*
 * ####################################################################### #
 * PROJECT : Morrigan - FILE : MAFenDetailMonde.java - CREATION : 2008/01/08 # #
 * Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com> # # This program can
 * be distributed under the terms of the GNU GPL. # See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.modele.Cartographie;
import morrigan.admin.modele.Matrix;
import morrigan.admin.modele.Point;
import morrigan.admin.modele.PointSelectionListener;
import morrigan.admin.modele.SelectionEvent;
import morrigan.admin.service.MondeService;
import morrigan.admin.service.ServiceManager;
import morrigan.admin.specif.cartographie.CartoPanel;
import morrigan.admin.specif.cartographie.SelectionManager;
import morrigan.admin.ui.enumeration.MEAffichage;
import morrigan.admin.ui.fenetre.MAFenetre;
import morrigan.admin.ui.field.MATextField;
import morrigan.admin.util.MADonnees;
import morrigan.admin.util.Transcoder;
import morrigan.graphique.MJLabel;
import morrigan.graphique.field.MIntegerField;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJTabbedPane;

/**
 * Selection du monde dans une combo. n onglets :
 * <li>Region
 * <li>Environnement
 * <li>Climat
 * <li>Courant marin
 * <li>Courant aerien (vent)
 * <li>... ? + Detail du point
 * 
 * @author Siltaom
 */
public class MAFenDetailMonde extends MAFenetre implements
    PointSelectionListener {

  // private MADonnees mdMonde = null;
  // private METable eNomTable = null; // METable.CARTEMONDE;
  // private final String sTrigTable = "mnd";

  /**
   * Regions composant le monde
   */
  private Cartographie cartographie = null;

  private static final int width = 50;

  private static final int height = 24;

  private Table pointTable = null;

  private final int MND_ID = 0;

  private SelectionManager selectionManager;

  /**
   * Default constructor.
   * @throws CreateWindowException 
   */
  public MAFenDetailMonde() throws CreateWindowException {
    super("Carte du monde", height, width);
    this.cartographie = loadCartographie();
    initScreen();

    setModeAffichage(MEAffichage.LECTURE);

    // Seul le bouton de validation est possible, ici.
    setPanelBoutonsAutorises(false);
    // Attention: Autoriser ne signifie pas rendre le bouton "clicable". Il faut
    // aussi que les
    // champs obligatoires soient tous remplis.
    setPanelBoutonAutorise(BOUTON.VALIDER, true);

    refreshEnabledBoutons();
  }

  /**
   * Only for test.
   * 
   * @param cartographie
   * @throws CreateWindowException 
   * @deprecated use withour parameter instead.
   */
  public MAFenDetailMonde(Cartographie cartographie) throws CreateWindowException {
    super("Carte du monde", width, height);
    this.cartographie = cartographie;
    initScreen();
  }

  private Cartographie loadCartographie() {
    MondeService mondeService = (MondeService) ServiceManager
        .getService(MondeService.class);
    Cartographie carto = mondeService.getCartographie(MND_ID);
    return carto;
  }

  protected void initScreen() throws CreateWindowException {
    // Declarations
    final int iLargeurLabel = 8;
    final int iHauteurDessin = 17;
    final int iLargeurDessin = width * 2 / 3;
    final int iHauteurTableauRegion = 5;

    // Libelles
    MJLabel mlIdMonde = new MJLabel("ID monde", iLargeurLabel, 1);
    MJLabel mlIdSaison = new MJLabel("ID saison", iLargeurLabel, 1);
    MJLabel mlNom = new MJLabel("Nom", iLargeurLabel, 1);

    MJLabel mlRegions = new MJLabel("Regions", iLargeurLabel, 1);

    // Champs
    MIntegerField mifId = new MIntegerField(1, iLargeurLabel);
    mifId.setEnabled(false);
    MATextField mtfNom = new MATextField(1, iLargeurLabel, true);
    mtfNom.setValue("Ank");
    mtfNom.setMandatory(true);

    pointTable = MAContextManager.getInstance().getDbContext().getTableParNom(
        "lieu");
    MADonnees mPointDonnees = new Transcoder()
        .transcodeTableIntoMADonnees(pointTable);
    mPointDonnees.setValueAt("Coucou", "rgn_id");

    // Placement
    int yLabel = 2;
    final int xLabel = 1;
    final int xChamps = 8;
    mpqPrinc.add(mlIdMonde, xLabel, yLabel);
    mpqPrinc.add(mifId, xChamps, yLabel);
    mpqPrinc.add(mlNom, xLabel, ++yLabel);
    mpqPrinc.add(mtfNom, xChamps, yLabel);

    // Dessin
    yLabel += 2;

    MJPanelQuad pointPanel = buildPointPanel(width - iLargeurDessin, 10);
    mpqPrinc.add(pointPanel, xLabel + iLargeurDessin + 1, yLabel + 1);

    selectionManager = new SelectionManager();
    MJTabbedPane tabbedPane = null;
    Set<Entry<String, Matrix<?>>> entries = cartographie.matrixSet();
    for (Entry<String, Matrix<?>> entry : entries) {
      entry.getKey();
      Matrix<?> matrix = entry.getValue();
      CartoPanel cartoPanel = buildPanel(matrix);
      cartoPanel.setSelectionManager(selectionManager);
      selectionManager.addListener(this);

      // Add tabbed panel
      if (tabbedPane == null) {
        tabbedPane = new MJTabbedPane(iHauteurDessin, iLargeurDessin);
        mpqPrinc.add(tabbedPane, xLabel, yLabel);
        yLabel += iHauteurDessin;
      }
      tabbedPane.add(entry.getKey(), cartoPanel);
    }

    // CartoPanel cartoPanel = new CartoPanel(panel.getLargeurCell() - 2,
    // iHauteurDessin);
    // MJScrollPane mjsp = new MJScrollPane(cartoPanel,
    // ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
    // ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    // Positionnement du pas
    // mjsp.getHorizontalScrollBar().setBlockIncrement(4 * CartoHelper.echelle);
    // mjsp.getVerticalScrollBar().setBlockIncrement(4 * CartoHelper.echelle);
    // mjsp.getHorizontalScrollBar().setUnitIncrement(CartoHelper.echelle);
    // mjsp.getVerticalScrollBar().setUnitIncrement(CartoHelper.echelle);
    // panel.add(mjsp, xLabel, yLabel);
    // cartoPanel.setDimensionsMonde(new Dimension(50, 50));

    mpqPrinc.add(mlRegions, xLabel, yLabel);
  }

  private MJPanelQuad buildPointPanel(int largeur, int hauteur) {

    MJPanelQuad panel = new MJPanelQuad(MJPanel.BORDURE.ETCHED, largeur,
        hauteur);

    final int xLabel = 1;
    final int xLargeurLabel = largeur / 2 - 1;
    final int xLargeurChamps = largeur - xLabel - xLargeurLabel - 1;

    MJLabel mlId = new MJLabel("ID", xLargeurLabel, 1);
    MIntegerField mifId = new MIntegerField(1, xLargeurChamps);
    MJLabel mlId2 = new MJLabel("ID2", xLargeurLabel, 1);
    MIntegerField mifId2 = new MIntegerField(1, xLargeurChamps);

    // Placement
    int yLabel = 2;
    panel.add(mlId, xLabel, yLabel);
    panel.add(mifId, xLargeurChamps, yLabel);
    panel.add(mlId2, xLabel, ++yLabel);
    panel.add(mifId2, xLargeurChamps, yLabel);

    return panel;
  }

  private CartoPanel buildPanel(Matrix<?> matrix) {
    CartoPanel cartoPanel = new CartoPanel(1, 1);
    cartoPanel.setDimensionsMonde(new Dimension(matrix.getLineSize(), matrix
        .getNumberOfLines()));
    return cartoPanel;
  }

  /** {@inheritDoc} */
  public void pointSelectionChanged(SelectionEvent selectionEvent) {
    Map<Point, Rectangle> selectedCells = selectionEvent.getSelectedCells();
    if (!selectedCells.isEmpty()) {

    } else {

    }
  }
}
