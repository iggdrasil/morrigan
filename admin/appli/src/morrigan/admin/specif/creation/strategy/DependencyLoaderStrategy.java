package morrigan.admin.specif.creation.strategy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.database.tabledesc.attribute.AttributeForeignKey;
import morrigan.admin.specif.creation.TableNode;
import morrigan.admin.specif.creation.CreationFen.KEY;

import org.apache.commons.lang.Validate;


/**
 * This strategy is used to determine the usage of OTHER tables for an element
 * of this one.
 * 
 * @author armel
 */
public class DependencyLoaderStrategy extends LinkLoaderStrategy {
  private KEY keyType = KEY.DEPENDENCY;

  public DependencyLoaderStrategy() {}

  @Override
  public Set<Attribute> retrieveChildrenKeys(Attribute attribute) {
    Table table = null;
    if (attribute.isID()) {
      table = attribute.getTable();
    } else {
      // Foreign
      table = ((AttributeForeignKey) attribute).getForeignTable();
    }
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    Set<Attribute> linkedAttributes = new HashSet<Attribute>();
    // 'personnage'
    // This table needs prs_jou_id (jou_id => 'joueur'), prs_tpr_id (tpr_id =>
    // 'type_personnage'),
    // prs_lie_id (lie_id => 'lieu'),

    Map<String, String> foreignToPrimaryKeyLinks = dbContext
        .getForeignToPrimaryKeyLinks();
    for (Iterator<Attribute> iterator = table.getAttributesIterator(); iterator
        .hasNext();) {
      Attribute anAttribute = iterator.next();
      if (anAttribute.isForeignID()) {
        String otherPrimaryKey = foreignToPrimaryKeyLinks.get(anAttribute
            .getName());
        Validate.notNull(otherPrimaryKey, "The foreign key attribute '"
            + anAttribute.getName() + "' (table="
            + anAttribute.getTable().getName() + ") has no linked table.");
        // Attribute otherPkAttribute = dbContext
        // .getAttributeByName(otherPrimaryKey);
        linkedAttributes.add(anAttribute);
      }
    }

    return linkedAttributes;
  }

  public KEY getKeyType() {
    return keyType;
  }
  
  @Override
  public TableNode createNode(Attribute keyAttribute) {
    Table linkedTable;
    if (keyAttribute.isForeignID()) {
      AttributeForeignKey fo = (AttributeForeignKey) keyAttribute;
      linkedTable = fo.getForeignTable();
    } else {
      linkedTable = keyAttribute.getTable();
    }
    return new TableNode(keyAttribute, linkedTable);
  }

}
