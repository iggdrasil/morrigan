package morrigan.admin.specif.creation.strategy;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.TableNode;
import morrigan.graphique.Colors;

public abstract class AbstractTreeCellRenderer extends DefaultTreeCellRenderer {

  private final Color fg;

  public AbstractTreeCellRenderer() {
    fg = Colors.getTreeTextColor();
  }

  public Color getFg() {
    return fg;
  }

  @Override
  public Component getTreeCellRendererComponent(JTree tree, Object value,
      boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
        hasFocus);

    if (value instanceof DefaultMutableTreeNode) {
      DefaultMutableTreeNode current = (DefaultMutableTreeNode) value;
      DefaultMutableTreeNode parent = (DefaultMutableTreeNode) current
          .getParent();
      if (parent != null && parent.getUserObject() instanceof TableNode
          && current.getUserObject() instanceof TableNode) {
        TableNode parentUserObject = (TableNode) parent.getUserObject();
        TableNode currentUserObject = (TableNode) current.getUserObject();
        Attribute attributeInfoToUse = currentUserObject.getKeyAttribute();
        setForeground(attributeInfoToUse.isMandatory() ? Colors
            .getMandatoryColor() : getFg());
        Table tableForText = retrieveTableInfoToUseForText(parentUserObject,
            currentUserObject);
        setText(tableForText.getName() + " [" + attributeInfoToUse.getLabel()
            + "]");
        setToolTipText(createTooltipText(attributeInfoToUse));
      }
    }

    return this;
  }

  private String createTooltipText(Attribute attribute) {
    StringBuffer tooltip = new StringBuffer("<HTML>");
    tooltip.append("<I>Name : </I>" + attribute.getName() + "<BR>");
    tooltip.append("<I>Comment : </I>" + attribute.getComment() + "<BR>");
    tooltip.append("</HTML>");
    return tooltip.toString();

  }

  abstract Table retrieveTableInfoToUseForText(TableNode parentUserObject,
      TableNode currentUserObject);
}
