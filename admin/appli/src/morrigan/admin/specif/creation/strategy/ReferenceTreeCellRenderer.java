package morrigan.admin.specif.creation.strategy;

import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.TableNode;

public class ReferenceTreeCellRenderer extends AbstractTreeCellRenderer {

  public ReferenceTreeCellRenderer() {
  }

  @Override
  Table retrieveTableInfoToUseForText(TableNode parentUserObject,
      TableNode currentUserObject) {
    return currentUserObject.getKeyAttribute().getTable();
  }
}
