package morrigan.admin.specif.creation.strategy;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.database.tabledesc.attribute.AttributeForeignKey;
import morrigan.admin.specif.creation.TableNode;

public class DependencyTreeCellRenderer extends AbstractTreeCellRenderer {
  public DependencyTreeCellRenderer() {
  }
  
  @Override
  Table retrieveTableInfoToUseForText(TableNode parentUserObject,
      TableNode currentUserObject) {
    Attribute attribute = currentUserObject.getKeyAttribute();
    if (attribute instanceof AttributeForeignKey) {
      AttributeForeignKey fk = (AttributeForeignKey) attribute;
      return fk.getForeignTable();
    }
    return attribute.getTable();
  }
}
