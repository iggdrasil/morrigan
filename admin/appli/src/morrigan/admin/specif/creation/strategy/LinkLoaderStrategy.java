package morrigan.admin.specif.creation.strategy;

import java.util.Set;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.TableNode;
import morrigan.admin.specif.creation.CreationFen.KEY;

/**
 * Loader of 'children' according to a context.
 * 
 * @author armel
 */
public abstract class LinkLoaderStrategy {
  private static final ReferenceLoaderStrategy referenceLoaderStrategy = new ReferenceLoaderStrategy();

  private static final DependencyLoaderStrategy dependencyLoaderStrategy = new DependencyLoaderStrategy();

  public LinkLoaderStrategy() {}

  public Attribute retrievePrimaryKey(Table aTable) {
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    String trig = dbContext.getLightTableParNom(aTable.getName()).getTrig();
    Attribute pkAttribute = dbContext.getAttributeByName(trig.concat("_id"));
    return pkAttribute;
  }

  public abstract Set<Attribute> retrieveChildrenKeys(Attribute attribute);
  
  abstract public TableNode createNode(Attribute keyAttribute);

  /**
   * Creates a link loader according to the key sens (primary <?> foreign).
   * 
   * @param key
   * @return a {@link LinkLoaderStrategy}
   */
  public static LinkLoaderStrategy createLinkLoader(KEY key) {
    switch (key) {
      case REFERENCE:
        return referenceLoaderStrategy;
      case DEPENDENCY:
        return dependencyLoaderStrategy;
      default:
        throw new RuntimeException("The link key type is unknown : " + key);
    }
  }

}
