package morrigan.admin.specif.creation.strategy;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.TableNode;
import morrigan.admin.specif.creation.CreationFen.KEY;
import morrigan.log.MLog;

/**
 * This strategy is used to determine the usage of this table in OTHER tables.
 * 
 * @author armel
 */
public class ReferenceLoaderStrategy extends LinkLoaderStrategy {
  private final KEY keyType = KEY.REFERENCE;

  public ReferenceLoaderStrategy() {}

  @Override
  public Set<Attribute> retrieveChildrenKeys(Attribute attribute) {
    Table table = attribute.getTable();
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    Set<Attribute> linkedAttributes = new HashSet<Attribute>();
    // joueur
    // => jou_id used in 'mj' and 'personnage'
    Map<String, Set<String>> allLinks = dbContext
        .getPrimaryToForeignKeysLinks();
    String primaryKeyName = retrievePrimaryKey(table).getName();
    Set<String> links = allLinks.get(primaryKeyName);
    if (links == null || links.isEmpty()) {
      MLog.infos("No dependency found for table '" + table.getName()
          + "', pk='" + primaryKeyName + "'");
      return Collections.emptySet();
    }
    for (String linkedKey : links) {
      Attribute fkAttribute = dbContext.getAttributeByName(linkedKey);
      linkedAttributes.add(fkAttribute);
    }

    return linkedAttributes;
  }

  public KEY getKeyType() {
    return keyType;
  }
  
  @Override
  public TableNode createNode(Attribute keyAttribute) {
    Table linkedTable = keyAttribute.getTable();
    return new TableNode(keyAttribute, linkedTable);
  }
}
