package morrigan.admin.specif.creation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.exception.OpenWindowException;
import morrigan.admin.specif.creation.CreationFen.KEY;
import morrigan.admin.specif.creation.expander.MandatoryNodeExpanderVisitor;
import morrigan.admin.specif.creation.expander.NLevelExpanderVisitor;
import morrigan.admin.specif.creation.expander.NodeVisitor;
import morrigan.admin.specif.creation.strategy.LinkLoaderStrategy;
import morrigan.admin.ui.MAJMenuItem;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.graphique.MJPopupMenu;
import morrigan.log.MLog;

import org.apache.commons.lang.Validate;

public class JTreeManager implements TreeWillExpandListener, ActionListener {

  private static final String OPEN_TABLE = "Open table '";

  private VisitableTreeNode root = null;

  private DefaultTreeModel model = null;

  private KEY keyType = KEY.REFERENCE;

  private Table targetTable;

  private final JTree tree;

  private LinkLoaderStrategy linkLoader;

  private MJPopupMenu popup = null;

  private MAJMenuItem menuItemOpenTable;

  public JTreeManager(JTree tree) {
    this.tree = tree;
    this.model = (DefaultTreeModel) tree.getModel();
    this.root = (VisitableTreeNode) tree.getModel().getRoot();

    linkLoader = LinkLoaderStrategy.createLinkLoader(keyType);
    tree.addMouseListener(new PopupListener());

    popup = new MJPopupMenu();
    menuItemOpenTable = new MAJMenuItem(OPEN_TABLE, (MASysWindow) null);
    menuItemOpenTable.addActionListener(this);
    popup.add(menuItemOpenTable);
  }

  /** {@inheritDoc} */
  public void treeWillExpand(TreeExpansionEvent event)
      throws ExpandVetoException {
    MLog.debug("CreationFen", "treeWillExpand", "in");

    TreePath source = event.getPath();
    VisitableTreeNode treeNode = (VisitableTreeNode) source
        .getLastPathComponent();
    loadChildrenDependenciesOf(treeNode);
  }

  /** {@inheritDoc} */
  public void treeWillCollapse(TreeExpansionEvent event)
      throws ExpandVetoException {}

  private void loadChildrenDependenciesOf(VisitableTreeNode father) {
    Enumeration<VisitableTreeNode> children = father.children();
    while (children.hasMoreElements()) {
      VisitableTreeNode child = children.nextElement();

      // If leaf, try to load dependencies
      if (child.isLeaf()) {
        TableNode tableNode = (TableNode) child.getUserObject();
        Set<Attribute> linkedKeys = linkLoader.retrieveChildrenKeys(tableNode
            .getKeyAttribute());
        createSubNodes(child, linkedKeys);
      }
    }
  }

  private void createSubNodes(VisitableTreeNode father,
      Set<Attribute> linkedKeys) {
    Validate.notNull(father, "The local root must no be null");
    Validate.notNull(linkedKeys, "The foreign key collection must not be null");

    for (Attribute linkedKey : linkedKeys) {
      TableNode userObject = createNode(linkedKey);
      VisitableTreeNode node = new VisitableTreeNode(userObject);
      father.add(node);
    }
  }

  public void loadDependencies() {
    flush();
    Validate.notNull(targetTable,
        "The table in which you want to create must not be null");
    Set<Attribute> linkedKeys = new HashSet<Attribute>();
    Attribute primaryKey = retrievePrimaryKey(targetTable);
    linkedKeys = linkLoader.retrieveChildrenKeys(primaryKey);
    createSubNodes(root, linkedKeys);
    loadChildrenDependenciesOf(root);
  }

  public void setKeyType(KEY keyType) {
    this.keyType = keyType;
    linkLoader = LinkLoaderStrategy.createLinkLoader(keyType);
  }

  private Attribute retrievePrimaryKey(Table aTable) {
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    String trig = dbContext.getLightTableParNom(aTable.getName()).getTrig();
    Attribute pkAttribute = dbContext.getAttributeByName(trig.concat("_id"));
    return pkAttribute;
  }

  private TableNode createNode(Table aTable) {
    Attribute primaryKey = retrievePrimaryKey(aTable);
    return createNode(primaryKey);
  }

  private TableNode createNode(Attribute keyAttribute) {
    return linkLoader.createNode(keyAttribute);
  }

  public void setTargetTable(Table targetTable) {
    this.targetTable = targetTable;
    root.setUserObject(createNode(this.targetTable));
    tree.expandPath(new TreePath(root.getPath()));
  }

  public void flush() {
    root.removeAllChildren();
  }

  public void refresh() {
    model.reload();
  }

  /**
   * Expand the nodes 'under' the root node, until another node encountered is
   * about the same table.
   */
  public void expandAll() {
    // Develop until 4st level
    NodeVisitor visitor = new NLevelExpanderVisitor(this, 4);
    root.accept(visitor);
    // Then develop mandatory nodes
    visitor = new MandatoryNodeExpanderVisitor(this);
    root.accept(visitor);
  }

  /**
   * Collapse all nodes.
   */
  public void collapseAll() {
    int row = tree.getRowCount() - 1;
    while (row >= 0) {
      tree.collapseRow(row);
      row--;
    }
  }

  /**
   * @return the tree
   */
  public JTree getTree() {
    return tree;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == menuItemOpenTable) {
      MASysWindow sysWindow = menuItemOpenTable.getFenetre();
      try {
        MAWindowManager.openWindow(sysWindow);
      } catch (OpenWindowException e1) {
        e1.printStackTrace();
      } catch (CreateWindowException e1) {
        e1.printStackTrace();
      }
    }
  }

  /**
   * @author Armel
   */
  class PopupListener extends MouseAdapter {
    @Override
    public void mouseReleased(MouseEvent e) {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
      if (e.isPopupTrigger() || e.getButton() >= 2) {
        // Pas tres propre mais bon...

        TreePath path = tree.getPathForLocation(e.getX(), e.getY());
        if (path != null) {
          Object lastPathComponent = path.getLastPathComponent();
          VisitableTreeNode node = (VisitableTreeNode) lastPathComponent;
          TableNode tableNode = (TableNode) node.getUserObject();
          if (tableNode != null) {
            Table linkedTable = tableNode.getLinkedTable();
            String sysname = linkedTable.getName();
            MASysWindow sysWindow = MAContextManager.getInstance()
                .getWindowContext().getSysWindowByWindowName(sysname);
            menuItemOpenTable.setText(OPEN_TABLE + sysWindow.getSysName() + "'");
            menuItemOpenTable.setFenetre(sysWindow);
            MLog.debug("JTreeManager", "mouseClicked", lastPathComponent
                .toString());
          }
        }
        popup.show(e.getComponent(), e.getX(), e.getY());
      }
    }
  }

}
