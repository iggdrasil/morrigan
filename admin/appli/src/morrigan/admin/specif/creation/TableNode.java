package morrigan.admin.specif.creation;

import morrigan.admin.database.tabledesc.Attribute;
import morrigan.admin.database.tabledesc.Table;

public class TableNode {

  private final Attribute keyAttribute;
  
  private final Table linkedTable;

  public TableNode(Attribute keyAttribute, Table linkedTable) {
    this.keyAttribute = keyAttribute;
    this.linkedTable = linkedTable;
  }
  
  @Override
  public String toString() {
    return keyAttribute.getName() + "   [" + linkedTable.getName() + "]";
  }
  
  public Table getLinkedTable() {
    return linkedTable;
  }
  
  /**
   * @return the attribute
   */
  public Attribute getKeyAttribute() {
    return keyAttribute;
  }
}
