package morrigan.admin.specif.creation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.tree.DefaultTreeModel;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.context.MADbContext;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.strategy.AbstractTreeCellRenderer;
import morrigan.admin.specif.creation.strategy.DependencyTreeCellRenderer;
import morrigan.admin.specif.creation.strategy.ReferenceTreeCellRenderer;
import morrigan.graphique.MJLabel;
import morrigan.graphique.MStylable.STYLE;
import morrigan.graphique.bouton.MJButton;
import morrigan.graphique.bouton.MJToggleButton;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.graphique.panel.MJPanelQuad;
import morrigan.graphique.panel.MJScrollPane;
import morrigan.graphique.panel.MJTree;
import morrigan.graphique.panel.MJPanel.BORDURE;

import org.apache.commons.lang.Validate;

public class CreationFen extends MJFramePosition {

  private static final String DEPENDENCIES = "Dependencies";

  private static final String COLLAPSE = "Collapse";

  private static final String EXPAND = "Expand";

  private final MJTree tree;

  private final MJLabel label;

  private Table targetTable;

  private final JTreeManager treeManager;

  static final int LARGEUR = 25;

  private MJToggleButton dependencyButton;

  public CreationFen() {
    this("Creation");
  }

  public CreationFen(String titre) {
    super(titre, LARGEUR - 2, 20);

    // 1 : Buttons
    MJPanelQuad buttonPanel = buildButtonPanel();

    // 2 : Label (helper)
    label = new MJLabel("information", LARGEUR, 1, STYLE.NORMAL);

    // 3 : tree in its scroll pane
    tree = new MJTree(LARGEUR, 18);
    ((DefaultTreeModel)tree.getModel()).setRoot(new VisitableTreeNode());
    MJScrollPane treeView = new MJScrollPane(tree);

    MJPanelQuad contentPane = new MJPanelQuad(BORDURE.VIDE, LARGEUR, 20);
    setContentPane(contentPane);
    contentPane.add(buttonPanel, 0, 0);
    contentPane.add(label, 0, 1);
    contentPane.add(treeView, 0, 2);

    treeManager = new JTreeManager(tree);
    tree.addTreeWillExpandListener(treeManager);
    tree.addMouseListener(new MouseAdapter(){
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
      }
      
    });

    fireKeyChanged(dependencyButton.isSelected() ? KEY.REFERENCE
        : KEY.DEPENDENCY);
  }

  private MJPanelQuad buildButtonPanel() {
    MJPanelQuad buttonPanel = new MJPanelQuad(BORDURE.ETCHED, LARGEUR, 1);
    MJButton expandButton = new MJButton("X", 2, 1);
    expandButton.setName(EXPAND);
    expandButton.setToolTipText(EXPAND);
    expandButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        treeManager.expandAll();
      }
    });
    buttonPanel.add(expandButton, 0, 0);

    MJButton collapseButton = new MJButton("x", 2, 1);
    collapseButton.setName(COLLAPSE);
    collapseButton.setToolTipText("Collapse");
    collapseButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        treeManager.collapseAll();
      }
    });
    buttonPanel.add(collapseButton, 3, 0);

    dependencyButton = new MJToggleButton("", 4, 1);
    dependencyButton.setName(DEPENDENCIES);
    dependencyButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        boolean selected = dependencyButton.isSelected();
        fireKeyChanged(selected ? KEY.REFERENCE : KEY.DEPENDENCY);
      }
    });
    buttonPanel.add(dependencyButton, 6, 0);
    return buttonPanel;
  }

  private void fireKeyChanged(KEY newKey) {
    boolean isDep = newKey.equals(KEY.DEPENDENCY);
    
    String labelText = null;
    String toggleText = null;
    String mode;
    AbstractTreeCellRenderer renderer;
    
    if (isDep) {
      labelText =(targetTable == null) ? "" : "Un(e) '"
        + targetTable.getName() + "' necessite les tables suivantes";
      toggleText = "Dep";
      mode = "DEPENDENCE";
      renderer = new DependencyTreeCellRenderer();
    } else {
      labelText = targetTable == null ? "" : "Un(e) '"
        + targetTable.getName() + "' est utile pour les tables suivantes";
      toggleText = "Ref";
      mode = "REFERENCE";
      renderer = new ReferenceTreeCellRenderer();
    }
    
    label.setText(labelText);
    
    dependencyButton.setText(toggleText);
    dependencyButton.setToolTipText("Passer en mode " + mode);

    tree.setCellRenderer(renderer);

    treeManager.setKeyType(newKey);
    if (targetTable != null) {
      treeManager.loadDependencies();
    }
    treeManager.refresh();
  }

  public void setTable(Table table) {
    Validate.notNull(table, "The target table must not be null");
    this.targetTable = table;
    setTitle("Creation assistee : " + this.targetTable.getName());
    treeManager.setTargetTable(table);
    fireKeyChanged(dependencyButton.isSelected() ? KEY.REFERENCE
        : KEY.DEPENDENCY);
  }

  public void setTable(String sysName) {
    Validate.notNull(sysName, "The target table name must not be null");
    MADbContext dbContext = MAContextManager.getInstance().getDbContext();
    setTable(dbContext.getTableParNom(sysName));
  }

  public void loadDependencies() {
    treeManager.loadDependencies();
    treeManager.refresh();
  }

  public enum KEY {
    REFERENCE, DEPENDENCY
  }

}
