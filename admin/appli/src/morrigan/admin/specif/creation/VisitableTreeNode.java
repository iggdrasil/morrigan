package morrigan.admin.specif.creation;

import javax.swing.tree.DefaultMutableTreeNode;

import morrigan.admin.specif.creation.expander.NodeVisitor;

public class VisitableTreeNode extends DefaultMutableTreeNode {
  public VisitableTreeNode() {
    super();
  }

  public VisitableTreeNode(TableNode userObject) {
    super(userObject);
  }

  void accept(NodeVisitor visitor) {
    visitor.visit(this);
  }
}
