package morrigan.admin.specif.creation.expander;

import java.util.Enumeration;

import javax.swing.tree.TreePath;

import morrigan.admin.specif.creation.JTreeManager;
import morrigan.admin.specif.creation.TableNode;
import morrigan.admin.specif.creation.VisitableTreeNode;

/**
 * Expand the nodes 'under' the father node, until another node encountered is
 * not mandatory.
 */
public class MandatoryNodeExpanderVisitor implements NodeVisitor {

  private final JTreeManager treeManager;

  public MandatoryNodeExpanderVisitor(JTreeManager treeManager) {
    this.treeManager = treeManager;
  }

  public void visit(VisitableTreeNode visitableTreeNode) {
    Enumeration<VisitableTreeNode> children = visitableTreeNode.children();
    while (children.hasMoreElements()) {
      VisitableTreeNode child = children.nextElement();
      TableNode userObject = (TableNode) child.getUserObject();
      TreePath path = new TreePath(child.getPath());
      if (treeManager.getTree().isExpanded(path) 
          || userObject.getKeyAttribute().isMandatory()) {
        treeManager.getTree().expandPath(path);
        visit(child);
      }
    }
  }

}
