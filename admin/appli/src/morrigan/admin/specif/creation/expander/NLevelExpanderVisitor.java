package morrigan.admin.specif.creation.expander;

import java.util.Enumeration;

import javax.swing.tree.TreePath;

import morrigan.admin.specif.creation.JTreeManager;
import morrigan.admin.specif.creation.VisitableTreeNode;

public class NLevelExpanderVisitor implements NodeVisitor {

  private final JTreeManager treeManager;

  private int maxLevel;

  public NLevelExpanderVisitor(JTreeManager treeManager, int maxLevel) {
    this.treeManager = treeManager;
    this.maxLevel = maxLevel;
  }

  public void visit(VisitableTreeNode visitableTreeNode) {
    Enumeration<VisitableTreeNode> children = visitableTreeNode.children();
    while (children.hasMoreElements()) {
      VisitableTreeNode child = children.nextElement();
      if (child.getLevel() < maxLevel) {
        treeManager.getTree().expandPath(new TreePath(child.getPath()));
        visit(child);
      }
    }
  }
}
