package morrigan.admin.specif.creation.expander;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.swing.tree.TreePath;

import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.specif.creation.JTreeManager;
import morrigan.admin.specif.creation.TableNode;
import morrigan.admin.specif.creation.VisitableTreeNode;

public class OnceListedNodeExpanderVisitor implements NodeVisitor {

  private final JTreeManager treeManager;

  private Set<Table> usedTables = new HashSet<Table>();

  public OnceListedNodeExpanderVisitor(JTreeManager treeManager) {
    this.treeManager = treeManager;
  }

  public void visit(VisitableTreeNode visitableTreeNode) {
    Enumeration<VisitableTreeNode> children = visitableTreeNode.children();
    while (children.hasMoreElements()) {
      VisitableTreeNode child = children.nextElement();
      TableNode userObject = (TableNode) child.getUserObject();
      Table table = userObject.getKeyAttribute().getTable();
      if (!usedTables.contains(table)) {
        treeManager.getTree().expandPath(new TreePath(child.getPath()));
        usedTables.add(table);
        visit(child);
      }
    }
  }
}
