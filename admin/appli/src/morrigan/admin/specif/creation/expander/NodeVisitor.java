package morrigan.admin.specif.creation.expander;

import morrigan.admin.specif.creation.VisitableTreeNode;

public interface NodeVisitor {

  void visit(VisitableTreeNode visitableTreeNode);
}
