/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenLog.java - CREATION : 2004/08/01
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
/*
 * Created on Aug 1, 2004 Ce fichier appartient au projet Morrigan.
 */
package morrigan.admin.specif;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import morrigan.admin.MAWindowManager;
import morrigan.admin.context.MAContextManager;
import morrigan.admin.ui.enumeration.MEAWindow;
import morrigan.graphique.fenetre.MJFramePosition;
import morrigan.log.MLogPanel;

/**
 * Cette classe appartient au projet Morrigan.
 * 
 * @author armel
 */
public class MAFenLog extends MJFramePosition {
  public MAFenLog() {
    super("Log");
    this.imposerTaille(600, 120);
    init();
    this.setLocation(0, getGraphicsConfiguration().getBounds().height
        - getHeight());
    this.setSize(getGraphicsConfiguration().getBounds().width, getHeight());
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    setResizable(true);

    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        MAWindowManager.closeWindow(MAContextManager.getInstance()
            .getWindowContext().getSysWindowByWindowName(
                MEAWindow.LOG.getNomSys()));
      }
    });
  }

  private void init() {
    this.setContentPane(new MLogPanel());
  }
}