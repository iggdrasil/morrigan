/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : SelectionManager.java - CREATION : 2008/01/08 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif.cartographie;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import morrigan.admin.modele.Point;
import morrigan.admin.modele.PointSelectionListener;
import morrigan.admin.modele.SelectionEvent;

/**
 * A selection manager.
 * 
 * @author armel
 */
public class SelectionManager {

  private List<PointSelectionListener> listeners = new ArrayList<PointSelectionListener>();

  private Point startPoint = null;

  private boolean selection;

  /** A map containing the selected points and their location and graphical size */
  private Map<Point, Rectangle> selectedCells = new HashMap<Point, Rectangle>();

  /**
   * Default constructor.
   */
  public SelectionManager() {}

  /**
   * @param startPoint the start point to set
   */
  public void setStartPoint(Point startPoint) {
    selection = !selectedCells.containsKey(startPoint);
    this.startPoint = startPoint;
  }

  /**
   * @param endPoint the selectionEnd to set
   */
  public void setEndPoint(Point endPoint) {

    int xMin = Math.min(startPoint.getX(), endPoint.getX());
    int xMax = Math.max(startPoint.getX(), endPoint.getX());
    int yMin = Math.min(startPoint.getY(), endPoint.getY());
    int yMax = Math.max(startPoint.getY(), endPoint.getY());

    for (int x = xMin; x <= xMax; x++) {
      for (int y = yMin; y <= yMax; y++) {
        Point point = new Point(x, y);
        boolean isPointListed = selectedCells.containsKey(point);
        if (selection) {
          if (!isPointListed) {
            selectedCells.put(point, CartoHelper.findBoundRectangle(point));
          }
        } else {
          if (isPointListed) {
            selectedCells.remove(point);
          }
        }
      }
    }
  }

  /**
   * @return the selected cells
   */
  public Map<Point, Rectangle> getSelectedCells() {
    return selectedCells;
  }

  public void reset() {
    selectedCells.clear();
  }

  /**
   * Notify the selection is complete.
   */
  public void fireSelectionEvent() {
    SelectionEvent selectionEvent = new SelectionEvent(selectedCells);
    for (PointSelectionListener listener : listeners) {
      listener.pointSelectionChanged(selectionEvent);
    }
  }

  /**
   * @param cartoPanel
   */
  public void addListener(PointSelectionListener pointSelectionListener) {
    listeners.add(pointSelectionListener);
  }

}