/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : CartoHelper.java - CREATION : 2008/01/08 
 * # 
 * # Copyright (C) 2005 Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * # This program can be distributed under the terms of the GNU GPL. 
 * # See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.specif.cartographie;

import java.awt.Dimension;
import java.awt.Rectangle;

import morrigan.admin.modele.Point;

/**
 * @author armel
 */
public class CartoHelper {

  /** n pixels pour 1 cote de case sur la carte */
  public static final int echelle = 7;

  public static final int margin = 3;

  public static Point convertAwtToCell(java.awt.Point point) {
    return new Point((int) ((point.getX() - CartoHelper.margin) / echelle),
        (int) ((point.getY() - CartoHelper.margin) / echelle));
  }

  public static java.awt.Point convertCellToAwt(Point point) {
    int x1 = CartoHelper.margin + point.getX() * CartoHelper.echelle;
    int y1 = CartoHelper.margin + point.getY() * CartoHelper.echelle;
    return new java.awt.Point(x1, y1);
  }

  public static Rectangle findBoundRectangle(Point point) {
    java.awt.Point awtPoint = CartoHelper.convertCellToAwt(point);
    Dimension dimension = new Dimension(CartoHelper.echelle,
        CartoHelper.echelle);
    return new Rectangle(awtPoint, dimension);
  }

}
