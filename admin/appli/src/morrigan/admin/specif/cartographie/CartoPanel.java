package morrigan.admin.specif.cartographie;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.util.Map.Entry;

import morrigan.admin.modele.Matrix;
import morrigan.admin.modele.Point;
import morrigan.admin.modele.PointSelectionListener;
import morrigan.admin.modele.SelectionEvent;
import morrigan.graphique.panel.MJPanel;
import morrigan.graphique.panel.MJPanelQuad;

public class CartoPanel extends MJPanelQuad implements MouseListener,
    MouseMotionListener, PointSelectionListener {

  private final Color foregroundColor = Color.lightGray;

  private final Color cellColor = Color.red;

  private Dimension dTailleTotale;

  private Matrix<?> matrix;

  private Dimension mondeDimension;

  private SelectionManager selectionManager;

  public CartoPanel(int largeurCell, int hauteurCell) {
    super(MJPanel.BORDURE.RAISED, largeurCell, hauteurCell);
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  /**
   * @param selectionManager the selectionManager to set
   */
  public void setSelectionManager(SelectionManager selectionManager) {
    this.selectionManager = selectionManager;
    selectionManager.addListener(this);
  }

  public void setDimensionsMonde(Dimension dMonde) {
    this.mondeDimension = dMonde;
    this.imposerTaille(dMonde.width * CartoHelper.echelle + 2
        * CartoHelper.margin, dMonde.height * CartoHelper.echelle + 2
        * CartoHelper.margin);
  }

  public void setMatrix(Matrix<?> matrix) {
    this.matrix = matrix;
  }

  /** {@inheritDoc} */
  public void mouseClicked(MouseEvent e) {}

  /** {@inheritDoc} */
  public void mouseEntered(MouseEvent e) {}

  /** {@inheritDoc} */
  public void mouseExited(MouseEvent e) {}

  /** {@inheritDoc} */
  public void mousePressed(MouseEvent e) {
    Point pressedCell = CartoHelper.convertAwtToCell(e.getPoint());
    if (!e.isControlDown()) {
      selectionManager.reset();
    }
    selectionManager.setStartPoint(pressedCell);
    repaint();
  }

  /** {@inheritDoc} */
  public void mouseReleased(MouseEvent e) {
    Point releasedCell = CartoHelper.convertAwtToCell(e.getPoint());
    selectionManager.setEndPoint(releasedCell);
    selectionManager.fireSelectionEvent();
    repaint();
  }

  /** {@inheritDoc} */
  public void mouseMoved(MouseEvent e) {
    setToolTipText("Test tool tip");
  }

  /** {@inheritDoc} */
  public void mouseDragged(MouseEvent e) {
    Point draggedPoint = CartoHelper.convertAwtToCell(e.getPoint());
    selectionManager.setEndPoint(draggedPoint);
    repaint();
  }

  /** {@inheritDoc} */
  public void pointSelectionChanged(SelectionEvent selectionEvent) {
  // System.out.println(selectionManager.getSelectedCells());
  }

  /** {@inheritDoc} */
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    final Graphics2D g2 = (Graphics2D) g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

    final double dCoeff = CartoHelper.echelle;
    final Stroke cellStroke = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE,
        BasicStroke.JOIN_MITER);

    // Grid drawing
    g2.setColor(foregroundColor);
    g2.setStroke(cellStroke);

    for (int column = 0; column <= mondeDimension.width; column++) {
      Line2D.Double verticalLine = new Line2D.Double(CartoHelper.margin
          + column * dCoeff, CartoHelper.margin, CartoHelper.margin + column
          * dCoeff, CartoHelper.margin + mondeDimension.height * dCoeff);
      g2.draw(verticalLine);
    }

    for (int iLigne = 0; iLigne <= mondeDimension.height; iLigne++) {
      Line2D.Double horizontalLine = new Line2D.Double(CartoHelper.margin,
          CartoHelper.margin + iLigne * dCoeff, CartoHelper.margin
              + mondeDimension.width * dCoeff, CartoHelper.margin + iLigne
              * dCoeff);
      g2.draw(horizontalLine);
    }

    g2.setColor(cellColor);
    g2.setStroke(cellStroke);

    // Selected cells drawing
    for (Entry<Point, Rectangle> entry : selectionManager.getSelectedCells()
        .entrySet()) {
      Rectangle value = entry.getValue();
      g2.draw(value);
    }
  }
}