package morrigan.admin.service;

import morrigan.admin.modele.Cartographie;
import morrigan.admin.modele.Monde;

/**
 * @author Armel
 */
public interface MondeService extends Service {

  public Monde getMonde(int mnd_id);

  public Cartographie getCartographie(int mnd_id);
}
