package morrigan.admin.service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Armel
 */
public class ServiceManager {

  private static final Map<Class<? extends Service>, Service> servicesImplementations;

  static {
    servicesImplementations = new HashMap<Class<? extends Service>, Service>();
    servicesImplementations.put(MondeService.class,
        new DefaultMondeServiceFromCsv());
  }

  static private boolean loaded = false;

  /**
   * Load services.
   */
  private ServiceManager() {}

  public static Service getService(Class<? extends Service> service) {
    if (!loaded) {
      for (Service serviceToLoad : servicesImplementations.values()) {
        serviceToLoad.start();
      }
      loaded = true;
    }
    return servicesImplementations.get(service);
  }
}
