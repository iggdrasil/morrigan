/*
 * DefaultMondeServiceFromCsv.java
 * 22 mars 08
 *
 * This file is a part of the Equity Product Manager (EProM)
 * Copyright (C) 2002-2007 SOCIETE GENERALE
 * All rights reserved.
 *
 */
package morrigan.admin.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.Validate;

import morrigan.admin.modele.Cartographie;
import morrigan.admin.modele.Matrix;
import morrigan.admin.modele.Monde;

/**
 * @author Armel
 */
public class DefaultMondeServiceFromCsv implements MondeService {

  private List<String[]> environmentCartos = null;

  private Cartographie cartographie = null;

  /** {@inheritDoc} */
  public void start() {
    try {
      environmentCartos = readFile();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    String[] envNames = new String[] { "environnement", "relief", "climat" };

    final int NB_COUCHES = envNames.length;
    final int Y = environmentCartos.size();
    final int X = environmentCartos.get(0).length;

    cartographie = new Cartographie();
    Integer[][][] points = new Integer[NB_COUCHES][X][Y];
    int y = 0;
    for (Iterator<String[]> iterator = environmentCartos.iterator(); iterator
        .hasNext();) {
      String[] line = iterator.next();
      int x = 0;
      for (String element : line) {
        if (element.length() != NB_COUCHES) {
          throw new RuntimeException("Info celle insuffisante (vs Couches)");
        }
        for (int idx = 0; idx < NB_COUCHES; idx++) {
          int a = Integer.parseInt(element.substring(idx, idx + 1));
          points[idx][x][y] = a;
        }
        x++;
      }
      y++;
    }

    cartographie.putMatrix("environnement", new Matrix<Integer>(points[0]));
    cartographie.putMatrix("climat", new Matrix<Integer>(points[1]));

    // System.out.println(environmentCartos);
  }

  private List<String[]> readFile() throws IOException {

    BufferedReader reader;
    FileReader fileReader = new FileReader("ressources/grid_bakkasab.csv");
    reader = new BufferedReader(fileReader);

    List<String[]> envCartos = new ArrayList<String[]>() {
      /** {@inheritDoc} */
      @Override
      public String toString() {
        StringBuffer buffer = new StringBuffer();
        for (String[] elements : this) {
          buffer.append("> ");
          for (String element : elements) {
            buffer.append(' ').append(element).append(' ');
          }
          buffer.append('\n');
        }
        return buffer.toString();
      }
    };

    String line = reader.readLine();
    while (line != null) {
      line = line.trim();
      // If this line is a comment, try the next one.
      if (line.length() == 0 || line.charAt(0) == '#') {
        continue;
      }
      StringTokenizer tokenizer = new StringTokenizer(line, ",");
      List<String> envLine = new ArrayList<String>();
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken();
        envLine.add(token);
      }

      String[] envAsIntLine = envLine.toArray(new String[envLine.size()]);
      envCartos.add(envAsIntLine);

      line = reader.readLine();
    }

    return envCartos;
  }

  /** {@inheritDoc} */
  public Cartographie getCartographie(int mnd_id) {
    return cartographie;
  }

  /** @deprecated */
  public Cartographie getCartographie2(int mnd_id) {
    Cartographie cartographie = new Cartographie();
    Validate.notNull(environmentCartos, "The cartographie has not been loaded");

    Integer[][] points = new Integer[environmentCartos.size()][environmentCartos
        .get(0).length];

    cartographie.putMatrix("environnement", new Matrix<Integer>(points));
    cartographie.putMatrix("climat", new Matrix<Integer>(points));

    return cartographie;
  }

  /** {@inheritDoc} */
  public Monde getMonde(int mnd_id) {
    return null;
  }
}
