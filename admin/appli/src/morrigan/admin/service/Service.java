package morrigan.admin.service;

/**
 * @author Armel
 */
public interface Service {

  /**
   * Start the service.
   */
  public void start();

}
