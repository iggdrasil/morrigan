package morrigan.admin.service;

import morrigan.admin.modele.Cartographie;
import morrigan.admin.modele.Matrix;
import morrigan.admin.modele.Monde;

/**
 * @author Armel
 */
public class DefaultMondeService implements MondeService {

  /** {@inheritDoc} */
  public void start() {}

  /** {@inheritDoc} */
  public Monde getMonde(int mnd_id) {
    return null;
  }

  public Cartographie getCartographie(int mnd_id) {
    Cartographie cartographie = new Cartographie();

    Integer[][] points = new Integer[50][100];

    cartographie.putMatrix("environnement", new Matrix<Integer>(points));
    cartographie.putMatrix("climat", new Matrix<Integer>(points));

    return cartographie;
  }
}
