/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAFenAdmin.java - CREATION : 2004/07/15
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JMenu;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.specif.creation.CreationFen;
import morrigan.admin.ui.MACheckBoxMenuItem;
import morrigan.admin.ui.MIMenuElement;
import morrigan.admin.ui.enumeration.MEASpecificWindow;
import morrigan.admin.ui.enumeration.MEActionMenu;
import morrigan.admin.ui.fenetre.MASysWindow;
import morrigan.common.command.Task;
import morrigan.common.command.TaskQueue;
import morrigan.common.command.TaskResult;
import morrigan.graphique.MImageLoader;
import morrigan.graphique.fenetre.MJFrame;
import morrigan.graphique.menu.MJCheckBoxMenuItem;
import morrigan.graphique.menu.MJMenu;
import morrigan.graphique.menu.MJMenuBar;
import morrigan.log.MLog;

import org.apache.commons.lang.Validate;

/**
 * @author armel
 */
public class MAFenAdmin extends MAdminAppliMain implements ActionListener,
    WindowListener {

  /** Checkbox on the log window */
  private MJCheckBoxMenuItem mcbFenetreLog = null;

  /**
   * Constructor of the administration window.
   */
  public MAFenAdmin() {
    super();
    this.setResizable(false);
    this.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        MAdminAppliMain.quitter(0);
      }
    });
    this.setIconImage(MImageLoader.load("ressources/images/morrigan_icon.png"));
  }

  /**
   * Apply menu.
   * 
   * @param mjbMenuBar new menu
   */
  public void applyMenu(MJMenuBar mjbMenuBar) {
    Validate.notNull(mjbMenuBar, "The menu bar must not be null");
    setJMenuBar(mjbMenuBar);
    pack();
    int iNbMenu = mjbMenuBar.getMenuCount();
    for (int iMenu = 0; iMenu < iNbMenu; iMenu++) {
      JMenu c = mjbMenuBar.getMenu(iMenu);
      applyActionListener(c);
    }
  }

  private void applyActionListener(Component c) {
    if (c instanceof MIMenuElement) {
      ((MIMenuElement) c).addActionListener(this);
    } else if (c instanceof MJMenu) {
      for (Component cFils : ((MJMenu) c).getMenuComponents()) {
        applyActionListener(cFils);
      }
    }
  }

  /**
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   * @param e Event
   */
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() instanceof MIMenuElement) {

      try {
        MIMenuElement menuElement = (MIMenuElement) e.getSource();
        MEActionMenu actionMenu = menuElement.getActionMenu();
        switch (actionMenu) {
        case OUVRIR_FENETRE:
          MASysWindow mFen = menuElement.getFenetre();
          MAWindowManager.openWindow(mFen);
          break;

        case CREATION:
          MASysWindow sysWindow = MAContextManager.getInstance()
              .getWindowContext().getSysWindowByWindowName(
                  MEASpecificWindow.CREATION.getSysName());
          CreationFen creationFen = (CreationFen) MAWindowManager
              .createWindow(sysWindow);
          MASysWindow fen = menuElement.getFenetre();
          creationFen.setTable(fen.getSysName());
          creationFen.loadDependencies();
          MAWindowManager.openWindow(sysWindow);
          break;

        case TOGGLE:
          MASysWindow mFenToggle = menuElement.getFenetre();
          if (((MACheckBoxMenuItem) e.getSource()).isSelected()) {
            MAWindowManager.openWindow(mFenToggle);
          } else {
            MAWindowManager.closeWindow(mFenToggle);
          }
          break;

        case QUITTER:
          MAdminAppliMain.quitter(0);
          break;

        case FERMER_TOUT:
          MAWindowManager.closeAllWindows();
          break;

        case OUVRIR_FENETRES_MENU:
          // TODO: implementer l'action OUVRIR_FENETRES_MENU
          throw new UnsupportedOperationException(
              "L'action OUVRIR_FENETRES_MENU n'est pas encore implementee");

        case SWITCH_USER:
          Task<TaskResult> task = new Task<TaskResult>(1000) {
            public TaskResult call() throws Exception {
              MAdminAppliMain.main();

              return new TaskResult(this, getNum(), 0);
            }
          };
          TaskQueue.getInstance().submit(task);

          break;

        default:
          throw new RuntimeException("Unknown action : " + actionMenu);
        }
      } catch (Exception ex) {
        MLog.error("MAFenAdmin", "actionPerformed", "Erreur a l'execution !",
            ex);
      }
    }
  }

  /**
   * Notify window opening.
   * 
   * @param fenetre window opened
   * @return <code>true</code> if ok; <code>false</code> otherwise
   */
  protected boolean notifyOpening(MASysWindow fenetre) {
    if (fenetre.getSysName().equals(MEASpecificWindow.LOG.getSysName())) {
      if (mcbFenetreLog != null && !mcbFenetreLog.isSelected()) {
        mcbFenetreLog.setSelected(true);
      }
    }

    return true;
  }

  /**
   * Notify window closing.
   * 
   * @param fenetre window closed
   * @return <code>true</code> if ok; <code>false</code> otherwise
   */
  protected boolean notifyClosing(MASysWindow fenetre) {
    if (fenetre.getSysName().equals(MEASpecificWindow.LOG.getSysName())) {
      if (mcbFenetreLog != null && mcbFenetreLog.isSelected()) {
        mcbFenetreLog.setSelected(false);
      }
    }

    return true;
  }

  /** {@inheritDoc} */
  public void windowActivated(WindowEvent e) {
  }

  /** {@inheritDoc} */
  public void windowClosed(WindowEvent e) {
  }

  /**
   * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
   */
  public void windowClosing(WindowEvent e) {
    // MLog.debug("MAFenAdmin", "windowClosing", "Fermeture de fenetre.");
    if (e.getSource() instanceof MJFrame) {
      MASysWindow fenetre = MAContextManager.getInstance().getWindowContext()
          .getSystemWindowByFrame((MJFrame) e.getSource());
      fenetre.saveLocationOnScreen();
      fenetre.setWindow(null);

      MLog.debug("MAFenAdmin", "windowClosing", "Fermeture de la fenetre "
          + fenetre.getSysName());
    }
  }

  /** {@inheritDoc} */
  public void windowDeactivated(WindowEvent e) {
  }

  /** {@inheritDoc} */
  public void windowDeiconified(WindowEvent e) {
  }

  /** {@inheritDoc} */
  public void windowIconified(WindowEvent e) {
  }

  /** {@inheritDoc} */
  public void windowOpened(WindowEvent e) {
  }
}