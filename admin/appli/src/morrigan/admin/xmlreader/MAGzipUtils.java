/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MAGzipUtils.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.xmlreader;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import morrigan.FileManager;

/**
 * Utils for GZ files.
 * 
 * @author Armel
 */
public final class MAGzipUtils {

  /** Buffer size for reading zip file */
  static final int BUFFER_SIZE = 1024;

  /**
   * Hidden constructor.
   */
  private MAGzipUtils() {}

  /**
   * Extract a gz file into a file.
   * 
   * @param gzFile the source file
   * @param extractedFile the destination file
   * @return the extracted file
   * @throws FileNotFoundException if the gz file is not found
   * @throws IOException if the reading fails
   */
  public static File extractTo(File gzFile, File extractedFile)
      throws FileNotFoundException, IOException {
    GZIPInputStream inputStream = null;
    FileOutputStream fileOutputStream = null;

    try {
      InputStream gzStream = new FileManager()
          .openFileToStream("ressources/schema.dia");
      // inputStream = new GZIPInputStream(new FileInputStream(gzFile));
      inputStream = new GZIPInputStream(gzStream);
      fileOutputStream = new FileOutputStream(extractedFile);
      MAGzipUtils.class.getClassLoader().getResource(extractedFile.getPath());

      byte[] tab = new byte[BUFFER_SIZE];
      int lu = 0;
      while (lu > -1) {
        lu = inputStream.read(tab);
        if (lu > -1) {
          fileOutputStream.write(tab, 0, lu);
        }
      }
    } finally {
      close(inputStream);
      close(fileOutputStream);
    }

    return extractedFile;
  }

  /**
   * Close the stream
   * 
   * @param stream the stream to close.
   */
  public static void close(Closeable stream) {
    if (stream != null) {
      try {
        stream.close();
      } catch (IOException e) {
        // Do nothing
      }
    }
  }

}
