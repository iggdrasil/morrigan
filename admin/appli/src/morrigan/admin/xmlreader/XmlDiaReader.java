/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : XmlDiaReader.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.xmlreader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.xmlreader.handler.AssociationHandler;
import morrigan.admin.xmlreader.handler.LightTableHandler;
import morrigan.admin.xmlreader.handler.MenuHandler;
import morrigan.admin.xmlreader.handler.TableHandler;
import morrigan.admin.xmlreader.handler.DiaXmlHandler;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

/**
 * Xml of dia reader.
 * 
 * @author armel
 */
public class XmlDiaReader {

  private List<DiaXmlHandler> listHandlers = new ArrayList<DiaXmlHandler>();

  private LightTableHandler lightTableHandler;

  private TableHandler tableHandler;

  private MenuHandler menuHandler;

  private final XPath xTable;

  private final XPath xAttributes;

  private final XPath xOperations;

  private final XPath xOpParameters;

  private final XPath xPlaceholder;

  private final boolean usePropertiesFile;

  private final XPath xAssociation;

  private final XPath xRoles;

  private AssociationHandler associationHandler;

  public XmlDiaReader(boolean usePropertiesFile) throws JDOMException {
    this.usePropertiesFile = usePropertiesFile;
    xTable = XPath.newInstance("//dia:object[@type=\"UML - Class\"]");
    xPlaceholder = XPath
        .newInstance("dia:attribute[@name=\"stereotype\"]/dia:string");
    xAttributes = XPath
        .newInstance("dia:attribute[@name=\"attributes\"]/dia:composite[@type=\"umlattribute\"]");
    xOperations = XPath
        .newInstance("dia:attribute[@name=\"operations\"]/dia:composite[@type=\"umloperation\"]");
    xOpParameters = XPath
        .newInstance("dia:attribute[@name=\"parameters\"]/dia:composite[@type=\"umlparameter\"]");

    xAssociation = XPath
        .newInstance("//dia:object[@type=\"UML - Association\"]");
    xRoles = XPath
        .newInstance("dia:attribute[@name=\"ends\"]/dia:composite/dia:attribute[@name=\"role\"]/..");
  }

  /**
   * @throws IOException
   * @throws JDOMException
   */
  public void parse() throws JDOMException, IOException {

    createHandlers();

    // Extract
    final File fileDiaXml = MAGzipUtils.extractTo(MAContextManager
        .getInstance().getSchemaContext().getDiaFile(), MAContextManager
        .getInstance().getSchemaContext().getXmlDiaFile());

    // Dom reading
    SAXBuilder builder = new SAXBuilder();
    Document doc = builder.build(fileDiaXml.toURL());

    List<?> listTables = xTable.selectNodes(doc);
    traiterTables(listTables);

    List<?> listAssociations = xAssociation.selectNodes(doc);
    traiterAssociations(listAssociations);
  }

  private void createHandlers() throws JDOMException {
    Properties tableDescProperties = MAContextManager.getInstance()
        .getDbContext().getTableDescProperties();
    lightTableHandler = new LightTableHandler(
        usePropertiesFile ? tableDescProperties : null);
    tableHandler = new TableHandler(usePropertiesFile ? tableDescProperties
        : null);
    menuHandler = new MenuHandler(usePropertiesFile ? tableDescProperties
        : null);
    associationHandler = new AssociationHandler(
        usePropertiesFile ? tableDescProperties : null);

    listHandlers.add(lightTableHandler);
    listHandlers.add(tableHandler);
    listHandlers.add(menuHandler);
    listHandlers.add(associationHandler);
  }

  private void traiterTables(List<?> listTables) throws JDOMException {
    for (Iterator<?> iter = listTables.iterator(); iter.hasNext();) {
      Element eTable = (Element) iter.next();

      if ("#placeholder#".equals(xPlaceholder.valueOf(eTable))) {
        // System.out.println("Placeholder '" + eTable + "' !");
        continue;
      }

      notifyEvent(XmlEvent.TABLE_START, eTable);

      // Attributes
      List<?> listAttributes = xAttributes.selectNodes(eTable);

      for (Iterator<?> iterator = listAttributes.iterator(); iterator.hasNext();) {
        Element eAttribute = (Element) iterator.next();
        traiterAttribut(eAttribute);
      }

      // Operations
      List<?> listOps = xOperations.selectNodes(eTable);

      for (Iterator<?> iterator = listOps.iterator(); iterator.hasNext();) {
        Element eOperation = (Element) iterator.next();
        traiterOperation(eOperation);
      }

      notifyEvent(XmlEvent.TABLE_END, eTable);

    }
  }

  private void traiterAttribut(Element attribute) {
    notifyEvent(XmlEvent.ATTRIBUTE_START, attribute);
    notifyEvent(XmlEvent.ATTRIBUTE_END, attribute);
  }

  private void traiterOperation(Element operation) throws JDOMException {
    notifyEvent(XmlEvent.OPERATION_START, operation);
    List<?> listOpParams = xOpParameters.selectNodes(operation);

    for (Iterator<?> iterator = listOpParams.iterator(); iterator.hasNext();) {
      Element eParam = (Element) iterator.next();
      notifyEvent(XmlEvent.OPERATION_PARAM_START, eParam);
      notifyEvent(XmlEvent.OPERATION_PARAM_END, eParam);
    }

    notifyEvent(XmlEvent.OPERATION_END, operation);
  }

  public void notifyEvent(XmlEvent xmlEvent, Element element) {
    for (Iterator<DiaXmlHandler> iter = listHandlers.iterator(); iter.hasNext();) {
      DiaXmlHandler diaXmlHandler = iter.next();

      try {
        switch (xmlEvent) {
          case TABLE_START:
            diaXmlHandler.startTable(element);
            break;
          case TABLE_END:
            diaXmlHandler.endTable();
            break;
          case ATTRIBUTE_START:
            diaXmlHandler.startAttribute(element);
            break;
          case ATTRIBUTE_END:
            diaXmlHandler.endAttribute();
            break;
          case OPERATION_START:
            diaXmlHandler.startOperation(element);
            break;
          case OPERATION_END:
            diaXmlHandler.endOperation();
            break;
          case OPERATION_PARAM_START:
            diaXmlHandler.startOpParameter(element);
            break;
          case OPERATION_PARAM_END:
            diaXmlHandler.endOpParameter();
            break;

          case ASSOCIATION_START:
            diaXmlHandler.startAssociation(element);
            break;
          case ASSOCIATION_END:
            diaXmlHandler.endAssociation();
            break;
          case ROLE_START:
            diaXmlHandler.startAssEnd(element);
            break;
          case ROLE_END:
            diaXmlHandler.endAssEnd();
            break;

          default:
            System.err.println("Evenement inconnu !");
            break;
        }
      } catch (JDOMException e) {
        e.printStackTrace();
      }
    }
  }

  public LightTableHandler getLightTableHandler() {
    return lightTableHandler;
  }

  public TableHandler getTableHandler() {
    return tableHandler;
  }

  public MenuHandler getMenuHandler() {
    return menuHandler;
  }

  public void setLightTableHandler(LightTableHandler lightTableHandler) {
    this.lightTableHandler = lightTableHandler;
  }

  private void traiterAssociations(List<?> listAssociations)
      throws JDOMException {
    for (Iterator<?> iter = listAssociations.iterator(); iter.hasNext();) {
      Element eAssociation = (Element) iter.next();
      notifyEvent(XmlEvent.ASSOCIATION_START, eAssociation);

      // Roles
      List<?> listRoles = xRoles.selectNodes(eAssociation);

      for (Iterator<?> iterator = listRoles.iterator(); iterator.hasNext();) {
        Element eRole = (Element) iterator.next();
        notifyEvent(XmlEvent.ROLE_START, eRole);
        notifyEvent(XmlEvent.ROLE_END, eRole);
      }

      notifyEvent(XmlEvent.ASSOCIATION_END, eAssociation);
    }
  }

  /**
   * @return the associationHandler
   */
  public AssociationHandler getAssociationHandler() {
    return associationHandler;
  }
}
