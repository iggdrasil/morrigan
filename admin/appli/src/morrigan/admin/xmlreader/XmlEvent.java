/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : XmlEvent.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.xmlreader;

/**
 * Xml element used for DIA reading.
 * 
 * @author armel
 */
public enum XmlEvent {
  TABLE_START,
  TABLE_END,
  ATTRIBUTE_START,
  ATTRIBUTE_END,
  OPERATION_START,
  OPERATION_END,
  OPERATION_PARAM_START,
  OPERATION_PARAM_END,
  ASSOCIATION_START,
  ASSOCIATION_END,
  ROLE_START,
  ROLE_END;
}
