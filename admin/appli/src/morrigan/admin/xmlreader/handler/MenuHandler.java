/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MenuHandler.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.xmlreader.handler;

import java.util.Properties;

import org.jdom.JDOMException;

/**
 * Menu handler used to stock menu information when reading DIA file.
 * 
 * @author armel
 */
public class MenuHandler extends DefaultXmlHandler {

  public MenuHandler(Properties tableDescProperties) throws JDOMException {
    super(tableDescProperties);
  }
}
