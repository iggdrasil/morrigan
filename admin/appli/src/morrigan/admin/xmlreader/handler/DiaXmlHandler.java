package morrigan.admin.xmlreader.handler;

import org.jdom.Element;
import org.jdom.JDOMException;

/**
 * The XML handler interface for a dia file.
 * 
 * @author Armel
 */
public interface DiaXmlHandler {

  /**
   * Handle the start table element.
   * 
   * @param elementTable the table element
   * @return the table name
   * @throws JDOMException if the parsing fails
   */
  public String startTable(Element elementTable) throws JDOMException;

  /**
   * Handle the end table parsing.
   */
  public void endTable();

  /**
   * Handle the start attribute element.
   * 
   * @param elementAttribute the attribute element
   * @throws JDOMException if the parsing fails
   */
  public void startAttribute(Element elementAttribute) throws JDOMException;

  /**
   * Handle the end attribute parsing.
   */
  public void endAttribute();

  /**
   * Handle the start operation element.
   * 
   * @param elementOperation the attribute element
   * @throws JDOMException if the parsing fails
   */
  public void startOperation(Element elementOperation) throws JDOMException;

  /**
   * Handle the end operation parsing.
   */
  public void endOperation();

  /**
   * Handle the start operation parameter element.
   * 
   * @param elementParameter the operation paremeter element
   * @throws JDOMException if the parsing fails
   */
  public void startOpParameter(Element elementParameter) throws JDOMException;

  /**
   * Handle the end operation parameter parsing.
   */
  public void endOpParameter();

  /**
   * Handle the start association
   * 
   * @param elementAssociation the association element
   * @throws JDOMException if the parsing fails
   */
  public void startAssociation(Element elementAssociation) throws JDOMException;

  /**
   * Handle the end association parsing.
   */
  public void endAssociation();

  /**
   * Handle the start role
   * 
   * @param element the role element
   * @throws JDOMException if the parsing fails
   */
  public void startAssEnd(Element element) throws JDOMException;

  /**
   * Handle the end role parsing.
   */
  public void endAssEnd();
}
