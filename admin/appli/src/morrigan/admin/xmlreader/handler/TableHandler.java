/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : TableHandler.java - CREATION : 2008/06/29
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.xmlreader.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import morrigan.admin.context.MAContextManager;
import morrigan.admin.database.tabledesc.Table;
import morrigan.admin.database.tabledesc.attribute.AttributeBuilder;
import morrigan.admin.database.tabledesc.operation.OperationBuilder;
import morrigan.admin.database.tabledesc.table.DefaultTable;
import morrigan.labeleditor.PropertySuffix;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

/**
 * Table handler used to stock SQL table information when reading DIA file.
 * 
 * @author armel
 */
public class TableHandler extends DefaultXmlHandler {

  private final XPath xName;

  private final XPath xType;

  private final XPath xValue;

  private final XPath xComment;

  private final Map<String, Table> tables = new HashMap<String, Table>();

  private DefaultTable currentTable = null;

  private OperationBuilder currentOperationBuilder = null;

  public TableHandler(Properties tableDescProperties) throws JDOMException {
    super(tableDescProperties);

    xName = XPath.newInstance("dia:attribute[@name=\"name\"]/dia:string");
    xType = XPath.newInstance("dia:attribute[@name=\"type\"]/dia:string");
    xValue = XPath.newInstance("dia:attribute[@name=\"value\"]/dia:string");
    xComment = XPath.newInstance("dia:attribute[@name=\"comment\"]/dia:string");
  }

  @Override
  public String startTable(Element elementTable) throws JDOMException {
    String nomTable = super.startTable(elementTable);
    // String defaultLabel = sub(xComment.valueOf(elementTable));
    String defaultLabel = "";
    // if (defaultLabel == null || defaultLabel.trim().length() == 0) {
    // defaultLabel = (nomTable.substring(0, 1).toUpperCase() + nomTable
    // .substring(1)).replaceAll("_", " ");
    // }

    currentTable = new DefaultTable(nomTable, getTextFromTableDescProp(nomTable
        + PropertySuffix.DOT_LABEL, defaultLabel));

    return nomTable;
  }

  @Override
  public void endTable() {
    tables.put(currentTable.getName(), currentTable);
    MAContextManager.getInstance().getWindowContext().addFenetre(
        currentTable.getName());
    currentTable = null;
  }

  @Override
  public void startAttribute(Element elementAttribute) throws JDOMException {
    String nom = sub(xName.valueOf(elementAttribute));
    String type = sub(xType.valueOf(elementAttribute));
    String defaultValue = sub(xValue.valueOf(elementAttribute));
    String defaultComment = sub(xComment.valueOf(elementAttribute));
    boolean isEnabled = Boolean.parseBoolean(getTextFromTableDescProp(nom
        + PropertySuffix.DOT_ENABLED, Boolean.TRUE.toString()));

    Integer size = null;
    try {
      String stringSize = type.substring(type.indexOf('(') + 1,
          type.length() - 1);
      size = Integer.parseInt(stringSize);
    } catch (Exception e) {
      // Do nothing
    }

    currentTable
        .addAttribute(AttributeBuilder.buildAttribute(nom,
            getTextFromTableDescProp(nom + PropertySuffix.DOT_LABEL, nom),
            type, defaultValue, getTextFromTableDescProp(nom
                + PropertySuffix.DOT_COMMENT, defaultComment), size, isEnabled));
  }

  @Override
  public void startOperation(Element elementOperation) throws JDOMException {
    String nom = sub(xName.valueOf(elementOperation));
    String type = sub(xType.valueOf(elementOperation));
    String comment = sub(xComment.valueOf(elementOperation));

    currentOperationBuilder = new OperationBuilder();
    currentOperationBuilder.newOperation(nom, type, comment);
  }

  @Override
  public void endOperation() {
    currentTable.addOperation(currentOperationBuilder.getBuiltOperation());

  }

  @Override
  public void startOpParameter(Element elementParameter) throws JDOMException {
    String nom = sub(xName.valueOf(elementParameter));
    currentOperationBuilder.addParam(nom);
  }

  public Map<String, Table> getTables() {
    return tables;
  }
}
