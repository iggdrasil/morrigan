package morrigan.admin.xmlreader.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import morrigan.admin.database.MALightTable;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

public class LightTableHandler extends DefaultXmlHandler {

  private final XPath xName;

  private Map<String, MALightTable> tables = new HashMap<String, MALightTable>();

  private MALightTable currentTable = null;

  private String nomTable = null;

  public LightTableHandler(Properties tableDescProperties) throws JDOMException {
    super(tableDescProperties);
    xName = XPath.newInstance("dia:attribute[@name=\"name\"]/dia:string");
  }

  @Override
  public String startTable(Element elementTable) throws JDOMException {
    nomTable = super.startTable(elementTable);
    // System.out.println("TABLE : " + nomTable);

    return nomTable;
  }

  @Override
  public void endTable() {
    currentTable = null;
    nomTable = null;
  }

  @Override
  public void startAttribute(Element elementAttribute) throws JDOMException {
    String nomChamp = xName.valueOf(elementAttribute);
    nomChamp = nomChamp.substring(1, nomChamp.length() - 1);

    // System.out.println(" - " + nomChamp);

    if (currentTable == null) {
      String trigramme = nomChamp.substring(0, 3);

      currentTable = new MALightTable(nomTable, trigramme);
      tables.put(nomTable, currentTable);
    }
  }

  public Map<String, MALightTable> getTables() {
    return tables;
  }
}
