package morrigan.admin.xmlreader.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import morrigan.admin.database.tabledesc.association.ForeignKeyAssociation;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

public class AssociationHandler extends DefaultXmlHandler {

  private final XPath xName;

  private final XPath xRole;

  private final Map<String, String> foreignToPrimaryKeysLinks = new HashMap<String, String>();
  
  private final Map<String, Set<String>> primaryToForeignKeysLinks = new HashMap<String, Set<String>>();

  private ForeignKeyAssociation currentAssociation = null;

  public AssociationHandler(Properties tableDescProperties)
      throws JDOMException {
    super(tableDescProperties);

    xName = XPath.newInstance("dia:attribute[@name=\"name\"]/dia:string");
    xRole = XPath.newInstance("dia:attribute[@name=\"role\"]/dia:string");
  }

  @Override
  public String startTable(Element elementTable) throws JDOMException {
    String tablename = super.startTable(elementTable);
    // currentAssociation.setTableName(tablename);
    return tablename;
  }

  @Override
  public void startAssociation(Element elementAssociation) throws JDOMException {
    String name = sub(xName.valueOf(elementAssociation));
    currentAssociation = new ForeignKeyAssociation(name);
  }

  @Override
  public void endAssociation() {
    String primaryKeyName = currentAssociation.getPrimaryKeyName();
    String foreignKeyName = currentAssociation.getForeignKeyName();
    
    // frm_prs_id => prs_id
    foreignToPrimaryKeysLinks.put(foreignKeyName, primaryKeyName);
    
    // prs_id => frm_prs_id
    Set<String> set = primaryToForeignKeysLinks.get(primaryKeyName);
    if (set == null) {
      set = new HashSet<String>();
      primaryToForeignKeysLinks.put(primaryKeyName, set);
    }
    set.add(foreignKeyName);
  }

  @Override
  public void startAssEnd(Element element) throws JDOMException {
    String role = sub(xRole.valueOf(element));
    if (currentAssociation.getFirstEndName() == null) {
      currentAssociation.setFirstEndName(role);
    } else {
      currentAssociation.setSecondEndName(role);
    }
  }

  /**
   * Returns the of primary keys needed to foreign keys.
   * @return the foreignPrimaryKeysLinks
   */
  public Map<String, String> getForeignToPrimaryKeysLinks() {
    return foreignToPrimaryKeysLinks;
  }
  
  /**
   * Returns primary keys are used.
   * @return the links between primary keys and foreign keys
   */
  public Map<String, Set<String>> getPrimaryToForeignKeysLinks() {
    return primaryToForeignKeysLinks;
  }
}
