package morrigan.admin.xmlreader.handler;

import java.util.Properties;

import morrigan.admin.util.MAUtil;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

/**
 * The default implementation of the DIA XML handler.
 * 
 * @author Armel
 */
public abstract class DefaultXmlHandler implements DiaXmlHandler {

  private final XPath xName;

  private final Properties tableDescProperties;

  /**
   * Construct the DefaultXmlHandler with a properties set. This set contains
   * the overriding labels, comments and others.
   * 
   * @param tableDescProperties labels, comments & co overriding the data of the
   *          dia file.
   * @throws JDOMException
   */
  public DefaultXmlHandler(Properties tableDescProperties) throws JDOMException {
    super();
    this.tableDescProperties = tableDescProperties;
    xName = XPath.newInstance("dia:attribute[@name=\"name\"]/dia:string");
  }

  public String startTable(Element elementTable) throws JDOMException {
    String nomTable = xName.valueOf(elementTable);
    nomTable = nomTable.substring(1, nomTable.length() - 1);
    return nomTable;
  }

  public void startAttribute(Element elementAttribute) throws JDOMException {
  // Do nothing
  }

  public void startOperation(Element elementOperation) throws JDOMException {
  // Do nothing
  }

  public void endTable() {
  // Do nothing
  }

  public void endAttribute() {
  // Do nothing
  }

  public void endOperation() {
  // Do nothing
  }

  /**
   * Handle the start operation parameter element, such as an index or grants.
   */
  public void startOpParameter(Element elementParameter) throws JDOMException {
  // Do nothing
  }

  public void endOpParameter() {
  // Do nothing
  }

  public void startAssociation(Element elementAssociation) throws JDOMException {
  // Do nothing
  }

  public void endAssociation() {
  // Do nothing
  }

  public void startAssEnd(Element element) throws JDOMException {
  // Do nothing
  }

  public void endAssEnd() {
  // Do nothing
  }

  /**
   * Extract the real name from the name
   * 
   * @param textWithDiese
   * @return
   */
  protected String sub(String textWithDiese) {
    return textWithDiese.substring(1, textWithDiese.length() - 1).trim();
  }

  /**
   * Look in the properties set if the text is overriden.<point2dPattern> The
   * <code>key</code> is the concatenation of the name field in the database
   * (ex: prs_nom_originel), the '.' character and the type of the text (ex:
   * comment, label). Ex : "prs_nom_originel.comment"
   * 
   * @param key a string made by the concatenation of field name + "." + the
   *          type (Ex: "prs_nom_originel.label")
   * @param defaultText the default text if no text is retrieved from the
   *          properties set.
   * @return the text corresponding to the key in the properties set if it
   *         exists; the default text otherwise.
   */
  protected String getTextFromTableDescProp(String key, String defaultText) {
    MAUtil.notNull(key, "key");

    if (tableDescProperties == null) {
      return defaultText;
    }
    return tableDescProperties.getProperty(key, defaultText);
  }
}
