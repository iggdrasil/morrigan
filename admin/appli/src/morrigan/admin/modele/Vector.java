package morrigan.admin.modele;

public class Vector {

  /** Origin */
  private Point origin;

  /** Direction */
  private Point direction;

  /**
   * Constructor
   * 
   * @param origin
   * @param direction
   */
  public Vector(Point origin, Point direction) {
    this.origin = origin;
    this.direction = direction;
  }

  /**
   * @return the origin
   */
  public Point getOrigin() {
    return this.origin;
  }

  /**
   * @return the direction
   */
  public Point getDirection() {
    return this.direction;
  }

}
