package morrigan.admin.modele;

import javax.swing.WindowConstants;

import morrigan.admin.exception.CreateWindowException;
import morrigan.admin.service.MondeService;
import morrigan.admin.service.ServiceManager;
import morrigan.admin.specif.MAFenDetailMonde;

public class Monde {

  private Region[] regions;

  private Cartographie cartographie;

  public static void main(String[] args) throws CreateWindowException {

    MondeService service = (MondeService) ServiceManager
        .getService(MondeService.class);
    Cartographie carto = service.getCartographie(0);

    MAFenDetailMonde fenDetailMonde = new MAFenDetailMonde(carto);
    fenDetailMonde.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    fenDetailMonde.setVisible(true);
  }
}
