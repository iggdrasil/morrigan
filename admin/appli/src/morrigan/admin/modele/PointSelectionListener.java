/*
 * PointSelectionListener.java 9 janv. 08 This file is a part of the Equity
 * Product Manager (EProM) Copyright (C) 2002-2007 SOCIETE GENERALE All rights
 * reserved.
 */
package morrigan.admin.modele;

/**
 * @author Armel
 */
public interface PointSelectionListener {

  public void pointSelectionChanged(SelectionEvent selectionEvent);

}
