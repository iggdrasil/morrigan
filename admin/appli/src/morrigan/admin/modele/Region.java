/*
 * #######################################################################
 * # PROJECT : Morrigan - FILE : MRegion.java - CREATION : 2008/01/08
 * #
 * # Copyright (C) 2005  Armel Lagadic <siltaom_AT_gmail.com>
 * #
 * #    This program can be distributed under the terms of the GNU GPL.
 * #    See the file COPYING.
 * #######################################################################
 */
package morrigan.admin.modele;

import java.awt.Color;
import java.awt.Polygon;

public class Region {

  private Polygon pSurface;

  private String sNom;

  private Color cSurface;

  public Region(Polygon pSurface, String sNom, Color cSurface) {
    this.pSurface = pSurface;
    this.sNom = sNom;
    this.cSurface = cSurface;
  }

  public Polygon getSurface() {
    return pSurface;
  }

  public Color getCouleur() {
    return cSurface;
  }

  public String getNom() {
    return sNom;
  }
}