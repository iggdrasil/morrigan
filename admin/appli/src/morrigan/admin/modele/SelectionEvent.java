package morrigan.admin.modele;

import java.awt.Rectangle;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * @author Armel
 */
public class SelectionEvent {

  private final Map<Point, Rectangle> selectedCells;

  /**
   * @param selectedCells
   */
  public SelectionEvent(Map<Point, Rectangle> selectedCells) {
    this.selectedCells = selectedCells;
    selectedCells.keySet();
  }

  /**
   * @return the points
   */
  public Set<Point> getPoints() {
    return Collections.unmodifiableSet(selectedCells.keySet());
  }

  /**
   * @return the selectedCells
   */
  public Map<Point, Rectangle> getSelectedCells() {
    return Collections.unmodifiableMap(this.selectedCells);
  }
}
