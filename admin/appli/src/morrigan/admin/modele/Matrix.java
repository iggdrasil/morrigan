package morrigan.admin.modele;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

public class Matrix<E> {

  private E[][] points;

  private int lineSize = -1;

  private List<PointSelectionListener> selectionListeners = new ArrayList<PointSelectionListener>();

  /**
   * Default constructor.
   */
  public Matrix(E[][] points) {
    super();
    Validate.notNull(points, "The matrix must contain data");
    this.points = points;

    for (E[] point : points) {
      Validate.notNull(point, "The matrix line must contain data");
      if (lineSize < 0) {
        lineSize = point.length;
      } else {
        Validate.isTrue(lineSize == point.length,
            "The matrix lines must have the same size");
      }
    }
  }

  public Matrix() {
    super();
  }

  public E getPoint(int x, int y) {
    return points[x][y];
  }

  public int getNumberOfLines() {
    return points.length;
  }

  public int getLineSize() {
    return lineSize;
  }

  public void addPointSelectionListener(
      PointSelectionListener pointSelectionListener) {
    selectionListeners.add(pointSelectionListener);
  }
}
