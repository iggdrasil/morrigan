package morrigan.admin.modele;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang.Validate;

public class Cartographie {

  private SortedMap<String, Matrix<?>> matrixes;

  /**
   * Default constructor.
   */
  public Cartographie() {
    matrixes = new TreeMap<String, Matrix<?>>();
  }

  /**
   * Constructor with a list of information.
   */
  public Cartographie(SortedMap<String, Matrix<?>> matrixes) {
    this.matrixes = matrixes;
  }

  public Object getInfo(String informationName, int x, int y) {
    Matrix<?> matrix = matrixes.get(informationName);
    Validate.notNull(matrix, "No entry with name '" + informationName
        + "' could be found in the matrixes map");
    return matrix.getPoint(x, y);
  }

  public void putMatrix(String informationName, Matrix<?> matrix) {
    this.matrixes.put(informationName, matrix);
  }

  public Matrix<?> getMatrix(String informationName) {
    return matrixes.get(informationName);
  }

  public Set<Entry<String, Matrix<?>>> matrixSet() {
    return matrixes.entrySet();
  }

  public int getMatrixNumber() {
    return matrixes.size();
  }
}
