package morrigan.admin.modele;

public class Point {

  /** x */
  private final int x;

  /** y */
  private final int y;

  /**
   * Constructor
   * 
   * @param x
   * @param y
   */
  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * @return the x
   */
  public int getX() {
    return this.x;
  }

  /**
   * @return the y
   */
  public int getY() {
    return this.y;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null) {
      return false;
    } else if (obj instanceof Point) {
      Point o = (Point) obj;
      return (this.x == o.x && this.y == o.y);
    } else {
      return false;
    }
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return x << 13 + y;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "Point x=" + x + ", y=" + y;
  }
}
