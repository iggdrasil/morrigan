<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_traiteouvertures.php                                                                                
* Fonction du fichier : page de traitement des changements des statuts de chaque race, classe
* ou sexe (ouvert ou fermée à l'inscription)
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                          
* Date de création :  02/09/2008                                                                              
* Version actuelle :  1.0 au 02/09/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");

include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_st_iterationrace = array ("iterationrace", "", HIDDEN);
$g_st_iterationclasse = array ("iterationclasse", "", HIDDEN);
$g_st_iterationsexe = array ("iterationsexe", "", HIDDEN);
$g_st_idrace = array ("idrace", "", HIDDEN);
$g_st_idclasse = array ("idclasse", "", HIDDEN);
$g_st_idsexe = array ("idsexe", "", HIDDEN);
$g_st_actionrace = array ("actionrace", "", NORMAL);
$g_st_actionclasse = array ("actionclasse", "", NORMAL);
$g_st_actionsexe = array ("actionsexe", "", NORMAL);

$l_i_logok = NON; // user logué? droits ok?
$l_i_old_error_handler = NON;
$l_t_i_existe = array (0, array ());
            


/* Récupération des variables passées en formulaire et Querystring*/
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login admin, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}


/* Récupération des variables de session */

/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handleradminouvertures_niv1 = $l_o_template -> Open ("local/tpl/tg_traiteadminouvertures_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

if ($l_i_logok == NON)
{
       include ("admin/admin_niv1_login.inc");
}
else
{
    if ( $l_i_logok == NON)
    {
        trigger_error ("Erreur lors de la vérification des droits de l'admin ".$_SESSION ["user"], FATAL);
    }
    if ($_SESSION ["droitrecrutement"] == OUI)
    {
    	// Récupérer et contrôler infos à traiter (formulaire)
    	recup_variable ($g_st_iterationrace, "integer");
        $l_i_erreur = controle_variable (&$g_st_iterationrace, "radio");
        if ($l_i_erreur == OUI || $g_st_iterationrace [CONTENU_VAR] == "")
        {
            $g_st_iterationrace [CONTENU_VAR] = 0;
        }
        
        recup_variable ($g_st_iterationclasse, "integer");
        $l_i_erreur = controle_variable (&$g_st_iterationclasse, "radio");
        if ($l_i_erreur == OUI || $g_st_iterationclasse [CONTENU_VAR] == "")
        {
            $g_st_iterationclasse [CONTENU_VAR] = 0;
        }
 
        recup_variable ($g_st_iterationsexe, "integer");
        $l_i_erreur = controle_variable (&$g_st_iterationsexe, "radio");
        if ($l_i_erreur == OUI || $g_st_iterationsexe [CONTENU_VAR] == "")
        {
            $g_st_iterationsexe [CONTENU_VAR] = 0;
        }
        
        
        if ($g_st_iterationrace [CONTENU_VAR] != 0)
    	{
			recup_variable ($g_st_actionrace, "integer");
			$l_i_erreur = controle_variable (&$g_st_actionrace, "radio");
			if ($l_i_erreur == OUI)
			{
				trigger_error ("Action de traitement de l'ouverture~fermeture de la race inconnue", FATAL);
				exit ();
			}
			
			recup_variable ($g_st_idrace, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idrace, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("Aucun id de race transmis pour son ouverture~fermeture", FATAL);
                exit ();
            }

            $l_t_i_existe = requete ("existerace", $g_st_idrace [CONTENU_VAR]);
            if ($l_t_i_existe [0] == 0)
            {
                trigger_error ("Pas de race correspondant à l'id de race demandé : ".$g_st_idrace [CONTENU_VAR].".", FATAL);
                exit ();
            } 
		}
		
		if ($g_st_iterationclasse [CONTENU_VAR] != 0)
		{
			recup_variable ($g_st_actionclasse, "integer");
			$l_i_erreur = controle_variable (&$g_st_actionclasse, "radio");
			if ($l_i_erreur == OUI)
			{
				trigger_error ("Action de traitement de l'ouverture~fermeture de la classe inconnue", FATAL);
				exit ();
			}  			
			recup_variable ($g_st_idclasse, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idclasse, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("Aucun id de classe transmis pour son ouverture~fermeture", FATAL);
                exit ();
            }

            $l_t_i_existe = requete ("existeclasse", $g_st_idclasse [CONTENU_VAR]);
            if ($l_t_i_existe [0] == 0)
            {
                trigger_error ("Pas de classe correspondant à l'id de classe demandé : ".$g_st_idclasse [CONTENU_VAR].".", FATAL);
                exit ();
            } 
		}
    
		if ($g_st_iterationsexe [CONTENU_VAR] != 0)
		{
			recup_variable ($g_st_actionsexe, "integer");
			$l_i_erreur = controle_variable (&$g_st_actionsexe, "radio");
			if ($l_i_erreur == OUI)
			{
				trigger_error ("Action de traitement de l'ouverture~fermeture du sexe inconnue", FATAL);
				exit ();
			}  			

			recup_variable ($g_st_idsexe, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idsexe, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("Aucun id de sexe transmis pour son ouverture~fermeture", FATAL);
                exit ();
            }

            $l_t_i_existe = requete ("existesexe", $g_st_idsexe [CONTENU_VAR]);
            if ($l_t_i_existe [0] == 0)
            {
                trigger_error ("Pas de sexe correspondant à l'id de sexe demandé : ".$g_st_idsexe [CONTENU_VAR].".", FATAL);
                exit ();
            } 
		}
		    
    	include ("admin/admin_niv1_traiteouvertures.inc");
    }
    else
	{
        include ("admin/admin_niv1_droitrefuse.inc");
	}
}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration - Ouverture et fermeture à l'inscription des races, classes et sexes");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
