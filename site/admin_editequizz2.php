<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_editequizz.php                                                                                 
* Fonction du fichier : page d'édition des questions et réponses du quizz 
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  03/08/2008                                                                              
* Version actuelle :  1.0 au 03/08/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");

include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD

$g_st_idquestion = array ("idquestion", "", HIDDEN);

$g_st_question = array ("question", "", NORMAL);

// Arbitrairement et limité en dur : max 5 réponses possibles à une question. Mais il n'y a pas de limite en base qui impose cela
$g_st_reponse1 = array("reponse1", "",NORMAL);
$g_st_reponse2 = array("reponse2", "",NORMAL);
$g_st_reponse3 = array("reponse3", "",NORMAL);
$g_st_reponse4 = array("reponse4", "",NORMAL);
$g_st_reponse5 = array("reponse5", "",NORMAL);

$g_st_idreponse1 = array ("idreponse1", "", HIDDEN);
$g_st_idreponse2 = array ("idreponse2", "", HIDDEN);
$g_st_idreponse3 = array ("idreponse3", "", HIDDEN);
$g_st_idreponse4 = array ("idreponse4", "", HIDDEN);
$g_st_idreponse5 = array ("idreponse5", "", HIDDEN);

$g_st_reponsebonne1 = array ("reponsebonne1", "",NORMAL);
$g_st_reponsebonne2 = array ("reponsebonne2", "",NORMAL);
$g_st_reponsebonne3 = array ("reponsebonne3", "",NORMAL);
$g_st_reponsebonne4 = array ("reponsebonne4", "",NORMAL);
$g_st_reponsebonne5 = array ("reponsebonne5", "",NORMAL);

$g_st_actionquizz = array ("actionquizz", "", HIDDEN);

$g_st_iteration = array ("iteration", "", HIDDEN);
$g_i_erreur = NON;

$g_i_numeroquestion = -1;

// VARIABLES LOCALES
$l_i_logok = NON; // user logué? droits ok?
$l_i_old_error_handler = NON;
$l_t_i_existequestion = NON;

/* Récupération des variables passées en formulaire et Querystring*/
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login admin, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}


/* Récupération des variables de session */



/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handleradminquizz_niv1 = $l_o_template -> Open ("local/tpl/tg_admineditequizz2_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

print($l_i_logok);

if ($l_i_logok == NON)
{
       include ("admin/admin_niv1_login.inc");
}
else
{
    if ( $l_i_logok == NON)
    {
        trigger_error ("Erreur lors de la vérification des droits de l'admin ".$_SESSION ["user"], FATAL);
    }
    
    if ($_SESSION ["droitrecrutement"] == OUI)
    {
    	// Récupérer et contrôler infos à traiter (formulaire)
        recup_variable ($g_st_iteration, "integer");
        $l_i_erreur = controle_variable (&$g_st_iteration, "radio");
        if ($l_i_erreur == OUI || $g_st_iteration [CONTENU_VAR] == "")
        {
			trigger_error ("Erreur d'itération dans le traitement de l'édition du quizz",FATAL);
			exit();
        }
        
        if ($g_st_iteration [CONTENU_VAR] == 1)
        {
			recup_variable ($g_st_idquestion, "integer");
            $g_i_numeroquestion = $g_st_idquestion [CONTENU_VAR];
            
            $l_i_erreur = controle_variable (&$g_st_idquestion, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("La question n'existe pas.", FATAL);
                exit ();
            }



             if ($g_st_idquestion [CONTENU_VAR] != 0)
            {
            	$l_t_i_existequestion = requete ("existe_question", $g_st_idquestion [CONTENU_VAR]);
            	if ($l_t_i_existequestion [0] == 0)
            	{
             	   trigger_error ("Pas de question correspondant à l'id de question demandé : ".$g_st_idquestion [CONTENU_VAR].".", FATAL);
            	    exit ();
            	} 
    
            	$g_i_numeroquestion = $g_st_idquestion [CONTENU_VAR];
           }
           
            recup_variable ($g_st_actionquizz, "integer");
            $l_i_erreur = controle_variable ($g_st_actionquizz, "select", 1, 1);   
            
            if ($l_i_erreur == OUI)
            {
            	trigger_error ("L'action d'édition du quizz demandée n'est pas reconnue.", FATAL);
            	exit ();	
			} 
		}
        
        if ($g_st_iteration [CONTENU_VAR] >= 2)
        {
			recup_variable ($g_st_actionquizz, "integer");
            $l_i_erreur = controle_variable ($g_st_actionquizz, "select", 1, 1);   
            
            if ($l_i_erreur == OUI)
            {
            	trigger_error ("L'action d'édition du quizz demandée n'est pas reconnue.", FATAL);
            	exit ();	
			} 
			
			recup_variable ($g_st_idquestion, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idquestion, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("La question n'existe pas.", FATAL);
                exit ();
            }
            $g_i_numeroquestion = $g_st_idquestion [CONTENU_VAR];

             if ($g_st_idquestion [CONTENU_VAR] != 0)
            {
            	$l_t_i_existequestion = requete ("existe_question", $g_st_idquestion [CONTENU_VAR]);
            	if ($l_t_i_existequestion [0] == 0)
            	{
            	    trigger_error ("Pas de question correspondant à l'id de question demandé : ".$g_st_idquestion [CONTENU_VAR].".", FATAL);
            	    exit ();
            	} 

			}

        	recup_variable ($g_st_question, "text");

        	$l_i_erreur = controle_variable (&$g_st_question, array ("text", OUI, 255, $g_st_idquestion [CONTENU_VAR]));
			if ($l_i_erreur == OUI &&  $g_st_question [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			
			recup_variable ($g_st_idreponse1, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idreponse1, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI &&  $g_st_idreponse1 [CONTENU_VAR] != "")
            {
                trigger_error ("La réponse n'existe pas.", FATAL);
                exit ();
            }			

			recup_variable ($g_st_idreponse2, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idreponse2, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI &&  $g_st_idreponse2 [CONTENU_VAR] != "")
            {
                trigger_error ("La réponse n'existe pas.", FATAL);
                exit ();
            }			
            
            recup_variable ($g_st_idreponse3, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idreponse3, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI &&  $g_st_idreponse3 [CONTENU_VAR] != "")
            {
                trigger_error ("La réponse n'existe pas.", FATAL);
                exit ();
            }			
            
            recup_variable ($g_st_idreponse4, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idreponse4, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI &&  $g_st_idreponse4 [CONTENU_VAR] != "")
            {
                trigger_error ("La réponse n'existe pas.", FATAL);
                exit ();
            }
            
            recup_variable ($g_st_idreponse5, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idreponse5, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI &&  $g_st_idreponse5 [CONTENU_VAR] != "")
            {
                trigger_error ("La réponse n'existe pas.", FATAL);
                exit ();
            }						

			recup_variable ($g_st_reponse1, "text");
			$l_i_erreur	= controle_variable (&$g_st_reponse1, array ("text", NON, 255));		
			if ($l_i_erreur == OUI && $g_st_reponse1 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else
			{
				// Champ facultatif
				$g_st_reponse1 [TYPE_DISPLAY] = NORMAL;
			}		

			recup_variable ($g_st_reponse2, "text");
			$l_i_erreur	= controle_variable (&$g_st_reponse2, array ("text", NON, 255));		
			if ($l_i_erreur == OUI && $g_st_reponse2 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else
			{
				// Champ facultatif
				$g_st_reponse2 [TYPE_DISPLAY] = NORMAL;
			}		
			
			recup_variable ($g_st_reponse3, "text");
			$l_i_erreur	= controle_variable (&$g_st_reponse3, array ("text", NON, 255));		
			if ($l_i_erreur == OUI && $g_st_reponse3 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else
			{
				// Champ facultatif
				$g_st_reponse3 [TYPE_DISPLAY] = NORMAL;
			}			
			
			recup_variable ($g_st_reponse4, "text");
			$l_i_erreur	= controle_variable (&$g_st_reponse4, array ("text", NON, 255));		
			if ($l_i_erreur == OUI && $g_st_reponse4 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else
			{
				// Champ facultatif
				$g_st_reponse4 [TYPE_DISPLAY] = NORMAL;
			}			
			
			recup_variable ($g_st_reponse5, "text");
			$l_i_erreur	= controle_variable (&$g_st_reponse5, array ("text", NON, 255));		
			if ($l_i_erreur == OUI && $g_st_reponse5 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else
			{
				// Champ facultatif
				$g_st_reponse5 [TYPE_DISPLAY] = NORMAL;
			}					
			recup_variable ($g_st_reponsebonne1, "array");
			$l_i_erreur	= controle_variable (&$g_st_reponsebonne1, array ("select", 0, 1)); 
			if ($l_i_erreur == OUI && $g_st_reponsebonne1 [CONTENU_VAR][0]!= "")
			{
				$g_i_erreur = OUI;
			}
			else 
			{
				$g_st_reponsebonne1 [TYPE_DISPLAY] = NORMAL;
			}
			
			recup_variable ($g_st_reponsebonne2, "array");
			$l_i_erreur	= controle_variable (&$g_st_reponsebonne2, array ("select", 0, 1)); 
			if ($l_i_erreur == OUI  && $g_st_reponsebonne2 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else 
			{
				$g_st_reponsebonne2 [TYPE_DISPLAY] = NORMAL;
			}
			
			recup_variable ($g_st_reponsebonne3, "array");
			$l_i_erreur	= controle_variable (&$g_st_reponsebonne3, array ("select", 0, 1));
			if ($l_i_erreur == OUI  && $g_st_reponsebonne3 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else 
			{
				$g_st_reponsebonne3 [TYPE_DISPLAY] = NORMAL;
			}
		
			recup_variable ($g_st_reponsebonne4, "array");
			$l_i_erreur	= controle_variable (&$g_st_reponsebonne4, array ("select", 0, 1));
			if ($l_i_erreur == OUI  && $g_st_reponsebonne4 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else 
			{
				$g_st_reponsebonne4 [TYPE_DISPLAY] = NORMAL;
			}			

			recup_variable ($g_st_reponsebonne5, "array");
			$l_i_erreur	= controle_variable (&$g_st_reponsebonne5, array ("select", 0, 1)); 
			if ($l_i_erreur == OUI  && $g_st_reponsebonne5 [CONTENU_VAR] != "")
			{
				$g_i_erreur = OUI;
			}
			else 
			{
				$g_st_reponsebonne5 [TYPE_DISPLAY] = NORMAL;
			}
		}
    	
    	include ("admin/admin_niv1_editionquizz.inc");
	}
	else
	{
        include ("admin/admin_niv1_droitrefuse.inc");
	}

}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration - Edition du Quizz");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");


/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>


