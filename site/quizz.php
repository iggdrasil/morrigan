<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : quizz.php                                                                                                           
* Fonction du fichier : page permettant de répondre au quizz de motivation                                         
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                      
* Date de création : 11/03/2008                                                                                                    
* Version actuelle : 1.0 au 11/03/2008                                                               
* License du projet : GPL                                                                                            
* Dernières modifications :                                                                                   
* Remarques particulières :   /!\ TODO : virer les print de debug                  
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/traite_quizz.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_i_debug == 0;

$g_st_idquestions = array ("idquestions", "", HIDDEN);
$g_st_idreponses = array ("idreponses", "", NORMAL);
$g_st_iterationquizz = array ("iterationquizz", "", HIDDEN);
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_st_idprofil = array ("idprofil", "", HIDDEN);
$g_i_erreur == NON;

// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = NON;
$l_i_importedroitsok = NON;
$l_i_profil = "";
$l_i_erreur = NON;
$l_s_referer = "quizz";

/*
$l_i_loginerr = 0;
*/

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;


/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerquizz_niv1 = $l_o_template -> Open ("local/tpl/tg_quizz_niv1.tpl");
$l_i_handlerloginusr_niv1 = $l_o_template -> Open ("local/tpl/tg_loginusr_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");


/* Récupération des variables passées en formulaire et Querystring */
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}

if (isset ($_GET ['idprofil'])) // récupère l'id du profil pour lequel le candidat veut déposer sa candidature
{
    $l_i_profil = $_GET ['idprofil'];
    settype ($l_i_profil, "integer");
}
elseif ((!(isset ($_GET ['idprofil']))) || (QUIZZ == NON) || ($_SESSION ['mj'] == OUI))
{
    // Le profil est non défini, retour au choix d'une offre
    header ("Location:http://".HOST."/insc_listeannonces.php");
}

print ("Id profil :".$l_i_profil."<br>");

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();
if ($l_i_logok == OUI)
{
	$l_i_verifdroitsok = verif_droits ();
}
//print($l_i_logok);


//Menu de gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

if ($l_i_logok == NON)
{
    // Utilisateur non logué => Possibilité de se loguer
    include ("inscription/insc_login_niv1.inc");
}
else 
{
	// Récupération des variables passées en formulaire
	recup_variable ($g_st_iterationquizz, "integer");
	$l_i_erreur = controle_variable ($g_st_iterationquizz, array ("text", 0, 11)); // variables en dur pas top
	print ("Erreur iteration : ".$g_st_iterationquizz [CONTENU_VAR]."<br>");
	if ($l_i_erreur == OUI)
	{
		// itération non définie : on suppose qu'on est au début du Quizz
		$g_st_iterationquizz [CONTENU_VAR] = 1;
		$g_st_iterationquizz [TYPE_DISPLAY] = HIDDEN;
	}

	if ($g_st_iterationquizz [CONTENU_VAR] == 2)
	{
		recup_variable ($g_st_idquestions, "array");
		print ("Récupération des variables idquestions<br>");
        if (controle_variable (&$g_st_idquestions, array ("select", NOMBREQUESTIONS, NOMBREQUESTIONS)) == OUI) $g_i_erreur = OUI;
   		//print ("Dis moi dis moi s'il y a une erreur ici  race! ".$g_i_erreur."<br>");    
 
		recup_variable ($g_st_idreponses, "array");
		print ("Récupération des variables idreponses<br>");
		// on vérifie le nombre max de réponses possibles 
        if (controle_variable (&$g_st_idreponses, array ("select", 0, NOMBREQUESTIONS*NOMBREREPONSES)) == OUI) $g_i_erreur = OUI;
	}
	elseif ($g_st_iterationquizz [CONTENU_VAR] > 2)
	{
		trigger_error ("Tentative de triche sur le quizz par '".$_SESSION ['id']."'", FATAL);	
		exit ();
	}	

	include ("inscription/quizz_niv1.inc");
}

// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Quizz");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");


/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
