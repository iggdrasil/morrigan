<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : enregistrement.php                                                                                                           
* Fonction du fichier : page d'enregistrement des visiteurs                                          
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                         
* Date de création : 03/02/2008                                                                                                    
* Version actuelle : 1.0 au 03/02/2008                                                               
* License du projet : GPL                                                                                            
* Dernières modifications :                                                                                   
* Remarques particulières :   /!\ TODO : virer les print de debug                                                                              
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_name ("standard");
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/verif_mail.inc");
include ("lib/formulaire.inc");
include ("lib/mdp.inc");
include ("Mail.php");
include ("lib/courriel.inc");


/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_st_pseudo = array ("pseudo", "", NORMAL);
$g_st_courriel = array ("courriel", "", NORMAL);
$g_st_commentaire = array ("commentaire", "", NORMAL);
$g_st_iteration = array ("iteration", 1, HIDDEN);


// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = 0;
$l_i_importedroitsok = 0;
$l_i_erreur = NON;
/*
$l_i_loginerr = 0;
*/

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;



/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}
/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerenregistrement_niv1 = $l_o_template -> Open ("local/tpl/tg_enregistrement_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");


/* Récupération des variables passées en formulaire et Querystring */
//if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login admin, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
/*
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}
*/
recup_variable ($g_st_iteration, "integer");
if (controle_variable ($g_st_iteration, "radio"))  $l_i_erreur = OUI;

if ($g_st_iteration [CONTENU_VAR] > 1)
{
	recup_variable ($g_st_pseudo, "text");
	recup_variable ($g_st_courriel, "email");
	recup_variable ($g_st_commentaire, "text");

	if (controle_variable ($g_st_pseudo, array ("text", OUI, 50, NULL))) $l_i_erreur = OUI; // quantité en dur, à passer en paramètre
	
	print ($g_st_pseudo [TYPE_DISPLAY]."<br/>");
	// vérif de type texte, contrôle de doublon, 
	// longueur max pseudo 50 caractères, champ à contrôler USR_login, table à contrôler t_user, champ id à contrôler USR_id, contrôler tous les pseudos
	if (controle_variable (&$g_st_courriel, array ("email", OUI, OUI))) $l_i_erreur = OUI; // contrôle sur doublon, contrôle sur bannis
	if (controle_variable (&$g_st_commentaire, array ("text", NON, 500)) && $g_st_commentaire [CONTENU_VAR] != "")  $l_i_erreur = OUI; // pas de contrôle de doublon, texte max 500 caractères
	// Le champ commentaire est facultatif
	if ($g_st_commentaire [CONTENU_VAR] == "")
	{
		// hack pour invalider l'erreur sur ce champ s'il est vide
		$g_st_commentaire [TYPE_DISPLAY] = NORMAL;
	}

}

//print ("Pseudo=".$g_st_pseudo [1]."<br>");
//print ("Itération=".$g_st_iteration [1]."<br>");


/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

//print($l_i_logok);

if ($l_i_logok == 0)
{
    // Utilisateur non logué => Enregistrement
    include ("inscription/enregistrement_niv1.inc");
}
else
{
	// Redirection vers la page d'accueil
	header("Location:http://".HOST."/accueil.php");
	exit();
}

// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Enregistrement");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerenregistrement_niv1, "enregistrement_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
