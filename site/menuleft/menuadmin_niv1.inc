<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : menuadmin_niv1.inc                                                                                 
* Fonction du fichier : permet d'afficher un menu d'administration dans le menu à gauche                                                                            
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                          
* Date de création :  05/12/2008                                                                             
* Version actuelle :  1.0 au 05/12/2008                                                                              
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                          
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */
if ($l_i_logok == OUI && $_SESSION ['mj'] == OUI)
{
	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu-admin");	
    if ($_SESSION ["droitnews"] == OUI) 
    {
    	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_news");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_news");	
	}   
        
    if ($_SESSION ["droitinscription"] == OUI)
    {
    	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_inscription");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_inscription");	    	
	}
        
    if ($_SESSION ["droitrecrutement"] == OUI)
    {
    	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_recrutement");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_recrutement");	    	
	}
    
    if ($_SESSION ["droiteditregles"] == OUI)
    {
    	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_regles");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_regles");	     	
	}
       
    if ($_SESSION ["droiteditmonde"] == OUI)
    {
     	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_monde");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_monde");	   	
	}        
        
    if ($_SESSION ["droitadmin"] == OUI)
    {
     	$l_o_template -> addSession ($l_i_handlermenuadmin_niv1,  "menu_admin");
    	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu_admin");	   	
	}
	
	$l_o_template -> closeSession ($l_i_handlermenuadmin_niv1, "menu-admin");
}s

/* Fin de fichier ***********************************************************************************/
?>
