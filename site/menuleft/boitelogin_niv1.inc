<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : boitelogin_niv1.inc                                                                                 
* Fonction du fichier : permet d'afficher la boite d'identification dans le menu à gauche                                                                            
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                           
* Date de création :  05/12/2008                                                                             
* Version actuelle :  1.0 au 05/12/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                          
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_var_login = array ("login", "", NORMAL);
$l_st_var_password = array ("password", "", NORMAL);
$l_st_var_referer = array ("referer", $l_s_referer, HIDDEN);
$l_st_affichage = array ();

if ($l_i_loginerr == 1)
{
    // Ici, on triche, seul le champ login sera considéré comme faux, pour de simples raisons d'affichage
    // de message d'erreur
    $l_st_var_login [2] = 2;
}

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlerboitelogin_niv1,  "boite-login");

if ($l_i_logok == NON)
{
	$l_o_template -> addSession ($l_i_handlerboitelogin_niv1,  "logueur");

	affiche_variable ($l_st_var_login, "input", $l_i_handlerboitelogin_niv1, NULL);
	affiche_variable ($l_st_var_password, "password", $l_i_handlerboitelogin_niv1, NULL);
	affiche_variable ($l_st_var_referer, "referer", $l_i_handlerboitelogin_niv1, NULL);

	$l_o_template -> closeSession ($l_i_handlerboitelogin_niv1, "logueur");
}
else
{
	$l_o_template -> addSession ($l_i_handlerboitelogin_niv1,  "logged");	
	$l_o_template -> setVar ($l_i_handlerboitelogin_niv1, "logged.nomjoueur", $_SESSION ['user']);
	$l_o_template -> closeSession ($l_i_handlerboitelogin_niv1, "logged");
}
$l_o_template -> closeSession ($l_i_handlerboitelogin_niv1, "boite-login");

/* Fin de fichier ***********************************************************************************/
?>
