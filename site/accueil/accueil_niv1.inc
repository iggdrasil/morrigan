<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : accueil_niv2.inc                                                                                 
* Fonction du fichier : permet d'afficher le baratin sur la page d'accueil                                                                            
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  14/04/2004                                                                             
* Version actuelle :  1.0 au 17/04/2004                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handleraccueil_niv1,  "accueil_niv1");
$l_o_template -> closeSession ($l_i_handleraccueil_niv1, "accueil_niv1");

/* Fin de fichier ***********************************************************************************/
?>

