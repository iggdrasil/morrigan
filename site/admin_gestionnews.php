<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : admin_gestionnews.php                                                                                                           
* Fonction du fichier : page de création/édition des news.                                           
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                         
* Date de création : 14/01/2009                                                                                                   
* Version actuelle : 1.0 au 14/01/2009                                                               
* License du projet : GPL                                                                                            
* Dernières modifications :                                                                                   
* Remarques particulières :                                                                                  
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_st_iteration1 = array ("iteration1", "", HIDDEN);
$g_st_idnews = array ("idnews", "", HIDDEN);
$g_st_actionnews = array ("actionnews", "", HIDDEN);
$g_st_titrenews = array ("titrenews", "", NORMAL);
$g_st_textenews = array ("textenews", "", NORMAL);

// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = NON;
$l_i_importedroitsok = NON;
$l_i_erreur = NON;
$l_s_referer = "news";
$l_i_iteration = 0;

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/mdp.inc");


/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}
/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handleraccueil_niv1 = $l_o_template -> Open ("local/tpl/tg_accueil_niv1.tpl");
$l_i_handlernews_niv1 = $l_o_template -> Open ("local/tpl/tg_editenews_niv1.tpl");
$l_i_handlerlistenews_niv2 = $l_o_template -> Open ("local/tpl/tg_liste_news_niv2.tpl");
$l_i_handlereditenews_niv2 = $l_o_template -> Open ("local/tpl/tg_editenews_niv2.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable

{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();
if ($l_i_logok == OUI)
{
	$l_i_verifdroitsok = verif_droits ();
}
//print($l_i_logok);

/* Récupération des variables passées en formulaire */
// Récupérer et contrôler infos à traiter (formulaire)
recup_variable ($g_st_iteration, "integer");
$l_i_erreur = controle_variable (&$g_st_iteration, "radio");
if ($l_i_erreur == OUI || $g_st_iteration [CONTENU_VAR] == "")
{
	$g_st_iteration [CONTENU_VAR] = 0;
}

recup_variable ($g_st_iteration1, "integer");
$l_i_erreur = controle_variable (&$g_st_iteration1, "radio");
if ($l_i_erreur == OUI || $g_st_iteration1 [CONTENU_VAR] == "")
{
	$g_st_iteration1 [CONTENU_VAR] = 0;
}

recup_variable ($g_st_actionnews, "integer");
$l_i_erreur = controle_variable (&$g_st_actionnews, "radio");
if ($l_i_erreur == OUI || $g_st_actionnews [CONTENU_VAR] == "")
{
	$g_st_actionnews [CONTENU_VAR] = 0;
}

if ($g_st_actionnews [CONTENU_VAR] == CREERNEWS)
{
	$l_i_iteration = $g_st_iteration1 [CONTENU_VAR];
}
elseif (($g_st_actionnews [CONTENU_VAR] == EDITERNEWS) || ($g_st_actionnews [CONTENU_VAR] == EFFACERNEWS))
{
	$l_i_iteration = $g_st_iteration [CONTENU_VAR];
}

if (($l_i_iteration > 0) && ($g_st_actionnews [CONTENU_VAR] != CREERNEWS))
{
	recup_variable ($g_st_idnews, "integer");
	$l_i_erreur = controle_variable (&$g_st_idnews, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
	if ($l_i_erreur == OUI || $g_st_idnews [CONTENU_VAR] == "")
	{
        trigger_error ("Aucun id de news transmis", FATAL);
        exit ();
	}
}

if ($l_i_iteration > 1)
{
	recup_variable ($g_st_titrenews, "text");
	if (controle_variable (&$g_st_titrenews, array ("text", 0, 50)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
	recup_variable ($g_st_textenews, "text");
	if (controle_variable (&$g_st_textenews, array ("text", 0, 1000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
} 


/* Contenu et inclusion des fichier de contenu */
// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
// News
if ($l_i_logok == NON)
{
       include ("admin/admin_niv1_login.inc");
}
else
{
    if ( $l_i_logok == NON)
    {
        include ("admin/admin_niv1_droitrefuse.inc");
    }
    if ($_SESSION ["droitnews"] == OUI)
    {
		include ("news/gerenews_niv1.inc");
		
		$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
		$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlernews_niv1, "news_niv1");
		$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");
	}
}


// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Gestion des News");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");



/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
