<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : loginusr.php                                                                                 
* Fonction du fichier : page transparente permettant de loguer l'user si ses coordonnées sont bonnes                                                                            
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                          
* Date de création : 16/02/2008                                                                               
* Version actuelle : 1.0 au 16/02/2008                                                                               
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières : nettoyer le fichier !                                                                        
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_st_var_login = array ("login", "", NORMAL);
$g_st_var_password = array ("password", "", NORMAL);
$g_st_var_referer = array ("referer", "", HIDDEN);

// VARIABLES LOCALES
$l_s_login = "";
$l_s_password = "";
$l_i_ok= 0;
$l_s_referer = "";

/* Récupération des variables de session */
// N/A

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/verif_mail.inc");



/* Redéfinition du gestionnaire d'erreurs */
$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Récupération des variables passées en formulaire */
recup_variable ($g_st_var_login, "text");
recup_variable ($g_st_var_password, "password");
recup_variable ($g_st_var_referer, "text");

//print ("Referer 1:".$g_st_var_referer [CONTENU_VAR]."<br>");

/* Instantiation de l'objet de template */
// N/A

/* Contenu et inclusion des fichier de contenu */

if (!in_array ($g_st_var_referer [CONTENU_VAR], $g_t_referers))
{
	$l_s_referer = "accueil";
}	
else 
{
	$l_s_referer = $g_st_var_referer [CONTENU_VAR];
	
}

$l_i_ok = login_user ($g_st_var_login [1], $g_st_var_password [1]);

if ($l_i_ok == 0)
{
    header ("Location:http://".HOST."/".$l_s_referer.".php?erreur=1");
    //print ("Referer=".$l_s_referer);
}
else
{
    header ("Location:http://".HOST."/".$l_s_referer.".php");
    //print ("Referer=".$l_s_referer);
}

/* Parsage final de la page */
// N/A

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();

/* Fin de fichier ***********************************************************************************/
?>

