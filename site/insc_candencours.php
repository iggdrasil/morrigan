<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : insc_candencours.php                                                                                                           
* Fonction du fichier : page permettant de suivre l'évolution de sa candidature                                         
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                         
* Date de création : 22/03/2008                                                                                                    
* Version actuelle : 1.0 au 22/03/2008                                                               
* License du projet : GPL                                                                                             
* Dernières modifications :                                                                                   
* Remarques particulières :   /!\ TODO : virer les print de debug 
* 							  /!\ TODO : renforcer le contrôle de doublon de nom de persos avec la table de persos du jeu
* 								                                                                            
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
//include ("lib/mdp.inc");
include ("Mail.php");
include ("lib/courriel.inc");
include ("lib/delta_nbpersos.inc");
//include ("lib/verif_mail.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_i_debug == 0;
$g_st_idcand = array ("idcand", "", HIDDEN);
$g_st_nomperso = array ("nomperso", "", NORMAL);
$g_st_race_cand = array ("race_cand", "", NORMAL);
$g_st_classe_cand = array ("classe_cand", "", NORMAL);
$g_st_sexe_cand = array ("sexe_cand", "", NORMAL);
$g_st_description = array ("description", "", NORMAL);
$g_st_background = array ("background", "", NORMAL);
$g_st_buts = array ("buts", "", NORMAL);
$g_st_debatusrmj = array ("debatusrmj", "", NORMAL);
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_st_actiontraitercand = array ("actiontraitercand", "", HIDDEN);
$g_i_erreur == NON;
$g_i_numeroprofil = 0;
$g_i_idcand = 0;

// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = NON;
$l_i_importedroitsok = NON;
$l_i_erreur = NON;
$l_s_referer = "insc_candencours";
$l_t_st_resultatcand = array ();

/*
$l_i_loginerr = 0;
*/

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;




/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlercandencours_niv1 = $l_o_template -> Open ("local/tpl/tg_traitercand_niv1.tpl");
$l_i_handlerloginusr_niv1 = $l_o_template -> Open ("local/tpl/tg_loginusr_niv1.tpl");
$l_i_handlerrappel_niv2 = $l_o_template -> Open ("local/tpl/tg_rappelcand_niv2.tpl");
$l_i_handlerformulaire_niv2 = $l_o_template -> Open ("local/tpl/tg_candformulaire_niv2.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");

/* Récupération des variables passées en formulaire et Querystring */
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable

{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();
if ($l_i_logok == OUI)
{
	$l_i_verifdroitsok = verif_droits ();
}
//print($l_i_logok);

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

if ($l_i_logok == NON)
{
    // Utilisateur non logué => Possibilité de se loguer
    include ("inscription/insc_login_niv1.inc");
}
else 
{
	recup_variable ($g_st_idcand, "integer");
	$l_i_erreur = controle_variable ($g_st_idcand, array ("text", 0, 11)); // variables en dur pas top
	if ($l_i_erreur == OUI)
	{
		trigger_error ("La candidature demandée pour postuler n'est pas valable", FATAL);
		exit ();
	}
	
	print ("Numéro candidature=".$g_st_idcand [CONTENU_VAR]."<br>");
	$g_i_idcand = $g_st_idcand [CONTENU_VAR];
	
	
	// Vérification du droit de traiter cette candidature
	$l_t_st_resultatcand = requete ("droittraitercand", $g_i_idcand, $_SESSION ['id']);
	if ($l_t_st_resultatcand [0] != 1)
	{
		// Pas le droit de traiter cette candidature, retour à l'accueil (soupçon de triche)
		trigger_error ("L'utilisateur '".$_SESSION ['id']."' a tenté de traiter cette candidature : '".$g_i_idcand."' sans en avoir le droit", WARNING);
		header ("Location:http://".HOST."/accueil.php");
	}
	
	$g_i_numeroprofil = $l_t_st_resultatcand [1][0][10];
	
	
	recup_variable ($g_st_iteration, "integer");
	$l_i_erreur = controle_variable (&$g_st_iteration, array ("text", 0, 11)); // variables en dur pas top
	print ("Erreur iteration : ".$g_st_iteration [CONTENU_VAR]."<br>");
	if ($l_i_erreur == OUI)
	{
		trigger_error ("L'itération du dépôt de candidature ne correspond à rien", FATAL);
		exit ();
	}
	
	if ($g_st_iteration [CONTENU_VAR] > 1)
	{
		recup_variable ($g_st_actiontraitercand, "integer");
		$l_i_erreur = controle_variable (&$g_st_actiontraitercand, array ("text", 0, 11)); // variables en dur pas top	
	}
	
	if ($g_st_iteration [CONTENU_VAR] > 2 && $g_st_actiontraitercand [CONTENU_VAR] == EDITERCAND)
	{
		
		// Action demandée par l'utilisateur : correction de la candidature
		recup_variable ($g_st_nomperso, "text");
		// Ce contrôle est totalement insuffisant vu qu'il ne contrôle que les candidatures en cours, et pas les personnages en jeu !
		if (controle_variable (&$g_st_nomperso, array ("text", OUI, 100, $g_i_idcand)) == OUI) $g_i_erreur = OUI;
		print ("Dis moi dis moi s'il y a une erreur ici nom ! ".$g_i_erreur."<br>");
        recup_variable ($g_st_race_cand, "array");
        if (controle_variable (&$g_st_race_cand, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;
   		print ("Dis moi dis moi s'il y a une erreur ici  race! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_classe_cand, "array");
        if (controle_variable (&$g_st_classe_cand, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;    
    	print ("Dis moi dis moi s'il y a une erreur ici classe ! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_sexe_cand, "array");
        if (controle_variable (&$g_st_sexe_cand, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;   
     	print ("Dis moi dis moi s'il y a une erreur ici sexe ! ".$g_i_erreur."<br>");   
        recup_variable ($g_st_description, "text");
        if (controle_variable (&$g_st_description, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
		print ("Dis moi dis moi s'il y a une erreur ici descro ! ".$g_i_erreur."<br>");
        recup_variable ($g_st_background, "text");
        if (controle_variable (&$g_st_background, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
    	print ("Dis moi dis moi s'il y a une erreur ici background ! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_buts, "text");
        if (controle_variable (&$g_st_buts, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
		print ("Dis moi dis moi s'il y a une erreur ici buts ! ".$g_i_erreur."<br>");	
        recup_variable ($g_st_debatusrmj, "text");
        if ((controle_variable (&$g_st_debatusrmj, array ("text", 0, 2000)) == OUI) && ($g_st_debatusrmj [CONTENU_VAR] != "")) $g_i_erreur = OUI; // var en dur à passer en define propre
        if ($g_st_debatusrmj [CONTENU_VAR] == "")
		{
			// hack pour invalider l'erreur sur ce champ s'il est vide (champ facultatif)
			$g_st_debatusrmj [TYPE_DISPLAY] = NORMAL;
			
		}       				
	}
	elseif ($g_st_iteration [CONTENU_VAR] > 1 && $g_st_actiontraitercand [CONTENU_VAR] == ABANDONNERCAND)
	{
		// Action demandée par l'utilisateur : abandon de la candidature
		// Rien d'autre à récupérer que l'action et l'id de la candidature
	}
	elseif ($g_st_iteration [CONTENU_VAR] > 1 && ($g_st_actiontraitercand [CONTENU_VAR] != ABANDONNERCAND) && $g_st_actiontraitercand [CONTENU_VAR] != EDITERCAND)
	{
		// Action non définie, erreur à notifier
		$g_i_erreur = OUI;
		$g_st_actiontraitercand [TYPE_DISPLAY] = ERRONE;
	}
	
	//print ("Dis moi dis moi s'il y a une erreur ici ! ".$g_i_erreur."<br>");

	include ("inscription/insc_traitercand_niv1.inc");
}

// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Suivi de candidature");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");


/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
