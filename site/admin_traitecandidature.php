<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_traitecandidature.php
* Fonction du fichier : page de traitement des candidatures
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  07/10/2007
* Version actuelle : 1.0 au 07/10/2007 
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : supprimer les traces de debug
*
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_st_idcandidature = array ("idcandidature", "", HIDDEN);
$g_st_actioncandidature = array ("actioncandidature", "", HIDDEN);
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_st_commentairepj = array ("commentairepj", "", NORMAL);
$g_st_avismj = array ("avismj", "", NORMAL);
$g_st_commentaireglobal = array ("commentaireglobal", "", NORMAL);
$g_st_statutcandidature = array ("statutcandidature", "", NORMAL);
$g_i_numerocandidature = 0;
$g_i_erreur = NON;
$g_t_st_infoscandidature = array (0, array ());

// VARIABLES LOCALES
$l_i_logok = NON; // user logué?
$l_i_erreur = NON; // sera reversé dans $g_i_erreur si égal à OUI
$l_t_i_existecandidature = array (0, array ());

$l_i_resultat = array (0, array ());

/* Récupération des variables de session */

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/admin_fonctions.inc");
include ("lib/delta_nbpersos.inc");
include ("Mail.php");
include ("lib/courriel.inc");

/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();
if (!$g_i_lien)
{
    trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
    exit ();
}

// construction d'une table utilisable lors des requetes de listing des statuts (pas très propre, mais pratique pour 
// rendre les statuts compatibles aux fonctions de manipulation de formulaire
$l_i_resultat = requete ("temp_statuts_candidatures");
if ($l_i_resultat ['nbaffecte'] != 0)
{
	trigger_error ("Impossible de créer la table temporaire des statuts des candidatures", FATAL);
	exit ();
}

$l_i_resultat = requete ("insert_statuts_candidatures");
if ($l_i_resultat ['nbaffecte'] == 0)
{
	trigger_error ("Impossible d'insérer les valeurs dans la table temporaire des statuts des candidatures", FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handlertraitecandidature_niv1 = $l_o_template -> Open ("local/tpl/tg_traitecandidature_niv1.tpl");
$l_i_handleraffichecandidature_niv1 = $l_o_template -> Open ("local/tpl/tg_affichecandidature_niv1.tpl");
$l_i_handleractionscandidature_niv1 = $l_o_template -> Open ("local/tpl/tg_actionscandidature_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */


// Vérification de l'user logué
$l_i_logok = verif_log_user ();


// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");


//print($l_i_logok);

if ($l_i_logok == NON)
{
    include ("admin/admin_niv1_login.inc");
}
else
{
    if ($_SESSION ["droitinscription"] == OUI)
    {
        recup_variable ($g_st_actioncandidature, "integer");
        $l_i_erreur = controle_variable ($g_st_actioncandidature, "radio");
        if ($l_i_erreur == OUI)
        {
            trigger_error ("Action inexistante.", ERROR);
            $l_i_erreur = NON;
            $g_st_actioncandidature [CONTENU_VAR] = AUCUNE;
        }

        recup_variable ($g_st_idcandidature, "integer");
        $l_i_erreur = controle_variable ($g_st_idcandidature, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
        if ($l_i_erreur == OUI)
        {
            trigger_error ("La candidature n'existe pas.", FATAL);
            exit ();
        }
        
        recup_variable ($g_st_iteration, "integer");
        $l_i_erreur = controle_variable (&$g_st_iteration,array ("text", 0, 11)); // idem
        if ($l_i_erreur == OUI)
        {
            trigger_error ("L'itération de traitement d'édition de candidature ne correspond à rien", FATAL);
            exit ();
        }
            
        $g_i_numerocandidature = $g_st_idcandidature [CONTENU_VAR];

        $l_t_i_existecandidature = requete ("existe_candidature", $g_st_idcandidature [CONTENU_VAR]);
        if ($l_t_i_existecandidature [0] == 0)
        {
            trigger_error ("Pas de candidaure correspondant à l'id de candidature demandé : ".$g_st_idcandidature [CONTENU_VAR].".", FATAL);
            exit ();
        }
        
        // Récupérer infos candidature en base de données
        $g_t_st_infoscandidature = requete ("infos_candidature", $g_st_idcandidature [CONTENU_VAR]);
        if ($g_t_st_infoscandidature [0] == 0)
        {
            trigger_error ("Pas de candidature correspondant à l'id de candidature demandé : ".$g_st_idcandidature [CONTENU_VAR].".", FATAL);
            exit ();
        }      
        
        if ($g_st_actioncandidature [CONTENU_VAR] == AUCUNE)
        {
        	include ("admin/admin_niv1_affichecandidature.inc");	
        }
        else
        {
        	$g_i_erreur = NON;
        	// Récupérer infos à traiter depuis le formulaire (selon l'action à traiter)
        	switch ($g_st_actioncandidature [CONTENU_VAR])
        	{
        		case COMMENTERPJ:
        		case REFUSERCAN:
        		case VALIDERCAN:
        		case METTREENATTENTE:
        			recup_variable ($g_st_commentairepj, "text");
        			$l_i_erreur = controle_variable (&$g_st_commentairepj, array ("text", 0, 2000));
        			
					if ($l_i_erreur == OUI && $g_st_commentairepj [CONTENU_VAR] != "") // commentaire facultatif
					{
						$g_i_erreur = OUI;
					}
					if ($g_st_commentairepj [CONTENU_VAR] == "")
					{
						// hack pour invalider l'erreur sur ce champ s'il est vide
						$g_st_commentairepj [TYPE_DISPLAY] = NORMAL;
					}
					
        			break;
        		case DONNERAVISMJ:
        		case DEMANDERAVIS:
        			recup_variable ($g_st_avismj, "text");
        			$l_i_erreur = controle_variable (&$g_st_avismj, array ("text", 0, 2000));
					if ($l_i_erreur == OUI && $g_st_avismj [CONTENU_VAR] != "") // commentaire facultatif
					{
						$g_i_erreur = OUI;
					}
					if ($g_st_avismj [CONTENU_VAR] == "")
					{
						// hack pour invalider l'erreur sur ce champ s'il est vide
						$g_st_avismj [TYPE_DISPLAY] = NORMAL;
					}       		
        			break;
        		case EFFACERCAN:
        			break;
        		case COMMENTERGLOBAL:
        			recup_variable ($g_st_commentaireglobal, "text");
        			$l_i_erreur = controle_variable (&$g_st_commentaireglobal, array ("text", 0, 2000));
        			if ($l_i_erreur == OUI)
        			{
            			$g_i_erreur == OUI;
        			}        		
        			break;
        		case CHANGERSTATUT:
        			// Cette action sert particulièrement à forcer un statut de candidature. Aucun commentaire n'est ajouté en base
                    recup_variable ($g_st_statutcandidature, "array");
                    $l_i_erreur = controle_variable (&$g_st_statutcandidature, array ("select", 1, 1));
                    if ($l_i_erreur == OUI)
                    {
                        trigger_error ("Le statut demandé pour la modification de la candidature n'est pas valable", FATAL);
                        exit ();
                    }

	       			break;
        		default:
        			// /!\ TODO : traitement d'erreur à faire
        			 
        	}
        	
        	
			if ($g_i_erreur == OUI)
			{
				$g_st_iteration [CONTENU_VAR] == 2;
				include ("admin/admin_niv1_affichecandidature.inc");
			}
			else
			{
				include ("admin/admin_niv1_traitecandidature.inc");				
			}
        }        
    }
   else
        include ("admin/admin_niv1_droitrefuse.inc");
}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration - Gestion des candidatures");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
    trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
