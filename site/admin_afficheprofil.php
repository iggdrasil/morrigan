<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_afficheprofil.php                                                                                 
* Fonction du fichier :
*                                  
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  02/12/2006                                                                              
* Version actuelle :  1.0 au 02/12/2006                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_st_var_liste_profils_uniques_rec = array ("liste_profils_uniques_rec", "", 0);
$g_st_var_liste_profils_types_rec = array ("liste_profils_types_rec", "", 0);
$g_st_var_liste_profils_soumis_rec = array ("liste_profils_soumis_rec", "", 0);
$g_st_var_liste_profils_uniques_insc = array ("liste_profils_uniques_insc", "", 0);
$g_st_var_liste_profils_types_insc = array ("liste_profils_types_insc", "", 0);
$g_st_var_liste_profils_soumis_insc = array ("liste_profils_soumis_insc", "", 0);

// VARIABLES LOCALES
$l_i_logok = 0; // user logué?

/* Récupération des variables de session */

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("local/def/def_vars_admin.inc");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/admin_fonctions.inc");

/* Récupération des variables passées en formulaire et Querystring*/
recup_variable ($g_st_var_liste_profils_uniques_rec, "integer");
recup_variable ($g_st_var_liste_profils_types_rec, "integer");
recup_variable ($g_st_var_liste_profils_soumis_rec, "integer");
recup_variable ($g_st_var_liste_profils_uniques_insc, "integer");
recup_variable ($g_st_var_liste_profils_types_insc, "integer");
recup_variable ($g_st_var_liste_profils_soumis_insc, "integer");

controle_variable (&$g_st_var_liste_profils_uniques_rec, array ("text", 0, 10));
controle_variable (&$g_st_var_liste_profils_types_rec, array ("text", 0, 10));
controle_variable (&$g_st_var_liste_profils_soumis_rec, array ("text", 0, 10));
controle_variable (&$g_st_var_liste_profils_uniques_insc, array ("text", 0, 10));
controle_variable (&$g_st_var_liste_profils_types_insc, array ("text", 0, 10));
controle_variable (&$g_st_var_liste_profils_soumis_insc, array ("text", 0, 10));

/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handlerafficheprofil_niv1 = $l_o_template -> Open ("local/tpl/tg_afficheprofil_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");


/* Contenu et inclusion des fichier de contenu */


// Vérification de l'user logué
$l_i_logok = verif_log_user ();

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

if ($l_i_logok == 0)
{
    include ("admin/admin_niv1_login.inc");
}
else
{
    if ($_SESSION ["droitinscription"] == 1)
        include ("admin/admin_niv1_afficheprofil.inc");
    else
        include ("admin/admin_niv1_droitrefuse.inc");
}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration - Gestion des profils");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
