<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_editequizz.php                                                                                 
* Fonction du fichier : page de listage des questions du quizz
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                          
* Date de création :  26/06/2008                                                                              
* Version actuelle :  1.0 au 26/06/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");

include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD

$g_st_idquestion = array ("idquestion", "", HIDDEN);
$g_st_question = array ("question", "", NORMAL);
$g_st_actionquizz = array ("actionquizz", "", HIDDEN);
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_i_erreur = NON;

$g_i_numeroquestion = 0;

// VARIABLES LOCALES
$l_i_logok = NON; // user logué? droits ok?
$l_i_old_error_handler = NON;
$l_t_i_existequestion = NON;

/* Récupération des variables passées en formulaire et Querystring*/
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login admin, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}


/* Récupération des variables de session */



/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handleradminquizz_niv1 = $l_o_template -> Open ("local/tpl/tg_admineditequizz_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */


// Vérification de l'user logué
$l_i_logok = verif_log_user ();


// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

print($l_i_logok);

if ($l_i_logok == NON)
{
       include ("admin/admin_niv1_login.inc");
}
else
{
    if ( $l_i_logok == NON)
    {
        trigger_error ("Erreur lors de la vérification des droits de l'admin ".$_SESSION ["user"], FATAL);
    }
    
    if ($_SESSION ["droitrecrutement"] == OUI)
    {
    	// Récupérer et contrôler infos à traiter (formulaire)
        recup_variable ($g_st_iteration, "integer");
        $l_i_erreur = controle_variable (&$g_st_iteration, "radio");
        if ($l_i_erreur == OUI || $g_st_iteration [CONTENU_VAR] == "")
        {
            $g_st_iteration [CONTENU_VAR] = 0;
            $g_st_iteration [TYPE_DISPLAY] = HIDDEN;
        }

    	
    	include ("admin/admin_niv1_editionquizz.inc");
	}
	else
	{
        include ("admin/admin_niv1_droitrefuse.inc");
	}

}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration - Edition du Quizz");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>


