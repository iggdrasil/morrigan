Très cher MJ [login],

Une candidature correspondant au profil '[profil]' que vous avez fait mettre en ligne a été déposée. Après relecture de ladite candidature, le MJ aimerait connaître votre avis.  Merci de consulter la candidature dans l'interface d'administration des inscriptions et de répondre au MJ ans les meilleurs délais. C'est qu'un joueur attend, tout de même !

Ludiquement,

Le robot du portail d'inscription de [jeu]
