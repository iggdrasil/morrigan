Cher [login],

Nous avons le regret de vous annoncer que votre candidature correspondant à l'offre '[profil]' a été refusée par le MJ Inscription pour la raison suivante :

[raison]

Mais ne vous découragez pas, vous pouvez dès maintenant retenter votre chance si une autre offre vous convient !

A bientôt !

Ludiquement, 

Le robot du portail d'inscription de [jeu]

