Cher [login],

Votre candidature correspondant à l'offre '[profil]' a été relue par les MJs. Ceux-ci estiment que des améliorations sont nécessaires, et vous invitent à prendre connaissance de leurs remarques afin de modifier votre candidature.

Ainsi, vous pouvez toujours la consulter, l'effacer ou la modifier selon son statut.

Pour rappel, vous pouvez effacer ou consulter votre candidature à tout moment. En revanche, vous ne pouvez la modifier que si le MJ Inscription passe votre candidature au statut 'En Edition par le joueur'.

En espérant que votre candidature sera bientôt acceptée,

Ludiquement,

Le robot portail d'inscription de [jeu]
