Bonjour, [pseudo]

Vous venez de déposer une candidature sur [nom_site] !

Le personnage que vous souhaitez incarner en jeu aurait pour pseudonyme [nomperso]

Le MJ Inscription va examiner votre candidature, et vous serez informé de l'avancement de son traitement jusqu'à votre mise en jeu, si tout se passe bien.

A bientôt,

L'équipe de [nom_site]
