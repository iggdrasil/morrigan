Vous venez de vous inscrire sur [nom_site] !

Bienvenue à vous. Nous espérons que nous partagerons de grands moments ludiques ensemble.

Afin de pouvoir vous considérer définitivement inscrits, il vous reste une petite formalité à accomplir : identifiez-vous avec les informations suivantes :
Pseudonyme : [pseudo]
Mot de passe : [password]

De cette manière, nous saurons que vous n'êtes pas un robot !

Vous avez une semaine pour vous identifier, sans quoi votre compte sera supprimé.

L'équipe de [nom_site]
