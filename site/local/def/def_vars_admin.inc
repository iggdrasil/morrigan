<?php
/****************************************************************************************************
*                                                                                                   *
*           PROJET MORRIGAN                                                                         *
*                                                                                                   *
* Nom du fichier : def_vars_admin.inc                                                               *
* Fonction du fichier : définit un certain nombre de variables générales à l'administration         *
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  *
* Date de création : 26/09/2007                                                                     *
* Version actuelle : 1.0 au 26/09/2007                                                              *
* License du projet : GPL                                                                              *
* Dernières modifications :                                                                         *
* Remarques particulières :                                                                         *
*                                                                                                   *
*****************************************************************************************************/

// Aide à la clarté des statuts des profils
define ("EDITION", 0);
define ("RELECTURE", 1);
define ("HORSLIGNE", 2);
define ("ENLIGNE", 3);

define ("EDITIONTXT", "En édition");
define ("RELECTURETXT", "En relecture");
define ("HORSLIGNETXT", "Validé et hors ligne");
define ("ENLIGNETXT", "Validé et en ligne");

// Aide à la clarté des critères de tri de listage des profils
define ("IDPROFIL", 0);
define ("AUTEURPROFIL", 1);
define ("STATUTPROFIL", 2);
define ("CATPROFIL", 3);

// Aide à la clarté des actions de traitement des profils
define ("AFFICHER", 1);
define ("CREER", 2);
define ("CREATIONDAPRES", 3);
define ("EDITER", 4);
define ("DEMANDEENLIGNE", 5);
define ("DEMANDEHORSLIGNE", 6);
define ("DEMANDEEDITER", 7);
define ("STATUT", 8);
define ("EFFACER", 9);
define ("REFUSER", 10);

define ("AFFICHERTXT", "Afficher ce profil");
define ("CREERTXT", "Créer un nouveau profil");
define ("CREATIONDAPRESTXT", "Créer un nouveau profil à partir de ce profil");
define ("EDITERTXT", "Éditer ce profil");
define ("DEMANDEENLIGNETXT", "Demander à mettre ce profil en ligne");
define ("DEMANDEHORSLIGNETXT", "Demander à mettre ce profil hors ligne");
define ("DEMANDEEDITERTXT", "Demande de mise hors ligne du profil et de sa remise en édition");
define ("STATUTTXT", "Changer le statut de ce profil");
define ("EFFACERTXT", "Effacer ce profil");
define ("REFUSERTXT", "Refuser la demande de modification de statut");
define ("METTREHORSLIGNETXT", "Mettre le profil hors ligne suite à la fermeture d'une race/classe/sexe");
// Aide à la clarté des statuts des candidatures
define ("EDITIONUSER", 0);
define ("RELECTUREMJ", 1);
define ("AVISMJ", 2);
define ("ENATTENTE", 3);
define ("ABANDONNEE", 4);
define ("ACCEPTEE", 5);
define ("REFUSEE", 6);

define ("EDITIONUSERTXT", "En édition par le joueur");
define ("RELECTUREMJTXT", "Soumise au MJ Inscription");
define ("AVISMJTXT", "En relecture par le MJ auteur du profil");
define ("ENATTENTETXT", "En attente");
define ("ABANDONNEETXT", "Abandonnée");
define ("ACCEPTEETXT", "Acceptée");
define ("REFUSEETXT", "Refusée");

// Aide à la clarté des actions de traitement des candidatures
define ("AUCUNE", 0);
define ("COMMENTERPJ", 1);
define ("DONNERAVISMJ", 2);
//define ("COMMENTERMJ", 3);
define ("DEMANDERAVIS", 3);
define ("REFUSERCAN", 4);
define ("EFFACERCAN", 5);
define ("VALIDERCAN", 6);
define ("METTREENATTENTE", 7);
define ("COMMENTERGLOBAL", 8);
define ("CHANGERSTATUT", 9);
define ("EDITERCAND", 10);
define ("ABANDONNERCAND", 11);

define ("DEPOSERCANDTXT", "Déposer la candidature");
define ("AUCUNETXT", "Pas d'action");
define ("COMMENTERPJTXT", "Commenter la candidature à destination du Joueur");
define ("DONNERAVISMJTXT", "Donner son avis sur la candidature au MJ Inscription");
define ("DEMANDERAVISTXT", "Demander son avis au MJ auteur du profil rattaché à la candidature");
define ("REFUSERCANTXT", "Refuser la candidature");
define ("EFFACERCANTXT", "Effacer la candidature");
define ("VALIDERCANTXT", "Valider la candidature");
define ("METTREENATTENTETXT", "Mettre la candidature en attente");
define ("COMMENTERGLOBALTXT", "Commenter la candidature");
define ("CHANGERSTATUTTXT", "Changer le statut de la candidature");
define ("EDITERCANTXT", "Corriger la candidature");
define ("ABANDONNERCANDTXT", "Abandonner la candidature");

define ("FERMETURETXT", "Un des choix que vous avez fait pour votre candidature a été fermé  aux inscriptions.");


// Aide à la gestion du champ nbpersos des profils
define ("NUL", 0);
define ("POSITIF", 1);
define ("AUCUN", 0);
define ("PLUS", 1);
define ("MOINS", -1);
define ("ERREUR", 2);
define ("REFUS", 3);

// Aide à l'édition des questions/réponses du quizz
define ("CREERQUESTION", 0);
define ("EFFACERQUESTION", 1);
define ("CORRIGERQUESTION", 2);

define ("CREERQUESTIONTXT", "Créer la question");
define ("EFFACERQUESTIONTXT", "Effacer la question");
define ("CORRIGERQUESTIONTXT", "Corriger la question");

// Aide à l'édition des catégories du quizz
define ("CREERCATEGORIE", 0);
define ("EFFACERCATEGORIE", 1);
define ("CORRIGERCATEGORIE", 2);

define ("CREERCATEGORIETXT", "Créer la categorie");
define ("EFFACERCATEGORIETXT", "Effacer la categorie");
define ("CORRIGERCATEGORIETXT", "Corriger la categorie");

// Aide à l'édition des news
define ("CREERNEWS", 0);
define ("EDITERNEWS", 1);
define ("EFFACERNEWS", 2);

define ("CREERNEWSTXT", "Créer une news");
define ("EDITERNEWSTXT", "Editer la news");
define ("EFFACERNEWSTXT", "Effacer cette news");
?>
