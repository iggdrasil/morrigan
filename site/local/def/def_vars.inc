<?php
/****************************************************************************************************
*                                                                                                   *
*           PROJET MORRIGAN                                                                         *
*                                                                                                   *
* Nom du fichier : def_vars.inc                                                                     *
* Fonction du fichier : définit un certain nombre de variables générales au site                    *
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  *
* Date de création : 04/04/2004                                                                     *
* Version actuelle : 1.0 au 04/04/2004                                                              *
* License du projet : GPL                                                                              *
* Dernières modifications :                                                                         *
* Remarques particulières :                                                                         *
*                                                                                                   *
*****************************************************************************************************/

// Définition de la variable host pour permettre une modification rapide du chemin en cas de
// changement d'adresse du site
define ("HOST", "www.zarkland.net/solum");
define ("NOM_SITE", "Solum");
define ("EMAIL_SITE", "Z@zarkland.net");
define ("SMTP_HOST", "smtp.orange.fr");

// Coordonnées de connexion à la base de données
define ("LOGBDD", "arkenlond"); // login
define ("PASSBDD", "NirsutFix2"); // password
define ("HOSTBDD", "localhost"); // hôte de la bdd
define ("NOMBDD", "morrigan-site"); // nom de la base de travail
define ("PORTBDD", 5432);

// Niveau d'erreurs souhaités
define ("FATAL", E_USER_ERROR);
define ("ERROR", E_USER_WARNING);
define ("WARNING", E_USER_NOTICE);

// Nombre de droits dans l'administration
define ("NBDROITS",7);

// Aide-mémoire pour les variables des formulaires
define ("NOM_VAR", 0);
define ("CONTENU_VAR", 1);
define ("TYPE_DISPLAY", 2);

define ("NORMAL", 0);
define ("HIDDEN", 1);
define ("ERRONE", 2);

// Aide à la clarté des drapeaux
define ("OUI", 1);
define ("NON", 0);

define ("NOLIMITCAND", "Pas de limite");

// Quizz ou pas?
define ("QUIZZ", 1); // 1 = OUI, 0 = NON;
// Nombre questions du Quizz
define ("NOMBREQUESTIONS", 10);
define ("NOMBREREPONSES", 5);
// En pourcentage, le score à atteindre pour être admis à l'étape suivante
define ("SCOREOK", 90);

// Pour dire OUI ou NON en toutes les langues
define ("OUITXT", "Oui");
define ("NONTXT", "Non");

define ("OUVERTTXT", "Ouvert");
define ("FERMETXT", "Fermé");

define ("NEWSACCUEIL", 5);

// On définit le niveau d'erreur souhaité - on va faire notre propre gestion d'erreur
/*error_reporting (0);*/

// Par défaut, Mode Debug désactivé
$g_i_debug = 0;

$g_t_referers = array ("insc_listeannonces");

// Temps maximal d'une session, défini en secondes
define ("SESSIONTIME", 1200); 

/* Fin de fichier ***********************************************************************************/
?>
