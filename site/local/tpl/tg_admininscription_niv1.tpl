<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_admininscription_niv1-->
<div class="contenu">
    <h1>Administration des inscriptions</h1>
    <div class="button"><a href="admin_profil.php">Gestion des profils</a></div>
    <div class="button"><a href="admin_candidatures.php">Gestion des candidatures</a></div>
</div>
<!--/VTP_admininscription_niv1-->
