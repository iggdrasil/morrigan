<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_afficherquizz_niv1-->
<div class="contenu">
	<h1>Connaissez-vous suffisamment le jeu et son monde?</h1>
	<p>Afin de nous assurer de votre motivation à entrer dans le jeu et à jouer un personnage cohérent, nous vous prions de vous plier à ce petit QCM. Si vous répondez bon à suffisamment de questions, nous vous permettrons de déposer votre candidature.<br>
	Pour répondre sereinement à ces questions, nous vous encourageons à lire les règles et le background du jeu sérieusement.</p>
	<p>Note pour les petits malins : il peut y avoir plusieurs réponses correctes aux questions posées, mais si vous cochez une seule réponse fausse, vous n'aurez pas bon à la question. Si vous ne cochez qu'une seule réponse et qu'il fallait en sélectionner plusieurs, vous n'aurez pas la note maximale. Pour chaque question, il y a toujours au moins une réponse bonne. A vous de voir !</p>
	<form action="quizz.php?idprofil={#idprofil}" method="POST">
	<!--VTP_question-->
		<p><span class="imp">Question :</span> {#question}<br>
		<!--VTP_hidden_idquestions-->
           <input type="hidden" name="idquestions[]" value="{#idquestions}">
		<!--/VTP_hidden_idquestions-->
		<!--VTP_idreponses-->
			<!--VTP_idreponses_checkbox_option-->
				<input type="checkbox" name="idreponses[]" value="{#value}" {#selected}>{#nom}<br>
			<!--/VTP_idreponses_checkbox_option-->
		<!--/VTP_idreponses-->
		</p>
	<!--/VTP_question-->
	<!--VTP_hidden_iterationquizz-->
    	<input type="hidden" name="iterationquizz" value="{#iterationquizz}">
	<!--/VTP_hidden_iterationquizz-->
	<input type="submit" value="Résultat du Quizz" onclick="return confirm('Êtes-vous sûr de vos réponses?')">
	</form><br>	
    	<a class="norm" href="accueil.php">Retourner à l'accueil</a><br>
    	<a class="norm" href="delogusr.php">Fermer la session d'identification</a>

</div>	
<!--/VTP_afficherquizz_niv1-->
<!--VTP_resultatquizz_niv1-->
<div class="contenu">
	<h1>Résultat du quizz</h1>
<!--VTP_qualifie-->
	<p>Votre score au Quizz vous permet de déposer votre candidature. Ce score est enregistré, et, dorénavant, vous n'aurez plus à le repasser si d'aventure vous avez à présenter une autre candidature.</p>
    	<form action="insc_posercand.php" method="POST">
    		<!--VTP_hidden_idprofil-->
    			<input type="hidden" name="idprofil" value="{#idprofil}">
    		<!--/VTP_hidden_idprofil-->
    		<!--VTP_hidden_iteration-->
    			<input type="hidden" name="iteration" value="{#iteration}">
    		<!--/VTP_hidden_iteration-->
    		<input type="submit" value="Répondre à la candidature">
        </form>
<!--/VTP_qualifie-->
<!--VTP_insuffisant-->
	<p>Votre score au Quizz ne vous permet pas de déposer votre candidature. Pour déposer une candidature, vous devez revenir à la liste des annonces, en choisir une et repasser un nouveau Quizz. Avec de la persévérence et une bonne dose de connaissances sur les règles et le background du jeu, vous ne devriez plus rencontrer de problèmes.</p>
<!--/VTP_insuffisant-->
<!--VTP_triche-->
	<p>Vous avez tenté de tricher à ce quizz. <span class="err">C'est mal et vous êtes démasqué.</span> Pour déposer une candidature, vous devez revenir à la liste des annonces, en choisir une et repasser un nouveau Quizz. Avec de la persévérence et une bonne dose de connaissances sur les règles et le background du jeu, vous ne devriez plus rencontrer de problèmes.</p>
<!--/VTP_triche-->
    	<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
    	<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_resultatquizz_niv1-->
