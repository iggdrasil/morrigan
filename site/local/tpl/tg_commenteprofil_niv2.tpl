<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_commenteprofil_niv2-->
    <!--VTP_commente_ok--> 
    <h1>Commentaire ajouté avec succès</h1>    
    <p>Le commentaire a été ajouté au débat sur ce profil.<p>
    <!--/VTP_commente_ok--> 

    <!--VTP_commente_nok-->     
    <h1>Erreurs dans le commentaire ajouté</h1>
    <form action="admin_traiteprofil.php" method="POST">
        <!--VTP_hidden_idprofil-->        
            <input type="hidden" name="idprofil" value="{#idprofil}">
        <!--/VTP_hidden_idprofil-->
        <!--VTP_hidden_operation-->        
            <input type="hidden" name="operation" value="{#operation}">
        <!--/VTP_hidden_operation-->

        <!--VTP_erreur_debat-->
        <p><span class="err">Vous avez oublié d'entrer un commentaire ou celui que vous avez entré est trop long.</span></p>
        <!--/VTP_erreur_debat-->
        <!--VTP_hidden_debat-->        
            <input type="hidden" name="debat" value="{#debat}">
        <!--/VTP_hidden_debat-->
        <!--VTP_debat-->
            <textarea name="debat" rows="5" cols="80">{#debat}</textarea>    
        <!--/VTP_debat-->    
        <br>
        <input type="submit" value="Soumettre les corrections" onclick="return confirm('Confirmer la soumission des corrections?')">
    </form>    
    <br/><br/>
    <!--/VTP_commente_nok--> 
<!--/VTP_commenteprofil_niv2-->
