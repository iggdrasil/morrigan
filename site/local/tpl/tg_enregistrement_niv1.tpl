<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_enregistrement_niv1-->
<div class="contenu">
<h1>Bienvenue sur le site du Projet Morrigan !</h1>
	<!--VTP_formulaire_niv1-->
		<form action="enregistrement.php" method="POST">
		<!--VTP_hidden_iteration-->
			<input type="hidden" name="iteration" value="{#iteration}">
		<!--/VTP_hidden_iteration-->
		<p><span class="imp">Pseudonyme : </span></p>
        <!--VTP_erreur_pseudo-->
            <p><span class="erreur">Le pseudonyme que vous avez entré pour ce profil est soit trop long, soit déjà utilisé</span></p>
        <!--/VTP_erreur_pseudo-->
        <!--VTP_pseudo-->
            <input type="text" name="pseudo" value="{#pseudo}" size="30" maxlength="50">
        <!--/VTP_pseudo-->
        <!--VTP_hidden_pseudo-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="pseudo" value="{#pseudo}">
        <!--/VTP_hidden_pseudo-->
        <p><span class="imp">Adresse de courriel : </span></p>
        <!--VTP_erreur_courriel-->
            <p><span class="erreur">Le courriel que vous avez entré pour ce profil est soit trop long, soit invalide, soit déjà utilisé (à moins qu'il ne soit banni de ce site?).</span></p>
        <!--/VTP_erreur_courriel-->
        <!--VTP_courriel-->
            <input type="text" name="courriel" value="{#courriel}" size="30" maxlength="50">
        <!--/VTP_courriel-->
        <!--VTP_hidden_courriel-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="courriel" value="{#courriel}">
        <!--/VTP_hidden_courriel-->
        <p><span class="imp">Vous pouvez ajouter un commentaire :</span></p>
        <!--VTP_erreur_commentaire-->
            <p><span class="erreur">Ce commentaire est trop long.</span></p>
        <!--/VTP_erreur_commentaire-->
        <!--VTP_commentaire-->
            <textarea name="commentaire" rows="10" cols="80">{#commentaire}</textarea>
        <!--/VTP_commentaire-->
        <!--VTP_hidden_commentaire-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="commentaire" value="{#commentaire}">
        <!--/VTP_hidden_commentaire-->
        <p>Vous n'avez pas à entrer de mot de passe. Morrigan va en générer un pour vous, qui va vous être fourni par courriel après la vérification des informations que vous avez entrées.        
        </p>
		<input type="submit" value="S'enregistrer" onclick="return confirm('Confirmer la soumission de ces informations ?')">
		</form>
	<!--/VTP_formulaire_niv1-->
	<!--VTP_succes_niv1-->
		<p>Votre compte a été créé sur notre site. Vous allez pouvoir postuler à la création d'un personnage et entrer en jeu, ou participer à la communauté du forum.<br>
		<span class="imp">Attention, cependant :</span> un courriel vous a été envoyé, qui contient votre pseudonyme pour le site et un mot de passe généré aléatoirement par nos soins.<br>
		Afin d'être <span class="imp">définitivement considéré comme enregistré sur ce site</span>, vous devrez vous <span class="imp">identifier</span> avec votre pseudonyme et le mot de passe que vous allez recevoir. Si vous ne le faites pas rapidement, votre compte sera effacé.<br>
		À bientôt et bienvenue parmi nous !
		</p>
	<!--/VTP_succes_niv1-->
	<!--VTP_echec_niv1-->
		<p>Un contretemps nous a empêché de vous faire parvenir le courriel contenant les informations vous permettant d'être définitivement enregistré sur notre site. Veuillez contacter le webmaster du site en lui fournissant votre adresse de courriel et votre pseudo afin qu'il puisse régler ce problème.</p>
	<!--/VTP_echec_niv1-->
</div>
<!--/VTP_enregistrement_niv1-->
