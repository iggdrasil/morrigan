<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_editioncategorie_niv1-->
<div class="contenu">
    <h1>Edition et correction des catégories des profils</h1>
	<p>Vous pouvez, au moyen de ce formulaire, créer, éditer ou effacer une catégorie de profil.</p>
	<!--VTP_erreur_globale_niv1-->
		<span class="erreur">La saisie de vos réponses comporte au moins un problème. Veuillez vérifier que :<br>
		<ul>
			<li>même si le script est incapable de vérifier cette information, veuillez vous assurer que la réponse que vous avez choisie comme bonne est réellement la bonne réponse !</li>
		</ul>
	  </span>
	<!--/VTP_erreur_globale_niv1-->
	<form action="admin_editecategories2.php" method="POST">
	<p>Intitulé de la categorie : <br>
	<!--VTP_hidden_idcategorie-->
		<input type="hidden" name="idcategorie" value="{#idcategorie}">
	<!--/VTP_hidden_idcategorie-->
	<!--VTP_erreur_categorie-->
		<span class="erreur">Il y a une erreur dans l'intitulé de la categorie. Il est soit vide, soit trop long, soit déjà existant dans la liste des catégories existantes.</span><br>
	<!--/VTP_erreur_categorie-->	
	<!--VTP_categorie-->
		<textarea name="categorie" rows="1" cols="60">{#categorie}</textarea>
	<!--/VTP_categorie--></p>
	
    <!--VTP_hidden_iteration-->
       	<input type="hidden" name="iteration" value="{#iteration}">
    <!--/VTP_hidden_iteration-->
    
    Que désirez-vous faire?<br>
    <!--VTP_actions_niv1-->
    	<input type="radio" name="actioncategorie" value="{#edition}">{#editiontxt}<br>
    	<input type="radio" name="actioncategorie" value="{#suppression}">{#suppressiontxt}<br>
    <!--/VTP_actions_niv1-->
    <!--VTP_creation_niv1-->
        <input type="radio" name="actioncategorie" value="{#creation}" checked>{#creationtxt}<br>
    <!--/VTP_creation_niv1-->
	<input type="submit" value="Exécuter cette action"onclick="return confirm('Êtes-vous sûr?')">
	</form>
		  
    <div class="button"><a href="admin_editecategories.php">Retour à la gestion des categories</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_editioncategorie_niv1-->

<!--VTP_succes_niv1-->
 <div class="contenu">
    <div class="t1">Succès de l'édition ou correction de la catégorie de profil</div>
 	<!--VTP_editioncategorieeffacerechec_niv1-->
		<p>La categorie ne peut pas être effacée, elle est actuellement utilisée par au moins un profil.</p>	
	<!--/VTP_editioncategorieeffacerechec_niv1-->  	
 	<!--VTP_editioncategorieeffacersucces_niv1-->
		<p>La categorie a bien été effacée.</p>	
	<!--/VTP_editioncategorieeffacersucces_niv1-->  
	 	<!--VTP_editioncategoriecreersucces_niv1-->
		<p>La categorie a bien été créée.</p>	
	<!--/VTP_editioncategoriecreersucces_niv1-->   
	 	<!--VTP_editioncategoriecorrigersucces_niv1-->
		<p>La categorie a bien été corrigée.</p>	
	   <!--/VTP_editioncategoriecorrigersucces_niv1-->    
    <div class="button"><a href="admin_editecategories.php">Retour à la gestion des categories</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_succes_niv1-->
