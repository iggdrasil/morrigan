<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traitementprofil_demande_niv2-->
    <form action="admin_traiteprofil.php" method="POST">
        <!--VTP_hidden_iteration-->
            <input type="hidden" name="iteration" value="{#iteration}">
        <!--/VTP_hidden_iteration-->
        <!--VTP_hidden_actionprofil-->
            <input type="hidden" name="actionprofil" value="{#actionprofil}">
        <!--/VTP_hidden_actionprofil-->
        <!--VTP_hidden_idprofil-->
            <input type="hidden" name="idprofil" value="{#idprofil}">
        <!--/VTP_hidden_idprofil-->	
        <p><span class="imp">Discussion à propos du profil :</span></p>
        <!--VTP_erreur_debat-->
            <p><span class="erreur">Le commentaire du profil est trop long.</span></p>
        <!--/VTP_erreur_debat-->
        <!--VTP_debat-->
            <textarea name="debat" rows="10" cols="80">{#debat}</textarea>
        <!--/VTP_debat-->
	</form>
<!--/VTP_traitementprofil_demande_niv2-->
<!--VTP_traitementimpossible_niv2-->
	<p><span class="erreur">Il y a une candidature associée à ce profil : changer son statut à En Édition ou l'effacer est impossible. Il n'y a pas de modification sur ce profil.</span></p>
<!--/VTP_traitementimpossible_niv2-->
