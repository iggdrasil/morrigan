<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_editionquizz2_niv1-->
<div class="contenu">
    <h1>Edition et correction des questions/réponses du quizz</h1>
	<p>Vous pouvez, au moyen de ce formulaire, créer, éditer ou effacer une question de quizz et ses réponses.</p>
	<!--VTP_erreur_globale_niv1-->
		<span class="erreur">La saisie de vos réponses comporte au moins un problème. Veuillez vérifier que :<br>
		<ul>
			<li>vous n'avez pas entré deux fois la même réponse,</li>
			<li>vous avez au moins sélectionné une bonne réponse,</li>
			<li>la réponse que vous avez sélectionnée pour être bonne est vide,</li>
			<li>il n'y a aucune réponse valide dans votre saisie (trop longue, vide, ...),</li>
			<li>même si le script est incapable de vérifier cette information, veuillez vous assurer que la réponse que vous avez choisie comme bonne est réellement la bonne réponse !</li>
		</ul>
	  </span>
	<!--/VTP_erreur_globale_niv1-->
	<form action="admin_editequizz2.php" method="POST">
	<p>Intitulé de la question : <br>
	<!--VTP_hidden_idquestion-->
		<input type="hidden" name="idquestion" value="{#idquestion}">
	<!--/VTP_hidden_idquestion-->
	<!--VTP_erreur_question-->
		<span class="err">Il y a une erreur dans l'intitulé de la question. Il est soit vide, soit trop long, soit déjà existant dans la liste des questions.</span><br>
	<!--/VTP_erreur_question-->	
	<!--VTP_question-->
		<textarea name="question" rows="1" cols="60">{#question}</textarea>
	<!--/VTP_question--></p>
	
	<p>Intitulé de la réponse 1 : <br>	
	<!--VTP_hidden_idreponse1-->
		<input type="hidden" name="idreponse1" value="{#idreponse1}">
	<!--/VTP_hidden_idreponse1-->
	<!--VTP_erreur_reponse1-->
		<span class="err">Il y a une erreur dans l'intitulé de cette réponse. Il est soit vide, soit trop long.</span><br>
	<!--/VTP_erreur_reponse1-->
	<!--VTP_reponse1-->
		<textarea name="reponse1" rows="1" cols="60">{#reponse1}</textarea>
	<!--/VTP_reponse1-->
	<!--VTP_reponsebonne1-->
		<!--VTP_reponsebonne1_checkbox_option-->
		<input type="checkbox" name="{#nom}[]" value="{#value}" {#selected}>
		<!--/VTP_reponsebonne1_checkbox_option-->
	<!--/VTP_reponsebonne1-->
	Cocher si cette réponse est bonne</p>
	
	<p>Intitulé de la réponse 2 : <br>	
	<!--VTP_hidden_idreponse2-->
		<input type="hidden" name="idreponse2" value="{#idreponse2}">
	<!--/VTP_hidden_idreponse2-->		
	<!--VTP_erreur_reponse2-->
		<span class="err">Il y a une erreur dans l'intitulé de cette réponse. Il est soit vide, soit trop long.</span><br>
	<!--/VTP_erreur_reponse2-->
	<!--VTP_reponse2-->
		<textarea name="reponse2" rows="1" cols="60">{#reponse2}</textarea>
	<!--/VTP_reponse2-->
	<!--VTP_reponsebonne2-->
		<!--VTP_reponsebonne2_checkbox_option-->
		<input type="checkbox" name="{#nom}[]" value="{#value}" {#selected}>
		<!--/VTP_reponsebonne2_checkbox_option-->
	<!--/VTP_reponsebonne2-->
	Cocher si cette réponse est bonne</p>
	
	<p>Intitulé de la réponse 3 : <br>	
	<!--VTP_hidden_idreponse3-->
		<input type="hidden" name="idreponse3" value="{#idreponse3}">
	<!--/VTP_hidden_idreponse3-->		
	<!--VTP_erreur_reponse3-->
		<span class="err">Il y a une erreur dans l'intitulé de cette réponse. Il est soit vide, soit trop long.</span><br>
	<!--/VTP_erreur_reponse3-->
	<!--VTP_reponse3-->
		<textarea name="reponse3" rows="1" cols="60">{#reponse3}</textarea>
	<!--/VTP_reponse3-->
	<!--VTP_reponsebonne3-->
		<!--VTP_reponsebonne3_checkbox_option-->
		<input type="checkbox" name="{#nom}[]" value="{#value}" {#selected}>
		<!--/VTP_reponsebonne3_checkbox_option-->
	<!--/VTP_reponsebonne3-->
	Cocher si cette réponse est bonne</p>
	
	<p>Intitulé de la réponse 4 : <br>		
	<!--VTP_hidden_idreponse4-->
		<input type="hidden" name="idreponse4" value="{#idreponse4}">
	<!--/VTP_hidden_idreponse4-->		
	<!--VTP_erreur_reponse4-->
		<span class="err">Il y a une erreur dans l'intitulé de cette réponse. Il est soit vide, soit trop long.</span><br>
	<!--/VTP_erreur_reponse4-->
	<!--VTP_reponse4-->
		<textarea name="reponse4" rows="1" cols="60">{#reponse4}</textarea>
	<!--/VTP_reponse4-->
	<!--VTP_reponsebonne4-->
		<!--VTP_reponsebonne4_checkbox_option-->
		<input type="checkbox" name="{#nom}[]" value="{#value}" {#selected}>
		<!--/VTP_reponsebonne4_checkbox_option-->
	<!--/VTP_reponsebonne4-->
	Cocher si cette réponse est bonne</p>
	
	<p>Intitulé de la réponse 5 : <br>	
	<!--VTP_hidden_idreponse5-->
		<input type="hidden" name="idreponse5" value="{#idreponse5}">
	<!--/VTP_hidden_idreponse5-->		
	<!--VTP_erreur_reponse5-->
		<span class="err">Il y a une erreur dans l'intitulé de cette réponse. Il est soit vide, soit trop long.</span><br>
	<!--/VTP_erreur_reponse5-->
	<!--VTP_reponse5-->
		<textarea name="reponse5" rows="1" cols="60">{#reponse5}</textarea>
	<!--/VTP_reponse5-->
	<!--VTP_reponsebonne5-->
		<!--VTP_reponsebonne5_checkbox_option-->
		<input type="checkbox" name="{#nom}[]" value="{#value}" {#selected}>
		<!--/VTP_reponsebonne5_checkbox_option-->
	<!--/VTP_reponsebonne5-->
	Cocher si cette réponse est bonne</p>
	
    <!--VTP_hidden_iteration-->
       	<input type="hidden" name="iteration" value="{#iteration}">
    <!--/VTP_hidden_iteration-->
    
    Que désirez-vous faire?<br>
    <!--VTP_actions_niv1-->
    	<input type="radio" name="actionquizz" value="{#edition}">{#editiontxt}<br>
    	<input type="radio" name="actionquizz" value="{#suppression}">{#suppressiontxt}<br>
    <!--/VTP_actions_niv1-->
    <!--VTP_creation_niv1-->
        <input type="radio" name="actionquizz" value="{#creation}" checked>{#creationtxt}<br>
    <!--/VTP_creation_niv1-->
	<input type="submit" value="Exécuter cette action"onclick="return confirm('Êtes-vous sûr?')">
	</form>
		  
    <div class='button'><a href="admin_editequizz.php">Retour à la gestion des questions</a></div>
    <div class='button'><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class='button'><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_editionquizz2_niv1-->
 <!--VTP_succes_niv1-->
 <div class="contenu">
    <h1>Succès de l'édition ou correction des questions/réponses du quizz</h1>
 	<!--VTP_editionquizzeffacersucces_niv1-->
		<p>La question a bien été effacée du quizz.</p>	
	<!--/VTP_editionquizzeffacersucces_niv1-->  
	 	<!--VTP_editionquizzcreersucces_niv1-->
		<p>La question a bien été créée avec ses réponses associées.</p>	
	<!--/VTP_editionquizzcreersucces_niv1-->   
	 	<!--VTP_editionquizzcorrigersucces_niv1-->
		<p>La question et ses réponses associées ont bien été corrigées.</p>	
	   <!--/VTP_editionquizzcorrigersucces_niv1-->    
    <div class='button'><a class="norm" href="admin_editequizz.php">Retour à la gestion des questions</a></div>
    <div class='button'><a class="norm" href="admin.php">Retour à l'interface d'administration</a></div>
    <a class="norm" href="delogadm.php"<div class='button'>>Fermer la session d'identification</a></div>
</div>
<!--/VTP_succes_niv1-->


