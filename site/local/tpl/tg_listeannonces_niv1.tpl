<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_listeannonces_niv1-->
	<!--VTP_pasannonces_niv1-->
<div class="contenu">
    <h1>Pas d'annonces disponibles</h1>
    <p>Nous sommes désolés, il n'y a pas d'annonces disponibles actuellement pour créer un personnage. Revenez plus tard, il se peut qu'un MJ ait de nouveau besoin d'un nouveau personnage !</p>
	<a class="norm" href="delogusr.php">Fermer la session d'identification</a>
</div>	
	<!--/VTP_pasannonces_niv1-->
	<!--VTP_candencours-->
<div class="contenu">
    <h1>Candidature en cours</h1>
    <p>Vous avez postulé pour l'annonce {#nomannonce} avec un personnage nommé {#nomperso}.<br>
    Cette candidature est actuellement <span class="imp">"{#statut}"</span> depuis cette date : {#datemodif}.<br>
    	<form action="insc_candencours.php" method="POST">
    		<!--VTP_hidden_idcand-->
    			<input type="hidden" name="idcand" value="{#idcand}">
    		<!--/VTP_hidden_idcand-->
    			<input type="hidden" name="iteration" value="1">
    		<input type="submit" value="Accéder à votre candidature">
        </form>
    </p>
</div>	
	<!--/VTP_candencours-->
<div class="contenu">
    <!--VTP_listeannonces-->
    <h1>Liste des annonces disponibles</h1>
    <p>Veuillez trouver ci-dessous la liste des annonces disponibles afin de créer un personnage.<br>
    Elles sont triées par catégorie.</p>
    <!--/VTP_listeannonces-->
    <!--VTP_annonce-->
    	<h2>{#nomannonce}</h2>
    	<p>
    	<span class="imp">Catégorie : </span>{#categorie}<br>
    	<span class="imp">Places disponibles : </span>{#nbpersos}<br>
    	<span class="imp">Description :</span><br/>
    	{#descannonce}<br/>
    	<!--VTP_lienannonce-->
    	<form action="insc_posercand.php" method="POST">
    		<!--VTP_hidden_idprofil-->
    			<input type="hidden" name="idprofil" value="{#idprofil}">
    		<!--/VTP_hidden_idprofil-->
    		<!--VTP_hidden_iteration-->
    			<input type="hidden" name="iteration" value="{#iteration}">
    		<!--/VTP_hidden_iteration-->
    		<input type="submit" value="Consulter cette annonce et y répondre">
        </form>    		
    	<!--/VTP_lienannonce-->
    	<!--VTP_ancrelogin-->
    		<div class='button'><a href="#login">Se loguer pour répondre à cette annonce</a></div>
    	<!--/VTP_ancrelogin-->
    	</p>
    <!--/VTP_annonce-->
    <br/><br/>
	<div class='button'><a href="accueil.php">Retourner à l'accueil</a></div>
	<div class='button'><a href="delogusr.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_listeannonces_niv1-->
