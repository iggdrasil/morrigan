
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
	<head>
		<title>Projet Morrigan</title>
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8" />		
		<META NAME="Description" CONTENT="Le site du jeu par email Morrigan" />
		<META NAME="Keywords" CONTENT="Morrigan,pbem,jpem,client/serveur,php,jeu" />
		<META NAME="Robots" CONTENT="INDEX,FOLLOW" />
		<META NAME="Document-state" CONTENT="Dynamic" />
		<META NAME="Resource-type" CONTENT="document" />
		<META name="author" content="Arkenlond" />
		
		<script type="text/javascript" src="http://www.peacefrogs.net/morrigan_extra/js/tools.js"></script>
		
		<link href="style.css" rel="stylesheet" type="text/css" title="Classique" />
		<!--VTP_scriptlog -->
		<script language="javascript" type="text/javascript" src="login.js"></script>
		<!--/VTP_scriptlog -->

		<script type="text/javascript">
		<!--
		initTab = ['communication_menu','location_menu','inventory_menu','different_things_menu','root_menu'];
		except = '';
		function showSeveral(tab){
			for (i=0;i<tab.length;i++){if(document.getElementById(tab[i]) && tab[i]!=except){show(tab[i]);}}
		}
		function hideSeveral(tab){
			for (i=0;i<tab.length;i++){if(document.getElementById(tab[i]) && tab[i]!=except){hide(tab[i]);}}
		}
		-->
		</script>
	</head>
	
	<body onload="hideSeveral(initTab)">
		<div class='cadre'>
			<div class='page'>
				<div class='head'>\_O&lt; ~ Morrigan ~ &gt;O_/</div>
				<ul class='menu-top'>
					<li class='menu-selected'>.::<a href='accueil.php'>Accueil</a>::.</li>
					<li>.::<a href='insc_listeannonces.php'>Inscription</a>::.</li>
					<li>.::<a href='#'>Règles</a>::.</li>
					<li>.::<a href='#'>Monde</a>::.</li>
					<li>.::<a href='http://www.peacefrogs.net/morrigan-dev/base'>Jouer</a>::.</li>
					<li>.::<a href='help'>Forum</a>::.</li>
				</ul>
				<div class="principal">
					<div class='menu-left'>
						<!--VTP_menu-left-->
							{#varmenu-left}
						<!--/VTP_menu-left-->
					</div>
		
					<script type="text/javascript">
					<!--
						hideSeveral(initTab);
					-->
					</script>

					<div class='contenu-right'>				
						<!--VTP_contenupage-->
							{#varcontenupage}
						<!--/VTP_contenupage-->
					</div>
					<div class='spacer'>&nbsp;</div>	
				</div>
				<div class='tail'>
				<!--VTP_pagecourante2-->
					---&nbsp;{#varpagecourante2}&nbsp;--- Copyright : Blue Team Corporation 2003-2009 ---
					<!--/VTP_pagecourante2-->
				</div>
			</div>
		</div>
	</body>
</html>
