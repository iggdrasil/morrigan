<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traiteprofil_niv1-->
<div class="contenu">
   <!--VTP_traiteprofil_niv2-->
		<h1>Traitement du profil</h1>      
      {#vartraiteprofil_niv2}
   <!--/VTP_traiteprofil_niv2-->
   <!--VTP_editeprofil_niv2-->
   	<h1>Edition du profil</h1>
		{#varediteprofil_niv2}
   <!--/VTP_editeprofil_niv2-->
	<!--VTP_succes-->
		<div class="t1">Succès de l'opération</div>   
	   <!--VTP_succescreation-->
	   	<p>Le profil a été créé et pris en compte. Le MJ Inscription va le relire avant de le valider.</p>
	   <!--/VTP_succescreation-->
	   <!--VTP_succesedition-->
	   	<p>Le profil "{#nomprofil}" a été édité. Le MJ Inscription va le relire avant de le valider.</p>
	   <!--/VTP_succesedition-->
	   <!--VTP_succesdemande-->
	   	<p>Votre demande a bien été prise en compte. Le MJ Inscription va l'étudier.</p>
	   <!--/VTP_succesdemande-->
	   <!--VTP_successtatut-->
	   	<p>Le statut du profil a bien été passé à "{#nouvstatut}".</p>
	   <!--/VTP_successtatut-->
	   <!--VTP_succeseffacer-->
	   	<p>Le profil "{#nomprofil}" a bien été effacé.</p>
	   <!--/VTP_succeseffacer-->
	   <!--VTP_succesrefuser-->
	   	<p>La demande a bien été refusée.</p>
		<!--/VTP_succesrefuser-->
	<!--/VTP_succes-->
	<div class="button"><a href="admin_profil.php">Retour à la gestion des profils</a></div>
	<div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
	<div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_traiteprofil_niv1-->
