<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_editionprofil_niv2-->
    <form action="admin_traiteprofil.php" method="POST">
        <!--VTP_hidden_iteration-->
            <input type="hidden" name="iteration" value="{#iteration}">
        <!--/VTP_hidden_iteration-->
        <!--VTP_hidden_actionprofil-->
            <input type="hidden" name="actionprofil" value="{#actionprofil}">
        <!--/VTP_hidden_actionprofil-->
        <!--VTP_hidden_idprofil-->
            <input type="hidden" name="idprofil" value="{#idprofil}">
        <!--/VTP_hidden_idprofil-->
        <p><span class="imp">Nom du profil : </span></p>
        <!--VTP_erreur_nomprofil-->
            <p><span class="erreur">Le nom que vous avez entré pour ce profil est soit trop long, soit déjà utilisé</span></p>
        <!--/VTP_erreur_nomprofil-->
        <!--VTP_nomprofil-->
            <input type="text" name="nomprofil" value="{#nomprofil}" size="80" maxlength="100">
        <!--/VTP_nomprofil-->
        <!--VTP_hidden_nomprofil-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="nomprofil" value="{#nomprofil}">
        <!--/VTP_hidden_nomprofil-->
        <p><span class="imp">Nombre d'inscriptions possibles : </span>
        <!--VTP_erreur_nbpersos-->
            <p><span class="erreur">Le nombre d'inscriptions de personnage possible doit être égal à -1, pour un nombre d'inscription indéfini, ou un nombre supérieur à 0.</span></p>
        <!--/VTP_erreur_nbpersos-->
        <!--VTP_nbpersos-->
            <input type="text" name="nbpersos" value="{#nbpersos}" size="2" maxlength="3">
        <!--/VTP_nbpersos--></p>
        <!--VTP_hidden_nbpersos-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="nbpersos" value="{#nbpersos}">
        <!--/VTP_hidden_nbpersos-->
        <p><span class="imp">Catégorie : </span>
        <!--VTP_categorie-->
            <select size=1 name="{#name}[]">
                <!--VTP_categorie_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_categorie_select_option-->
            </select>
        <!--/VTP_categorie-->
        <!--VTP_hidden_categorie-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="categorie" value="{#categorie}">
        <!--/VTP_hidden_categorie-->
        </p>
        <p><scan class="imp">Auteur du profil : </scan>
            <!--VTP_auteurprofil-->
                <select size=1 name="{#name}[]">
                
                <!--VTP_auteurprofil_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_auteurprofil_select_option-->
                </select>
            <!--/VTP_auteurprofil-->
            <!--VTP_hidden_auteurprofil-->
        		<p>Pas de correction nécessaire</p>
        		<input type="hidden" name="auteurprofil" value="{#auteurprofil}">
        	<!--/VTP_hidden_auteurprofil-->
            <!--VTP_auteurprofil2-->
            	{#auteur}
            <!--/VTP_auteurprofil2-->
        </p>
        <p><span class="imp">Statut du profil : </span>
            <!--VTP_statutprofil-->
                <select size=1 name="{#name}[]">
                <!--VTP_statutprofil_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_statutprofil_select_option-->
                </select>
            <!--/VTP_statutprofil-->
            <!--VTP_statutprofil2-->
            	{#statut}
            <!--/VTP_statutprofil2-->
            <!--VTP_hidden_statutprofil-->
        		<p>Pas de correction nécessaire</p>
        		<input type="hidden" name="statutprofil" value="{#statutprofil}">
        	<!--/VTP_hidden_statutprofil-->
        </p>
        <table class="items">
            <thead>
                <tr>
                    <th>Race</th>
                    <th>Classe</th>
                    <th>Sexe</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td style="vertical-align:top;text-align:left;">
                <!--VTP_erreur_racespossiblesed-->
                	<p><span class="erreur">Sélectionnez au moins une race dans ce tableau</span></p>
                <!--/VTP_erreur_racespossiblesed-->                
                <!--VTP_racespossiblesed-->
                    <select multiple size=5 name="{#name}[]">
                    <!--VTP_racespossiblesed_select_option-->
                        <option value='{#value}' {#selected}>{#nom}</option>
                    <!--/VTP_racespossiblesed_select_option-->
                    </select>
                <!--/VTP_racespossiblesed-->
                <!--VTP_hidden_racespossiblesed-->
                	<input type="hidden" name="racespossiblesed[]" value="{#racespossiblesed}">
                <!--/VTP_hidden_racespossiblesed-->
                
                </td>
                <td style="vertical-align:top;text-align:left;">
                <!--VTP_erreur_classespossiblesed-->
                	<p><span class="erreur">Sélectionnez au moins une classe dans ce tableau</span></p>
                <!--/VTP_erreur_classespossiblesed-->   
                <!--VTP_classespossiblesed-->
                    <select multiple size=5 name="{#name}[]">
                    <!--VTP_classespossiblesed_select_option-->
                        <option value='{#value}' {#selected}>{#nom}</option>
                    <!--/VTP_classespossiblesed_select_option-->
                    </select>
                <!--/VTP_classespossiblesed-->
                <!--VTP_hidden_classespossiblesed-->
                	<input type="hidden" name="classespossiblesed[]" value="{#classespossiblesed}">
                <!--/VTP_hidden_classespossiblesed-->
                </td>
                <td style="vertical-align:top;text-align:left;">
                <!--VTP_erreur_sexespossiblesed-->
                	<p><span class="erreur">Sélectionnez au moins un sexe dans ce tableau</span></p>
                <!--/VTP_erreur_sexespossiblesed-->   
                <!--VTP_sexespossiblesed-->
                    <select multiple size=5 name="{#name}[]">
                    <!--VTP_sexespossiblesed_select_option-->
                        <option value='{#value}' {#selected}>{#nom}</option>
                    <!--/VTP_sexespossiblesed_select_option-->
                    </select>
                <!--/VTP_sexespossiblesed-->
                <!--VTP_hidden_sexespossiblesed-->
                	<input type="hidden" name="sexespossiblesed[]" value="{#sexespossiblesed}">
                <!--/VTP_hidden_sexespossiblesed-->
                </td>
            </tr>
            </tbody>
        </table>
        <p><span class="imp">Description du profil :</span><br></p>
        <!--VTP_erreur_descriptionprofil-->
            <p><span class="erreur">La description du profil est trop longue.</span></p>
        <!--/VTP_erreur_descriptionprofil-->
        <!--VTP_descriptionprofil-->
            <textarea name="descriptionprofil" rows="10" cols="80">{#descriptionprofil}</textarea>
        <!--/VTP_descriptionprofil-->
        <!--VTP_hidden_descriptionprofil-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="descriptionprofil" value="{#descriptionprofil}">
        <!--/VTP_hidden_descriptionprofil-->        
        <br>
        
        <p><span class="imp">Discussion à propos du profil :</span><br>
			{#debatprofil}</p>
        <!--VTP_erreur_debat-->
            <p><span class="erreur">Le commentaire du profil est trop long.</span></p>
        <!--/VTP_erreur_debat-->
        <!--VTP_debat-->
            <textarea name="debat" rows="10" cols="80">{#debat}</textarea>
        <!--/VTP_debat-->
        <!--VTP_hidden_debat-->
        	<p>Pas de correction nécessaire</p>
        	<input type="hidden" name="debat" value="{#debat}">
        <!--/VTP_hidden_debat-->
        <br>
        <p><span class="imp">Historique du profil :</span><br></p>
        <!--VTP_historiqueprofil-->
        	<p>{#historiqueprofil}</p>
        <!--/VTP_historiqueprofil-->
		<input type="submit" value="{#action}" onclick="return confirm('Confirmer l\'édition ou la création ?')">
    </form>
<!--/VTP_editionprofil_niv2-->
