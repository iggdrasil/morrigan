<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_loginusr_niv1-->
<div class="contenu">
    <h1>Login de l'utilisateur</h1>
    <a name="login"></a>
    <p>Afin de déposer une candidature, vous devriez vous loguer.</p>
    <form action="loginusr.php" method="POST">
        <!--VTP_erreur_login-->
        <p><span class="erreur">Vous vous êtes trompé dans les coordonnées transmises. Le pseudonyme et/ou le mot de passe est faux ou ne correspond pas.</span></p>
        <!--/VTP_erreur_login-->
        <!--VTP_login-->
        Pseudonyme : <input type="text" name="login" value="{#login}">
        <!--/VTP_login-->
        <!--VTP_password-->
        Mot de passe : <input type="password" name="password" value="{#password}">
        <!--/VTP_password-->
    	<!--VTP_hidden_referer-->
    	<input type="hidden" name="referer" value="{#referer}"> 
    	<!--/VTP_hidden_referer-->
        <input type="submit" value="Se loguer">

    </form><br>
    Si vous n'êtes pas encore enregistré sur ce site, passez par la page d'<a href="enregistrement.php">enregistrement</a>.
</div>
<!--/VTP_loginusr_niv1-->
