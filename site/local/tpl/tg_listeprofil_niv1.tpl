<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_listeprofil_niv1-->
<div class="contenu">
    <h1>Liste des profils</h1>
<!--VTP_liste_req_action-->
    <h2>Liste des profils requérant une action de votre part</h2>
    <p>Ces profils requièrent une action de votre part : relecture, mise en ligne ou hors ligne, édition...</p>
    <table class="items">
        <thead><tr>
            <th>Auteur&nbsp;(<a href="admin_profil.php?criteretri=1" class="norm">^</a>)</th>
            <th>
                Statut&nbsp;(<a href="admin_profil.php?criteretri=2" class="norm">^</a>)
            </th>
            <th>Catégorie&nbsp;(<a href="admin_profil.php?criteretri=3" class="norm">^</a>)</th>
            <th>Nom</th>
            <th>Candidatures<br> possibles</th>
            <th>&nbsp;</th>
        </tr></thead>
        <tbody>
        <!--VTP_profil_req_action-->
        <tr>
            <td>{#auteur}</td><td>{#statut}</td><td>{#categorie}</td><td>{#nom}</td><td>{#nbpersos}</td>
            <td>
                <form action="admin_traiteprofil.php" method="POST">
                    <!--VTP_hidden_idprofil1-->
                        <input type="hidden" name="idprofil" value="{#idprofil1}">
                    <!--/VTP_hidden_idprofil1-->
                    <!--VTP_hidden_actionprofil1-->
                        <input type="hidden" name="actionprofil" value="{#actionprofil1}">
                    <!--/VTP_hidden_actionprofil1-->
                    <input type="submit" value="Afficher ce profil">
                </form>
            </td>
        </tr>
        <!--/VTP_profil_req_action-->
        </tbody>
    </table>
<br/><br/>
<!--/VTP_liste_req_action-->

<!--VTP_liste_auteur-->
    <h2>Liste des profils dont vous êtes l'auteur</h2>
    <p>Ces profils sont les "vôtres".</p>
    <table class="items">
        <thead><tr>
            <th>Auteur</th>
            <th>Statut&nbsp;(<a href="admin_profil.php?criteretri=2" class="norm">^</a>)</th>
            <th>Catégorie&nbsp;(<a href="admin_profil.php?criteretri=3" class="norm">^</a>)</th>
            <th>Nom</th>
            <th>Candidatures<br> possibles</th>
            <th>&nbsp;</th>
        </tr></thead>
        <tbody>
        <!--VTP_profil_auteur-->
        <tr>
            <td>{#auteur}</td><td>{#statut}</td><td>{#categorie}</td><td>{#nom}</td><td>{#nbpersos}</td>
            <td>
                <form action="admin_traiteprofil.php" method="POST">
                    <!--VTP_hidden_idprofil2-->
                        <input type="hidden" name="idprofil" value="{#idprofil2}">
                    <!--/VTP_hidden_idprofil2-->
                    <!--VTP_hidden_actionprofil2-->
                        <input type="hidden" name="actionprofil" value="{#actionprofil2}">
                    <!--/VTP_hidden_actionprofil2-->
                    <input type="submit" value="Afficher ce profil">
                </form>
            </td>
        </tr>
        <!--/VTP_profil_auteur-->
        </tbody>
    </table>
    <br><br>
<!--/VTP_liste_auteur-->
<!--VTP_liste_reste-->
    <h2>Liste des autres profils</h2>
    <p>Ces profils sont ceux qui restent.</p>
    <table class="items">
        <thead><tr>
            <th>Auteur&nbsp;(<a href="admin_profil.php?criteretri=1" class="norm">^</a>)</th>
            <th>Statut&nbsp;(<a href="admin_profil.php?criteretri=2" class="norm">^</a>)</th>
            <th>Catégorie&nbsp;(<a href="admin_profil.php?criteretri=3" class="norm">^</a>)</th>
            <th>Nom</th>
            <th>Candidatures<br> possibles</th>
            <th>&nbsp;</th>
        </tr></thead>
        <tbody>
        <!--VTP_profil_reste-->
        <tr>
            <td>{#auteur}</td><td>{#statut}</td><td>{#categorie}</td><td>{#nom}</td><td>{#nbpersos}</td>
            <td>
                <form action="admin_traiteprofil.php" method="POST">
                    <!--VTP_hidden_idprofil3-->
                        <input type="hidden" name="idprofil" value="{#idprofil3}">
                    <!--/VTP_hidden_idprofil3-->
                    <!--VTP_hidden_actionprofil3-->
                        <input type="hidden" name="actionprofil" value="{#actionprofil3}">
                    <!--/VTP_hidden_actionprofil3-->
                    <input type="submit" value="Afficher ce profil">
                </form>
            </td>
        </tr>
        <!--/VTP_profil_reste-->
        </tbody>
    </table>
<!--/VTP_liste_reste-->
<br/><br/>
<!--VTP_creer_profil-->
    <h2>Créer un profil</h2>
    Si vous désirez créer un tout nouveau profil, cliquez ci-dessous.
    <form action="admin_traiteprofil.php" method="POST">
        <!--VTP_hidden_action4-->
            <input type="hidden" name="actionprofil" value="{#action4}">
        <!--/VTP_hidden_action4-->
        <!--VTP_hidden_iteration-->
           <input type="hidden" name="iteration" value="{#iteration}">
        <!--/VTP_hidden_iteration-->

        <input type="submit" value="Créer un nouveau profil">    </form>
<!--/VTP_creer_profil-->

<div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
<div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_listeprofil_niv1-->
