<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_listenews_niv2-->
	<!--VTP_pas_news-->
		Il n'y a pas de news à afficher. Vous pouvez cependant en créer une !
	<!--/VTP_pas_news-->
	<!--VTP_form_list_news-->
		<h2>Edition des news existantes</h2>
		Voici les news que vous pouvez consulter pour les corriger.<br/><br/>
		<form method="POST" action="admin_gestionnews.php">
		<!--VTP_idnews-->
            <select size=1 name="{#name}">
                <!--VTP_idnews_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_idnews_select_option-->
            </select><br/>
        <!--/VTP_idnews-->
        <!--VTP_pasdenews-->
        	<span class="erreur">Aucune des news existantes ne peut être traitée par vos soins.</span><br/>
        <!--/VTP_pasdenews-->
		<br/>
		<!--VTP_submit-->
			Que souhaitez-vous faire de votre sélection?<br/>
			<!--VTP_actionnews-->
				<!--VTP_actionnews_radio_option-->
				<input type="radio" value="{#value}" name="actionnews" {#selected}>{#nom}<br/>
				<!--/VTP_actionnews_radio_option-->
			<!--/VTP_actionnews-->
			<br/>
			<!--VTP_hidden_iteration-->
           		<input type="hidden" name="iteration" value="{#iteration}">
       		<!--/VTP_hidden_iteration-->
			<input type="submit" value="Exécuter cette action"  onclick="return confirm('Êtes-vous sûr de vouloir effectuer cette action?');">
		<!--/VTP_submit-->
		</form>
	<!--/VTP_form_list_news-->
	<!--VTP_form_creer_news-->
		<h2>Création d'une news</h2>
		Vous avez la possibilité de créer une nouvelle news, histoire de faire savoir au monde entier que vous avez du nouveau à leur offrir.<br/><br/>
		<form method="POST" action="admin_gestionnews.php">
       		<!--VTP_hidden_iteration1-->
           		<input type="hidden" name="iteration1" value="{#iteration1}">
       		<!--/VTP_hidden_iteration1-->
        	<!--VTP_hidden_actionnews-->
           		<input type="hidden" name="actionnews" value="{#actionnews}">
       		<!--/VTP_hidden_actionnews-->      		
			<input type="submit" value="Créer une news"  onclick="return confirm('Êtes-vous sûr de vouloir créer une news?');">
		</form>		
	<!--/VTP_form_creer_news-->
<!--/VTP_listenews_niv2-->
