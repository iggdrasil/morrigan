<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_affichecandidature_niv1-->
<div class="contenu">
	<h1>Affichage d'une candidature</h1>
	<div class="button"><a href="#actions">Aller directement au menu des actions</a></div>
	<h2>Informations générales</h2>
	<p><span class="imp">Nom du candidat :&nbsp;</span>{#joueur}</p>
	<p><span class="imp">Nom du profil de référence :&nbsp;</span>{#nomprofil}</p>
	<p><span onclick = "showHide ('descprofil')"><span class="imp">Description du profil de référence</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="descprofil">{#descprofil}</span></p>	
	<p><span class="imp">Nom du MJ auteur de ce profil :&nbsp;</span>{#nommj}</p>
	<p><span class="imp">Statut de la candidature :&nbsp;</span>{#statut}</p>
	<p><span class="imp">Date de la dernière modification de cette candidature :&nbsp;</span>{#datemodification} (Année, Mois, Jour)</p>	
	<h2>Informations à propos du personnage candidat</h2>
	<p><span class="imp">Nom du personnage :&nbsp;</span>{#nomperso}</p>
	<p>
		<span class="imp">Race choisie :&nbsp;</span>{#race}&nbsp;
		<!--VTP_racefermee-->
			<span class="err">(Attention, cette race a été fermée à l'inscription depuis le dépôt de cette candidature)</span>
		<!--/VTP_racefermee-->
	</p>
	<p>
		<span class="imp">Classe choisie :&nbsp;</span>{#classe}&nbsp;
		<!--VTP_classefermee-->
			<span class="err">(Attention, cette classe a été fermée à l'inscription depuis le dépôt de cette candidature)</span>
		<!--/VTP_classefermee-->
	</p>
	<p>
		<span class="imp">Sexe choisi :&nbsp;</span>{#sexe}&nbsp;
		<!--VTP_sexeferme-->
			<span class="err">(Attention, ce sexe a été fermé à l'inscription depuis le dépôt de cette candidature)</span>
		<!--/VTP_sexeferme-->
	</p>
	<p><span onclick = "showHide ('description')"><span class="imp">Description du personnage</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="description">{#description}</span></p>
	<p><span onclick = "showHide ('background')"><span class="imp">Background du personnage</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="background">{#background}</span></p>
	<p><span onclick = "showHide ('buts')"><span class="imp">Buts du personnage dans le jeu</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="buts">{#buts}</span></p>
	<h2>Discussions et commentaires à propos de la candidature</h2>	
	<p><span onclick = "showHide ('discussionusrmj')"><span class="imp">Discussion entre le MJ Inscription et le joueur candidat</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id ="discussionusrmj">{#debatusrmj}</span></p>
	<p><span onclick = "showHide ('discussionmj')"><span class="imp">Discussion entre le MJ Inscription et le MJ auteur du profil</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="discussionmj">{#debatmj}</span></p>
	<p><span onclick = "showHide ('comment')"><span class="imp">Commentaire général à propos de la candidature</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id="comment">{#commentaire}</span></p>
	<p><span onclick = "showHide ('histo')"><span class="imp">Historique de la candidature</span> (cliquer pour afficher/cacher):&nbsp;</span><br><span id = "histo">{#historique}</span></p>
</div>
<!--/VTP_affichecandidature_niv1-->
