<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_news_niv1-->
	<div class='contenu'>
		<h1>Gestion des news</h1>
		<!--VTP_listenews_niv1-->
			{#contenuliste}
		<!--/VTP_listenews_niv1-->
		<!--VTP_editenews_niv1-->
			{#contenuedite}
		<!--/VTP_editenews_niv1-->
		<br/>
		<!--VTP_succeseffacer-->
			La news a bien été effacée.<br/>
		<!--/VTP_succeseffacer-->
		<!--VTP_succesediter-->
			La news a bien été éditée ou créée.<br/>
		<!--/VTP_succesediter-->	
		<!--VTP_echeceffacer-->
			La news n'a pas pu être effacée. Une erreur inattendue est survenue.<br/>
		<!--/VTP_echeceffacer-->
		<!--VTP_echecediter-->
			La news n'a pas pu être éditée ou créée. Une erreur inattendue est survenue.<br/>
		<!--/VTP_echecediter-->	
		<div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
		<div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
	</div>
<!--/VTP_news_niv1-->
