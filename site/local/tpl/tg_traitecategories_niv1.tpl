<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traitecategories_niv1-->
<div class="contenu">
    <h1>Edition et correction des categories des profils</h1>
	Que souhaitez-vous faire?<br>
	
	<form action="admin_editecategories2.php" method="POST">
	    
    <!--VTP_pasdecategorie-->
    <p>Il n'y a pas encore de categorie à éditer</p>
    <!--/VTP_pasdecategorie-->    

    <!--VTP_listecategories-->
		<input type="radio" name="actioncategorie" value="{#action}">Choisir une catégorie à éditer :<br>
		<!--VTP_idcategorie-->
        	<select name="{#name}">
            	<!--VTP_idcategorie_select_option-->
                	<option value="{#value}" {#selected}>{#nom}</option>
           	 	<!--/VTP_idcategorie_select_option-->
        	</select><br>
    	<!--/VTP_idcategorie-->

    <!--/VTP_listecategories--> 
    
    <!--VTP_creercategorie-->
		<input type="radio" name="actioncategorie" value="{#action}">Créer une nouvelle categorie<br>
    <!--/VTP_creercategorie--> 
    
    <!--VTP_hidden_iteration-->
       	<input type="hidden" name="iteration" value="{#iteration}">
    <!--/VTP_hidden_iteration-->
    	
    <input type="submit" value="Editer ou créer une catégorie"onclick="return confirm('Êtes-vous sûr?')">
    </form>       
	  
    <div class="button"><a href="admin_editecategories.php">Retour à la gestion des catégories</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_traitecategories_niv1-->

