<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_listeouvertures_niv1-->
<div class="contenu">
    <h1>Liste des races/classes/sexes pour leur ouverture ou fermeture à l'inscription</h1>
	<p>Ouvrez ou fermez individuellement les races, classes ou sexes à l'inscription. L'effet est global et impacte tous les profils.</p>
	<h2>Gestion des races</h2>	
	<table class="items">
		<caption>Gestion des races</caption>
		<thead>
			<th>Nom de la race</th>
			<th>Ouverture</th>
		</thead>
	    <tbody>
	    <!--VTP_ligne_race-->
	    <tr>
	        <td style="width: 200px;">
	        {#nomrace}
	        </td>
	       	<td>
	       		<form action ="admin_traiteouvertures.php" method="POST">
	       			<!--VTP_hidden_iterationrace-->
	       				<input type="hidden" name="iterationrace" value="{#iterationrace}">
	       			<!--/VTP_hidden_iterationrace-->
	       			<!--VTP_hidden_idrace-->
	       				<input type="hidden" name="idrace" value="{#idrace}">
	       			<!--/VTP_hidden_idrace-->
	       			<!--VTP_actionrace-->
	       				<!--VTP_actionrace_radio_option-->
	       				<input type="radio" name="actionrace" value="{#value}" {#selected}>{#nom}
	       				<!--/VTP_actionrace_radio_option-->
	       			<!--/VTP_actionrace-->	       			
	       			<input type="submit" value="Modifier" onclick="return confirm('Êtes-vous sûr?')">
	       		</form>
	        </td>
	    </tr>
	    <!--/VTP_ligne_race-->
	    </tbody>
	</table>

	<h2>Gestion des classes</h2>	
	<table class="items">
		<caption>Gestion des classes</caption>
		<thead>
			<th>Nom de la classe</th>
			<th>Ouverture</th>
		</thead>
	    <tbody>
	    <!--VTP_ligne_classe-->
	    <tr>
	        <td style="width: 200px;">
	        {#nomclasse}
	        </td>
	       	<td>
	       		<form action ="admin_traiteouvertures.php" method="POST">
	       			<!--VTP_hidden_iterationclasse-->
	       				<input type="hidden" name="iterationclasse" value="{#iterationclasse}">
	       			<!--/VTP_hidden_iterationclasse-->
	       			<!--VTP_hidden_idclasse-->
	       				<input type="hidden" name="idclasse" value="{#idclasse}">
	       			<!--/VTP_hidden_idclasse-->
	       			<!--VTP_actionclasse-->
	       				<!--VTP_actionclasse_radio_option-->
	       				<input type="radio" name="actionclasse" value="{#value}" {#selected}>{#nom}
	       				<!--/VTP_actionclasse_radio_option-->
	       			<!--/VTP_actionclasse-->	       			
	       			<input type="submit" value="Modifier" onclick="return confirm('Êtes-vous sûr?')">
	       		</form>
	        </td>
	    </tr>
	    <!--/VTP_ligne_classe-->
	    </tbody>
	</table>	

	<h2>Gestion des sexes</h2>	
	<table class="items">
		<caption>Gestion des sexes</caption>
		<thead>
			<th>Nom du sexe</th>
			<th>Ouverture</th>
		</thead>
	    <tbody>
	    <!--VTP_ligne_sexe-->
	    <tr>
	        <td style="width: 200px;">
	        {#nomsexe}
	        </td>
	       	<td>
	       		<form action ="admin_traiteouvertures.php" method="POST">
	       			<!--VTP_hidden_iterationsexe-->
	       				<input type="hidden" name="iterationsexe" value="{#iterationsexe}">
	       			<!--/VTP_hidden_iterationsexe-->
	       			<!--VTP_hidden_idsexe-->
	       				<input type="hidden" name="idsexe" value="{#idsexe}">
	       			<!--/VTP_hidden_idsexe-->
	       			<!--VTP_actionsexe-->
	       				<!--VTP_actionsexe_radio_option-->
	       				<input type="radio" name="actionsexe" value="{#value}" {#selected}>{#nom}
	       				<!--/VTP_actionsexe_radio_option-->
	       			<!--/VTP_actionsexe-->	       			
	       			<input type="submit" value="Modifier" onclick="return confirm('Êtes-vous sûr?')">
	       		</form>
	        </td>
	    </tr>
	    <!--/VTP_ligne_sexe-->
	    </tbody>
	</table>	
	<br><br>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_listeouvertures_niv1-->
