<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_boite-login-->
<div class='menu-left-contenu'>
	<!--VTP_logueur-->
	<form action="loginusr.php" method="POST">
		<!--VTP_erreur_login-->
		<p><span class="erreur">Vous vous êtes trompé dans les coordonnées transmises. Le pseudonyme et/ou le mot de passe est faux ou ne correspond pas.</span></p>
		<!--/VTP_erreur_login-->
		<!--VTP_login-->
		Pseudonyme : <input type="text" name="login" value="{#login}" /><br/>
		<!--/VTP_login-->
		<!--VTP_password-->
		Mot de passe : <input type="password" name="password" value="{#password}" /><br/>
		<!--/VTP_password-->
		<!--VTP_hidden_referer-->
		<input type="hidden" name="referer" value="{#referer}" /> 
		<!--/VTP_hidden_referer-->
		<input type="submit" value="Se loguer" />
	</form><br/>
	Si vous n'êtes pas encore enregistré sur ce site, passez par la page d'<a href="enregistrement.php">enregistrement</a>.
	<!--/VTP_logueur-->
	
	<!--VTP_logged-->
	<p>Vous êtes connecté en tant que : <div class="nom">{#nomjoueur}</div><br/> Bon voyage !</p>
	<div class="button"><a href="delogusr.php">Déconnexion</a></div>	
	<!--/VTP_logged-->
</div>
<!--/VTP_boite-login-->
