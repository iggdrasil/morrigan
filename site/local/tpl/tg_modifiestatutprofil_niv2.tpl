<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_modifiestatutprofil_niv2-->
    <!--VTP_modifiestatutprofil_ok-->
    <h1>Statut modifié avec succès</h1>    
    <p>Le statut du profil a été modifié.<p>
    <!--/VTP_modifiestatutprofil_ok-->
<!--/VTP_modifiestatutprofil_niv2-->
