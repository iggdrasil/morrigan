<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_posercand_niv1-->
	<!--VTP_rappel_annonce_niv1-->
    	<div class="contenu">
    		<h1>Rappel de l'annonce</h1>
    		<p>
    		<span class="imp">Nom de l'annonce : </span>{#nomannonce}<br>
    		<span class="imp">Catégorie : </span>{#categorie}<br>
    		<span class="imp">Places disponibles : </span>{#nbpersos}<br>
    		<span class="imp">Description :</span><br>
    		{#descannonce}</p>
    	</div>
	<!--/VTP_rappel_annonce_niv1-->
	<!--VTP_formulaire_niv1-->
    	<div class="contenu">
    	<h1>Dépôt de la candidature</h1>		
		Merci de remplir les champs suivants afin de déposer votre candidature.
		<form action="insc_posercand.php" method="POST">
		   <!--VTP_hidden_idprofil-->
    		<input type="hidden" name="idprofil" value="{#idprofil}">
    	<!--/VTP_hidden_idprofil-->
		<!--VTP_erreur_nomperso-->
			<p><span class="erreur">Le nom que vous avez choisi ne convient pas. Il est peut-être déjà utilisé, ou trop long.</span></p>
		<!--/VTP_erreur_nomperso-->
		<!--VTP_nomperso-->
			Nom du personnage :
			<input type="text" name="nomperso" value="{#nomperso}" size="50" maxlength="100"><br>
		<!--/VTP_nomperso-->
		<!--VTP_hidden_nomperso-->
			<input type="hidden" name="nomperso" value="{#nomperso}">
		<!--/VTP_hidden_nomperso-->

		<!--VTP_erreur_race_ouverte-->
			<p><span class="erreur">La race que vous avez choisie ne convient pas. Sélectionnez-en une autre.</span></p>
		<!--/VTP_erreur_race_ouverte-->
		<!--VTP_race_ouverte-->
            Race du personnage :<br>
            <select size=5 name="{#name}[]">
                <!--VTP_race_ouverte_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_race_ouverte_select_option-->
            </select><br>
		<!--/VTP_race_ouverte-->
		<!--VTP_hidden_race_ouverte-->
			<input type="hidden" name="race" value="{#race}[]">
		<!--/VTP_hidden_race_ouverte-->		
		
		<!--VTP_erreur_classe_ouverte-->
			<p><span class="erreur">La classe que vous avez choisie ne convient pas. Sélectionnez-en une autre.</span></p>
		<!--/VTP_erreur_classe_ouverte-->
		<!--VTP_classe_ouverte-->
            Classe du personnage :<br>
            <select size=5 name="{#name}[]">
                <!--VTP_classe_ouverte_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_classe_ouverte_select_option-->
            </select><br>
		<!--/VTP_classe_ouverte-->
		<!--VTP_hidden_classe_ouverte-->
			<input type="hidden" name="classe" value="{#classe}[]">
		<!--/VTP_hidden_classe_ouverte-->		
		
		<!--VTP_erreur_sexe_ouvert-->
			<p><span class="erreur">Le sexe que vous avez choisi ne convient pas. Sélectionnez-en un autre.</span></p>
		<!--/VTP_erreur_sexe_ouvert-->
		<!--VTP_sexe_ouvert-->
            Sexe du personnage :<br>
            <select size=3 name="{#name}[]">
                <!--VTP_sexe_ouvert_select_option-->
                    <option value='{#value}' {#selected}>{#nom}</option>
                <!--/VTP_sexe_ouvert_select_option-->
            </select><br>
		<!--/VTP_sexe_ouvert-->
		<!--VTP_hidden_sexe_ouvert-->
			<input type="hidden" name="sexe" value="{#sexe}[]">
		<!--/VTP_hidden_sexe_ouvert-->
		
		<!--VTP_erreur_description-->
			<p><span class="erreur">La description que vous avez entrée pour votre personnage ne convient pas. Elle peut être trop longue. Il faut absolument compléter ce champ.</span></p>
		<!--/VTP_erreur_description-->
		<!--VTP_description-->
            Entrez la description du personnage :<br>
            <textarea name="description" rows="10" cols="80">{#description}</textarea></br>
		<!--/VTP_description-->
		<!--VTP_hidden_description-->
			<input type="hidden" name="description" value="{#description}">
		<!--/VTP_hidden_description-->
		
		<!--VTP_erreur_background-->
			<p><span class="erreur">Le background que vous avez entré pour votre personnage ne convient pas. Il peut être trop long. Il faut absolument compléter ce champ.</span></p>
		<!--/VTP_erreur_background-->
		<!--VTP_background-->
            Entrez le background du personnage :<br>
            <textarea name="background" rows="10" cols="80">{#background}</textarea><br>
		<!--/VTP_background-->
		<!--VTP_hidden_background-->
			<input type="hidden" name="background" value="{#background}">
		<!--/VTP_hidden_background-->
		
		<!--VTP_erreur_buts-->
			<p><span class="erreur">Les buts que vous avez entrés pour votre personnage ne conviennent pas. Ils peuvent être trop longs. Il faut absolument compléter ce champ.</span></p>
		<!--/VTP_erreur_buts-->
		<!--VTP_buts-->
            Entrez les buts du personnage :<br>
            <textarea name="buts" rows="10" cols="80">{#buts}</textarea><br>
		<!--/VTP_buts-->
		<!--VTP_hidden_buts-->
			<input type="hidden" name="buts" value="{#buts}">
		<!--/VTP_hidden_buts-->
		
		<!--VTP_erreur_debatusrmj-->
			<p><span class="erreur">Le commentaire que vous avez entré pour votre personnage ne convient pas. Il peut être trop long. Il faut absolument compléter ce champ.</span></p>
		<!--/VTP_erreur_debatusrmj-->
		<!--VTP_debatusrmj-->
			Vous pouvez entrer un commentaire à l'attention du MJ Inscription :<br>
            <textarea name="debatusrmj" rows="10" cols="80">{#debatusrmj}</textarea><br>
		<!--/VTP_debatusrmj-->
		<!--VTP_hidden_debatusrmj-->
			<input type="hidden" name="debatusrmj" value="{#debatusrmj}">
		<!--/VTP_hidden_debatusrmj-->
		
		<!--VTP_hidden_iteration-->
			<input type="hidden" name="iteration" value="{#iteration}">
		<!--/VTP_hidden_iteration-->
			<input type="submit" value="Poser sa candidature" onclick="return confirm('Confirmer le dépôt de cette candidature ?')">
		</form>	
		<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
		<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
		</div>	
	<!--/VTP_formulaire_niv1-->
	<!--VTP_okcandidature_niv1-->
    	<div class="contenu">
    		<h1>Candidature déposée avec succès</h1>
    		<p>Votre candidature a été enregistrée avec succès. Le MJ Inscription va la consulter dès qu'il le pourra.</p>
    		<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
    		<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
    	</div>	
    <!--/VTP_okcandidature_niv1-->
	<!--VTP_erreurcandidature_niv1-->    
		<p>Une erreur est survenue lors du dépôt de votre candidature. Votre candidature n'a pas pu être prise en compte. Veuillez en aviser le MJ Inscription.</p>
	<!--/VTP_erreurcandidature_niv1-->  
	<!--VTP_refuscandidature_niv1-->    
		<p>Votre candidature ne peut pas être prise en compte : il n'est plus possible de déposer de candidature pour cette offre.</p>
	<!--/VTP_refuscandidature_niv1-->  
<!--/VTP_posercand_niv1-->
<!--VTP_cand_interdite_niv1-->
    <div class="contenu">
    	<h1>Impossible de déposer une candidature</h1>
    	<p>Vous n'avez pas la possibilité de déposer votre candidature pour cette offre.</p>
    	<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
    	<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
    </div>
<!--/VTP_cand_interdite_niv1-->
