<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traitercand_niv1-->
	<!--VTP_rappel_annonce_niv1-->
    	<div class="contenu">
    		<h1>Rappel de l'annonce</h1>
    		<p>
    		<span class="imp">Nom de l'annonce : </span>{#nomannonce}<br>
    		<span class="imp">Catégorie : </span>{#categorie}<br>
    		<span class="imp">Places disponibles : </span>{#nbpersos}<br>
    		<span class="imp">Description :</span><br>
    		{#descannonce}</p>
    	</div>
	<!--/VTP_rappel_annonce_niv1-->
	<!--VTP_rappel_candidature_niv1-->
		{#varcontenu}
	<!--/VTP_rappel_candidature_niv1-->	

	<!--VTP_formulaire_niv1-->
    	{#varcontenu2}
	<!--/VTP_formulaire_niv1-->
	
	<!--VTP_resultattraitement_niv1-->
    	<div class="contenu">
    		<h1>Résultat du traitement de la candidature</h1>
		<!--VTP_oktraitement_niv1-->
    			<p>Votre candidature a été traitée avec succès. Le MJ Inscription va la consulter dès qu'il le pourra.</p>
    	<!--/VTP_oktraitement_niv1-->
		<!--VTP_erreurtraitement_niv1-->    
			<p>Une erreur est survenue lors du traitement de votre candidature. Votre modification n'a pas pu être prise en compte. Veuillez en aviser le MJ Inscription.</p>
		<!--/VTP_erreurtraitement_niv1-->  
		    <div class="button"><a href="accueil.php">Retourner à l'accueil</a><br>
    		<div class="button"><a href="delogusr.php">Fermer la session d'identification</a>
		  </div>	 
	<!--/VTP_resultattraitement_niv1-->
<!--/VTP_traitercand_niv1-->
<!--VTP_cand_interdite_niv1-->
    <div class="contenu">
    	<div class="t1">Impossible de déposer une candidature</div>
    	<p>Vous n'avez pas la possibilité de déposer votre candidature pour cette offre.</p>
    	<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
    	<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
    </div>
<!--/VTP_cand_interdite_niv1-->
