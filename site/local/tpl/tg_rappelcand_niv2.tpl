<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_rappel_candidature_niv2-->
    	<div class="contenu">
    		<h1>Rappel de votre candidature</h1>
    		<p>
    		<span class="imp">Nom du personnage : </span>{#nomperso}<br>
    		<span class="imp">Race : </span>{#race}<br>
    		<span class="imp">Classe : </span>{#classe}<br>
    		<span class="imp">Sexe : </span>{#sexe}<br>
    		<span class="imp">Description du personnage : </span><br>{#description}<br>
     		<span class="imp">Background du personnage : </span><br>{#background}<br> 
     		<span class="imp">Buts du personnage : </span><br>{#buts}<br>  		
     		<span class="imp">Discussion avec le MJ Inscription : </span><br>{#debat}<br>
    		<span class="imp">Statut de la candidature :</span>{#statut}</p>
    	</div>
	<!--VTP_action_candidature_niv2-->
    	<div class="contenu">
    		<h1>Actions possibles pour cette candidature</h1>   
			<form action="insc_candencours.php" method="POST"> 
				<!--VTP_hidden_idcand-->
					<input type="hidden" name="idcand" value="{#idcand}">
				<!--/VTP_hidden_idcand-->
				<!--VTP_hidden_iteration-->
					<input type="hidden" name="iteration" value="{#iteration}">
				<!--/VTP_hidden_iteration-->
				<!--VTP_actiontraitercand-->
					<!--VTP_actiontraitercand_radio_option-->
                    <input type="radio"  name="actiontraitercand" value='{#value}' {#selected}>{#nom}<br>
                	<!--/VTP_actiontraitercand_radio_option-->
				<!--/VTP_actiontraitercand-->
				<input type="submit" value="Traiter cette action" onclick="return confirm('Confirmer votre action ? (Attention, action irréversible)')"> 
			</form><br>

			<div class="button"><a href="accueil.php">Retourner à l'accueil</a></div>
			<div class="button"><a href="delogusr.php">Fermer la session d'identification</a></div>
    	</div>
	<!--/VTP_action_candidature_niv2-->
<!--/VTP_rappel_candidature_niv2-->
