<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_editionquizz_niv1-->
<div class="contenu">
    <h1>Edition et correction des questions/réponses du quizz</h1>
	Que souhaitez-vous faire?<br>
	
	<form action="admin_editequizz2.php" method="POST">
	    
    <!--VTP_pasdequestion-->
    <p>Il n'y a pas encore de question à éditer pour ce quizz</p>
    <!--/VTP_pasdequestion-->    

    <!--VTP_listequestions-->
		<input type="radio" name="actionquizz" value="{#actionquizz}">Choisir une question à éditer :<br>
		<!--VTP_idquestion-->
        	<select name="{#name}">
            	<!--VTP_idquestion_select_option-->
                	<option value="{#value}" {#selected}>{#nom}</option>
           	 	<!--/VTP_idquestion_select_option-->
        	</select><br>
    	<!--/VTP_idquestion-->

    <!--/VTP_listequestions--> 
    
    <!--VTP_creerquestion-->
		<input type="radio" name="actionquizz" value="{#actionquizz}">Créer une nouvelle question<br>
    <!--/VTP_creerquestion--> 
    
    <!--VTP_hidden_iteration-->
       	<input type="hidden" name="iteration" value="{#iteration}">
    <!--/VTP_hidden_iteration-->
    	
    <input type="submit" value="Editer ou créer une question"onclick="return confirm('Êtes-vous sûr?')">
    </form>       
	  
    <div class="button"><a href="admin_editequizz.php">Retour à la gestion des questions</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_editionquizz_niv1-->
