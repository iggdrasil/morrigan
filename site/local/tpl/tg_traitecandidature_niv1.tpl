<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traitecandidature_niv1-->
<div class="contenu">
	<h1>Traitement de la candidature</h1>
	<!--VTP_succescommentaire-->
		<p>Votre commentaire a bien été pris en compte et a été transmis à son destinataire.</p>
	<!--/VTP_succescommentaire-->	
	<!--VTP_succesrefus-->
		<p>La candidature a bien été refusée. Le candidat en est informé. Vous pouvez effacer cette candidature.</p>
	<!--/VTP_succesrefus-->	
	<!--VTP_succeseffacer-->
		<p>La candidature a été effacée.</p>
	<!--/VTP_succeseffacer-->
	<!--VTP_succesvalider-->
		<p>La candidature a été validée. Le candidat en est informé. Topo pour expliquer quoi faire d'une candidature validée au MJ.</p>
	<!--/VTP_succesvalider-->
	<!--VTP_succesattente-->
		<p>La candidature a été mise en attente.</p>
	<!--/VTP_succesattente-->
	<!--VTP_successtatut-->
		<p>Le statut de cette candidature a été modifié comme requis. Attention, quel que soit le nouveau statut de cette candidature, personne n'en a été avisé.</p>
	<!--/VTP_successtatut-->
	<!--VTP_erreur_nbpersos-->
		<p>La valeur nbpersos pour ce profil est erronée. Veuillez corriger cet état de fait ou en avertir le MJ Inscription.</p>	
	<!--/VTP_erreur_nbpersos-->
	<!--VTP_refus_nbpersos-->
		<p>Vous ne pouvez pas faire ce traitement pour cette candidature : le maximum de candidatures déposables est déjà atteint pour ce profil.</p>	
	<!--/VTP_refus_nbpersos-->			
    <div class="button"><a href="admin_candidatures.php">Retour à la gestion des candidatures</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_traitecandidature_niv1-->
