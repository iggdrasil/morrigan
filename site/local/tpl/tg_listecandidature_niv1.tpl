<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_listecandidature_niv1-->
<div class="contenu">
    <h1>Liste des candidatures</h1>
	<!--VTP_pas_candidatures-->
	<p>Il n'y a pas de candidatures à étudier actuellement.</p>
	<!--/VTP_pas_candidatures-->
	<!--VTP_liste_table-->
    <h2>Liste des candidatures dont le statut est : {#statut}</h2>
    <p>Les candidatures sont classées par profil de référence.</p>
    <table class="items">
        <thead><tr>
            <th>Nom du joueur</th>
            <th>Nom du personnage</th>
            <th>Nom du profil</th>
            <th>Dernière modification (année, mois, jour)</th>
            <th>&nbsp;</th>
        </tr></thead>
   		<tbody> 
        <!--VTP_candidature_tableau-->

        <tr>
            <td>{#joueur}</td><td>{#personnage}</td><td>{#nomprofil}</td><td>{#dernieremodif}</td>
            <td>
                <form action="admin_traitecandidature.php" method="POST">
                    <!--VTP_hidden_idcandidature-->
                        <input type="hidden" name="idcandidature" value="{#idcandidature}">
                    <!--/VTP_hidden_idcandidature-->
                    <!--VTP_hidden_actioncandidature-->
                        <input type="hidden" name="actioncandidature" value="{#actioncandidature}">
                    <!--/VTP_hidden_actioncandidature-->
                    <!--VTP_hidden_iteration-->
                        <input type="hidden" name="iteration" value="{#iteration}">
                    <!--/VTP_hidden_iteration-->                    
                    <input type="submit" value="Afficher cette candidature"> 
                </form>
            </td>
        </tr>
        <!--/VTP_candidature_tableau-->
        </tbody> 
    </table>
	<br/><br/>
	<!--/VTP_liste_table-->

    <div class="button"><a href="admin_candidatures.php">Retour à la gestion des candidatures</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_listecandidature_niv1-->
