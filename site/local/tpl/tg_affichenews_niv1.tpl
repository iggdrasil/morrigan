<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_news_niv1-->
<div class='contenu'>
<h1>Les News</h1>
<!--VTP_nonews_niv1-->
Il n'y a pas de news à afficher. Revenez plus tard !
<!--/VTP_nonews_niv1-->
<!--VTP_news-->
<h3>{#titrenews} --- {#auteurnews} --- Le {#datenews}</h3>
{#textenews}<br/>
	<!--VTP_newsedit-->
	Dernière édition le : {#dateeditnews}.
	<!--/VTP_newsedit-->
	<br/>
<!--/VTP_news-->
</div>
<!--/VTP_news_niv1-->
