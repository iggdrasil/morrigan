<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_droitsrefuses_niv1-->
	<div class="contenu">
		<h1> Droits refusés</h1>
		Vous n'avez pas le droit de consulter cette page. Vous n'êtes pas un MJ ou vous n'êtes pas habilités à accéder à cette fonctionnalité du site.
	    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    	<div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
	</div>
<!--/VTP_droitsrefuses_niv1-->
