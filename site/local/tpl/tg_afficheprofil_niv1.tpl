<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_afficheprofil_niv1-->
<div class="contenu">
   <h1>Affichage d'un profil</h1>
   <div class="button"><a href="#actions">Aller directement au menu des actions</a></div>
	<p><span class="imp">Nom du profil :&nbsp;</span>{#nomprofil}</p>
	<p><span class="imp">Auteur du profil :&nbsp;</span>{#auteurprofil}</p>
	<p><span class="imp">Statut du profil :&nbsp;</span>{#statutprofil}</p>
	<p><span class="imp">Catégorie du profil :&nbsp;</span>{#categorieprofil}</p>
	<p><span class="imp">Nombre de personnages encore inscriptibles avec ce profil :&nbsp;</span>{#nbpersosprofil}</p>
	
	<!--VTP_afficheraceclassesexe-->	
	<p><span class="imp">Races, classes et sexes sélectionnables :</span><br>Pour plus de clarté, les races/classes/sexes ouverts à l'inscription sont précédés d'un signe <span class="imp">#</span>. Ceux qui sont fermés à l'inscription sont précédés d'un signe <span class="imp">X</span>.</p>	
	<table class="items">
		<thead>		
			<tr>
				<th>Race</th>
				<th>Classe</th>
				<th>Sexe</th>		
			</tr>	
		</thead>	
		<tbody>
		<tr>
			<td style="vertical-align:top;text-align:left;">
				<!--VTP_afficherace-->
					{#race}<br>
				<!--/VTP_afficherace-->
			</td>
			<td style="vertical-align:top;text-align:left;">
				<!--VTP_afficheclasse-->
					{#classe}<br>
				<!--/VTP_afficheclasse-->			
			</td>
			<td style="vertical-align:top;text-align:left;">
				<!--VTP_affichesexe-->
					{#sexe}<br>
				<!--/VTP_affichesexe-->
			</td>
		</tr>
		</tbody>	
	</table>
	<!--/VTP_afficheraceclassesexe-->
	<p><span class="imp">Description du profil :</span><br>
	{#descriptionprofil}	
	</p>
	<p><span class="imp">Débat à propos du profil :</span><br>
	{#debatprofil}	
	</p>
	<p><span class="imp">Historique du profil :</span><br>
	{#historiqueprofil}	
	</p>	
	<!--VTP_affichedemande-->
	<p><span class="imp">Demande de modification de statut (voir l'historique) :</span>&nbsp;<span class="err">{#demandeprofil}</span></p>
	<!--/VTP_affichedemande-->
</div>
<!--/VTP_afficheprofil_niv1-->
