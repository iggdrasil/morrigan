<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_editenews_niv2-->
	<!--VTP_form_editer_news-->
		<h2>Edition d'une news</h2>
		<form method="POST" action="admin_gestionnews.php">
       		Titre de la news :<br/>
       		<!--VTP_erreur_titrenews-->
       			<span class="err">Il y a une erreur dans le titre de la news. Il est soit vide, soit trop long, soit déjà existant dans la liste des titres de news.</span><br>
       		<!--/VTP_erreur_titrenews-->
       		<!--VTP_titrenews-->
       			<textarea name="titrenews" rows="1" cols="60">{#titrenews}</textarea>
       		<!--/VTP_titrenews-->   
        	<br/>
        	Texte de la news :<br/>
        	<!--VTP_erreur_textenews-->
       			<span class="err">Il y a une erreur dans le texte de la news. Il est soit vide, soit trop long.</span><br>
       		<!--/VTP_erreur_textenews-->
       		<!--VTP_textenews-->
       			<textarea name="textenews" rows="5" cols="60">{#textenews}</textarea>
       		<!--/VTP_textenews-->         		    		       		
       		<!--VTP_hidden_iteration-->
           		<input type="hidden" name="iteration" value="{#iteration}">
       		<!--/VTP_hidden_iteration-->
       		<!--VTP_hidden_iteration1-->
           		<input type="hidden" name="iteration1" value="{#iteration1}">
       		<!--/VTP_hidden_iteration1-->
        	<!--VTP_hidden_actionnews-->
           		<input type="hidden" name="actionnews" value="{#actionnews}">
       		<!--/VTP_hidden_actionnews-->
       		<!--VTP_hidden_idnews-->
           		<input type="hidden" name="idnews" value="{#idnews}">
       		<!--/VTP_hidden_idnews-->
       		<br/><br/> 		
			<input type="submit" value="Editer cette news"  onclick="return confirm('Êtes-vous sûr de vouloir éditer cette news?');">
		</form>		
	<!--/VTP_form_editer_news-->
<!--/VTP_editenews_niv2-->
