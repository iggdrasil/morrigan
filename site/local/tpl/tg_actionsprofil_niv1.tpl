<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_actionsprofil_niv1-->
<div class="contenu">
    <a id="actions"></a>
   <h1>Actions possibles pour ce profil</h1>
   <p><span class="imp">Que voulez-vous faire avec ce profil?</span></p>
    <form action="admin_traiteprofil.php" method="POST">
   <!--VTP_actionprofil-->
       <input type="radio" name="actionprofil" value="{#actionprofil}">{#texteaction}</input>
        <!--VTP_statutprofil-->
            <select name="{#name}[]">
                <!--VTP_statutprofil_select_option-->
                    <option value="{#value}" {#selected}>{#nom}</option>
                <!--/VTP_statutprofil_select_option-->
            </select>
        <!--/VTP_statutprofil-->
       <br>
   <!--/VTP_actionprofil-->
       <!--VTP_debatprofil-->
            <p><span class="imp">Ajouter un commentaire pour cette action</span> (le commentaire ne sera pas pris en compte pour certaines actions (édition, effacer...) :</p>
            <textarea name="debatprofil" rows="5" cols="100"></textarea>
       <!--/VTP_debatprofil-->
       <!--VTP_hidden_idprofil-->
           <input type="hidden" name="idprofil" value="{#idprofil}">
       <!--/VTP_hidden_idprofil-->
       <!--VTP_hidden_iteration-->
           <input type="hidden" name="iteration" value="{#iteration}">
       <!--/VTP_hidden_iteration-->
        <input type="submit" value="Exécuter cette action"  onclick="return confirm('Êtes-vous sûr de vouloir effectuer cette action (dernier avertissement)?')">
    </form>
    <div class='button'><a href="admin_profil.php">Retour à la gestion des profils</a></div>
    <div class='button'><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class='button'><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_actionsprofil_niv1-->
