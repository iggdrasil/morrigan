<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_actionscandidature_niv1-->
<div class="contenu">
    <a id="actions"></a>
   <h1>Actions possibles pour cette candidature</h1>
   <p><span class="imp">Que voulez-vous faire avec cette candidature ?</span></p>
    <form action="admin_traitecandidature.php" method="POST">
   		<!--VTP_actioncandidature-->
       	<input type="radio" name="actioncandidature" value="{#actioncandidature}">{#texteactioncandidature} :</input>
        	<!--VTP_statutcandidature-->
            	<select name="{#name}[]">
                	<!--VTP_statutcandidature_select_option-->
                    	<option value="{#value}" {#selected}>{#nom}</option>
                	<!--/VTP_statutcandidature_select_option-->
            	</select>
        	<!--/VTP_statutcandidature-->
       	<br>
    	<!--/VTP_actioncandidature-->
    	<!--VTP_commentairepj-->
        	<p><span class="imp">Ajouter un commentaire pour cette action à l'attention du candidat</span> (ce commentaire sera pris en compte pour les actions appelant une action du candidat):</p>
        	<textarea name="commentairepj" rows="5" cols="100">{#commentairepj}</textarea>
    	<!--/VTP_commentairepj-->
    	<!--VTP_avismj-->
        	<p><span class="imp">Ajouter un commentaire pour cette action à l'attention du MJ</span> (ce commentaire ne sera pris en compte que  pour les actions appelant une action d'un MJ):</p>
        	<textarea name="avismj" rows="5" cols="100">{#avismj}</textarea>
    	<!--/VTP_avismj-->
    	<!--VTP_commentaireglobal-->
        	<p><span class="imp">Ajouter un commentaire pour cette candidature</span> (sert de pense-bête) :</p>
        	<textarea name="commentaireglobal" rows="5" cols="100">{#commentaireglobal}</textarea>
    	<!--/VTP_commentaireglobal-->
    	<!--VTP_hidden_idcandidature-->
        	<input type="hidden" name="idcandidature" value="{#idcandidature}">
    	<!--/VTP_hidden_idcandidature-->
    	<!--VTP_hidden_iteration-->
        	<input type="hidden" name="iteration" value="{#iteration}">
    	<!--/VTP_hidden_iteration-->
        <input type="submit" value="Exécuter cette action"  onclick="return confirm('Êtes-vous sûr de vouloir effectuer cette action (dernier avertissement)?')">
    </form>
    <div class="button"><a href="admin_candidatures.php">Retour à la gestion des candidatures</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_actionscandidature_niv1-->
