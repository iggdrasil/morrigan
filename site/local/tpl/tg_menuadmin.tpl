<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_menu-admin-->
	<div class='menu-left-contenu'>
		<!--<form action="#" method="get">-->
			<ul>
				<li class='button'>
					<a href='admin.php'>Accueil de l'administration</a>
				</li>	

				<!--VTP_menu_news-->
				<li class='button'>
					<a href='admin_gestionnews.php'>Administration des news</a>
				</li>
				<!--/VTP_menu_news-->
				
				<!--VTP_menu_inscription-->
				<li>
					<div class='menuSection'>
						<div class='menuHeader' onclick="javascript:showHide('menu_inscription');">Gestion des Inscriptions</div>
						<ul class='menuContainer' id='menu_inscription'>
							<li class='button'>
								<a href='admin_profil.php'>Gestion des profils</a>
							</li>
							<li class='button'>
								<a href='admin_candidatures.php'>Gestion des candidatures</a>
							</li>
						</ul>
					</div>
				</li>
				<!--/VTP_menu_inscription-->
				
				<!--VTP_menu_recrutement-->
				<li>
					<div class='menuSection'>
						<div class='menuHeader' onclick="javascript:showHide('menu_recrutement');">Gestion du Recrutement</div>
						<ul class='menuContainer' id='menu_recrutement'>
							<li class='button'>
								<a href='admin_editequizz.php'>Gestion du Quizz</a>
							</li>
							<li class='button'>
								<a href='admin_editecategories.php'>Gestion des catégories de profils</a>
							</li>
							<li class='button'>
								<a href='admin_ouvertures.php'>Gestion des ouvertures des races/classes/sexes</a>
						</ul>
					</div>
				</li>
				<!--/VTP_menu_recrutement-->
				
				<!--VTP_menu_regles-->
				<li class='button'>
					<a href='?_action_logout=True'>Édition des règles du jeu</a>
				</li>
				<!--/VTP_menu_regles-->
				
				<!--VTP_menu_monde-->
				<li class='button'>
					<a href='#'>Édition du monde du jeu</a>
				</li>
				<!--/VTP_menu_monde-->
				
				<!--VTP_menu_admin-->
				<li class='button'>
					<a href='#'>Actions d'administration générale</a>
				</li>
				<!--/VTP_menu_admin-->

			</ul>
		<!--</form>-->		
	</div>
<!--/VTP_menu-admin-->
