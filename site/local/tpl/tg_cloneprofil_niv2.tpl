<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_cloneprofil_niv2-->
    <!--VTP_clone_ok-->
    <h1>Nouveau profil créé avec succès</h1>    
    <p>Le nouveau profil nommé "{#nomclone}" a été créé. Il est nécessaire de le soumettre au MJ recruteur en repassant par la page d'affichage. Vérifier sa cohérence est vivement encouragé.<p>
    <!--/VTP_clone_ok-->
    
    <!--VTP_clone_nok-->

    <h1>Erreurs lors de la création du nouveau profil</h1>
    <form action="admin_traiteprofil.php" method="POST">
    
        <!--VTP_hidden_idprofil-->        
            <input type="hidden" name="idprofil" value="{#idprofil}">
        <!--/VTP_hidden_idprofil-->
        <!--VTP_hidden_operation-->        
            <input type="hidden" name="operation" value="{#operation}">
        <!--/VTP_hidden_operation-->
        
        <!--VTP_erreur_nomprofil-->
        <p><span class="erreur">Le nom du profil que vous avez choisi existe déjà ou est trop long.</span></p>
        <!--/VTP_erreur_nomprofil-->
        <!--VTP_hidden_nomprofil-->        
            <input type="hidden" name="nomprofil" value="{#nomprofil}">
        <!--/VTP_hidden_nomprofil-->
        <!--VTP_nomprofil-->
            <input type="text" name="nomprofil" value="{#nomprofil}" size="80" maxlength="100">
        <!--/VTP_nomprofil-->  
        
        <table style="border: none; width: 80%;" cellpadding="0" cellspacing="0">
  		    <tr>
    		    <td>
                    <!--VTP_erreur_racespossiblesed-->
                        <p class="txt"><span class="imp">Race :</span></p>
                        Vos choix de race sont erronés.
                    <!--/VTP_erreur_racespossiblesed-->                				
				    <!--VTP_racespossiblesed-->			
    				    <select multiple size=5 name="{#name}[]">
        			    <!--VTP_racespossiblesed_select_option-->
            		    	<option value='{#value}' {#selected}>{#nom}</option>
        			    <!--/VTP_racespossiblesed_select_option--> 			
        			    </select> 	
				    <!--/VTP_racespossiblesed-->
                    <!--VTP_hidden_racespossiblesed-->
                        <input type="hidden" name="racespossiblesed[]" value="{#racespossiblesed}">
                    <!--/VTP_hidden_racespossiblesed-->				

			    </td>
    		    <td>
                    <!--VTP_erreur_classespossiblesed-->
                        <p class="txt"><span class="imp">Classe :</span></p>                    
                        Vos choix de classe sont erronés.
                    <!--/VTP_erreur_classespossiblesed-->                				
				    <!--VTP_classespossiblesed-->			
    				    <select multiple size=5 name="{#name}[]">
        			    <!--VTP_classespossiblesed_select_option-->
            		    	<option value='{#value}' {#selected}>{#nom}</option>
        			    <!--/VTP_classespossiblesed_select_option--> 			
        			    </select> 	
				    <!--/VTP_classespossiblesed-->
                    <!--VTP_hidden_classespossiblesed-->
                        <input type="hidden" name="classespossiblesed[]" value="{#classespossiblesed}">
                    <!--/VTP_hidden_classespossiblesed-->
                </td>
     		    <td>          
                    <!--VTP_erreur_sexespossiblesed-->
                        <p class="txt"><span class="imp">Sexe :</span></p> 
                        Vos choix de sexe sont erronés.
                    <!--/VTP_erreur_sexespossiblesed-->                				
				    <!--VTP_sexespossiblesed-->			
    				    <select multiple size=5 name="{#name}[]">
        			    <!--VTP_sexespossiblesed_select_option-->
            			    <option value='{#value}' {#selected}>{#nom}</option>
        			    <!--/VTP_sexespossiblesed_select_option--> 			
        			    </select> 	
				    <!--/VTP_sexespossiblesed-->
                    <!--VTP_hidden_sexespossiblesed-->  
                        <input type="hidden" name="sexespossiblesed[]" value="{#sexespossiblesed}">
                    <!--/VTP_hidden_sexespossiblesed-->	
			    </td>
  		    </tr>
        </table>  

        <!--VTP_erreur_descriptionprofil-->
        <p><span class="erreur">La description entrée est trop longue ou n'a pas été modifiée.</span></p>
        <!--/VTP_erreur_descriptionprofil-->
        <!--VTP_hidden_descriptionprofil-->        
            <input type="hidden" name="descriptionprofil" value="{#descriptionprofil}">
        <!--/VTP_hidden_descriptionprofil-->
        <!--VTP_descriptionprofil-->
            <textarea name="descriptionprofil" rows="5" cols="80">{#descriptionprofil}</textarea>    
        <!--/VTP_descriptionprofil-->   

        <!--VTP_erreur_debat-->
        <p><span class="erreur">Les commentaires entrés sont trop longs.</span></p>
        <!--/VTP_erreur_debat-->
        <!--VTP_hidden_debat-->        
            <input type="hidden" name="debat" value="{#debat}">
        <!--/VTP_hidden_debat-->
        <!--VTP_debat-->
            <textarea name="debat" rows="5" cols="80">{#debat}</textarea>    
        <!--/VTP_debat-->    
        <br>
        <input type="submit" value="Soumettre les corrections" onclick="return confirm('Confirmer la soumission des corrections?')">
    </form>        
    <br/><br/>
    <!--/VTP_clone_nok-->    
<!--/VTP_cloneprofil_niv2-->
