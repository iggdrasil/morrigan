<!-- 
Auteur : Arkenlond (arkenlond@peacefrogs.net)  
License : GPL
-->
<!--VTP_traiteouvertures_niv1-->
<div class="contenu">
    <h1>Traitement des races/classes/sexes pour leur ouverture ou fermeture à l'inscription</h1>
	<!--VTP_succesouvertures_niv1-->
	<p>L'ouverture/fermeture a eu lieu. Les profils et candidatures impactés ont été mis à jour en conséquence.</p>	
	<!--/VTP_succesouvertures_niv1-->
	<!--VTP_echecouvertures_niv1-->	
	<p>L'ouverture/fermeture a échoué. Les profils et candidatures impactés n'ont pas été correctement mis à jour. <span class="err">Attention aux incohérences !</span></p>
	<!--/VTP_echecouvertures_niv1-->
	<div class="button"><a href="admin_ouvertures.php">Retour à la gestion des ouvertures/fermetures</a></div>
    <div class="button"><a href="admin.php">Retour à l'interface d'administration</a></div>
    <div class="button"><a href="delogadm.php">Fermer la session d'identification</a></div>
</div>
<!--/VTP_traiteouvertures_niv1-->
