<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : delogusr.php                                                                                 
* Fonction du fichier : ferme la session de l'utilisateur                                                                            
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                          
* Date de création : 23/02/2008                                                                               
* Version actuelle : 1.0 au 23/02/2008                                                                               
* License du projet : GPL                                                                               
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES

// VARIABLES LOCALES
$l_i_ok = 0; // retour de la fonction delog

/* Récupération des variables de session */
// N/A

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/verif_mail.inc");

/* Redéfinition du gestionnaire d'erreurs */
$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Récupération des variables passées en formulaire */

/* Instantiation de l'objet de template */
// N/A

/* Contenu et inclusion des fichier de contenu */
$l_i_ok = logout_user ();


if ($l_i_ok == 0)
{
    print("vala l_i_ok :$l_i_ok<br>");
    //header ("Location:http://".HOST."/admin.php?erreur=1");
}
else
{
    print("youpie<br>");
    header ("Location:http://".HOST."/accueil.php");
}

/* Parsage final de la page */
// N/A

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();

/* Fin de fichier ***********************************************************************************/
?>
