<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : editelistenews_niv2.inc                                                                                 
* Fonction du fichier : liste les news éditables et permet de lancer les actions sur ces news                                                                           
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                         
* Date de création :  08/02/2009                                                                             
* Version actuelle :  1.0 au 08/02/2009                                                                              
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_resultat = array ();
$l_i_compteur = 0;
$l_t_st_mesnews = array ();
$l_t_st_autresnews = array ();
$l_t_st_mesnews [0] = 0;
$l_t_st_toutesnews [0] = 0;
$l_i_flagsubmit = NON;
$l_t_st_options = array ();

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "listenews_niv2");

$l_st_resultat = requete ("listenews");
if ($l_st_resultat [0] == 0)
{
	// Pas de réponse : erreur notifiée, sans gravité
    trigger_error ("Pas de news en base.",  NOTICE);
	$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "pas_news");
	$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "pas_news");
}
else
{
	$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "form_list_news");	
	
	if ($_SESSION ['droitnews'] == OUI && ($_SESSION ['droitallnews'] == NON))
	{
		// lister uniquement les news dont on est l'auteur
		for ($l_i_compteur= 0; $l_i_compteur < $l_st_resultat [0]; $l_i_compteur ++)
		{
			if ($l_i_resultat [1][$l_i_compteur][2] == $_SESSION ['id'])
			{
				$l_t_st_mesnews [1][$l_i_compteur][0] = $l_st_resultat [1][$l_i_compteur][0];
				$l_t_st_mesnews [1][$l_i_compteur][1] = $l_st_resultat [1][$l_i_compteur][1];
				$l_t_st_mesnews [0] ++;
			}
		}

		// Affichage
		if ($l_t_st_mesnews [0]!= 0)
		{
			affiche_variable ($g_st_idnews, "select", $l_i_handlerlistenews_niv2, $l_t_st_mesnews);
			$l_i_flagsubmit = OUI;
		}
		else
		{
			$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "pasdenews");
			$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "pasdenews");			
		}
		
	}
	elseif (($_SESSION ['droitnews'] == OUI) && ($_SESSION ['droitallnews'] == OUI))
	{
		// lister toutes les news
		for ($l_i_compteur= 0; $l_i_compteur < $l_st_resultat [0]; $l_i_compteur ++)
		{
			$l_t_st_toutesnews [1][$l_i_compteur][0] = $l_st_resultat [1][$l_i_compteur][0];
			$l_t_st_toutesnews [1][$l_i_compteur][1] = $l_st_resultat [1][$l_i_compteur][1];
			$l_t_st_toutesnews [0] ++;
		}

		// Affichage
		if ($l_t_st_toutesnews [0]!= 0)
		{
			affiche_variable ($g_st_idnews, "select", $l_i_handlerlistenews_niv2, $l_t_st_toutesnews);	
			$l_i_flagsubmit = OUI;
		}
		else
		{
			$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "pasdenews");
			$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "pasdenews");			
		}
	}
	else
	{
		trigger_error ("Vous n'avez pas les droits pour éditer une news", FATAL);
		exit ();
	}

	$l_t_st_options [0] = 2;
	$l_t_st_options [1][0][0] = EDITERNEWS;
	$l_t_st_options [1][0][1] = EDITERNEWSTXT;
	$l_t_st_options [1][1][0] = EFFACERNEWS;
	$l_t_st_options [1][1][1] = EFFACERNEWSTXT;
	

	if ($_SESSION ['droitnews'] == OUI)
	{	
		// Liste des actions possibles

		if ($l_i_flagsubmit == OUI)
		{
			
			affiche_variable ($g_st_actionnews, "radio", $l_i_handlerlistenews_niv2, $l_t_st_options);
			$g_st_iteration [CONTENU_VAR] = 1;
			$g_st_iteration [TYPE_DISPLAY] = HIDDEN;
			affiche_variable ($g_st_iteration, "input", $l_i_handlerlistenews_niv2, NULL);
			$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "submit");
			$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "submit");
		}	
	}
	
	

	$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "form_list_news");
	
	if ($_SESSION ['droitnews'] == OUI)
	{	
		$l_o_template -> addSession ($l_i_handlerlistenews_niv2, "form_creer_news");
		
		$g_st_actionnews [CONTENU_VAR] = CREERNEWS;
		$g_st_actionnews [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_actionnews, "input", $l_i_handlerlistenews_niv2, NULL);			
		$g_st_iteration1 [CONTENU_VAR] = 1;
		$g_st_iteration1 [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_iteration1, "input", $l_i_handlerlistenews_niv2, NULL);		
		$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "form_creer_news");
	}	
			
}


$l_o_template -> closeSession ($l_i_handlerlistenews_niv2, "listenews_niv2");
?>
