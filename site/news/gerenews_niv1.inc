<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : gerenews_niv1.inc                                                                                 
* Fonction du fichier : permet de gérer les news                                                                           
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                           
* Date de création :  25/01/2009                                                                             
* Version actuelle :  1.0 au 25/01/2009                                                                              
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_resultat = array ();
$l_i_compteur = 0;
$l_i_flagsucces = NON;
	
/* Contenu et inclusion des fichier de contenu */
switch ($l_i_iteration)
{
	case 0:
		include ("news/editelistenews_niv2.inc");
		break;
	case 1:
		switch ($g_st_actionnews [CONTENU_VAR])
		{
			case CREERNEWS:
			case EDITERNEWS:
				include ("news/editenews_niv2.inc");
				break;
			case EFFACERNEWS:
				// Action d'effacement
				$l_st_resultat = requete ("efface_news", $g_st_idnews [CONTENU_VAR]);
				if ($l_st_resultat ['nbaffecte'] == 1)
				{
					// En cas de succès, flag succès
					$l_i_flagsucces	= OUI;
				}
				else
				{
					trigger_error ("Effacement de la news '".$g_st_idnews [CONTENU_VAR]."' impossible", FATAL);
					exit ();
				}
				break;
			default:
				trigger_error ("Action non reconnue", FATAL);
				exit ();
				break;	
		} 
		break;
	case 2:
		switch ($g_st_actionnews [CONTENU_VAR])
		{
			case CREERNEWS:
				if ($g_i_erreur == OUI)
				{
					include ("news/editenews_niv2.inc");					
				}
				else
				{
					// Requête d'insertion
					$l_st_resultat = requete ("creernews", $g_st_titrenews [CONTENU_VAR], $g_st_textenews [CONTENU_VAR]);
					if ($l_st_resultat ['nbaffecte'] == 1)
					{					
						// En cas de succès, flag succès
						$l_i_flagsucces	= OUI;
					}
					else
					{
						trigger_error ("Ajout de la news impossible", FATAL);
						exit ();						
					}
				}
				break;
			case EDITERNEWS:
				if ($g_i_erreur == OUI)
				{
					include ("news/editenews_niv2.inc");					
				}
				else
				{
					// Requête d'update
					$l_st_resultat = requete ("editenews", $g_st_idnews [CONTENU_VAR], $g_st_titrenews [CONTENU_VAR], $g_st_textenews [CONTENU_VAR]);
					if ($l_st_resultat ['nbaffecte'] == 1)
					{
						// En cas de succès, flag succès
						$l_i_flagsucces	= OUI;
					}
					else
					{
						trigger_error ("Edition de la news '".$g_st_idnews [CONTENU_VAR]."' impossible", FATAL);
						exit ();
					}
				}
				break;
			case EFFACERNEWS:
				trigger_error ("Cette iteration est impossible pour la suppression de la news", FATAL);
				exit ();
				break;
			default:
				trigger_error ("Action non reconnue", FATAL);
				exit ();
				break;	
		}		
		break;
	default:
		trigger_error ("Iteration non reconnue", FATAL);
		exit ();
		break;
}

// Affichage proprement dit
$l_o_template -> addSession ($l_i_handlernews_niv1,  "news_niv1");
if ($l_i_iteration == 0)
{
	$l_o_template -> addSession ($l_i_handlernews_niv1,  "listenews_niv1");
	$l_o_template -> Parse ($l_i_handlernews_niv1, "listenews_niv1.contenuliste", $l_i_handlerlistenews_niv2, "listenews_niv2");
	$l_o_template -> closeSession ($l_i_handlernews_niv1, "listenews_niv1");
}
else
{
	if ($g_st_actionnews [CONTENU_VAR] != EFFACERNEWS) 
	{
		$l_o_template -> addSession ($l_i_handlernews_niv1,  "editenews_niv1");
		$l_o_template -> Parse ($l_i_handlernews_niv1, "editenews_niv1.contenuedite", $l_i_handlereditenews_niv2, "editenews_niv2");
		$l_o_template -> closeSession ($l_i_handlernews_niv1, "editenews_niv1");
	}
	
	// Critère de succès
	if (($l_i_flagsucces == OUI) && ((($l_i_iteration == 1) && ($g_st_actionnews [CONTENU_VAR] == EFFACERNEWS)) || (($l_i_iteration > 1) && ($g_st_actionnews [CONTENU_VAR] != EFFACERNEWS))))
	{
		switch ($g_st_actionnews [CONTENU_VAR])
		{
			case EFFACERNEWS:
				$l_o_template -> addSession ($l_i_handlernews_niv1,  "succeseffacer");
				$l_o_template -> closeSession ($l_i_handlernews_niv1,  "succeseffacer");				
				break;
			case CREERNEWS:
			case EDITERNEWS:
				$l_o_template -> addSession ($l_i_handlernews_niv1,  "succesediter");
				$l_o_template -> closeSession ($l_i_handlernews_niv1,  "succesediter");					
				break;
			default:
				trigger_error ("Action mal définie lors du traitement du critère de succès OK de traitement des news", FATAL);
				exit ();
				break;
					
		}
	}	
	elseif (($l_i_flagsucces == NON) && ((($l_i_iteration == 1) && ($g_st_actionnews [CONTENU_VAR] == EFFACERNEWS)) || (($l_i_iteration > 1) && ($g_st_actionnews [CONTENU_VAR] != EFFACERNEWS))))
	{
		switch ($g_st_actionnews [CONTENU_VAR])
		{
			case EFFACERNEWS:
				$l_o_template -> addSession ($l_i_handlernews_niv1,  "echeceffacer");
				$l_o_template -> closeSession ($l_i_handlernews_niv1,  "echeceffacer");	
				break;
			case CREERNEWS:
			case EDITERNEWS:
				$l_o_template -> addSession ($l_i_handlernews_niv1,  "echecediter");
				$l_o_template -> closeSession ($l_i_handlernews_niv1,  "echecediter");	
				break;	
			default:
				trigger_error ("Action mal défini lors du traitement du critère de succès NOK de traitement des news", FATAL);
				exit ();
				break;
		}		
	}
	/*else
	{
		// Qu'est-ce que c'est que ce drapeau pourri?
		trigger_error ("Le drapeau de succès de traitement des news a une valeur incohérente", FATAL);
		exit ();	
	}*/
}
$l_o_template -> closeSession ($l_i_handlernews_niv1,  "news_niv1");

/* Fin de fichier ***********************************************************************************/
?>
