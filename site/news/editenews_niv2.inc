<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : editenews_niv2.inc                                                                                 
* Fonction du fichier : permet d'éditer ou créer une news                                                                           
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                           
* Date de création :  22/02/2009                                                                             
* Version actuelle :  1.0 au 22/02/2009                                                                              
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_resultat = array ();
$l_i_compteur = 0;
$l_i_flagok = NON;

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlereditenews_niv2, "editenews_niv2");

if ($g_st_actionnews [CONTENU_VAR] == EDITERNEWS)
{
	$l_st_resultat = requete ("contenu_news", $g_st_idnews [CONTENU_VAR]);
	if ($l_st_resultat [0] == 0)
	{
		// Pas de réponse
		trigger_error ("Pas de news en base pour cet id ('".$g_st_idnews [CONTENU_VAR]."').",  FATAL);
		exit();
	}
}

// Vérification des droits
switch ($g_st_actionnews [CONTENU_VAR])
{
	case CREERNEWS:
		if (($_SESSION ['droitnews'] == OUI) || ($_SESSION ['droitallnews'] == OUI))
		{
			$l_i_flagok = OUI;
		}
		break;
	case EDITERNEWS:
		if (($_SESSION ['droitnews'] == OUI) && ($_SESSION ['id'] == $l_st_resultat [1][0][3]))
		{
			$l_i_flagok = OUI;
		}
		
		if ($_SESSION ['droitallnews'] == OUI)
		{
			$l_i_flagok = OUI;		
			print ("En plein dans le mille<br>");	
		}
		break;
	default:
		break;
}

if ($l_i_flagok == OUI)
{	
	$l_o_template -> addSession ($l_i_handlereditenews_niv2, "form_editer_news");
	
	
	if (($l_st_resultat [0] != 0) && ($l_i_iteration == 1))
	{
		$g_st_titrenews [CONTENU_VAR] = $l_st_resultat [1][0][1];
		$g_st_textenews [CONTENU_VAR] = $l_st_resultat [1][0][2];
	}
	
	affiche_variable ($g_st_titrenews, "textarea", $l_i_handlereditenews_niv2, NULL);
	affiche_variable ($g_st_textenews, "textarea", $l_i_handlereditenews_niv2, NULL);		
	
	$g_st_actionnews [TYPE_DISPLAY] = HIDDEN;
	affiche_variable ($g_st_actionnews, "input", $l_i_handlereditenews_niv2, NULL);			
	
	if ($g_st_actionnews [CONTENU_VAR] == EDITERNEWS)
	{
		$g_st_iteration [CONTENU_VAR] = 2;
		$g_st_iteration [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_iteration, "input", $l_i_handlereditenews_niv2, NULL);
		$g_st_idnews [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_idnews, "input", $l_i_handlereditenews_niv2, NULL);					
	}
	elseif ($g_st_actionnews [CONTENU_VAR] == CREERNEWS)
	{
		$g_st_iteration1 [CONTENU_VAR] = 2;
		$g_st_iteration1 [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_iteration1, "input", $l_i_handlereditenews_niv2, NULL);
	}		
	$l_o_template -> closeSession ($l_i_handlereditenews_niv2, "form_editer_news");
}



$l_o_template -> closeSession ($l_i_handlereditenews_niv2, "editenews_niv2");
?>
