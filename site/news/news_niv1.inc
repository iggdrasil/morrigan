<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : news_niv2.inc                                                                                 
* Fonction du fichier : permet d'afficher les 5 premieres news sur la page d'accueil                                                                            
* Auteur : Arkenlond (arkenlond]@peacefrogs.net)                                                                                           
* Date de création :  14/04/2004                                                                             
* Version actuelle :  1.0 au 17/04/2004                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_resultat = array ();
$l_i_compteur = 0;
$l_i_nbnews = 0;

/* Contenu et inclusion des fichier de contenu */
$l_st_resultat = requete ("news_accueil");

$l_o_template -> addSession ($l_i_handlernews_niv1,  "news_niv1");

if ($l_st_resultat [0] == 0)
{
	$l_o_template -> addSession ($l_i_handlernews_niv1,  "nonews_niv1");
	$l_o_template -> closeSession ($l_i_handlernews_niv1, "nonews_niv1");	
}
else
{
	if ($l_st_resultat [0]< NEWSACCUEIL)
	{
		$l_i_nbnews = $l_st_resultat [0];
	}
	else 
	{
		$l_i_nbnews = NEWSACCUEIL;
	}
	for ($l_i_compteur = 0; $l_i_compteur < $l_i_nbnews; $l_i_compteur++)
	{
		$l_o_template -> addSession ($l_i_handlernews_niv1,  "news");
		$l_o_template -> setVar ($l_i_handlernews_niv1, "news.titrenews", stripslashes ($l_st_resultat [1][$l_i_compteur][0]));
		$l_o_template -> setVar ($l_i_handlernews_niv1, "news.auteurnews", stripslashes ($l_st_resultat [1][$l_i_compteur][2]));
		$l_o_template -> setVar ($l_i_handlernews_niv1, "news.datenews", stripslashes ($l_st_resultat [1][$l_i_compteur][1]));
		$l_o_template -> setVar ($l_i_handlernews_niv1, "news.textenews", stripslashes ($l_st_resultat [1][$l_i_compteur][3]));
		if ($l_st_resultat [1][$l_i_compteur][4] != "")
		{
			$l_o_template -> addSession ($l_i_handlernews_niv1, "newsedit");
			$l_o_template -> setVar ($l_i_handlernews_niv1, "newsedit.dateeditnews", stripslashes ($l_st_resultat [1][$l_i_compteur][4]));	
			$l_o_template -> closeSession ($l_i_handlernews_niv1, "newsedit");
		}
	
		$l_o_template -> closeSession ($l_i_handlernews_niv1, "news");		
	}
}

$l_o_template -> closeSession ($l_i_handlernews_niv1, "news_niv1");

/* Fin de fichier ***********************************************************************************/
?>
