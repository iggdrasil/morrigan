<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_traiteprofil.php
* Fonction du fichier : page de traitement des profils soumis via afficheprofil
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  27/05/2007
* Version actuelle : 2.0 au 28/09/2007 
* License du projet : GPL
* Dernières modifications : 2.0 au 28/09/2007
* 							1.0 au 27/05/2007
* Remarques particulières : supprimer les traces de debug
*
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_st_idprofil = array ("idprofil", "", NORMAL);
$g_st_nomprofil = array ("nomprofil", "", NORMAL);
$g_st_auteurprofil = array ("auteurprofil", "", NORMAL);
$g_st_race = array ("racespossiblesed", "", NORMAL);
$g_st_classe = array ("classespossiblesed", "", NORMAL);
$g_st_sexe = array ("sexespossiblesed", "", NORMAL);
$g_st_description = array ("descriptionprofil", "", NORMAL);
$g_st_statut = array ("statutprofil", "", NORMAL);
$g_st_categorie = array ("categorie", "", NORMAL);
$g_st_debat = array ("debat", "", NORMAL);
$g_st_nbpersos = array ("nbpersos", "", NORMAL);
$g_st_actionprofil = array ("actionprofil", "", NORMAL);
$g_st_iteration = array ("iteration", "", NORMAL);
$g_i_numeroprofil = 0;
$g_i_erreur = NON;

// VARIABLES LOCALES
$l_i_logok = NON; // admin logué?
$l_i_erreur = NON; // sera reversé dans $g_i_erreur si égal à OUI
$l_t_i_existeprofil = array (0, array (0, 0));
$l_i_resultat = array ();

/* Récupération des variables de session */

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/admin_fonctions.inc");

/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();
if (!$g_i_lien)
{
    trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
    exit ();
}

// construction d'une table utilisable lors des requetes de listing des statuts (pas très propre, mais pratique pour 
// rendre les statuts compatibles aux fonctions de manipulation de formulaire
$l_i_resultat = requete ("temp_statuts_profils");
if ($l_i_resultat ['nbaffecte'] != 0)
{
	trigger_error ("Impossible de créer la table temporaire des statuts des profils", FATAL);
	exit ();
}

$l_i_resultat = requete ("insert_statuts_profils");
if ($l_i_resultat ['nbaffecte'] != 4)
{
	trigger_error ("Impossible d'insérer les valeurs dans la table temporaire des statuts des profils", FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handlertraiteprofil_niv1 = $l_o_template -> Open ("local/tpl/tg_traiteprofil_niv1.tpl");
$l_i_handlerafficheprofil_niv1 = $l_o_template -> Open ("local/tpl/tg_afficheprofil_niv1.tpl");
$l_i_handleractionsprofil_niv1 = $l_o_template -> Open ("local/tpl/tg_actionsprofil_niv1.tpl");
$l_i_handlerediteprofil_niv2 = $l_o_template -> Open ("local/tpl/tg_editeprofil_niv2.tpl");
$l_i_handlertraiteprofil_niv2 = $l_o_template -> Open ("local/tpl/tg_traiteprofil_niv2.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");
$l_i_handlerdroitsrefuses_niv1 = $l_o_template -> Open ("local/tpl/tg_droitsrefuses_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

//print($l_i_logok);

if ($l_i_logok == NON)
{
    include ("admin/admin_niv1_login.inc");
}
else
{
    if ($_SESSION ["droitinscription"] == OUI)
    {
        recup_variable ($g_st_actionprofil, "integer");
        $l_i_erreur = controle_variable (&$g_st_actionprofil, "radio");
        if ($l_i_erreur == OUI)
        {
            trigger_error ("Action inexistante.", ERROR);
            $l_i_erreur = NON;
            $g_st_actionprofil [CONTENU_VAR] = AFFICHER;
        }

                //print ("Action=".$g_st_actionprofil [CONTENU_VAR]."<br>");

        // Action autorisée?
        switch ($g_st_actionprofil [CONTENU_VAR])
        {
            case AFFICHER:
            case CREER:
            case CREATIONDAPRES:
            case EDITER:
            case DEMANDEHORSLIGNE:
            case DEMANDEENLIGNE:
            case DEMANDEEDITER:
                // Action autorisée, tout va bien
                break;
            case STATUT:
            case EFFACER:
            case REFUSER:
                if ($_SESSION ['droitrecrutement'] == NON)
                {
                    trigger_error ("Le MJ (".$_SESSION ['user'].") n'a pas le droit de faire cette action (".$g_st_actionprofil    [CONTENU_VAR].").");
                    exit ();
                }
                break;
            default:
                break;
        }


        // Récupérer et contrôler infos à traiter (formulaire)
        recup_variable ($g_st_iteration, "integer");
        $l_i_erreur = controle_variable (&$g_st_iteration, "radio");
        if ($l_i_erreur == OUI)
        {
            trigger_error ("L'itération de traitement d'édition de profil ne correspond à rien", FATAL);
            exit ();
        }


        if ($g_st_actionprofil [CONTENU_VAR] != CREER)
        {
            // print("Action différente de créer ou deuxième itération<br>");
            recup_variable ($g_st_idprofil, "integer");
            
            $l_i_erreur = controle_variable (&$g_st_idprofil, array ("text", 0, 11)); // vérifier que c'est bien 11 qu'il faut mettre
            if ($l_i_erreur == OUI)
            {
                trigger_error ("Le profil n'existe pas.", FATAL);
                exit ();
            }
            
            $g_i_numeroprofil = $g_st_idprofil [CONTENU_VAR];

            $l_t_i_existeprofil = requete ("existe_profil", $g_st_idprofil [CONTENU_VAR]);
            if ($l_t_i_existeprofil [0] == 0)
            {
                trigger_error ("Pas de profil correspondant à l'id de profil demandé : ".$g_st_idprofil [CONTENU_VAR].".", FATAL);
                exit ();
            }            		
        }
        else
        {
        	$l_t_i_existeprofil [0] = 1;
        }

        //  Deuxième itération ou action d'affichage ou action annexe (demande, effacement, changement de statut, refus d'action)
        if (
        		$g_st_iteration [CONTENU_VAR] == 2 
        	|| 
        		$g_st_actionprofil [CONTENU_VAR] == AFFICHER
        	||
        		$g_st_actionprofil [CONTENU_VAR] == DEMANDEENLIGNE
        	||
        		$g_st_actionprofil [CONTENU_VAR] == DEMANDEHORSLIGNE
        	||
        		$g_st_actionprofil [CONTENU_VAR] == DEMANDEEDITER
        	||
        		$g_st_actionprofil [CONTENU_VAR] == EFFACER
        	||
        		$g_st_actionprofil [CONTENU_VAR] == STATUT
        	||
        		$g_st_actionprofil [CONTENU_VAR] == REFUSER
        	)
        {            
            if ($l_t_i_existeprofil [0] > 0)
            {
                 print ("Action=".$g_st_actionprofil [CONTENU_VAR]."<br>");

                // Action = afficher profil ou autre?
                if    ($g_st_actionprofil [CONTENU_VAR] == AFFICHER)
                {
                    include ("admin/admin_niv1_afficheprofil.inc");
                }
                else
                {
                    switch ($g_st_actionprofil [CONTENU_VAR])
                    {
                        case CREATIONDAPRES:
                        case EDITER:
                            if ($g_st_iteration [CONTENU_VAR] == 1)
                            {
                                break;
                            }
						case CREER:
                            // récupérer la totale (ou presque)
                            recup_variable ($g_st_nomprofil, "text");

                            $l_i_erreur = controle_variable (&$g_st_nomprofil, array ("text", 1, 255, $g_st_idprofil [CONTENU_VAR]));
							if ($l_i_erreur == OUI)
							{
								$g_i_erreur = OUI;
							}
                            recup_variable ($g_st_race, "array");
                            $l_i_erreur = controle_variable (&$g_st_race, array ("select", 1, 100)); // variable en dur à  retravailler...
                            if ($l_i_erreur == OUI)
                            {
                                trigger_error ("Les éléments race transmis par ".$_SESSION ['user']." ne sont pas corrects", ERROR);
                                $g_i_erreur = OUI;
                            }

                            recup_variable ($g_st_classe, "array");
                            $l_i_erreur = controle_variable (&$g_st_classe, array ("select", 1, 100)); // idem
                            if ($l_i_erreur == OUI)
                            {
                                trigger_error ("Les éléments classe transmis par ".$_SESSION ['user']." ne sont pas corrects", ERROR);
                                $g_i_erreur = OUI;
                            }

                            recup_variable ($g_st_sexe, "array");
                            $l_i_erreur = controle_variable (&$g_st_sexe, array ("select", 1, 100)); // idem
                            if ($l_i_erreur == OUI)
                            {
                                trigger_error ("Les éléments sexe transmis par ".$SESSION ['user']." ne sont pas corrects", ERROR);
                                $g_i_erreur = OUI;
                            }

                            recup_variable ($g_st_description, "text");
                            $l_i_erreur = controle_variable (&$g_st_description, array ("text", 0, 2000)); // variable en dur à transformer en paramètre centralisé
							if ($l_i_erreur == OUI)
							{
								$g_i_erreur = OUI;
							}

                            recup_variable ($g_st_categorie, "array");
                            $l_i_erreur = controle_variable (&$g_st_categorie, array ("select", 1, 1));
							if ($l_i_erreur == OUI)
							{
								$g_i_erreur = OUI;
							}
							
                            recup_variable ($g_st_debat, "text");
                            $l_i_erreur = controle_variable (&$g_st_debat, array ("text", 0, 200)); // variable en dur à transformer en paramètre
                            // Le débat est facultatif, on permet donc que le champ soit vide
							if ($l_i_erreur == OUI && $g_st_debat [CONTENU_VAR] != "")
							{
								$g_i_erreur = OUI;
							}
							if ($g_st_debat [CONTENU_VAR] == "")
							{
								// hack pour invalider l'erreur sur ce champ s'il est vide
								$g_st_debat [TYPE_DISPLAY] = NORMAL;
							}
							
                            recup_variable ($g_st_nbpersos, "integer");
                            $l_i_erreur = controle_variable (&$g_st_nbpersos, array ("text", 0, 11)); // idem
							if ($l_i_erreur == OUI)
							{
								$g_i_erreur = OUI;
							}
							
							if ($g_st_nbpersos [CONTENU_VAR]== "")
							{
								$g_st_nbpersos [CONTENU_VAR] = 1; // par défaut, on crée UN poste vacant pour ce profil, si rien n'est renseigné dans le champ
							}
							
                            // attention à la modification de statut uniquement possible pour le MJ Inscription
                            if ($_SESSION ['droitrecrutement'] == OUI)
                            {
                                recup_variable ($g_st_auteurprofil, "array");
                                $l_i_erreur = controle_variable (&$g_st_auteurprofil, array ("select", 1, 1));
								if ($l_i_erreur == OUI)
								{
									$g_i_erreur = OUI;
								}      
								 
                                recup_variable ($g_st_statut, "array");
                                $l_i_erreur = controle_variable (&$g_st_statut, array ("select", 1, 1));
                                if ($l_i_erreur == OUI)
                                {
                                    trigger_error ("Le statut demandé pour la modification du profil n'est pas valable", FATAL);
                                    exit ();
                                }

                            }

                            break;
                        case STATUT:
                            // récupérer le nouveau statut
                            recup_variable ($g_st_statut, "array");
                            $l_i_erreur = controle_variable (&$g_st_statut, array ("select", 1, 1));
                            if ($l_i_erreur == OUI)
                            {
                                trigger_error ("Le statut demandé pour la modification du profil n'est pas valable", FATAL);
                                exit ();
                            }

                            break;
                        case DEMANDEHORSLIGNE:
                        case DEMANDEENLIGNE:
                        case DEMANDEEDITER:
                        case REFUSER:
                            // récupérer le champ de débat
                            recup_variable ($g_st_debat, "text");
                            $l_i_erreur = controle_variable (&$g_st_debat, array ("text", 0, 2000)); // variable en dur à transformer en paramètre
                            if ($l_i_erreur == OUI)
							{
								$g_i_erreur = OUI;
							}
                            break;

                        case EFFACER:
                            break;
                            // besoin de rien (envie de toi...)
                        default:
                            break;
                    }
                }
            }

        }
        elseif ($g_st_actionprofil [CONTENU_VAR] == CREER)
        {
            // On a juste besoin de récupérer l'itération, pour pouvoir afficher le formulaire d'édition vierge
            recup_variable ($g_st_iteration, "integer");
            $l_i_erreur = controle_variable (&$g_st_iteration, "radio");
            if ($l_i_erreur == OUI)
            {
                trigger_error ("L'itération de traitement d'édition de profil ne correspond à rien", FATAL);
                exit ();
            }
        }
        

        // Pour toutes les actions sauf celle d'affichage, traitement :
        if ($g_st_actionprofil [CONTENU_VAR] != AFFICHER)
        {
            include ("admin/admin_niv1_traiteprofil.inc");
        }
    }
   else
        include ("admin/admin_niv1_droitrefuse.inc");
}

$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
    trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
