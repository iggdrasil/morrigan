<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier :admin_niv1_droitrefuse.inc
* Fonction du fichier : Indique que les droits sont refusés pour l'accès à cette page
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  24/02/2009
* Version actuelle : 1.0 au 24/02/2009
* License du projet : GPL
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */

$l_o_template -> addSession ($l_i_handlerdroitsrefuses_niv1, "droitsrefuses_niv1");
$l_o_template -> closeSession ($l_i_handlerdroitsrefuses_niv1, "droitsrefuses_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerdroitsrefuses_niv1, "droitsrefuses_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


/* Fin de fichier ***********************************************************************************/
?>
