<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_traiteouvertures.inc
* Fonction du fichier : permet de traiter les ouvertures/fermetures des races/classes/sexes
*									à l'inscription : prise en compte des modifications
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  02/09/2008
* Version actuelle : 1.0 au 02/09/2008
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
* 											/!\ TODO : coder le mailing vers les utilisateurs concernés
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array ());
$l_t_st_resultatprofil = array (0, array ());
$l_t_st_resultatcand = array (0, array ());
$l_i_compteur = 0; // compteur pour les boucles for
$l_i_compteur2 = 0; 
$l_st_affichage = array ("", "", NORMAL);
$l_t_traitementprofil = array (); // profil par profil 
$l_t_traitementcand = array (); // candidature par candidature 
$l_i_flagsucces = NON;

/* Contenu et inclusion des fichier de contenu */
// Pour les races
if ($g_st_iterationrace [CONTENU_VAR] == 1)
{
	// Récupération des données de travail
	$l_st_resultatprofil = requete ("possibilitesprofilrace", $g_st_idrace [CONTENU_VAR]);
		
	$l_st_resultatcand = requete ("candassocierace", $g_st_idrace [CONTENU_VAR]);
	
	
	// traitement des profils 
	switch ($g_st_actionrace [CONTENU_VAR])
	{
		case NON: // retrait de la possibilité
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
			{
				$l_t_traitementprofil [$l_i_compteur]["idprofil"] = $l_st_resultatprofil [1][$l_i_compteur][0];
				$l_t_traitementprofil [$l_i_compteur]["traiter"] = NON;
						
				if ($l_st_resultatprofil [1][$l_i_compteur][1] == 1)
				{
					// Une seule possibilité pour ce profil, il faudra le mettre hors-ligne
					$l_t_traitementprofil [$l_i_compteur]["traiter"] = OUI;
				}
			}

			// traitement des candidatures en conséquence (refuser si le profil est mis hors ligne, remettre en édition si la race est fermée et qu'il reste d'autres possibilités sur ce profil)
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
			{
				$l_t_traitementcand [$l_i_compteur]["idcand"] = $l_st_resultatcand [1][$l_i_compteur][0];
				for ($l_i_compteur2 = 0; $l_i_compteur2 < $l_st_resultatprofil [0]; $l_i_compteur2++)
				{
					if ($l_t_traitementprofil [$l_i_compteur2]["idprofil"] == $l_st_resultatcand [1][$l_i_compteur][1])
					{
						if ($l_t_traitementprofil [$l_i_compteur2]["traiter"] == OUI)
						{
							// Candidature à refuser
							$l_t_traitementcand [$l_i_compteur]["traiter"] = REFUSERCAN;
						}
						else
						{
							// Candidature à remettre en édition
							$l_t_traitementcand [$l_i_compteur]["traiter"] = EDITERCAND;
						}
					}
				}
			}
			break;
		case OUI: // ajout de la possibilité
			// Pas de traitement particulier
			break;
		default:
			// Action non reconnue
			trigger_error ("Action de traitement de l'ouverture~fermeture de la race non reconnue", FATAL);
			exit();
			break;
	}
	
	
	// Mise en base
	// La race
	$l_t_st_resultat = requete ("majrace", $g_st_idrace [CONTENU_VAR], $g_st_actionrace [CONTENU_VAR]);
		
	// Les profils
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
	{
		if ($l_t_traitementprofil [$l_i_compteur]["traiter"] == OUI)
		{
			$l_t_st_resultat = requete ("change_statut_profil", $l_t_traitementprofil [$l_i_compteur]["idprofil"], HORSLIGNE);
		
			$l_t_st_resultat = requete ("ajout_historique_profil", METTREHORSLIGNETXT, $l_t_traitementprofil [$l_i_compteur]["idprofil"]);
		}
	}
	
	// Les candidatures
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
	{
		if ($l_t_traitementcand [$l_i_compteur]["traiter"] == REFUSERCAN)
		{
			// maj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], REFUSEE); // normalement, inutile
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, REFUSEE, POSITIF);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", REFUSERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);
			
			// mailer l'utilisateur
		}
		elseif ($l_t_traitementcand [$l_i_compteur]["traiter"] == EDITERCAND)
		{
			// màj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], EDITIONUSER);
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, EDITIONUSER, NUL);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", EDITERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);	
			
			// mailer l'utilisateur

		}
	}
	$l_i_flagsucces = OUI;
}

/*******************************
 *  Pour les classes
*******************************/

if ($g_st_iterationclasse [CONTENU_VAR] == 1)
{
	// Récupération des données de travail
	$l_st_resultatprofil = requete ("possibilitesprofilclasse", $g_st_idclasse [CONTENU_VAR]);
		
	$l_st_resultatcand = requete ("candassocieclasse", $g_st_idclasse [CONTENU_VAR]);
	
	
	// traitement des profils 
	switch ($g_st_actionclasse [CONTENU_VAR])
	{
		case NON: // retrait de la possibilité
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
			{
				$l_t_traitementprofil [$l_i_compteur]["idprofil"] = $l_st_resultatprofil [1][$l_i_compteur][0];
				$l_t_traitementprofil [$l_i_compteur]["traiter"] = NON;
						
				if ($l_st_resultatprofil [1][$l_i_compteur][1] == 1)
				{
					// Une seule possibilité pour ce profil, il faudra le mettre hors-ligne
					$l_t_traitementprofil [$l_i_compteur]["traiter"] = OUI;
					print ("traiter le profil '".$l_t_traitementprofil [$l_i_compteur]["idprofil"]."'<br>");			
				}
			}

			// traitement des candidatures en conséquence (refuser si le profil est mis hors ligne, remettre en édition si la race est fermée et qu'il reste d'autres possibilités sur ce profil)
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
			{
				$l_t_traitementcand [$l_i_compteur]["idcand"] = $l_st_resultatcand [1][$l_i_compteur][0];
				for ($l_i_compteur2 = 0; $l_i_compteur2 < $l_st_resultatprofil [0]; $l_i_compteur2++)
				{
					if ($l_t_traitementprofil [$l_i_compteur2]["idprofil"] == $l_st_resultatcand [1][$l_i_compteur][1])
					{
						if ($l_t_traitementprofil [$l_i_compteur2]["traiter"] == OUI)
						{
							// Candidature à refuser
							$l_t_traitementcand [$l_i_compteur]["traiter"] = REFUSERCAN;
						}
						else
						{
							// Candidature à remettre en édition
							$l_t_traitementcand [$l_i_compteur]["traiter"] = EDITERCAND;
						}
					}
				}
			}
			break;
		case OUI: // ajout de la possibilité
			// Pas de traitement particulier
			break;
		default:
			// Action non reconnue
			trigger_error ("Action de traitement de l'ouverture~fermeture de la race non reconnue", FATAL);
			exit();
			break;
	}
	
	
	// Mise en base
	// La classe
	$l_t_st_resultat = requete ("majclasse", $g_st_idclasse [CONTENU_VAR], $g_st_actionclasse [CONTENU_VAR]);
		
	// Les profils
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
	{
		if ($l_t_traitementprofil [$l_i_compteur]["traiter"] == OUI)
		{
			$l_t_st_resultat = requete ("change_statut_profil", $l_t_traitementprofil [$l_i_compteur]["idprofil"], HORSLIGNE);
		
			$l_t_st_resultat = requete ("ajout_historique_profil", METTREHORSLIGNETXT, $l_t_traitementprofil [$l_i_compteur]["idprofil"]);
		}
	}
	
	// Les candidatures
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
	{
		if ($l_t_traitementcand [$l_i_compteur]["traiter"] == REFUSERCAN)
		{
			// maj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], REFUSEE); // normalement, inutile
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, REFUSEE, POSITIF);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", REFUSERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);
			
			// mailer l'utilisateur
		}
		elseif ($l_t_traitementcand [$l_i_compteur]["traiter"] == EDITERCAND)
		{
			// màj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], EDITIONUSER);
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, EDITIONUSER, NUL);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", EDITERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);			
			// mailer l'utilisateur
		}
	}	
	$l_i_flagsucces = OUI;
}

/*****************************
 *  Pour les sexes
 *****************************/
if ($g_st_iterationsexe [CONTENU_VAR] == 1)
{
	// Récupération des données de travail
	$l_st_resultatprofil = requete ("possibilitesprofilsexe", $g_st_idsexe [CONTENU_VAR]);
		
	$l_st_resultatcand = requete ("candassociesexe", $g_st_idsexe [CONTENU_VAR]);
	
	
	// traitement des profils 
	switch ($g_st_actionsexe [CONTENU_VAR])
	{
		case NON: // retrait de la possibilité
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
			{
				$l_t_traitementprofil [$l_i_compteur]["idprofil"] = $l_st_resultatprofil [1][$l_i_compteur][0];
				$l_t_traitementprofil [$l_i_compteur]["traiter"] = NON;
						
				if ($l_st_resultatprofil [1][$l_i_compteur][1] == 1)
				{
					// Une seule possibilité pour ce profil, il faudra le mettre hors-ligne
					$l_t_traitementprofil [$l_i_compteur]["traiter"] = OUI;
				}
			}

			// traitement des candidatures en conséquence (refuser si le profil est mis hors ligne, remettre en édition si la race est fermée et qu'il reste d'autres possibilités sur ce profil)
			for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
			{
				$l_t_traitementcand [$l_i_compteur]["idcand"] = $l_st_resultatcand [1][$l_i_compteur][0];
				for ($l_i_compteur2 = 0; $l_i_compteur2 < $l_st_resultatprofil [0]; $l_i_compteur2++)
				{
					if ($l_t_traitementprofil [$l_i_compteur2]["idprofil"] == $l_st_resultatcand [1][$l_i_compteur][1])
					{
						if ($l_t_traitementprofil [$l_i_compteur2]["traiter"] == OUI)
						{
							// Candidature à refuser
							$l_t_traitementcand [$l_i_compteur]["traiter"] = REFUSERCAN;
						}
						else
						{
							// Candidature à remettre en édition
							$l_t_traitementcand [$l_i_compteur]["traiter"] = EDITERCAND;
						}
					}
				}
			}
			break;
		case OUI: // ajout de la possibilité
			// Pas de traitement particulier
			break;
		default:
			// Action non reconnue
			trigger_error ("Action de traitement de l'ouverture~fermeture de la race non reconnue", FATAL);
			exit();
			break;
	}
	
	
	// Mise en base
	// Le sexe
	$l_t_st_resultat = requete ("majsexe", $g_st_idsexe [CONTENU_VAR], $g_st_actionsexe [CONTENU_VAR]);
		
	// Les profils
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatprofil [0]; $l_i_compteur++)
	{
		if ($l_t_traitementprofil [$l_i_compteur]["traiter"] == OUI)
		{
			$l_t_st_resultat = requete ("change_statut_profil", $l_t_traitementprofil [$l_i_compteur]["idprofil"], HORSLIGNE);
		
			$l_t_st_resultat = requete ("ajout_historique_profil", METTREHORSLIGNETXT, $l_t_traitementprofil [$l_i_compteur]["idprofil"]);
		}
	}
	
	// Les candidatures
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatcand [0]; $l_i_compteur++)
	{
		if ($l_t_traitementcand [$l_i_compteur]["traiter"] == REFUSERCAN)
		{
			// maj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], REFUSEE); // normalement, inutile
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, REFUSEE, POSITIF);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", REFUSERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);
			
			// mailer l'utilisateur
		}
		elseif ($l_t_traitementcand [$l_i_compteur]["traiter"] == EDITERCAND)
		{
			// màj bdd
			//$l_t_st_resultat = requete ("changestatut_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], EDITIONUSER);
			
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $l_t_traitementcand [$l_i_compteur]["idcand"], FERMETURETXT, EDITIONUSER, NUL);
			
			$l_t_st_resultat = requete ("ajout_historique_candidature", EDITERCANTXT, $l_t_traitementcand [$l_i_compteur]["idcand"]);			
			// mailer l'utilisateur
			

		}
	}
	
	$l_i_flagsucces = OUI;	

}

// Affichage
$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "traiteouvertures_niv1");	
if ($l_i_flagsucces == OUI)
{
	$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "succesouvertures_niv1");	
	$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "succesouvertures_niv1");	
}
else
{
	$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "echecouvertures_niv1");	
	$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "echecouvertures_niv1");		
}

$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "traiteouvertures_niv1");						
							
$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminouvertures_niv1, "traiteouvertures_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");		

?>
