<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : admin_niv1_gestionnews.inc                                                                                 
* Fonction du fichier : permet de gérer, éditer et poster des news                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  27/09/2007                                                                             
* Version actuelle :  1.0 au 27/09/2007                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handleradmingestionnews_niv1,  "admingestionnews_niv1");


$l_o_template -> closeSession ($l_i_handleradmingestionnews_niv1, "admingestionnews_niv1");


$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmingestionnews_niv1, "admingestionnews_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
