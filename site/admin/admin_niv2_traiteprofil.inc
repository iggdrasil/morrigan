<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : admin_niv2_traiteprofil.inc                                                                                 
* Fonction du fichier : permet de traiter toutes les actions ne nécessitant pas l'édition
* d'un profil
*
*                                                               
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                         
* Date de création :  20/09/2007                                                                             
* Version actuelle : 1.0 au 20/09/2007                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_resultat_requete = array (0, array ());
$l_t_st_equivalencedemande = array (DEMANDEENLIGNE => DEMANDEENLIGNETXT, DEMANDEHORSLIGNE => DEMANDEHORSLIGNETXT, DEMANDEEDITER => DEMANDEEDITERTXT);
$l_i_actionok = NON; // permet de savoir si on peut changer le statut ou effacer le profil

/* Contenu et inclusion des fichier de contenu */
if (
		$g_st_actionprofil [CONTENU_VAR] == DEMANDEENLIGNE 
	||
		$g_st_actionprofil [CONTENU_VAR] ==  DEMANDEHORSLIGNE 
	|| 
		$g_st_actionprofil [CONTENU_VAR] == DEMANDEEDITER
	)
{
	if ($g_i_erreur == OUI)
	{
		$l_o_template -> addSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");
		
		affiche_variable ($g_st_debat, "text", $l_i_handlertraiteprofil_niv2, NULL);
		
		$g_st_iteration [CONTENU_VAR] = 2;
    	$g_st_iteration [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_iteration, "input", $l_i_handlertraiteprofil_niv2, NULL);
    
    	$g_st_actionprofil [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_actionprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);   

    	$g_st_idprofil [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_idprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);
    	
		$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");	
	}
	else	
	{
		// Requête de mise à jour de la base de données		
		$l_st_resultat_requete = requete ("demande_profil", $g_st_idprofil [CONTENU_VAR], $g_st_actionprofil [CONTENU_VAR], $l_t_st_equivalencedemande [$g_st_actionprofil [CONTENU_VAR]]);
		if ($l_st_resultat_requete ["nbaffecte"] != 1)
		{
			trigger_error ("Enregistrement de la demande impossible pour le profil ''", ERROR);
			exit ();
		}
		
		// Ajout du commentaire si nécessaire	
		if ($g_st_debat [CONTENU_VAR] != "")
		{
			$l_st_resultat_requete = requete ("commente_profil", $g_st_debat [CONTENU_VAR], $g_st_idprofil [CONTENU_VAR]);
			if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout du commentaire n'a pas pas pu etre fait en bdd lors de la création du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}	
		}
		
		// TRES IMPORTANT pour qu'on sache si la mise à jour a fonctionné
        $g_i_succesrequete = OUI;		
		
	}	
}
elseif 
	(
		$g_st_actionprofil [CONTENU_VAR] == STATUT
	||
		$g_st_actionprofil [CONTENU_VAR] == EFFACER
	)
{
	// requête pour vérifier s'il n'y a pas de candidature associée à ce profil
	$l_st_resultat_requete = requete ("pas_candidat", $g_st_idprofil [CONTENU_VAR]);
	if ($l_st_resultat_requete [0] == 0)
	{
		// Toutes les actions sont permises
		$l_i_actionok = OUI;
	}
	elseif 
		(
			$l_st_resultat_requete [0] > 0 
			&&
			(
				$g_st_actionprofil [CONTENU_VAR] != EFFACER
			|| 
				(
					$g_st_actionprofil [CONTENU_VAR] == STATUT
				&&
					$g_st_statut [CONTENU_VAR][0] != EDITION
				)
			)
		)
	{	
		// Action permise dans ces cas-là uniquement, pusiqu'une candidature est associée au profil
		$l_i_actionok = OUI;
	}
	else 
	{
		// On ne peut pas effectuer l'action
		$l_i_actionok = NON;		
	}
	
	if ($l_i_actionok = OUI)
	{
		if ($g_st_actionprofil [CONTENU_VAR] == STATUT && $g_i_erreur == NON)
		{
			$l_st_resultat_requete = requete ("change_statut_profil", $g_st_idprofil [CONTENU_VAR], $g_st_statut [CONTENU_VAR][0]);
			if ($l_st_resultat_requete ["nbaffecte"] != 1)
			{
				trigger_error ("Le statut du profil '".$g_st_idprofil [CONTENU_VAR]."' n'a pas pu être changé en '".$g_st_statut [CONTENU_VAR][0]."'", FATAL);
				exit ();
			}
			
			// Ajout du commentaire si nécessaire	
			if ($g_st_debat [CONTENU_VAR] != "")
			{
				$l_st_resultat_requete = requete ("commente_profil", $g_st_debat [CONTENU_VAR], $g_st_idprofil [CONTENU_VAR]);
				if ($l_st_resultat_requete [nbaffecte] != 1)
        		{
        		    // Gros plantage dans la requete 
            		trigger_error ("L'ajout du commentaire n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            		exit ();                   
        		}	
			}
				
			// Ajout dans l'historique
			$l_st_resultat_requete = requete ("ajout_historique_profil", STATUTTXT, $g_st_idprofil [CONTENU_VAR]);
			if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}
        	
        	// TRES IMPORTANT pour qu'on sache si la mise à jour a fonctionné
       		$g_i_succesrequete = OUI;	
			
		}
		elseif ($g_st_actionprofil [CONTENU_VAR] == STATUT && $g_i_erreur == OUI)
		{
			$l_o_template -> addSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");
		
			affiche_variable ($g_st_debat, "text", $l_i_handlertraiteprofil_niv2, NULL);
		
			$g_st_iteration [CONTENU_VAR] = 2;
    		$g_st_iteration [TYPE_DISPLAY] = HIDDEN;
    		affiche_variable ($g_st_iteration, "input", $l_i_handlertraiteprofil_niv2, NULL);
    
    		$g_st_actionprofil [TYPE_DISPLAY] = HIDDEN;
    		affiche_variable ($g_st_actionprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);   

    		$g_st_idprofil [TYPE_DISPLAY] = HIDDEN;
    		affiche_variable ($g_st_idprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);
    	
			$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");	
		}
		else
		{
			$l_st_resultat_requete = requete ("efface_profil", $g_st_idprofil [CONTENU_VAR]);
			if ($l_st_resultat_requete ["nbaffecte"] == 0)
			{
				trigger_error ("Le profil '".$g_st_idprofil [CONTENU_VAR]."' n'a pas pu être effacé", FATAL);
				exit ();
			}
			
			// TRES IMPORTANT pour qu'on sache si la mise à jour a fonctionné
        	$g_i_succesrequete = OUI;	
		}
	}
	else 
	{
		// $l_i_actionok = NON
		$l_o_template -> addSession ($l_i_handlertraiteprofil_niv2, "traitementimpossible_niv2");
		$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv2, "traitementimpossible_niv2");					
	}
		
}
elseif ($g_st_actionprofil [CONTENU_VAR] == REFUSER)
{
	if ($g_i_erreur == OUI)
	{
		$l_o_template -> addSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");
		
		affiche_variable ($g_st_debat, "text", $l_i_handlertraiteprofil_niv2, NULL);
		
		$g_st_iteration [CONTENU_VAR] = 2;
    	$g_st_iteration [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_iteration, "input", $l_i_handlertraiteprofil_niv2, NULL);
    
    	$g_st_actionprofil [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_actionprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);   

    	$g_st_idprofil [TYPE_DISPLAY] = HIDDEN;
    	affiche_variable ($g_st_idprofil, "input", $l_i_handlertraiteprofil_niv2, NULL);
    	
		$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv2, "traitementprofil_demande_niv2");			
	}
	else
	{
		$l_st_resultat_requete = requete ("refuse_action", $g_st_idprofil [CONTENU_VAR]);
		if ($l_st_resultat_requete ["nbaffecte"] == 0)
		{
			trigger_error ("La demande de changement du statut du profil '".$g_st_idprofil [CONTENU_VAR]."' n'a pas pu être effacée.", FATAL);
			exit ();
		}
			
		// Ajout du commentaire si nécessaire	
		if ($g_st_debat [CONTENU_VAR] != "")
		{
			$l_st_resultat_requete = requete ("commente_profil", $g_st_debat [CONTENU_VAR], $g_st_idprofil [CONTENU_VAR]);
			if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        		// Gros plantage dans la requete 
            	trigger_error ("L'ajout du commentaire n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}	
		}
				
		// Ajout dans l'historique
		$l_st_resultat_requete = requete ("ajout_historique_profil", REFUSERTXT, $g_st_idprofil [CONTENU_VAR]);
		if ($l_st_resultat_requete [nbaffecte] != 1)
        {
        	// Gros plantage dans la requete 
        	trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            exit ();                   
        }
        	
        	// TRES IMPORTANT pour qu'on sache si la mise à jour a fonctionné
       		$g_i_succesrequete = OUI;	
	}	
}
/* Fin de fichier **************************************************************************************/
?>
