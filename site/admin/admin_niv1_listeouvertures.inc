<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_listeouvertures.inc
* Fonction du fichier : permet de traiter les ouvertures/fermetures des races/classes/sexes
*									à l'inscription : listage des possibilités
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  01/09/2008
* Version actuelle : 1.0 au 01/09/2008
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0; // compteur pour les boucles for
$l_st_affichage = array ("", "", NORMAL);
$l_st_resultatrace = array ();
$l_st_resultatclasse = array ();
$l_st_resultatsexe = array ();
$l_t_st_options = array ();

/* Contenu et inclusion des fichier de contenu */

// Récupération des informations
$l_st_resultatrace = requete ("raceouverte_admin");
if ($l_st_resultatrace [0] == 0)
{
	trigger_error ("Il est impossible de lister les races ouvertes à l'inscription", FATAL);
	exit ();
}


$l_st_resultatclasse = requete ("classeouverte_admin");
if ($l_st_resultatclasse [0] == 0)
{
	trigger_error ("Il est impossible de lister les classes ouvertes à l'inscription", FATAL);
	exit ();
}

$l_st_resultatsexe = requete ("sexeouvert_admin");
if ($l_st_resultatsexe [0] == 0)
{
	trigger_error ("Il est impossible de lister les sexes ouverts à l'inscription", FATAL);
	exit ();
}

// Affichage du formulaire
$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "listeouvertures_niv1");	

// Pour les races
for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatrace [0]; $l_i_compteur++)
{
	$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "ligne_race");		
	$l_o_template -> setVar ($l_i_handleradminouvertures_niv1, "ligne_race.nomrace", $l_st_resultatrace [1][$l_i_compteur][1]);
	
	$g_st_iterationrace [CONTENU_VAR] = 1;
	$g_st_idrace [CONTENU_VAR] = $l_st_resultatrace [1][$l_i_compteur][0];
	
	affiche_variable ($g_st_iterationrace, "input", $l_i_handleradminouvertures_niv1, NULL);
	affiche_variable ($g_st_idrace, "input", $l_i_handleradminouvertures_niv1, NULL);
	
	$g_st_actionrace [CONTENU_VAR] = $l_st_resultatrace [1][$l_i_compteur][2];

	affiche_variable ($g_st_actionrace, "radio", $l_i_handleradminouvertures_niv1, NULL);

	$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "ligne_race");			
}

// Pour les classes
for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatclasse [0]; $l_i_compteur++)
{
	$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "ligne_classe");		
	$l_o_template -> setVar ($l_i_handleradminouvertures_niv1, "ligne_classe.nomclasse", $l_st_resultatclasse [1][$l_i_compteur][1]);
	
	$g_st_iterationclasse [CONTENU_VAR] = 1;
	$g_st_idclasse [CONTENU_VAR] = $l_st_resultatclasse [1][$l_i_compteur][0];
	
	affiche_variable ($g_st_iterationclasse, "input", $l_i_handleradminouvertures_niv1, NULL);
	affiche_variable ($g_st_idclasse, "input", $l_i_handleradminouvertures_niv1, NULL);
	
	$g_st_actionclasse [CONTENU_VAR] = $l_st_resultatclasse [1][$l_i_compteur][2];

	affiche_variable ($g_st_actionclasse, "radio", $l_i_handleradminouvertures_niv1, NULL);

	$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "ligne_classe");			
}

// Pour les sexes
for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultatsexe [0]; $l_i_compteur++)
{
	$l_o_template -> 	addSession ($l_i_handleradminouvertures_niv1, "ligne_sexe");		
	$l_o_template -> setVar ($l_i_handleradminouvertures_niv1, "ligne_sexe.nomsexe", $l_st_resultatsexe [1][$l_i_compteur][1]);
	
	$g_st_iterationsexe [CONTENU_VAR] = 1;
	$g_st_idsexe [CONTENU_VAR] = $l_st_resultatsexe [1][$l_i_compteur][0];
	
	affiche_variable ($g_st_iterationsexe, "input", $l_i_handleradminouvertures_niv1, NULL);
	affiche_variable ($g_st_idsexe, "input", $l_i_handleradminouvertures_niv1, NULL);
	
	$g_st_actionsexe [CONTENU_VAR] = $l_st_resultatsexe [1][$l_i_compteur][2];

	affiche_variable ($g_st_actionsexe, "radio", $l_i_handleradminouvertures_niv1, NULL);

	$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "ligne_sexe");			
}


$l_o_template -> closeSession ($l_i_handleradminouvertures_niv1, "listeouvertures_niv1");						
							
$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminouvertures_niv1, "listeouvertures_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");		
/* Fin de fichier **************************************************************************************/
?>
