<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_listecandidature.inc
* Fonction du fichier : permet de lister les candidatures selon leur statut
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  06/10/2007
* Version actuelle : 1.0 au 06/10/2007
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : effacer toutes les traces de debug qui trainent sur la page
*
******************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALE
$g_t_st_infoscandidaturebase = array (0, array (array ())); // permet de récupérer les infos des candidatures à lister
$g_i_compteur = 0; // sert pour les boucles for
$l_i_compteur2 = 0; // sert pour la construction des tableaux sur la page
$g_i_compteur3 = 0; // parcourt les statuts
$l_i_affichageok = NON;
$l_t_st_equivalencecandidature = array (EDITIONUSERTXT, RELECTUREMJTXT, AVISMJTXT, ENATTENTETXT, ABANDONNEETXT, ACCEPTEETXT, REFUSEETXT);

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlerlistecandidature_niv1,  "listecandidature_niv1");

// Le MJ a les droits d'inscription, critère vérifié dans admin_candidature

// Le critère de tri est un tri par profil


$g_t_st_infoscandidaturebase = requete ("infos_candidature_base");
if ($g_t_st_infoscandidaturebase [0] == 0)
{
    // Pas de réponse : erreur
    trigger_error ("Pas de candidatures en base.",  NOTICE);
	$l_o_template -> addSession ($l_i_handlerlistecandidature_niv1, "pas_candidatures");
	$l_o_template -> closeSession ($l_i_handlerlistecandidature_niv1, "pas_candidatures");
}
else
{
	// Boucle d'affichage des candidatures par statut
	for ($g_i_compteur3 = 0; $g_i_compteur3 < 7; $g_i_compteur3++)
	{
		for ($g_i_compteur = 0; $g_i_compteur < $g_t_st_infoscandidaturebase [0]; $g_i_compteur++)
		{
			$l_i_affichageok = NON;
			
			switch ($g_i_compteur3)
			{
				case EDITIONUSER:
				case RELECTUREMJ:
				case ENATTENTE:
				case ABANDONNEE:
				case ACCEPTEE:
				case REFUSEE:
					if (($_SESSION ['droitrecrutement'] == OUI) && ($_SESSION ['droitinscription'] == OUI))
					{
						$l_i_affichageok = OUI;	
					}
					break;
				case AVISMJ:
					if ($_SESSION ['droitinscription'] == OUI)
					{
						$l_i_affichageok = OUI;	
					}    		
					break;
				default: 
					break;
			}
			
			if (
				$l_i_affichageok == OUI
				&&
				$g_i_compteur3 == $g_t_st_infoscandidaturebase [1][$g_i_compteur][5] // compléter avec la colonne statut de la requete
				)

			{
				// Ce profil requiert une action, on l'affiche donc dans la table
				if ($l_i_compteur2 == 0)
				{
					// Ouvrir le tableau
					$l_o_template -> addSession ($l_i_handlerlistecandidature_niv1, "liste_table");
					$l_o_template -> setVar ($l_i_handlerlistecandidature_niv1, "liste_table.statut", stripslashes ($l_t_st_equivalencecandidature [$g_i_compteur3]));
					$l_i_compteur2++;
				}

				// Afficher une ligne du tableau
				affiche_ligne_candidature ("candidature_tableau");
			}
		}

		if ($l_i_compteur2 > 0)
		{
			// Fermer le tableau
			$l_o_template -> closeSession ($l_i_handlerlistecandidature_niv1, "liste_table");
		}

		$l_i_compteur2 = 0; // Remise à 0 compteur de construction de page	
	}
}

$l_o_template -> closeSession ($l_i_handlerlistecandidature_niv1,  "listecandidature_niv1");


$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerlistecandidature_niv1, "listecandidature_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
