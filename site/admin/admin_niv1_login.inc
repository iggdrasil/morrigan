<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : admin_niv1_login.inc                                                                                 
* Fonction du fichier : affiche le cartouche de login de l'administration                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  08/05/2004                                                                             
* Version actuelle : 1.0 au 08/05/2004                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_var_login = array ("login", "", 0);
$l_st_var_password = array ("password", "", 0);

/* Contenu et inclusion des fichier de contenu */
if ($l_i_loginerr == 1)
{
    // Ici, on triche, seul le champ login sera considéré comme faux, pour de simples raisons d'affichage
    // de message d'erreur
    $l_st_var_login [2] = 2;
}


$l_o_template -> addSession ($l_i_handlerloginadmin_niv1,  "loginadmin_niv1");

affiche_variable ($l_st_var_login, "input", $l_i_handlerloginadmin_niv1, NULL);
affiche_variable ($l_st_var_password, "password", $l_i_handlerloginadmin_niv1, NULL);


$l_o_template -> closeSession ($l_i_handlerloginadmin_niv1, "loginadmin_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerloginadmin_niv1, "loginadmin_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


/* Fin de fichier ***********************************************************************************/
?>

