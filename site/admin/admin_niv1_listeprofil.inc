<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_listeprofil.inc
* Fonction du fichier : permet de lister les profils :
*                 - liste des profils requérant une action
*                 - liste des profils de l'auteur
*                 - liste des autres profils listables par le MJ logué
*                        - Formulaire pour demander la création d'un nouveau profil
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  08/05/2006
* Version actuelle : 2.0 au 17/09/2007
                     1.0 au 08/05/2006
* License du projet : GPL
* Dernières modifications : refonte du système de gestion des profils
* Remarques particulières : effacer toutes les traces de debug qui trainent sur la page
*
******************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALE
$g_t_st_infosprofilbase = array (0, array (array ())); // permet de récupérer les infos des profils à lister
$g_i_compteur = 0; // sert pour les boucles for
$l_i_compteur2 = 0; // sert pour la construction des tableaux sur la page

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlerlisteprofil_niv1,  "listeprofil_niv1");

// Le MJ a les droits d'inscription, critère vérifié dans admin_profil

// Affichage des listes de profils
if ($g_st_criteretri [TYPE_DISPLAY] == ERRONNE)
{
    trigger_error ("Le critère de tri des profils n'est pas valable.", FATAL);
    exit ();
}

switch ($g_st_criteretri [CONTENU_VAR])
{
    case IDPROFIL:
        $g_st_criteretri [CONTENU_VAR] = "t1.PRO_id";
        break;
    case AUTEURPROFIL:
        $g_st_criteretri [CONTENU_VAR] = "t1.PRO_fk_USR";
        break;
    case CATPROFIL:
        $g_st_criteretri [CONTENU_VAR] = "t1.PRO_fk_CAT";
        break;
    case STATUTPROFIL:
        $g_st_criteretri [CONTENU_VAR] = "t1.PRO_statut";
        break;
    default:
        $g_st_criteretri [CONTENU_VAR] = "t1.PRO_id";
        break;
}

$g_t_st_infosprofilbase = requete ("infos_profil_base", $g_st_criteretri [CONTENU_VAR]);
if ($g_t_st_infosprofilbase [0] == 0)
{
    // Pas de réponse : erreur
    trigger_error ("Pas de profils en base.",  FATAL);
    exit ();
}



// 1 - Affichage des profils requérant une action
for ($g_i_compteur = 0; $g_i_compteur < $g_t_st_infosprofilbase [0]; $g_i_compteur++)
{
    if (
            (
                ($_SESSION ['droitrecrutement'] == OUI)
                &&
                ($g_t_st_infosprofilbase [1][$g_i_compteur][2] == RELECTURE
                ||
                 $g_t_st_infosprofilbase [1][$g_i_compteur][6] == DEMANDEENLIGNE
               ||
                 $g_t_st_infosprofilbase [1][$g_i_compteur][6] == DEMANDEHORSLIGNE
               ||
                $g_t_st_infosprofilbase [1][$g_i_compteur][6] == DEMANDEEDITER)
            )
            ||
            (
                ($_SESSION ['droitrecrutement'] == NON)
                &&
                ($g_t_st_infosprofilbase [1][$g_i_compteur][2] == EDITION)
                &&
                ($g_t_st_infosprofilbase [1][$g_i_compteur][1] == $_SESSION ['user'])
            )
        )

    {
        // Ce profil requiert une action, on l'affiche donc dans la table
        if ($l_i_compteur2 == 0)
        {
            // Ouvrir le tableau
            $l_o_template -> addSession ($l_i_handlerlisteprofil_niv1, "liste_req_action");
            $l_i_compteur2++;
        }

        // Afficher une ligne du tableau
        affiche_ligne_profil ("profil_req_action", 1);
        // Ce profil est déjà référencé, il est inutile qu'il le soit une fois de plus dans les autres tableaux
        $g_t_st_infosprofilbase [1][$g_i_compteur][7] = 0;
    }
}

if ($l_i_compteur2 > 0)
{
    // Fermer le tableau
    $l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1, "liste_req_action");
}

$l_i_compteur2 = 0; // Remise à 0 compteur de construction de page

// 2 - Affichage des profils dont le MJ logué est l'auteur
for ($g_i_compteur = 0; $g_i_compteur < $g_t_st_infosprofilbase [0]; $g_i_compteur++)
{
    if ($g_t_st_infosprofilbase [1][$g_i_compteur][1] == $_SESSION ['user'] && $g_t_st_infosprofilbase [1][$g_i_compteur][7] == 1)
    {
        // Ce profil est au MJ logué, on l'affiche donc dans la table (sauf profil en relecture d'un MJ non recruteur)
        if (($_SESSION ["droitrecrutement"] == NON && $g_t_st_infosprofilbase [1][$g_i_compteur][2] != 1) || $_SESSION ["droitrecrutement"] == OUI)
        {        
        	if ($l_i_compteur2 == 0)
        	{
            	// Ouvrir le tableau
            	$l_o_template -> addSession ($l_i_handlerlisteprofil_niv1, "liste_auteur");
            	$l_i_compteur2++;
        	}

        	// Afficher une ligne du tableau
        	affiche_ligne_profil ("profil_auteur", 2);
        	// Ce profil est déjà référencé, il est inutile qu'il le soit une fois de plus dans les autres tableaux
        	$g_t_st_infosprofilbase [1][$g_i_compteur][7] = 0;
        }
    }
}

if ($l_i_compteur2 > 0)
{
    // Fermer le tableau
    $l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1, "liste_auteur");
}

$l_i_compteur2 = 0; // Remise à 0 compteur de construction de page

// 3 - Affichage du reste des profils (ne requérant pas d'action, dont le MJ logué n'est pas l'auteur)
for ($g_i_compteur = 0; $g_i_compteur < $g_t_st_infosprofilbase [0]; $g_i_compteur++)
{
    if (
            (
                $g_t_st_infosprofilbase [1][$g_i_compteur][7] == 1
                &&
                $_SESSION ['droitrecrutement'] == OUI
            )
            ||
            (
                $_SESSION ['droitrecrutement'] == NON
                &&
                $g_t_st_infosprofilbase [1][$g_i_compteur][7] == 1
                &&
                $g_t_st_infosprofilbase [1][$g_i_compteur][2] != EDITION
                &&
                $g_t_st_infosprofilbase [1][$g_i_compteur][2] != RELECTURE
            )
        )

    {
        // Ce profil est encore à afficher, on l'affiche donc dans la table
        if ($l_i_compteur2 == 0)
        {
            // Ouvrir le tableau
            $l_o_template -> addSession ($l_i_handlerlisteprofil_niv1, "liste_reste");
            $l_i_compteur2++;
        }

        // Afficher une ligne du tableau
        affiche_ligne_profil ("profil_reste", 3);
        // Ce profil est déjà référencé, il est inutile qu'il le soit une fois de plus dans les autres tableaux
        $g_t_st_infosprofilbase [1][$g_i_compteur][7] = 0;
    }
}

if ($l_i_compteur2 > 0)
{
    // Fermer le tableau
    $l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1, "liste_reste");
}

// Affichage du formulaire de demande de création d'un nouveau profil
$l_o_template -> addSession ($l_i_handlerlisteprofil_niv1, "creer_profil");
$l_st_variableaction = array ("action"."4", CREER, HIDDEN);
affiche_variable ($l_st_variableaction, "input", $l_i_handlerlisteprofil_niv1, NULL);

$l_st_variableaction [NOM_VAR] = "iteration";
$l_st_variableaction [CONTENU_VAR] = 1;
$l_st_variableaction [TYPE_DISPLAY] = HIDDEN;
affiche_variable ($l_st_variableaction, "input", $l_i_handlerlisteprofil_niv1, NULL);

$l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1, "creer_profil");


$l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1,  "listeprofil_niv1");


$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerlisteprofil_niv1, "listeprofil_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
