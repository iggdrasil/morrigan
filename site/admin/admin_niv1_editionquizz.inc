<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_.editionquizz.inc
* Fonction du fichier : permet d'éditer et créer des questions et réponses au quizz
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  29/06/2008
* Version actuelle : 1.0 au 29/06/2008
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_t_st_resultat2 = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0; // compteur pour les boucles for d'affichage
$l_i_compteur2 = 0;
$l_i_compteurreponses = 0; // permet de compter le nombre de réponses non vides
$l_i_compteurdoublons = 0; // permet de compter le nombre de doublons
$l_i_compteurbonnes = 0; // permet de compter le nombre de réponses bonnes
$l_st_affichage = array ("", "", NORMAL);
$l_t_reponses = array (); // sert aux vérifications sur les réponses
$l_i_idquestion = 0; // sert à l'enregistrement d'une nouvelle question
$l_s_flagsucces = "";


/* Contenu et inclusion des fichier de contenu */
switch ($g_st_iteration [CONTENU_VAR])
{
	case "0":
		$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizz_niv1");
		
		$g_st_iteration [CONTENU_VAR]++;
		
		// Pas d'action, juste une liste de questions à afficher dans un select
		$l_t_st_resultat = requete ("liste_questions");
		if ($l_t_st_resultat [0] == NULL)
		{
			trigger_error ("Erreur dans la récupération de la liste de questions", NOTICE);
			$l_t_st_resultat [0] = 0;
		}
		
		if ($l_t_st_resultat [0] == 0)
		{
			$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "pasdequestion");
			$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "pasdequestion");	
		}
		else
		{
			$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "listequestions");
			$g_st_idquestion [TYPE_DISPLAY] = NORMAL;
			affiche_variable ($g_st_idquestion, "select", $l_i_handleradminquizz_niv1, $l_t_st_resultat);
			$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "listequestions.actionquizz", CORRIGERQUESTION);
			$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "listequestions");	
		}
		
		
		
		$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "creerquestion");
		$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "creerquestion.actionquizz", CREERQUESTION);

		$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "creerquestion");			

		affiche_variable ($g_st_iteration, "input", $l_i_handleradminquizz_niv1, NULL);

		$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizz_niv1");

		$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
		$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminquizz_niv1, "editionquizz_niv1");
		$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


		break;	
		
	case "1":


		$g_st_iteration [CONTENU_VAR]++;

		if ($g_st_actionquizz [CONTENU_VAR]!=CREERQUESTION)
		{
			// On affiche la question à corriger (ou des champs vides si on crée une nouvelle question)
			$l_t_st_resultat = requete ("recupere_question", $g_i_numeroquestion);
			if ($l_t_st_resultat [0] == 0)
			{
				trigger_error ("La question '".$g_st_idquestion [CONTENU_VAR]."' n'a pas pu être affichée", FATAL);
				exit ();
			}
		
			$l_t_st_resultat2 = requete ("recupere_reponses", $g_st_idquestion [CONTENU_VAR]);
			if ($l_t_st_resultat2 [0] == 0)
			{
				trigger_error ("Il n'y a pas de réponse à la question '".$g_st_idquestion [CONTENU_VAR]."'", FATAL);
				exit ();
			}
		}


		
		$g_st_idquestion [CONTENU_VAR] = $l_t_st_resultat [1][0][0];
		$g_st_question [CONTENU_VAR] = $l_t_st_resultat [1][0][1];

		$g_st_idreponse1 [CONTENU_VAR] = $l_t_st_resultat2 [1][0][0];
		$g_st_idreponse2 [CONTENU_VAR] = $l_t_st_resultat2 [1][1][0];			
		$g_st_idreponse3 [CONTENU_VAR] = $l_t_st_resultat2 [1][2][0];			
		$g_st_idreponse4 [CONTENU_VAR] = $l_t_st_resultat2 [1][3][0];
		$g_st_idreponse5 [CONTENU_VAR] = $l_t_st_resultat2 [1][4][0];	
		
		$g_st_reponse1 [CONTENU_VAR] = $l_t_st_resultat2 [1][0][1];	
		$g_st_reponse2 [CONTENU_VAR] = $l_t_st_resultat2 [1][1][1];	
		$g_st_reponse3 [CONTENU_VAR] = $l_t_st_resultat2 [1][2][1];	
		$g_st_reponse4 [CONTENU_VAR] = $l_t_st_resultat2 [1][3][1];	
		$g_st_reponse5 [CONTENU_VAR] = $l_t_st_resultat2 [1][4][1];	
											
		$g_st_reponsebonne1 [CONTENU_VAR] = $l_t_st_resultat2 [1][0][2];
		$g_st_reponsebonne2 [CONTENU_VAR] = $l_t_st_resultat2 [1][1][2];
		$g_st_reponsebonne3 [CONTENU_VAR] = $l_t_st_resultat2 [1][2][2];
		$g_st_reponsebonne4 [CONTENU_VAR] = $l_t_st_resultat2 [1][3][2];
		$g_st_reponsebonne5 [CONTENU_VAR] = $l_t_st_resultat2 [1][4][2];

		$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizz2_niv1");
		
		affiche_variable ($g_st_idquestion, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_question, "textarea", $l_i_handleradminquizz_niv1, NULL);
		
		affiche_variable ($g_st_idreponse1, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponse1, "textarea", $l_i_handleradminquizz_niv1, NULL);		
		affiche_variable ($g_st_reponsebonne1, "checkbox", $l_i_handleradminquizz_niv1, NULL);
				
		affiche_variable ($g_st_idreponse2, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponse2, "textarea", $l_i_handleradminquizz_niv1, NULL);	
		affiche_variable ($g_st_reponsebonne2, "checkbox", $l_i_handleradminquizz_niv1, NULL);
		
		affiche_variable ($g_st_idreponse3, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponse3, "textarea", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponsebonne3, "checkbox", $l_i_handleradminquizz_niv1, NULL);		
					
		affiche_variable ($g_st_idreponse4, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponse4, "textarea", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponsebonne4, "checkbox", $l_i_handleradminquizz_niv1, NULL);		
							
		affiche_variable ($g_st_idreponse5, "input", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponse5, "textarea", $l_i_handleradminquizz_niv1, NULL);
		affiche_variable ($g_st_reponsebonne5, "checkbox", $l_i_handleradminquizz_niv1, NULL);					
		
		if ($g_st_actionquizz [CONTENU_VAR]!=CREERQUESTION)
		{
			$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "actions_niv1");
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.edition", CORRIGERQUESTION);
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.editiontxt", CORRIGERQUESTIONTXT);
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.suppression", EFFACERQUESTION);	
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.suppressiontxt", EFFACERQUESTIONTXT);		
			$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "actions_niv1");			
		}
		else
		{
			$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "creation_niv1");
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "creation_niv1.creation", CREERQUESTION);
				$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "creation_niv1.creationtxt", CREERQUESTIONTXT);
			$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "creation_niv1");				
		}
		
		affiche_variable ($g_st_iteration, "input", $l_i_handleradminquizz_niv1, NULL);
		
		$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizz2_niv1");

		$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
		$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminquizz_niv1, "editionquizz2_niv1");
		$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

		break;	
			
	case "2":
		if ($g_st_actionquizz [CONTENU_VAR]==EFFACERQUESTION)
		{
			// Requete d'effacement de la question et de ses réponses associées	
			$l_st_resultat = requete ("effacerquestion", $g_st_idquestion [CONTENU_VAR]);
			
			if ($l_st_resultat ["nbaffecte"] > 0)
			{
				// Succès
				$l_s_flagsucces = "effacer";
			}
			
			else 
			{
				trigger_error ("La question '".$g_st_idquestion [CONTENU_VAR]."' du quizz n'a pas pu être effacée.", FATAL);	
				exit ();
			}
		}
		else
		{
		print ("Contrôle des réponses proposées <br>");	
			// Construction du tableau des réponses à traiter
			$l_t_reponses = array (array ($g_st_reponse1 [CONTENU_VAR], $g_st_reponse2 [CONTENU_VAR], $g_st_reponse3 [CONTENU_VAR], $g_st_reponse4 [CONTENU_VAR], $g_st_reponse5 [CONTENU_VAR]), array ($g_st_reponsebonne1 [CONTENU_VAR][0], $g_st_reponsebonne2 [CONTENU_VAR][0], $g_st_reponsebonne3 [CONTENU_VAR][0], $g_st_reponsebonne4 [CONTENU_VAR][0], $g_st_reponsebonne5 [CONTENU_VAR][0]), array (), array ());
			
			// contenu des réponses : texte ou vide?
			if ($g_st_reponse1 [CONTENU_VAR] != "")
			{
				$l_t_reponses [2][0] = OUI; // texte
			}
			else 
			{
				$l_t_reponses [2][0] = NON;	// vide
			}
			
			if ($g_st_reponse2 [CONTENU_VAR] != "")
			{
				$l_t_reponses [2][1] = OUI;
			}
			else 
			{
				$l_t_reponses [2][1] = NON;	
			}
			
			if ($g_st_reponse3 [CONTENU_VAR] != "")
			{
				$l_t_reponses [2][2] = OUI;
			}
			else 
			{
				$l_t_reponses [2][2] = NON;	
			}
			
			if ($g_st_reponse4 [CONTENU_VAR] != "")
			{
				$l_t_reponses [2][3] = OUI;
			}
			else 
			{
				$l_t_reponses [2][3] = NON;	
			}
			
			if ($g_st_reponse5 [CONTENU_VAR] != "")
			{
				$l_t_reponses [2][4] = OUI;
			}
			else 
			{
				$l_t_reponses [2][4] = NON;	
			}
			
			// Vérification des doublons pour les réponses non vides, comptage des réponses sous forme de texte; comptage des doublons; comptage des réponses bonnes
			for ($l_i_compteur = 0; $l_i_compteur < NOMBREREPONSES; $l_i_compteur++)
			{
				if ($l_t_reponses [2][$l_i_compteur] == OUI)
				{
					$l_i_compteurreponses ++;
					
					if ($l_t_reponses [1][$l_i_compteur] == OUI)
					{
						$l_i_compteurbonnes ++;
					}
					else
					{
						$l_t_reponses [1][$l_i_compteur] = NON;
					}
					
					for ($l_i_compteur2 = $l_i_compteur+1; $l_i_compteur2 < NOMBREREPONSES; $l_i_compteur2++)
					{
						if ($l_t_reponses [0][$l_i_compteur] == $l_t_reponses [0][$l_i_compteur2])
						{
							$l_t_reponses [3][$l_i_compteur] = OUI;
							$l_t_reponses [3][$l_i_compteur2] = OUI;
							
							$l_i_compteurdoublons ++;
						}
					}
				}
			}
			
			// Traitement des résultats obtenus dans les compteurs
			if ($l_i_compteurreponses == 0)
			{
				$g_i_erreur = OUI;
			}
			
			if ($l_i_compteurdoublons > 0)
			{
				$g_i_erreur = OUI;
			}
			
			if ($l_i_compteurbonnes == 0)
			{
				$g_i_erreur = OUI;
			}	

			// S'il y a une erreur, réaffichage du formulaire avec les notifications d'erreurs	
			if ($g_i_erreur == OUI)			
			{
				$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizz2_niv1");

				$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "erreur_globale_niv1");
				$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "erreur_globale_niv1");
				
				affiche_variable ($g_st_idquestion, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_question, "textarea", $l_i_handleradminquizz_niv1, NULL);
				
				affiche_variable ($g_st_idreponse1, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponse1, "textarea", $l_i_handleradminquizz_niv1, NULL);		
				affiche_variable ($g_st_reponsebonne1, "checkbox", $l_i_handleradminquizz_niv1, NULL);
				
				affiche_variable ($g_st_idreponse2, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponse2, "textarea", $l_i_handleradminquizz_niv1, NULL);	
				affiche_variable ($g_st_reponsebonne2, "checkbox", $l_i_handleradminquizz_niv1, NULL);
				
				affiche_variable ($g_st_idreponse3, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponse3, "textarea", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponsebonne3, "checkbox", $l_i_handleradminquizz_niv1, NULL);		
			
				affiche_variable ($g_st_idreponse4, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponse4, "textarea", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponsebonne4, "checkbox", $l_i_handleradminquizz_niv1, NULL);		
				
				affiche_variable ($g_st_idreponse5, "input", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponse5, "textarea", $l_i_handleradminquizz_niv1, NULL);
				affiche_variable ($g_st_reponsebonne5, "checkbox", $l_i_handleradminquizz_niv1, NULL);					
				
				if ($g_st_actionquizz [CONTENU_VAR]!=CREERQUESTION)
				{
					$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "actions_niv1");
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.edition", CORRIGERQUESTION);
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.editiontxt", CORRIGERQUESTIONTXT);
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.suppression", EFFACERQUESTION);	
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "actions_niv1.suppressiontxt", EFFACERQUESTIONTXT);		
					$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "actions_niv1");			
				}
				else
				{
					$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "creation_niv1");
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "creation_niv1.creation", CREERQUESTION);
						$l_o_template -> setVar ($l_i_handleradminquizz_niv1, "creation_niv1.creationtxt", CREERQUESTIONTXT);
					$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "creation_niv1");				
				}
				
				affiche_variable ($g_st_iteration, "input", $l_i_handleradminquizz_niv1, NULL);			
				$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizz2_niv1");

				$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
				$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminquizz_niv1, "editionquizz2_niv1");
				$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");			
			}
			else
			{
				// Pas d'erreur, on traite l'ensemble
				if ($g_st_actionquizz [CONTENU_VAR] == CREERQUESTION)
				{
					// Créer de 0, requete d'insert
					// insérer la question
					$l_st_resultat = requete ("ajoutequestionquizz", $g_st_question [CONTENU_VAR]);
					
					if ($l_st_resultat ["nbaffecte"] == 0)
					{
						trigger_error ("La nouvelle question n'a pas pu être enregistrée en base de données.", FATAL);
						exit ();
					}
					
					$l_i_idquestion = $l_st_resultat ["dernier_id_affecte"];	
					
					// insérer les réponses
					for ($l_i_compteur = 0 ; $l_i_compteur < NOMBREREPONSES; $l_i_compteur++)
					{
						if ($l_t_reponses [2][$l_i_compteur] == OUI)
						$l_st_resultat = requete ("ajoutereponsequizz", $l_t_reponses [0][$l_i_compteur], $l_t_reponses [1][$l_i_compteur], $l_i_idquestion);
						if ($l_st_resultat ["nbaffecte"] == 0)
						{
							trigger_error ("La nouvelle réponse n'a pas pu être enregistrée en base de données.", FATAL);
							exit ($l_st_resultat);								
						}
					}
					
					$l_s_flagsucces = "creer";
				}
				else 
				{
					// corriger, requetes d'update
					$l_i_idquestion = $g_st_idquestion [CONTENU_VAR];
					// corriger la question
					$l_st_resultat = requete ("corrigequestion", $l_i_idquestion, $g_st_question [CONTENU_VAR]);
										
					// corriger les réponses
					// supprimer les anciennes réponses
					$l_st_resultat = requete ("effacereponsesquizz", $l_i_idquestion);
					if ($l_st_resultat ["nbaffecte"] < 2)
					{
						trigger_error ("Un nombre insuffisant de reponses ont été effacées pour la question '".$l_i_idquestion."'", FATAL);
						exit ();
					}
					// ajouter les nouvelles réponses
					for ($l_i_compteur = 0 ; $l_i_compteur < NOMBREREPONSES; $l_i_compteur++)
					{
						if ($l_t_reponses [2][$l_i_compteur] == OUI)
						$l_st_resultat = requete ("ajoutereponsequizz", $l_t_reponses [0][$l_i_compteur], $l_t_reponses [1][$l_i_compteur], $l_i_idquestion);
						if ($l_st_resultat ["nbaffecte"] == 0)
						{
							trigger_error ("La nouvelle réponse n'a pas pu être enregistrée en base de données.", FATAL);
							exit ($l_st_resultat);								
						}
					}
					
					$l_s_flagsucces = "corriger";
				}
			}
		}
		
		// En cas de succès, notification de l'utilisateur
		if ($l_s_flagsucces != "")
		{
			$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "succes_niv1");
			switch ($l_s_flagsucces)
			{
				case "effacer":
					$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizzeffacersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizzeffacersucces_niv1");			
					break;
				case "creer":
					$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizzcreersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizzcreersucces_niv1");			
					break;				
				case "corriger":
					$l_o_template -> addSession ($l_i_handleradminquizz_niv1, "editionquizzcorrigersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "editionquizzcorrigersucces_niv1");			
					break;
				default:
					trigger_error ("Le critère de succès '".$l_s_flagsucces."' n'existe pas pour l'édition du quizz.", FATAL);
					exit ();	
			}			
				
			$l_o_template -> closeSession ($l_i_handleradminquizz_niv1, "succes_niv1");						
							
			$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
			$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminquizz_niv1, "succes_niv1");
			$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");		
		}
		break;		
	default:
		trigger_error ("Cette iteration est inconnue pour le traitement du quizz", FATAL);
		exit ();
		break;			
}



/* Fin de fichier ***********************************************************************************/
?>

