<?php
/***************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_traiteprofil.inc
* Fonction du fichier : permet de traiter les profils soumis via admin_profil
*
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  27/05/2007
* Version actuelle : 2.0 au 28/09/2007
* License du projet : GPL
* Dernières modifications : 2.0 au 28/09/2007
* 							1.0 au 27/05/2007
* Remarques particulières :
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$g_i_succesrequete = NON; // permet de savoir si le traitement s'est passé avec succès

/* Contenu et inclusion des fichier de contenu */
// Le MJ a les droits d'inscription, critère vérifié dans admin_profil : pas besoin de le revérifier
$l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "traiteprofil_niv1");


if (
		$g_st_actionprofil [CONTENU_VAR] == CREER 
	|| 
		$g_st_actionprofil [CONTENU_VAR] == CREATIONDAPRES 
	|| 
		$g_st_actionprofil [CONTENU_VAR] == EDITER
	)
{
    // print ("Edition du profil<br>");
    include ("admin/admin_niv2_editeprofil.inc");
}
elseif (
		$g_st_actionprofil [CONTENU_VAR] == DEMANDEENLIGNE
	||
		$g_st_actionprofil [CONTENU_VAR] == DEMANDEHORSLIGNE 
	||
		$g_st_actionprofil [CONTENU_VAR] == DEMANDEEDITER
	||
		$g_st_actionprofil [CONTENU_VAR] == STATUT
	||
		$g_st_actionprofil [CONTENU_VAR] == REFUSER
	||
		$g_st_actionprofil [CONTENU_VAR] == EFFACER
	)
{
    include ("admin/admin_niv2_traiteprofil.inc");
}
else
{
    trigger_error ("Cette action (".$g_st_actionprofil [CONTENU_VAR].") n'est pas permise ou n'existe pas et ne peut donc pas être traitée pour ce profil.", NOTICE);
    exit ();
}

// Tout s'est bien passé ?
if ($g_i_erreur == NON && $g_st_actionprofil [CONTENU_VAR] != AFFICHER && $g_i_succesrequete == OUI)
{
    // /!\ TODO : transformer ce tableau en paramètre global
    $l_t_equivalencestatut = array ("En édition", "En relecture", "Validé et hors ligne", "Validé et en ligne");

    // Afficher que tout s'est bien passé selon l'action de traitement
    $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succes");

    switch ($g_st_actionprofil [CONTENU_VAR])
    {
        case CREER:
        case CREATIONDAPRES:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succescreation");
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succescreation");
            break;
        case EDITER:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succesedition");
            // /!\ TODO : formater pour l'affichage
            $l_o_template -> setVar ($l_i_handlertraiteprofil_niv1, "succesedition.nomprofil", $l_t_i_existeprofil [1][0][3]);
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succesedition");
            break;
        case DEMANDEHORSLIGNE:
        case DEMANDEENLIGNE:
        case DEMANDEEDITER:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succesdemande");
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succesdemande");
            break;
        case STATUT:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "successtatut");
            $l_o_template -> setVar ($l_i_handlertraiteprofil_niv1, "successtatut.nouvstatut", $l_t_equivalencestatut [$g_st_statut [CONTENU_VAR][0]]);
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "successtatut");
            break;
        case EFFACER:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succeseffacer");
            $l_o_template -> setVar ($l_i_handlertraiteprofil_niv1, "succeseffacer.nomprofil", $l_t_i_existeprofil [1][0][3]);
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succeseffacer");
            break;
        case REFUSER:
            $l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "succesrefuser");
            $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succesrefuser");
            break;
        default:
            break;
    }

    $l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "succes");
}

$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "traiteprofil_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlertraiteprofil_niv1, "traiteprofil_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
