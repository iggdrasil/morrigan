<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_affichecandidature.inc
* Fonction du fichier : permet d'afficher une candidature - ou/puis de corriger ses erreurs avant traitement d'action
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  07/10/2007
* Version actuelle : 1.0 au 07/10/2007
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0; // compteur pour les boucles for d'affichage
$l_s_demande = ""; // pour écrire la demande d'action en clair
$g_i_flag_debatpj = NON; // drapeau pour afficher la textarea de commentaire
$g_i_flag_debatmj = NON; // drapeau pour afficher la textarea de commentaire
$g_i_flag_commentaire = NON; // drapeau pour afficher la textarea de commentaire
$g_i_numerocandidature = $g_st_idcandidature [CONTENU_VAR];


$l_st_affichage = array ("", "", NORMAL);
$l_t_equivalencestatutcandidature = array (EDITIONUSERTXT, RELECTUREMJTXT, AVISMJTXT, ENATTENTETXT, ABANDONNEETXT, ACCEPTEETXT, REFUSEETXT);


/* Contenu et inclusion des fichier de contenu */


// Affichage de la candidature
// Le MJ logué a-t-il le droit d'afficher cette candidature?
if (
		$_SESSION ['droitrecrutement'] == NON
	&&
		$g_t_st_infoscandidature [4] != AVISMJ
    )
{
    // Pas de droit d'affichage dans ces cas-là
    trigger_error ("Le MJ ".$_SESSION ['user']." n'a pas le droit d'afficher la candidature ".$g_st_idcandidature [CONTENU_VAR].".", FATAL );
    exit ();
}

$l_o_template -> addSession ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1");

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.joueur", stripslashes ($g_t_st_infoscandidature [1][0][1]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.nomprofil", stripslashes ($g_t_st_infoscandidature [1][0][2]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.descprofil", stripslashes ($g_t_st_infoscandidature [1][0][20]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.nommj", stripslashes ($g_t_st_infoscandidature [1][0][3]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.statut", stripslashes ($l_t_equivalencestatutcandidature [$g_t_st_infoscandidature [1][0][4]]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.nomperso", stripslashes ($g_t_st_infoscandidature [1][0][5]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.race", stripslashes ($g_t_st_infoscandidature [1][0][6]));

if ($g_t_st_infoscandidature [1][0][7] == NON)
{
	$l_o_template -> addSession ($l_i_handleraffichecandidature_niv1, "racefermee");
	$l_o_template -> closeSession ($l_i_handleraffichecandidature_niv1, "racefermee");	
}

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.classe", stripslashes ($g_t_st_infoscandidature [1][0][8]));

if ($g_t_st_infoscandidature [1][0][9] == NON)
{
	$l_o_template -> addSession ($l_i_handleraffichecandidature_niv1, "classefermee");
	$l_o_template -> closeSession ($l_i_handleraffichecandidature_niv1, "classefermee");	
}

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.sexe", stripslashes ($g_t_st_infoscandidature [1][0][10]));

if ($g_t_st_infoscandidature [1][0][11] == NON)
{
	$l_o_template -> addSession ($l_i_handleraffichecandidature_niv1, "sexeferme");
	$l_o_template -> closeSession ($l_i_handleraffichecandidature_niv1, "sexeferme");	
}

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.description", stripslashes ($g_t_st_infoscandidature [1][0][12]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.background", stripslashes ($g_t_st_infoscandidature [1][0][13]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.buts", stripslashes ($g_t_st_infoscandidature [1][0][14]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.debatusrmj", stripslashes ($g_t_st_infoscandidature [1][0][15]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.debatmj", stripslashes ($g_t_st_infoscandidature [1][0][16]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.historique", stripslashes ($g_t_st_infoscandidature [1][0][17]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.commentaire", stripslashes ($g_t_st_infoscandidature [1][0][18]));

$l_o_template -> setVar ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1.datemodification", stripslashes ($g_t_st_infoscandidature [1][0][19]));


$l_o_template -> closeSession ($l_i_handleraffichecandidature_niv1, "affichecandidature_niv1");


/**************************************************
 * Actions possibles à partir de cette candidature
 **************************************************/

$l_o_template -> addSession ($l_i_handleractionscandidature_niv1, "actionscandidature_niv1");

// Toujours
$l_st_affichage [NOM_VAR] = "idcandidature";
$l_st_affichage [CONTENU_VAR] = $g_st_idcandidature [CONTENU_VAR];
$l_st_affichage [TYPE_DISPLAY] = HIDDEN;
affiche_variable ($l_st_affichage, "input", $l_i_handleractionscandidature_niv1, NULL);

$l_st_affichage [NOM_VAR] = "iteration";
$l_st_affichage [CONTENU_VAR] = $g_st_iteration [CONTENU_VAR];
$l_st_affichage [TYPE_DISPLAY] = HIDDEN;
affiche_variable ($l_st_affichage, "input", $l_i_handleractionscandidature_niv1, NULL);


// Actions possibles selon le statut et l'identité du MJ
// print ("Action de la candidature = ".$g_t_st_infoscandidature [1][0][4]."<br>");

switch ($g_t_st_infoscandidature [1][0][4])
{
	case EDITIONUSER:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Actions : changer de statut
			affiche_action_candidature (CHANGERSTATUT);	
		}
		break;
		
	case RELECTUREMJ:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Actions : demander avis, refuser, commenter pj, 	mettre en attente, commenter global, valider
			affiche_action_candidature (VALIDERCAN);
			affiche_action_candidature (COMMENTERPJ);
			affiche_action_candidature (METTREENATTENTE);
			affiche_action_candidature (REFUSERCAN);
			// commentairepj
			affiche_variable ($g_st_commentairepj, "textarea", $l_i_handleractionscandidature_niv1, NULL);
			affiche_action_candidature (DEMANDERAVIS);
			// avismj
			affiche_variable ($g_st_avismj, "textarea", $l_i_handleractionscandidature_niv1, NULL);			
			affiche_action_candidature (COMMENTERGLOBAL);
			// commentaireglobal
			affiche_variable ($g_st_commentaireglobal, "textarea", $l_i_handleractionscandidature_niv1, NULL);
		}
		break;
		
	case AVISMJ:
		if ($_SESSION ["droitinscription"] == OUI)
		{
			// Action : donner avis
			affiche_action_candidature (DONNERAVISMJ);
			// avismj
			affiche_variable ($g_st_avismj, "textarea", $l_i_handleractionscandidature_niv1, NULL);		
		}		
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Actions : changer de statut
			affiche_action_candidature (CHANGERSTATUT);
		}
		break;
				
	case ENATTENTE:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Actions : demander avis, refuser, commenter pj, commenter global
			affiche_action_candidature (REFUSERCAN);
			affiche_action_candidature (COMMENTERPJ);
			affiche_action_candidature (VALIDERCAN);
			// commentairepj
			affiche_variable ($g_st_commentairepj, "textarea", $l_i_handleractionscandidature_niv1, NULL);			
			affiche_action_candidature (DEMANDERAVIS);
			// avismj
			affiche_variable ($g_st_avismj, "textarea", $l_i_handleractionscandidature_niv1, NULL);
			affiche_action_candidature (COMMENTERGLOBAL);
			// commentaireglobal
			affiche_variable ($g_st_commentaireglobal, "textarea", $l_i_handleractionscandidature_niv1, NULL);	
		}
		break;
		
	case ABANDONNEE:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Action : effacer	la candidature, changer de statut
			affiche_action_candidature (EFFACERCAN);
			affiche_action_candidature (CHANGERSTATUT);
		}
		break;
		
	case ACCEPTEE:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Action : effacer	la candidature, commenter global
			affiche_action_candidature (EFFACERCAN);
			affiche_action_candidature (COMMENTERGLOBAL);	
		}		
		break;
		
	case REFUSEE:
		if ($_SESSION ["droitrecrutement"] == OUI)
		{
			// Action : effacer	la candidature, changer de statut
			affiche_action_candidature (EFFACERCAN);
			affiche_action_candidature (CHANGERSTATUT);
		}		
		break;
	default: 
		// /!\ TODO : ajouter le traitement d'erreur
}


$l_o_template -> closeSession ($l_i_handleractionscandidature_niv1, "actionscandidature_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleraffichecandidature_niv1, "affichecandidature_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleractionscandidature_niv1, "actionscandidature_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
