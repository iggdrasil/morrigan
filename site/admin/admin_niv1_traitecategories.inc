<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_traitecategories.inc
* Fonction du fichier : permet de traiter les catégories de profils
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  03/09/2008
* Version actuelle : 1.0 au 03/09/2008
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array ());
$l_i_compteur = 0; // compteur pour les boucles for
$l_st_affichage = array ("", "", NORMAL);

/* Contenu et inclusion des fichier de contenu */
switch ($g_st_iteration [CONTENU_VAR])
{
	case "0":
		$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "traitecategories_niv1");
		
		$g_st_iteration [CONTENU_VAR]++;
		
		// Pas d'action, juste une liste de questions à afficher dans un select
		$l_t_st_resultat = requete ("listecategories");
		if ($l_t_st_resultat [0] == NULL)
		{
			trigger_error ("Erreur dans la récupération de la liste des categories", NOTICE);
			$l_t_st_resultat [0] = 0;
		}
		
		if ($l_t_st_resultat [0] == 0)
		{
			$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "pasdecategorie");
			$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "pasdecategorie");	
		}
		else
		{
			$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "listecategories");
			$g_st_idcategorie [TYPE_DISPLAY] = NORMAL;
			affiche_variable ($g_st_idcategorie, "select", $l_i_handleradmincategories_niv1, $l_t_st_resultat);
			$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "listecategories.action", CORRIGERCATEGORIE);
			$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "listecategories");	
		}
		
		
		
		$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "creercategorie");
		$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "creercategorie.action", CREERCATEGORIE);

		$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "creercategorie");			

		affiche_variable ($g_st_iteration, "input", $l_i_handleradmincategories_niv1, NULL);

		$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "traitecategories_niv1");

		$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
		$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmincategories_niv1, "traitecategories_niv1");
		$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


		break;	
		
	case "1":
		$g_st_iteration [CONTENU_VAR]++;

		if ($g_st_actioncategorie [CONTENU_VAR] != CREERCATEGORIE)
		{
			// On affiche la question à corriger (ou des champs vides si on crée une nouvelle question)
			$l_t_st_resultat = requete ("recuperecategorie", $g_i_numerocategorie);
			if ($l_t_st_resultat [0] == 0)
			{
				trigger_error ("La categorie '".$g_st_idcategorie [CONTENU_VAR]."' n'a pas pu être affichée", FATAL);
				exit ();
			}
		}

		$g_st_idcategorie [CONTENU_VAR] = $l_t_st_resultat [1][0][0];
		$g_st_categorie [CONTENU_VAR] = $l_t_st_resultat [1][0][1];

		
		$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategorie_niv1");
		
		affiche_variable ($g_st_idcategorie, "input", $l_i_handleradmincategories_niv1, NULL);
		affiche_variable ($g_st_categorie, "textarea", $l_i_handleradmincategories_niv1, NULL);
		
		
		if ($g_st_actioncategorie [CONTENU_VAR] != CREERCATEGORIE)
		{
			$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "actions_niv1");
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.edition", CORRIGERCATEGORIE);
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.editiontxt", CORRIGERCATEGORIETXT);
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.suppression", EFFACERCATEGORIE);	
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.suppressiontxt", EFFACERCATEGORIETXT);		
			$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "actions_niv1");			
		}
		else
		{
			$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "creation_niv1");
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "creation_niv1.creation", CREERCATEGORIE);
				$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "creation_niv1.creationtxt", CREERCATEGORIETXT);
			$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "creation_niv1");				
		}
		
		affiche_variable ($g_st_iteration, "input", $l_i_handleradmincategories_niv1, NULL);
		
		$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategorie_niv1");

		$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
		$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmincategories_niv1, "editioncategorie_niv1");
		$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

		break;	
			
	case "2":
		if ($g_st_actioncategorie [CONTENU_VAR]==EFFACERCATEGORIE)
		{
			// Vérification du droit d'effacement de cette catégorie : si des profils y sont accrochés, impossible
			$l_t_st_resultat = requete ("profilsassociescategorie", $g_st_idcategorie [CONTENU_VAR]);
			if ($l_t_st_resultat [0] == 0)
			{
				
				// Requete d'effacement de la categorie	
				$l_st_resultat = requete ("effacercategorie", $g_st_idcategorie [CONTENU_VAR]);
				
				if ($l_st_resultat ["nbaffecte"] > 0)
				{
					// Succès
					$l_s_flagsucces = "effacer";
				}
				
				else 
				{
					trigger_error ("La categorie '".$g_st_idcategorie [CONTENU_VAR]."' n'a pas pu être effacée.", FATAL);	
					exit ();
				}
			}
			else
			{
					$l_s_flagsucces = "echec";				
			}
		}
		else
		{
			// S'il y a une erreur, réaffichage du formulaire avec les notifications d'erreurs	
			if ($g_i_erreur == OUI)			
			{
				$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategorie_niv1");

				$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "erreur_globale_niv1");
				$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "erreur_globale_niv1");
				
				affiche_variable ($g_st_idquestion, "input", $l_i_handleradmincategories_niv1, NULL);
				affiche_variable ($g_st_question, "textarea", $l_i_handleradmincategories_niv1, NULL);
				
				if ($g_st_actioncategorie [CONTENU_VAR]!=CREERCATEGORIE)
				{
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "actions_niv1");
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.edition", CORRIGERCATEGORIE);
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.editiontxt", CORRIGERCATEGORIETXT);
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.suppression", EFFACERCATEGORIE);	
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "actions_niv1.suppressiontxt", EFFACERCATEGORIETXT);		
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "actions_niv1");			
				}
				else
				{
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "creation_niv1");
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "creation_niv1.creation", CREERQUESTION);
						$l_o_template -> setVar ($l_i_handleradmincategories_niv1, "creation_niv1.creationtxt", CREERQUESTIONTXT);
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "creation_niv1");				
				}
				
				affiche_variable ($g_st_iteration, "input", $l_i_handleradmincategories_niv1, NULL);			
				$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategorie_niv1");

				$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
				$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmincategories_niv1, "editioncategorie_niv1");
				$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");			
			}
			else
			{
				// Pas d'erreur, on traite l'ensemble
				if ($g_st_actioncategorie [CONTENU_VAR] == CREERCATEGORIE)
				{
					// Créer de 0, requete d'insert
					// insérer la question
					$l_st_resultat = requete ("ajoutecategorie", $g_st_categorie [CONTENU_VAR]);
					
					if ($l_st_resultat ["nbaffecte"] == 0)
					{
						trigger_error ("La nouvelle categorie n'a pas pu être enregistrée en base de données.", FATAL);
						exit ();
					}
					
					$l_s_flagsucces = "creer";
				}
				else 
				{
					// corriger, requetes d'update
					$l_i_idcategorie = $g_st_idcategorie [CONTENU_VAR];
					// corriger la question
					$l_st_resultat = requete ("corrigecategorie", $l_i_idcategorie, $g_st_categorie [CONTENU_VAR]);
										
					$l_s_flagsucces = "corriger";
				}
			}
		}
		
		// En cas de succès, notification de l'utilisateur
		if ($l_s_flagsucces != "")
		{
			$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "succes_niv1");
			switch ($l_s_flagsucces)
			{
				case "effacer":
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategorieeffacersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategorieeffacersucces_niv1");			
					break;
				case "creer":
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategoriecreersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategoriecreersucces_niv1");			
					break;				
				case "corriger":
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategoriecorrigersucces_niv1");
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategoriecorrigersucces_niv1");			
					break;
				case "echec":
					$l_o_template -> addSession ($l_i_handleradmincategories_niv1, "editioncategorieeffacerechec_niv1");
					$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "editioncategorieeffacerechec_niv1");			
					break;				
				default:
					trigger_error ("Le critère de succès '".$l_s_flagsucces."' n'existe pas pour l'édition du quizz.", FATAL);
					exit ();	
			}			
				
			$l_o_template -> closeSession ($l_i_handleradmincategories_niv1, "succes_niv1");						
							
			$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
			$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmincategories_niv1, "succes_niv1");
			$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");		
		}
		break;
		
	default:
		trigger_error ("Cette iteration est inconnue pour le traitement des categories", FATAL);
		exit ();
		break;			
}
/* Fin de fichier **************************************************************************************/
?>
