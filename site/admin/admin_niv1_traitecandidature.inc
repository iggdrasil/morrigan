<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_traitecandidature.inc
* Fonction du fichier : permet de traiter les actions sur une candidature
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  14/10/2007
* Version actuelle : 1.0 au 14/10/2007
* License du projet : GPL
* Dernières modifications : 
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_t_st_resultatmailcand = array (0, array());
$l_t_st_resultatmailmj = array (0, array());
$l_i_compteur = 0; // compteur pour les boucles for d'affichage
$l_i_resultat = AUCUN; // Permet de savoir quelle modification appliquer à nbpersos
$l_s_demande = ""; // pour écrire la demande d'action en clair
$g_i_flag_debatpj = NON; // drapeau pour afficher la textarea de commentaire
$g_i_flag_debatmj = NON; // drapeau pour afficher la textarea de commentaire
$g_i_flag_commentaire = NON; // drapeau pour afficher la textarea de commentaire
$g_i_numerocandidature = $g_st_idcandidature [CONTENU_VAR];
$l_i_succesrequete = NON;

$l_st_affichage = array ("", "", NORMAL);
$l_t_equivalencestatutcandidature = array (EDITIONUSERTXT, RELECTUREMJTXT, AVISMJTXT, ENATTENTETXT, ABANDONNEETXT, ACCEPTEETXT, REFUSEETXT);

$l_t_st_resultatmailcand = requete ("mailcand", $g_st_idcandidature [CONTENU_VAR]);
if ($l_t_st_resultatmailcand [0] == 0)
{
	trigger_error ("Pas de résultat à la requete de collecte d'informations pour envoyer un email au candidat.", FATAL);
	exit ();
}

$l_t_st_resultatmailmj = requete ("mailmj", $g_st_idcandidature [CONTENU_VAR]);
if ($l_t_st_resultatmailmj [0] == 0)
{
	trigger_error ("Pas de résultat à la requete de collecte d'informations pour envoyer un email au MJ.", FATAL);
	exit ();
}

$l_t_st_resultatprofil = requete ("idprofil_cand", $g_st_idcandidature [CONTENU_VAR]);
if ($l_t_st_resultatprofil [0] == 0)
{
        trigger_error ("Pas de résultat à la requete de récupération de l id du profil associé à la candidature traitée.", FATAL);
        exit ();
}


/* Contenu et inclusion des fichier de contenu */
// Le MJ logué a-t-il le droit de traiter cette candidature?
if (
		$_SESSION ['droitrecrutement'] == NON
	&&
		$g_t_st_infoscandidature [4] != AVISMJ
    )
{
    // Pas de droit de traitement dans ces cas-là
    trigger_error ("Le MJ ".$_SESSION ['user']." n'as pas le droit de traiter la candidature ".$g_st_idcandidature [CONTENU_VAR].".", FATAL );
    exit ();
}



switch ($g_st_actioncandidature [CONTENU_VAR])
{
	case COMMENTERPJ:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (EDITION, $g_st_idcandidature [CONTENU_VAR], NULL);
		
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			// Ajout du commentaire, changement du statut en edition par le joueur
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $g_st_idcandidature [CONTENU_VAR], $g_st_commentairepj [CONTENU_VAR], EDITIONUSER);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", COMMENTERPJTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (commentaire pj).",  FATAL);
        	    exit ();                   
        	}
        	
        	$g_i_succesrequete = OUI;
        	
        	// mailer candidat
			// Préparation du courriel
			$l_s_objet = "Votre candidature à ".NOM_SITE." doit être modifiée";
			$l_t_champs ["login"] = $l_t_st_resultatinfosmail [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatinfosmail [1][0][2];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatinfosmail [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "correctioncand";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email de demande de correction de candidature n'a pas pu être envoyé à '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}
		}
		
		break;
	
	case DONNERAVISMJ:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (RELECTURE, $g_st_idcandidature [CONTENU_VAR], NULL);
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			// Ajout du commentaire mj, changement du statut en relecture MJ Inscription
			$l_t_st_resultat = requete ("commentaire_mj_candidature", $g_st_idcandidature [CONTENU_VAR],  $g_st_avismj [CONTENU_VAR], RELECTUREMJ);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}

			if (modifie_nbpersos ($l_t_st_resultatprofil [1][0][0], $l_i_resultat) != OUI)
			{
				trigger_error ("Erreur lors de la modification de nbpersos lors de rendu avis sur une candidature", FATAL);
				exit ();
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", DONNERAVISMJTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (donner avis).",  FATAL);
        	    exit ();                   
        	}
        	
        	$g_i_succesrequete = OUI;
        		
        	// mailer MJ auteur
			// Préparation du courriel
			$l_s_objet = "La candidature à ".NOM_SITE." doit être relue";
			$l_t_champs ["login"] = $l_t_st_resultatmailmj [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatmailmj [1][0][2];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatmailmj [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "avismj";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email de demande de relecture de candidature n'a pas pu être envoyé à '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}


		}			
		break;
	
	//case COMMENTERMJ:
	//	break;
	
	case DEMANDERAVIS:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (AVISMJ, $g_st_idcandidature [CONTENU_VAR], NULL);
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			
			// Ajout du commentaire mj, changement du statut en demande d'avis
			$l_t_st_resultat = requete ("commentaire_mj_candidature", $g_st_idcandidature [CONTENU_VAR],  $g_st_avismj [CONTENU_VAR], AVISMJ);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}

                       if (modifie_nbpersos ($l_t_st_resultatprofil [1][0][0], $l_i_resultat) != OUI)
                        {  
                                trigger_error ("Erreur lors de la modification de nbpersos lors de demande avis sur une candidature", FATAL);
                                exit ();
                        }

			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", DEMANDERAVISTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (demander avis).",  FATAL);
        	    exit ();                   
        	}

        	$g_i_succesrequete = OUI;
        	
        	// mailer MJ auteur
			// Préparation du courriel
			$l_s_objet = "La candidature à ".NOM_SITE." doit être relue";
			$l_t_champs ["login"] = $l_t_st_resultatmailmj [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatmailmj [1][0][2];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatmailmj [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "avismj";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email de demande d'avis de MJ n'a pas pu être envoyé à '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}
		}			
		break;
	
	case REFUSERCAN:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (REFUSEE, $g_st_idcandidature [CONTENU_VAR], NULL);
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			// Ajout du commentaire pj, changement du statut en refusée
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $g_st_idcandidature [CONTENU_VAR],  $g_st_commentairepj [CONTENU_VAR], REFUSEE, $l_i_resultat);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
                       if (modifie_nbpersos ($l_t_st_resultatprofil [1][0][0], $l_i_resultat) != OUI)
                        {
                                trigger_error ("Erreur de modification de nbpersos lors du refus  une candidature", FATAL);
                                exit ();
                        }


			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", REFUSERCANTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (refuser).",  FATAL);
        	    exit ();                   
        	}
        	
      	
        	$g_i_succesrequete = OUI;
        	
        	// mailer candidat
			// Préparation du courriel
			$l_s_objet = "Votre candidature à ".NOM_SITE." a été refusée";
			$l_t_champs ["login"] = $l_t_st_resultatmailcand [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatmailcand [1][0][2];
			$l_t_champs ["raison"] = $g_st_commentairepj [CONTENU_VAR];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatmailcand [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "refuscand";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email de refus de candidature n'a pas pu être envoyé à '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}       	
		}	
		break;
	
	case EFFACERCAN:
		$l_t_st_resultat = requete ("effacer_candidature", $g_st_idcandidature [CONTENU_VAR]);
		if ($l_t_st_resultat ["nbaffecte"] != 1)
		{
			trigger_error ("La requête de suppression de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
			exit ();	
		}
		
		$g_i_succesrequete = OUI;
		break;
	
	case VALIDERCAN:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (ACCEPTEE, $g_st_idcandidature [CONTENU_VAR], NULL);
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			// Ajout du commentaire pj, changement du statut en validée
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $g_st_idcandidature [CONTENU_VAR],  $g_st_commentairepj [CONTENU_VAR], ACCEPTEE, $l_i_resultat);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", VALIDERCANTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (valider).",  FATAL);
        	    exit ();                   
        	}

        	$g_i_succesrequete = OUI;
        	
        	// mailer candidat
			// Préparation du courriel
			$l_s_objet = "Votre candidature à ".NOM_SITE." a été acceptée";
			$l_t_champs ["login"] = $l_t_st_resultatmailcand [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatmailcand [1][0][2];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatmailcand [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "acceptercand";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email d'acceptation de candidature n'a pas pu être envoyé à '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}          	
		}	
		break;
	
	case METTREENATTENTE:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos (ATTENTE, $g_st_idcandidature [CONTENU_VAR], NULL);
		if ($l_i_resultat == ERREUR)
		{
			// Traiter problème affichage
			$g_i_succesrequete = ERREUR;
			
		}
		elseif ($l_i_resultat == REFUS)
		{
			// Traiter impossibilité de changer statut à cause de nbperso
			$g_i_succesrequete = REFUS;
		}
		
		elseif ($g_i_erreur == NON && (($l_i_resultat == AUCUN) || ($l_i_resultat == PLUS) || ($l_i_resultat == MOINS)))
		{
			// Ajout du commentaire pj, changement du statut en en attente
			$l_t_st_resultat = requete ("commentaire_pj_candidature", $g_st_idcandidature [CONTENU_VAR], $g_st_commentairepj [CONTENU_VAR], ENATTENTE, $l_i_resultat);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", METTREENATTENTETXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (mettre en attente).",  FATAL);
        	    exit ();                   
        	}

        	$g_i_succesrequete = OUI;
        	
        	// mailer candidat
			// Préparation du courriel
			$l_s_objet = "Votre candidature à ".NOM_SITE." a été mise en attente";
			$l_t_champs ["login"] = $l_t_st_resultatmailcand [1][0][0];
			$l_t_champs ["profil"] = $l_t_st_resultatmailcand [1][0][2];
			$l_t_champs ["jeu"] = NOM_SITE;
			
			$l_st_parametres ["destinataire"] = $l_t_st_resultatmailcand [1][0][1];
			$l_st_parametres ["copie"] = "";
			$l_st_parametres ["copie_cachee"] = "";
			$l_st_parametres ["objet"] = $l_s_objet;
			$l_st_parametres["champs"] = $l_t_champs;
			$l_st_parametres ["template"] = "attentecand";

			// Envoi du courriel
			$l_i_resultat = courriel ($l_st_parametres);
			
			if ($l_i_resultat == NON)
			{
				trigger_error ("Un email d'acceptation de candidature a été mise en attente '".$l_t_st_resultatinfosmail [1][0][0]."'", NOTICE);
			}         		        	
		}		
		break;
	
	case COMMENTERGLOBAL:
		if ($g_i_erreur == NON)
		{
			// Ajout du commentaire global, pas de changement du statut
			$l_t_st_resultat = requete ("commentaire_global", $g_st_idcandidature [CONTENU_VAR], $g_st_commentaireglobal [CONTENU_VAR]);
			if ($l_t_st_resultat ["nbaffecte"] != (1 || 2))
			{
				trigger_error ("La requête d'ajout de commentaire pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", METTREENATTENTETXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (commentaire global).",  FATAL);
        	    exit ();                   
        	}
        	
        	$g_i_succesrequete = OUI;
        }		
		break;
	
	case CHANGERSTATUT:
		// Vérifier la possibilité de traiter la candidature
		$l_i_resultat = delta_nbpersos ($g_st_statutcandidature [CONTENU_VAR][0], $g_st_idcandidature [CONTENU_VAR], NULL);

		if ($g_i_erreur == NON)
		{
			// Modification du statut
			$l_t_st_resultat = requete ("changestatut_candidature", $g_st_idcandidature [CONTENU_VAR], $g_st_statutcandidature [CONTENU_VAR][0]);
			if ($l_t_st_resultat ["nbaffecte"] != 1)
			{
				trigger_error ("La requête de modification du statut pour la candidature '".$g_st_idcandidature [CONTENU_VAR]."' a échoué", FATAL);
				exit ();	
			}
			
			// Ajout dans l'historique
			$l_st_resultat = requete ("ajout_historique_candidature", CHANGERSTATUTTXT, $g_st_idcandidature [CONTENU_VAR]);
			if ($l_st_resultat ["nbaffecte"] != 1)
        	{
        		// Gros plantage dans la requete 
        	    trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition de la candidature '".$g_st_idcandidature [CONTENU_VAR]."' (changement statut).",  FATAL);
        	    exit ();                   
        	}
        	
        	// Si le statut actuel est "refusé" et qu'on le repasse en non refusé, il faut décrémenter le nombre de places disponibles 
        	//if ($g_st_statutcandidature [CONTENU_VAR][0] == 
        	
        	$g_i_succesrequete = OUI;
		}	
		break;
	default:
		// l'action n'existe pas
		// /!\ TODO : faire le traitement d'erreur
		break;
}

// print ("Succès : ".$g_i_succesrequete = OUI."<br>");
// print ("Action : ".$g_st_actioncandidature [CONTENU_VAR]."<br>");

$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "traitecandidature_niv1");

if ($g_i_succesrequete == OUI)
{
	
	switch ($g_st_actioncandidature [CONTENU_VAR])
	{
		case COMMENTERPJ:
		case DONNERAVISMJ:
		case DEMANDERAVIS:
		case COMMENTERGLOBAL:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "succescommentaire");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "succescommentaire");			
			break;
		case REFUSERCAN:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "succesrefus");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "succesrefus");			
			break;
		case EFFACERCAN:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "succeseffacer");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "succeseffacer");	
			break;
		case VALIDERCAN:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "succesvalider");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "succesvalider");			
			break;
		case METTREENATTENTE:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "succesattente");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "succesattente");	
			break;
		case CHANGERSTATUT:
			$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "successtatut");
			$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "successtatut");	
			break;
		default: 
	}
}
elseif ($g_i_succesrequete == ERREUR)
{
	$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "erreur_nbpersos");
	$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "erreur_nbpersos");	
	
}
elseif ($g_i_succesrequete == REFUS)
{
	$l_o_template -> addSession ($l_i_handlertraitecandidature_niv1, "refus_nbpersos");
	$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "refus_nbpersos");	

}

$l_o_template -> closeSession ($l_i_handlertraitecandidature_niv1, "traitecandidature_niv1");	

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlertraitecandidature_niv1, "traitecandidature_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");
/* Fin de fichier **************************************************************************************/
?>
