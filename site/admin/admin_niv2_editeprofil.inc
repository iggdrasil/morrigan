<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv2_edititeprofil.inc
* Fonction du fichier : permet de traiter toutes les actions nécessitant l'édition
* d'un profil
*
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  20/09/2007
* Version actuelle : 1.0 au 20/09/2007
* License du projet : GPL
* Dernières modifications :
* Remarques particulières : TODO : supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
// $g_i_numeroprofil = ""; // pour l'affichage des select$g_i_numeroprofil = ""; // pour l'affichage des select
$l_st_infoprofil = array (0, array ());// pour la récupération des infos nécessaire à l'affichage d'un profil en cas d'édition et d'itération = 1
$l_st_equivalencestatut = array (EDITIONTXT, RELECTURETXT, HORSLIGNETXT, ENLIGNETXT); // pour un équivalent en forme de texte des statuts
$l_st_resultat_requete = array (0, array ());
$l_i_idnouveauprofil = 0;
$l_s_valeur = "";
$l_i_valeur = 0;
$l_i_valeur2 = 0;
$l_st_res_ajout = array (0, array());
$l_st_res_retrait = array (0, array ());
$l_i_flag_effacement = NON; // drapeau utilisé dans les boucles de vérification qu'un élément est à effacer de la bdd ou non (race, classe, sexe)
$l_i_verif = NON; // retour de la fonction de vérification de résolution de demande

/* Contenu et inclusion des fichier de contenu */
// $g_i_numeroprofil = $l_t_i_existeprofil [1][0][0];


if ($g_st_iteration [CONTENU_VAR] == 2)
{
    // Vérification de la validité des données 
    // (en dehors des vérifs faites automatique à la récupération des variables de formulaire) 
	// Il ne faut pas que nbpersos soit en dehors de [-1;+infini]
	if ($g_st_nbpersos [CONTENU_VAR] < -1)
	{
		$g_i_erreur = OUI;
		$g_st_nbpersos [TYPE_DISPLAY] = ERRONE;	
	}
	
	// Si le titre n'a pas changé par rapport au titre du profil, ce n'est pas une erreur
	/*if ()
	{
		
	}*/
	
}
elseif (($g_st_iteration [CONTENU_VAR] == 1) && ($g_st_actionprofil [CONTENU_VAR] != CREER))
{
	// Récupération des données à afficher depuis la bdd plutôt que le formulaire 
	// (puisque ce qui vient du formulaire est encore vide)
	$l_st_infoprofil = requete ("infos_profil", $g_st_idprofil [CONTENU_VAR]);
	if ($l_st_infoprofil [0] != 1)
	{
		trigger_error ("Le profil '".$g_st_idprofil [CONTENU_VAR]."' recherché n'existe pas.", FATAL);
		exit ();
	}
	
	$g_st_nomprofil [CONTENU_VAR] = stripslashes ($l_st_infoprofil [1][0][1]);
	$g_st_nomprofil [TYPE_DISPLAY] = NORMAL;
	
	$g_st_auteurprofil [CONTENU_VAR][0] = stripslashes ($l_st_infoprofil [1][0][11]);
	$g_st_auteurprofil [TYPE_DISPLAY] = NORMAL;
	
	$g_st_statut [CONTENU_VAR][0] = $l_st_infoprofil [1][0][2]; 
	$g_st_statut [TYPE_DISPLAY] = NORMAL;	
	
	$g_st_categorie [CONTENU_VAR][0] = $l_st_infoprofil [1][0][10]; 
	$g_st_categorie [TYPE_DISPLAY] = NORMAL;
	
	$g_st_nbpersos [CONTENU_VAR] = $l_st_infoprofil [1][0][6];
	$g_st_nbpersos [TYPE_DISPLAY] = NORMAL;
	
	$g_st_description [CONTENU_VAR] = stripslashes ($l_st_infoprofil [1][0][4]);
	$g_st_description [TYPE_DISPLAY] = NORMAL;
	
	$g_st_debat [TYPE_DISPLAY] = NORMAL;	
}
else 
{
	// /!\ TODO : Test pas très rigoureux à reprendre
	if ($g_st_actionprofil [CONTENU_VAR] != CREER)
	{
		trigger_error ("Itération impossible.", FATAL);
		exit ();	
	}
}

// print ("Itération : ".$g_st_iteration [CONTENU_VAR]."<br>");
// print ("Erreur : ".$g_i_erreur."<br>");

if ($g_i_erreur == OUI || $g_st_iteration [CONTENU_VAR] == 1)
{
    // Par défaut, on définit le numéro du profil à 0 (inexistant) pour la création d'un profil : ainsi, on arrive à lister races, classes, sexes, etc...

    
    if ($g_st_actionprofil [CONTENU_VAR] == CREER )
    {
    	$g_i_numeroprofil = 0;
    }
    
    // print ("Des erreurs ou première itération<br>");
    // print ("Id du profil : ".$g_i_numeroprofil."<br>");

    $l_o_template -> addSession ($l_i_handlerediteprofil_niv2, "editionprofil_niv2");

    $g_st_iteration [CONTENU_VAR] = 2;
    $g_st_iteration [TYPE_DISPLAY] = HIDDEN;
    affiche_variable ($g_st_iteration, "input", $l_i_handlerediteprofil_niv2, NULL);
    
    $g_st_actionprofil [TYPE_DISPLAY] = HIDDEN;
    affiche_variable ($g_st_actionprofil, "input", $l_i_handlerediteprofil_niv2, NULL);   

    $g_st_idprofil [TYPE_DISPLAY] = HIDDEN;
    affiche_variable ($g_st_idprofil, "input", $l_i_handlerediteprofil_niv2, NULL);

    affiche_variable ($g_st_nomprofil, "text", $l_i_handlerediteprofil_niv2, NULL);

    // Requête d'édition du profil
	if ($_SESSION ["droitrecrutement"] == NON)
	{
		$g_st_statut [CONTENU_VAR][0] == 1;
		$g_st_auteurprofil [CONTENU_VAR][0] == $_SESSION ["id"];
	}
    
    if ($_SESSION ['droitrecrutement'] == OUI)
    {	
        affiche_variable ($g_st_auteurprofil, "select", $l_i_handlerediteprofil_niv2, NULL);

        affiche_variable ($g_st_statut, "select", $l_i_handlerediteprofil_niv2, NULL);
    }
    else 
    {
		$l_o_template -> addSession ($l_i_handlerediteprofil_niv2, "auteurprofil2");
		$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "auteurprofil2.auteur", stripslashes ($g_st_auteurprofil [CONTENU_VAR]));
		$l_o_template -> closeSession ($l_i_handlerediteprofil_niv2, "auteurprofil2");
		
		$l_o_template -> addSession ($l_i_handlerediteprofil_niv2, "statutprofil2");
		$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "statutprofil2.statut", stripslashes ($g_st_statut [CONTENU_VAR]));
		$l_o_template -> closeSession ($l_i_handlerediteprofil_niv2, "statutprofil2");			
    }

    affiche_variable ($g_st_categorie, "select", $l_i_handlerediteprofil_niv2, NULL);    
    
    affiche_variable ($g_st_nbpersos, "text", $l_i_handlerediteprofil_niv2, NULL);

    affiche_variable ($g_st_race, "select", $l_i_handlerediteprofil_niv2, NULL);

    affiche_variable ($g_st_classe, "select", $l_i_handlerediteprofil_niv2, NULL);

    affiche_variable ($g_st_sexe, "select", $l_i_handlerediteprofil_niv2, NULL);

    affiche_variable ($g_st_description, "text", $l_i_handlerediteprofil_niv2, NULL);
	
	$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "editionprofil_niv2.debatprofil", stripslashes ($l_st_infoprofil [1][0][5]));
    
    affiche_variable ($g_st_debat, "text", $l_i_handlerediteprofil_niv2, NULL);

	$l_o_template -> addSession ($l_i_handlerediteprofil_niv2, "historiqueprofil");
	$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "historiqueprofil.historiqueprofil", stripslashes ($l_st_infoprofil [1][0][7]));
	$l_o_template -> closeSession ($l_i_handlerediteprofil_niv2, "historiqueprofil"); 
	
  
	// Définition de la valeur du submit
	// print ("Action : ".$g_st_actionprofil [CONTENU_VAR]."<br>");
	if ($g_st_actionprofil [CONTENU_VAR] == CREER || $g_st_actionprofil [CONTENU_VAR] == CREATIONDAPRES)
	{
		$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "editionprofil_niv2.action", CREERTXT);	
	}
	elseif ($g_st_actionprofil [CONTENU_VAR] == EDITER)
	{
		$l_o_template -> setVar ($l_i_handlerediteprofil_niv2, "editionprofil_niv2.action", EDITERTXT);		
	}
	else 
	{
		trigger_error ("L'action n'est pas valable lors de l'édition du profil", FATAL);
		exit ();	
	}

    $l_o_template -> closeSession ($l_i_handlerediteprofil_niv2, "editionprofil_niv2");

	$l_o_template -> addSession ($l_i_handlertraiteprofil_niv1, "editeprofil_niv2");
	$l_o_template -> Parse ($l_i_handlertraiteprofil_niv1, "editeprofil_niv2.varediteprofil_niv2", $l_i_handlerediteprofil_niv2, "editionprofil_niv2");
	$l_o_template -> closeSession ($l_i_handlertraiteprofil_niv1, "editeprofil_niv2");
}
elseif ($g_i_erreur == NON)
{
	if ($g_st_actionprofil [CONTENU_VAR] == CREER || $g_st_actionprofil [CONTENU_VAR] == CREATIONDAPRES)
	{
		// Requête de création du profil
		if ($_SESSION ["droitrecrutement"] == NON)
		{
			$g_st_auteurprofil [CONTENU_VAR] = $_SESSION ["id"];
		}
		
		
		// Paramètres : Nom du profil, statut du profil, catégorie du profil, auteur du profil, description du profil,
		// nombre de personnages possibles
		$l_st_resultat_requete = requete ("creer_profil", $g_st_nomprofil [CONTENU_VAR], RELECTURE, $g_st_categorie [CONTENU_VAR ][0], $g_st_auteurprofil [CONTENU_VAR ][0], $g_st_description [CONTENU_VAR], $g_st_nbpersos [CONTENU_VAR]);
		if ($l_st_resultat_requete ["nbaffecte"] != 1)
		{
			trigger_error ("Création du profil échouée", FATAL);
			exit ();
		}
		
		// Ajout des races, classe, sexe
		$l_i_idnouveauprofil = $l_st_resultat_requete ['dernier_id_affecte']; 
		   
    
    	// - Race
    	foreach ($g_st_race [CONTENU_VAR] as $l_s_valeur)
    	{
        	$l_st_resultat_requete = requete ("ajoute_champ_race", $l_i_idnouveauprofil, $l_s_valeur);    

        	if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout de la valeur ".$l_s_valeur." de race n'a pas pas pu etre fait en bdd lors de la création du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}
    	}

		// - Classe
    	foreach ($g_st_classe [CONTENU_VAR] as $l_s_valeur)
    	{
        	$l_st_resultat_requete = requete ("ajoute_champ_classe", $l_i_idnouveauprofil, $l_s_valeur);    

        	if ($l_st_resultat_requete ["nbaffecte"] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout de la valeur ".$l_s_valeur." de classe n'a pas pas pu etre fait en bdd lors de la création du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}
    	}

	    // - Sexe	
    	foreach ($g_st_sexe [CONTENU_VAR] as $l_s_valeur)
    	{
        	$l_st_resultat_requete = requete ("ajoute_champ_sexe", $l_i_idnouveauprofil, $l_s_valeur);    

        	if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout de la valeur ".$l_s_valeur." de race n'a pas pas pu etre fait en bdd lors de la création du profil.",  FATAL);
            	exit ();                   
        	}
    	}
	
		// Ajout du commentaire si nécessaire
		if ($g_st_debat [CONTENU_VAR] != "")
		{
			$l_st_resultat_requete = requete ("commente_profil", $g_st_debat [CONTENU_VAR], $l_i_idnouveauprofil);
			if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout du commentaire n'a pas pas pu etre fait en bdd lors de la création du profil ".$l_i_idnouveauprofil.".",  FATAL);
            	exit ();                   
        	}	
		}
	
		// Ajout dans l'historique
		$l_st_resultat_requete = requete ("ajout_historique_profil", CREERTXT, $l_i_idnouveauprofil);
		if ($l_st_resultat_requete [nbaffecte] != 1)
        {
        	// Gros plantage dans la requete 
            trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de la création du profil ".$l_i_idnouveauprofil.".",  FATAL);
            exit ();                   
        }
        
        // TRES IMPORTANT pour qu'on sache si la mise à jour a fonctionné
        $g_i_succesrequete = OUI;				
	}
	elseif ($g_st_actionprofil [CONTENU_VAR] == EDITER) 
	{
		if ($_SESSION ["droitrecrutement"] == NON)
		{
			$g_st_auteurprofil [CONTENU_VAR] = $_SESSION ["id"];
			$g_st_statut [CONTENU_VAR][0] = RELECTURE;	
		}
		
		// Requête d'édition du profil
		$l_t_st_resultat_requete = requete ("corrige_profil", $g_st_nomprofil [CONTENU_VAR], $g_st_categorie [CONTENU_VAR][0], $g_st_statut [CONTENU_VAR][0], $g_st_auteurprofil [CONTENU_VAR][0], $g_st_description [CONTENU_VAR], $g_st_nbpersos [CONTENU_VAR], $g_st_idprofil [CONTENU_VAR]);
    	if ($l_t_st_resultat_requete ["nbaffecte"] == 0)
    	{
        	// Gasp, pas de résultat à cette requete
        	trigger_error ("Pas eu de modification dans ces champs lors de l'edition du profil ".$g_st_idprofil [CONTENU_VAR].".", WARNING);
        	//exit ();         
    	}		
		
		// Ajout/retrait des races, classes et sexes
		// - Race
    	// Requete de récupération de l'existant
    	$l_t_st_resultat_requete = requete ("racespossiblesed");
    	if ($l_t_st_resultat_requete [0] == 0)
    	{
        	// Gasp, pas de résultat à cette requete
        	trigger_error ("Problème avec le listage des races possibles pour le profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée",  FATAL);
        	exit ();         
    	}
    
    	// Parcours pour chercher ce qu'il faut ajouter
    	foreach ($g_st_race [CONTENU_VAR] as $l_i_valeur2)
    	{
        	for ($l_i_valeur = 0; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
        	{
            	// print ("Parsage 2 : ".$l_t_st_resultat_requete [1][$l_i_valeur][0]."<br>");            
            	if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2 && $l_t_st_resultat_requete [1][$l_i_valeur][2] == NULL) 
            	{
                	// Cette option n'est pas encore choisie, on l'ajoute en base
                	$l_st_res_ajout = requete ("ajoute_champ_race", $g_st_idprofil [CONTENU_VAR], $l_i_valeur2);  
                	if ($l_st_res_ajout [nbaffecte] != 1)
                	{
                    	// Gros plantage dans la requete 
                    	trigger_error ("L'ajout de la valeur ".$l_i_valeur2." de race n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée.",  FATAL);
                    	exit ();                   
                	} 
            	}
        	}
    	}
    	// Parcours pour chercher ce qu'il faut effacer
    	for ($l_i_valeur = 0; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
    	{
        	// On considère par défaut qu'il faut effacer l'option de la base
           	$l_i_flag_effacement = NON;
        	if ($l_t_st_resultat_requete [1][$l_i_valeur][2] > 0)
        	{        
            	$l_i_flag_effacement = OUI;
            	foreach ($g_st_race [CONTENU_VAR] as $l_i_valeur2)
            	{
                	if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2)
                	{
                     	// Trouvé ! On n'efface pas cette option
                     	$l_i_flag_effacement = NON; 
                	}
            	}
        	}
        
        	if ($l_i_flag_effacement == OUI)
        	{
            	// Effaçons donc cette option de la base 
            	$l_st_res_retrait = requete ("enleve_champ_race", $g_st_idprofil [CONTENU_VAR], $l_t_st_resultat_requete [1][$l_i_valeur][0]);  
            	if ($l_st_res_retrait [nbaffecte] != 1)
            	{
            	    // Gros plantage dans la requete 
            	    trigger_error ("La suppression de la valeur ".$l_t_st_resultat_requete [1][$l_i_valeur][0]." de race n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée.",  FATAL);
            	    exit ();                   
            	}        
        	}
    	}    
            
    	// - Classe
    	// Même traitement que pour la race
    	// Requete de récupération de l'existant
    	$l_t_st_resultat_requete = requete ("classespossiblesed");
    	if ($l_t_st_resultat_requete [0] == 0)
    	{
        	// Gasp, pas de résultat à cette requete
        	trigger_error ("Problème avec le listage des classes possibles pour le profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée",  FATAL);
        	exit ();         
    	}
    
    	// Parcours pour chercher ce qu'il faut ajouter
    	foreach ($g_st_classe [CONTENU_VAR] as $l_i_valeur2)
    	{
        	for ($l_i_valeur = 0; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
        	{
	           	if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2 && $l_t_st_resultat_requete [1][$l_i_valeur][2] == NULL) 
            	{
                	// Cette option n'est pas encore choisie, on l'ajoute en base
                	$l_st_res_ajout = requete ("ajoute_champ_classe", $g_st_idprofil [CONTENU_VAR], $l_i_valeur2);  
                	if ($l_st_res_ajout [nbaffecte] != 1)
                	{
                    	// Gros plantage dans la requete 
                    	trigger_error ("L'ajout de la valeur ".$l_i_valeur2." de classe n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée.",  FATAL);
                    	exit ();                   
                	} 
            	}
        	}
    	}
    	// Parcours pour chercher ce qu'il faut effacer
    	for ($l_i_valeur = 0; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
    	{
        	// On considère par défaut qu'il faut effacer l'option de la base   
        	$l_i_flag_effacement = NON;
        	if ($l_t_st_resultat_requete [1][$l_i_valeur][2] > 0)
        	{        
            	$l_i_flag_effacement = OUI;
            	foreach ($g_st_classe [CONTENU_VAR] as $l_i_valeur2)
            	{           
               		if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2)
                	{
                    	// Trouvé ! On n'efface pas cette option
                    	$l_i_flag_effacement = NON;                             
                	}
            	}
        	}
        
        	if ($l_i_flag_effacement == OUI)
        	{
            	// Effaçons donc cette option de la base 
            	$l_st_res_retrait = requete ("enleve_champ_classe", $g_st_idprofil [CONTENU_VAR], $l_t_st_resultat_requete [1][$l_i_valeur][0]);  
            	if ($l_st_res_retrait [nbaffecte] != 1)
            	{
            	    // Gros plantage dans la requete 
            	    trigger_error ("La suppression de la valeur ".$l_t_st_resultat_requete [1][$l_i_valeur][0]." de classe n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée.",  FATAL);
            	    exit ();                   
            	}        
        	}
    	}   

    	// - Sexe
    	// Même traitement que pour la race   
    	// Requete de récupération de l'existant
    	$l_t_st_resultat_requete = requete ("sexespossiblesed");
    	if ($l_t_st_resultat_requete [0] == 0)
    	{
        	// Gasp, pas de résultat à cette requete
        	trigger_error ("Problème avec le listage des sexes possibles pour le profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée",  FATAL);
        	exit ();         
    	}
    
    	// Parcours pour chercher ce qu'il faut ajouter
    	foreach ($g_st_sexe [CONTENU_VAR] as $l_i_valeur2)
    	{
        	for ($l_i_valeur = 0 ; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
        	{         
            	if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2 && $l_t_st_resultat_requete [1][$l_i_valeur][2] == NULL) 
            	{
                	// Cette option n'est pas encore choisie, on l'ajoute en base
                	$l_st_res_ajout = requete ("ajoute_champ_sexe", $g_st_idprofil [CONTENU_VAR], $l_i_valeur2);  
                	if ($l_st_res_ajout [nbaffecte] != 1)
                	{
                    	// Gros plantage dans la requete 
                    	trigger_error ("L'ajout de la valeur ".$l_i_valeur2." de sexe n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [1].". Correction en partie échouée.",  FATAL);
                    	exit ();                   
                	} 
            	}
        	}
    	}
    	
    	// Parcours pour chercher ce qu'il faut effacer
    	for ($l_i_valeur = 0 ; $l_i_valeur < count ($l_t_st_resultat_requete [1]); $l_i_valeur++)
    	{
        	// On considère par défaut qu'il faut effacer l'option de la base       
        	$l_i_flag_effacement = NON;
        	if ($l_t_st_resultat_requete [1][$l_i_valeur][2] > 0)
        	{        
            	$l_i_flag_effacement = OUI;
            	foreach ($g_st_sexe [CONTENU_VAR] as $l_i_valeur2)
            	{          
                	if ($l_t_st_resultat_requete [1][$l_i_valeur][0] == $l_i_valeur2)
                	{
                    	// Trouvé ! On n'efface pas cette option
                    	$l_i_flag_effacement = NON;                              
                	}
            	}
        	}
        
        	if ($l_i_flag_effacement == OUI)
        	{
            	// Effaçons donc cette option de la base 
            	$l_st_res_retrait = requete ("enleve_champ_sexe", $g_st_idprofil [CONTENU_VAR], $l_t_st_resultat_requete [1][$l_i_valeur][0]);  
            	if ($l_st_res_retrait [nbaffecte] != 1)
            	{
                	// Gros plantage dans la requete 
                	trigger_error ("La suppression de la valeur ".$l_t_st_resultat_requete [1][$l_i_valeur][0]." de sexe n'a pas pas pu etre fait en bdd lors de la correction du profil ".$g_st_idprofil [CONTENU_VAR].". Correction en partie échouée.",  FATAL);
                	exit ();                   
            	}        
        	}
    	}

		
		// Ajout du commentaire si nécessaire	
		if ($g_st_debat [CONTENU_VAR] != "")
		{
			$l_st_resultat_requete = requete ("commente_profil", $g_st_debat [CONTENU_VAR], $g_st_idprofil [CONTENU_VAR]);
			if ($l_st_resultat_requete [nbaffecte] != 1)
        	{
        	    // Gros plantage dans la requete 
            	trigger_error ("L'ajout du commentaire n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            	exit ();                   
        	}	
		}
				
		// Ajout dans l'historique
		$l_st_resultat_requete = requete ("ajout_historique_profil", EDITERTXT, $g_st_idprofil [CONTENU_VAR]);
		if ($l_st_resultat_requete [nbaffecte] != 1)
        {
        	// Gros plantage dans la requete 
            trigger_error ("L'ajout de l'historique n'a pas pas pu etre fait en bdd lors de l'édition du profil ".$g_st_idprofil [CONTENU_VAR].".",  FATAL);
            exit ();                   
        }
        
        // Vérifier si on n'a pas résolu une demande d'action
        $l_i_verif = verif_demande_action ($g_st_idprofil [CONTENU_VAR]);
        
    	$g_i_succesrequete = OUI;
        
	}
	else 
	{
		trigger_error ("Action impossible lors de l'édition du profil", FATAL);
		exit ();
	}
}
/* Fin de fichier **************************************************************************************/
?>
