<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_niv1_inscription.inc                                                                                 
* Fonction du fichier : page d'accueil de l'interface d'inscription MJ. On y trouve 
* l'accès à l'interface de création des profils et à celle de la gestion des candidatures
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  08/05/2008                                                                              
* Version actuelle :  1.0 au 08/05/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handleradmininscription_niv1,  "admininscription_niv1");


$l_o_template -> closeSession ($l_i_handleradmininscription_niv1, "admininscription_niv1");


$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradmininscription_niv1, "admininscription_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
