<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_niv1_recrutement.inc                                                                                 
* Fonction du fichier : page d'accueil de l'interface du MJ Inscription. On y trouve 
* l'accès à l'interface de création/édition des questions et des réponses au quizz, à l'édition des
* catégories de profils, et à l'ouverture/ fermeture des éléments d'inscription
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  25/06/2008                                                                              
* Version actuelle :  1.0 au 25/06/2008                                                                              
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handleradminrecrutement_niv1,  "adminrecrutement_niv1");


$l_o_template -> closeSession ($l_i_handleradminrecrutement_niv1, "adminrecrutement_niv1");


$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleradminrecrutement_niv1, "adminrecrutement_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
