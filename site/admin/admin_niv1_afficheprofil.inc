<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : admin_niv1_afficheprofil.inc
* Fonction du fichier : permet d'afficher un profil - ou/puis de l'éditer
*
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  02/12/2006
* Version actuelle : 2.0 au 28/09/2007
* License du projet : GPL
* Dernières modifications : 2.0 au 28/09/2007
* 							1.0 au 02/12/2006
* Remarques particulières : Supprimer les print de debug
*
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_profil = array (0, array()); // contiendra toutes les informations d'un profil
$l_t_st_races = array (0, array()); // contient les races possibles
$l_t_st_classes = array (0, array()); // contient les classes possibles
$l_t_st_sexes = array (0, array()); // contient les sexes possibles
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0; // compteur pour les boucles for d'affichage
$l_s_demande = ""; // pour écrire la demande d'action en clair
$l_i_flag_debat = NON; // drapeau pour afficher la textarea de commentaire
$l_st_affichage = array ("", "", NORMAL);
$l_t_equivalencestatut = array (EDITIONTXT, RELECTURETXT, HORSLIGNETXT, ENLIGNETXT);

/* Contenu et inclusion des fichier de contenu */
// Affichage du profil
// Le MJ logué a-t-il le droit d'afficher ce profil?
if (
        (
            ($l_t_i_existeprofil [1][0][2] == EDITION)
            &&
            ($_SESSION ['id'] != $l_t_i_existeprofil [1][0][1])
            &&
            ($_SESSION ['droitrecrutement'] == NON)
        )
        ||
        (
            ($l_t_i_existeprofil [1][0][2] == RELECTURE)
            &&
            ($_SESSION ['droitrecrutement'] == NON)
        )
    )
{
    // Pas de droit d'affichage dans ces cas-là
    trigger_error ("Le MJ ".$_SESSION ['user']." n'as pas le droit d'afficher le profil ".$l_t_i_existeprofil [1][0][0].".", FATAL );
    exit ();
}

// Récupération de toutes les informations du profil qu'il faut afficher
$l_t_st_profil = requete ("infos_profil", $l_t_i_existeprofil [1][0][0]);

if ($l_t_st_profil [0] != 1)
{
    trigger_error ("Le profil ".$l_t_st_existeprofil [1][0][0]." n'existe pas.");
    exit ();
}

// Divers formatages avant l'affichage
// Traduction du statut en langue de littéraires


// Récupération des races
$l_t_st_races = requete ("racespossiblesed2", $l_t_st_profil [1][0][0]);
if ($l_t_st_races == 0)
{
    trigger_error ("Il n'y a pas de races possibles pour le profil ".$l_t_st_profil [1][0][0].".", ERROR);
}

// Récupération des classes
$l_t_st_classes = requete ("classespossiblesed2", $l_t_st_profil [1][0][0]);
if ($l_t_st_classes == 0)
{
    trigger_error ("Il n'y a pas de classes possibles pour le profil ".$l_t_st_profil [1][0][0].".", ERROR);
}

// Récupération des sexes
$l_t_st_sexes = requete ("sexespossiblesed2", $l_t_st_profil [1][0][0]);
if ($l_t_st_sexes == 0)
{
    trigger_error ("Il n'y a pas de sexes possibles pour le profil ".$l_t_st_profil [1][0][0].".", ERROR);
}


//print ("Affichage du profil<br>");

$l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1");

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.nomprofil", stripslashes ($l_t_st_profil [1][0][1]));

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.auteurprofil", stripslashes ($l_t_st_profil [1][0][9]));

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.statutprofil", stripslashes ($l_t_equivalencestatut [$l_t_st_profil [1][0][2]]));

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.categorieprofil", stripslashes ($l_t_st_profil [1][0][3]));

if ($l_t_st_profil [1][0][6] == -1)
{
    $l_t_st_profil [1][0][6] = NOLIMITCAND;
}

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.nbpersosprofil", stripslashes ($l_t_st_profil [1][0][6]));


$l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "afficheraceclassesexe");

// boucles d'affichage des races, classes et sexes possibles
for ($l_i_compteur = 0; $l_i_compteur < $l_t_st_races [0]; $l_i_compteur++)
{
    if ($l_t_st_races [1][$l_i_compteur][2] != NULL)
    {
        $l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "afficherace");
        $l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficherace.race", stripslashes ($l_t_st_races [1][$l_i_compteur][1]));
        $l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "afficherace");
    }
}

for ($l_i_compteur = 0; $l_i_compteur < $l_t_st_classes [0]; $l_i_compteur++)
{
    if ($l_t_st_classes [1][$l_i_compteur][2] != NULL)
    {
        $l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "afficheclasse");
        $l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheclasse.classe", stripslashes ($l_t_st_classes [1][$l_i_compteur][1]));
        $l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "afficheclasse");
    }

}

for ($l_i_compteur = 0; $l_i_compteur < $l_t_st_sexes [0]; $l_i_compteur++)
{
    if ($l_t_st_sexes [1][$l_i_compteur][2] != NULL)
    {
        $l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "affichesexe");
        $l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "affichesexe.sexe", stripslashes ($l_t_st_sexes [1][$l_i_compteur][1]));
        $l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "affichesexe");
    }
}

$l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "afficheraceclassesexe");

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.descriptionprofil", stripslashes ($l_t_st_profil [1][0][4]));

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.debatprofil", stripslashes ($l_t_st_profil [1][0][5]));

$l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1.historiqueprofil", stripslashes ($l_t_st_profil [1][0][7]));

// Gestion de la demande d'action si il y a lieu
if ($_SESSION ['droitrecrutement'] == OUI && $l_t_st_profil [1][0][8] != NULL)
{

    switch ($l_t_st_profil [1][0][8])
    {
        case DEMANDEENLIGNE:
            $l_s_demande = DEMANDEENLIGNETXT;
            break;
        case DEMANDEHORSLIGNE:
            $l_s_demande = DEMANDEHORSLIGNETXT;
            break ;
        case DEMANDEEDITER:
            $l_s_demande = DEMANDEEDITERTXT;
            break;
        default:
            break;
    }

    $l_o_template -> addSession ($l_i_handlerafficheprofil_niv1, "affichedemande");
    $l_o_template -> setVar ($l_i_handlerafficheprofil_niv1, "affichedemande.demandeprofil", stripslashes ($l_s_demande));
    $l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "affichedemande");
}

$l_o_template -> closeSession ($l_i_handlerafficheprofil_niv1, "afficheprofil_niv1");


/*********************************************
 * Actions possibles à partir de ce profil
 *********************************************/

 $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionsprofil_niv1");

// Toujours
$l_st_affichage [NOM_VAR] = "idprofil";
$l_st_affichage [CONTENU_VAR] = $l_t_st_profil [1][0][0];
$l_st_affichage [TYPE_DISPLAY] = HIDDEN;
affiche_variable ($l_st_affichage, "input", $l_i_handleractionsprofil_niv1, NULL);

$l_st_affichage [NOM_VAR] = "iteration";
$l_st_affichage [CONTENU_VAR] = 1;
$l_st_affichage [TYPE_DISPLAY] = HIDDEN;
affiche_variable ($l_st_affichage, "input", $l_i_handleractionsprofil_niv1, NULL);


// Toujours (sauf si profil en édition ou en relecture)
if ($l_t_st_profil [1][0][2] != EDITION && $l_t_st_profil [1][0][2] != RELECTURE)
{
    $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
    $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", CREATIONDAPRES);
    $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", CREATIONDAPRESTXT);
    $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");
}

if ($_SESSION ['droitrecrutement'] == OUI)
{
    // Option Action changer le statut du profil
    $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
    $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", STATUT);
    $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", STATUTTXT);
    affiche_variable ($g_st_statut, "select", $l_i_handleractionsprofil_niv1, NULL);
    $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");

    if ($l_t_st_profil [1][0][2] != EDITION)
    {
        // Option Action éditer le profil
        $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", EDITER);
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", EDITERTXT);
        $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");
    }

    if (
    		$l_t_st_profil [1][0][8] == DEMANDEHORSLIGNE
    	|| 
    		$l_t_st_profil [1][0][8] == DEMANDEENLIGNE 
    	||
    		$l_t_st_profil [1][0][8] == DEMANDEEDITER
    	)
    {
        // Option Action Refuser action demandée
        $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", REFUSER);
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", REFUSERTXT);
        $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");

        $l_i_flag_debat = OUI;

    }

    $l_t_st_resultat = requete ("pas_candidat", $l_t_st_profil [1][0][0]);

    if ($l_t_st_resultat [0] == NON)
    {
        // Option Action effacer le profil
        $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", EFFACER);
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", EFFACERTXT);
        $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");
    }
}
else
{
    if (
    		$l_t_st_profil [1][0][2] == HORSLIGNE
    	&&
    		$l_t_st_profil [1][0][8] != DEMANDEENLIGNE
        &&
        	$l_t_st_profil [1][0][8] != DEMANDEHORSLIGNE
        &&
        	$l_t_st_profil [1][0][8] != DEMANDEEDITER
    )
    {
        // Option Action demande de remise en ligne
        $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", DEMANDEENLIGNE);
        $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", DEMANDEENLIGNETXT);
        $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");

        $l_i_flag_debat = OUI;
    }

    if ($_SESSION ['user'] == $l_t_st_profil [1][0][9])
    {
        if ($l_t_st_profil [1][0][2] == EDITION)
        {
            // Option Action Editer le profil
            $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", EDITER);
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", EDITERTXT);
            $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");
        }
        if (
        		$l_t_st_profil [1][0][2] == ENLIGNE
        	&&
        		$l_t_st_profil [1][0][8] != DEMANDEHORSLIGNE
        	&&
        		$l_t_st_profil [1][0][8] != DEMANDEEDITER
    		&&
    			$l_t_st_profil [1][0][8] != DEMANDEENLIGNE
        	)
        {
            // Option Action Demander à mettre hors ligne
            $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", DEMANDEHORSLIGNE);
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", DEMANDEHORSLIGNETXT);
            $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");

            $l_i_flag_debat = OUI;
        }

        if (
        		(
        			$l_t_st_profil [1][0][2] == HORSLIGNE 
        		|| 
        			$l_t_st_profil [1][0][2] == ENLIGNE
        		)
        		&&
        			$l_t_st_profil [1][0][8] != DEMANDEEDITER
        		&&
        			$l_t_st_profil [1][0][8] != DEMANDEHORSLIGNE
    			&&
    				$l_t_st_profil [1][0][8] != DEMANDEENLIGNE	
        	)
        {
            // Demander à mettre hors ligne et Editer
            $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "actionprofil");
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.actionprofil", DEMANDEEDITER);
            $l_o_template -> setVar ($l_i_handleractionsprofil_niv1, "actionprofil.texteaction", DEMANDEEDITERTXT);
            $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionprofil");

            $l_i_flag_debat = OUI;
        }
    }
}

if ($l_i_flag_debat == OUI)
{
    $l_o_template -> addSession ($l_i_handleractionsprofil_niv1, "debatprofil");
    $l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "debatprofil");
}



$l_o_template -> closeSession ($l_i_handleractionsprofil_niv1, "actionsprofil_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerafficheprofil_niv1, "afficheprofil_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handleractionsprofil_niv1, "actionsprofil_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
