<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : enregistrement_niv1.inc                                                                                 
* Fonction du fichier : de s'enregistrer sur le site                                                                          
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  03/02/2008                                                                             
* Version actuelle :  1.0 au 03/02/2008                                                                              
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :  /!\ TODO : virer les print de debug
* 										ajouter le captcha                                                                      
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_s_password = "";
$l_s_password_chiffre = "";
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_s_objet = "";
$l_t_champs = array ("pseudo" => "", "password" => "");

/* Contenu et inclusion des fichier de contenu */
$l_o_template -> addSession ($l_i_handlerenregistrement_niv1,  "enregistrement_niv1");


// Vérification nombre d'itérations en cours
if ($g_st_iteration [1] <= 1)
{
	// Contrôle des variables
	// Les premiers contrôles sont déjà faits sur la page principale d'enregistrement, on complète avec les compléments de sécurité si nécessaire
	// Non nécessaire actuellement
	
}

if ($g_st_iteration [1] == 1 || $l_i_erreur == OUI)
{
	$g_st_iteration [1] += 1;
	// Affichage du formulaire
	$l_o_template -> addSession ($l_i_handlerenregistrement_niv1,  "formulaire_niv1");	
	affiche_variable ($g_st_iteration, "input", $l_i_handlerenregistrement_niv1, NULL);
	affiche_variable ($g_st_pseudo, "text", $l_i_handlerenregistrement_niv1, NULL);
	affiche_variable ($g_st_courriel, "text", $l_i_handlerenregistrement_niv1, NULL);
	affiche_variable ($g_st_commentaire, "textarea", $l_i_handlerenregistrement_niv1, NULL);
	$l_o_template -> closeSession ($l_i_handlerenregistrement_niv1,  "formulaire_niv1");	
		
}
else
{
	// Pas d'erreur, On enregistre en base puis on notifie
	// Génération d'un mot de passe (8 caractères), en minuscules, non épelé
	$l_s_password = genere_password (8, OUI, NON);
	$l_s_password_chiffre = chiffre ($l_s_password);
	$l_s_password;
	// Enregistrement en base de données
	$l_t_st_resultat = requete ("enregistre_user", $g_st_pseudo [1], $l_s_password_chiffre, $g_st_courriel [1], $g_st_commentaire [1]);
	if ($l_t_st_resultat ["nbaffecte"] == 0)
	{
   		trigger_error ("Echec de l'enregistrement.", ERROR);
   		exit ();
	}
	
	// Préparation du courriel
	$l_s_objet = "Votre enregistrement sur le site ".NOM_SITE;
	$l_t_champs ["pseudo"] = $g_st_pseudo [1];
	$l_t_champs ["password"] = $l_s_password;
	$l_t_champs ["nom_site"] = NOM_SITE;
	
	$l_st_parametres ["destinataire"] = $g_st_courriel [1];
	$l_st_parametres ["copie"] = "";
	$l_st_parametres ["copie_cachee"] = "";
	$l_st_parametres ["objet"] = $l_s_objet;
	$l_st_parametres["champs"] = $l_t_champs;
	$l_st_parametres ["template"] = "bienvenue";

	// Envoi du courriel
	$l_i_resultat = courriel ($l_st_parametres);

	if ($l_i_resultat == FALSE)
	{
		// Echec de l'envoi du mail avec le mot de passe
		trigger_error ("Echec de l'envoi du courriel d'enregistrement", NOTICE);
		// On notifie qu'il y a eu un couac lors de l'envoi de l'email
		$l_o_template -> addSession ($l_i_handlerenregistrement_niv1,  "echec_niv1");
		$l_o_template -> closeSession ($l_i_handlerenregistrement_niv1,  "echec_niv1");			
			
	}
	else
	{
		// Succès de l'envoi du mot de passe, on explique ce qu'il en est au visiteur
		$l_o_template -> addSession ($l_i_handlerenregistrement_niv1,  "succes_niv1");
		$l_o_template -> closeSession ($l_i_handlerenregistrement_niv1,  "succes_niv1");		
	}
}

$l_o_template -> closeSession ($l_i_handlerenregistrement_niv1, "enregistrement_niv1");
/* Fin de fichier ***********************************************************************************/
?>
