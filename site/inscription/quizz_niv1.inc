<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : quizz_niv1.inc                                                                                 
* Fonction du fichier : construit, affiche et corrige le quizz                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  11/03/2008                                                                             
* Version actuelle : 1.0 au 11/03/2008                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0;
$l_i_flag_noannonce = OUI;
$l_st_affichage = array ("", "", NORMAL);
$l_i_quizz_ok = NON;
$l_i_resultat = NON;

/* Contenu et inclusion des fichier de contenu */
$l_st_questions = array ();
$l_st_reponses = array ();

if ($g_st_iterationquizz [CONTENU_VAR] == 1)
{
	// Génération d'un quizz
	genere_quizz ($l_st_questions, $l_st_reponses);

	// Ajout d'un grain de sel dans les ids de questions du quizz (empêche de deviner les vrais ids)
	$_SESSION ['graindeselquizz'] = rand (0, 100);
	
	// Affichage du quizz
	$l_o_template -> addSession ($l_i_handlerquizz_niv1,  "afficherquizz_niv1");
	$l_o_template -> setVar ($l_i_handlerquizz_niv1, "afficherquizz_niv1.idprofil", stripslashes ($l_i_profil));	
	
	
	for ($l_i_compteur = 0; $l_i_compteur < NOMBREQUESTIONS; $l_i_compteur ++)
	{
		// Remise à 0 du tableau d'affichage des réponses
		$l_st_affichereponses = array ();
		
		$l_o_template -> addSession ($l_i_handlerquizz_niv1,  "question");
		$l_o_template -> setVar ($l_i_handlerquizz_niv1, "question.question", stripslashes ($l_st_questions [$l_i_compteur][1]));	
		// Passage en champ caché de l'id de la question
		$g_st_idquestions = array ("idquestions", $l_st_questions [$l_i_compteur][0] + $_SESSION ['graindeselquizz'], HIDDEN);
		affiche_variable ($g_st_idquestions, "input", $l_i_handlerquizz_niv1, NULL);		
		// Affichage des réponses possibles aux questions
		$g_st_idreponses = array ("idreponses", "", NORMAL);

		for ($l_i_compteur2 = 0; $l_i_compteur2 < count ($l_st_reponses [$l_i_compteur]); $l_i_compteur2++)
		{
			$l_st_affichereponses [0] = count ($l_st_reponses [$l_i_compteur]);
			$l_st_affichereponses [1][$l_i_compteur2][0] = $l_st_reponses [$l_i_compteur][$l_i_compteur2][0];
			$l_st_affichereponses [1][$l_i_compteur2][1] = $l_st_reponses [$l_i_compteur][$l_i_compteur2][1];			
		}
		
		affiche_variable ($g_st_idreponses, "checkbox", $l_i_handlerquizz_niv1, $l_st_affichereponses);
		$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "question");
	}
		
	// Incrémenter itération
	$g_st_iterationquizz [CONTENU_VAR] = 2;
	
	// Refermer le formulaire de quizz
	affiche_variable ($g_st_iterationquizz, "input", $l_i_handlerquizz_niv1, NULL);
	
	$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "afficherquizz_niv1");
	
	
	$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
	$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerquizz_niv1, "afficherquizz_niv1");
	$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");
}
elseif ($g_st_iterationquizz [CONTENU_VAR] == 2)	
{
	// Corriger le quizz à partir des données récupérées
	
	//var_dump ($g_st_idquestions [CONTENU_VAR]);
	$l_i_resultat = corrige_quizz ($g_st_idquestions [CONTENU_VAR], $g_st_idreponses [CONTENU_VAR]);
	// Afficher le résultat du quizz
	$l_o_template -> addSession ($l_i_handlerquizz_niv1, "resultatquizz_niv1");	
	
	if ($l_i_resultat == NON)
	{
		// Score insuffisant pour aller à la page suivante
		$l_o_template -> addSession ($l_i_handlerquizz_niv1, "insuffisant");		
		$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "insuffisant");
	}
	elseif ($l_i_resultat == ERREUR)
	{
		// Tentative de triche quelconque
		$l_o_template -> addSession ($l_i_handlerquizz_niv1, "triche");		
		$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "triche");		
	}
	elseif ($l_i_resultat == OUI)
	{
		// Candidat qualifié pour déposer sa candidature !
		// Ajout score dans la BdD
		$l_st_resultat = requete ("scorequizz_ok", $_SESSION ['id']);
		if ($l_st_resultat ['nbaffecte'] == 0)
		{
			trigger_error ("Le score du candidat '".$_SESSION ['id']."' n'a pas pu etre enregistré comme suffisant en base de données", FATAL);
			exit ();
		}
		
		// Affichage
		$l_o_template -> addSession ($l_i_handlerquizz_niv1, "qualifie");
		$g_st_iteration [CONTENU_VAR] = 1;
		affiche_variable ($g_st_iteration, "input", $l_i_handlerquizz_niv1, NULL);
		$g_st_idprofil [CONTENU_VAR] = $l_i_profil;
		affiche_variable ($g_st_idprofil, "input", $l_i_handlerquizz_niv1, NULL);
		$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "qualifie");	
	}
	else
	{
		trigger_error ("Le résultat du quizz effectué par '".$_SESSION ['id']."' n'est pas reconnu.", FATAL);
		exit ();
	}
	
	$l_o_template -> closeSession ($l_i_handlerquizz_niv1, "resultatquizz_niv1");
	
	$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
	$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerquizz_niv1, "resultatquizz_niv1");
	$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");	
}

/* Fin de fichier **************************************************************************************/
?>
