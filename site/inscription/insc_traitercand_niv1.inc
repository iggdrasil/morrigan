<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : insc_traitercand_niv1.inc                                                                                 
* Fonction du fichier : affiche le formulaire pour corriger sa candidature ou la traiter si nécessaire
						(en fonction du statut de la candidature)                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de création :  22/03/2008                                                                             
* Version actuelle : 1.0 au 22/03/2008                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0;
$l_i_flag_noannonce = OUI;
$l_st_affichage = array ("", "", NORMAL);
$l_i_quizz_ok = NON;
$l_i_resultat = FALSE;
$l_i_resultat2 = AUCUN; // sert pour delta_nbpersos
$l_t_st_options = array ();


/* Contenu et inclusion des fichier de contenu */
// Vérification du score au quizz
$l_t_st_resultat = requete ("scorequizz", $_SESSION ['id']);
if (($l_t_st_resultat [0] == 0) || ($l_t_st_resultat [0] > 1))
{
	
	trigger_error ("Erreur lors du rappatriement du score au quizz de l'utilisateur '".$_SESSION ['id']."'", FATAL);
	exit ();
}
$l_i_quizz_ok = $l_t_st_resultat [1][0][0]; // Valeurs prises : OUI ou NON

if ($l_i_quizz_ok == NON)
{
	// Pas le droit de traiter cette candidature, retour à l'accueil (soupçon de triche)
	trigger_error ("L'utilisateur '".$_SESSION ['id']."' a tenté de traiter cette candidature : '".$g_i_idcand."' sans en avoir le droit (quizz non réussi)", WARNING);
	header ("Location:http://".HOST."/insc_listeannonces.php");
}

// On a vérifié par avance que le traitement était autorisé pour cette candidature
// L'utilisateur peut donc traiter la candidature si on en arrive jusqu'ici


// On affiche le rappel de l'offre à laquelle l'utilisateur postule
$l_t_st_resultat = requete ("rappel_annonce", $g_st_idcand [CONTENU_VAR]);	
	
if ($l_t_st_resultat [0] != 1)
{
	trigger_error ("L'offre demandée (".$g_st_idprofil [CONTENU_VAR].") pour le dépôt de candidature de l'utilisateur ".$_SESSION ['id']." n'est pas disponible.", FATAL);
	exit ();
}
$l_i_nbpersos = $l_t_st_resultat [1] [0] [4];

$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "traitercand_niv1");	
	
$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "rappel_annonce_niv1");
$l_o_template -> setVar ($l_i_handlercandencours_niv1, "rappel_annonce_niv1.nomannonce", stripslashes ($l_t_st_resultat [1] [0] [1]));	
$l_o_template -> setVar ($l_i_handlercandencours_niv1, "rappel_annonce_niv1.categorie", stripslashes ($l_t_st_resultat [1] [0] [2]));
$l_o_template -> setVar ($l_i_handlercandencours_niv1, "rappel_annonce_niv1.nbpersos", stripslashes ($l_i_nbpersos));
$l_o_template -> setVar ($l_i_handlercandencours_niv1, "rappel_annonce_niv1.descannonce", stripslashes ($l_t_st_resultat [1] [0] [3]));
$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "rappel_annonce_niv1");	



// En première itération, on fait le rappel de la candidature
// Plus bas, on donne plus ou moins d'options d'actions sur la candidature
if ($g_st_iteration [CONTENU_VAR] == 1)
{
	$g_st_iteration [CONTENU_VAR] ++;
	
	if ($l_t_st_resultatcand [1][0][1] == RELECTUREMJ || $l_t_st_resultatcand [1][0][1] == AVISMJ || $l_t_st_resultatcand [1][0][1] == ATTENTE || $l_t_st_resultatcand [1][0][1] == EDITIONUSER)
	{
		// Dans tous ces cas-là, on affiche juste les champs sans formulaire, et la seule action possible sera : abandonner
		$l_o_template -> addSession ($l_i_handlerrappel_niv2,  "rappel_candidature_niv2");
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.nomperso", stripslashes ($l_t_st_resultatcand [1][0][2]));	
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.race", stripslashes ($l_t_st_resultatcand [1][0][3]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.classe", stripslashes ($l_t_st_resultatcand [1][0][4]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.sexe", stripslashes ($l_t_st_resultatcand [1][0][5]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.description", stripslashes ($l_t_st_resultatcand [1][0][6]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.background", stripslashes ($l_t_st_resultatcand [1][0][7]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.buts", stripslashes ($l_t_st_resultatcand [1][0][8]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.debat", stripslashes ($l_t_st_resultatcand [1][0][9]));
		$l_o_template -> setVar ($l_i_handlerrappel_niv2, "rappel_candidature_niv2.statut", stripslashes ($l_t_st_resultatcand [1][0][1]));			
		
		// Formulaire d'action pour abandonner ou corriger la candidature - A corriger !!!
		$l_o_template -> addSession ($l_i_handlerrappel_niv2,  "action_candidature_niv2");
		affiche_variable ($g_st_idcand, "input", $l_i_handlerrappel_niv2, NULL);	
		affiche_variable ($g_st_iteration, "input", $l_i_handlerrappel_niv2, NULL);
		

		// Construction du tableau des options possibles
		// Option ne rien faire, par défaut
		$l_i_compteur = 0;
		$l_t_st_options [1][$l_i_compteur][0] = AUCUNE;
		$l_t_st_options [1][$l_i_compteur][1] = AUCUNETXT;
		$l_i_compteur ++;		
		// Autres options
		switch ($l_t_st_resultatcand [1][0][1])
		{
			case EDITIONUSER:
				$l_t_st_options [1][$l_i_compteur][0] = EDITERCAND;	
				$l_t_st_options [1][$l_i_compteur][1] = EDITERCANTXT;	
				$l_i_compteur ++;							
			case RELECTUREMJ:
			case AVISMJ:
			case ATTENTE:
				$l_t_st_options [1][$l_i_compteur][0] = ABANDONNERCAND;	
				$l_t_st_options [1][$l_i_compteur][1] = ABANDONNERCANDTXT;								
				break;
			default:
				// Jamais atteinte sauf si piratage quelconque
				break;
		}
		
		// Afficher la variable
		// Par défaut, AUCUNE action choisie
		$g_st_actiontraitercand [CONTENU_VAR] = AUCUNE;
		$g_st_actiontraitercand [TYPE_DISPLAY] = NORMAL;
		affiche_variable ($g_st_actiontraitercand, "radio", $l_i_handlerrappel_niv2, $l_t_st_options);			
		
		$l_o_template -> closeSession ($l_i_handlerrappel_niv2, "action_candidature_niv2");
		
		$l_o_template -> closeSession ($l_i_handlerrappel_niv2, "rappel_candidature_niv2");
		
		$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "rappel_candidature_niv1");
		$l_o_template -> Parse ($l_i_handlercandencours_niv1, "rappel_candidature_niv1.varcontenu", $l_i_handlerrappel_niv2, "rappel_candidature_niv2");
		$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "rappel_candidature_niv1");
	}
	else
	{
		// Ce statut n'est pas reconnu comme utilisable pour le traitement, erreur
		trigger_error ("Statut '".$l_t_st_resultatcand [1][0][1]."' interdit de traitement pour la candidature '".$g_i_idcand."' par l'utilisateur '".$_SESSION ['id']."'", FATAL);
		exit ();
	}
}
elseif ($g_st_iteration [CONTENU_VAR] == 2)
{
	$g_st_iteration [CONTENU_VAR] ++;

	if ($l_t_st_resultatcand [1][0][1] == RELECTUREMJ || $l_t_st_resultatcand [1][0][1] == AVISMJ || $l_t_st_resultatcand [1][0][1] == ATTENTE)
	{
		// Dans ces cas-là, seule action possible était : abandonner
		if ($g_i_erreur == NON && $g_st_actiontraitercand [CONTENU_VAR] == ABANDONNERCAND)
		{
			$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "resultattraitement_niv1");
			// Mettre à jour la candidature
			$l_i_resultat2 = delta_nbpersos (ABANDONNEE, $g_st_idcand [CONTENU_VAR], NULL);
			
			if ($l_i_resultat2 == ERREUR || $l_i_resultat2 == MOINS || $l_i_resultat2 == REFUS)
			{
				$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "erreurtraitement_niv1");		
				$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "erreurtraitement_niv1");		
			}
			elseif ($l_i_resultat2 == PLUS  || $l_i_resultat2 == AUCUN)
			{
				// On peut faire la requête d'abandon
				$l_i_resultat = requete ("abandonnercand", $g_st_idcand [CONTENU_VAR]);
				if ($l_i_resultat ['nbaffecte'] != 1)
				{
					trigger_error ("La candidature '".$g_st_idcand [CONTENU_VAR]."' n'a pas pu être abandonnée par '".$_SESSION ['id']."'", FATAL);
					exit ();
				}
				else
				{
					// En cas de succès
					$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "oktraitement_niv1");		
					$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "oktraitement_niv1");						
				}
			}
			
			$l_o_template -> closeSession ($l_i_handlercandencours_niv1,  "resultattraitement_niv1");		
		}
		elseif ($g_i_erreur == OUI && $g_st_actiontraitercand [CONTENU_VAR] == ABANDONNERCAND)
		{
			// Avoir une erreur ici est normalement impossible (sinon, attention, pirate à l'oeuvre)
			trigger_error ("Erreur inattendue lors de l'abandon de la candidature '".$g_st_idcand [CONTENU_VAR]."' par '".$_SESSION ['id']."'", FATAL);
			exit ();				
		}
		else
		{
			// Normalement impossible à atteindre
			trigger_error ("Action inattendue lors de l'abandon de la candidature '".$g_st_idcand [CONTENU_VAR]."' par '".$_SESSION ['id']."'", FATAL);
			exit ();				
		}		
		
	}
	elseif ($l_t_st_resultatcand [1][0][1] == EDITIONUSER)
	{
		// Dans ce cas-là, on affiche le formulaire de correction
		
		// Retranscription des variables
		$g_st_nomperso [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][2]);
		$g_st_race_cand [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][11]);
		$g_st_classe_cand [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][12]);
		$g_st_sexe_cand [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][13]);
		$g_st_description [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][6]);
		$g_st_background [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][7]);
		$g_st_buts [CONTENU_VAR] = stripslashes ($l_t_st_resultatcand [1][0][8]);
		$g_st_debatusrmj [CONTENU_VAR] = ""; // Ce champ est facultatif à remplir par l'utilisateur
		
		
		$l_o_template -> addSession ($l_i_handlerformulaire_niv2,  "formulaire_niv2");		
		
		affiche_variable ($g_st_idcand, "input", $l_i_handlerformulaire_niv2, NULL);	
		affiche_variable ($g_st_nomperso, "text", $l_i_handlerformulaire_niv2, NULL);	
		affiche_variable ($g_st_race_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_classe_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_sexe_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_description, "textarea", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_background, "textarea", $l_i_handlerformulaire_niv2, NULL);				
		affiche_variable ($g_st_buts, "textarea", $l_i_handlerformulaire_niv2, NULL);		
		$l_o_template -> setVar ($l_i_handlerformulaire_niv2, "formulaire_niv2.textedebat", stripslashes ($l_t_st_resultatcand [1][0][9]));
		affiche_variable ($g_st_debatusrmj, "textarea", $l_i_handlerformulaire_niv2, NULL);		
		affiche_variable ($g_st_iteration, "input", $l_i_handlerformulaire_niv2, NULL);	
		
		// Action : corriger
		$g_st_actiontraitercand [CONTENU_VAR] = EDITERCAND;
		$g_st_actiontraitercand [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_actiontraitercand, "input", $l_i_handlerformulaire_niv2, NULL);			
		
		$l_o_template -> closeSession ($l_i_handlerformulaire_niv2, "formulaire_niv2");
		
		$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "formulaire_niv1");
		$l_o_template -> Parse ($l_i_handlercandencours_niv1, "formulaire_niv1.varcontenu2", $l_i_handlerformulaire_niv2, "formulaire_niv2");
		$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "formulaire_niv1");
		
	}	
}
elseif ($g_st_iteration [CONTENU_VAR] >= 3)
{		
	// iteration 3 utilisée pour distinguer l'action d'abandonner (en 2 itérations) et l'action editer (en au moins 3 itérations)
	
	$g_st_iteration [CONTENU_VAR] ++;	
	
	if ($l_t_st_resultatcand [1][0][1] == EDITIONUSER && $g_i_erreur == NON)
	{
		// On met à jour la candidature avec les champs modifiés
		$l_t_st_resultat = requete ("editercand",  $_SESSION ['id'], $g_st_idcand [CONTENU_VAR], $g_st_nomperso [CONTENU_VAR], $g_st_race_cand [CONTENU_VAR][0], $g_st_classe_cand [CONTENU_VAR][0], $g_st_sexe_cand [CONTENU_VAR][0], $g_st_description [CONTENU_VAR], $g_st_background [CONTENU_VAR], $g_st_buts [CONTENU_VAR], $g_st_debatusrmj [CONTENU_VAR], EDITERCANTXT);
		if ($l_t_st_resultat ['nbaffecte'] != 1)
		{
			trigger_error ("Problème dans l'édition de la candidature '".$g_st_idcand [CONTENU_VAR]."' par '".$_SESSION ['id']."'", FATAL);
			exit ();
		}
		else
		{
			// Affichage à l'écran du résultat
			$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "resultattraitement_niv1");
			$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "oktraitement_niv1");		
			$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "oktraitement_niv1");	
			$l_o_template -> closeSession ($l_i_handlercandencours_niv1,  "resultattraitement_niv1");
		
			// On notifie par mail le MJ (?)
			// TODO : si nécessaire, notifier par mail
		}
		
	}
	elseif ($l_t_st_resultatcand [1][0][1] == EDITIONUSER && $g_i_erreur == OUI)
	{
		// Erreur dans les champs, on représente le formulaire
		$l_o_template -> addSession ($l_i_handlerformulaire_niv2,  "formulaire_niv1");		
		
		affiche_variable ($g_st_idcand, "input", $l_i_handlerformulaire_niv2, NULL);	
		affiche_variable ($g_st_nomperso, "text", $l_i_handlerformulaire_niv2, NULL);	
		affiche_variable ($g_st_race_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_classe_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_sexe_cand, "select", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_description, "textarea", $l_i_handlerformulaire_niv2, NULL);
		affiche_variable ($g_st_background, "textarea", $l_i_handlerformulaire_niv2, NULL);				
		affiche_variable ($g_st_buts, "textarea", $l_i_handlerformulaire_niv2, NULL);		
		affiche_variable ($g_st_debatusrmj, "textarea", $l_i_handlerformulaire_niv2, NULL);		
		affiche_variable ($g_st_iteration, "input", $l_i_handlerformulaire_niv2, NULL);	
		
		// Action : corriger
		$g_st_actiontraitercand [CONTENU_VAR] = EDITERCAND;
		$g_st_actiontraitercand [TYPE_DISPLAY] = HIDDEN;
		affiche_variable ($g_st_actiontraitercand, "input", $l_i_handlerformulaire_niv2, NULL);			
		
		$l_o_template -> closeSession ($l_i_handlerformulaire_niv2, "formulaire_niv2");
				
		$l_o_template -> addSession ($l_i_handlercandencours_niv1,  "formulaire_niv1");
		$l_o_template -> Parse ($l_i_handlercandencours_niv1, "formulaire_niv1.varcontenu2", $l_i_handlerformulaire_niv2, "formulaire_niv2");
		$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "formulaire_niv1");
	}
	else
	{
		// Normalement, impossible d'arriver là
		trigger_error ("Erreur inattendue (2) lors de l'abandon de la candidature '".$g_st_idcand [CONTENU_VAR]."' par '".$_SESSION ['id']."'", FATAL);
		exit ();	
	}
}	


$l_o_template -> closeSession ($l_i_handlercandencours_niv1, "traitercand_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlercandencours_niv1, "offre_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlercandencours_niv1, "traitercand_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


/* Fin de fichier ***********************************************************************************/
?>
