<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : insc_niv1_login.inc                                                                                 
* Fonction du fichier : affiche le cartouche de login de l'user                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  16/02/2008                                                                             
* Version actuelle : 1.0 au 16/02/2008                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_st_var_login = array ("login", "", NORMAL);
$l_st_var_password = array ("password", "", NORMAL);
$l_st_var_referer = array ("referer", $l_s_referer, HIDDEN);
$l_st_affichage = array ();

/* Contenu et inclusion des fichier de contenu */
if ($l_i_loginerr == 1)
{
    // Ici, on triche, seul le champ login sera considéré comme faux, pour de simples raisons d'affichage
    // de message d'erreur
    $l_st_var_login [2] = 2;
}


$l_o_template -> addSession ($l_i_handlerloginusr_niv1,  "loginusr_niv1");

affiche_variable ($l_st_var_login, "input", $l_i_handlerloginusr_niv1, NULL);
affiche_variable ($l_st_var_password, "password", $l_i_handlerloginusr_niv1, NULL);
affiche_variable ($l_st_var_referer, "referer", $l_i_handlerloginusr_niv1, NULL);

$l_o_template -> closeSession ($l_i_handlerloginusr_niv1, "loginusr_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerloginusr_niv1, "loginusr_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");


/* Fin de fichier ***********************************************************************************/
?>

