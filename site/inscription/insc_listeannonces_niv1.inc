<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : insc_listeannonces_niv1.inc                                                                                 
* Fonction du fichier : affiche les annonces disponibles pour le visiteur                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  16/02/2008                                                                             
* Version actuelle : 1.0 au 16/02/2008                                                                               
* License du projet : GPL                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :     - effacer les print de debug  
* 								- quelques petits todo qui trainent                                                                  
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_i_nocand = NON;
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_t_st_equivalencecandidature = array (EDITIONUSERTXT, RELECTUREMJTXT, AVISMJTXT, ENATTENTETXT, ABANDONNEETXT, ACCEPTEETXT, REFUSEETXT);
$l_i_compteur = 0;
$l_i_flag_noannonce = OUI;


$l_st_affichage = array ("", "", NORMAL);

/* Contenu et inclusion des fichier de contenu */
// Vérification du droit de poser une candidature
if ($_SESSION ['mj'] == OUI)
{
	$l_i_nocand = OUI;
}

// Recherche de candidature en cours
if ($_SESSION ['id'] != "")
{
	$l_t_st_resultat = requete ("candidatureencours_light", $_SESSION ['id']);
}
else
{
	$l_t_st_resultat [0] = 0;
}

	
if ($l_t_st_resultat [0] == 1)
{
	if ($l_t_st_resultat [1][0][1] == EDITIONUSR || $l_t_st_resultat [1][0][1] == RELECTUREMJ || $l_t_st_resultat [1][0][1] == AVISMJ || $l_t_st_resultat [1][0][1] == ENATTENTE)
	{
		// Si la candidature est dans le bon statut, on peut s'y rendre pour la consulter
		// afficher candidature en question
		$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "candencours");
		$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "candencours.nomannonce", $l_t_st_resultat [1] [0] [4]);
		$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "candencours.nomperso", $l_t_st_resultat [1] [0] [2]);
		$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "candencours.statut", $l_t_st_equivalencecandidature [$l_t_st_resultat [1] [0] [1]]);
		$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "candencours.datemodif", $l_t_st_resultat [1] [0] [3]);
		$l_st_affichage = array ("idcand", $l_t_st_resultat [1] [0] [0], HIDDEN);	
		affiche_variable ($l_st_affichage, "input", $l_i_handlerlisteannonces_niv1, NULL);
		// Le champ iteration est passé en dur dans le formulaire du template
		$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "candencours");	
	}
	// Si candidature en cours, interdiction de poser une autre candidature
	$l_i_nocand = OUI;

}
elseif ($l_t_st_resultat [0] > 1)
{
	// Plus d'une candidature pour cet utilisateur : problème !	
	trigger_error ("Plus d'une candidature en même temps pour cet utilisateur (".$_SESSION [id].").", FATAL);
	exit ();
}

// Liste des annonces, lien vers le dépôt de candidature si autorisé (les utilisateurs non logués ont un lien (ancre) vers le login)
$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "listeannonces_niv1");

$l_t_st_resultat = requete ("listeannonces");

if ($l_t_st_resultat [0] > 0)
{

	
	// Boucle d'affichage de toutes les annonces classées par catégorie
	
	for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_st_resultat [1]); $l_i_compteur++)
	{
		if ($l_t_st_resultat [1] [$l_i_compteur] [4] != 0)
		{
			// Au moins une place restante pour cette candidature
			// Un peu de formatage de la sortie requete
			if ($l_t_st_resultat [1] [$l_i_compteur] [4] == -1)
			{
				$l_t_st_resultat [1] [$l_i_compteur] [4] = NOLIMITCAND; // /!\ TODO : placer ce morceau de texte en paramètre !
			}
			// On affiche
			$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "annonce");
			$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "annonce.nomannonce", stripslashes ($l_t_st_resultat [1] [$l_i_compteur] [1]));	
			$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "annonce.categorie", stripslashes ($l_t_st_resultat [1] [$l_i_compteur] [2]));
			$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "annonce.nbpersos", stripslashes ($l_t_st_resultat [1] [$l_i_compteur] [4]));
			$l_o_template -> setVar ($l_i_handlerlisteannonces_niv1, "annonce.descannonce", stripslashes ($l_t_st_resultat [1] [$l_i_compteur] [3]));
			
			if ($l_i_nocand == NON && $l_i_logok == OUI)
			{
				$l_st_affichage = array ("idprofil", $l_t_st_resultat [1] [$l_i_compteur] [0], HIDDEN);
				
				// Lien vers dépôt de candidature
				$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "lienannonce");
				affiche_variable ($l_st_affichage, "input", $l_i_handlerlisteannonces_niv1, NULL);

				$l_st_affichage = array ("iteration", 1, HIDDEN);	
				affiche_variable ($l_st_affichage, "input", $l_i_handlerlisteannonces_niv1, NULL);
				
				$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "lienannonce");	
			
			}
			elseif ($l_i_logok == NON)
			{
				// Lien vers l'ancre de login
				$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "ancrelogin");				
				$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "ancrelogin");						
			}
			
			$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "annonce");		
			
			if ($l_i_flag_noannonce)
			{
				$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "listeannonces");
				$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "listeannonces");					
			}
				
			$l_i_flag_noannonce = NON;	
			
		}	
	}
}

//print ("Flag annonces : ".$l_i_flag_noannonce."<br>");

if ($l_t_st_resultat [0] == 0 || $l_i_flag_noannonce == OUI)
{
	$l_o_template -> addSession ($l_i_handlerlisteannonces_niv1,  "pasannonces_niv1");
	$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "pasannonces_niv1");		
}


$l_o_template -> closeSession ($l_i_handlerlisteannonces_niv1, "listeannonces_niv1");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerlisteannonces_niv1, "candidature_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerlisteannonces_niv1, "listeannonces_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Fin de fichier ***********************************************************************************/
?>
