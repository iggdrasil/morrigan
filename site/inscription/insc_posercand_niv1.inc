<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : insc_posercand_niv1.inc                                                                                 
* Fonction du fichier : affiche le formulaire pour déposer sa candidature, et contrôle les entrées du candidat                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de création :  24/02/2008                                                                             
* Version actuelle : 1.0 au 24/02/2008                                                                               
* License du projet : GPL                                                                             
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* Définition des variables */
// VARIABLES LOCALES
$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
$l_i_compteur = 0;
$l_i_flag_noannonce = OUI;
$l_st_affichage = array ("", "", NORMAL);
$l_i_quizz_ok = NON;
$l_i_resultat = FALSE;
$l_i_resultat2 = AUCUN; // sert pour delta_nbpersos


/* Contenu et inclusion des fichier de contenu */
// Vérification du score au quizz
$l_t_st_resultat = requete ("scorequizz", $_SESSION ['id']);
if (($l_t_st_resultat [0] == 0) || ($l_t_st_resultat [0] > 1))
{
	trigger_error ("Erreur lors du rappatriement du score au quizz de l'utilisateur '".$_SESSION ['id']."'", FATAL);
	exit ();
}
$l_i_quizz_ok = $l_t_st_resultat [1][0][0]; // Valeurs prises : OUI ou NON


// Vérification du droit de poser une candidature
$l_t_st_resultat = requete ("droit_depotcand", $_SESSION ['id'], $g_st_idprofil [CONTENU_VAR]);


if ($_SESSION ['mj'] == OUI || $l_t_st_resultat [0] > 0)
{
	// MJ n'a pas le droit de déposer sa candidature
	// Un utilisateur qui a déjà une candidature en cours ne peut pas poser sa candidature une deuxième fois
	// Postuler à cette offre est impossible pour les raisons suivantes : 
	// - déjà trop de candidats sur cette offre
	// - offre hors ligne
	
	$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "cand_interdite_niv1");
	$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "cand_interdite_niv1");
	
	$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
	$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerposercand_niv1, "cand_interdite_niv1");
	$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");	
	
}
// Vérification que l'utilisateur a passé le quizz avec succès
elseif ($l_i_quizz_ok == NON and QUIZZ)
{
	// Utilisateur n'a pas encore sacrifié au quizz
	header ("Location:http://".HOST."/quizz.php?idprofil=".$g_st_idprofil [CONTENU_VAR]."&iteration=1");
}
else
{
	// Tout va bien, l'utilisateur peut postuler
	// On affiche le rappel de l'offre à laquelle l'utilisateur postule
	$l_t_st_resultat = requete ("affiche_annonce", $g_st_idprofil [CONTENU_VAR]);	
	
	if ($l_t_st_resultat [0] != 1)
	{
		trigger_error ("L'offre demandée (".$g_st_idprofil [CONTENU_VAR].") pour le dépôt de candidature de l'utilisateur ".$_SESSION ['id']." n'est pas disponible.", FATAL);
		exit ();
	}
	$l_i_nbpersos = $l_t_st_resultat [1] [0] [4];
	
	$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "posercand_niv1");	
	
	$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "rappel_annonce_niv1");
	$l_o_template -> setVar ($l_i_handlerposercand_niv1, "rappel_annonce_niv1.nomannonce", stripslashes ($l_t_st_resultat [1] [0] [1]));	
	$l_o_template -> setVar ($l_i_handlerposercand_niv1, "rappel_annonce_niv1.categorie", stripslashes ($l_t_st_resultat [1] [0] [2]));
	$l_o_template -> setVar ($l_i_handlerposercand_niv1, "rappel_annonce_niv1.nbpersos", stripslashes ($l_i_nbpersos));
	$l_o_template -> setVar ($l_i_handlerposercand_niv1, "rappel_annonce_niv1.descannonce", stripslashes ($l_t_st_resultat [1] [0] [3]));
	$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "rappel_annonce_niv1");	
	
	// Selon l'état des champs passés en formulaire, on enregistre la candidature ou on affiche/réaffiche le formulaire
	$l_i_resultat2 = delta_nbpersos (RELECTURE, NULL, $g_st_idprofil [CONTENU_VAR]);
	if ($l_i_resultat2 == ERREUR || $l_i_resultat2 == PLUS || $l_i_resultat2 == AUCUN)
	{
		$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "erreurcandidature_niv1");		
		$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "erreurcandidature_niv1");		
	}
	elseif ($l_i_resultat2 == REFUS)
	{
		$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "refuscandidature_niv1");		
		$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "refuscandidature_niv1");		
	}	
	elseif (($g_i_erreur == NON) && ($g_st_iteration [CONTENU_VAR] > 1) && ($l_i_resultat2 == MOINS))
	{
		// Formulaire correct, on enregistre la candidature et on notifie
		$l_t_st_resultat = requete ("enregistre_candidature", $_SESSION ['id'], $g_st_idprofil [CONTENU_VAR], $g_st_nomperso [CONTENU_VAR], $g_st_race_ouverte [CONTENU_VAR][0], $g_st_classe_ouverte [CONTENU_VAR][0], $g_st_sexe_ouvert [CONTENU_VAR][0], $g_st_description [CONTENU_VAR], $g_st_background [CONTENU_VAR], $g_st_buts [CONTENU_VAR], $g_st_debatusrmj [CONTENU_VAR], DEPOSERCANDTXT);
		if ($l_t_st_resultat ["nbaffecte"] == 0)
		{
			trigger_error ("Problème lors de l'enregistrement de la candidature pour l'utilisateur '".$_SESSION ['id']."' et le profil '".$g_st_idprofil [CONTENU_VAR]."' en base de données.", FATAL);
			exit ();
		}
		
		// On fait varier nbpersos comme nécessaire
		$l_i_resultat = modifie_nbpersos ($g_st_idprofil [CONTENU_VAR], $l_i_resultat2);
		

		// On notifie
		// - Par mail
		// Préparation du courriel
		$l_s_objet = "Votre candidature sur le site ".NOM_SITE;
		$l_t_champs ["pseudo"] = $_SESSION ['user'];
		$l_t_champs ["nomperso"] = $g_st_nomperso [CONTENU_VAR];
		$l_t_champs ["nom_site"] = NOM_SITE;
	
		$l_st_parametres ["destinataire"] = $_SESSION ['email'];
		$l_st_parametres ["copie"] = "";
		$l_st_parametres ["copie_cachee"] = "";
		$l_st_parametres ["objet"] = $l_s_objet;
		$l_st_parametres["champs"] = $l_t_champs;
		$l_st_parametres ["template"] = "depot_cand";

		// Envoi du courriel
		$l_i_resultat = courriel ($l_st_parametres);
		
		if ($l_i_resultat == FALSE)
		{
			// Echec de l'envoi du mail avec le mot de passe
			trigger_error ("Echec de l'envoi du courriel du dépot de candidature de '".$_SESSION ['id']."pour le personnage '".$g_st_nomperso [CONTENU_VAR]."'", NOTICE);		
		}
		
		// - à l'écran
		$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "okcandidature_niv1");		
		$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "okcandidature_niv1");			
		
		
	}
	else
	{
		$g_st_iteration [CONTENU_VAR] ++;
		
		// Premier affichage ou erreurs dans les saisies
		$l_o_template -> addSession ($l_i_handlerposercand_niv1,  "formulaire_niv1");		
		
		affiche_variable ($g_st_idprofil, "input", $l_i_handlerposercand_niv1, NULL);	
		affiche_variable ($g_st_nomperso, "text", $l_i_handlerposercand_niv1, NULL);	
		affiche_variable ($g_st_race_ouverte, "select", $l_i_handlerposercand_niv1, NULL);
		affiche_variable ($g_st_classe_ouverte, "select", $l_i_handlerposercand_niv1, NULL);
		affiche_variable ($g_st_sexe_ouvert, "select", $l_i_handlerposercand_niv1, NULL);
		affiche_variable ($g_st_description, "textarea", $l_i_handlerposercand_niv1, NULL);
		affiche_variable ($g_st_background, "textarea", $l_i_handlerposercand_niv1, NULL);				
		affiche_variable ($g_st_buts, "textarea", $l_i_handlerposercand_niv1, NULL);		
		affiche_variable ($g_st_debatusrmj, "textarea", $l_i_handlerposercand_niv1, NULL);		
		affiche_variable ($g_st_iteration, "input", $l_i_handlerposercand_niv1, NULL);	
		
		$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "formulaire_niv1");			
	}
	
	$l_o_template -> closeSession ($l_i_handlerposercand_niv1, "posercand_niv1");		
	
	
	$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
	$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerposercand_niv1, "offre_niv1");
	$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

	$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
	$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlerposercand_niv1, "posercand_niv1");
	$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

}
/* Fin de fichier ***********************************************************************************/
?>
