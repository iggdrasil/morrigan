<?php
/****************************************************************************************************
*                                                                                                   *
*           PROJET MORRIGAN                                                                         *
*                                                                                                   *
* Nom du fichier : erreur.php                                                                       *
* Fonction du fichier : page d'erreur. l'utilisateur lambda est redirig� vers cette page en cas     *
* d'erreur                                                                                          *
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  *
* Date de cr�ation : 04/04/2004                                                                     *
* Version actuelle : 1.0 au 04/04/2004                                                              *
* License du projet : GPL                                                                               *
* Derni�res modifications :                                                                         *
* Remarques particuli�res :                                                                         *
*                                                                                                   *
*****************************************************************************************************/

/*  Mode silencieux. ABSOLUMENT RIEN n'est envoy� au client tant que la page n'est pas construite enti�rement
(sauf les en-t�tes) */
ob_start();

/* Ouverture de session */
// N/A

/* D�finition des variables */
// VARIABLES GLOBALES

// VARIABLES LOCALES

/* R�cup�ration des variables pass�es en formulaire */

/* R�cup�ration des variables de session */

/* Inclusion des biblioth�ques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");

/* Red�finition du gestionnaire d'erreurs */
// N/A ---  la gestion des erreurs normales du site est d�sactiv�e ici, sinon, on tourne en boucle si une erreur est
// d�tect�e sur la page


/* Connexion � la Base de Donn�es */
// N/A

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlererreur_niv1 = $l_o_template -> Open ("local/tpl/tg_erreur_niv1.tpl");

/* Contenu et inclusion des fichier de contenu */
// Erreur niveau 2
include ("erreur/erreur_niv1.inc");

// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Erreur");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlererreur_niv1, "erreur_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Parsage final de la page */
$l_o_template -> Display ();

/* D�connexion de la Base de Donn�es */
// N/A

/* Envoi de la page au client */
ob_end_flush();
/* Fin de fichier ***********************************************************************************/
?>
