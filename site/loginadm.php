<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : loginadm.php                                                                                 
* Fonction du fichier : page transparente permettant de loguer l'admin si ses coordonn�es sont bonnes                                                                            
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                          
* Date de cr�ation : 02/07/2004                                                                               
* Version actuelle : 1.0 au 02/07/2004                                                                               
* License du projet : GPL                                                                              
* Derni�res modifications :                                                                         
* Remarques particuli�res : nettoyer le fichier !                                                                        
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoy� au client tant que la page n'est pas construite enti�rement
ob_start ();

/* Ouverture de session */
session_start ();

/* D�finition des variables */
// VARIABLES GLOBALES

// VARIABLES LOCALES
$l_s_login = "";
$l_s_password = "";
$l_i_ok= 0;

$g_st_var_login = array ("login", "", 0);
$g_st_var_password = array ("password", "", 0);

/* R�cup�ration des variables de session */
// N/A

/* Inclusion des biblioth�ques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/verif_mail.inc");



/* Red�finition du gestionnaire d'erreurs */
$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion � la Base de Donn�es */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion � la base de donn�es du site",  FATAL);
	exit ();
}

/* R�cup�ration des variables pass�es en formulaire */
recup_variable ($g_st_var_login, "text");
recup_variable ($g_st_var_password, "password");

/* Instantiation de l'objet de template */
// N/A

/* Contenu et inclusion des fichier de contenu */
$l_i_ok = login_user ($g_st_var_login [1], $g_st_var_password [1]);
//print("L_i_ok:".$l_i_ok."<br>");

if ($l_i_ok == 0)
{
    header ("Location:http://".HOST."/admin.php?erreur=1");
}
else
{
    header ("Location:http://".HOST."/admin.php");
}

/* Parsage final de la page */
// N/A

/* D�connexion de la Base de Donn�es */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de donn�es du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();

/* Fin de fichier ***********************************************************************************/
?>

