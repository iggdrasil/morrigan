<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : news.php                                                                                                           
* Fonction du fichier : page d'affichage de toutes les news.                                           
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                         
* Date de création : 12/01/2009                                                                                                   
* Version actuelle : 1.0 au 12/01/2009                                                               
* License du projet : GPL                                                                                            
* Dernières modifications :                                                                                   
* Remarques particulières :                                                                                  
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = NON;
$l_i_importedroitsok = NON;
$l_i_erreur = NON;
$l_s_referer = "accueil";

/* Récupération des variables passées en formulaire */

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
//include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
include ("lib/mdp.inc");


/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}
/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handleraccueil_niv1 = $l_o_template -> Open ("local/tpl/tg_accueil_niv1.tpl");
$l_i_handlernews_niv1 = $l_o_template -> Open ("local/tpl/tg_affichenews_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");

if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable

{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();
if ($l_i_logok == OUI)
{
	$l_i_verifdroitsok = verif_droits ();
}
//print($l_i_logok);

/* Contenu et inclusion des fichier de contenu */
// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
// News
include ("news/affichenews_niv1.inc");


// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "News");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");

$l_o_template -> addSession ($l_i_handlersquelette, "contenupage");
$l_o_template -> Parse ($l_i_handlersquelette, "contenupage.varcontenupage", $l_i_handlernews_niv1, "news_niv1");
$l_o_template -> closeSession ($l_i_handlersquelette, "contenupage");

/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
