<?php
/****************************************************************************************************
*                                                                                                                                                    
*           PROJET MORRIGAN                                                                                                              
*                                                                                                                                                    
* Nom du fichier : insc_posercand.php                                                                                                           
* Fonction du fichier : page permettant de déposer une candidature                                         
* Auteur :  Arkenlond (arkenlond@peacefrogs.net)                                                                                                                         
* Date de création : 24/02/2008                                                                                                    
* Version actuelle : 1.0 au 24/02/2008                                                               
* License du projet :  GPL                                                                                           
* Dernières modifications :                                                                                   
* Remarques particulières :   /!\ TODO : virer les print de debug 
* 							  /!\ TODO : renforcer le contrôle de doublon de nom de persos avec la table de persos du jeu
* 								                                                                            
*                                                                                                                            
*****************************************************************************************************/
// Mode silencieux. ABSOLUMENT RIEN n'est envoyé au client tant que la page n'est pas construite entièrement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des bibliothèques */
include ("local/def/def_vars.inc");
include ("local/def/def_vars_admin.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");
include ("lib/gestionlogin.inc");
include ("lib/formulaire.inc");
//include ("lib/mdp.inc");
include ("Mail.php");
include ("lib/courriel.inc");
include ("lib/delta_nbpersos.inc");
//include ("lib/verif_mail.inc");

/* Définition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion à la BDD
$g_i_debug == 0;
$g_st_idprofil = array ("idprofil", "", HIDDEN);
$g_st_nomperso = array ("nomperso", "", NORMAL);
$g_st_race_ouverte = array ("race_ouverte", "", NORMAL);
$g_st_classe_ouverte = array ("classe_ouverte", "", NORMAL);
$g_st_sexe_ouvert = array ("sexe_ouvert", "", NORMAL);
$g_st_description = array ("description", "", NORMAL);
$g_st_background = array ("background", "", NORMAL);
$g_st_buts = array ("buts", "", NORMAL);
$g_st_debatusrmj = array ("debatusrmj", "", NORMAL);
$g_st_iteration = array ("iteration", "", HIDDEN);
$g_i_erreur == NON;
$g_i_numeroprofil = 0;

// VARIABLES LOCALES
$l_i_old_error_handler = 0;
$l_i_logok = 0; // user logué?
$l_i_verifdroitsok = NON;
$l_i_importedroitsok = NON;
$l_i_erreur = NON;
$l_s_referer = "insc_posercand";

/*
$l_i_loginerr = 0;
*/

/* Récupération des variables de session */
// Variable de mode de debug définie?
if (isset ($_SESSION['debug'])) $g_i_debug = $_SESSION['debug'];
else $g_i_debug = 0;




/* Redéfinition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion à la Base de Données */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion à la base de données du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerposercand_niv1 = $l_o_template -> Open ("local/tpl/tg_posercand_niv1.tpl");
$l_i_handlerloginusr_niv1 = $l_o_template -> Open ("local/tpl/tg_loginusr_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");

/* Récupération des variables passées en formulaire et Querystring */
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable

{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}

/* Contenu et inclusion des fichier de contenu */
// Vérification de l'user logué
$l_i_logok = verif_log_user ();
if ($l_i_logok == OUI)
{
	$l_i_verifdroitsok = verif_droits ();
}
//print($l_i_logok);

//Menu de gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

// Page
if ($l_i_logok == NON)
{
    // Utilisateur non logué => Possibilité de se loguer
    include ("inscription/insc_login_niv1.inc");
}
else 
{
	recup_variable ($g_st_idprofil, "integer");
	$l_i_erreur = controle_variable ($g_st_idprofil, array ("text", 0, 11)); // variables en dur pas top
	if ($l_i_erreur == OUI)
	{
		trigger_error ("Le profil demandé pour postuler n'est pas valable", FATAL);
		exit ();
	}
	
	print ("Numéro profil=".$g_st_idprofil [CONTENU_VAR]."<br>");
	$g_i_numeroprofil = $g_st_idprofil [CONTENU_VAR];
	
	recup_variable ($g_st_iteration, "integer");
	$l_i_erreur = controle_variable (&$g_st_iteration, array ("text", 0, 11)); // variables en dur pas top
	print ("Erreur iteration : ".$g_st_iteration [CONTENU_VAR]."<br>");
	if ($l_i_erreur == OUI)
	{
		trigger_error ("L'itération du dépôt de candidature ne correspond à rien", FATAL);
		exit ();
	}
	
	if ($g_st_iteration [CONTENU_VAR] > 1)
	{
		recup_variable ($g_st_nomperso, "text");
		// Ce contrôle est totalement insuffisant vu qu'il ne contrôle que les candidatures en cours, et pas les personnages en jeu !
		if (controle_variable (&$g_st_nomperso, array ("text", OUI, 100, NULL)) == OUI) $g_i_erreur = OUI;
		print ("Dis moi dis moi s'il y a une erreur ici nom ! ".$g_i_erreur."<br>");
        recup_variable ($g_st_race_ouverte, "array");
        if (controle_variable (&$g_st_race_ouverte, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;
   		print ("Dis moi dis moi s'il y a une erreur ici  race! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_classe_ouverte, "array");
        if (controle_variable (&$g_st_classe_ouverte, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;    
    	print ("Dis moi dis moi s'il y a une erreur ici classe ! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_sexe_ouvert, "array");
        if (controle_variable (&$g_st_sexe_ouvert, array ("select", 1, 1)) == OUI) $g_i_erreur = OUI;   
     	print ("Dis moi dis moi s'il y a une erreur ici sexe ! ".$g_i_erreur."<br>");   
        recup_variable ($g_st_description, "text");
        if (controle_variable (&$g_st_description, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
		print ("Dis moi dis moi s'il y a une erreur ici descro ! ".$g_i_erreur."<br>");
        recup_variable ($g_st_background, "text");
        if (controle_variable (&$g_st_background, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
    	print ("Dis moi dis moi s'il y a une erreur ici background ! ".$g_i_erreur."<br>");    
        recup_variable ($g_st_buts, "text");
        if (controle_variable (&$g_st_buts, array ("text", 0, 2000)) == OUI) $g_i_erreur = OUI; // var en dur à passer en define propre
		print ("Dis moi dis moi s'il y a une erreur ici buts ! ".$g_i_erreur."<br>");	
        recup_variable ($g_st_debatusrmj, "text");
        if ((controle_variable (&$g_st_debatusrmj, array ("text", 0, 2000)) == OUI) && ($g_st_debatusrmj [CONTENU_VAR] != "")) $g_i_erreur = OUI; // var en dur à passer en define propre
        if ($g_st_debatusrmj [CONTENU_VAR] == "")
		{
			// hack pour invalider l'erreur sur ce champ s'il est vide (champ facultatif)
			$g_st_debatusrmj [TYPE_DISPLAY] = NORMAL;
			
		}       				
	}

	//print ("Dis moi dis moi s'il y a une erreur ici ! ".$g_i_erreur."<br>");

	include ("inscription/insc_posercand_niv1.inc");
}

// Construction finale de la page
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Dépôt de candidature");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");


/* Parsage final de la page */
$l_o_template -> Display ();

/* Déconnexion de la Base de Données */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de données du site",  NOTICE);
	exit ();
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>
