<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                       
*                                                                                                  
* Nom du fichier : erreur_niv1.inc                                                                                 
* Fonction du fichier : Affiche le message d'erreur par d�faut lorsqu'une erreur survient sur le site                                                                           
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                            
* Date de cr�ation :  14/04/2004                                                                             
* Version actuelle :  1.0 au 17/04/2004                                                                              
* License du projet : GPL                                                                             
* Derni�res modifications :                                                                         
* Remarques particuli�res :                                                                         
*                                                                                                   
*****************************************************************************************************/

/* D�finition des variables */
// VARIABLES LOCALES
// N/A

/* Contenu et inclusion des fichier de contenu */
logout_user ();

$l_o_template -> addSession ($l_i_handlererreur_niv1,  "erreur_niv1");
$l_o_template -> setVar ($l_i_handlererreur_niv1, "erreur_niv1.varurlaccueil", "http://".HOST."/index.php");
$l_o_template -> closeSession ($l_i_handlererreur_niv1, "erreur_niv1");

/* Fin de fichier ***********************************************************************************/
?>
