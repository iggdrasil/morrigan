<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin.php                                                                                 
* Fonction du fichier : page d'accueil de l'administration : pr�sente les modules accessibles � l'admin s'il 
*                                  est logu�. S'il ne l'est pas, pr�sente le cartouche de login 
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                           
* Date de cr�ation :  08/05/2004                                                                              
* Version actuelle :  1.0 au 08/05/2004                                                                              
* License du projet : GPL                                                                              
* Derni�res modifications :                                                                         
* Remarques particuli�res :                                                                         
*                                                                                                   
*****************************************************************************************************/

// Mode silencieux. ABSOLUMENT RIEN n'est envoy� au client tant que la page n'est pas construite enti�rement
ob_start ();

/* Ouverture de session */
session_start ();

/* Inclusion des biblioth�ques */
include ("local/def/def_vars.inc");
include ("lib/erreur.inc");
include ("lib/bdd_postgres.inc");
include ("lib/vtemplate.class.php");

include ("lib/gestionlogin.inc");
include("lib/formulaire.inc");

/* D�finition des variables */
// VARIABLES GLOBALES
$g_i_lien=0; // descripteur de connexion � la BDD

// VARIABLES LOCALES
$l_i_logok = NON; // user logu�? droits ok?
$l_i_old_error_handler = NON;

/* R�cup�ration des variables pass�es en formulaire et Querystring*/
if (isset ($_GET ['erreur'])) // en cas d'erreur sur le login admin, juste pour information. 
// Si quelqu'un pirate, il ne verra rien avec cette variable, vu que le test critique se fait dans le veriflog
// et pas sur la valeur de cette variable
{
    $l_i_loginerr = $_GET ['erreur'];
    settype ($l_i_loginerr, "integer");
}
else
{
    $l_i_loginerr = 0;
}


/* R�cup�ration des variables de session */



/* Red�finition du gestionnaire d'erreurs */
//$l_i_old_error_handler = set_error_handler("myErrorHandler");

/* Connexion � la Base de Donn�es */
$g_i_lien = ouvrebdd();

if (!$g_i_lien)
{
	trigger_error ("Echec de la connexion � la base de donn�es du site",  FATAL);
	exit ();
}

/* Instantiation de l'objet de template */
$l_o_template = new VTemplate;
$l_i_handlersquelette = $l_o_template -> Open ("local/tpl/tg_squelette.tpl");
$l_i_handlerloginadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_loginadmin_niv1.tpl");
$l_i_handleradmininscription_niv1 = $l_o_template -> Open ("local/tpl/tg_admininscription_niv1.tpl");
$l_i_handleradmingestionnews_niv1 = $l_o_template -> Open ("local/tpl/tg_admingestionnews_niv1.tpl");
$l_i_handleradminrecrutement_niv1 = $l_o_template -> Open ("local/tpl/tg_adminrecrutement_niv1.tpl");
$l_i_handlerboitelogin_niv1 = $l_o_template -> Open ("local/tpl/tg_boitelogin.tpl");
$l_i_handlermenuadmin_niv1 = $l_o_template -> Open ("local/tpl/tg_menuadmin.tpl");

/* Contenu et inclusion des fichier de contenu */


// V�rification de l'user logu�
$l_i_logok = verif_log_user ();

print($l_i_logok);

// Boite de login gauche
include ("menuleft/boitelogin_niv1.inc");
// Menu d'administration
include ("menuleft/menuadmin_niv1.inc");

if ($l_i_logok == NON)
{
       include ("admin/admin_niv1_login.inc");
}
else
{
    if ( $l_i_logok == NON)
    {
        trigger_error ("Erreur lors de la v�rification des droits de l'admin ".$_SESSION ["user"], FATAL);
    }
    
    //include ("admin/admin_niv1_adminmj.inc");
    
    if ($_SESSION ["droitnews"] == OUI) 
        include ("admin/admin_niv1_gestionnews.inc");
        
    if ($_SESSION ["droitinscription"] == OUI)
        include ("admin/admin_niv1_inscription.inc");
        
    if ($_SESSION ["droitrecrutement"] == OUI)
    	include ("admin/admin_niv1_recrutement.inc");
    
    if ($_SESSION ["droiteditregles"] == OUI)
        //include ("admin/admin_niv1_edit_regles.inc");
        print ("Ici sera l'admin des r�gles<br>");
       
    if ($_SESSION ["droiteditmonde"] == OUI)
        //include ("admin/admin_niv1_edit_monde.inc");
        print ("Ici sera l'admin du background<br>");        
        
    if ($_SESSION ["droitadmin"] == OUI)
        //include ("admin/admin_niv1_admintotale.inc");
        print ("Ici sera l'admin du super admin<br>");
}


$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlerboitelogin_niv1, "boite-login");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> Parse ($l_i_handlersquelette, "menu-left.varmenu-left", $l_i_handlermenuadmin_niv1, "menu-admin");
$l_o_template -> closeSession ($l_i_handlersquelette, "menu-left");
$l_o_template -> addSession ($l_i_handlersquelette, "pagecourante2");
$l_o_template -> setVar ($l_i_handlersquelette, "pagecourante2.varpagecourante2", "Administration");
$l_o_template -> closeSession ($l_i_handlersquelette, "pagecourante2");


/* Parsage final de la page */
$l_o_template -> Display ();

/* D�connexion de la Base de Donn�es */
$l_i_retour = fermebdd ();
if (!$l_i_retour)
{
	trigger_error ("Echec de la fermeture de la connexion la base de donn�es du site",  NOTICE);
}

/* Envoi de la page au client */
ob_end_flush ();
/* Fin de fichier ***********************************************************************************/
?>

