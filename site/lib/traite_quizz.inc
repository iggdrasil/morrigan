<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : traite_quizz.inc
* Fonction du fichier : génèration et correction du quizz
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création : 11/03/2008
* Version actuelle : 1.0 au 11/03/2008
* License du projet : gpl
* Dernières modifications :
* Remarques particulières :
*
*****************************************************************************************************/

function genere_quizz (&$l_t_questions, &$l_t_reponses)
/****************************************************************************************************
*
*   FONCTION genere_quizz
*          Génère le quizz
*
*   PARAMETRES : tableaux de questions et de réponses vides
*   RENVOIE : rien, les tableaux à construire sont passés par référence
* ******************************************************************************************************/
{
	/* Définition des variables locales à la fonction */
	$l_t_st_resultat = array (0, array()); // contient les résultats des requetes diverses
	$l_t_tableintermediaire = array();
	$l_i_compteur = 0;
	$l_i_compteur2 = 0;
	$l_i_compteur3 = 0;

	/* Corps de la fonction */	 	
	// Prendre un nombre de questions au hasard
	// 1) Vérifier qu'on peut récupérer le nombre de questions demandé
	$l_t_st_resultat = requete ("nb_question_max");
	
	if ($l_t_st_resultat [1][0][0] < NOMBREQUESTIONS)
	{
		// Trop de questions à récupérer
		trigger_error ("Il y a trop de questions à poser par rapport au nombre de questions disponibles.", FATAL);
		exit ();
	}
	
	// 2) Aller chercher les questions en base	
	$l_t_st_resultat = requete ("recup_questions");
	if ($l_t_st_resultat [0] < NOMBREQUESTIONS || $l_t_st_resultat [0] == NULL)
	{
		trigger_error ("Pas suffisamment de questions rapatriées pour ce quizz", FATAL);
		exit ();
	}
	
	for ($l_i_compteur = 0; $l_i_compteur < NOMBREQUESTIONS; $l_i_compteur ++)
	{
		$l_t_questions [$l_i_compteur][0] =  (int) $l_t_st_resultat [1][$l_i_compteur][0];
		$l_t_questions [$l_i_compteur][1] = (string) $l_t_st_resultat [1][$l_i_compteur][1];		

		$l_t_tableintermediaire [$l_i_compteur] = $l_t_questions [$l_i_compteur][0];
	}
	
	// Enregistrement des ids de questions	
	$_SESSION ['idquestions'] = $l_t_tableintermediaire;
	
	// Réinitialisation table intermédiaire
	$l_t_tableintermediaire = array ();
	
	// Prendre les réponses associées aux questions prises au-dessus
	for ($l_i_compteur = 0; $l_i_compteur < NOMBREQUESTIONS; $l_i_compteur ++)
	{
		$l_t_st_resultat = requete ("recup_reponses", $l_t_questions [$l_i_compteur][0]);		
		if ($l_t_st_resultat [0] <= 1 || $l_t_st_resultat [0] == NULL)
		{
			trigger_error ("Pas assez de réponses à la question '".$l_t_questions [$l_i_compteur]['id']."' pour être correctement récupérées", FATAL);
			exit ();
		}
		for ($l_i_compteur2 = 0; $l_i_compteur2 < $l_t_st_resultat [0]; $l_i_compteur2 ++)
		{
			$l_t_reponses [$l_i_compteur][$l_i_compteur2][0] = $l_t_st_resultat [1][$l_i_compteur2][0];
			$l_t_reponses [$l_i_compteur][$l_i_compteur2][1] = $l_t_st_resultat [1][$l_i_compteur2][1];
		
			$l_t_tableintermediaire [$l_i_compteur3] = $l_t_reponses [$l_i_compteur][$l_i_compteur2][0];
			$l_i_compteur3++;
		}
	}
	
	// Enregistrement des ids des réponses
	$_SESSION ['idreponses'] = $l_t_tableintermediaire;
   
}

function corrige_quizz ($l_st_idquestions, $l_st_idreponses)
/****************************************************************************************************
*
*   FONCTION corrige_quizz
*          corrige le quizz et vérifie la cohérence des questions et des réponses données
*
*   PARAMETRES : tableaux 
* 					- contenant les id des questions posées à la page précédente
* 					- contenant les id des réponses cochées à la page précédente
*   RENVOIE : résultat de la correction :
* 				OUI : quizz OK
* 				NON : quizz NOK
* 				ERREUR : tentative de triche sur le quizz
* ******************************************************************************************************/
{
	/* Définition des variables locales à la fonction */
	$l_i_score  = 0; // score brut cumulé
	$l_i_scoremax = 0; // score max atteignable pour ce questionnaire
	$l_i_scorequestion = 0; // score intermédiaire, question par question
	$l_i_flagidquestion = 0; // contient l'id de la question qu'on corrige
	$l_i_flag_reponsefausse = NON;  // Permet de savoir si une réponse précédente à la question courante était fausse
	$l_st_resultat = array (); // pour les réponses des requêtes
	$l_i_resultat = NON;
	$l_s_idquestions = "";
	$l_i_compteur = 0;
	$l_i_compteur2  = 0;
	$l_s_idquestions = ""; // buffer pour regrouper les ids des questions et permettre de construire la requete
	$l_t_tableintermediaire = array (); // buffer pour les ids des questions 
	global $g_st_idreponses;
	    
	/* Corps de la fonction */	
	// Vérification de l'existence des questions remontées comme étant posées
	if (!isset ($_SESSION ['idquestions']))
	{
		trigger_error ("La variable de session idquestions dans le quizz de '".$_SESSION ['id']."' n'est pas définie", FATAL);
		exit ();
	}	
	
	// Rechercher les réponses à ces questions
	if (!isset ($_SESSION ['idreponses']))
	{
		trigger_error ("La variable de session idreponses dans le quizz de '".$_SESSION ['id']."' n'est pas définie", FATAL);
		exit ();
	}
	
	
	$l_t_tableintermediaire = $_SESSION ['idquestions'];
	for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_tableintermediaire); $l_i_compteur ++)
	{
		if ($l_i_compteur == 0)
		{
			$l_s_idquestions = "REP_fk_QUE = '".$l_t_tableintermediaire [$l_i_compteur]."' ";
		}
		else
		{
			$l_s_idquestions = $l_s_idquestions . "OR REP_fk_QUE = '".$l_t_tableintermediaire [$l_i_compteur]."' ";
		}	
	}
	
	// Récupération des réponses au quizz depuis la BdD
	$l_st_resultat = requete ("reponsesquizz", $l_s_idquestions);	
	if ($l_st_resultat [0] > (NOMBREREPONSES * count ($_SESSION ['idquestions'])))
	{
		trigger_error ("Le nombre de réponses au questionnaire passé par '".$_SESSION ['id']."' est faux", WARNING);
		return ERREUR;
	}
	
	// Calcul du score maximum atteignable (1 point par case cochée bonne)
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultat [0]; $l_i_compteur ++)
	{
		$l_i_scoremax = $l_i_scoremax + $l_st_resultat [1][$l_i_compteur][1];
	}
	
	// Calcul du score
	// Nettoyage des ids de réponses
	if (is_array ($g_st_idreponses [CONTENU_VAR]))
	{
		// Si le candidat ne répond à aucune question, idreponses n'est pas un tableau		
		array_unique ($g_st_idreponses [CONTENU_VAR]);
	}
	// Calcul proprement dit, 1 point par bonne réponse donnée, 0 si une seule réponse fausse à une question
	for ($l_i_compteur = 0; $l_i_compteur < $l_st_resultat [0]; $l_i_compteur ++)
	{
		// Initialisation des flags et scores de la boucle
		if ($l_i_compteur == 0)
		{
			$l_i_flagidquestion = $l_st_resultat [1][$l_i_compteur][2];
			$l_i_scorequestion = 0;
			$l_i_flag_reponsefausse = NON;
		}
		
		// Si on change de question
		if ($l_i_flagidquestion != $l_st_resultat [1][$l_i_compteur][2])
		{
			// Versement du résultat de la question courant dans le résultat global
			$l_i_score = $l_i_score + $l_i_scorequestion;
			// Changement du flag d'id de question
			$l_i_flagidquestion = $l_st_resultat [1][$l_i_compteur][2];
			// réinitialisation du score de la question courante 
			$l_i_scorequestion = 0;
			$l_i_flag_reponsefausse = NON;
		}
		
		// Réponse bonne? Faut-il sauter cette itération parce qu'une réponse précédente à cette question était fausse?
		// On parcourt le tableau des réponses données pour chercher si une réponse correspond
		for ($l_i_compteur2 = 0; $l_i_compteur2 < count ($g_st_idreponses [CONTENU_VAR]); $l_i_compteur2 ++)
		{
			if (($l_i_flag_reponsefausse == NON) && ($g_st_idreponses [CONTENU_VAR][$l_i_compteur2] == $l_st_resultat [1][$l_i_compteur][0]) && ($l_st_resultat [1][$l_i_compteur][1] == OUI))
			{
				// La réponse est bonne, on incrémente le score de la question courante
				$l_i_scorequestion = $l_i_scorequestion + 1;
			}
			elseif (($l_i_flag_reponsefausse == NON) && ($g_st_idreponses [CONTENU_VAR][$l_i_compteur2] == $l_st_resultat [1][$l_i_compteur][0]) && ($l_st_resultat [1][$l_i_compteur][1] == NON))
			{
				// La réponse est fausse, on lève le drapeau réponse fausse
				$l_i_flag_reponsefausse = OUI;
				// On annule le score à la question courante
				$l_i_scorequestion = 0;
			}
			else
			{
				// La réponse n'a pas été trouvée parmi les réponses soumises par le candidat
			}	
		}
	}
	// Ajout du score de la dernière question au score total
	$l_i_score = $l_i_score + $l_i_scorequestion;
	
	// Trop de points par rapport au nombre de points qu'on peut obtenir
	if ($l_i_score > $l_i_scoremax)
	{
		return ERREUR;
	}
	
	// Détermination en pourcentage du score obtenu
	$l_i_score = ($l_i_score / $l_i_scoremax) * 100;
	
	/*print ("Score au quizz en pourcent : ".$l_i_score."%<br>");*/

	// Désamorçage du quizz
	$_SESSION ['idquestions'] = "";
	$_SESSION ['idreponses'] = "";
	$_SESSION ['graindeselquizz'] = "";	
	
	// Détermination de la qualification du candidat
	if ($l_i_score < SCOREOK)
	{
		return NON;
	}
	elseif ($l_i_score >= SCOREOK)
	{
		return OUI;
	}
	else
	{
		return ERREUR;
	}
}
/* Fin de fichier **************************************************************************************/
?>
