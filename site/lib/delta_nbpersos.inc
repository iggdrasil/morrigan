<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : delta_nbpersos.inc
* Fonction du fichier : gestion des variations du champ nbpersos pour un profil donné
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création : 11/03/2008
* Version actuelle : 1.0 au 11/03/2008
* License du projet : gpl
* Dernières modifications :
* Remarques particulières :
*
*****************************************************************************************************/


function delta_nbpersos ($l_i_statut_arrivee, $l_i_idcand, $l_i_idprofil)
{
/****************************************************************************************************
*
*   FONCTION delta_nbpersos
*          Donne la variation à appliquer au cahmp nbpersos du profil associé à la candidature traitée
*
*   PARAMETRES : 
* 				- $l_i_statut_arrivee : statut que l'on veut donner à la candidature
* 				- $l_i_idcand : id de la candidature à traiter (NULL si création de candidature, on se rabat sur id profil
*  				- $l_i_idprofil : id du profil à traiter (NULL si idcand passé en paramètre)
*   RENVOIE : un entier prenant les valeurs suivantes :
* 				- ERREUR : nbpersos < -1 : champ avec valeur fausse
* 				- REFUS : nbpersos déjà égal à 0, on ne peut plus le décrémenter
* 				- AUCUN : pas de variation du champ
* 				- PLUS : champ + 1
* 				- MOINS : champ - 1
* 		
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
	$l_i_compteur = 0;
	$l_t_tableconstruite = array ();
	$l_t_resultat = array ();

	// Tableau récapitulant les variations possibles de nbpersos
	$l_t_tableauvarnbpersos = array (array (NON, RELECTURE, POSITIF, MOINS),
							  		 array (NON, RELECTURE, NUL, REFUS),

							  		 array (EDITION, ATTENTE, POSITIF, PLUS),
							  		 array (EDITION, ATTENTE, NUL, PLUS),
							  		 array (EDITION, ABANDONNEE, POSITIF, PLUS),
							  		 array (EDITION, ABANDONNEE, NUL, PLUS),
							  		 array (EDITION, REFUSEE, POSITIF, PLUS),												  		array (EDITION, REFUSEE, NUL, PLUS),
									
							  		 array (RELECTURE, ATTENTE, POSITIF, PLUS),
							  		 array (RELECTURE, ATTENTE, NUL, PLUS),
							  		 array (RELECTURE, ABANDONNEE, POSITIF, PLUS),
							  		 array (RELECTURE, ABANDONNEE, NUL, PLUS),
							  		 array (RELECTURE, REFUSEE, POSITIF, PLUS),
							  		 array (RELECTURE, REFUSEE, NUL, PLUS),

							  		 array (AVIS, ATTENTE, POSITIF, PLUS),
							  		 array (AVIS, ATTENTE, NUL, PLUS),
							  		 array (AVIS, ABANDONNEE, POSITIF, PLUS),
							  		 array (AVIS, ABANDONNEE, NUL, PLUS),
							  		 array (AVIS, REFUSEE, POSITIF, PLUS),
							  		 array (AVIS, REFUSEE, NUL, PLUS),

							  		 array (ATTENTE, EDITION, POSITIF, MOINS),
							  		 array (ATTENTE, EDITION, NUL, REFUS),
							  		 array (ATTENTE, RELECTURE, POSITIF, MOINS),
							  		 array (ATTENTE, RELECTURE, NUL, REFUS),
 							  		 array (ATTENTE, AVIS, POSITIF, MOINS),
							  		 array (ATTENTE, AVIS, NUL, REFUS),
							  		 array (ATTENTE, ACCEPTEE, POSITIF, MOINS),
							  		 array (ATTENTE, ACCEPTEE, NUL, REFUS),

							  		 array (ABANDONNEE, EDITION, POSITIF, MOINS),
							  		 array (ABANDONNEE, EDITION, NUL, REFUS),
							  		 array (ABANDONNEE, RELECTURE, POSITIF, MOINS),
							  		 array (ABANDONNEE, RELECTURE, NUL, REFUS),
 							  		 array (ABANDONNEE, AVIS, POSITIF, MOINS),
							  		 array (ABANDONNEE, AVIS, NUL, REFUS),
							  		 array (ABANDONNEE, ACCEPTEE, POSITIF, MOINS),
							  		 array (ABANDONNEE, ACCEPTEE, NUL, REFUS),							  		 

							  		 array (REFUSEE, EDITION, POSITIF, MOINS),
							  		 array (REFUSEE, EDITION, NUL, REFUS),
							  		 array (REFUSEE, RELECTURE, POSITIF, MOINS),
							  		 array (REFUSEE, RELECTURE, NUL, REFUS),
 							  		 array (REFUSEE, AVIS, POSITIF, MOINS),
							  		 array (REFUSEE, AVIS, NUL, REFUS),
							  		 array (REFUSEE, ACCEPTEE, POSITIF, MOINS),
							  		 array (REFUSEE, ACCEPTEE, NUL, REFUS)								  		 
							  		 );	
	/* Corps de la fonction */
	// Requete de récupération des informations nécessaires : statut de départ, nbpersos actuel
	if ($l_i_idcand == NULL)
	{
		// la candidature n'existe pas encore, on ne se repose que sur nbpersos du profil
		$l_t_st_resultat = requete ("delta_nbpersos_creation", $l_i_idprofil);
		if ($l_t_st_resultat [0] == 0)
		{
			trigger_error ("Echec de la requête delta_nbpersos_creation", FATAL);
			exit ();		
		}


		if ($l_t_st_resultat [1][0][0] < -1)
		{
			// Situation normalement impossible
			return (ERREUR);
		}
		elseif ($l_t_st_resultat [1][0][0] == -1)		
		{
			// nbpersos infini, pas de variation
			return (AUCUN);
		}
		else
		{
			($l_t_st_resultat [1][0][0] == 0) ? ($l_t_st_resultat [1][0][0] = NUL) : ($l_t_st_resultat [1][0][0] = POSITIF);
			// Construction de la table à comparer aux cas de test
			$l_t_tableconstruite = array (NON, $l_i_statut_arrivee, $l_t_st_resultat [1][0][0]);		
		}
	}
	else
	{
		// La candidature existe déjà, on récupère son statut et nbpersos du profil
		$l_t_st_resultat = requete ("delta_nbpersos", $l_i_idcand);
		if ($l_t_st_resultat [0] == 0)
		{
			trigger_error ("Echec de la requête delta_nbpersos pour la candidature '".$l_i_idcand."'.", FATAL);
			exit ();
		}
		
		if ($l_t_st_resultat [1][0][1] < -1)
		{
			// Situation normalement impossible
			return (REFUS);
		}
		elseif ($l_t_st_resultat [1][0][1] == -1)		
		{
			// nbpersos infini, pas de variation
			return (AUCUN);
		}
		else
		{
			if ($l_t_st_resultat [1][0][1] == 0)
			{
				$l_t_st_resultat [1][0][1] = NUL;

			}
			else
			{
				$l_t_st_resultat [1][0][1] =  POSITIF;
			}

			// Construction de la table à comparer aux cas de test
			// [statut_départ, statut_arrivée, nb_persos]
			$l_t_tableconstruite = array ((int) $l_t_st_resultat [1][0][0], $l_i_statut_arrivee, $l_t_st_resultat [1][0][1]);		

			var_dump ($l_t_tableconstruite);
			print ("<br/>");
		}
	}		

	// Pour chaque ligne du tableau de cas de test, on compare avec notre situation. 
	// S'il y a correspondance, on renvoie la dernière valeur de la ligne testée, qui donne la variation à appliquer
	for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_tableauvarnbpersos); $l_i_compteur++)
	{
		$l_t_tableextraite = array_slice ($l_t_tableauvarnbpersos [$l_i_compteur], 0, 3);
		
		var_dump ($l_t_tableextraite);
		print ("<br/>");

		$l_t_resultat = array_diff ($l_t_tableconstruite, $l_t_tableextraite);
		
		if ($l_t_resultat == NULL)
		{
			print ("'".$l_t_tableauvarnbpersos [$l_i_compteur][3]."'<br/>");
			return ($l_t_tableauvarnbpersos [$l_i_compteur][3]);
		}
	}
	
	// Table parcourue en entier, pas de correspondance : par défaut, il n'y a pas d'évolution de nbpersos
	return (AUCUN);
}

function modifie_nbpersos ($l_i_idprofil, $l_i_delta)
{
/****************************************************************************************************
*
*   FONCTION modifie_nbpersos
*          Appliquer la variation du champ nbpersos du profil associé à la candidature traitée
*
*   PARAMETRES : 
*  				- $l_i_idprofil : id du profil à traiter
* 				- $l_i_delta : variation à appliquer  
* RENVOIE : un booléen de succès (OUI/NON)
* 		
*****************************************************************************************************/
    
    /* Définition des variables locales à la fonction */
	$l_i_compteur = 0;
	$l_t_tableconstruite = array ();
	$l_t_resultat = array ();
	
	/* Corps de la fonction */
	$l_t_st_resultat = requete ("modifie_nbpersos", $l_i_idprofil, $l_i_delta);
	if ($l_t_st_resultat ["nbaffecte"] == 0)
	{
		trigger_error ("Problème lors de la modification du nombre de personnages du profil '".$l_i_idprofil."'.", WARNING);
		return NON;
	}
	elseif ($l_t_st_resultat ["nbaffecte"] == 1) 
	{
		return OUI;
	}
	else
	{
		return NON;
	}		
}
/* Fin de fichier **************************************************************************************/
?>
