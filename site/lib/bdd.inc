<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier : bdd.inc
* Fonction du fichier : gestion de la base de données
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création : 04/04/2004
* Version actuelle : 1.0 au 04/04/2004
* License du projet : gpl
* Dernières modifications :
* Remarques particulières :	TODO : controler la requete droitdepotcand
*
*****************************************************************************************************/


function ouvrebdd ()
{
/****************************************************************************************************
*
*   FONCTION ouvrebdd
*          Effectue la connexion à la base de données du site
*
*   PARAMETRES : rien
*   RENVOIE : $l_i_lien : entier descripteur pointant vers la connexion
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    $l_i_lien = 0; // valeur de retour de la fonction

      /* Corps de la fonction */
    $l_i_lien = mysql_connect (HOSTBDD, LOGBDD, PASSBDD); // connexion

    mysql_select_db (NOMBDD); // choix de la BDD de travail

    return $l_i_lien;
}

function fermebdd ()
{
/****************************************************************************************************
*
*   FONCTION  fermebdd
*          Clot la connexion avec la BdD
*
*   PARAMETRES : rien, mais utilise $l_i_lien comme variable globale
*   RENVOIE :   $l_i_retour, booléen permettant de savoir si la fermeture a bien eu lieu
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
   global $g_i_lien;
   $l_i_retour = 0; // valeur de retour de la fonction

   /* Corps de la fonction */
   $l_i_retour = mysql_close ($g_i_lien); // fermeture du la bdd

    return $l_i_retour;
}

function requete ()
{
/****************************************************************************************************
*
*   FONCTION  requete
*          effectue la requete demandée et formate le resulat pour être retourné
*
*
*   PARAMETRES : - nom de la requête à effectuer
                 - nombre d'arguments fonction de la requête à effectuer
*   RENVOIE :    - s'il n'y a pas de réponse à une requête select : un tableau de la forme (0, array(vide))
*                - s'il y a UNE ou PLUSIEURS réponses : un tableau de la forme (nb_réponses, (réponse1(champ1, champ2, ...), réponse2(),...)
*                - s'il n'y a pas de réponses, le nombre de lignes impactées par la requete (pour des requetes autres que des SELECT
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    global $g_i_lien; // descripteur de la connexion à la bdd
    $l_st_arguments = array (); // liste des arguments de la fonction
    $l_st_resultat = array (); // tableau de retour du résultat de la requête
    $l_s_requete = ""; // corps de la requete
    $l_i_type = 0; // type de requete : 0 pour un SELECT, 1 pour le reste (détermine si on doit parcourir les résultats)
    $l_i_resultat = 0; // descripteur du résultat de la requête
    $l_i_nbenregistrements = 0; // nombre de lignes d'enregistrement du résultat de la requête
    $l_i_nbaffecte = 0; // nombre de lignes affectées dans la bdd (pour les requêtes du type UPDATE, INSERT, DELETE)
    $l_st_tableresultat = array (); // tableau représentant le résultat de la requête
    $l_i_index = 0; // index de parcours de la boucle for pour construire le tabeau $l_st_resultat
    $l_i_dernier_id_affecte = 0; // dernier id affecte dans la bdd /!\ à ne pas utiliser pour un UPDATE
    global $g_i_numeroprofil; // sert pour certaines variables
    global $g_i_numerocandidature; // sert pour certaines variables
	global $g_i_idcand; // idem  /!\ TODO : faire en sorte qu'il n'y ait qu'une seule variable pour toutes les idcands
	global $g_i_numeroquestion; // idem

    /* Corps de la fonction */
    $l_st_arguments = func_get_args (); // récupération des arguments de la fonction;
	//print ($l_st_arguments [0]."<br>");

    // Détermination de la requête à exécuter et construction de la requete en question
    switch ($l_st_arguments [0])
    {
        case "login_user":
            $l_s_requete = "SELECT * FROM t_user WHERE USR_login='".$l_st_arguments [1]."' AND USR_password='".$l_st_arguments[2]."'";
            $l_i_type = 0;
            break;
        case "testexistencelogue":
            $l_s_requete="SELECT USR_id FROM t_user WHERE USR_login='".$l_st_arguments [1]."' AND USR_password='".$l_st_arguments[2]."'";
            $l_i_type = 0;
            break;
        case "importedroits":
            $l_s_requete = "SELECT USR_droits_news, USR_droits_all_news, USR_droits_inscription, USR_droits_recrutement, USR_droits_edit_regles, USR_droits_edit_monde, USR_droits_edit_admin FROM t_user WHERE USR_login='".$l_st_arguments[1]."'";
            $l_i_type = 0;
            break;
        case "doublon_mail":
           $l_s_requete= "SELECT USR_id FROM t_user WHERE USR_email='".$l_st_arguments [1]."'";
           $l_i_type = 0;
           break;
        case "doublon_texte":
            $l_s_requete = "SELECT ".$l_st_arguments [5]." FROM ".$l_st_arguments [4]." WHERE ".$l_st_arguments [3]."='".$l_st_arguments [1]."' AND ".$l_st_arguments [5]."<> '".$l_st_arguments [6]."'";
            $l_i_type = 0;
            break;

        case "infos_profil_base":
            $l_s_requete = "SELECT t1.PRO_id, (SELECT t2.USR_login FROM t_user as t2 WHERE t2.USR_id=t1.PRO_fk_USR), t1.PRO_statut, (SELECT t3.CAT_nom FROM t_categorie AS t3 WHERE t3.CAT_id = t1.PRO_fk_CAT), t1.PRO_nom, t1.PRO_nbpersos, t1.PRO_demandeaction, '1' FROM t_profil as t1 ORDER BY ".$l_st_arguments [1]."";
            $l_i_type = 0;
            break;

        case "infos_profil":
            $l_s_requete = "SELECT t1.PRO_id, t1.PRO_nom, t1.PRO_statut, (SELECT t3.CAT_nom FROM t_categorie AS t3 WHERE t3.CAT_id = t1.PRO_fk_CAT), t1.PRO_desc, t1.PRO_debat, t1.PRO_nbpersos, t1.PRO_historique, t1.PRO_demandeaction, t2.USR_login, t1.PRO_fk_CAT, t1.PRO_fk_USR FROM t_profil AS t1, t_user AS t2 WHERE (PRO_id = '".$l_st_arguments [1]."') AND PRO_fk_USR = USR_id";
            $l_i_type = 0;
            break;

        case "existe_profil":
                $l_s_requete = "SELECT PRO_id, PRO_fk_USR, PRO_statut, PRO_nom FROM t_profil WHERE PRO_id='".$l_st_arguments [1]."'";
                $l_i_type = 0;
                break;

        case "racespossiblesed2":
        // hack pas propre pour forcer le numéro de profil avec un argument
            $g_i_numeroprofil = $l_st_arguments [1];
        case "racespossiblesed":
            $l_s_requete = "SELECT RAC_id, CONCAT(IF(RAC_insc_ouverte, '#', 'X'), ' - ', RAC_nom), RPO_id FROM t_race LEFT OUTER JOIN t_racespossibles ON RPO_fk_RAC = RAC_id AND RPO_fk_PRO = ".$g_i_numeroprofil.""; // was ... WHERE  RAC_insc_ouverte = 1
            $l_i_type = 0;
            break;

        case "classespossiblesed2":
        // hack pas propre pour forcer le numéro de profil avec un argument
            $g_i_numeroprofil = $l_st_arguments [1];
        case "classespossiblesed":
            $l_s_requete = "SELECT CLA_id, CONCAT(IF(CLA_insc_ouverte, '#', 'X'), ' - ', CLA_nom), CPO_id FROM t_classe LEFT OUTER JOIN t_classespossibles ON CPO_fk_CLA = CLA_id AND CPO_fk_PRO = ".$g_i_numeroprofil.""; // was ... WHERE CLA_insc_ouverte = 1
            $l_i_type = 0;
            break;

        case "sexespossiblesed2":
        // hack pas propre pour forcer le numéro de profil avec un argument
            $g_i_numeroprofil = $l_st_arguments [1];
        case "sexespossiblesed":
            $l_s_requete = "SELECT SEX_id, CONCAT(IF(SEX_insc_ouverte,'#', 'X'), ' - ', SEX_nom) , SPO_id FROM t_sexe LEFT OUTER JOIN t_sexespossibles ON SPO_fk_SEX = SEX_id AND SPO_fk_PRO = ".$g_i_numeroprofil.""; //  was ... WHERE SEX_insc_ouverte = 1
            $l_i_type = 0;
            break;

        case "ajoute_champ_race":
            $l_s_requete = "INSERT INTO t_racespossibles SET RPO_fk_PRO = '".$l_st_arguments [1]."', RPO_fk_RAC = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "ajoute_champ_classe":
            $l_s_requete = "INSERT INTO t_classespossibles SET CPO_fk_PRO = '".$l_st_arguments [1]."', CPO_fk_CLA = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "ajoute_champ_sexe":
            $l_s_requete = "INSERT INTO t_sexespossibles SET SPO_fk_PRO = '".$l_st_arguments [1]."', SPO_fk_SEX = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "commente_profil":
            // Requête particulièrement tordue : on ajoute à la fin de l'enregistrement
            // une nouvelle date, l'auteur du commentaire, puis le texte du commentaire
            $l_s_requete = "UPDATE t_profil SET PRO_debat = concat(PRO_debat, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:</span><br> ".$l_st_arguments [1]."<br>') WHERE PRO_id = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "enleve_champ_race":
            $l_s_requete = "DELETE FROM t_racespossibles WHERE RPO_fk_PRO = '".$l_st_arguments [1]."' AND RPO_fk_RAC = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "enleve_champ_classe":
            $l_s_requete = "DELETE FROM t_classespossibles WHERE CPO_fk_PRO = '".$l_st_arguments [1]."' AND CPO_fk_CLA = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "enleve_champ_sexe":
            $l_s_requete = "DELETE FROM t_sexespossibles WHERE SPO_fk_PRO = '".$l_st_arguments [1]."' AND SPO_fk_SEX = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;

        case "corrige_profil":
            $l_s_requete = "UPDATE t_profil SET PRO_nom = '".$l_st_arguments [1]."', PRO_fk_CAT = '".$l_st_arguments [2]."', PRO_statut = '".$l_st_arguments [3]."', PRO_fk_USR = '".$l_st_arguments [4]."', PRO_desc = '".$l_st_arguments [5]."', PRO_nbpersos = '".$l_st_arguments [6]."' WHERE PRO_id = '".$l_st_arguments [7]."'";
            $l_i_type = 1;
            break;

        case "change_statut_profil":
            $l_s_requete = "UPDATE t_profil SET PRO_statut = '".$l_st_arguments [2]."' WHERE PRO_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;
            break;

        case "efface_profil":
            $l_s_requete = "DELETE FROM t_profil WHERE PRO_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;
            break;

        case "pas_candidat":
            $l_s_requete = "SELECT CAN_id FROM t_candidatures WHERE CAN_fk_PRO = '".$l_st_arguments [1]."' AND CAN_statut <> '".ACCEPTEE."'";
            $l_i_type = 0;
            break;
            
        case "temp_statuts_profils":
            $l_s_requete = "CREATE TEMPORARY TABLE t_statutprofil 
            (STP_id tinyint(4) unsigned NOT NULL ,
            STP_texte varchar(30) NOT NULL,
            PRIMARY KEY  (`STP_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            $l_i_type = 1;
            break;   
            
        case "insert_statuts_profils":
            $l_s_requete = "INSERT INTO t_statutprofil (STP_id, STP_texte) VALUES (0, '".EDITIONTXT."'), (1, '".RELECTURETXT."'), (2, '".HORSLIGNETXT."'), (3, '".ENLIGNETXT."')";
            $l_i_type = 1;
            break;
            
        case "auteurprofil":
            $l_s_requete = "SELECT t2.USR_id, t2.USR_login, t1.PRO_fk_USR FROM t_user AS t2 LEFT OUTER JOIN t_profil AS t1 ON t1.PRO_fk_USR = t2.USR_id AND PRO_id='".$g_i_numeroprofil."'";
            $l_i_type = 0;           
            break;
             
        case "statutprofil":
            $l_s_requete = "SELECT t2.STP_id, t2.STP_texte, t1.PRO_STATUT FROM t_statutprofil AS t2 LEFT OUTER JOIN t_profil AS t1 ON t1.PRO_statut = t2.STP_id AND PRO_id='".$g_i_numeroprofil."'";
            $l_i_type = 0;           
            break;

        case "categorie":
            $l_s_requete = "SELECT t2.CAT_id, t2.CAT_nom, t1.PRO_fk_CAT FROM t_categorie AS t2 LEFT OUTER JOIN t_profil AS t1 ON t1.PRO_fk_CAT = t2.CAT_id AND PRO_id='".$g_i_numeroprofil."'";
            $l_i_type = 0;           
            break;
            
        case "ajout_historique_profil":
            // Requête particulièrement tordue : on ajoute à la fin de l'enregistrement
            // une nouvelle date, l'auteur dde l'action, puis le nom de l'action
            $l_s_requete = "UPDATE t_profil SET PRO_historique = concat(PRO_historique, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:&nbsp;</span> ".$l_st_arguments [1]."<br>') WHERE PRO_id = '".$l_st_arguments [2]."'";
            $l_i_type = 1;
            break;
        
        case "creer_profil":
        	// Paramètres : Nom du profil, statut du profil, catégorie du profil, auteur du profil, description du profil,
			// nombre de personnages possibles
            $l_s_requete = "INSERT INTO t_profil (PRO_nom, PRO_statut, PRO_fk_CAT, PRO_fk_USR, PRO_desc, PRO_nbpersos) VALUES ('".$l_st_arguments [1]."', '".$l_st_arguments [2]."', '".$l_st_arguments [3]."', '".$l_st_arguments [4]."', '".$l_st_arguments [5]."', '".$l_st_arguments [6]."')";
            $l_i_type = 1;
            break;
 
         case "demande_profil":
        	// Paramètres : Nom du profil, statut du profil, catégorie du profil, auteur du profil, description du profil,
			// nombre de personnages possibles
            $l_s_requete = "UPDATE t_profil SET PRO_demandeaction = '".$l_st_arguments [2]."', PRO_historique = concat(PRO_historique, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:&nbsp;</span> ".$l_st_arguments [3]."<br>') WHERE PRO_id = '".$l_st_arguments [1]."' ";
            $l_i_type = 1;
            break;   
            
        case "recupere_statut_profil":      
			$l_s_requete = "SELECT PRO_statut, PRO_demandeaction FROM t_profil WHERE PRO_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
            break;
            
        case "refuse_action":
			$l_s_requete = "UPDATE t_profil SET PRO_demandeaction = NULL WHERE PRO_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
            break;  
            
        case "infos_candidature_base":      
			$l_s_requete = "SELECT t1.CAN_id, t2.USR_login, t3.PRO_nom, t1.CAN_nomperso, t1.CAN_datemodification, t1.CAN_statut FROM t_candidatures AS t1, t_user AS t2, t_profil AS t3 WHERE (t1.CAN_fk_USR = t2.USR_id AND t1.CAN_fk_PRO = t3.PRO_id) ORDER BY t3.PRO_nom";
			$l_i_type = 0;
            break; 
            
        case "temp_statuts_candidatures":
            $l_s_requete = "CREATE TEMPORARY TABLE t_statutcandidature 
            (STC_id tinyint(4) unsigned NOT NULL ,
            STC_texte varchar(30) NOT NULL,
            PRIMARY KEY  (`STC_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            $l_i_type = 1;
            break;
            
        case "insert_statuts_candidatures":
            $l_s_requete = "INSERT INTO t_statutcandidature (STC_id, STC_texte) VALUES (0, '".EDITIONUSERTXT."'), (1, '".RELECTUREMJTXT."'), (2, '".AVISMJTXT."'), (3, '".ENATTENTETXT."'), (4, '".ABANDONNEETXT."'), (5, '".ACCEPTEETXT."'), (6, '".REFUSEETXT."')";
            $l_i_type = 1;
            break;

        case "existe_candidature":
            $l_s_requete = "SELECT CAN_id FROM t_candidatures WHERE CAN_id='".$l_st_arguments [1]."'";
            $l_i_type = 0;
            break;
        
        case "infos_candidature":
            $l_s_requete = "SELECT t1.CAN_id, t2.USR_login, t3.PRO_nom,
            				t4.USR_login, t1.CAN_statut, t1.CAN_nomperso, t5.RAC_nom, t5.RAC_insc_ouverte,
            				t6.CLA_nom, t6.CLA_insc_ouverte, t7.SEX_nom, t7.SEX_insc_ouverte,
            				t1.CAN_descriptionperso, t1.CAN_backgroundperso, t1.CAN_butsperso,
            				t1.CAN_debatusrmj, t1.CAN_debatmj, t1.CAN_historique, t1.CAN_commentaire, t1.CAN_datemodification, t3.PRO_desc
            				FROM
            				t_candidatures AS t1, t_user AS t2, t_profil AS t3, t_user AS t4, t_race AS t5, t_classe AS t6, t_sexe AS t7
            				WHERE
            					t1.CAN_id = '".$l_st_arguments [1]."' 
            				AND
            					t1.CAN_fk_USR = t2.USR_id 
            				AND
            					t1.CAN_fk_PRO = t3.PRO_id
            				AND
            					t1.CAN_fk_RAC = t5.RAC_id
            				AND
            					t1.CAN_fk_CLA = t6.CLA_id
            				AND
            					t1.CAN_fk_SEX = t7.SEX_id
            				AND
            					t3.PRO_fk_USR = t4.USR_id";
            $l_i_type = 0;
            break;
        
        case "statutcandidature":   
            $l_s_requete = "SELECT t2.STC_id, t2.STC_texte, t1.CAN_STATUT FROM t_statutcandidature AS t2 LEFT OUTER JOIN t_candidatures AS t1 ON t1.CAN_statut = t2.STC_id AND CAN_id='".$g_i_numerocandidature."'";
            $l_i_type = 0;        
			break;
			
		case "changestatut_candidature":
            $l_s_requete = "UPDATE t_candidatures SET CAN_statut = '".$l_st_arguments [2]."' WHERE CAN_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;        
			break;
			
		case "commentaire_global":		
            $l_s_requete = "UPDATE t_candidatures SET CAN_commentaire = concat(CAN_commentaire, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:</span><br> ".$l_st_arguments [2]."<br>') WHERE CAN_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;        
			break;

		case "commentaire_pj_candidature":		
            $l_s_requete = "UPDATE t_candidatures AS t1, t_profil AS t2
            			    SET t1.CAN_debatusrmj = concat(t1.CAN_debatusrmj, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:</span><br> ".$l_st_arguments [2]."<br>'),
            			   		t1.CAN_statut = '".$l_st_arguments [3]."',
            			   		t2.PRO_nbpersos = t2.PRO_nbpersos + ".$l_st_arguments [4]."
            			    WHERE t1.CAN_id = '".$l_st_arguments [1]."' AND t1.CAN_fk_PRO = t2.PRO_id";
            $l_i_type = 1;        
			break;
			
		case "commentaire_mj_candidature":		
            $l_s_requete = "UPDATE t_candidatures AS t1, t_profil AS t2 
            				SET t1.CAN_debatmj = concat(t1.CAN_debatmj, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:</span><br> ".$l_st_arguments [2]."<br>'),
            				    t1.CAN_statut = '".$l_st_arguments [3]."',
            				    t2.PRO_nbpersos = t2.PRO_nbpersos + ".$l_st_arguments [4]." 
            				WHERE t1.CAN_id = '".$l_st_arguments [1]."' AND t1.CAN_fk_PRO = t2.PRO_id";
            $l_i_type = 1;        
			break;

		case "effacer_candidature":		
            $l_s_requete = "DELETE FROM t_candidatures WHERE CAN_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;        
			break;
		
		case "ajout_historique_candidature":		
            $l_s_requete = "UPDATE t_candidatures SET CAN_historique = concat(CAN_historique, '".date('j\/n\/Y H\:i')." - <span class=\'imp\'>', (SELECT USR_login FROM t_user WHERE USR_id = '".$_SESSION['id']."'),'&nbsp;:&nbsp;</span> ".$l_st_arguments [1]."<br>') WHERE CAN_id = '".$l_st_arguments [2]."'";
            $l_i_type = 1;        
			break;

		case "enregistre_user":	
			// TODO : ajouter dans la requete l'injection de l'ip pour sauvegarde	
            $l_s_requete = "INSERT INTO t_user (USR_login, USR_password, USR_email, USR_commentaire) VALUES ('".$l_st_arguments [1]."', '".$l_st_arguments [2]."', '".$l_st_arguments [3]."', '".$l_st_arguments [4]."')";
            $l_i_type = 1;        
			break;
			
		case "candidatureencours_light":	
            $l_s_requete = "SELECT t1.CAN_id,
            					   t1.CAN_statut,
            					   t1.CAN_nomperso,
            					   t1.CAN_datemodification,
            					   t2.PRO_nom 
            				FROM t_candidatures as t1, t_profil as t2 
            				WHERE 
            					t1.CAN_fk_USR = '".$l_st_arguments [1]."' 
            					AND 
            					t1.CAN_fk_PRO = PRO_id";
            $l_i_type = 0;        
			break;	
			
		case "listeannonces":
			$l_s_requete = "SELECT t1.PRO_id,
								   t1.PRO_nom,
								   t2.CAT_nom,
								   t1.PRO_desc,
								   t1.PRO_nbpersos
							FROM t_profil as t1 LEFT JOIN t_categorie as t2 
							ON t1.PRO_fk_CAT = t2.CAT_id 
							WHERE t1.PRO_statut = '".ENLIGNE."' 
							ORDER BY t2.CAT_id";
            $l_i_type = 0;        
			break;	
		
		case "affiche_annonce":								
			$l_s_requete = "SELECT t1.PRO_id,
								   t1.PRO_nom,
								   t2.CAT_nom,
								   t1.PRO_desc,
								   t1.PRO_nbpersos
							FROM t_profil as t1 LEFT JOIN t_categorie as t2 
							ON t1.PRO_fk_CAT = t2.CAT_id 
							WHERE t1.PRO_statut = '".ENLIGNE."' 
							AND t1.PRO_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;

		case "rappel_annonce":								
			$l_s_requete = "SELECT t1.PRO_id,
								   t1.PRO_nom,
								   t2.CAT_nom,
								   t1.PRO_desc,
								   t1.PRO_nbpersos
							FROM t_profil as t1 LEFT JOIN t_categorie as t2  
							ON t1.PRO_fk_CAT = t2.CAT_id, t_candidatures as t3 
							WHERE t1.PRO_statut = '".ENLIGNE."' 
							AND t1.PRO_id = t3.CAN_fk_PRO AND t3.CAN_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
			
		case "droit_depotcand":
			// requete "à l'envers" : si il y a la moindre condition remplie, on renvoie l'id de la candidature ou du profil incriminé
			// requete à tester plus en profondeur...
			$l_s_requete = "SELECT CAN_id, PRO_id FROM t_candidatures JOIN t_profil 
			WHERE (CAN_fk_USR = '".$l_st_arguments [1]."') 
				   XOR (PRO_id = '".$l_st_arguments [2]."' AND PRO_nbpersos = 0  AND CAN_fk_PRO = PRO_id)
				   XOR (PRO_id = '".$l_st_arguments [2]."' AND (PRO_statut = '".EDITION."' OR PRO_statut = '".RELECTURE."' OR PRO_statut = '".HORSLIGNE."')
				   )";
			$l_i_type = 0;
			break;

		case "droittraitercand":
			// requete "à l'endroit" : on renvoie la CAN_id qui est traitable (avec toutes ses infos)
            $l_s_requete = "SELECT t1.CAN_id, t1.CAN_statut, t1.CAN_nomperso, t2.RAC_nom, t3.CLA_nom, t4.SEX_nom, t1.CAN_descriptionperso, t1.CAN_backgroundperso, t1.CAN_butsperso, t1.CAN_debatusrmj, t5.PRO_id, t1.CAN_fk_RAC, t1.CAN_fk_CLA, t1.CAN_fk_SEX     
            				FROM t_candidatures AS t1 LEFT JOIN t_profil as t5 ON CAN_fk_PRO = PRO_id, t_race AS t2, t_classe AS t3, t_sexe AS t4 
            				WHERE (CAN_id = '".$l_st_arguments [1]."' AND CAN_fk_USR = '".$l_st_arguments [2]."' 
            					   AND CAN_statut != '".ABANDONNEE."' 
            					   AND CAN_statut != '".ACCEPTEE."' 
            					   AND CAN_statut != '".REFUSEE."')
            					  AND 
            					  t1.CAN_fk_RAC = RAC_id AND t1.CAN_fk_CLA = CLA_id AND t1.CAN_fk_SEX = SEX_id";
            $l_i_type = 0;
            break;

			
		case "race_ouverte":
			$l_s_requete = "SELECT t1.RAC_id, t1.RAC_nom FROM t_race as t1 JOIN t_racespossibles as t2 ON t2.RPO_fk_RAC = t1.RAC_id WHERE t2.RPO_fk_PRO = '".$g_i_numeroprofil."'";
			$l_i_type = 0;
			break;

		case "classe_ouverte":
			$l_s_requete = "SELECT t1.CLA_id, t1.CLA_nom FROM t_classe as t1 JOIN t_classespossibles as t2 ON t2.CPO_fk_CLA = t1.CLA_id WHERE t2.CPO_fk_PRO = '".$g_i_numeroprofil."'";
			$l_i_type = 0;
			break;
			
		case "sexe_ouvert":
			$l_s_requete = "SELECT t1.SEX_id, t1.SEX_nom FROM t_sexe as t1 JOIN t_sexespossibles as t2 ON t2.SPO_fk_SEX = t1.SEX_id WHERE t2.SPO_fk_PRO = '".$g_i_numeroprofil."'";
			$l_i_type = 0;
			break;
			
		case "enregistre_candidature":
			$l_s_requete = "INSERT INTO t_candidatures 
							(CAN_fk_USR, 
							CAN_fk_PRO, 
							CAN_STATUT, 
							CAN_nomperso, 
							CAN_fk_RAC, 
							CAN_fk_CLA, 
							CAN_fk_SEX, 
							CAN_descriptionperso, 
							CAN_backgroundperso, 
							CAN_butsperso, 
							CAN_debatusrmj, 
							CAN_historique)
							VALUES 
							('".$l_st_arguments [1]."', 
							'".$l_st_arguments [2]."',
							'1',  
							'".$l_st_arguments [3]."', 
							'".$l_st_arguments [4]."', 
							'".$l_st_arguments [5]."', 
							'".$l_st_arguments [6]."', 
							'".$l_st_arguments [7]."', 
							'".$l_st_arguments [8]."', 
							'".$l_st_arguments [9]."', 
							'".$l_st_arguments [10]."', 
							'".date ('j\/n\/Y H\:i')." - <span class=\'imp\'>".$_SESSION ['user']."&nbsp;:&nbsp;</span> ".$l_st_arguments [11]."<br>')";
			$l_i_type = 1;
			break;
			
		case "decremente_nbpersos":
			$l_s_requete = "UPDATE t_profil SET PRO_nbpersos = (PRO_nbpersos -1) WHERE PRO_id = '".$l_st_arguments [1]."' AND PRO_nbpersos != -1";
			$l_i_type = 0;
			break;
		
		case "incremente_nbpersos":
			$l_s_requete = "UPDATE t_profil AS t1, t_candidatures AS t2 SET t1.PRO_nbpersos = (t1.PRO_nbpersos +1) WHERE t2.CAN_id= '".$l_st_arguments [1]."' AND t2.CAN_fk_PRO = t1.PRO_id AND t1.PRO_nbpersos != -1";
			$l_i_type = 0;
			break;
			

		case "delta_nbpersos_creation":
			$l_s_requete = "SELECT PRO_nbpersos FROM t_profil WHERE PRO_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
			
		case "delta_nbpersos":
			$l_s_requete = "SELECT t1.CAN_statut, t2.PRO_nbpersos FROM t_candidatures AS t1, t_profil AS t2 WHERE CAN_id = '".$l_st_arguments [1]."' AND PRO_id = CAN_fk_PRO";
			$l_i_type = 0;
			break;
			
		case "modifie_nbpersos":
			$l_s_requete = "UPDATE t_profil SET PRO_nbpersos = PRO_nbpersos + ".$l_st_arguments [2]." WHERE PRO_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;

		case "nb_question_max":
			$l_s_requete = "SELECT COUNT(*) FROM t_question WHERE 1";
			$l_i_type = 0;
			break;	
			
		case "recup_questions":
			$l_s_requete = "SELECT QUE_id, QUE_intitule FROM t_question ORDER BY RAND() LIMIT ".NOMBREQUESTIONS."";
			$l_i_type = 0;
			break;		

		case "recup_reponses":
			$l_s_requete = "SELECT REP_id, REP_intitule FROM t_reponse WHERE REP_fk_QUE = '".$l_st_arguments [1]."' ORDER BY RAND()";
			$l_i_type = 0;
			break;

		case "scorequizz":
			$l_s_requete = "SELECT USR_scorequizz FROM t_user WHERE USR_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
		
		case "scorequizz_ok":
			$l_s_requete = "UPDATE t_user SET USR_scorequizz = '".OUI."' WHERE USR_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;
			
		case "reponsesquizz":
			$l_s_requete = "SELECT REP_id, REP_bonne, REP_fk_QUE FROM t_reponse WHERE ".$l_st_arguments [1];
			$l_i_type = 0;
			break;	
			
		case "abandonnercand":
            $l_s_requete = "UPDATE t_candidatures SET CAN_statut = '".ABANDONNEE."' WHERE CAN_id = '".$l_st_arguments [1]."'";
            $l_i_type = 1;			
			break;
			
		case "editercand":
            $l_s_requete = "UPDATE t_candidatures 
            				SET CAN_nomperso = '".$l_st_arguments [3]."', 
            					CAN_fk_RAC = '".$l_st_arguments [4]."', 
            					CAN_fk_CLA = '".$l_st_arguments [5]."', 
            					CAN_fk_SEX = '".$l_st_arguments [6]."', 
            					CAN_descriptionperso = '".$l_st_arguments [7]."', 
            					CAN_backgroundperso = '".$l_st_arguments [8]."', 
            					CAN_butsperso = '".$l_st_arguments [9]."', 
            					CAN_debatusrmj = '".$l_st_arguments [10]."', 
            					CAN_historique = '".date ('j\/n\/Y H\:i')." - <span class=\'imp\'>".$_SESSION ['user']."&nbsp;:&nbsp;</span> ".$l_st_arguments [11]."<br>', 
            					CAN_statut = '".RELECTUREMJ."'
            				WHERE CAN_id = '".$l_st_arguments [2]."'";
            $l_i_type = 1;			
			break;	

		case "race_cand":
			$l_s_requete = "SELECT RAC_id, RAC_nom FROM t_race, t_candidatures, t_racespossibles
							WHERE
							(
								CAN_id = '".$g_i_idcand."' 
							AND
								RPO_fk_PRO = CAN_fk_PRO 
							AND
								RAC_id = RPO_fk_RAC 
							AND
								RAC_insc_ouverte = ".OUI." 
							)
							OR
							(
								RAC_id = '".$g_i_idcand."' 
							AND
								RAC_id = CAN_fk_RAC 
							AND
								RAC_insc_ouverte = ".NON." 
							AND
								RPO_fk_PRO = CAN_fk_PRO 
							AND
								RPO_fk_RAC = RAC_id 
							)";
			$l_i_type = 0;
			break;	

			
		case "classe_cand":
			$l_s_requete = "SELECT CLA_id, CLA_nom FROM t_classe, t_candidatures, t_classespossibles
							WHERE
							(
								CAN_id = '".$g_i_idcand."' 
							AND
								CPO_fk_PRO = CAN_fk_PRO 
							AND
								CLA_id = CPO_fk_CLA 
							AND
								CLA_insc_ouverte = ".OUI." 
							)
							OR
							(
								CAN_id = '".$g_i_idcand."' 
							AND
								CLA_id = CAN_fk_CLA 
							AND
								CLA_insc_ouverte = ".NON." 
							AND
								CPO_fk_PRO = CAN_fk_PRO 
							AND
								CPO_fk_CLA = CLA_id 
							)";
			$l_i_type = 0;
			break;		

		case "sexe_cand":
			$l_s_requete = "SELECT SEX_id, SEX_nom FROM t_sexe, t_candidatures, t_sexespossibles
							WHERE
							(
								CAN_id = '".$g_i_idcand."' 
							AND
								SPO_fk_PRO = CAN_fk_PRO 
							AND
								SEX_id = SPO_fk_SEX 
							AND
								SEX_insc_ouverte = ".OUI." 
							)
							OR
							(
								CAN_id = '".$g_i_idcand."' 
							AND
								SEX_id = CAN_fk_SEX 
							AND
								SEX_insc_ouverte = ".NON." 
							AND
								SPO_fk_PRO = CAN_fk_PRO 
							AND
								SPO_fk_SEX = SEX_id 
							)";
			$l_i_type = 0;
			break;
			
		case "liste_questions":
			$l_s_requete = "SELECT QUE_id, QUE_intitule FROM t_question WHERE 1";
			$l_i_type = 0;
			break;			
			
		case "recupere_question":
			$l_s_requete = "SELECT QUE_id, QUE_intitule FROM t_question WHERE QUE_id = '".$g_i_numeroquestion."'";
			$l_i_type = 0;
			break;
			
		case "existe_question":
			$l_s_requete = "SELECT QUE_id FROM t_question WHERE QUE_id = '".$g_i_numeroquestion."'";
			$l_i_type = 0;
			break;
			
		case "recupere_reponses":
			$l_s_requete = "SELECT REP_id, REP_intitule, REP_bonne FROM t_reponse WHERE REP_fk_QUE = '".$g_i_numeroquestion."'";
			$l_i_type = 0;
			break;
			
        case "effacerquestion":
        default:
            $l_s_requete = "DELETE FROM t_question WHERE QUE_id='".$l_st_arguments [1]."'";
            $l_i_type = 1;
            break;
		
		case "ajoutequestionquizz":
            $l_s_requete = "INSERT INTO t_question (QUE_intitule) VALUES ('".$l_st_arguments [1]."')";
            $l_i_type = 1;			
			break;		

		case "ajoutereponsequizz":
            $l_s_requete = "INSERT INTO t_reponse (REP_fk_QUE, REP_intitule, REP_bonne) VALUES ('".$l_st_arguments [3]."', '".$l_st_arguments [1]."', '".$l_st_arguments [2]."')";
            $l_i_type = 1;			
			break;
			
		case "effacereponsesquizz":
            $l_s_requete = "DELETE FROM t_reponse WHERE REP_fk_QUE='".$l_st_arguments [1]."'";
            $l_i_type = 1;			
			break;	
			
		case "corrigequestion":
			$l_s_requete = "UPDATE t_question SET QUE_intitule = '".$l_st_arguments [2]."' WHERE QUE_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;
			
		case "raceouverte_admin":
			$l_s_requete = "SELECT RAC_id, RAC_nom, RAC_insc_ouverte FROM t_race WHERE 1 ORDER BY RAC_nom ASC";
			$l_i_type = 0;
			break;		

		case "classeouverte_admin":
			$l_s_requete = "SELECT CLA_id, CLA_nom, CLA_insc_ouverte FROM t_classe WHERE 1  ORDER BY CLA_nom ASC";
			$l_i_type = 0;
			break;
		
		case "sexeouvert_admin":
			$l_s_requete = "SELECT SEX_id, SEX_nom, SEX_insc_ouverte FROM t_sexe WHERE 1  ORDER BY SEX_nom ASC";
			$l_i_type = 0;
			break;
			
		case "existerace":
			$l_s_requete = "SELECT RAC_id FROM t_race WHERE RAC_id='".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;			
		
		case "existeclasse":
			$l_s_requete = "SELECT CLA_id FROM t_classe WHERE CLA_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
					
		case "existesexe":		
			$l_s_requete = "SELECT SEX_id FROM t_sexe WHERE SEX_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
		
		case "possibilitesprofilrace":
			$l_s_requete = "SELECT 
											t1.PRO_id, 
											(SELECT COUNT( t2.RPO_id ) FROM t_racespossibles WHERE RPO_fk_RAC = '".$l_st_arguments [1]."' AND RPO_fk_PRO = t1.PRO_id) AS compte
									FROM 
										t_profil AS t1, 
										t_racespossibles AS t2 
									WHERE 
											t1.PRO_statut = '".ENLIGNE."' 
									GROUP BY 
										t1.PRO_id 
									HAVING 
										compte != 'NULL'";
			$l_i_type = 0;
			break;
		
		case "possibilitesprofilclasse":
			$l_s_requete = "SELECT 
											t1.PRO_id, 
											(SELECT COUNT( t2.CPO_id ) FROM t_classespossibles WHERE CPO_fk_CLA = '".$l_st_arguments [1]."' AND CPO_fk_PRO = t1.PRO_id) AS compte
									FROM 
										t_profil AS t1, 
										t_classespossibles AS t2 
									WHERE 
											t1.PRO_statut = '".ENLIGNE."' 
									GROUP BY 
										t1.PRO_id 
									HAVING 
										compte != 'NULL'";
			$l_i_type = 0;
			break;		
		
		case "possibilitesprofilsexe":
			$l_s_requete = "SELECT 
											t1.PRO_id, 
											(SELECT COUNT( t2.SPO_id ) FROM t_sexespossibles WHERE SPO_fk_SEX = '".$l_st_arguments [1]."' AND SPO_fk_PRO = t1.PRO_id) AS compte
									FROM 
										t_profil AS t1, 
										t_sexespossibles AS t2 
									WHERE 
											t1.PRO_statut = '".ENLIGNE."' 
									GROUP BY 
										t1.PRO_id 
									HAVING 
										compte != 'NULL'";
			$l_i_type = 0;
			break;
			
		case "candassocierace":
            $l_s_requete = "SELECT CAN_id, CAN_fk_PRO FROM t_candidatures WHERE CAN_fk_RAC = '".$l_st_arguments [1]."' AND (CAN_statut = '".EDITIONUSER."' OR CAN_statut ='".RELECTUREMJ."' OR CAN_statut = '".AVISMJ."' OR CAN_statut = '".ENATTENTE."')";
            $l_i_type = 0;		
			break;

		case "candassocieclasse":
            $l_s_requete = "SELECT CAN_id, CAN_fk_PRO FROM t_candidatures WHERE CAN_fk_CLA = '".$l_st_arguments [1]."' AND (CAN_statut = '".EDITIONUSER."' OR CAN_statut ='".RELECTUREMJ."' OR CAN_statut = '".AVISMJ."' OR CAN_statut = '".ENATTENTE."')";
            $l_i_type = 0;		
			break;
			
		case "candassociesexe":
            $l_s_requete = "SELECT CAN_id, CAN_fk_PRO FROM t_candidatures WHERE CAN_fk_SEX = '".$l_st_arguments [1]."' AND (CAN_statut = '".EDITIONUSER."' OR CAN_statut ='".RELECTUREMJ."' OR CAN_statut = '".AVISMJ."' OR CAN_statut = '".ENATTENTE."')";
            $l_i_type = 0;		
			break;
			
		case "majrace":
			$l_s_requete = "UPDATE t_race SET RAC_insc_ouverte = '".$l_st_arguments [2]."' WHERE RAC_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;

		case "majclasse":
			$l_s_requete = "UPDATE t_classe SET CLA_insc_ouverte = '".$l_st_arguments [2]."' WHERE CLA_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;

		case "majsexe":
			$l_s_requete = "UPDATE t_sexe SET SEX_insc_ouverte = '".$l_st_arguments [2]."'  WHERE SEX_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;
			
		case "listecategories":
			$l_s_requete = "SELECT CAT_id, CAT_nom FROM t_categorie WHERE 1";
			$l_i_type = 0;
			break;
			
		case "existe_categorie":
			$l_s_requete = "SELECT CAT_id FROM t_categorie WHERE CAT_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;	

		case "recuperecategorie":
			$l_s_requete = "SELECT CAT_id, CAT_nom FROM t_categorie WHERE CAT_id = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
			
		case "effacercategorie":
			$l_s_requete = "DELETE FROM t_categorie WHERE CAT_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;
		
		case "profilsassociescategorie":
			$l_s_requete = "SELECT PRO_id FROM t_profil WHERE PRO_fk_CAT = '".$l_st_arguments [1]."'";
			$l_i_type = 0;
			break;
		
		case "ajoutecategorie":
			$l_s_requete = "INSERT INTO t_categorie (CAT_nom) VALUES ('".$l_st_arguments [1]."')";
			$l_i_type = 1;
			break;
		
		case "corrigecategorie":
			$l_s_requete = "UPDATE t_categorie SET CAT_nom = '".$l_st_arguments [2]."' WHERE CAT_id = '".$l_st_arguments [1]."'";
			$l_i_type = 1;
			break;												
							
        case "":
        default:
            $l_s_requete = "";
            $l_i_type = 0;
            break;
    }

	print ("nom_requete=".$l_st_arguments [0]."<br>");
    print("requete=".$l_s_requete."<br>");
    // Exécution de la requête
    $l_i_resultat = mysql_query ($l_s_requete, $g_i_lien);
    if ($l_i_resultat == FALSE)
    {
        trigger_error ("Erreur lors de l'exécution de la requete : ".$l_s_requete.", ".mysql_error ($g_i_lien), ERROR);
        exit (0);
    }
    // Construction de la réponse
    switch ($l_i_type)
    {
        case 0: // requêtes de type SELECT
            $l_i_nbenregistrements = mysql_num_rows ($l_i_resultat);
            //print ("Nb résultats : ".$l_i_nbenregistrements."<br>");
            if ($l_i_nbenregistrements == 0) // Pas de résultat à la requête exécutée
            {
                //print("Pas de résultat à la requête<br>");
                $l_st_resultat = array (0, array ());
            }

            else // 1 résultat ou plus à la requête exécutée
            {
                for ($l_i_index = 0; $l_i_index < $l_i_nbenregistrements; $l_i_index++)
                {
                    $l_st_tableresultat [$l_i_index] = mysql_fetch_array ($l_i_resultat, MYSQL_NUM);
                }

                //print ("Y a du résultat...<br>");
                $l_st_resultat = array ($l_i_nbenregistrements, $l_st_tableresultat);
            }

            break;

        case 1: // autres types de requêtes
            $l_i_nbaffecte = mysql_affected_rows ($g_i_lien);
            $l_i_dernier_id_affecte = mysql_insert_id ($g_i_lien);
            print ("Nombre affecte : ".$l_i_nbaffecte."<br>");
            $l_st_resultat = array ("nbaffecte" => $l_i_nbaffecte, "dernier_id_affecte" => $l_i_dernier_id_affecte);
            break;
    }

  return $l_st_resultat;
}
?>
