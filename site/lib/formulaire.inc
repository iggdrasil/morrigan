<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier :  formulaire.inc
* Fonction du fichier :  bibliothèque de fonctions ayant trait au traitement des formulaires
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  09/12/2004
* Version actuelle :  1.0 au 09/12/2004
* License du projet : gpl
* Dernières modifications :
* Remarques particulières : Multiples TODOs plus ou moins critiques à traiter
							Supprimer les print de debug
							/!\ TODO : controler que les champs textes ne sont pas vides si nécessaire
							dans controle_variable							
*
*****************************************************************************************************/

function liste_options ($l_s_nomchamp)
{
/****************************************************************************************************
*
*   FONCTION liste_options
*   Gère la constrution de la liste d'options à afficher pour un champ de formulaire de type
*     - select
*     - radio
*     - checkbox
*
*   PARAMETRES :
*     - $l_s_nomchamp : nom du champ dont on veut la liste d'options
*   RENVOIE :
*     - $l_t_st_options : tableau contenant la liste des options possibles sous forme :
*      {id; nom de l'option}
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    $l_t_st_options = array ();
    $l_i_compteur = 0;
    //$l_t_st_options = array (array (0, "guerrier"), array (1, "tapette"), array (2, "grosnaze"), array (3, "amaot")); // pour debug
      /* Corps de la fonction */
    switch ($l_s_nomchamp)
    {
        // Ces champs-là appellent une requête
        case "racespossiblesed":
        case "classespossiblesed":
        case "sexespossiblesed":
        case "statutprofil":
        case "auteurprofil":
        case "categorie":
        case "statutcandidature":
        case "race_ouverte":
        case "classe_ouverte":
        case "sexe_ouvert":
        case "race_cand":
        case "classe_cand":
        case "sexe_cand":
            $l_t_st_options = requete ($l_s_nomchamp);
            break;

        // Ces champs-là ont leur liste d'options codée en dur
        case "criteretri":
            $l_t_st_options [0] = 4;
            $l_t_st_options [1][0][0] = IDPROFIL;
            $l_t_st_options [1][1][0] = AUTEURPROFIL;
            $l_t_st_options [1][2][0] = CATPROFIL;
            $l_t_st_options [1][3][0] = STATUTPROFIL;
            break;

        case "actionprofil":
            $l_t_st_options [0] = 10;           
            $l_t_st_options [1][0][0] = AFFICHER;
            $l_t_st_options [1][1][0] = CREER;
            $l_t_st_options [1][2][0] = CREATIONDAPRES;
            $l_t_st_options [1][3][0] = EDITER;
            $l_t_st_options [1][4][0] = DEMANDEENLIGNE;
            $l_t_st_options [1][5][0] = DEMANDEHORSLIGNE;
            $l_t_st_options [1][6][0] = DEMANDEEDITER;
            $l_t_st_options [1][7][0] = STATUT;
            $l_t_st_options [1][8][0] = EFFACER;
            $l_t_st_options [1][9][0] = REFUSER;
            break;
        
        case "actioncandidature":
            $l_t_st_options [0] = 11;
            $l_t_st_options [1][0][0] = AUCUNE;
            $l_t_st_options [1][1][0] = COMMENTERPJ;
            $l_t_st_options [1][2][0] = DONNERAVISMJ;
            $l_t_st_options [1][3][0] = COMMENTERMJ;
            $l_t_st_options [1][4][0] = DEMANDERAVIS;
            $l_t_st_options [1][5][0] = REFUSERCAN;
            $l_t_st_options [1][6][0] = EFFACERCAN;
            $l_t_st_options [1][7][0] = VALIDERCAN;
            $l_t_st_options [1][8][0] = METTREENATTENTE;
            $l_t_st_options [1][9][0] = COMMENTERGLOBAL;   
            $l_t_st_options [1][10][0] = CHANGERSTATUT;  
        	break;
        
        case "actiontraitercand":
            $l_t_st_options [0] = 3;
            $l_t_st_options [1][0][0]= AUCUNE;
            $l_t_st_options [1][0][1]= AUCUNETXT;
            $l_t_st_options [1][1][0]= ABANDONNERCAND;
            $l_t_st_options [1][1][1]= ABANDONNERCANDTXT;           
            $l_t_st_options [1][2][0] = EDITERCAND; 
            $l_t_st_options [1][2][1] = EDITERCANTXT;   
            
        case "actionnews":
        	$l_t_st_options [0] = 3;
            $l_t_st_options [1][0][0]= CREERNEWS;
            $l_t_st_options [1][0][1]= CREERNEWSTXT;
            $l_t_st_options [1][1][0]= EDITERNEWS;
            $l_t_st_options [1][1][1]= EDITERNEWSTXT;           
            $l_t_st_options [1][2][0] = EFFACERNEWS; 
            $l_t_st_options [1][2][1] = EFFACERNEWSTXT;         	  	
        
        case "idquestions":
	       	if (isset ($_SESSION ['idquestions']) && isset ($_SESSION ['graindeselquizz']))
        	{
	       		for ($l_i_compteur = 0; $l_i_compteur < count ($_SESSION ['idquestions']); $l_i_compteur ++)
        		{
        			$l_t_st_options [1][$l_i_compteur][0] = $_SESSION ['idquestions'][$l_i_compteur];
				}

        		// Prise en compte du grain de sel
        		for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_st_options [1]); $l_i_compteur ++)
        		{
        			$l_t_st_options [1][$l_i_compteur][0] = $l_t_st_options [1][$l_i_compteur][0] + (int)$_SESSION ['graindeselquizz'];
				}
        	}
        	else
        	{
        		trigger_error ("La variable de session idquestions n'existe pas lors du passage du quizz par le candidat '".$_SESSION ['id']."'.", FATAL);
        		exit ();
        	}
        	break; 
        	
        case "idreponses":   		
        	if (isset ($_SESSION ['idreponses']))
        	{
        		for ($l_i_compteur = 0; $l_i_compteur < count ($_SESSION ['idreponses']); $l_i_compteur ++)
        		{        		
        			$l_t_st_options [1][$l_i_compteur][0] = $_SESSION ['idreponses'][$l_i_compteur];
        		}
        	}
        	else
        	{
        		trigger_error ("La variable de session idreponses n'existe pas lors du passage du quizz par le candidat '".$_SESSION ['id']."'.", FATAL);
        		exit ();
        	}
        	break;
        	
      	case "actionquizz":
            $l_t_st_options [0] = 3;
            $l_t_st_options [1][0][0] = CREERQUESTION;
            $l_t_st_options [1][1][0] = EFFACERQUESTION;    
            $l_t_st_options [1][2][0] = CORRIGERQUESTION;      	
      		break;
      	
      	case "reponsebonne1":
       	case "reponsebonne2":     		
      	case "reponsebonne3":
      	case "reponsebonne4":      	
      	case "reponsebonne5":
      		$l_t_st_options [0] = 2;
      		$l_t_st_options [1][0][0] = OUI;
      		$l_t_st_options [1][0][1] = $l_s_nomchamp;     	
      		break;
      		
		case "actionrace":
		case "actionclasse":
		case "actionsexe":
			$l_t_st_options [0] = 2;
			$l_t_st_options [1][0][0] = OUI;
			$l_t_st_options [1][1][0] = NON;
			$l_t_st_options [1][0][1] = OUVERTTXT;
			$l_t_st_options [1][1][1] = FERMETXT;	
			break;
	
        default:
            // Là, y a un hic : le nom du champ n'est pas reconnu
            break;
    }
    return $l_t_st_options;
}


function recup_variable (&$l_st_variable, $l_s_type)
{
/****************************************************************************************************
*
*   FONCTION recup_variable
*   Gère la récupération des variables passées en formulaire
*
*   PARAMETRES :
*      - &$l_st_variable : passage par référence de la structure de manipulation de la variable récupérée
*      - $l_s_type : indique le type de la variable récupérée : array, integer, password, email, text
*   RENVOIE :
*      - rien, la structure passée en référence est modifiée par la valeur récupérée par cette fonction
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    $l_i_clef = 0; //clef d'itération utilisée dans la boucle de traitement de variables à valeurs multiples
    $l_i_contenu = ""; // référence utilisée pour manipuler les variables multiples

      /* Corps de la fonction */
    if (isset ($_POST [$l_st_variable [NOM_VAR]]))
    {
        // Traitement spécial pour les variables pouvant prendre plusieurs valeurs
        if ($l_s_type == "array")
        {
            $l_st_variable [CONTENU_VAR] = $_POST [$l_st_variable [NOM_VAR]];

            foreach (array_keys ($l_st_variable [CONTENU_VAR]) as $l_i_clef)
            {
                $l_i_contenu = $l_st_variable [CONTENU_VAR] [$l_i_clef];
                $l_i_contenu = addslashes(htmlspecialchars (trim ($l_i_contenu)));
                // Tant qu'on y est, on en profite pour fixer le type de la variable
                settype ($l_i_contenu, "integer");
                $l_st_variable [CONTENU_VAR] [$l_i_clef] = $l_i_contenu;
            }
        }
        else
        {
            $l_st_variable [CONTENU_VAR] = addslashes(htmlspecialchars (trim ($_POST [$l_st_variable [NOM_VAR]])));
        }

        if ($l_s_type == "password")
        {
            $l_st_variable [CONTENU_VAR] = chiffre ($l_st_variable [CONTENU_VAR]);
        }

        switch ($l_s_type)
        {
            case "integer":
                settype ($l_st_variable [CONTENU_VAR],  "integer");
            break;

            case "array":
                // le settype est déjà fait plus haut
            break;

            case "text":
            case "password":
            case "email":
                settype ($l_st_variable [CONTENU_VAR],  "string");
            break;

            default:
                trigger_error ("Récupération d'une variable de type inconnu dans un formulaire",  FATAL);
                exit ();
            break;
        }
    }
    else
    {
        switch ($l_s_type)
        {
            case "array":
                // renvoyer une coquille vide, l'utilisateur n'a probablement rien sélectionné
                $l_st_variable [CONTENU_VAR] = array();
                break;
            default:
                // variable non définie : ajouter le traitement d'erreur, puisqu'il devait y avoir un e variable a ce nom avec sa valeur associée
                // /!\ TODO : ajouter le traitement d'erreur
                break;
        }
    }
}


function affiche_variable (&$l_st_variable, $l_s_genre, $l_i_handlerformulaire, $l_t_st_options)
{
/****************************************************************************************************
*
*   FONCTION affiche_variable
*   Gère l'affichage des variables dans les formulaires
*   Trois états possibles :
*       - par défaut, lors du premier affichage du formulaire
*       - avec un texte d'explication en cas d'erreur
*       - en champ caché, si le champ ne contient pas d'erreur, mais qu'il y a une erreur dans un
*          autre champ du formulaire qui est erronné et qu'il faudra retransmettre le formulaire
*   ATTENTION, les variables renvoyées par un select, un radio ou un checkbox auront toujours des valeurs NUMERIQUES
*
*
*   PARAMETRES :
*      - &$l_st_variable : passage par référence de la structure de manipulation de la variable à afficher
*      - $l_s_genre : indique le genre de la variable à afficher : textbox, select, radio, checkbox, text, input
*      - $l_i_handlerformulaire : indique le handler du formulaire à afficher
*	   - $l_t_st_options : si les options sont prédéfinies, elles sont passées ici, sinon, il y aura appel à la fonction qui va bien
*   RENVOIE :
*       - rien, l'affichage est ajouté dans le flux de la page
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    global $l_o_template;
    $l_i_compteur = 0;
    $l_i_compteur2 = 0;

    /* Corps de la fonction */
    switch ($l_st_variable [TYPE_DISPLAY])
    {
         case HIDDEN: // affichage d'un champ caché
            $l_o_template -> addSession ($l_i_handlerformulaire,  "hidden_".$l_st_variable [NOM_VAR]);
            $l_o_template -> setVar ($l_i_handlerformulaire, "hidden_".$l_st_variable [NOM_VAR].".".$l_st_variable [NOM_VAR], stripslashes ($l_st_variable [CONTENU_VAR]));
            $l_o_template -> closeSession ($l_i_handlerformulaire,  "hidden_".$l_st_variable [NOM_VAR]);
            break;
         case ERRONE: // affichage du message d'erreur
            $l_o_template -> addSession ($l_i_handlerformulaire,  "erreur_".$l_st_variable [NOM_VAR]);
            $l_o_template -> closeSession ($l_i_handlerformulaire,  "erreur_".$l_st_variable [NOM_VAR]);
         case NORMAL: // affichage du champ proprement dit
         default:
            switch ($l_s_genre)
            {
                case "input":

                case "password":

                case "text":

                case "textarea":
                   $l_o_template -> addSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                   $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR].".".$l_st_variable [NOM_VAR], stripslashes ($l_st_variable [CONTENU_VAR]));
                   $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                   break;

                case "select":
                   
                    // Récupérer les options possibles
                    if (!is_int ($l_t_st_options [0]))
                    {
                    	$l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);
					}

                    // Afficher le select et ses options
                    $l_o_template -> addSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                    $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR].".name", $l_st_variable [NOM_VAR]);

                    for ($l_i_compteur = 0; $l_i_compteur < $l_t_st_options [0]; $l_i_compteur ++)
                    {
                        $l_o_template -> addSession ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_select_option");
                        // Penser à formater la valeur pour l'affichage
                        // /!\ TODO : formater la valeur pour l'affichage
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_select_option.value", $l_t_st_options [1] [$l_i_compteur] [0]);
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_select_option.nom", stripslashes ($l_t_st_options [1] [$l_i_compteur] [1]));

                        for ($l_i_compteur2 = 0; $l_i_compteur2 < count ($l_st_variable [CONTENU_VAR]); $l_i_compteur2++)
                        {
		                    if (
		                    		(
		                    			$l_st_variable [CONTENU_VAR][$l_i_compteur2] == $l_t_st_options [1][$l_i_compteur] [0]
		                    		) 
		                    		|| 
		                    		(
		                    			$l_t_st_options [1][$l_i_compteur] [2] != NULL
		                    			&&
		                    			$l_st_variable [CONTENU_VAR][$l_i_compteur2] == ""
		                    		)
		                    	) // option selectionnée
                        	{
                            	$l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR] . "_select_option.selected", "selected");
                        	}	
                        }
                        

                        $l_o_template -> closeSession ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_select_option");
                    }
                    $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                    break;

                case "radio":
                    // Récupérer les options possibles
                    // ici doit se trouver une requête associée au nom de la variable
                    if (!is_int ($l_t_st_options [0]))
                    {
                    	$l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);
					}

                    if ($l_t_st_options == NULL && (!is_array ($l_t_st_options)))
                    {
                    	$l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);
					}
					
                    // Afficher les options
                    $l_o_template -> addSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                    for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_st_options [1]); $l_i_compteur ++)
                    {
                        $l_o_template -> addSession ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_radio_option");
                        // Penser à formater la valeur pour l'affichage
                        // /!\ TODO : formater la valeur pour l'affichage
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_radio_option.value", $l_t_st_options [1][$l_i_compteur] [0]);
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_radio_option.nom", stripslashes ($l_t_st_options [1][$l_i_compteur] [1]));

                        if ($l_st_variable [CONTENU_VAR] == $l_t_st_options [1] [$l_i_compteur] [0]) // option selectionnée
                        {
                            $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_radio_option.selected", "checked");
                        }
                        $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]."_radio_option");
                    }
                    $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                    break;

                case "checkbox":
                    // Récupérer les options possibles
                    // ici doit se trouver une requête associée au nom de la variable
                    if ($l_t_st_options == NULL  && (!is_array ($l_t_st_options)))
                    {
                    	$l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);
					}
                    // Afficher le select et ses options
                    $l_o_template -> addSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);

                    for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_st_options [1]); $l_i_compteur ++)
                    {
                        $l_o_template -> addSession ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_checkbox_option");
                        // Penser à formater la valeur pour l'affichage
                        // /!\ TODO : formater la valeur pour l'affichage
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_checkbox_option.value", $l_t_st_options [1][$l_i_compteur] [0]);
                        $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_checkbox_option.nom", stripslashes ($l_t_st_options [1][$l_i_compteur] [1]));

                        for ($l_i_compteur2 = 0; $l_i_compteur2 < count ($l_st_variable [CONTENU_VAR]); $l_i_compteur2 ++)
                        {
                            if ($l_st_variable [CONTENU_VAR] [$l_i_compteur2] == $l_t_st_options [1][$l_i_compteur] [0]) // option selectionnée
                            {
                                    $l_o_template -> setVar ($l_i_handlerformulaire, $l_st_variable [NOM_VAR]."_checkbox_option.selected", "checked");
                            }
                        }
                        $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]."_checkbox_option");
                    }
                    $l_o_template -> closeSession ($l_i_handlerformulaire,  $l_st_variable [NOM_VAR]);
                    break;

                default:
                    // /!\ TODO : insérer ici une gestion d'erreur pour un champ non reconnu
                    break;
            }
            break;
    }
}



function controle_variable (&$l_st_variable, $l_st_controle)
{
/****************************************************************************************************
*
*   FONCTION controle_variable
*   Effectue le contrôle automatisé des variables passées en formulaire
*
*   PARAMETRES :
*      - &$l_st_variable : passage par référence de la structure de manipulation de la variable récupérée
*      - $l_st_controle : indique le type de contrôle à faire sur la variable récupérée, et les valeurs nécessaires
*      au contrôle personnalisé de certaines variables (longueur de champ de texte par exemple)
*   RENVOIE :
*      - NON s'il n'y a pas d'erreur, OUI s'il y a une erreur
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    // liste des options possibles pour la variable passée en paramètre.
    $l_t_st_options = array (); // (array (0, "guerrier"), array (1, "tapette"), array (2, "grosnaze"), array (3, "amaot")); // tableau défini pour debug
    $l_i_valeur = 0; // sert à parcourir les options choisies
    $l_st_valeur2 = array (); // sert à parcourir les options possibles
    $l_i_flag = 0; //  sert à vérifier la présence de l'option choisie parmi les options possibles
    $l_i_decompte = 0; // sert à compter le nombre d'options passées
    $l_st_doublon_resultat = array (); // pour les vérifications par requête

    /* Corps de la fonction */
    switch ($l_st_controle [0])
    {
        case "checkbox":

        case "select":
            // Il faut vérifier que :
            // - il n'y a pas plus de résultats que de valeurs possibles
            // - le(s) résultat(s) choisi(s) est(sont) valide(s)
            // - il n'y a pas de doublons dans les résultats choisis

            // Dans $l_st_controle, pour les select et les checkbox, on trouve [type_controle, nb_options_min, nb_options_max]

            // Récupérer les options possibles
            $l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);
		
            // On cherche les éventuels doublons pour faire du nettoyage
            $l_st_variable [1] = array_unique ($l_st_variable [CONTENU_VAR]);

            // On compte le nombre d'entrées possibles et le nombre d'entrées sélectionnées
            // $l_i_decompte est utilisé pour compter les variables sélectionnées
            // count ($l_t_st_options) est utilisé pour compter les variables possibles
            $l_i_decompte = count ($l_st_variable [CONTENU_VAR]);

            if (count ($l_t_st_options [1]) < $l_i_decompte && $l_st_controle [2] < $l_i_decompte)
            {
                trigger_error ("Le nombre d'options passees est superieur au nombre d'options possibles (variable ".$l_st_variable [NOM_VAR].", options "/*.var_dump ($l_st_variable [CONTENU_VAR][1]).")."*/, ERROR);
                exit ();
            }
            elseif ($l_i_decompte < $l_st_controle [1])
            {
                // Pas assez d'options choisies
                $l_st_variable [TYPE_DISPLAY] = ERRONE;
                return OUI;
            }
            elseif ($l_i_decompte > $l_st_controle [2])
            {
                // Trop d'options choisies
                $l_st_variable [TYPE_DISPLAY] = ERRONE;
                return OUI;
            }

            // On contrôle la validité des options choisies, s'il y en a
            foreach ($l_st_variable [CONTENU_VAR] as $l_i_valeur)
            {
                // On réinitialise le drapeau
                $l_i_flag = NON;

                for ($l_i_compteur = 0; $l_i_compteur < count ($l_t_st_options [1]); $l_i_compteur++)
                {
                	if ($l_t_st_options [1][$l_i_compteur][0] == $l_i_valeur)
                	{

                	    $l_i_flag = OUI;	
					}
				}
                
                if ($l_i_flag == NON)
                {
                    // On n'a pas trouvé l'option choisie dans les options possibles
                    trigger_error ("Une des options passees n'est pas compatible avec les options passees (variable ".$l_st_variable [NOM_VAR].").", ERROR);
                    exit ();
                }
            }

            break;

        case "radio":
            // Il faut vérifier que :
            // - le résultat choisi est valide
            // Récupérer les options possibles
            $l_t_st_options = liste_options ($l_st_variable [NOM_VAR]);

            // On compte nombre d'entrées sélectionnées
           if (!is_numeric ($l_st_variable [CONTENU_VAR]))
            {
                $l_st_variable [TYPE_DISPLAY] = ERRONE;
                return OUI;
            }
            else
            {
                // On contrôle la validité de  l'option choisie
                foreach ($l_t_st_options as $l_st_valeur2)
                {
                    if (!in_array ($l_st_variable [CONTENU_VAR], $l_st_valeur2, TRUE))
                    {
                        // pas dedans, on passe à l'option suivante
                    }
                    else
                    {
                        $l_i_flag = OUI;
                    }
                }

                if ($l_i_flag == NON)
                {
                    // On n'a pas trouvé l'option choisie dans les options possibles
                    trigger_error ("Une des options passees n'est pas compatible avec les options passsees (variable ".$l_st_variable [NOM_VAR].", options "/*.var_dump ($l_st_variable [CONTENU_VAR]).")."*/, ERROR);
                    exit ();
                 }
            }

            break;

        case "email":
            // Il faut vérifier que :
            // - l'email est valable syntaxiquement
            // - dans le cas d'une première entrée, l'email n'est pas déjà utilisé
            // - l'email n'est pas dans la liste des bannis si nécessaire
            // La structure de contrôle a cette forme (à faire évoluer selon les besoins de vérification supplémentaires) :
            // [type_controle, flag_verif_doublon, flag_verif_bannis]


            // vérification syntaxique
            if(!check_mail ($l_st_variable [CONTENU_VAR]))
            {
                // L'adresse n'est pas syntaxiquement valide
                $l_st_variable [TYPE_DISPLAY] = ERRONE;
                return OUI;
            }

            // vérification de doublon
            if ($l_st_controle [1] == OUI)
            {
                $l_st_doublon_resultat = requete ("doublon_mail", $l_st_variable [CONTENU_VAR]);

                if ($l_st_doublon_resultat [0] != NON)
                {
                    $l_st_variable [TYPE_DISPLAY] = ERRONE;
                    return OUI;
                }
            }

            if ($l_st_controle [2] == OUI)
            {
                // TODO : ici doit se trouver une requête et un traitement pour s'assurer que le mail ou le domaine employé n'est pas banni
                // NOTE : Non implémenté dans cette version
            }

            break;

        case "text":
            // Il faut vérifier que :
            // - le texte n'est pas trop long
            // - il y a un texte dans le champ (si demandé de contrôler)
            // - le cas échéant, la valeur n'existe pas déjà (pour un pseudo, par exemple)
            // La structure de contrôle a cette forme (à faire évoluer selon les besoins de vérification supplémentaires):
            // [type_controle, flag_verif_doublon, longueur_max, champ_a_controler, table_a_controler, champ_id, id_a_controler]

            if ($l_st_controle [1] == OUI) // flag verif doublon
            {
                $l_st_doublon_resultat = requete ("doublon_".$l_st_variable [NOM_VAR], $l_st_variable [CONTENU_VAR], $l_st_controle [3]);
                
                // si jamais c'est le champ en cours qui est contrôlé comme doublon, tout va bien
                // contrôle déjà effectué dans la requête
                /*if (($l_st_doublon_resultat [0] != 0) && ($l_st_doublon_resultat [1][0][1] != $l_st_variable [CONTENU_VAR]))
                {
                    $l_st_variable [TYPE_DISPLAY] = ERRONE;
                    return OUI;
                }*/
                print ("doublon résultat :".$l_st_doublon_resultat [0]."<br/>");
                
                if ($l_st_doublon_resultat [0] >= 1)
                {
                    $l_st_variable [TYPE_DISPLAY] = ERRONE;
                    return OUI;
				}
            }

            if ($l_st_controle [2] > 0)
            {
                // Il faut vérifier la longueur du champ
                if (strlen ($l_st_variable [CONTENU_VAR]) > $l_st_controle [2])
                {
                    // Trop long
                    //$l_st_variable [CONTENU_VAR] = "";
                    $l_st_variable [TYPE_DISPLAY] = ERRONE;
                    return OUI;  
                }
                if (strlen ($l_st_variable [CONTENU_VAR]) == 0)
                {
                	// Trop court
                    $l_st_variable [TYPE_DISPLAY] = ERRONE;
                    return OUI;           	
                }
            }
            break;

        default:
            // type non défini, ajouter le traitement d'erreur
            // /!\ TODO : Ajouter le traitement d'erreur

    }

	// $l_st_variable [TYPE_DISPLAY] = HIDDEN; // pas utilisé pour le moment, à discuter (confort graphique et ergonomique à débattre)
    // De toutes façons, provoque un bug dans la classe de template, incapable de caser un tableau de valeurs dans son string d'affichage
    return NON;
}
/* Fin de fichier **************************************************************************************/
?>
