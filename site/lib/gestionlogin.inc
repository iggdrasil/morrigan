<?php
/****************************************************************************************************
*                                                                                                 
*           PROJET MORRIGAN                                                                      
*                                                                                                   
* Nom du fichier :  gestionlogin.inc                                                                     
* Fonction du fichier : fonctions permettant de v�rifier le login d'un utilisateur, ou de le loguer/deloguer                                
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                 
* Date de cr�ation :  24/04/2004                                                                    
* Version actuelle :  1.0 au 24/04/2004                                                            
* License du projet : gpl                                                                               
* Derni�res modifications :                                                                         
* Remarques particuli�res : nettoyer le fichier d�s qu'on est s�rs que tout tient debout     
* 							/!\ TODO : v�rifier plus en d�tail dans les fonctions login_user et veriflog
* 							/!\ TODO : choisir le type de chiffrement du password   
* 							/!\ TODO : bloquer le compte au bout de x tentatives                                                          
*                                                                                                   
*****************************************************************************************************/
function chiffre ($l_s_password)
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION chiffre                                                                         
    *   Chiffre le mot de passe au format voulu                                              
    *                                                                                                                                                      
    *   PARAMETRES : - $l_s_password : mot de passe en clair                                       
    *   RENVOIE : $l_s_password 
    *****************************************************************************************************/
    $l_s_password = md5 ($l_s_password);
        
    return $l_s_password;
}


function login_user ($l_s_login, $l_s_passwd)
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION login_user                                                                         
    *   Logue un utilisateur sur le site, en v�rifiant son authenticit�                                              
    *                                                                                                                                                      
    *   PARAMETRES : - $l_s_login : login soumis par l'utilisateur                                       
    *                - $l_s_passwd : password (chiffr�) soumis par l'utilisateur                                  
    *   RENVOIE : $l_i_logok permettant de d�finir si le login s'est pass� correctement ou pas
    *****************************************************************************************************/
    
    /* D�finition des variables locales � la fonction */
    $l_i_logok = NON; // valeur de retour de la fonction
    $l_st_login_resultat = array ();
    
    /* Corps de la fonction */
    $l_st_login_resultat = requete ("login_user", $l_s_login, $l_s_passwd);
    if ($l_st_login_resultat [0] == 0)
    {
        $l_i_logok = NON;
    }
    else // Il y a un user qui correspond � ce qui a �t� entr� en formulaire
    {
        session_start ();

        $_SESSION ['last_access'] = time ();
        $_SESSION ['ipaddr'] = $_SERVER ['REMOTE_ADDR'];
        $_SESSION ['id'] = $l_st_login_resultat [1] [0] [0];
        $_SESSION ['user'] = $l_st_login_resultat [1] [0] [1];
        $_SESSION ['passwd'] = $l_st_login_resultat [1] [0][2];
        $_SESSION ['email'] = $l_st_login_resultat [1] [0][3];
        $_SESSION ['mj'] = $l_st_login_resultat [1] [0][7];
        $_SESSION ['droitnews'] = $l_st_login_resultat [1] [0] [8];
        $_SESSION ['droitallnews'] = $l_st_login_resultat [1] [0] [9];
        $_SESSION ['droitinscription'] = $l_st_login_resultat [1] [0] [10];
        $_SESSION ['droitrecrutement'] = $l_st_login_resultat [1] [0] [11];        
        $_SESSION ['droiteditregles'] = $l_st_login_resultat [1] [0] [12];
        $_SESSION ['droiteditmonde'] = $l_st_login_resultat [1] [0] [13];
        $_SESSION ['droitadmin'] = $l_st_login_resultat [1] [0] [14];
        
        $l_i_logok = OUI;
    }
    
    return $l_i_logok;
}


function logout_user ()
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION logout_user                                                                         
    *   D�logue l'utilisateur : d�truit la session en cours                                                    
    *                                                                                                   
    *   PARAMETRES :  aucun        
    *   RENVOIE :  $l_i_logoutok : permet de savoir si le logout s'est bien pass�                                                                                
    *****************************************************************************************************/
    
    /* D�finition des variables locales � la fonction */
    $l_i_logoutok = 0; // valeur de retour de la fonction
    
    /* Corps de la fonction */
		session_unset ();
        //session_destroy ();

    
    $l_i_logoutok = 1;
    
    return $l_i_logoutok;
}


function verif_log_user ()
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION verif_log_user                                                                          
    *   V�rifie qu'un utilisateur est logu� pour avoir acc�s � la page
    *                                                                                                                                                   
    *   PARAMETRES : aucun          
    *   RENVOIE : $l_i_verifloguserok                                                                                 
    *****************************************************************************************************/
    
    /* D�finition des variables locales � la fonction */
    $l_i_verifloguserok = NON; // valeur de renvoi de la fonction
    $l_st_existence = array ();
    /* Corps de la fonction */
    // Variables de session non d�finies -> user non logu�
    if (!isset($_SESSION ['last_access']) || !isset ($_SESSION ['ipaddr']) || !isset ($_SESSION ['id']) || !isset ($_SESSION ['user']))
    {
        $l_i_verifloguserok = NON;
    }
    else
    {
    	//Temps de session expir� -> delog, donc user non logu� 
    	if ((time ()-$_SESSION ['last_access']) >SESSIONTIME)
		{
			session_destroy ();
			$l_i_verifloguserok = NON;
		}
		
		if ($_SERVER ['REMOTE_ADDR'] != $_SESSION ['ipaddr'])
		{
			session_destroy ();
			trigger_error ("Tentative d'usurpation de l'utilisateur ".$_SESSION ['user']." de la session par cette IP: ".$_SERVER ['REMOTE_ADDR']."", FATAL);
			$l_i_verifloguserok = NON;
		}
		
		 //Test pour savoir si l'user "logu�" existe
		
		$l_st_existence = requete ("testexistencelogue", $_SESSION ['user'], $_SESSION ['passwd']); // On v�rifie que l'user existe et qu'il a le bon password
		
		if ($l_st_existence[0] == NON)
		{ 
			trigger_error ("User ".$_SESSION ['user']." soit disant logu� inexistant", FATAL);
			$l_i_verifloguserok = NON;
		}
		else
		{
			if (!($l_i_droitsok = verif_droits ()))
			{
				trigger_error ("Les droits de l'user '".$_SESSION ['user']."' ne coincident pas", FATAL);
				$l_i_verifloguserok = NON;
			}
			else
			{
				$l_i_verifloguserok = OUI; // on consid�re que c'est suffisant pour d�clarer l'utilisateur logu�
			}
		}
    
    $_SESSION ['last_access'] = time();
    }
   
    return $l_i_verifloguserok;
}


function verif_droits ()
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION verif_droits                                                                         
    *   V�rifie si les droits de l'administrateur sont ceux qui sont en base
    *                                                                                                                                                    
    *   PARAMETRES : aucun         
    *   RENVOIE :  $l_i_droitsok : permet de savoir si il y a correspondance entre la base et la session        
    *****************************************************************************************************/
    
    /* D�finition des variables locales � la fonction */
    $l_i_droitsok = 0;
    $l_ti_droitsadmin = array ("news" => $_SESSION ['droitnews'], "allnews" => $_SESSION ['droitallnews'], "inscription" => $_SESSION ['droitinscription'], "recrutement" => $_SESSION ['droitrecrutement'], "edit_r" => $_SESSION ['droiteditregles'], "edit_m" => $_SESSION ['droiteditmonde'], "admin" => $_SESSION ['droitadmin']);
    $l_st_droitsadmin_req = array ();
    
    $l_i_id_adm = 0;
    $l_s_index = "";
    $l_i_valeur = 0;
    $l_i_increment = 0;
    
    /* Corps de la fonction */
    // 1/ r�cup�ration de l'identifiant de l'admin logu� (id_adm)
    $l_i_id_adm = $_SESSION ['user'];
    // 2/ Requ�te de r�cup�ration sur la bdd des droits
    $l_st_droitsadmin_req = requete ("importedroits", $l_i_id_adm);
    // 3/ Traitement des droits
    if (count($l_st_droitsadmin_req [1][0]) != NBDROITS)
    {
        trigger_error ("Erreur dans la r�cup�ration des droits de l'administrateur : nombre de droits possibles erron�", FATAL);
        exit(0);
    }
    else
    {
        foreach($l_ti_droitsadmin as $l_s_index => $l_i_valeur)
        {
            if ($l_ti_droitsadmin [$l_s_index] != $l_st_droitsadmin_req [1][0][$l_i_increment])
            {
                trigger_error ("Erreur dans la comparaison des droits de l'administrateur ".$_SESSION ['user'].", droit $l_s_index modifi�", FATAL);
                $l_i_droitsok =0;
            }
            $l_i_increment ++;
        }
        $l_i_droitsok = 1;
    }
    return $l_i_droitsok;
}

function importe_droits ()
{
    /****************************************************************************************************
    *                                                                                                   
    *   FONCTION importe_droits                                                                         
    *   R�cup�re les droits de l'utilisateur stock�s en base de donn�es
    *                                                                                                                                                    
    *   PARAMETRES :  aucun        
    *   RENVOIE :  $l_ti_droitsadmin : tableau de tous les droits de l'admin logu�           
    *****************************************************************************************************/
    
    /* D�finition des variables locales � la fonction */
    $l_ti_droitsadmin = array ("news" => 0, "allnews" => 0, "inscription" => 0, "recrutement" => 0, "edit_r" => 0, "edit_m" => 0, "admin" => 0);
    $l_st_droitsadmin_req = array ();
    
    $l_i_id_adm = 0;
    $l_s_index = "";
    $l_i_valeur = 0;
    $l_i_increment = 0;
    
    /* Corps de la fonction */
    // 1/ Requ�te de r�cup�ration sur la bdd des droits
    $l_st_droitsadmin_req = requete ("importedroits", $_SESSION ['user'], $_SESSION ['passwd']);
    // 2/ Traitement des droits
    if ($l_st_droitsadmin_req [0] [0] != NBDROITS)
    {
        trigger_error ("Erreur dans la r�cup�ration des droits de l'administrateur : nombre de droits possibles erron�", FATAL);
    }
    else
    {
        foreach($l_ti_droitsadmin as $l_s_index => $l_i_valeur)
        {
            $l_ti_droitsadmin [$l_s_index] = $l_st_droitsadmin_req [1] [$l_i_increment];
            $l_i_increment ++;
        }
    }
    return $l_ti_droitsadmin;
}

/* Fin de fichier **************************************************************************************/
?>

