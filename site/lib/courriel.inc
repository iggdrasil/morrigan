<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                  
* Nom du fichier : courriel.inc                                                                          
* Fonction du fichier : gestion de l'envoi des courriels                                               
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                       
* Date de création : 28/01/2008                                                                     
* Version actuelle : 1.0 au 28/01/2008                                                             
* License du projet : gpl                                                                              
* Dernières modifications :                                                                         
* Remarques particulières : nettoyer les traces de debug                                                                     
*                                                                                                   
*****************************************************************************************************/

function remplace_chaine ($l_s_chaine, $l_s_clef, $l_s_message)
{
/****************************************************************************************************
*                                                                                                   
*   FONCTION remplace_chaine                                                                         
*          parse une chaine de caracteres pour remplacer un tag par un texte prédéfini
*                                                                                                                                                           
*   PARAMETRES : $l_s_chaine : texte prédéfini
* 				 $l_s_clef : tag à remplacer
* 				 $l_s_message (passé par référence)         
*   RENVOIE : rien, le message est modifié en référence                                                                                 
*****************************************************************************************************/

	$l_s_remplacement = "[".$l_s_clef."]";
	$l_s_message = str_replace ($l_s_remplacement, $l_s_chaine, $l_s_message);
	
}


function construit_message ($l_t_s_champs, $l_s_template)
{
/****************************************************************************************************
*                                                                                                   
*   FONCTION construit_message                                                                         
*          Construit le message du mail à partir de son template et des champs à y placer
*                                                                                                                                                           
*   PARAMETRES : $l_t_s_champs : champs utiles à la construction du message
* 					champ par champ, la clef du tableau doit être une chaîne de caractères correspondant 
* 					au tag à remplacer
* 				 $l_s_template : nom du template à utiliser         
*   RENVOIE : $l_s_message, le message du mail à envoyer                                                                                 
*****************************************************************************************************/
	$l_s_message = "";
	$l_s_clef = "";
	$l_i_fic = NULL;
	$l_s_chemin_template = "";
	

	// Lecture du fichier txt du corps de mail
	if (!is_string ($l_s_template))
	{
		trigger_error ("Le nom du template de courriel à envoyer n'est pas une chaîne de caractères.", FATAL);
		exit ();	
	}
	
	$l_s_chemin_template = "local/mls/".$l_s_template.".ml";
	
	if(!$l_i_fic = fopen ($l_s_chemin_template, "r"))
	{
		trigger_error ("Ouverture du fichier de courriel impossible.", FATAL);
		exit ();
	}
	
	// Récupération du texte du mail pour la mise en forme
	$l_s_message = fread ($l_i_fic, filesize($l_s_chemin_template));
	if ($l_s_message == "")
	{
		trigger_error ("Lecture du fichier de courriel impossible.", FATAL);
		exit ();		
	}

	fclose ($l_i_fic);
	
	// Remplir le template avec les champs à paramétrer
	array_walk ($l_t_s_champs, "remplace_chaine", &$l_s_message);
	
	return $l_s_message;
}


function courriel($l_st_parametres)
{
/****************************************************************************************************
*                                                                                                   
*   FONCTION courriel                                                                         
*          Effectue l'envoi d'un courriel
*                                                                                                                                                           
*   PARAMETRES : $l_st_paramètres : structure contenant tous les paramètres utiles à l'envoi du courriel :
* 					  $l_st_parametres ["destinataire"]
*        			  $l_st_parametres ["copie"]
* 				      $l_st_parametres ["copie_cachee"]
* 					  $l_st_parametres ["objet"]
* 					  $l_st_parametres["champs"] : tableau des chaînes de caractères à insérer dans le courriel
* 					  $l_st_parametres ["template"]
* 
*   RENVOIE : un booléen de validité                                                                                 
*****************************************************************************************************/

	$l_t_headers = array();
	$l_s_message = "";
	$l_i_succes == NON;
	$l_o_smtp = NULL;

	$l_s_destinataire = $l_st_parametres ["destinataire"];
	// Pour les champs $expediteur / $copie / $destinataire, séparer par une virgule s'il y a plusieurs adresses
	$l_s_expediteur = EMAIL_SITE;

	$l_s_objet = $l_st_parametres ["objet"]; // Objet du message
	$l_s_hote = SMTP_HOST;
	
	$l_t_headers = array ("From" => $l_s_expediteur, "To" => $l_s_destinataire, "Subject" => $l_s_objet);
	
	$l_o_smtp = Mail::factory('smtp', array ('host' => $l_s_hote, 'auth' => false));
	
	$l_s_message = construit_message ($l_st_parametres ["champs"], $l_st_parametres ["template"]);

	if ($l_o_smtp -> send ($l_s_destinataire, $l_t_headers, $l_s_message)) // Envoi du message
	{
		//echo 'Votre message a bien été envoyé <br> ';
		$l_i_succes = OUI;
	}
	else // Non envoyé
	{
		trigger_error ("L'envoi du courriel '".$l_s_objet."' au destinataire '".$l_s_destinataire."' a echoué", FATAL);
		exit ();
	}
	
	return $l_i_succes;

}

/* Fin de fichier **************************************************************************************/
?>

