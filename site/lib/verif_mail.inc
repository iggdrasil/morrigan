<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                  
* Nom du fichier : verif_mail.inc                                                                          
* Fonction du fichier : gestion de la validité des emails                                               
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  
* Date de création : 08/05/2006                                                                     
* Version actuelle : 1.0 au 08/05/2006                                                             
* License du projet : gpl                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                        
*                                                                                                   
*****************************************************************************************************/

function check_mail ($l_s_email)
{
/****************************************************************************************************
*                                                                                                   
*   FONCTION check_mail                                                                         
*          Effectue la connexion à la base de données du site
*                                                                                                                                                           
*   PARAMETRES : $l_s_email         
*   RENVOIE : un booléen de validité                                                                                 
*****************************************************************************************************/
    
	// checks proper syntax
 	print ("Adresse : ".$l_s_email."<br>");
 	
 	if(preg_match ('/[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)*\@[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)+/i' , $l_s_email))
 	{
  		print ("Syntaxe correcte<br>");
  		// gets domain name
  		list($l_s_username,$l_s_domain)=split('@',$l_s_email);
  		// checks for if MX records in the DNS
  		if(!checkdnsrr($l_s_domain, 'MX'))
  		{
   			print ("DNS incorrect<br>");
   			return NON;
  		}
  		
  		// attempts a socket connection to mail server
  		/*if(!fsockopen($l_s_domain,25,$errno,$errstr,30))
  		{
   			print ("Socket impossible à ouvrir<br>");
   			return NON;
  		}*/
  		return OUI;
 	}
 	return NON;
}
?>
