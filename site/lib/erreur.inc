<?php
/****************************************************************************************************
*                                                                                                   *
*           PROJET MORRIGAN                                                                         *
*                                                                                                   *
* Nom du fichier : erreur.inc                                                                       *
* Fonction du fichier : Librairies de fonctions de gestions d'erreur                                *
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  *
* Date de cr�ation : 04/04/2004                                                                     *
* Version actuelle : 1.0 au 04/04/2004                                                              *
* License du projet : gpl                                                                              *
* Derni�res modifications :                                                                         *
* Remarques particuli�res :                                                                         *
*                                                                                                   *
*****************************************************************************************************/

function myErrorHandler ($l_i_errno, $l_s_errstr, $l_s_errfile, $l_i_errline)
{
/****************************************************************************************************
*                                                                                                   *
*   FONCTION myErrorHandler                                                                         *
*   G�re les erreurs d�clench�es par les pages du site                                              *
*   Selon l'option activ�e, log de l'erreur dans un fichier et redirection vers page d'erreur       *
*   ou affichage � l'�cran de l'erreur + log                                                        *
*                                                                                                   *
*   PARAMETRES : - $errno : num�ro de l'erreur, g�r� par PHP                                        *
*                - $errstr : description de l'erreur, g�r� par PHP                                  *
*                - $errfile : fichier o� a eu lieu l'erreur, g�r� par PHP                           *
*                - $errline : ligne o� a eu lieu l'erreur dans le fichier, g�r� par PHP             *
*   RENVOIE : rien                                                                                  *
*****************************************************************************************************/

	/* D�finition des variables locales � la fonction */
	// D�finition du message d'erreur
	$l_ts_errortype = array (
               E_ERROR          => "Error",
               E_WARNING        => "Warning",
               E_PARSE          => "Parsing Error",
               E_NOTICE          => "Notice",
               E_CORE_ERROR      => "Core Error",
               E_CORE_WARNING    => "Core Warning",
               E_COMPILE_ERROR  => "Compile Error",
               E_COMPILE_WARNING => "Compile Warning",
               E_USER_ERROR      => "User Error",
               E_USER_WARNING    => "User Warning",
               E_USER_NOTICE    => "User Notice",
               E_STRICT          => "Runtime Notice"
               );

	$l_s_messageerreur = "[".date("D j M G:i:s T Y")."] [morrigan][".$l_ts_errortype[$l_i_errno]."] $l_s_errstr survenue dans $l_s_errfile � la ligne $l_i_errline";

	// D�finition du mode debug
	global $g_i_debug;
 
	// R�cup�ration de la variable host
	global $g_s_host;

	/* Corps de la fonction */
	if ($g_i_debug == OUI)
	{
		// Mode DEBUG on (administrateur  a demand� le debug)
		print($l_s_messageerreur."<br>\n");
	}
	else
	{
		// Mode DEBUG off (par d�faut)
		switch($l_i_errno)
		{
			case E_USER_NOTICE:
				// Les notices ne redirigent pas vers la page d'erreur
				$erreur = error_log ($l_s_messageerreur, 0);
			case E_NOTICE:
				// Les notices ne font rien sous peine d'�craser le log de conneries
				break;

			default:
				$erreur = error_log ($l_s_messageerreur, 0);
                header ("Location:http://".HOST."/erreur.php");
				exit ();
				break;
		}
	}
}

?>
