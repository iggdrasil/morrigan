<?php
/****************************************************************************************************
*
*           PROJET MORRIGAN
*
* Nom du fichier :  mdp.inc
* Fonction du fichier :  bibliothèque de fonctions ayant trait à la génération des mots de passe
* Auteur : Arkenlond (arkenlond@peacefrogs.net)  
* Date de création :  03/02/2008
* Version actuelle :  1.0 au 03/02/2008
* License du projet : gpl
* Dernières modifications :
* Remarques particulières : 
*
*****************************************************************************************************/

function genere_password ($l_i_longueur = 8, $l_i_min = OUI, $l_i_epelle = NON)
{
/****************************************************************************************************
*
*   FONCTION genere_password
*   Fabrique un mot de passe aux petits oignons
*	Auteur : intégralement repris sur une fonction générée par CrazyCat et proposée sur http://www.crazycat.info/
*   PARAMETRES :
*     - $l_i_longueur : klongueur du mot de passe à générer
* 	  - $l_i_min : minuscules ou majuscules
* 	  - $l_i_epelle : renvoie l'épelation du mot de passe
*   RENVOIE :
*     - $l_t_password : tableau contenant le mot de passe sous la forme : 
*      {id; nom de l'option}
*****************************************************************************************************/

	$l_t_password = array ("string" => "", "litteral" => "");
	$l_i_compteur = 0;
	$l_i_valeur = 0;
	
	$l_t_ok_chars = Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Alpha", "Bravo", "Charlie", "Delta", 
       "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliet", "Kilo", "Lima", "Mike", "November", "Oscar", 
    "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whisky", "X-ray", "Yankee", "Zulu");
	// Le tableau des caractères utilise le code phonétique international
	
	for ($l_i_compteur = 0; $l_i_compteur < $l_i_longueur; $l_i_compteur++)
	{
    	$l_i_valeur = rand (0, count($l_t_ok_chars) - 1);
    	$l_t_password ["string"] .= $l_t_ok_chars [$l_i_valeur] {0} ;
    	$l_t_password ["litteral"] .= $l_t_ok_chars[$l_i_valeur]." ";
	}

	if ($l_i_min == OUI) 
	{
		$l_t_password ["string"] = strtolower ($l_t_password ["string"]);
	}
  
	$l_t_password ["litteral"] = trim ($l_t_password ["litteral"]);
	
	
	if ($l_i_epelle == OUI)
	{
		return $l_t_password;
   	}
	else
	{
    	return $l_t_password ["string"];
   	}
}

?>
