<?php
/****************************************************************************************************
*                                                                                                   
*           PROJET MORRIGAN                                                                         
*                                                                                                   
* Nom du fichier : admin_fonctions.php                                                                                 
* Fonction du fichier : regroupe quelques fionctions utiles à l'administration
*                                  
* Auteur : Arkenlond  (arkenlond@peacefrogs.net)                                                                                          
* Date de création :  02/10/2007                                                                              
* Version actuelle :  1.0 au 02/10/2007                                                                             
* License du projet : gpl                                                                              
* Dernières modifications :                                                                         
* Remarques particulières :                                                                         
*                                                                                                   
*****************************************************************************************************/



function affiche_ligne_profil ($l_st_nomsection, $l_i_numsection)
{
/****************************************************************************************************
*
*   FONCTION affiche_ligne_profil
*   Gère l'affichage d'une ligne de tableau récapitulant les infos d'un profil
*
*   PARAMETRES :
*      - $l_st_nomsection : nom de la zone de template à afficher
*   RENVOIE :
*       - rien, l'affichage est ajouté dans le flux de la page
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    global $l_o_template;
    global $l_i_handlerlisteprofil_niv1;
    global $g_t_st_infosprofilbase;
    global $g_i_compteur;
    $l_st_equivalencestatut = array (EDITIONTXT, RELECTURETXT, HORSLIGNETXT, ENLIGNETXT);


    $l_st_variableid = array ("idprofil".$l_i_numsection, $g_t_st_infosprofilbase [1][$g_i_compteur][0], HIDDEN); // Permet le renseignement du profil à afficher ultérieurement
    $l_st_variableaction = array ("actionprofil".$l_i_numsection, AFFICHER, HIDDEN); // Permet le renseignement de l'action à exécuter ultérieurement

    /* Corps de la fonction */
    $l_o_template -> addSession ($l_i_handlerlisteprofil_niv1, $l_st_nomsection);
    $l_o_template -> setVar ($l_i_handlerlisteprofil_niv1, $l_st_nomsection.".auteur", $g_t_st_infosprofilbase [1][$g_i_compteur][1]);
    $l_o_template -> setVar ($l_i_handlerlisteprofil_niv1, $l_st_nomsection.".statut", $l_st_equivalencestatut [$g_t_st_infosprofilbase [1][$g_i_compteur][2]]);
    $l_o_template -> setVar ($l_i_handlerlisteprofil_niv1, $l_st_nomsection.".categorie", $g_t_st_infosprofilbase [1][$g_i_compteur][3]);
    $l_o_template -> setVar ($l_i_handlerlisteprofil_niv1, $l_st_nomsection.".nom", $g_t_st_infosprofilbase [1][$g_i_compteur][4]);
    
    if ($g_t_st_infosprofilbase [1][$g_i_compteur][5] == -1)
	{
   		$g_t_st_infosprofilbase [1][$g_i_compteur][5] = NOLIMITCAND;
	}
    
    $l_o_template -> setVar ($l_i_handlerlisteprofil_niv1, $l_st_nomsection.".nbpersos", $g_t_st_infosprofilbase [1][$g_i_compteur][5]);
    affiche_variable ($l_st_variableid, "", $l_i_handlerlisteprofil_niv1, NULL);
    affiche_variable ($l_st_variableaction, "", $l_i_handlerlisteprofil_niv1, NULL);
    $l_o_template -> closeSession ($l_i_handlerlisteprofil_niv1, $l_st_nomsection);

}


function verif_demande_action ($l_i_idprofil)
{
/****************************************************************************************************
*
*   FONCTION verif_demande_action
*   Permet de savoir si l'action faite par le MJ Inscription correspond bien à la demande qui
* 	a été faite par un autre MJ (mise en/hors ligne, mise en édition)
*
*   PARAMETRES :
*      - $l_i_idprofil : nom de la zone de template à afficher
*   RENVOIE :
*       - OUI ou NON selon que l'action a bien réglé l'action ou pas
*****************************************************************************************************/
    
    /* Définition des variables locales à la fonction */
	$l_st_requete = array (0, array ());
	$l_i_flag_ok = NON;

    /* Corps de la fonction */
    $l_st_requete = requete ("recupere_statut_profil", $l_i_idprofil);
    if ($l_st_requete [0] == 0)
    {
    	trigger_error ("Impossible d'extraire le statut du profil '".$l_i_idprofil."'", FATAL);
    	exit ();
    }
    else
    {
    	switch ($l_st_requete [1][0][1])
    	{
    		case DEMANDEENLIGNE:
    			if ($l_st_requete [1][0][0] == ENLIGNE)
    			{
    				
    				$l_i_flag_ok = OUI;
    			}
    			break;
       		case DEMANDEHORSLIGNE: 
    			if ($l_st_requete [1][0][0] == HORSLIGNE)
    			{
    				$l_i_flag_ok = OUI;
    			}
    			break;
    		case DEMANDEEDITER: 
    			if ($l_st_requete [1][0][0] == EDITION)
    			{
    				$l_i_flag_ok = OUI;
    			}
    			break;
    		default:
    			$l_i_flag_ok = NON;
    			break; 
    	}	
    }
	
	if ($l_i_flag_ok == OUI)
	{
		// La requete refuse_action est en fait juste une requete effaçant la demande d'action de la base
		$l_st_requete = requete ("refuse_action", $l_i_idprofil);
		if ($l_st_requete ["nbaffecte"] != NULL)
		{
			return OUI;
		}
		else 
		{
			trigger_error ("L'action n'a pas pu être effacée du profil '".$l_i_idprofil."'", FATAL);
			exit ();
		}
	}
	else 
	{
		return NON;			
	}
}


function affiche_ligne_candidature ($l_st_nomsection)
{
/****************************************************************************************************
*
*   FONCTION affiche_ligne_candidature
*   Gère l'affichage d'une ligne de tableau récapitulant les infos d'une candidature
*
*   PARAMETRES :
*      - $l_st_nomsection : nom de la zone de template à afficher
*   RENVOIE :
*       - rien, l'affichage est ajouté dans le flux de la page
*****************************************************************************************************/

    /* Définition des variables locales à la fonction */
    global $l_o_template;
    global $l_i_handlerlistecandidature_niv1;
    global $g_t_st_infoscandidaturebase;
    global $g_i_compteur;

    $l_st_variableid = array ("idcandidature", $g_t_st_infoscandidaturebase [1][$g_i_compteur][0], HIDDEN); // Permet le renseignement de la candidature à afficher ultérieurement
    $l_st_variableaction = array ("actioncandidature", AUCUNE, HIDDEN); // Permet le renseignement de l'action à exécuter ultérieurement
    $l_st_variableiteration = array ("iteration", 1, HIDDEN);  

    /* Corps de la fonction */
    $l_o_template -> addSession ($l_i_handlerlistecandidature_niv1, $l_st_nomsection);
    $l_o_template -> setVar ($l_i_handlerlistecandidature_niv1, $l_st_nomsection.".joueur", $g_t_st_infoscandidaturebase [1][$g_i_compteur][1]);
    $l_o_template -> setVar ($l_i_handlerlistecandidature_niv1, $l_st_nomsection.".personnage", $g_t_st_infoscandidaturebase [1][$g_i_compteur][3]);
    $l_o_template -> setVar ($l_i_handlerlistecandidature_niv1, $l_st_nomsection.".nomprofil", $g_t_st_infoscandidaturebase [1][$g_i_compteur][2]);
    $l_o_template -> setVar ($l_i_handlerlistecandidature_niv1, $l_st_nomsection.".dernieremodif", $g_t_st_infoscandidaturebase [1][$g_i_compteur][4]);

    affiche_variable ($l_st_variableid, "", $l_i_handlerlistecandidature_niv1, NULL);
    affiche_variable ($l_st_variableaction, "", $l_i_handlerlistecandidature_niv1, NULL);
    affiche_variable ($l_st_variableiteration, "", $l_i_handlerlistecandidature_niv1, NULL);
    $l_o_template -> closeSession ($l_i_handlerlistecandidature_niv1, $l_st_nomsection);

}


function affiche_action_candidature ($l_i_action)
{
/****************************************************************************************************
*
*   FONCTION affiche_action_candidature
*   Gère l'affichage d'une option de candidature
*
*   PARAMETRES :
*      - $l_i_action : permet de savoir quelle action on va afficher
*   RENVOIE :
*       - rien, l'affichage est ajouté dans le flux de la page
*****************************************************************************************************/

    /* Définition des variables locales et globales à la fonction */
    global $l_o_template;
    global $l_i_handleractionscandidature_niv1;
    global $g_st_statutcandidature;
    $l_i_equivalentaction = array (AUCUNETXT, COMMENTERPJTXT, DONNERAVISMJTXT, DEMANDERAVISTXT, REFUSERCANTXT, EFFACERCANTXT, VALIDERCANTXT, METTREENATTENTETXT, COMMENTERGLOBALTXT, CHANGERSTATUTTXT);

    /* Corps de la fonction */
    $l_o_template -> addSession ($l_i_handleractionscandidature_niv1, "actioncandidature");
    $l_o_template -> setVar ($l_i_handleractionscandidature_niv1, "actioncandidature.actioncandidature", $l_i_action);
    $l_o_template -> setVar ($l_i_handleractionscandidature_niv1, "actioncandidature.texteactioncandidature", $l_i_equivalentaction [$l_i_action]);
    if ($l_i_action == CHANGERSTATUT)
    {
    	$g_st_statutcandidature [TYPE_DISPLAY] = NORMAL;
    	affiche_variable ($g_st_statutcandidature, "select", $l_i_handleractionscandidature_niv1, NULL);	
    }
    
    $l_o_template -> closeSession ($l_i_handleractionscandidature_niv1, "actioncandidature");
}
/* Fin de fichier **************************************************************************************/
?>
