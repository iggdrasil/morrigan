<?php
/****************************************************************************************************
*                                                                                                   *
*           PROJET MORRIGAN                                                                         *
*                                                                                                   *
* Nom du fichier : index.php                                                                        *
* Fonction du fichier : redirige vers la page d'accueil                                             *
* Auteur : Arkenlond (arkenlond@peacefrogs.net)                                                                                  *
* Date de cr�ation : 04/04/2004                                                                     *
* Version actuelle : 1.0                                                                            *
* License du projet : GPL                                                                              *
* Derni�res modifications :                                                                         *
* Remarques particuli�res :                                                                         *
*                                                                                                   *
*****************************************************************************************************/

/* Ouverture de session */
// N/A

/* D�finition des variables */
// N/A

/* R�cup�ration des variables pass�es en formulaire */
// N/A

/* R�cup�ration des variables de session */
// N/A

/* Inclusion des biblioth�ques */
require_once("local/def/def_vars.inc");

/* Connexion � la Base de Donn�es */
// N/A

/* Contenu et inclusion des fichier de contenu */
// Redirection vers la page d'accueil
header("Location:http://".HOST."/accueil.php");
exit();

/* Gestion des erreurs sur la page */
// N/A

/* Parsage final de la page */

/* D�connexion de la Base de Donn�es */

/* Fin de fichier ***********************************************************************************/
?>
